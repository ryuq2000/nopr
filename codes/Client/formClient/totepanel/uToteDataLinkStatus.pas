unit uToteDataLinkStatus;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls, ExtCtrls,uTotePanel;

type
  TfrmDataLinkStatus = class(TfrmTotePanel)
    gbDataLinkStatus: TPanel;
    pnlDS2: TPanel;
    pnlDS8: TPanel;
    pnlDS9: TPanel;
    lvDS2: TListView;
    pnlDS10: TPanel;
    lblDS5: TLabel;
    lblDSStatus: TLabel;
    lbDSBand: TLabel;
    lbDSStatusValue: TLabel;
    lbDSBandValue: TLabel;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmDataLinkStatus: TfrmDataLinkStatus;

implementation

{$R *.dfm}

end.
