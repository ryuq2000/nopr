object frmComChannelDefinition: TfrmComChannelDefinition
  Left = 0
  Top = 0
  Caption = 'ComChannelDefinition'
  ClientHeight = 797
  ClientWidth = 1090
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object gbCommunicationsChannelDefinition: TPanel
    Left = 0
    Top = 0
    Width = 1090
    Height = 797
    Align = alClient
    TabOrder = 0
    object Panel24: TPanel
      Left = 1
      Top = 39
      Width = 1088
      Height = 757
      Align = alClient
      BevelOuter = bvNone
      BorderWidth = 4
      TabOrder = 1
      object sgCommChannelDef: TStringGrid
        Left = 4
        Top = 4
        Width = 1080
        Height = 749
        Align = alClient
        ColCount = 7
        DrawingStyle = gdsGradient
        FixedCols = 0
        RowCount = 100
        TabOrder = 0
        ColWidths = (
          116
          98
          141
          82
          94
          90
          360)
      end
      object cbbBand: TComboBox
        Left = 544
        Top = 400
        Width = 145
        Height = 21
        ItemIndex = 0
        TabOrder = 1
        Text = 'HF'
        Visible = False
        Items.Strings = (
          'HF'
          'VHF/UHF'
          'SATCOM'
          'UWT'
          'FM')
      end
    end
    object Panel9: TPanel
      Left = 1
      Top = 1
      Width = 1088
      Height = 38
      Align = alTop
      Alignment = taLeftJustify
      BorderWidth = 4
      Caption = 'Communications Channel Definition'
      Color = clBtnShadow
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -24
      Font.Name = 'Arial'
      Font.Style = []
      ParentBackground = False
      ParentFont = False
      TabOrder = 0
    end
  end
end
