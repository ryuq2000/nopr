unit uTotePlatformStatus;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls, ExtCtrls, ImgList, Menus,uTotePanel, uT3Vehicle ;

type
  TfrmPlatformStatus = class(TfrmTotePanel)
    gbPlatformStatus: TPanel;
    pnlPlatformLeft: TPanel;
    pnlPlatforms: TPanel;
    Image1: TImage;
    Label75: TLabel;
    lvPlatforms: TListView;
    Panel13: TPanel;
    btSelectHookedPlatform: TButton;
    btHookSelectedPlatform: TButton;
    pnlPlatEmbarked: TPanel;
    Label79: TLabel;
    tvEmbarkedPlatforms: TTreeView;
    edtEmbarkQuantity: TEdit;
    Panel2: TPanel;
    pnlPlatformRight: TPanel;
    pnlPlatCounterMeasure: TPanel;
    Label78: TLabel;
    tvCountermeasures: TTreeView;
    pnlPlatSystemState: TPanel;
    Label80: TLabel;
    lvSystemState: TListView;
    edtState: TEdit;
    pnlPlatSensor: TPanel;
    Label76: TLabel;
    lvSensors: TListView;
    pnlPlatWeapon: TPanel;
    Label77: TLabel;
    edtWeaponQuantity: TEdit;
    tvWeapons: TTreeView;
    ImageList1: TImageList;
    pmPlatformLV: TPopupMenu;
    Repair3: TMenuItem;
    RepairAll1: TMenuItem;
    procedure FormResize(Sender: TObject);
    procedure lvPlatformsMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure lvPlatformsSelectItem(Sender: TObject; Item: TListItem;
      Selected: Boolean);
  private
    { Private declarations }
    procedure AdjustComponentDisplayPosition;
    procedure RefreshPlatformList;

    procedure UpdateVehicle(sender : TT3Vehicle);
    procedure UpdateSensorVehicle(sender : TT3Vehicle);
    procedure UpdateWeaponVehicle(sender : TT3Vehicle);
    procedure UpdateCountermeasureVehicle(sender : TT3Vehicle);
    procedure UpdateStatusVehicle(sender : TT3Vehicle);
    procedure UpdateEmbarkVehicle(sender : TT3Vehicle);
  public
    { Public declarations }
    procedure UpdateForm;override;
  end;

var
  frmPlatformStatus: TfrmPlatformStatus;

implementation

uses
  uT3PlatformInstance, uT3ClientManager, uGlobalVar, tttData,
  uT3Track , ufTacticalDisplay, uT3MountedDevice, uT3MountedSensor,
  uT3MountedRadar, uT3MountedSonar, uT3MountedVisual,
  uT3MountedMAD, uT3MountedEO, uT3MountedIFF, uT3MountedESM,
  uT3MountedWeapon,
  uT3MountedGun, uT3MountedMissile, uT3MountedTorpedo,
  uT3MountedBomb, uT3MountedMine, uT3MountedHybrid,
  uT3MountedECM;

{$R *.dfm}

{ TfrmPlatformStatus }

procedure TfrmPlatformStatus.AdjustComponentDisplayPosition;
var
  w, h: Integer;
begin
  pnlPlatformRight.Width := gbPlatformStatus.Width div 3;
  if pnlPlatformRight.Height < 740 then
    h := (pnlPlatformRight.Height - 20) div 4
  else
    h := 180;

  pnlPlatSystemState.Height := h;
  pnlPlatCounterMeasure.Height := h;
  pnlPlatCounterMeasure.Height := h;
  pnlPlatEmbarked.Height := h;

  btHookSelectedPlatform.Left := pnlPlatformLeft.Width -
    btHookSelectedPlatform.Width - 16;
  btSelectHookedPlatform.Left := btHookSelectedPlatform.Left -
    btSelectHookedPlatform.Width - 8;

  w := lvPlatforms.Width;
  lvPlatforms.Column[0].Width := Round(0.3 * w);
  lvPlatforms.Column[1].Width := Round(0.3 * w);
  lvPlatforms.Column[2].Width := Round(0.2 * w);
  lvPlatforms.Column[3].Width := Round(0.1 * w);

  w := lvSensors.Width;
  lvSensors.Column[0].Width := Round(0.6 * w);
  lvSensors.Column[1].Width := Round(0.3 * w);
end;

procedure TfrmPlatformStatus.FormResize(Sender: TObject);
begin
  AdjustComponentDisplayPosition;
end;

procedure TfrmPlatformStatus.lvPlatformsMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  pos : TPoint;
  I, index : Integer;
begin
  if (lvPlatforms.Selected = nil) or (lvPlatforms.ItemIndex = -1) then
  begin
    if Assigned(clientManager.ControlledTrack) then
    begin
      clientManager.ControlledTrack.isSelected := True;
      Exit;
    end;
  end;

  for I := 0 to frmTacticalDisplay.cbAssumeControl.Items.Count - 1 do
  begin
    if Assigned(lvPlatforms.Selected) and
      (TT3Track(frmTacticalDisplay.cbAssumeControl.Items.Objects[I]).ObjectInstanceIndex =
        TT3PlatformInstance(lvPlatforms.Selected.Data).InstanceIndex) then
    begin
      index := I;
      frmTacticalDisplay.cbAssumeControl.ItemIndex := index;
      frmTacticalDisplay.cbAssumeControlChange(self);
      frmTacticalDisplay.MapTouch.Repaint;
      Break;
    end;
  end;

  if machineRole = crCubicle then
    Exit
  else
  begin
    GetCursorPos(pos);
    if (Button = mbRight) and (lvPlatforms.Selected <> nil) then
    begin
      pmPlatformLV.Popup(pos.X, pos.Y);
    end;
  end;
end;

procedure TfrmPlatformStatus.lvPlatformsSelectItem(Sender: TObject;
  Item: TListItem; Selected: Boolean);
begin
  if (Item = nil) or (item.Data = nil) then
    Exit;

//  if Assigned(ToteSelectedPlatform) and (ToteSelectedPlatform = Item.Data)  then
//    exit;

//  FLastPlatform := ToteSelectedPlatform;
//  ToteSelectedPlatform := Item.Data;
//
//  //------- remove asset listener first for old vehicle
//  RemoveAssetListener(FLastPlatform);
  //-------

  UpdateVehicle(Item.Data);

  //------- then add asset listener for new one
//  AddAssetListener(ToteSelectedPlatform);
  //-------

end;

procedure TfrmPlatformStatus.RefreshPlatformList;
var
  s          : string;
  li         : TListItem;
  pi         : TT3PlatformInstance;
  i          : Integer;
  forceColor : String;
begin

  lvPlatforms.Items.Clear;

  with clientManager do
  begin
    if machineRole = crController then
    begin
      for i := 0 to SimPlatforms.itemCount - 1 do
      begin
        pi := TT3PlatformInstance(SimPlatforms.getObject(i));
		    if not(Assigned(pi)) then Continue;

        s := pi.InstanceName;

        case pi.ForceDesignation of
          1:
            begin
              lvPlatforms.Canvas.Brush.Color := clRed;
              forceColor := 'Red';
            end;
          2:
            begin
              lvPlatforms.Canvas.Brush.Color := clYellow;
              forceColor := 'Yellow';
            end;
          3:
            begin
              lvPlatforms.Canvas.Brush.Color := clBlue;
              forceColor := 'Blue';
            end;
          4:
            begin
              lvPlatforms.Canvas.Brush.Color := clGreen;
              forceColor := 'Green';
            end;
          5:
            begin
              lvPlatforms.Canvas.Brush.Color := clWhite;
              forceColor := 'No Force';
            end;
          6:
            begin
              lvPlatforms.Canvas.Brush.Color := clBlack;
              forceColor := 'Black';
            end;
        else
          lvPlatforms.Canvas.Brush.Color := clBlack;
          forceColor := 'Black'; //mk
        end;

        if pi is TT3Vehicle then     //mk
        begin

          if not(pi.FreeMe) then
          begin
            li            := lvPlatforms.Items.Add;
            li.Caption    := pi.PlatformClass;
            li.StateIndex := pi.ForceDesignation;
            li.Data       := pi;

            li.SubItems.Add(pi.InstanceName);
            li.SubItems.Add(pi.TrackID);
            li.SubItems.Add(forceColor);
          end;
        end;
      end;
    end
    else
    begin
      for i := 0 to SimPlatforms.itemCount - 1 do
      begin

        pi := TT3PlatformInstance(SimPlatforms.getObject(i));
        if not(Assigned(pi)) then
          Continue;

        if clientManager.MyForceDesignation = -1 then
          break
        else
        begin
          if clientManager.MyForceDesignation <> pi.ForceDesignation then
            continue;
        end;

        case pi.ForceDesignation of
          1:
          begin
            lvPlatforms.Canvas.Brush.Color := clRed;
            forceColor := 'Red';
          end;
          2:
          begin
            lvPlatforms.Canvas.Brush.Color := clYellow;
            forceColor := 'Yellow';
          end;
          3:
          begin
            lvPlatforms.Canvas.Brush.Color := clBlue;
            forceColor := 'Blue';
          end;
          4:
          begin
            lvPlatforms.Canvas.Brush.Color := clGreen;
            forceColor := 'Green';
          end;
          5:
          begin
            lvPlatforms.Canvas.Brush.Color := clWhite;
            forceColor := 'No Force';
          end;
          6:
          begin
            lvPlatforms.Canvas.Brush.Color := clBlack;
            forceColor := 'Black';
          end;
        else
          lvPlatforms.Canvas.Brush.Color := clBlack;
          forceColor := 'Black'; //mk
        end;

        if pi is TT3Vehicle then     //mk
        begin
          if not(pi.FreeMe) then
          begin
            li            := lvPlatforms.Items.Add;
            li.Caption    := pi.PlatformClass;
            li.StateIndex := pi.ForceDesignation;
            li.Data       := pi;
            li.SubItems.Add(pi.InstanceName);
            li.SubItems.Add('');
            li.SubItems.Add(forceColor);
          end;
        end;
      end;
    end;
  end;

end;

procedure TfrmPlatformStatus.UpdateCountermeasureVehicle(sender: TT3Vehicle);
var
  i         : Integer;
  ecmstatus : string;
  tn        : TTreeNode;
  counter   : TT3MountedECM;
begin
  if not Assigned(sender) then
    Exit;

  tvCountermeasures.Items.Clear;

//  if Assigned(sender.MountedECMs) then
//  begin
//    tn := TTreeNode.Create(tvCountermeasures.Items);
//
//    for counter in sender.MountedECMs do
//    begin
//
//      case counter.Status of
//        esAvailable:
//          ecmStatus := 'Available';
//        esUnavailable:
//          ecmStatus := 'Unavailable';
//        esLaunchingChaff:
//          ecmStatus := 'Launching Chaff';
//        esDamaged:
//          ecmStatus := 'Damaged';
//        esOn:
//          ecmStatus := 'On';
//        esOff:
//          ecmStatus := 'Off';
//        esEMCON:
//          ecmStatus := 'EMCON';
//        esAutomatic:
//          ecmStatus := 'Automatic';
//        esManual:
//          ecmStatus := 'Manual';
//        esDeployed:
//          ecmStatus := 'Deployed';
//        esStowed:
//          ecmStatus := 'Stowed';
//      end;
//
//      tn.Text := counter.InstanceName + ' : ' + ecmstatus;
//      tvCountermeasures.Items.AddChildObject(tn, tn.Text, counter);
//    end;
//  end;
end;

procedure TfrmPlatformStatus.UpdateEmbarkVehicle(sender: TT3Vehicle);
begin

end;

procedure TfrmPlatformStatus.UpdateForm;
begin
  inherited;

  RefreshPlatformList;
end;

procedure TfrmPlatformStatus.UpdateSensorVehicle(sender: TT3Vehicle);
var
  i       : Integer;
  li      : TListItem;
  sensor  : TT3MountedSensor;
begin
  if not Assigned(sender) then
    Exit;

  lvSensors.Items.Clear;

//  if Assigned(sender.MountedSensors) then
//  begin
//    for sensor in sender.MountedSensors do
//    begin
//
//      li := lvSensors.Items.Add;
//
//      if sensor is TT3MountedRadar then
//        li.StateIndex := 1
//      else if sensor is TT3MountedSonar then
//        li.StateIndex := 0
//      else if sensor is TT3MountedVisual then
//        li.StateIndex := 2
//      else if sensor is TT3MountedMAD then
//        li.StateIndex := 4
//      else if sensor is TT3MountedESM then
//        li.StateIndex := 3
//      else if sensor is TT3MountedEO then
//        li.StateIndex := 4
//      else if sensor is TT3MountedIFF then
//        li.StateIndex := 5
//      else
//        li.StateIndex := 0;
//
//      if sensor.EmconOperationalStatus = EmconOff then
//      begin
//        case sensor.OperationalStatus of
//          sopOff, sopOffIFF:
//            li.SubItems.Add('Off');
//          sopOn:
//            li.SubItems.Add('On');
//          sopDamage:
//            li.SubItems.Add('Damaged');
//          sopTooDeep:
//            li.SubItems.Add('Too Deep');
//          sopEMCON:
//            li.SubItems.Add('EMCON');
//          sopActive:
//            li.SubItems.Add('Active');
//          sopPassive:
//            li.SubItems.Add('Passive');
//          sopTooFast:
//            li.SubItems.Add('Too Fast');
//          sopDeploying:
//            li.SubItems.Add('Deploying');
//          sopDeployed:
//            li.SubItems.Add('Deployed');
//          sopStowing:
//            li.SubItems.Add('Stowing');
//          sopStowed:
//            li.SubItems.Add('Stowed');
//        end;
//      end
//      else
//        li.SubItems.Add('EMCON');
//
//      li.Caption := sensor.InstanceName;
//      li.Data := sensor;
//    end;
//  end;
end;

procedure TfrmPlatformStatus.UpdateStatusVehicle(sender: TT3Vehicle);
var
  li      : TListItem;
begin
  if not Assigned(sender) then
    Exit;

  lvSystemState.Items.Clear;

  //Overall Damage
  li         := lvSystemState.Items.Add;
  li.Data    := TT3PlatformInstance(sender);
  li.Caption := 'Overall Damage';
  li.SubItems.Add(IntToStr(sender.DamageOverall) + ' %');

  //Helm
  li         := lvSystemState.Items.Add;
  li.Data    := TT3PlatformInstance(sender);
  li.Caption := 'Helm';
  if sender.DamageHelm then
    li.SubItems.Add('Damage')
  else
    li.SubItems.Add('Operational');

  //Propultion
  li         := lvSystemState.Items.Add;
  li.Data    := TT3PlatformInstance(sender);
  li.Caption := 'Propulsion';
  if sender.DamagePropulsion then
    li.SubItems.Add('Damage')
  else
    li.SubItems.Add('Operational');

  //Speed
  li         := lvSystemState.Items.Add;
  li.Data    := TT3PlatformInstance(sender);
  li.Caption := 'Speed';
  li.SubItems.Add(IntToStr(sender.DamagePercentSpeed)  +' %');

  //Fuel Remaining
  li         := lvSystemState.Items.Add;
  li.Data    := TT3PlatformInstance(sender);
  li.Caption := 'Fuel Remaining';
  li.SubItems.Add(FloatToStr(Round(sender.FuelPercentage)) + ' %');

  //Fuel Leakage
  li         := lvSystemState.Items.Add;
  li.Data    := TT3PlatformInstance(sender);
  li.Caption := 'Fuel Leakage';
  if sender.FuelLeakage then
    li.SubItems.Add('Yes')
  else
    li.SubItems.Add('No');

  //Communication
  li         := lvSystemState.Items.Add;
  li.Data    := TT3PlatformInstance(sender);
  li.Caption := 'Communications';
  if sender.DamageComm then
    li.SubItems.Add('Damage')
  else
    li.SubItems.Add('Operational');
end;

procedure TfrmPlatformStatus.UpdateVehicle(sender: TT3Vehicle);
begin
  if not Assigned(sender) then
    Exit;

  // --  clear embarked timer event first
//  if tvEmbarkedPlatforms.Items.count > 0 then
//    unAssignedAllEmbarkedTimerEvent(tvEmbarkedPlatforms.items.Item[0]);

  UpdateSensorVehicle(sender);
  UpdateWeaponVehicle(sender);
  //  setUpTreeWeapons(sender.Weapons);
  UpdateCountermeasureVehicle(sender);
  UpdateStatusVehicle(sender);
  UpdateEmbarkVehicle(sender);

end;

procedure TfrmPlatformStatus.UpdateWeaponVehicle(sender: TT3Vehicle);
const
  TORPEDO = 0;
  MISSILE = 1;
  MINE = 2;
  GUN = 3;
  BOMB = 4;
var
  tnParent             : TTreeNode;
  i, j                 : Integer;
  WeaponText,
  LauncherText, status : String;
  weapon               : TT3MountedWeapon;

//  WeaponLauncher : TFitted_Weap_Launcher_On_Board;
//  launcherNumber : integer;
begin
//  if not Assigned(sender) then
//    Exit;
//
//  tvWeapons.Items.Clear;
//
//  if Assigned(sender.MountedWeapons) then
//  begin
//    for weapon in sender.MountedWeapons do
//    begin
//      if not(Assigned(weapon)) then Continue;
//
//      case weapon.WeaponStatus of
//        wsAvailable   : status := 'Available';
//        wsUnavailable : status := 'Unavailable';
//        wsDamaged     : status := 'Damage';
//        wsTooHigh     : status := 'Too High';
//      end;
//
//      // GUN
//      if weapon is TT3MountedGun then
//      begin
//        WeaponText := weapon.InstanceName + ' : ' + status;
//        tnParent := tvWeapons.Items.AddObject(nil, WeaponText, TT3MountedGun(weapon));
//        tvWeapons.Items.AddChildObject(tnParent, IntToStr(weapon.Quantity), weapon);
//      end
//      // MISSILE
//      else if weapon is TT3MountedMissile then
//      begin
//        WeaponText := weapon.InstanceName + ' : ' + status + ' : ' + IntToStr(weapon.Quantity);
//        tnParent := tvWeapons.Items.AddObject(nil, WeaponText, weapon);
//
//        if Assigned(TT3MountedMissile(weapon).MissileDefinition) then
//        begin
//          for j := 0 to TT3MountedMissile(weapon).MissileDefinition.FLaunchs.Count - 1 do
//          begin
//            WeaponLauncher := TFitted_Weap_Launcher_On_Board(
//              TT3MountedMissile(weapon).MissileDefinition.FLaunchs.Items[j]);
//
//            if not(Assigned(WeaponLauncher)) then Continue;
//
//            if WeaponLauncher.FData.Launcher_Type > 8 then
//              launcherNumber :=  WeaponLauncher.FData.Launcher_Type - 8
//            else
//              launcherNumber := WeaponLauncher.FData.Launcher_Type;
//
//            LauncherText := 'Launcher ' + IntToStr(launcherNumber) + ' : ' + IntToStr(WeaponLauncher.FData.Launcher_Qty);
//
//            tvWeapons.Items.AddChildObject(tnParent, LauncherText, WeaponLauncher);
//          end;
//        end;
//      end
//      // TORPEDO
//      else if weapon is TT3MountedTorpedo then
//      begin
//        WeaponText := weapon.InstanceName + ' : ' + status + ' : ' + IntToStr(weapon.Quantity);
//        tnParent := tvWeapons.Items.AddObject(nil, WeaponText, weapon);
//
//        if Assigned(TT3MountedTorpedo(weapon).TorpedoDefinition) then
//        begin
//          for j := 0 to TT3MountedTorpedo(weapon).TorpedoDefinition.FLaunchs.Count - 1 do
//          begin
//            WeaponLauncher := TFitted_Weap_Launcher_On_Board(
//              TT3MountedTorpedo(weapon).TorpedoDefinition.FLaunchs.Items[j]);
//
//            if not(Assigned(WeaponLauncher)) then Continue;
//
//            if WeaponLauncher.FData.Launcher_Type > 8 then
//              launcherNumber :=  WeaponLauncher.FData.Launcher_Type - 8
//            else
//              launcherNumber := WeaponLauncher.FData.Launcher_Type;
//
//            LauncherText := 'Tube ' + IntToStr(launcherNumber) + ' : ' + IntToStr(WeaponLauncher.FData.Launcher_Qty);
//
//            tvWeapons.Items.AddChildObject(tnParent, LauncherText, WeaponLauncher);
//          end;
//        end;
//      end
//      // BOMB
//      else if weapon is TT3MountedBomb then
//      begin
//        WeaponText := weapon.InstanceName + ' : ' + status;
//        tnParent := tvWeapons.Items.AddObject(nil, WeaponText, weapon);
//        tvWeapons.Items.AddChildObject(tnParent, IntToStr(weapon.Quantity), weapon);
//      end
//      // MINE
//      else if weapon is TT3MountedMine then
//      begin
//        WeaponText := weapon.InstanceName + ' : ' + status;
//        tnParent := tvWeapons.Items.AddObject(nil, WeaponText, weapon);
//        tvWeapons.Items.AddChildObject(tnParent, IntToStr(weapon.Quantity), weapon);
//      end
//      //Hybrid Missile
//      else if weapon is TT3MountedHybrid then
//      begin
//        WeaponText := weapon.InstanceName + ' : ' + status;
//        tnParent := tvWeapons.Items.AddObject(nil, WeaponText, weapon);
//        tvWeapons.Items.AddChildObject(tnParent, IntToStr(weapon.Quantity), weapon);
//      end;
//    end;
//  end;

//  tvWeapons.FullExpand;
end;

end.
