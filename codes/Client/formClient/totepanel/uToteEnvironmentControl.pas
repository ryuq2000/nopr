unit uToteEnvironmentControl;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, StdCtrls, Buttons, ExtCtrls, uTotePanel;

type
  TfrmEnvironmentControl = class(TfrmTotePanel)
    gbEnvironmentControl: TPanel;
    Panel16: TPanel;
    Panel23: TPanel;
    Panel55: TPanel;
    Panel56: TPanel;
    Button40: TButton;
    Button41: TButton;
    Panel92: TPanel;
    pcEnvironmentControl: TPageControl;
    tsAboveWater: TTabSheet;
    GroupBox6: TGroupBox;
    Label167: TLabel;
    Label168: TLabel;
    Label169: TLabel;
    Label170: TLabel;
    Label171: TLabel;
    lblEnviCtrlSunrise: TLabel;
    Label173: TLabel;
    lblEnviCtrlSunset: TLabel;
    Label175: TLabel;
    lblEnviCtrlPeriodTwilight: TLabel;
    edtDayVis: TEdit;
    edtNightVis: TEdit;
    trbDaytimeVisual: TTrackBar;
    trbNighttimeVisual: TTrackBar;
    trbDaytimeInfra: TTrackBar;
    trbNighttimeInfra: TTrackBar;
    edtNightInfra: TEdit;
    edtDayInfra: TEdit;
    GroupBox7: TGroupBox;
    Label177: TLabel;
    Label178: TLabel;
    Label179: TLabel;
    Label180: TLabel;
    Label181: TLabel;
    Label182: TLabel;
    Label183: TLabel;
    Label184: TLabel;
    edtAttenRainRate: TEdit;
    edtAttenCloud: TEdit;
    trbAttenRainRate: TTrackBar;
    trbAttenCloud: TTrackBar;
    edtAirTemp: TEdit;
    edtBarometricPressure: TEdit;
    edtCloudBaseHeight: TEdit;
    GroupBox8: TGroupBox;
    Label185: TLabel;
    Label186: TLabel;
    Label187: TLabel;
    Label189: TLabel;
    Label190: TLabel;
    Label192: TLabel;
    edtAtmRefract: TEdit;
    trbAtmRefract: TTrackBar;
    GroupBox5: TGroupBox;
    Label163: TLabel;
    Label164: TLabel;
    Label165: TLabel;
    Label166: TLabel;
    edtWindDir: TEdit;
    edtWindSpeed: TEdit;
    pnWheelAbove: TPanel;
    tsSurface: TTabSheet;
    Label194: TLabel;
    Label195: TLabel;
    Label188: TLabel;
    Label212: TLabel;
    Label213: TLabel;
    Label214: TLabel;
    Label215: TLabel;
    Label216: TLabel;
    Label217: TLabel;
    Label218: TLabel;
    Label219: TLabel;
    Label220: TLabel;
    Label221: TLabel;
    Label222: TLabel;
    Label223: TLabel;
    GroupBox11: TGroupBox;
    Label158: TLabel;
    Label191: TLabel;
    Label193: TLabel;
    Label196: TLabel;
    edtOceanCurrentDirection: TEdit;
    edtOceanCurrentSpeed: TEdit;
    pnlWheelSurface: TPanel;
    cbxShippingRate: TComboBox;
    edtDepthTermalLayer: TEdit;
    GroupBox12: TGroupBox;
    Label197: TLabel;
    Label198: TLabel;
    Label199: TLabel;
    Label200: TLabel;
    Label201: TLabel;
    Label202: TLabel;
    edtSpeedOfSoundSurface: TEdit;
    edtSpeedOfSoundlayer: TEdit;
    edtSpeedOfSoundBottom: TEdit;
    GroupBox2: TGroupBox;
    Label203: TLabel;
    SpeedButton25: TSpeedButton;
    SpeedButton24: TSpeedButton;
    SpeedButton26: TSpeedButton;
    SpeedButton27: TSpeedButton;
    GroupBox13: TGroupBox;
    Label204: TLabel;
    Label205: TLabel;
    Label206: TLabel;
    Label207: TLabel;
    edtSurfaceDuctLow: TEdit;
    edtSurfaceDuctUp: TEdit;
    GroupBox14: TGroupBox;
    Label208: TLabel;
    Label209: TLabel;
    Label210: TLabel;
    Label211: TLabel;
    edtSubSurfaceDuctLow: TEdit;
    edtSubSurfaceDuctUp: TEdit;
    trbSeaState: TTrackBar;
    edtSeaState: TEdit;
    edtBottomLost: TEdit;
    trbBottomLost: TTrackBar;
    edtShadowZone: TEdit;
    edtAvgOceanDepth: TEdit;
    edtSurfaceTemperatur: TEdit;
    cbxConvergenceZone: TCheckBox;
    tsBoundary: TTabSheet;
    GroupBox9: TGroupBox;
    GroupBox10: TGroupBox;
    Panel22: TPanel;
    Panel54: TPanel;
    lvEnviroArea: TListView;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmEnvironmentControl: TfrmEnvironmentControl;

implementation

{$R *.dfm}

end.
