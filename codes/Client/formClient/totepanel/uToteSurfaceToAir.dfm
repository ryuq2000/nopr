object frmSurfaceToAir: TfrmSurfaceToAir
  Left = 0
  Top = 0
  Caption = 'frmSurfaceToAir'
  ClientHeight = 772
  ClientWidth = 1029
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object gbSurfaceToAir: TPanel
    Left = 0
    Top = 0
    Width = 1029
    Height = 772
    Align = alClient
    TabOrder = 0
    ExplicitLeft = -340
    ExplicitTop = -3217
    ExplicitWidth = 975
    ExplicitHeight = 3517
    object Panel4: TPanel
      Left = 1
      Top = 1
      Width = 1027
      Height = 38
      Align = alTop
      Alignment = taLeftJustify
      BorderWidth = 4
      Caption = 'Surface-to-Air Missile Engagements'
      Color = clBtnShadow
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -24
      Font.Name = 'Arial'
      Font.Style = []
      ParentBackground = False
      ParentFont = False
      TabOrder = 0
      ExplicitWidth = 973
    end
    object Panel27: TPanel
      Left = 1
      Top = 39
      Width = 1027
      Height = 472
      Align = alTop
      Anchors = [akLeft, akTop, akRight, akBottom]
      BevelOuter = bvNone
      BorderWidth = 4
      TabOrder = 1
      ExplicitWidth = 973
      ExplicitHeight = 2938
      object Panel89: TPanel
        Left = 4
        Top = 391
        Width = 1019
        Height = 77
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 0
        ExplicitTop = 4
        ExplicitWidth = 1238
        DesignSize = (
          1019
          77)
        object btAbort: TButton
          Left = 873
          Top = 0
          Width = 140
          Height = 50
          Anchors = [akTop, akRight]
          Caption = 'Abort'
          TabOrder = 0
          ExplicitLeft = 830
        end
      end
      object sgSurfacetoAir: TStringGrid
        Left = 4
        Top = 4
        Width = 1019
        Height = 387
        Align = alClient
        ColCount = 8
        DefaultColWidth = 150
        DrawingStyle = gdsGradient
        FixedCols = 0
        RowCount = 20
        TabOrder = 1
        ExplicitWidth = 965
        ExplicitHeight = 523
      end
    end
  end
end
