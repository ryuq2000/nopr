unit uToteAudioTrack;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, Grids, StdCtrls, ExtCtrls, uTotePanel;

type
  TfrmAudioRecordTracks = class(TfrmTotePanel)
    gbAudioRecordTracks: TPanel;
    Panel14: TPanel;
    Panel41: TPanel;
    Button32: TButton;
    Button33: TButton;
    Button34: TButton;
    Button35: TButton;
    Panel42: TPanel;
    Panel69: TPanel;
    Label149: TLabel;
    Edit7: TEdit;
    Panel70: TPanel;
    Label151: TLabel;
    Panel71: TPanel;
    sgExChannel: TStringGrid;
    lvRecordOut: TListView;
    Panel72: TPanel;
    Label152: TLabel;
    Panel73: TPanel;
    sgInChannel: TStringGrid;
    Panel40: TPanel;
    Panel65: TPanel;
    Label150: TLabel;
    Panel66: TPanel;
    PageControl2: TPageControl;
    TabSheet6: TTabSheet;
    Panel67: TPanel;
    sgAvailableExChannel: TStringGrid;
    lvRecordIn: TListView;
    TabSheet7: TTabSheet;
    Panel68: TPanel;
    sgAvailableInChannel: TStringGrid;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmAudioRecordTracks: TfrmAudioRecordTracks;

implementation

{$R *.dfm}

end.
