unit uToteComChannelDefinition;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Grids, ExtCtrls, uTotePanel;

type
  TfrmComChannelDefinition = class(TfrmTotePanel)
    gbCommunicationsChannelDefinition: TPanel;
    Panel24: TPanel;
    sgCommChannelDef: TStringGrid;
    cbbBand: TComboBox;
    Panel9: TPanel;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmComChannelDefinition: TfrmComChannelDefinition;

implementation

{$R *.dfm}

end.
