unit uToteSensorOverride;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls, ComCtrls, uTotePanel;

type
  TfrmSensorOverride = class(TfrmTotePanel)
    gbSensorOverride: TPanel;
    Panel17: TPanel;
    Panel33: TPanel;
    lvSensorOverride: TListView;
    Panel18: TPanel;
    Panel19: TPanel;
    Bevel12: TBevel;
    Bevel13: TBevel;
    Label224: TLabel;
    Label225: TLabel;
    Label226: TLabel;
    SpeedButton28: TSpeedButton;
    btnInhibitAllESM: TButton;
    btnInhibitSonar: TButton;
    btnSelectControlledPlatform: TButton;
    btnSelectHookedPlatform: TButton;
    cbInhibitESMbearing: TCheckBox;
    cbInhibitSonarBearing: TCheckBox;
    edtDetectionStatus: TEdit;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmSensorOverride: TfrmSensorOverride;

implementation

{$R *.dfm}

end.
