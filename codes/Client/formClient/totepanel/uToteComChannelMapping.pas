unit uToteComChannelMapping;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Buttons, StdCtrls, ComCtrls, Grids, ExtCtrls,uTotePanel;

type
  TfrmComChannelMapping = class(TfrmTotePanel)
    gbCommunicationsChannelMapping: TPanel;
    Panel10: TPanel;
    Panel35: TPanel;
    Panel57: TPanel;
    sgAvailableChannel: TStringGrid;
    LbIn: TListBox;
    LbRoomIn: TListBox;
    lvIn: TListView;
    Panel58: TPanel;
    Label129: TLabel;
    SpeedButton13: TSpeedButton;
    edtGroupName: TEdit;
    Panel59: TPanel;
    Label130: TLabel;
    Panel36: TPanel;
    Button25: TButton;
    Button26: TButton;
    Panel37: TPanel;
    Panel60: TPanel;
    Label131: TLabel;
    Panel62: TPanel;
    sgMappedChannel: TStringGrid;
    LbOut: TListBox;
    LbRoomOut: TListBox;
    lvOut: TListView;
    Panel61: TPanel;
    Label132: TLabel;
    SpeedButton14: TSpeedButton;
    SpeedButton29: TSpeedButton;
    SpeedButton17: TSpeedButton;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmComChannelMapping: TfrmComChannelMapping;

implementation

{$R *.dfm}

end.
