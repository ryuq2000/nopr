object frmSurfaceToSurface: TfrmSurfaceToSurface
  Left = 0
  Top = 0
  Caption = 'frmSurfaceToSurface'
  ClientHeight = 644
  ClientWidth = 1239
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesigned
  PixelsPerInch = 96
  TextHeight = 13
  object gbSurfaceToSurface: TPanel
    Left = 0
    Top = 0
    Width = 1239
    Height = 644
    Align = alClient
    TabOrder = 0
    object Panel7: TPanel
      Left = 1
      Top = 1
      Width = 1237
      Height = 38
      Align = alTop
      Alignment = taLeftJustify
      BorderWidth = 4
      Caption = 'Surface-to-Surface Missile Engagements'
      Color = clBtnShadow
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -24
      Font.Name = 'Arial'
      Font.Style = []
      ParentBackground = False
      ParentFont = False
      TabOrder = 0
    end
    object Panel26: TPanel
      Left = 1
      Top = 39
      Width = 1237
      Height = 604
      Align = alClient
      BevelOuter = bvNone
      BorderWidth = 4
      TabOrder = 1
      object Panel87: TPanel
        Left = 4
        Top = 536
        Width = 1229
        Height = 64
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 0
        DesignSize = (
          1229
          64)
        object btnAbortSurfaceToSurface: TButton
          AlignWithMargins = True
          Left = 898
          Top = 6
          Width = 140
          Height = 50
          Margins.Right = 10
          Anchors = [akTop, akRight]
          Caption = 'Abort'
          Enabled = False
          TabOrder = 0
        end
        object btnLaunch: TButton
          Left = 1051
          Top = 6
          Width = 137
          Height = 49
          Anchors = [akTop, akRight]
          Caption = 'Launch'
          Enabled = False
          TabOrder = 1
        end
      end
      object sgSurfacetoSurface: TStringGrid
        Left = 4
        Top = 4
        Width = 1229
        Height = 532
        Align = alClient
        ColCount = 8
        DefaultColWidth = 150
        DrawingStyle = gdsGradient
        FixedCols = 0
        RowCount = 20
        Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goRowSelect]
        TabOrder = 1
      end
    end
  end
end
