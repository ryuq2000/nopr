object frmSensorOverride: TfrmSensorOverride
  Left = 0
  Top = 0
  Caption = 'frmSensorOverride'
  ClientHeight = 548
  ClientWidth = 1005
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object gbSensorOverride: TPanel
    Left = 0
    Top = 0
    Width = 1005
    Height = 548
    Align = alClient
    TabOrder = 0
    ExplicitLeft = -340
    ExplicitTop = -3217
    ExplicitWidth = 975
    ExplicitHeight = 3517
    object Panel17: TPanel
      Left = 1
      Top = 1
      Width = 1003
      Height = 35
      Align = alTop
      Alignment = taLeftJustify
      BorderWidth = 4
      Caption = 'Sensor Override / Error Terms'
      Color = clBtnShadow
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -24
      Font.Name = 'Arial'
      Font.Style = []
      ParentBackground = False
      ParentFont = False
      TabOrder = 0
      ExplicitWidth = 973
    end
    object Panel33: TPanel
      Left = 1
      Top = 36
      Width = 613
      Height = 511
      Align = alLeft
      BevelOuter = bvNone
      BorderWidth = 4
      TabOrder = 1
      ExplicitHeight = 3480
      object lvSensorOverride: TListView
        Left = 4
        Top = 4
        Width = 605
        Height = 503
        Align = alClient
        Columns = <
          item
            Caption = ' '
            Width = 30
          end
          item
            Caption = 'Class'
            Width = 100
          end
          item
            Caption = 'Name'
            Width = 100
          end
          item
            Caption = 'Track'
          end
          item
            AutoSize = True
            Caption = 'Force'
          end>
        HideSelection = False
        RowSelect = True
        TabOrder = 0
        ViewStyle = vsReport
        ExplicitHeight = 3472
      end
    end
    object Panel18: TPanel
      Left = 614
      Top = 36
      Width = 390
      Height = 511
      Align = alClient
      BevelOuter = bvNone
      BorderWidth = 4
      TabOrder = 2
      ExplicitWidth = 360
      ExplicitHeight = 3480
      object Panel19: TPanel
        Left = 4
        Top = 4
        Width = 382
        Height = 503
        Align = alClient
        TabOrder = 0
        ExplicitWidth = 352
        ExplicitHeight = 3472
        DesignSize = (
          382
          503)
        object Bevel12: TBevel
          Left = 108
          Top = 30
          Width = 270
          Height = 7
          Anchors = [akLeft, akTop, akRight]
          Shape = bsBottomLine
          ExplicitWidth = 240
        end
        object Bevel13: TBevel
          Left = 83
          Top = 167
          Width = 295
          Height = 7
          Anchors = [akLeft, akTop, akRight]
          Shape = bsBottomLine
          ExplicitWidth = 265
        end
        object Label224: TLabel
          Left = 18
          Top = 28
          Width = 84
          Height = 13
          Caption = 'Selected Platform'
        end
        object Label225: TLabel
          Left = 18
          Top = 60
          Width = 83
          Height = 13
          Caption = 'Detection status:'
        end
        object Label226: TLabel
          Left = 18
          Top = 165
          Width = 59
          Height = 13
          Caption = 'All platforms'
        end
        object SpeedButton28: TSpeedButton
          Left = 318
          Top = 55
          Width = 23
          Height = 22
          Glyph.Data = {
            D6050000424DD605000000000000360000002800000017000000140000000100
            180000000000A005000000000000000000000000000000000000B2B2B2B2B2B2
            B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
            B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B200
            0000B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
            B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
            B2B2B2B2B2B2B2000000B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
            B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
            B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000B2B2B2B2B2B2B2B2B2B2B2B2B2B2
            B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
            B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000B2B2B2B2B2B2
            B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
            B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B200
            0000B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2C1C1C1B2B2B2B2B2B2
            B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
            B2B2B2B2B2B2B2000000B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B200000000
            0000B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
            B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000B2B2B2B2B2B2B2B2B2B2B2B2B2B2
            B2B2B2B2000000000000000000000000C1C1C1B2B2B2B2B2B2B2B2B2B2B2B2B2
            B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000B2B2B2B2B2B2
            B2B2B2B2B2B2B2B2B2B2B2B2000000000000000000000000000000000000B2B2
            B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B200
            0000B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000000000000000000000
            000000000000000000000000C1C1C1B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
            B2B2B2B2B2B2B2000000B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B200000000
            0000000000000000000000000000000000000000000000000000B2B2B2B2B2B2
            B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000B2B2B2B2B2B2B2B2B2B2B2B2B2B2
            B2B2B2B2000000000000000000000000000000000000000000000000B2B2B2B2
            B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000B2B2B2B2B2B2
            B2B2B2B2B2B2B2B2B2B2B2B2000000000000000000000000000000000000B2B2
            B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B200
            0000B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000000000000000000000
            B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
            B2B2B2B2B2B2B2000000B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B200000000
            0000B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
            B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000B2B2B2B2B2B2B2B2B2B2B2B2B2B2
            B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
            B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000B2B2B2B2B2B2
            B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
            B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B200
            0000B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
            B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
            B2B2B2B2B2B2B2000000B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
            B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
            B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000B2B2B2B2B2B2B2B2B2B2B2B2B2B2
            B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
            B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000}
        end
        object btnInhibitAllESM: TButton
          Left = 56
          Top = 203
          Width = 243
          Height = 33
          Caption = 'Inhibit All ESM Bearing Errors'
          TabOrder = 0
        end
        object btnInhibitSonar: TButton
          Left = 56
          Top = 242
          Width = 243
          Height = 33
          Caption = 'Inhibit All Sonar Bearing Errors'
          TabOrder = 1
        end
        object btnSelectControlledPlatform: TButton
          Left = 18
          Top = 381
          Width = 213
          Height = 33
          Caption = 'Select Controlled Platform'
          TabOrder = 2
        end
        object btnSelectHookedPlatform: TButton
          Left = 18
          Top = 336
          Width = 213
          Height = 33
          Caption = 'Select Hooked Platform'
          TabOrder = 3
        end
        object cbInhibitESMbearing: TCheckBox
          Left = 18
          Top = 99
          Width = 253
          Height = 17
          Caption = 'Inhibit ESM bearing errors'
          TabOrder = 4
        end
        object cbInhibitSonarBearing: TCheckBox
          Left = 18
          Top = 125
          Width = 202
          Height = 17
          Caption = 'Inhibit sonar bearing errors'
          Color = clBtnFace
          ParentColor = False
          TabOrder = 5
        end
        object edtDetectionStatus: TEdit
          Left = 107
          Top = 56
          Width = 205
          Height = 21
          ReadOnly = True
          TabOrder = 6
        end
      end
    end
  end
end
