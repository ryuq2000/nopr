object frmDataLinkStatus: TfrmDataLinkStatus
  Left = 0
  Top = 0
  Caption = 'frmDataLinkStatus'
  ClientHeight = 481
  ClientWidth = 805
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object gbDataLinkStatus: TPanel
    Left = 0
    Top = 0
    Width = 805
    Height = 481
    Align = alClient
    TabOrder = 0
    ExplicitLeft = -340
    ExplicitTop = -3217
    ExplicitWidth = 975
    ExplicitHeight = 3517
    object pnlDS2: TPanel
      Left = 1
      Top = 1
      Width = 803
      Height = 38
      Align = alTop
      Alignment = taLeftJustify
      BorderWidth = 4
      Caption = 'Datalink Status'
      Color = clBtnShadow
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -24
      Font.Name = 'Arial'
      Font.Style = []
      ParentBackground = False
      ParentFont = False
      TabOrder = 0
      ExplicitWidth = 973
    end
    object pnlDS8: TPanel
      Left = 1
      Top = 39
      Width = 803
      Height = 441
      Align = alClient
      BevelOuter = bvNone
      BorderWidth = 4
      TabOrder = 1
      ExplicitWidth = 973
      ExplicitHeight = 3477
      object pnlDS9: TPanel
        Left = 4
        Top = 397
        Width = 795
        Height = 40
        Align = alBottom
        BevelOuter = bvNone
        BorderWidth = 30
        TabOrder = 0
        ExplicitTop = 3433
        ExplicitWidth = 965
      end
      object lvDS2: TListView
        Left = 4
        Top = 104
        Width = 795
        Height = 293
        Align = alClient
        Anchors = [akLeft, akTop, akBottom]
        Columns = <
          item
            Caption = 'PU Number'
            MaxWidth = 100
            MinWidth = 50
            Width = 76
          end
          item
            Caption = 'Name'
            MaxWidth = 200
            MinWidth = 200
            Width = 200
          end
          item
            Caption = 'Status'
            MaxWidth = 200
            MinWidth = 100
            Width = 125
          end>
        MultiSelect = True
        RowSelect = True
        TabOrder = 2
        ViewStyle = vsReport
        ExplicitWidth = 965
        ExplicitHeight = 3329
      end
      object pnlDS10: TPanel
        Left = 4
        Top = 4
        Width = 795
        Height = 100
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 1
        ExplicitWidth = 965
        object lblDS5: TLabel
          Left = 5
          Top = 79
          Width = 89
          Height = 13
          Caption = 'Participating units:'
        end
        object lblDSStatus: TLabel
          Left = 12
          Top = 20
          Width = 35
          Height = 13
          Caption = 'Status:'
        end
        object lbDSBand: TLabel
          Left = 12
          Top = 44
          Width = 28
          Height = 13
          Caption = 'Band:'
        end
        object lbDSStatusValue: TLabel
          Left = 66
          Top = 20
          Width = 12
          Height = 13
          Caption = '---'
        end
        object lbDSBandValue: TLabel
          Left = 66
          Top = 44
          Width = 12
          Height = 13
          Caption = '---'
        end
      end
    end
  end
end
