object frmWeaponEngagementsSUmmary: TfrmWeaponEngagementsSUmmary
  Left = 0
  Top = 0
  Caption = 'frmWeaponEngagementsSUmmary'
  ClientHeight = 796
  ClientWidth = 1163
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object gbWeaponEngagementsSUmmary: TPanel
    Left = 0
    Top = 0
    Width = 1163
    Height = 796
    Align = alClient
    TabOrder = 0
    ExplicitTop = -2721
    ExplicitWidth = 975
    ExplicitHeight = 3517
    object Panel5: TPanel
      Left = 1
      Top = 1
      Width = 1161
      Height = 38
      Align = alTop
      Alignment = taLeftJustify
      Caption = ' Weapon Engagements Summary'
      Color = clBtnShadow
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -24
      Font.Name = 'Arial'
      Font.Style = []
      ParentBackground = False
      ParentFont = False
      TabOrder = 0
      ExplicitWidth = 973
    end
    object Panel1: TPanel
      Left = 1
      Top = 39
      Width = 1161
      Height = 756
      Align = alClient
      BevelOuter = bvNone
      BorderWidth = 4
      TabOrder = 1
      ExplicitWidth = 973
      ExplicitHeight = 3477
      object lvWeaponEngagement: TListView
        Left = 4
        Top = 4
        Width = 1153
        Height = 748
        Align = alClient
        Columns = <
          item
            Caption = 'Time'
            Width = 150
          end
          item
            Caption = 'Launching Platform'
            Width = 150
          end
          item
            Caption = 'Weapon Class'
            Width = 100
          end
          item
            Caption = 'Target Platform'
            Width = 80
          end
          item
            Caption = 'Engagement '
          end>
        RowSelect = True
        TabOrder = 0
        ViewStyle = vsReport
        ExplicitWidth = 965
        ExplicitHeight = 3469
      end
    end
  end
end
