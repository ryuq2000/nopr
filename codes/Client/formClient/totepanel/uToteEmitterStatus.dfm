object frmEmitterStatus: TfrmEmitterStatus
  Left = 0
  Top = 0
  Caption = 'frmEmitterStatus'
  ClientHeight = 502
  ClientWidth = 740
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object gbEmitterStatus: TPanel
    Left = 0
    Top = 0
    Width = 740
    Height = 502
    Align = alClient
    TabOrder = 0
    ExplicitLeft = -340
    ExplicitTop = -3217
    ExplicitWidth = 975
    ExplicitHeight = 3517
    object Panel98: TPanel
      Left = 1
      Top = 1
      Width = 738
      Height = 38
      Align = alTop
      Alignment = taLeftJustify
      BorderWidth = 4
      Caption = 'Emitter Status'
      Color = clBtnShadow
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -24
      Font.Name = 'Arial'
      Font.Style = []
      ParentBackground = False
      ParentFont = False
      TabOrder = 0
      ExplicitWidth = 973
    end
    object Panel99: TPanel
      Left = 1
      Top = 39
      Width = 738
      Height = 462
      Align = alClient
      BevelOuter = bvNone
      BorderWidth = 4
      TabOrder = 1
      ExplicitWidth = 973
      ExplicitHeight = 3477
      object sgEmitters: TStringGrid
        Left = 4
        Top = 130
        Width = 730
        Height = 328
        Align = alClient
        ColCount = 7
        DrawingStyle = gdsGradient
        FixedCols = 0
        RowCount = 100
        TabOrder = 0
        ExplicitWidth = 965
        ExplicitHeight = 3343
        ColWidths = (
          116
          98
          103
          116
          112
          135
          360)
      end
      object Panel100: TPanel
        Left = 4
        Top = 4
        Width = 730
        Height = 126
        Align = alTop
        TabOrder = 1
        ExplicitWidth = 965
        object Label9: TLabel
          Left = 10
          Top = 14
          Width = 33
          Height = 13
          Caption = 'Track :'
        end
        object Label10: TLabel
          Left = 11
          Top = 37
          Width = 34
          Height = 13
          Caption = 'Name :'
        end
        object Label11: TLabel
          Left = 11
          Top = 60
          Width = 43
          Height = 13
          Caption = 'Bearing :'
        end
        object lbTrack: TLabel
          Left = 66
          Top = 15
          Width = 33
          Height = 13
          Caption = 'Track :'
        end
        object lbName: TLabel
          Left = 66
          Top = 37
          Width = 33
          Height = 13
          Caption = 'Track :'
        end
        object lbBearing: TLabel
          Left = 66
          Top = 60
          Width = 33
          Height = 13
          Caption = 'Track :'
        end
        object Label15: TLabel
          Left = 126
          Top = 60
          Width = 48
          Height = 13
          Caption = 'degrees T'
        end
        object Label16: TLabel
          Left = 1
          Top = 112
          Width = 46
          Height = 13
          Align = alBottom
          Anchors = [akRight, akBottom]
          Caption = 'Emitters :'
        end
      end
    end
  end
end
