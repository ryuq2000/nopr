unit uToteSurfaceToSurface;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, StdCtrls, ExtCtrls, uTotePanel;

type
  TfrmSurfaceToSurface = class(TfrmTotePanel)
    gbSurfaceToSurface: TPanel;
    Panel7: TPanel;
    Panel26: TPanel;
    Panel87: TPanel;
    btnAbortSurfaceToSurface: TButton;
    btnLaunch: TButton;
    sgSurfacetoSurface: TStringGrid;
  private
    { Private declarations }
  public
    { Public declarations }
    procedure UpdateForm;override;
  end;

var
  frmSurfaceToSurface: TfrmSurfaceToSurface;

implementation

{$R *.dfm}

{ TfrmSurfaceToSurface }

procedure TfrmSurfaceToSurface.UpdateForm;
begin
  inherited;

end;

end.
