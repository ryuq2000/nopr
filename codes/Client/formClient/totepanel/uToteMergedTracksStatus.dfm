object frmMergedTracksStatus: TfrmMergedTracksStatus
  Left = 0
  Top = 0
  Caption = 'frmMergedTracksStatus'
  ClientHeight = 662
  ClientWidth = 977
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object gbMergedTracksStatus: TPanel
    Left = 0
    Top = 0
    Width = 977
    Height = 662
    Align = alClient
    TabOrder = 0
    ExplicitLeft = -340
    ExplicitTop = -3217
    ExplicitWidth = 975
    ExplicitHeight = 3517
    object Panel46: TPanel
      Left = 1
      Top = 1
      Width = 975
      Height = 38
      Align = alTop
      Alignment = taLeftJustify
      BorderWidth = 4
      Caption = 'Merged Track Status'
      Color = clBtnShadow
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -24
      Font.Name = 'Arial'
      Font.Style = []
      ParentBackground = False
      ParentFont = False
      TabOrder = 0
      ExplicitWidth = 973
    end
    object Panel47: TPanel
      Left = 1
      Top = 39
      Width = 438
      Height = 622
      Align = alLeft
      BevelOuter = bvNone
      BorderWidth = 4
      TabOrder = 1
      ExplicitHeight = 3477
      object Panel48: TPanel
        Left = 4
        Top = 27
        Width = 430
        Height = 591
        Align = alClient
        BevelOuter = bvNone
        BorderWidth = 10
        Caption = 'Panel57'
        TabOrder = 0
        ExplicitHeight = 3446
        object lvMergedTrack: TListView
          Left = 10
          Top = 10
          Width = 410
          Height = 571
          Align = alClient
          Columns = <
            item
              Caption = 'Track'
              MaxWidth = 75
              MinWidth = 75
              Width = 75
            end
            item
              Caption = 'Domain'
              MaxWidth = 160
              MinWidth = 160
              Width = 160
            end
            item
              Caption = 'Identity'
              MaxWidth = 160
              MinWidth = 160
              Width = 160
            end>
          TabOrder = 0
          ViewStyle = vsReport
          ExplicitHeight = 3426
        end
      end
      object Panel50: TPanel
        Left = 4
        Top = 4
        Width = 430
        Height = 23
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 1
        object Label3: TLabel
          Left = 13
          Top = 6
          Width = 74
          Height = 13
          Caption = 'Merged Tracks:'
        end
      end
    end
    object Panel51: TPanel
      Left = 439
      Top = 39
      Width = 90
      Height = 622
      Align = alLeft
      BevelOuter = bvNone
      BorderWidth = 4
      TabOrder = 2
      ExplicitHeight = 3477
      object btAdd: TButton
        Left = 6
        Top = 124
        Width = 75
        Height = 25
        Caption = 'Add >'
        TabOrder = 0
      end
      object btRemove: TButton
        Left = 6
        Top = 156
        Width = 75
        Height = 25
        Caption = '< Remove'
        TabOrder = 1
      end
    end
    object Panel52: TPanel
      Left = 529
      Top = 39
      Width = 440
      Height = 622
      Align = alLeft
      BevelOuter = bvNone
      BorderWidth = 4
      TabOrder = 3
      ExplicitHeight = 3477
      object Panel86: TPanel
        Left = 4
        Top = 4
        Width = 432
        Height = 28
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object Label4: TLabel
          Left = 10
          Top = 8
          Width = 127
          Height = 13
          Caption = 'Merged Track Component:'
        end
      end
      object Panel88: TPanel
        Left = 4
        Top = 32
        Width = 432
        Height = 544
        Align = alTop
        Anchors = [akTop]
        BevelOuter = bvNone
        BorderWidth = 10
        TabOrder = 1
        object lvMergedTrackComponent: TListView
          Left = 10
          Top = 10
          Width = 412
          Height = 524
          Align = alClient
          Columns = <
            item
              Caption = 'Track'
              MaxWidth = 75
              MinWidth = 75
              Width = 75
            end
            item
              Caption = 'Type'
              MaxWidth = 140
              MinWidth = 140
              Width = 140
            end
            item
              Caption = 'Range [nm]'
              MaxWidth = 75
              MinWidth = 75
              Width = 75
            end
            item
              Caption = 'Bearing [degrees T]'
              MaxWidth = 125
              MinWidth = 125
              Width = 125
            end>
          TabOrder = 0
          ViewStyle = vsReport
        end
      end
      object Panel91: TPanel
        Left = 4
        Top = 576
        Width = 432
        Height = 41
        Align = alTop
        BevelOuter = bvNone
        BorderWidth = 10
        TabOrder = 2
        object Button1: TButton
          Left = 312
          Top = 0
          Width = 105
          Height = 25
          Caption = 'Split'
          TabOrder = 0
        end
      end
    end
  end
end
