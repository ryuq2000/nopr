object frmSonarDetectionInformation: TfrmSonarDetectionInformation
  Left = 0
  Top = 0
  Caption = 'frmSonarDetectionInformation'
  ClientHeight = 741
  ClientWidth = 986
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object gbSonarDetectionInformation: TPanel
    Left = 0
    Top = 0
    Width = 986
    Height = 741
    Align = alClient
    TabOrder = 0
    ExplicitLeft = -340
    ExplicitTop = -3217
    ExplicitWidth = 975
    ExplicitHeight = 3517
    object pnlSonarDetectInfoLeft: TPanel
      Left = 1
      Top = 36
      Width = 534
      Height = 704
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 1
      ExplicitWidth = 523
      ExplicitHeight = 3480
      object Label17: TLabel
        Left = 6
        Top = 9
        Width = 47
        Height = 13
        Caption = 'Platform :'
      end
      object Label20: TLabel
        Left = 8
        Top = 347
        Width = 39
        Height = 13
        Caption = 'Target :'
      end
      object lvSonarPlatform: TListView
        Left = 3
        Top = 28
        Width = 514
        Height = 312
        Columns = <
          item
            Caption = 'Class'
            Width = 250
          end
          item
            Caption = 'Name'
            Width = 120
          end
          item
            Caption = 'Track'
            Width = 70
          end
          item
            Caption = 'Force'
            Width = 60
          end>
        HideSelection = False
        RowSelect = True
        TabOrder = 0
        ViewStyle = vsReport
      end
      object lvSonarTarget: TListView
        Left = 3
        Top = 366
        Width = 514
        Height = 312
        Columns = <
          item
            Caption = 'Class'
            Width = 250
          end
          item
            Caption = 'Name'
            Width = 120
          end
          item
            Caption = 'Track'
            Width = 70
          end
          item
            Caption = 'Force'
            Width = 60
          end>
        HideSelection = False
        RowSelect = True
        TabOrder = 1
        ViewStyle = vsReport
      end
    end
    object pnlSonarDetectInfoRight: TPanel
      Left = 535
      Top = 36
      Width = 450
      Height = 704
      Align = alRight
      BevelOuter = bvNone
      ParentBackground = False
      TabOrder = 2
      ExplicitLeft = 524
      ExplicitHeight = 3480
      object Label21: TLabel
        Left = 11
        Top = 9
        Width = 35
        Height = 13
        Caption = 'Sonar :'
      end
      object GroupBox1: TGroupBox
        Left = 2
        Top = 201
        Width = 367
        Height = 477
        Caption = 'Detail Information : '
        TabOrder = 0
        object Label22: TLabel
          Left = 20
          Top = 358
          Width = 132
          Height = 13
          Caption = 'Boundary Crossing Count : '
        end
        object Label23: TLabel
          Left = 9
          Top = 377
          Width = 141
          Height = 13
          Caption = 'Sonar Signal-to-Noise Ratio : '
        end
        object Label24: TLabel
          Left = 9
          Top = 396
          Width = 153
          Height = 13
          Caption = 'Sonar Probability of Detection : '
        end
        object Label25: TLabel
          Left = 9
          Top = 415
          Width = 78
          Height = 13
          Caption = 'Random Value : '
        end
        object CheckBox1: TCheckBox
          Left = 5
          Top = 38
          Width = 100
          Height = 17
          Caption = 'Scan Processing'
          Enabled = False
          TabOrder = 0
        end
        object CheckBox2: TCheckBox
          Left = 20
          Top = 61
          Width = 200
          Height = 17
          Caption = 'Target Inside Swept Azimuth Sector'
          Enabled = False
          TabOrder = 1
        end
        object CheckBox3: TCheckBox
          Left = 20
          Top = 84
          Width = 145
          Height = 17
          Caption = 'Target Outside Blind Zone'
          Enabled = False
          TabOrder = 2
        end
        object CheckBox4: TCheckBox
          Left = 5
          Top = 107
          Width = 255
          Height = 17
          Caption = 'Determine Eligible Targets And Maximum Range'
          Enabled = False
          TabOrder = 3
        end
        object CheckBox5: TCheckBox
          Left = 20
          Top = 130
          Width = 160
          Height = 17
          Caption = 'Target Eligible For Detection'
          Enabled = False
          TabOrder = 4
        end
        object CheckBox6: TCheckBox
          Left = 20
          Top = 153
          Width = 225
          Height = 17
          Caption = 'Sonar Operating Mode Valid For Detection'
          Enabled = False
          TabOrder = 5
        end
        object CheckBox7: TCheckBox
          Left = 20
          Top = 176
          Width = 180
          Height = 17
          Caption = 'Target Inside Detection Range'
          Enabled = False
          TabOrder = 6
        end
        object CheckBox8: TCheckBox
          Left = 5
          Top = 199
          Width = 115
          Height = 17
          Caption = 'Physical Processing'
          Enabled = False
          TabOrder = 7
        end
        object CheckBox9: TCheckBox
          Left = 20
          Top = 222
          Width = 160
          Height = 17
          Caption = 'Sonar Operation Depth Valid'
          Enabled = False
          TabOrder = 8
        end
        object CheckBox10: TCheckBox
          Left = 20
          Top = 245
          Width = 150
          Height = 17
          Caption = 'Sonar Deploy Speed Valid'
          Enabled = False
          TabOrder = 9
        end
        object CheckBox11: TCheckBox
          Left = 20
          Top = 268
          Width = 220
          Height = 17
          Caption = 'Towed Array Sonar - Ocean Depth Valid'
          Enabled = False
          TabOrder = 10
        end
        object CheckBox12: TCheckBox
          Left = 20
          Top = 291
          Width = 220
          Height = 17
          Caption = 'Towed Array Sonar - Deploy Speed Valid'
          Enabled = False
          TabOrder = 11
        end
        object CheckBox13: TCheckBox
          Left = 20
          Top = 314
          Width = 200
          Height = 17
          Caption = 'Towed Array Sonar - Cable Unkinked'
          Enabled = False
          TabOrder = 12
        end
        object CheckBox14: TCheckBox
          Left = 5
          Top = 337
          Width = 170
          Height = 17
          Caption = 'Sub Area Boundary Processing'
          Enabled = False
          TabOrder = 13
        end
        object Panel76: TPanel
          Left = 9
          Top = 432
          Width = 349
          Height = 41
          Caption = 'Detected / Not Detected'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -19
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 14
        end
        object CheckBox15: TCheckBox
          Left = 5
          Top = 15
          Width = 215
          Height = 17
          Caption = 'Check Deployment And Operating Mode'
          Enabled = False
          TabOrder = 15
        end
      end
      object lvSonar: TListView
        Left = 2
        Top = 28
        Width = 367
        Height = 167
        Columns = <
          item
            Caption = 'Name'
            Width = 250
          end
          item
            Caption = 'Status'
            Width = 110
          end>
        RowSelect = True
        TabOrder = 1
        ViewStyle = vsReport
      end
    end
    object Panel34: TPanel
      Left = 1
      Top = 1
      Width = 984
      Height = 35
      Align = alTop
      Alignment = taLeftJustify
      BorderWidth = 4
      Caption = 'Sonar Detection Information'
      Color = clBtnShadow
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -24
      Font.Name = 'Arial'
      Font.Style = []
      ParentBackground = False
      ParentFont = False
      TabOrder = 0
      ExplicitWidth = 973
    end
  end
end
