unit uToteComInterference;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls, ExtCtrls, Grids, uTotePanel;

type
  TfrmCommunicationsInterference = class(TfrmTotePanel)
    gbCommunicationsInterference: TPanel;
    Panel11: TPanel;
    Panel32: TPanel;
    PageControl1: TPageControl;
    TabSheet4: TTabSheet;
    Panel53: TPanel;
    sgComInInjection: TStringGrid;
    cbbJam: TComboBox;
    TabSheet5: TTabSheet;
    Label133: TLabel;
    Bevel11: TBevel;
    Label134: TLabel;
    Label135: TLabel;
    Label136: TLabel;
    Label137: TLabel;
    Label138: TLabel;
    Label139: TLabel;
    Label140: TLabel;
    tbNoise: TTrackBar;
    tbJamming: TTrackBar;
    tbMHStones: TTrackBar;
    btnNoiseCg: TButton;
    btnJamCg: TButton;
    btnMHTCg: TButton;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmCommunicationsInterference: TfrmCommunicationsInterference;

implementation

{$R *.dfm}

end.
