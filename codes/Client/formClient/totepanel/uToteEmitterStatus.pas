unit uToteEmitterStatus;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Grids, ExtCtrls, uTotePanel;

type
  TfrmEmitterStatus = class(TfrmTotePanel)
    gbEmitterStatus: TPanel;
    Panel98: TPanel;
    Panel99: TPanel;
    sgEmitters: TStringGrid;
    Panel100: TPanel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    lbTrack: TLabel;
    lbName: TLabel;
    lbBearing: TLabel;
    Label15: TLabel;
    Label16: TLabel;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmEmitterStatus: TfrmEmitterStatus;

implementation

{$R *.dfm}

end.
