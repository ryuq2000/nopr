object frmMessageHandlingSystem: TfrmMessageHandlingSystem
  Left = 0
  Top = 0
  Caption = 'frmMessageHandlingSystem'
  ClientHeight = 751
  ClientWidth = 987
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object gbMessageHandlingSystem: TPanel
    Left = 0
    Top = 0
    Width = 987
    Height = 751
    Align = alClient
    TabOrder = 0
    ExplicitLeft = -19
    ExplicitTop = -2802
    ExplicitWidth = 975
    ExplicitHeight = 3517
    object Panel8: TPanel
      Left = 1
      Top = 1
      Width = 985
      Height = 38
      Align = alTop
      Alignment = taLeftJustify
      BorderWidth = 4
      Caption = 'Message Handling System'
      Color = clBtnShadow
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -24
      Font.Name = 'Arial'
      Font.Style = []
      ParentBackground = False
      ParentFont = False
      TabOrder = 0
      ExplicitWidth = 973
    end
    object Panel25: TPanel
      Left = 1
      Top = 39
      Width = 985
      Height = 711
      Align = alClient
      BorderWidth = 4
      TabOrder = 1
      ExplicitWidth = 973
      ExplicitHeight = 3477
      object pcReceived: TPageControl
        Left = 5
        Top = 5
        Width = 975
        Height = 349
        ActivePage = TabSheet1
        Align = alClient
        TabOrder = 0
        ExplicitWidth = 963
        ExplicitHeight = 3115
        object TabSheet1: TTabSheet
          Caption = 'Received'
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 955
          ExplicitHeight = 3087
          object Panel74: TPanel
            Left = 0
            Top = 0
            Width = 967
            Height = 321
            Align = alClient
            BevelOuter = bvNone
            BorderWidth = 10
            TabOrder = 0
            ExplicitWidth = 955
            ExplicitHeight = 3087
            object lvReceive: TListView
              Left = 10
              Top = 10
              Width = 947
              Height = 301
              Align = alClient
              Columns = <
                item
                  Caption = 'From'
                  Width = 200
                end
                item
                  Caption = 'Message'
                  Width = 200
                end
                item
                  Caption = 'Subject'
                  Width = 200
                end
                item
                  Caption = 'Priority'
                  Width = 100
                end>
              RowSelect = True
              TabOrder = 0
              ViewStyle = vsReport
              ExplicitWidth = 935
              ExplicitHeight = 3067
            end
          end
        end
        object TabSheet2: TTabSheet
          Caption = 'Sent'
          ImageIndex = 1
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object Panel78: TPanel
            Left = 0
            Top = 0
            Width = 955
            Height = 3087
            Align = alClient
            BevelOuter = bvNone
            BorderWidth = 10
            TabOrder = 0
            object lvSent: TListView
              Left = 10
              Top = 10
              Width = 935
              Height = 3067
              Align = alClient
              Columns = <
                item
                  Caption = 'To'
                  Width = 200
                end
                item
                  Caption = 'Message'
                  Width = 200
                end
                item
                  Caption = 'Subject'
                  Width = 200
                end
                item
                  Caption = 'Priority'
                  Width = 100
                end>
              RowSelect = True
              TabOrder = 0
              ViewStyle = vsReport
            end
          end
        end
        object TabSheet3: TTabSheet
          Caption = 'Draft'
          ImageIndex = 2
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object Panel82: TPanel
            Left = 0
            Top = 0
            Width = 955
            Height = 3087
            Align = alClient
            BevelOuter = bvNone
            BorderWidth = 10
            TabOrder = 0
            object lvDraft: TListView
              Left = 10
              Top = 10
              Width = 935
              Height = 3067
              Align = alClient
              Columns = <
                item
                  Caption = 'To'
                  Width = 200
                end
                item
                  Caption = 'Message'
                  Width = 200
                end
                item
                  Caption = 'Subject'
                  Width = 200
                end
                item
                  Caption = 'Priority'
                  Width = 100
                end>
              RowSelect = True
              TabOrder = 0
              ViewStyle = vsReport
            end
          end
        end
      end
      object Panel20: TPanel
        Left = 5
        Top = 354
        Width = 975
        Height = 352
        Align = alBottom
        TabOrder = 1
        ExplicitTop = 3120
        ExplicitWidth = 963
        object Panel49: TPanel
          Left = 1
          Top = 296
          Width = 973
          Height = 55
          Align = alBottom
          TabOrder = 0
          ExplicitWidth = 961
          object btnNew: TButton
            Left = 6
            Top = 14
            Width = 57
            Height = 25
            Caption = 'New'
            TabOrder = 0
          end
          object btnRemove: TButton
            Left = 393
            Top = 14
            Width = 75
            Height = 25
            Caption = 'Remove'
            TabOrder = 1
          end
          object btnReply: TButton
            Left = 231
            Top = 14
            Width = 75
            Height = 25
            Caption = 'Reply'
            TabOrder = 2
          end
          object btnForward: TButton
            Left = 312
            Top = 14
            Width = 75
            Height = 25
            Caption = 'Forward'
            TabOrder = 3
          end
          object btnSend: TButton
            Left = 150
            Top = 14
            Width = 75
            Height = 25
            Caption = 'Send'
            TabOrder = 4
          end
          object btnEdit: TButton
            Left = 69
            Top = 14
            Width = 75
            Height = 25
            Caption = 'Edit'
            TabOrder = 5
          end
          object btnPrint: TButton
            Left = 472
            Top = 14
            Width = 75
            Height = 25
            Caption = 'Print'
            TabOrder = 6
          end
        end
        object Panel75: TPanel
          Left = 1
          Top = 1
          Width = 973
          Height = 295
          Align = alClient
          TabOrder = 1
          ExplicitWidth = 961
          object mmoMessage: TMemo
            Left = 1
            Top = 1
            Width = 971
            Height = 293
            Align = alClient
            Lines.Strings = (
              '')
            TabOrder = 0
            ExplicitWidth = 959
          end
        end
      end
    end
  end
end
