object frmCommunicationsInterference: TfrmCommunicationsInterference
  Left = 0
  Top = 0
  Caption = 'CommunicationsInterference'
  ClientHeight = 731
  ClientWidth = 880
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object gbCommunicationsInterference: TPanel
    Left = 0
    Top = 0
    Width = 880
    Height = 731
    Align = alClient
    TabOrder = 0
    object Panel11: TPanel
      Left = 1
      Top = 1
      Width = 878
      Height = 38
      Align = alTop
      Alignment = taLeftJustify
      BorderWidth = 4
      Caption = 'Communcations Interference'
      Color = clBtnShadow
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -24
      Font.Name = 'Arial'
      Font.Style = []
      ParentBackground = False
      ParentFont = False
      TabOrder = 0
    end
    object Panel32: TPanel
      Left = 1
      Top = 39
      Width = 878
      Height = 691
      Align = alClient
      BevelOuter = bvNone
      BorderWidth = 4
      TabOrder = 1
      object PageControl1: TPageControl
        Left = 4
        Top = 4
        Width = 870
        Height = 683
        ActivePage = TabSheet4
        Align = alClient
        TabOrder = 0
        object TabSheet4: TTabSheet
          Caption = 'Injection'
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object Panel53: TPanel
            Left = 0
            Top = 0
            Width = 862
            Height = 655
            Align = alClient
            BevelOuter = bvNone
            BorderWidth = 10
            TabOrder = 0
            object sgComInInjection: TStringGrid
              Left = 10
              Top = 10
              Width = 842
              Height = 635
              Align = alClient
              ColCount = 4
              DrawingStyle = gdsGradient
              FixedCols = 0
              RowCount = 99
              TabOrder = 0
              ColWidths = (
                120
                202
                120
                235)
            end
            object cbbJam: TComboBox
              Left = 127
              Top = 119
              Width = 145
              Height = 21
              ItemIndex = 0
              TabOrder = 1
              Text = 'Clear'
              Visible = False
              Items.Strings = (
                'Clear'
                'Jamming'
                'Noise'
                'MHTones')
            end
          end
        end
        object TabSheet5: TTabSheet
          Caption = 'Volume'
          ImageIndex = 1
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object Label133: TLabel
            Left = 12
            Top = 17
            Width = 67
            Height = 13
            Caption = 'Sound effects'
          end
          object Bevel11: TBevel
            Left = 96
            Top = 23
            Width = 333
            Height = 5
          end
          object Label134: TLabel
            Left = 16
            Top = 44
            Width = 44
            Height = 13
            Caption = 'Maximum'
          end
          object Label135: TLabel
            Left = 20
            Top = 488
            Width = 40
            Height = 13
            Caption = 'Minimum'
          end
          object Label136: TLabel
            Left = 390
            Top = 487
            Width = 40
            Height = 13
            Caption = 'Minimum'
          end
          object Label137: TLabel
            Left = 392
            Top = 47
            Width = 44
            Height = 13
            Caption = 'Maximum'
          end
          object Label138: TLabel
            Left = 65
            Top = 514
            Width = 26
            Height = 13
            Caption = 'Noise'
          end
          object Label139: TLabel
            Left = 199
            Top = 514
            Width = 41
            Height = 13
            Caption = 'Jamming'
          end
          object Label140: TLabel
            Left = 345
            Top = 514
            Width = 51
            Height = 13
            Caption = 'MHS tones'
          end
          object tbNoise: TTrackBar
            Left = 67
            Top = 46
            Width = 60
            Height = 462
            Orientation = trVertical
            TabOrder = 0
            TickMarks = tmTopLeft
          end
          object tbJamming: TTrackBar
            Left = 200
            Top = 46
            Width = 60
            Height = 462
            Orientation = trVertical
            TabOrder = 1
            TickMarks = tmTopLeft
          end
          object tbMHStones: TTrackBar
            Left = 347
            Top = 46
            Width = 39
            Height = 462
            Orientation = trVertical
            TabOrder = 2
            TickMarks = tmTopLeft
          end
          object btnNoiseCg: TButton
            Left = 54
            Top = 538
            Width = 75
            Height = 25
            Caption = 'Change'
            TabOrder = 3
          end
          object btnJamCg: TButton
            Left = 185
            Top = 538
            Width = 75
            Height = 25
            Caption = 'Change'
            TabOrder = 4
          end
          object btnMHTCg: TButton
            Left = 328
            Top = 539
            Width = 75
            Height = 25
            Caption = 'Change'
            TabOrder = 5
          end
        end
      end
    end
  end
end
