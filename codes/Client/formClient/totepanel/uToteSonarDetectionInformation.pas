unit uToteSonarDetectionInformation;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls, ExtCtrls, uTotePanel;

type
  TfrmSonarDetectionInformation = class(TfrmTotePanel)
    gbSonarDetectionInformation: TPanel;
    pnlSonarDetectInfoLeft: TPanel;
    Label17: TLabel;
    Label20: TLabel;
    lvSonarPlatform: TListView;
    lvSonarTarget: TListView;
    pnlSonarDetectInfoRight: TPanel;
    Label21: TLabel;
    GroupBox1: TGroupBox;
    Label22: TLabel;
    Label23: TLabel;
    Label24: TLabel;
    Label25: TLabel;
    CheckBox1: TCheckBox;
    CheckBox2: TCheckBox;
    CheckBox3: TCheckBox;
    CheckBox4: TCheckBox;
    CheckBox5: TCheckBox;
    CheckBox6: TCheckBox;
    CheckBox7: TCheckBox;
    CheckBox8: TCheckBox;
    CheckBox9: TCheckBox;
    CheckBox10: TCheckBox;
    CheckBox11: TCheckBox;
    CheckBox12: TCheckBox;
    CheckBox13: TCheckBox;
    CheckBox14: TCheckBox;
    Panel76: TPanel;
    CheckBox15: TCheckBox;
    lvSonar: TListView;
    Panel34: TPanel;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmSonarDetectionInformation: TfrmSonarDetectionInformation;

implementation

{$R *.dfm}

end.
