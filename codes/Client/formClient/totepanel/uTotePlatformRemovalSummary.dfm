object frmPlatformRemovalSummary: TfrmPlatformRemovalSummary
  Left = 0
  Top = 0
  Caption = 'frmPlatformRemovalSummary'
  ClientHeight = 730
  ClientWidth = 1034
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object gbPlatformRemovalSummary: TPanel
    Left = 0
    Top = 0
    Width = 1034
    Height = 730
    Align = alClient
    TabOrder = 0
    ExplicitTop = -2787
    ExplicitWidth = 975
    ExplicitHeight = 3517
    object Panel6: TPanel
      Left = 1
      Top = 1
      Width = 1032
      Height = 38
      Align = alTop
      Alignment = taLeftJustify
      BorderWidth = 4
      Caption = 'Platform Removal Summary'
      Color = clBtnShadow
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -24
      Font.Name = 'Arial'
      Font.Style = []
      ParentBackground = False
      ParentFont = False
      TabOrder = 0
      ExplicitWidth = 973
    end
    object Panel28: TPanel
      Left = 1
      Top = 39
      Width = 1032
      Height = 1110
      Align = alTop
      BevelOuter = bvNone
      BorderWidth = 4
      TabOrder = 1
      ExplicitWidth = 973
      object Panel90: TPanel
        Left = 4
        Top = 4
        Width = 1024
        Height = 649
        Align = alTop
        BevelOuter = bvNone
        BorderWidth = 10
        TabOrder = 0
        ExplicitWidth = 965
        object lvPlatformRemovalSum: TListView
          Left = 10
          Top = 10
          Width = 1004
          Height = 629
          Align = alClient
          Columns = <
            item
              Caption = 'Date/Time'
              Width = 150
            end
            item
              Caption = 'Name'
              Width = 250
            end
            item
              Caption = 'Reason'
              Width = 300
            end>
          RowSelect = True
          TabOrder = 0
          ViewStyle = vsReport
          ExplicitWidth = 945
        end
      end
    end
  end
end
