unit uToteCubicleGroups;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, StdCtrls, Grids, ExtCtrls,uTotePanel ;

type
  TfrmCubicleGroups = class(TfrmTotePanel)
    gbCubicleGroups: TPanel;
    Panel15: TPanel;
    Panel39: TPanel;
    PageControl3: TPageControl;
    TabSheet8: TTabSheet;
    Panel63: TPanel;
    tvCubicleGroups: TTreeView;
    TabSheet9: TTabSheet;
    Label157: TLabel;
    sgResponsibilityControllers: TStringGrid;
    Button36: TButton;
    Button37: TButton;
    Panel38: TPanel;
    Panel64: TPanel;
    lvCubicle: TListView;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmCubicleGroups: TfrmCubicleGroups;

implementation

{$R *.dfm}

end.
