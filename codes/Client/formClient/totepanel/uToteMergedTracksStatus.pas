unit uToteMergedTracksStatus;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls, ExtCtrls, uTotePanel;

type
  TfrmMergedTracksStatus = class(TfrmTotePanel)
    gbMergedTracksStatus: TPanel;
    Panel46: TPanel;
    Panel47: TPanel;
    Panel48: TPanel;
    lvMergedTrack: TListView;
    Panel50: TPanel;
    Label3: TLabel;
    Panel51: TPanel;
    btAdd: TButton;
    btRemove: TButton;
    Panel52: TPanel;
    Panel86: TPanel;
    Label4: TLabel;
    Panel88: TPanel;
    lvMergedTrackComponent: TListView;
    Panel91: TPanel;
    Button1: TButton;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmMergedTracksStatus: TfrmMergedTracksStatus;

implementation

{$R *.dfm}

end.
