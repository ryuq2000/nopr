unit uToteMessageHandlingSystem;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls, ExtCtrls, uTotePanel;

type
  TfrmMessageHandlingSystem = class(TfrmTotePanel)
    gbMessageHandlingSystem: TPanel;
    Panel8: TPanel;
    Panel25: TPanel;
    pcReceived: TPageControl;
    TabSheet1: TTabSheet;
    Panel74: TPanel;
    lvReceive: TListView;
    TabSheet2: TTabSheet;
    Panel78: TPanel;
    lvSent: TListView;
    TabSheet3: TTabSheet;
    Panel82: TPanel;
    lvDraft: TListView;
    Panel20: TPanel;
    Panel49: TPanel;
    btnNew: TButton;
    btnRemove: TButton;
    btnReply: TButton;
    btnForward: TButton;
    btnSend: TButton;
    btnEdit: TButton;
    btnPrint: TButton;
    Panel75: TPanel;
    mmoMessage: TMemo;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmMessageHandlingSystem: TfrmMessageHandlingSystem;

implementation

{$R *.dfm}

end.
