unit uTotePlatformRemovalSummary;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, ExtCtrls, uTotePanel;

type
  TfrmPlatformRemovalSummary = class(TfrmTotePanel)
    gbPlatformRemovalSummary: TPanel;
    Panel6: TPanel;
    Panel28: TPanel;
    Panel90: TPanel;
    lvPlatformRemovalSum: TListView;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmPlatformRemovalSummary: TfrmPlatformRemovalSummary;

implementation

{$R *.dfm}

end.
