unit uToteDatalinkControl;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ComCtrls, ExtCtrls, uTotePanel;

type
  TfrmDatalinkControl = class(TfrmTotePanel)
    gbDatalinkControl: TPanel;
    Panel12: TPanel;
    Panel29: TPanel;
    Panel94: TPanel;
    Label142: TLabel;
    Panel93: TPanel;
    Label141: TLabel;
    Label144: TLabel;
    Label148: TLabel;
    edtDatalink: TEdit;
    edtDLForce: TEdit;
    edtDLBand: TEdit;
    btnDatalink: TBitBtn;
    btnDatalinkForce: TBitBtn;
    btnDatalinkBand: TBitBtn;
    Panel95: TPanel;
    lvEligibleUnits: TListView;
    Panel31: TPanel;
    Panel96: TPanel;
    btnStopDataLink: TSpeedButton;
    btnPlayDataLink: TSpeedButton;
    Button31: TButton;
    Panel97: TPanel;
    Label143: TLabel;
    lvParticipatingUnits: TListView;
    Panel30: TPanel;
    Button27: TButton;
    Button28: TButton;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmDatalinkControl: TfrmDatalinkControl;

implementation

{$R *.dfm}

end.
