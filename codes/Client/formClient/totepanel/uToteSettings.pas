unit uToteSettings;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, Mask, StdCtrls, ExtCtrls, uTotePanel;

type
  TfrmSettings = class(TfrmTotePanel)
    gbSettings: TPanel;
    Panel44: TPanel;
    Panel45: TPanel;
    btnSetXML: TButton;
    btnGetXML: TButton;
    gbPercentage: TGroupBox;
    btSetPercentage: TButton;
    edtPercentage: TEdit;
    gbProbability: TGroupBox;
    rbNormal: TRadioButton;
    rbAlways: TRadioButton;
    rbNever: TRadioButton;
    gbSetEnDis: TGroupBox;
    cbSetEnDis: TCheckBox;
    gbSetTime: TGroupBox;
    btSetTimeApply: TButton;
    edtSetTime: TMaskEdit;
    gbGuidanceCommand: TGroupBox;
    rbRealistic: TRadioButton;
    rbDirect: TRadioButton;
    gbDistance: TGroupBox;
    edtDistance: TEdit;
    btSetDistanceApply: TButton;
    tvSettings: TTreeView;
    Panel43: TPanel;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmSettings: TfrmSettings;

implementation

{$R *.dfm}

end.
