unit uControlPanelManager;

interface

uses
  ufrmControlled, ufrmPanelOwnShip,
  ufrmPanelGuidance,
  ComCtrls, Classes, Controls, Forms;

type

  { class untuk mengelola panel - panel contropanel tactical display }
  TControlPanelType = (cptOwnShip, cptGuidance);
  TControlPanelManager = class
  private
    FPanelList : TList;
    function panelFactory(panel : TControlPanelType) : TfrmControlled;

  public
    constructor Create(controlPanel : TPageControl);
    destructor Destroy;override;

    procedure PanelUpdate;
  end;

implementation

{ TControlPanelManager }

constructor TControlPanelManager.Create;
var
  panel: TfrmControlled;
  enum : TControlPanelType;
begin
  if not Assigned(controlPanel) then
    Exit;

  FPanelList := TList.Create;

  { create each panel }
  for enum := cptOwnShip to cptGuidance do
  begin
    panel := panelFactory(enum);

    if Assigned(panel) then
    begin
      FPanelList.Add(panel);

      panel.Parent        := controlPanel.Pages[Byte(enum)];
      panel.Align         := alClient;
      panel.BorderStyle   := bsNone;
      panel.Show;

    end;

  end;
end;

destructor TControlPanelManager.Destroy;
var
  I: Integer;
  obj : TObject;
begin

  for I := 0 to FPanelList.Count - 1 do
  begin
    obj := FPanelList.Items[I];
    obj.Free;
  end;

  FPanelList.Free;

  inherited;
end;

function TControlPanelManager.panelFactory(panel: TControlPanelType): TfrmControlled;
begin
  result := nil;

  case panel of
    cptOwnShip    : result := TfrmPanelOwnShip.Create(nil);
    cptGuidance   : result := TfrmPanelGuidance.Create(nil);
  end;
end;

procedure TControlPanelManager.PanelUpdate;
var
  I: Integer;
  obj : TObject;
begin

  for I := 0 to FPanelList.Count - 1 do
  begin
    obj := FPanelList.Items[I];
    TfrmControlled(obj).UpdateForm;
  end;
end;

end.
