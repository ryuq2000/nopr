unit ufrmHookDetection;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ufrmHookPanel, uT3Track, StdCtrls, uT3MountedSensor,
  uSensorInfo;

type
  TfrmHookDetection = class(TfrmHookPanel)
    lblTrackDetection: TLabel;
    lbl1: TLabel;
    lblNameDetection: TLabel;
    lblClassDetection: TLabel;
    lbl2: TLabel;
    lbl3: TLabel;
    lbl4: TLabel;
    lblFirstDetected: TLabel;
    lbl5: TLabel;
    lblLastDetected: TLabel;
    lblOwner: TLabel;
    lblDetectionDetectionType: TLabel;
    txt1: TStaticText;
    txt2: TStaticText;
    txtDetectionType: TStaticText;
    txt3: TStaticText;
    txt4: TStaticText;
    txt5: TStaticText;
    txt6: TStaticText;
    txt7: TStaticText;
    txt8: TStaticText;
    txt9: TStaticText;
  private
    { Private declarations }
    function getLastDetector(track : TT3Track; out sensorInfo : TSensorTrackInfo) : TT3MountedSensor;
    function getSensorClassName(sensor : TT3MountedSensor) : String;
  public
    { Public declarations }
    procedure UpdateForm; override;
    procedure EmptyField;override;
  end;

var
  frmHookDetection: TfrmHookDetection;

implementation

uses
  {uT3PointTrack, uT3BearingTrack,}
  uGlobalVar, uT3Vehicle, uT3Common,
  Generics.Collections, uT3MountedVisual,
  uT3MountedIFF, uT3MountedRadar,
  uT3MountedSonar, uT3MountedMAD,
  uT3MountedEO,uT3MountedESM, tttData;

{$R *.dfm}

{ TfrmHookDetection }

procedure TfrmHookDetection.EmptyField;
begin
  inherited;

  lblTrackDetection.Caption         := '---';
  lblNameDetection.Caption          := '---';
  lblClassDetection.Caption         := '---';
  lblFirstDetected.Caption          := '---';
  lblLastDetected.Caption           := '---';
  lblOwner.Caption                  := '---';
  lblDetectionDetectionType.Caption := '---';

end;

function TfrmHookDetection.getLastDetector(track: TT3Track;
  out sensorInfo : TSensorTrackInfo): TT3MountedSensor;
var
  pair : TPair<TT3MountedSensor,TSensorTrackInfo>;
  latestDetTime : TDateTime;
  latestDetector: TT3MountedSensor;
  latestStatus  : TSensorTrackInfo;
begin
  latestDetTime := 0;
  latestStatus  := nil;
  latestDetector:= nil;

  for pair in track.DetectedBySensors do
  begin
    if pair.Key is TT3MountedIFF then
      continue;

    { if visual always become high order detector }
    if pair.Key is TT3MountedVisual then
    begin
      result := pair.Key;
      sensorInfo := pair.Value;
      break;
    end
    else
    begin

      if latestDetTime < pair.Value.LastDetected then
      begin
        latestDetTime   := pair.Value.LastDetected;
        latestStatus    := pair.Value;
        latestDetector  := pair.Key;
      end;

    end;
  end;

  sensorInfo  := latestStatus;
  result      := latestDetector;
end;

function TfrmHookDetection.getSensorClassName(sensor: TT3MountedSensor): String;
begin
  case sensor.SensorType of
    stRadar    : result := TT3MountedRadar(sensor).RadarDefinition.Radar_Identifier;
    stSonar    : result := TT3MountedSonar(sensor).SonarDefinition.Sonar_Identifier;
    stESM      : result := TT3MountedESM(sensor).ESMDefinition.Class_Identifier;
    stEO       : result := TT3MountedEO(sensor).EODefinition.Class_Identifier;
    stVisual   : result := 'Visual';
    stMAD      : result := TT3MountedMAD(sensor).MADDefinition.Class_Identifier;
    else
      result := 'Unknown';
  end;

end;

procedure TfrmHookDetection.UpdateForm;
var
  sensor : TT3MountedSensor;
  info   : TSensorTrackInfo;
begin
  inherited;

//  if Assigned(FHookedTrack) and (FHookedTrack is TT3Track ) then
//    with TT3Track(FHookedTrack) do
//    begin
//      if FHookedTrack is TT3BearingTrack then
//      begin
//        lblTrackDetection.Caption         := 'Unknown';
//        lblNameDetection.Caption          := 'Unknown';
//        lblClassDetection.Caption         := 'Unknown';
//        lblFirstDetected.Caption          := 'Unknown';
//        lblLastDetected.Caption           := 'Unknown';
//        lblOwner.Caption                  := 'Unknown';
//        lblDetectionDetectionType.Caption := 'ESM';
//      end
//      else
//      if FHookedTrack is TT3PointTrack then
//      begin
//        if TT3PointTrack(FHookedTrack).CanControl then
//          EmptyField
//        else
//        begin
//          sensor := getLastDetector(TT3Track(FHookedTrack),info);
//
//          if Assigned(sensor) then
//          begin
//            if DetailedDetectionShowed.Track_ID then
//              lblTrackDetection.Caption         := TrackLabelInfo
//            else
//              lblTrackDetection.Caption         := 'Unknown';
//
//            if DetailedDetectionShowed.Plat_Name_Recog_Capability then
//              lblNameDetection.Caption          := sensor.InstanceName
//            else
//              lblNameDetection.Caption          := 'Unknown';
//
//            if DetailedDetectionShowed.Plat_Class_Recog_Capability then
//              lblClassDetection.Caption         := getSensorClassName(sensor)
//            else
//              lblClassDetection.Caption         := 'Unknown';
//
//            lblFirstDetected.Caption          := TimeToStr(info.FirstDetected);
//            lblLastDetected.Caption           := TimeToStr(info.LastDetected);;
//
//            lblDetectionDetectionType.Caption := 'Track';
//
//            lblOwner.Caption                  := '---';
//          end
//          else
//            EmptyField;
//
//        end;
//      end
//    end;

end;

end.
