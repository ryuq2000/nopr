unit ufrmHookDetails;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ufrmHookPanel, uT3Track, uT3PlatformInstance;

type
  TfrmHookDetails = class(TfrmHookPanel)
    lblTrackDetails: TLabel;
    lbl1: TLabel;
    lblNameDetails: TLabel;
    lblClassdetails: TLabel;
    lbl2: TLabel;
    lbl3: TLabel;
    lbl4: TLabel;
    lblDomain: TLabel;
    lbl5: TLabel;
    lblPropulsion: TLabel;
    lblIdentifier: TLabel;
    lblDoppler: TLabel;
    lblSonarClass: TLabel;
    lblTrackType: TLabel;
    lblTypeDetails: TLabel;
    lblMergeStatus: TLabel;
    txt1: TStaticText;
    txt2: TStaticText;
    txt3: TStaticText;
    txt4: TStaticText;
    txt5: TStaticText;
    txt6: TStaticText;
    txt7: TStaticText;
    txt8: TStaticText;
    txt9: TStaticText;
    txt10: TStaticText;
    txt11: TStaticText;
    txt12: TStaticText;
    txt13: TStaticText;
    txt14: TStaticText;
    txt15: TStaticText;
    txt16: TStaticText;
    txt17: TStaticText;
    txt18: TStaticText;
  private
    { Private declarations }
    function getClassName(pf : TT3PlatformInstance) : String;
    function getForceDesig(pf : TT3PlatformInstance) : String;
    function getTrackType(track : TT3Track) : string;
  public
    { Public declarations }
    procedure UpdateForm; override;
    procedure EmptyField;override;
  end;

var
  frmHookDetails: TfrmHookDetails;

implementation

uses
  {uT3PointTrack, uT3BearingTrack,}
  uGlobalVar, uT3Vehicle, uT3Common;

{$R *.dfm}

{ TForm1 }

procedure TfrmHookDetails.EmptyField;
begin
  inherited;

  lblNameDetails.Caption  := '---';
  lblTrackDetails.Caption := '---';
  lblClassdetails.Caption := '---';
  lblDomain.Caption       := '---';
  lblPropulsion.Caption   := '---';
  lblIdentifier.Caption   := '---';
  lblDoppler.Caption      := '---';
  lblSonarClass.Caption   := '---';
  lblTrackType.Caption    := '---';
  lblTypeDetails.Caption  := '---';
  lblMergeStatus.Caption  := '---';

end;

function TfrmHookDetails.getClassName(pf: TT3PlatformInstance): String;
begin
  if pf is TT3Vehicle then
    result := TT3Vehicle(pf).VehicleDefinition.Vehicle_Identifier
  else
    result := 'Unknown';

//    if v is TT3Missile then
//      lbClassDetails.Caption := TMissile_On_Board(v.UnitDefinition)
//        .FDef.Class_Identifier;
//
//      if v is TT3Torpedo then
//        lbClassDetails.Caption := TTorpedo_On_Board(v.UnitDefinition)
//          .FDef.Class_Identifier;
//
//      if v is TT3Chaff then lbClassDetails.Caption := 'Chaff';
//
//      if v is TT3AirBubble then lbClassDetails.Caption := 'Air Bubble';
//
//      if v is TT3Decoy then lbClassDetails.Caption := 'Decoy';
//
//      if v is TT3Sonobuoy then lbClassDetails.Caption := 'Sonobuoy';
//
//      if v is TT3Mine then lbClassDetails.Caption := 'Mine';
end;

function TfrmHookDetails.getForceDesig(pf: TT3PlatformInstance): String;
begin
  case pf.ForceDesignation of
    1 : result := 'Red Force';
    2 : result := 'Yellow Force';
    3 : result := 'Blue Force';
    4 : result := 'Green Force';
    5 : result := 'White Force';
    6 : result := 'Black Force';
  else
    result := 'White Force';
  end;
end;

function TfrmHookDetails.getTrackType(track: TT3Track): string;
var
  t : String;
begin
  case track.TrackCategory of
    tcRealTrack    : t := 'Real Time ' ;
    tcNonRealTrack : t := 'Non Real Time ' ;
  end;

  case track.TrackType of
    trBearing      : t := t + 'Bearing Track';
    trAOP          : t := t + 'Area of Probability';
    trPoint        : t := t + 'Point Track';
    trSpecialPoint : t := 'Special Point';
  end;

  result := t;
end;

procedure TfrmHookDetails.UpdateForm;
var
  pf: TT3PlatformInstance;
begin
  inherited;

  pf := simManager.FindT3PlatformByID(TT3Track(FHookedTrack).ObjectInstanceIndex);

  if not Assigned(pf) then
  begin
    EmptyField;
    Exit;
  end;

//  if Assigned(FHookedTrack) and (FHookedTrack is TT3Track ) then
//  begin
//    lblTrackType.Caption    := getTrackType( TT3Track(FHookedTrack));
//
//    with TT3Track(FHookedTrack) do
//      // bearing track
//      if FHookedTrack is TT3BearingTrack then
//      begin
//        lblNameDetails.Caption  := 'Unknown';
//        lblTrackDetails.Caption := 'Unknown';
//        lblClassdetails.Caption := 'Unknown';
//        lblDomain.Caption       := 'Unknown';
//        lblPropulsion.Caption   := 'Unknown';
//        lblIdentifier.Caption   := 'Unknown';
//        lblDoppler.Caption      := 'Unknown';
//        lblSonarClass.Caption   := 'Unknown';
//        lblTypeDetails.Caption  := 'Unknown';
//        lblMergeStatus.Caption  := 'Unknown';
//      end
//      else
//      if FHookedTrack is TT3PointTrack then
//        with TT3PointTrack(FHookedTrack) do
//        begin
//          if CanControl then
//          begin
//            lblNameDetails.Caption  := pf.InstanceName;
//            lblTrackDetails.Caption := TrackLabelInfo;
//            lblClassdetails.Caption := getClassName(pf);
//            lblDomain.Caption       := getDomain(TrackDomain);
//            lblPropulsion.Caption   := 'Unknown';
//            lblIdentifier.Caption   := getForceDesig(pf);
//            lblDoppler.Caption      := '[None]';
//            lblSonarClass.Caption   := 'Unknown';
//            lblTypeDetails.Caption  := getTypeStr(TrackTypeDomain);
//            lblMergeStatus.Caption  := 'None';
//          end
//          else
//          begin
//            if DetailedDetectionShowed.Track_ID then
//              lblTrackDetails.Caption := TrackLabelInfo
//            else
//              lblTrackDetails.Caption   := 'Unknown';
//
//            if DetailedDetectionShowed.Plat_Name_Recog_Capability then
//              lblNameDetails.Caption      := pf.InstanceName
//            else
//              lblNameDetails.Caption      := 'Unknown';
//
//            if DetailedDetectionShowed.Plat_Class_Recog_Capability then
//              lblClassdetails.Caption     := getClassName(pf)
//            else
//              lblClassdetails.Caption     := 'Unknown';
//
//            if DetailedDetectionShowed.Plat_Type_Recog_Capability then
//              lblTypeDetails.Caption := getTypeStr(TrackTypeDomain)
//            else
//              lblTypeDetails.Caption := 'Unknown';
//
//            lblDomain.Caption       := getDomain(TrackDomain);
//            lblPropulsion.Caption   := 'Unknown';
//            lblIdentifier.Caption   := getIdentStr(TrackIdentifier);
//            lblDoppler.Caption      := 'Unknown';
//            lblSonarClass.Caption   := 'Unknown';
//            lblMergeStatus.Caption  := 'Not Merged';
//          end;
//        end;
//  end;
end;

end.
