unit ufrmHookPanel;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs;

type
  TfrmHookPanel = class(TForm)
  private
    { Private declarations }

  protected
    FHookedTrack : TObject;
  public
    { Public declarations }

    procedure SetHookedTrack(ctrlObj: TObject); virtual;
    procedure UpdateForm; virtual;
    procedure EmptyField;virtual;

  end;

var
  frmHookPanel: TfrmHookPanel;

implementation

{$R *.dfm}

{ TfrmHookPanel }

procedure TfrmHookPanel.EmptyField;
begin

end;

procedure TfrmHookPanel.SetHookedTrack(ctrlObj: TObject);
begin
  FHookedTrack := ctrlObj;

  if FHookedTrack = nil then
    EmptyField
  else
    UpdateForm;
end;

procedure TfrmHookPanel.UpdateForm;
begin

end;

end.
