unit ufrmHookIFF;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ufrmHookPanel, uT3Track, uT3PlatformInstance;

type
  TfrmHookIFF = class(TfrmHookPanel)
    lblTrackIff: TLabel;
    lbl1: TLabel;
    lblNameIff: TLabel;
    lblClassIff: TLabel;
    lbl2: TLabel;
    lbl3: TLabel;
    lbl4: TLabel;
    lblMode2Iff: TLabel;
    lbl5: TLabel;
    lblMode1Iff: TLabel;
    lblMode3CIff: TLabel;
    lblMode3Iff: TLabel;
    lblMode4Iff: TLabel;
    txt1: TStaticText;
    txt2: TStaticText;
    txt3: TStaticText;
    txt4: TStaticText;
    txt5: TStaticText;
    txt6: TStaticText;
    txt7: TStaticText;
    txt8: TStaticText;
    txt9: TStaticText;
    txt10: TStaticText;
    txt11: TStaticText;
    txt12: TStaticText;
  private
    { Private declarations }
    function getClassName(pf : TT3PlatformInstance) : String;
  public
    { Public declarations }
    procedure UpdateForm; override;
    procedure EmptyField;override;
  end;

var
  frmHookIFF: TfrmHookIFF;

implementation

uses
  {uT3PointTrack, uT3BearingTrack,}
  uGlobalVar, uT3Vehicle, uT3Common,
  Generics.Collections, uT3MountedVisual,
  uT3MountedIFF, uT3MountedRadar,
  uT3MountedSonar, uT3MountedMAD,
  uT3MountedEO,uT3MountedESM, tttData;

{$R *.dfm}

{ TfrmHookIFF }


procedure TfrmHookIFF.EmptyField;
begin
  inherited;

  lblTrackIff.Caption  := '---';
  lblNameIff.Caption   := '---';
  lblClassIff.Caption  := '---';
  lblMode2Iff.Caption  := '---';
  lblMode1Iff.Caption  := '---';
  lblMode3CIff.Caption := '---';
  lblMode3Iff.Caption  := '---';
  lblMode4Iff.Caption  := '---';

end;

function TfrmHookIFF.getClassName(pf: TT3PlatformInstance): String;
begin
  if pf is TT3Vehicle then
    result := TT3Vehicle(pf).VehicleDefinition.Vehicle_Identifier
  else
    result := 'Unknown';

end;

procedure TfrmHookIFF.UpdateForm;
var
  pf: TT3PlatformInstance;
begin
  inherited;

  pf := simManager.FindT3PlatformByID(TT3Track(FHookedTrack).ObjectInstanceIndex);

  if not Assigned(pf) then
  begin
    EmptyField;
    Exit;
  end;

//  if Assigned(FHookedTrack) and (FHookedTrack is TT3Track ) then
//    with TT3Track(FHookedTrack) do
//    begin
//      if FHookedTrack is TT3BearingTrack then
//      begin
//        lblTrackIff.Caption  := TrackLabelInfo;
//        lblNameIff.Caption   := 'Unknown';
//        lblClassIff.Caption  := 'Unknown';
//        lblMode2Iff.Caption  := '---';
//        lblMode1Iff.Caption  := '---';
//        lblMode3CIff.Caption := '---';
//        lblMode3Iff.Caption  := '---';
//        lblMode4Iff.Caption  := '---';
//      end
//      else
//      if FHookedTrack is TT3PointTrack then
//      begin
//        if TT3PointTrack(FHookedTrack).CanControl then
//          EmptyField
//        else
//        begin
//          if DetailedDetectionShowed.Track_ID then
//            lblTrackIff.Caption := TrackLabelInfo
//          else
//            lblTrackIff.Caption := 'Unknown';
//
//          if DetailedDetectionShowed.Plat_Name_Recog_Capability then
//            lblNameIff.Caption  := pf.InstanceName
//          else
//            lblNameIff.Caption  := 'Unknown';
//
//          if DetailedDetectionShowed.Plat_Class_Recog_Capability then
//            lblClassIff.Caption := getClassName(pf)
//          else
//            lblClassIff.Caption := 'Unknown';
//
////          lblMode1Iff.Caption := det.TransMode1Detected;
////          lblMode2Iff.Caption := det.TransMode2Detected;
////          lblMode3Iff.Caption := det.TransMode3Detected;
////          lblMode3CIff.Caption := det.TransMode3CDetected;
////
////          if det.TransMode1Detected = '' then
////          lbMode1Iff.Caption := '---';
////
////          if det.TransMode2Detected = '' then
////          lbMode2Iff.Caption := '---';
////
////          if det.TransMode3Detected = '' then
////          lbMode3Iff.Caption := '---';
////
////          if det.TransMode3CDetected = '' then
////          lbMode3CIff.Caption := '---';
//        end;
//      end;
//    end;

end;

end.
