unit ufrmHookInfo;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, ufrmHookPanel, uT3Track;

type
  TfrmHookInfo = class(TfrmHookPanel)
    lblTrackHook: TLabel;
    lbl1: TLabel;
    lblNameHook: TLabel;
    lblClassHook: TLabel;
    lbl2: TLabel;
    lblBearingHook: TLabel;
    lbl3: TLabel;
    lbl4: TLabel;
    lbl5: TLabel;
    lblRangeHook: TLabel;
    lbl6: TLabel;
    lbl7: TLabel;
    lblPositionHook1: TLabel;
    lblCourseHook: TLabel;
    lblPositionHook2: TLabel;
    lblGround: TLabel;
    lblFormation: TLabel;
    lbl8: TLabel;
    lbl9: TLabel;
    lblDamage: TLabel;
    lbllb8: TLabel;
    pnlDepth: TPanel;
    lblDepth: TLabel;
    lbllb2: TLabel;
    txtlbtext3: TStaticText;
    txtlb1: TStaticText;
    pnlAltitude: TPanel;
    lbllb4: TLabel;
    lblAltitude: TLabel;
    txtlb6: TStaticText;
    txtlb5: TStaticText;
    txt1: TStaticText;
    txt2: TStaticText;
    txt3: TStaticText;
    txt4: TStaticText;
    txt5: TStaticText;
    txt6: TStaticText;
    txt7: TStaticText;
    txt8: TStaticText;
    txt9: TStaticText;
    txt10: TStaticText;
    txt11: TStaticText;
    txt12: TStaticText;
    txt13: TStaticText;
    txtlb7: TStaticText;
    txtlb3: TStaticText;
  private
    { Private declarations }
  public
    { Public declarations }
    procedure UpdateForm; override;
    procedure EmptyField; override;
    procedure SetHookedTrack(ctrlObj: TObject); override;
  end;

var
  frmHookInfo: TfrmHookInfo;

implementation

uses {uT3PointTrack, }uT3PlatformInstance, uGlobalVar,
  uT3Vehicle, uT3Missile, uT3Torpedo, uT3ClientManager,
  uT3Mine, uT3Sonobuoy, uBaseCoordSystem, uSettingCoordinate
  {uT3BearingTrack};
{$R *.dfm}
{ TfrmHookInfo }

procedure TfrmHookInfo.EmptyField;
begin
  inherited;

  lblTrackHook.Caption := '---';
  lblNameHook.Caption := '---';
  lblClassHook.Caption := '---';
  lblBearingHook.Caption := '---';
  lblRangeHook.Caption := '---';
  lblPositionHook1.Caption := '---';
  lblCourseHook.Caption := '---';
  lblPositionHook2.Caption := '---';
  lblGround.Caption := '---';
  lblFormation.Caption := '---';
  lblDamage.Caption := '---';
  lblDepth.Caption := '---';
  lblAltitude.Caption := '---';
end;

procedure TfrmHookInfo.SetHookedTrack(ctrlObj: TObject);
begin
  inherited;

  if Assigned(FHookedTrack) then
  begin
    if TT3Track(FHookedTrack).TrackDomain = 0 then
    begin
      pnlDepth.Visible := False;
      pnlAltitude.Visible := True;
    end
    else
    begin
      pnlDepth.Visible := True;
      pnlAltitude.Visible := False;
    end;
  end;

end;

procedure TfrmHookInfo.UpdateForm;
var
  pf: TT3PlatformInstance;
  idCoordinat: Integer;
  long, lat, b, d: double;
  pY, pX: Extended;
begin
  inherited;

//  if Assigned(FHookedTrack) and (FHookedTrack is TT3Track) then
//  begin
//    idCoordinat := fSettingCoordinate.IdCoordinat;
//    pf := queryPlatformInfo(TT3Track(FHookedTrack).ObjectInstanceIndex);
//
//    if not Assigned(pf) then
//    begin
//      EmptyField;
//      Exit;
//    end;
//
//    // esm track
//    if FHookedTrack is TT3BearingTrack then
//    begin
//
//    end
//    else
//    if FHookedTrack is TT3PointTrack then
//      with TT3PointTrack(FHookedTrack) do
//      begin
//        { owned platform }
//        if CanControl then
//        begin
//          lblTrackHook.Caption   := TrackLabelInfo;
//          lblNameHook.Caption    := pf.InstanceName;
//
//          if pf is TT3Vehicle then
//            lblClassHook.Caption := TT3Vehicle(pf)
//              .VehicleDefinition.FData.Vehicle_Identifier
//          else
//            lblClassHook.Caption := 'Unknown';
//
//          lblBearingHook.Caption := '---';
//          lblRangeHook.Caption   := '---';
//          lblCourseHook.Caption  := FormatCourse(pf.Course);
//          lblGround.Caption      := FormatSpeed(pf.Speed);
//          lblFormation.Caption   := '---';
//          lblDamage.Caption      := 'Unknown';
//          lblDepth.Caption       := FormatAltitude(pf.Altitude);
//          lblAltitude.Caption    := FormatAltitude(pf.Altitude);
//        end
//        else
//        begin
//          if DetailedDetectionShowed.Track_ID then
//            lblTrackHook.Caption := TrackLabelInfo
//          else
//            lblTrackHook.Caption := 'Unknown';
//
//          if DetailedDetectionShowed.Plat_Name_Recog_Capability then
//            lblNameHook.Caption  := pf.InstanceName
//          else
//            lblNameHook.Caption  := 'Unknown';
//
//          if DetailedDetectionShowed.Plat_Class_Recog_Capability then
//          begin
//            if pf is TT3Vehicle then
//              lblClassHook.Caption := TT3Vehicle(pf)
//                .VehicleDefinition.FData.Vehicle_Identifier
//            else
//            if pf is TT3Missile then
//              //lblClassHook.Caption := TT3Missile(pf)
//              //  .VehicleDefinition.FData.Vehicle_Identifier
//            else
//            if pf is TT3Torpedo then
//              //lbClassHook.Caption := TTorpedo_On_Board(v.UnitDefinition)
//              //  .FDef.Class_Identifier;
//            //else
//            //if pf is TT3Chaff then lbClassHook.Caption := 'Chaff';
//            //else
//            //if pf is TT3AirBubble then lbClassHook.Caption := 'Air Bubble';
//            //else
//            //if v is TT3Decoy then lbClassHook.Caption := 'Decoy';
//            else
//            if pf is TT3Sonobuoy then lblClassHook.Caption := 'Sonobuoy'
//            else
//            if pf is TT3Mine then lblClassHook.Caption := 'Mine';
//          end
//          else
//            lblClassHook.Caption := 'Unknown';
//
//          if DetailedDetectionShowed.Heading_Data_Capability then
//            lblCourseHook.Caption    := FormatCourse(DetectedCourse)
//          else
//            lblCourseHook.Caption    := '---';
//
//          if DetailedDetectionShowed.Ground_Speed_Data_Capability then
//            lblGround.Caption        := FormatSpeed(DetectedSpeed)
//          else
//            lblGround.Caption        := '---';
//
//          if DetailedDetectionShowed.Altitude_Data_Capability then
//          begin
//            lblAltitude.Caption    := FormatAltitude(DetectedAltitude * C_Meter_To_Feet);
//            lblDepth.Caption       := FormatAltitude(DetectedAltitude * C_Meter_To_Feet);
//          end
//          else
//          begin
//            lblAltitude.Caption    := '---';
//            lblDepth.Caption       := '---';
//          end;
//
//          lblBearingHook.Caption   := '---';
//          lblRangeHook.Caption     := '---';
//
//          if Assigned(clientManager.ControlledTrack) then
//          begin
//            b := CalcBearing(clientManager.ControlledTrack.PosX,
//                  clientManager.ControlledTrack.PosY, PosX, PosY);
//            d := CalcRange(clientManager.ControlledTrack.PosX,
//                  clientManager.ControlledTrack.PosY, PosX, PosY);
//
//            lblBearingHook.Caption   := FormatCourse(b); ;
//            lblRangeHook.Caption     := FormatFloat('000.0', d);
//          end;
//
//          lblFormation.Caption := '---';
//          lblDamage.Caption := 'Unknown';
//        end;
//
//        long  := clientManager.Scenario.GameEnvironment.FGameArea.Game_Centre_Long;
//        lat   := clientManager.Scenario.GameEnvironment.FGameArea.Game_Centre_Lat;
//        txt2.Caption := 'Position';
//
//        case idCoordinat of
//          1:
//          begin
//            lblPositionHook1.Caption := formatDMS_long(PosX);
//            lblPositionHook2.Caption := formatDMS_latt(PosY);
//          end;
//          2:
//          begin
//            pX := CalcMove(PosX, long);
//            pY := CalcMove(PosY, lat);
//
//            if (pX >= 0) and (pY >=0) then
//            begin
//              lblPositionHook1.Caption := 'White ' + FormatFloat('0.00', Abs(pX));  //kuadran 1
//            end;
//            if (pX <= 0) and (pY >=0) then
//            begin
//              lblPositionHook1.Caption := 'Red ' + FormatFloat('0.00', Abs(pX));   //kuadran 2
//            end;
//            if (pX < 0) and (pY < 0) then
//            begin
//              lblPositionHook1.Caption := 'Green ' + FormatFloat('0.00', Abs(pX)); //kuadran 3
//            end;
//            if (pX >= 0) and (pY <= 0) then
//            begin
//              lblPositionHook1.Caption := 'Blue ' + FormatFloat('0.00', Abs(pX));  //kuadran 4
//            end;
//
//            lblPositionHook2.Caption := FormatFloat('0.00', Abs(pY));
//          end;
//          3:
//          begin
//            lblPositionHook1.Caption := ConvDegree_To_Georef(PosX, PosY);
//            lblPositionHook2.Caption := '';
//          end;
//        end;
//      end;
//  end;
end;

end.
