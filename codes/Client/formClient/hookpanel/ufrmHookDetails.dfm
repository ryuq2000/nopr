object frmHookDetails: TfrmHookDetails
  Left = 0
  Top = 0
  Caption = 'frmHookDetails'
  ClientHeight = 218
  ClientWidth = 285
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object lblTrackDetails: TLabel
    Left = 110
    Top = 5
    Width = 12
    Height = 13
    Caption = '---'
  end
  object lbl1: TLabel
    Left = 3
    Top = 20
    Width = 27
    Height = 13
    Caption = 'Name'
  end
  object lblNameDetails: TLabel
    Left = 110
    Top = 21
    Width = 12
    Height = 13
    Caption = '---'
  end
  object lblClassdetails: TLabel
    Left = 110
    Top = 37
    Width = 12
    Height = 13
    Caption = '---'
  end
  object lbl2: TLabel
    Left = 3
    Top = 37
    Width = 25
    Height = 13
    Caption = 'Class'
  end
  object lbl3: TLabel
    Left = 3
    Top = 54
    Width = 24
    Height = 13
    Caption = 'Type'
  end
  object lbl4: TLabel
    Left = 3
    Top = 72
    Width = 35
    Height = 13
    Caption = 'Domain'
  end
  object lblDomain: TLabel
    Left = 110
    Top = 72
    Width = 12
    Height = 13
    Caption = '---'
  end
  object lbl5: TLabel
    Left = 85
    Top = 105
    Width = 3
    Height = 13
  end
  object lblPropulsion: TLabel
    Left = 110
    Top = 108
    Width = 12
    Height = 13
    Caption = '---'
  end
  object lblIdentifier: TLabel
    Left = 110
    Top = 90
    Width = 12
    Height = 13
    Caption = '---'
  end
  object lblDoppler: TLabel
    Left = 110
    Top = 144
    Width = 12
    Height = 13
    Caption = '---'
  end
  object lblSonarClass: TLabel
    Left = 110
    Top = 126
    Width = 12
    Height = 13
    Caption = '---'
  end
  object lblTrackType: TLabel
    Left = 110
    Top = 161
    Width = 12
    Height = 13
    Caption = '---'
  end
  object lblTypeDetails: TLabel
    Left = 110
    Top = 54
    Width = 12
    Height = 13
    Caption = '---'
  end
  object lblMergeStatus: TLabel
    Left = 110
    Top = 180
    Width = 12
    Height = 13
    Caption = '---'
  end
  object txt1: TStaticText
    Left = 3
    Top = 3
    Width = 30
    Height = 17
    Caption = 'Track'
    TabOrder = 0
  end
  object txt2: TStaticText
    Left = 3
    Top = 90
    Width = 48
    Height = 17
    Caption = 'Identifier'
    TabOrder = 1
  end
  object txt3: TStaticText
    Left = 3
    Top = 108
    Width = 80
    Height = 17
    Caption = 'Propulsion Type'
    TabOrder = 2
  end
  object txt4: TStaticText
    Left = 3
    Top = 126
    Width = 41
    Height = 17
    Caption = 'Doppler'
    TabOrder = 3
  end
  object txt5: TStaticText
    Left = 3
    Top = 144
    Width = 60
    Height = 17
    Caption = 'Sonar Class'
    TabOrder = 4
  end
  object txt6: TStaticText
    Left = 3
    Top = 162
    Width = 57
    Height = 17
    Caption = 'Track Type'
    TabOrder = 5
  end
  object txt7: TStaticText
    Left = 3
    Top = 180
    Width = 68
    Height = 17
    Caption = 'Merge Status'
    TabOrder = 6
  end
  object txt8: TStaticText
    Left = 96
    Top = 3
    Width = 8
    Height = 17
    Caption = ':'
    TabOrder = 7
  end
  object txt9: TStaticText
    Left = 96
    Top = 19
    Width = 8
    Height = 17
    Caption = ':'
    TabOrder = 8
  end
  object txt10: TStaticText
    Left = 96
    Top = 35
    Width = 8
    Height = 17
    Caption = ':'
    TabOrder = 9
  end
  object txt11: TStaticText
    Left = 96
    Top = 52
    Width = 8
    Height = 17
    Caption = ':'
    TabOrder = 10
  end
  object txt12: TStaticText
    Left = 96
    Top = 72
    Width = 8
    Height = 17
    Caption = ':'
    TabOrder = 11
  end
  object txt13: TStaticText
    Left = 96
    Top = 90
    Width = 8
    Height = 17
    Caption = ':'
    TabOrder = 12
  end
  object txt14: TStaticText
    Left = 96
    Top = 126
    Width = 8
    Height = 17
    Caption = ':'
    TabOrder = 13
  end
  object txt15: TStaticText
    Left = 96
    Top = 144
    Width = 8
    Height = 17
    Caption = ':'
    TabOrder = 14
  end
  object txt16: TStaticText
    Left = 96
    Top = 160
    Width = 8
    Height = 17
    Caption = ':'
    TabOrder = 15
  end
  object txt17: TStaticText
    Left = 96
    Top = 108
    Width = 8
    Height = 17
    Caption = ':'
    TabOrder = 16
  end
  object txt18: TStaticText
    Left = 96
    Top = 180
    Width = 8
    Height = 17
    Caption = ':'
    TabOrder = 17
  end
end
