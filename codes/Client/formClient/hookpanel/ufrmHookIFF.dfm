object frmHookIFF: TfrmHookIFF
  Left = 0
  Top = 0
  Caption = 'frmHookIFF'
  ClientHeight = 191
  ClientWidth = 234
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object lblTrackIff: TLabel
    Left = 91
    Top = 3
    Width = 12
    Height = 13
    Caption = '---'
  end
  object lbl1: TLabel
    Left = 3
    Top = 19
    Width = 27
    Height = 13
    Caption = 'Name'
  end
  object lblNameIff: TLabel
    Left = 91
    Top = 19
    Width = 12
    Height = 13
    Caption = '---'
  end
  object lblClassIff: TLabel
    Left = 91
    Top = 36
    Width = 12
    Height = 13
    Caption = '---'
  end
  object lbl2: TLabel
    Left = 3
    Top = 36
    Width = 25
    Height = 13
    Caption = 'Class'
  end
  object lbl3: TLabel
    Left = 3
    Top = 66
    Width = 35
    Height = 13
    Caption = 'Mode 1'
  end
  object lbl4: TLabel
    Left = 3
    Top = 85
    Width = 35
    Height = 13
    Caption = 'Mode 2'
  end
  object lblMode2Iff: TLabel
    Left = 91
    Top = 86
    Width = 12
    Height = 13
    Caption = '---'
  end
  object lbl5: TLabel
    Left = 95
    Top = 106
    Width = 3
    Height = 13
  end
  object lblMode1Iff: TLabel
    Left = 91
    Top = 66
    Width = 12
    Height = 13
    Caption = '---'
  end
  object lblMode3CIff: TLabel
    Left = 91
    Top = 126
    Width = 12
    Height = 13
    Caption = '---'
  end
  object lblMode3Iff: TLabel
    Left = 91
    Top = 106
    Width = 12
    Height = 13
    Caption = '---'
  end
  object lblMode4Iff: TLabel
    Left = 91
    Top = 147
    Width = 12
    Height = 13
    Caption = '---'
  end
  object txt1: TStaticText
    Left = 3
    Top = 3
    Width = 30
    Height = 17
    Caption = 'Track'
    TabOrder = 0
  end
  object txt2: TStaticText
    Left = 3
    Top = 105
    Width = 39
    Height = 17
    Caption = 'Mode 3'
    TabOrder = 1
  end
  object txt3: TStaticText
    Left = 3
    Top = 125
    Width = 46
    Height = 17
    Caption = 'Mode 3C'
    TabOrder = 2
  end
  object txt4: TStaticText
    Left = 3
    Top = 145
    Width = 39
    Height = 17
    Caption = 'Mode 4'
    TabOrder = 3
  end
  object txt5: TStaticText
    Left = 80
    Top = 3
    Width = 8
    Height = 17
    Caption = ':'
    TabOrder = 4
  end
  object txt6: TStaticText
    Left = 80
    Top = 19
    Width = 8
    Height = 17
    Caption = ':'
    TabOrder = 5
  end
  object txt7: TStaticText
    Left = 80
    Top = 35
    Width = 8
    Height = 17
    Caption = ':'
    TabOrder = 6
  end
  object txt8: TStaticText
    Left = 80
    Top = 64
    Width = 8
    Height = 17
    Caption = ':'
    TabOrder = 7
  end
  object txt9: TStaticText
    Left = 80
    Top = 84
    Width = 8
    Height = 17
    Caption = ':'
    TabOrder = 8
  end
  object txt10: TStaticText
    Left = 80
    Top = 124
    Width = 8
    Height = 17
    Caption = ':'
    TabOrder = 9
  end
  object txt11: TStaticText
    Left = 80
    Top = 145
    Width = 8
    Height = 17
    Caption = ':'
    TabOrder = 10
  end
  object txt12: TStaticText
    Left = 80
    Top = 104
    Width = 8
    Height = 17
    Caption = ':'
    TabOrder = 11
  end
end
