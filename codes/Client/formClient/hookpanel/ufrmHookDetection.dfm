object frmHookDetection: TfrmHookDetection
  Left = 0
  Top = 0
  Caption = 'frmHookDetection'
  ClientHeight = 162
  ClientWidth = 265
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object lblTrackDetection: TLabel
    Left = 137
    Top = 5
    Width = 12
    Height = 13
    Caption = '---'
  end
  object lbl1: TLabel
    Left = 3
    Top = 19
    Width = 27
    Height = 13
    Caption = 'Name'
  end
  object lblNameDetection: TLabel
    Left = 137
    Top = 20
    Width = 12
    Height = 13
    Caption = '---'
  end
  object lblClassDetection: TLabel
    Left = 137
    Top = 36
    Width = 12
    Height = 13
    Caption = '---'
  end
  object lbl2: TLabel
    Left = 3
    Top = 36
    Width = 25
    Height = 13
    Caption = 'Class'
  end
  object lbl3: TLabel
    Left = 3
    Top = 62
    Width = 88
    Height = 13
    Caption = 'Owner PU Number'
  end
  object lbl4: TLabel
    Left = 3
    Top = 93
    Width = 68
    Height = 13
    Caption = 'First Detected'
  end
  object lblFirstDetected: TLabel
    Left = 137
    Top = 93
    Width = 12
    Height = 13
    Caption = '---'
  end
  object lbl5: TLabel
    Left = 85
    Top = 114
    Width = 3
    Height = 13
  end
  object lblLastDetected: TLabel
    Left = 137
    Top = 114
    Width = 12
    Height = 13
    Caption = '---'
  end
  object lblOwner: TLabel
    Left = 137
    Top = 62
    Width = 12
    Height = 13
    Caption = '---'
  end
  object lblDetectionDetectionType: TLabel
    Left = 137
    Top = 135
    Width = 8
    Height = 13
    Caption = '--'
  end
  object txt1: TStaticText
    Left = 3
    Top = 3
    Width = 30
    Height = 17
    Caption = 'Track'
    TabOrder = 0
  end
  object txt2: TStaticText
    Left = 3
    Top = 113
    Width = 71
    Height = 17
    Caption = 'Last Detected'
    TabOrder = 1
  end
  object txtDetectionType: TStaticText
    Left = 3
    Top = 133
    Width = 77
    Height = 17
    Caption = 'Detection Type'
    TabOrder = 2
  end
  object txt3: TStaticText
    Left = 120
    Top = 3
    Width = 8
    Height = 17
    Caption = ':'
    TabOrder = 3
  end
  object txt4: TStaticText
    Left = 120
    Top = 18
    Width = 8
    Height = 17
    Caption = ':'
    TabOrder = 4
  end
  object txt5: TStaticText
    Left = 120
    Top = 34
    Width = 8
    Height = 17
    Caption = ':'
    TabOrder = 5
  end
  object txt6: TStaticText
    Left = 120
    Top = 60
    Width = 8
    Height = 17
    Caption = ':'
    TabOrder = 6
  end
  object txt7: TStaticText
    Left = 120
    Top = 91
    Width = 8
    Height = 17
    Caption = ':'
    TabOrder = 7
  end
  object txt8: TStaticText
    Left = 120
    Top = 133
    Width = 8
    Height = 17
    Caption = ':'
    TabOrder = 8
  end
  object txt9: TStaticText
    Left = 120
    Top = 112
    Width = 8
    Height = 17
    Caption = ':'
    TabOrder = 9
  end
end
