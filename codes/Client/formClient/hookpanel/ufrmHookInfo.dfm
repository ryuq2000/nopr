object frmHookInfo: TfrmHookInfo
  Left = 0
  Top = 0
  Caption = 'frmHookInfo'
  ClientHeight = 214
  ClientWidth = 352
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object lblTrackHook: TLabel
    Left = 100
    Top = 5
    Width = 12
    Height = 13
    Caption = '---'
  end
  object lbl1: TLabel
    Left = 3
    Top = 20
    Width = 27
    Height = 13
    Caption = 'Name'
  end
  object lblNameHook: TLabel
    Left = 100
    Top = 21
    Width = 12
    Height = 13
    Caption = '---'
  end
  object lblClassHook: TLabel
    Left = 100
    Top = 38
    Width = 12
    Height = 13
    Caption = '---'
  end
  object lbl2: TLabel
    Left = 3
    Top = 38
    Width = 25
    Height = 13
    Caption = 'Class'
  end
  object lblBearingHook: TLabel
    Left = 100
    Top = 56
    Width = 15
    Height = 13
    Caption = '---'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lbl3: TLabel
    Left = 3
    Top = 56
    Width = 36
    Height = 13
    Caption = 'Bearing'
  end
  object lbl4: TLabel
    Left = 190
    Top = 56
    Width = 43
    Height = 13
    Caption = 'degree T'
  end
  object lbl5: TLabel
    Left = 3
    Top = 73
    Width = 31
    Height = 13
    Caption = 'Range'
  end
  object lblRangeHook: TLabel
    Left = 100
    Top = 73
    Width = 15
    Height = 13
    Caption = '---'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lbl6: TLabel
    Left = 190
    Top = 73
    Width = 14
    Height = 13
    Caption = 'nm'
  end
  object lbl7: TLabel
    Left = 140
    Top = 109
    Width = 3
    Height = 13
  end
  object lblPositionHook1: TLabel
    Left = 100
    Top = 90
    Width = 12
    Height = 13
    Caption = '---'
  end
  object lblCourseHook: TLabel
    Left = 100
    Top = 108
    Width = 12
    Height = 13
    Caption = '---'
  end
  object lblPositionHook2: TLabel
    Left = 190
    Top = 90
    Width = 12
    Height = 13
    Caption = '---'
  end
  object lblGround: TLabel
    Left = 100
    Top = 125
    Width = 12
    Height = 13
    Caption = '---'
  end
  object lblFormation: TLabel
    Left = 100
    Top = 160
    Width = 12
    Height = 13
    Caption = '---'
  end
  object lbl8: TLabel
    Left = 190
    Top = 108
    Width = 43
    Height = 13
    Caption = 'degree T'
  end
  object lbl9: TLabel
    Left = 190
    Top = 125
    Width = 21
    Height = 13
    Caption = 'knot'
  end
  object lblDamage: TLabel
    Left = 100
    Top = 144
    Width = 12
    Height = 13
    Caption = '---'
  end
  object lbllb8: TLabel
    Left = 3
    Top = 142
    Width = 39
    Height = 13
    Caption = 'Damage'
  end
  object pnlDepth: TPanel
    Left = 3
    Top = 172
    Width = 329
    Height = 22
    BevelOuter = bvNone
    TabOrder = 0
    Visible = False
    object lblDepth: TLabel
      Left = 97
      Top = 5
      Width = 12
      Height = 13
      Caption = '---'
    end
    object lbllb2: TLabel
      Left = 187
      Top = 5
      Width = 28
      Height = 13
      Caption = 'meter'
    end
    object txtlbtext3: TStaticText
      Left = 0
      Top = 5
      Width = 33
      Height = 17
      Caption = 'Depth'
      TabOrder = 0
    end
    object txtlb1: TStaticText
      Left = 86
      Top = 5
      Width = 8
      Height = 17
      Caption = ':'
      TabOrder = 1
    end
  end
  object pnlAltitude: TPanel
    Left = 0
    Top = 171
    Width = 329
    Height = 22
    BevelOuter = bvNone
    TabOrder = 1
    Visible = False
    object lbllb4: TLabel
      Left = 187
      Top = 5
      Width = 20
      Height = 13
      Caption = 'feet'
    end
    object lblAltitude: TLabel
      Left = 100
      Top = 3
      Width = 12
      Height = 13
      Caption = '---'
    end
    object txtlb6: TStaticText
      Left = 89
      Top = 5
      Width = 8
      Height = 17
      Caption = ':'
      TabOrder = 0
    end
    object txtlb5: TStaticText
      Left = 3
      Top = 4
      Width = 41
      Height = 17
      Caption = 'Altitude'
      TabOrder = 1
    end
  end
  object txt1: TStaticText
    Left = 3
    Top = 3
    Width = 30
    Height = 17
    Caption = 'Track'
    TabOrder = 2
  end
  object txt2: TStaticText
    Left = 3
    Top = 90
    Width = 41
    Height = 17
    Caption = 'Position'
    TabOrder = 3
  end
  object txt3: TStaticText
    Left = 3
    Top = 108
    Width = 38
    Height = 17
    Caption = 'Course'
    TabOrder = 4
  end
  object txt4: TStaticText
    Left = 3
    Top = 125
    Width = 72
    Height = 17
    Caption = 'Ground Speed'
    TabOrder = 5
  end
  object txt5: TStaticText
    Left = 3
    Top = 158
    Width = 74
    Height = 17
    Caption = 'Formation Size'
    TabOrder = 6
  end
  object txt6: TStaticText
    Left = 89
    Top = 3
    Width = 8
    Height = 17
    Caption = ':'
    TabOrder = 7
  end
  object txt7: TStaticText
    Left = 89
    Top = 19
    Width = 8
    Height = 17
    Caption = ':'
    TabOrder = 8
  end
  object txt8: TStaticText
    Left = 89
    Top = 36
    Width = 8
    Height = 17
    Caption = ':'
    TabOrder = 9
  end
  object txt9: TStaticText
    Left = 89
    Top = 56
    Width = 8
    Height = 17
    Caption = ':'
    TabOrder = 10
  end
  object txt10: TStaticText
    Left = 89
    Top = 73
    Width = 8
    Height = 17
    Caption = ':'
    TabOrder = 11
  end
  object txt11: TStaticText
    Left = 89
    Top = 90
    Width = 8
    Height = 17
    Caption = ':'
    TabOrder = 12
  end
  object txt12: TStaticText
    Left = 89
    Top = 125
    Width = 8
    Height = 17
    Caption = ':'
    TabOrder = 13
  end
  object txt13: TStaticText
    Left = 89
    Top = 108
    Width = 8
    Height = 17
    Caption = ':'
    TabOrder = 14
  end
  object txtlb7: TStaticText
    Left = 89
    Top = 160
    Width = 8
    Height = 17
    Caption = ':'
    TabOrder = 15
  end
  object txtlb3: TStaticText
    Left = 89
    Top = 142
    Width = 8
    Height = 17
    Caption = ':'
    TabOrder = 16
  end
end
