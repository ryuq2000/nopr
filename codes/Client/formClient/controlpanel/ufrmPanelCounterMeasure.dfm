object frmPanelCM: TfrmPanelCM
  Left = 0
  Top = 0
  Caption = 'frmPanelCM'
  ClientHeight = 444
  ClientWidth = 350
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object PanelCounterMeasureChoice: TPanel
    Left = 0
    Top = 0
    Width = 350
    Height = 100
    Align = alTop
    BevelOuter = bvNone
    ParentBackground = False
    TabOrder = 0
    object lvECM: TListView
      Left = 0
      Top = 0
      Width = 350
      Height = 100
      Align = alClient
      Columns = <
        item
          Caption = 'Name'
          Width = 243
        end
        item
          Caption = 'Status'
          Width = 80
        end>
      HideSelection = False
      ReadOnly = True
      RowSelect = True
      TabOrder = 0
      ViewStyle = vsReport
      OnSelectItem = lvECMSelectItem
    end
  end
end
