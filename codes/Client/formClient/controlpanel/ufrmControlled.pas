unit ufrmControlled;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, uT3Track, uT3PlatformInstance;

type
  TControlPanelType = (cptOwnShip, cptGuidance, cptSensors, cptWeapon, cptCounterMeasure,
    cptFireControl, cptEMCON);

  TfrmControlled = class(TForm)
  private
    FControlPanelType: TControlPanelType;
    procedure SetControlPanelType(const Value: TControlPanelType);
    { Private declarations }
  protected
    FControlledTrack : TT3Track;
    FSelectedTrack   : TT3Track;

    FControlledPlatform : TT3PlatformInstance;
    FActiveTab: integer;
    procedure DisplayTab(const i: byte); virtual;
  public
    { Public declarations }
    procedure InitCreate(sender: TForm); virtual;

    procedure SetControlledObject(ctrlObj: TT3Track); virtual;
    procedure SetSelectedObject(selectedObj: TT3Track);
    procedure UpdateForm; virtual;
    procedure ReadOnlyMode; virtual;
    procedure UnReadOnlyMode; virtual;
    procedure EmptyField;virtual;

    property  ControlPanelType   : TControlPanelType read FControlPanelType write SetControlPanelType;

  end;

var
  frmControlled: TfrmControlled;

implementation

uses uGlobalVar;

{$R *.dfm}

{ TfrmControlled }

procedure TfrmControlled.DisplayTab(const i: byte);
begin

end;

procedure TfrmControlled.EmptyField;
begin

end;

procedure TfrmControlled.InitCreate(sender: TForm);
begin

end;

procedure TfrmControlled.ReadOnlyMode;
begin

end;

procedure TfrmControlled.SetControlledObject(ctrlObj: TT3Track);
begin
  FControlledTrack := ctrlObj;
  FControlledPlatform := nil;

  if Assigned(FControlledTrack) then
  begin
    FControlledPlatform := simManager.FindT3PlatformByID(FControlledTrack.ObjectInstanceIndex);
  end;
end;

procedure TfrmControlled.SetControlPanelType(const Value: TControlPanelType);
begin
  FControlPanelType := Value;
end;

procedure TfrmControlled.SetSelectedObject(selectedObj: TT3Track);
begin
  FSelectedTrack := selectedObj;
end;

procedure TfrmControlled.UnReadOnlyMode;
begin

end;

procedure TfrmControlled.UpdateForm;
begin

end;

end.
