unit ufrmPanelEMCON;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, ExtCtrls, ufrmControlled, StdCtrls, Buttons, uT3Track;

type
  TfrmPanelEMCON = class(TfrmControlled)
    PanelFCChoices: TPanel;
    lstAssetsChoices: TListView;
    PaneALL: TPanel;
    PanelEmcon: TPanel;
    ScrollBox2: TScrollBox;
    Bevel17: TBevel;
    Bevel18: TBevel;
    Label562: TLabel;
    sbEmconAllSystemsAllSilent: TSpeedButton;
    sbEmconAllSystemsClearAll: TSpeedButton;
    btnEmconDistributeToGroup: TButton;
    cbEmconAcousticDecoys: TCheckBox;
    cbEmconActiveSonar: TCheckBox;
    cbEmconFireControl: TCheckBox;
    cbEmconHFComm: TCheckBox;
    cbEmconHFDatalink: TCheckBox;
    cbEmconIFF: TCheckBox;
    cbEmconJammingSystems: TCheckBox;
    cbEmconLasers: TCheckBox;
    cbEmconSearchRadar: TCheckBox;
    cbEmconUWT: TCheckBox;
    cbEmconVHFUHFComm: TCheckBox;
    cbEmconVHFUHFDatalink: TCheckBox;
    cbxEmcon: TComboBox;
    pnlGroupAirbone: TPanel;
    Label87: TLabel;
    Bevel22: TBevel;
    sbEmconGroupAirboneEMCON: TSpeedButton;
    sbEmconGroupAirboneClear: TSpeedButton;
  private
    { Private declarations }
  public
    { Public declarations }
    procedure SetControlledObject(ctrlObj: TT3Track); override;
    procedure UpdateForm; override;
    procedure EmptyField; override;
  end;

var
  frmPanelEMCON: TfrmPanelEMCON;

implementation

{$R *.dfm}

{ TfrmPanelEMCON }

procedure TfrmPanelEMCON.EmptyField;
begin
  inherited;

end;

procedure TfrmPanelEMCON.SetControlledObject(ctrlObj: TT3Track);
begin
  inherited;

end;

procedure TfrmPanelEMCON.UpdateForm;
begin
  inherited;

end;

end.
