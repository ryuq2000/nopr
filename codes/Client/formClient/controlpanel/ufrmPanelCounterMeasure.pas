unit ufrmPanelCounterMeasure;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, ExtCtrls, ufrmControlled, ufrmECMType, uT3Track, uT3MountedECM,
  uT3Vehicle;

type
  TECMControlType = (ecAcousticDecoy, ecAirborneChaff, ecAirBubble, ecFLoatingDecoy,
      ecSelfDefense, ecRadarNoise, ecSurfaceChaff, ecTowedJammer,
      ecInfrared);

  TfrmPanelCM = class(TfrmControlled)
    PanelCounterMeasureChoice: TPanel;
    lvECM: TListView;
    procedure lvECMSelectItem(Sender: TObject; Item: TListItem;
      Selected: Boolean);
  private
    { Private declarations }
    FECMControl : TfrmECMType;

    procedure addECM(aECM : TT3MountedECM);

    function createECMControl(aType : TECMControlType) : TfrmECMType;
    procedure RefreshPlatformECMs(Pf: TT3Vehicle);
    procedure updatePlatformECMs(Pf: TT3Vehicle);
  public
    { Public declarations }
    procedure SetControlledObject(ctrlObj: TT3Track); override;
    procedure UpdateForm; override;
    procedure EmptyField; override;
  end;

var
  frmPanelCM: TfrmPanelCM;

implementation

uses
  ufrmAcousticDecoy, ufrmAirborneChaff,
  ufrmAirBubble, ufrmFloatingDecoy,
  ufrmOnBoardSelfDefenseJammer,
  ufrmRadarNoiseJammer,
  ufrmSurfaceChaffDeployment,
  ufrmTowedJammerDecoy,

  uGlobalVar,

  uT3MountedAcousticDeploy, uT3MountedAirborneChaff,
  uT3MountedDefensiveJammer, uT3MountedFloatingDecoy,
  uT3MountedInfrared, uT3MountedRadarNoiseJammer,
  uT3MountedTowedJammer, uT3MountedSurfaceChaff,
  uT3MountedChaff, uT3MountedAirBubble;

{$R *.dfm}


{ TfrmPanelCM }

const
  AcousticMode : array [0..3] of string =
                 ('Swept Frequency','Noise','Pulsed Noise','Alternating');

  ECMStatus    : array [0..10] of string =
                 ('Available', 'Launching Chaff', 'Unavailable', 'Damaged', 'On',
                  'Off', 'EMCON', 'Automatic', 'Manual', 'Deployed', 'Stowed');

procedure TfrmPanelCM.addECM(aECM: TT3MountedECM);
var
  li      : TListItem;
begin
  if aECM is TT3MountedChaff then
  begin
    li := lvECM.Items.Add;
    li.Caption  := 'Chaff';
    li.Data     := aECM;

    li.SubItems.Add(ECMStatus[Byte(aECM.Status)]);

  end
//  else if device is TT3ChaffLauncher then
//  begin
//    item := TMenuItem.Create(Self);
//    item.Caption := TT3ChaffOnVehicle(device).InstanceName;
//    item.Tag := tagLauncher;
//    item.OnClick := MenuChaffLauncherSelected;
//    pmChaffLauncher.Items.Add(item);
//
//    Inc(tagLauncher);
//  end
  else
  begin
    li := lvECM.Items.Add;
    li.Caption  := aECM.InstanceName;
    li.Data     := aECM;

    li.SubItems.Add(ECMStatus[Byte(aECM.Status)]);
  end;
end;

function TfrmPanelCM.createECMControl(aType: TECMControlType): TfrmECMType;
begin
  result := nil;

  case aType of
    ecAcousticDecoy : result        := TfrmAcousticDecoy.Create(nil);
    ecAirborneChaff : result        := TfrmAirborneChaff.Create(nil);
    ecAirBubble     : result        := TfrmAirBubble.Create(nil);
    ecFLoatingDecoy : result        := TfrmFloatingDecoy.Create(nil);
    ecSelfDefense   : result        := TfrmOnBoardSelfDefenseJammer.Create(nil);
    ecRadarNoise    : result        := TfrmRadarNoiseJammer.Create(nil);
    ecSurfaceChaff  : result        := TfrmSurfaceChaffDeployment.Create(nil);
    ecTowedJammer   : result        := TfrmTowedJammerDecoy.Create(nil);
    ecInfrared      :;
  end;

  if Assigned(result) then
  begin
    result.Parent := Self;
    result.Align  := alClient;
    result.BorderStyle := bsNone;
    result.Show;
  end;

end;

procedure TfrmPanelCM.EmptyField;
begin
  inherited;

  { set empty }
  if ASsigned( FECMControl ) then
    FECMControl.Free;
  FECMControl := nil;

end;

procedure TfrmPanelCM.lvECMSelectItem(Sender: TObject; Item: TListItem;
  Selected: Boolean);
var
  ecm : TObject;
begin

  ecm := Item.Data;

  if ecm = nil then
  begin
    EmptyField;
    exit;
  end;

  EmptyField;

  if not Assigned(FECMControl) then
  begin
    if (ecm is TT3MountedAirborneChaff)  then
    begin
      EmptyField;
      FECMControl := createECMControl(ecAirborneChaff);
    end
    else
    if (ecm is TT3MountedSurfaceChaff)  then
    begin
      EmptyField;
      FECMControl := createECMControl(ecSurfaceChaff);
    end
    else
    if (ecm is TT3MountedInfrared)  then
    begin
      EmptyField;
      FECMControl := createECMControl(ecInfrared);
    end
    else
    if (ecm is TT3MountedTowedJammer)  then
    begin
      EmptyField;
      FECMControl := createECMControl(ecTowedJammer);
    end
    else
    if (ecm is TT3MountedRadarNoiseJammer)  then
    begin
      EmptyField;
      FECMControl := createECMControl(ecRadarNoise);
    end
    else
    if (ecm is TT3MountedFloatingDecoy)  then
    begin
      EmptyField;
      FECMControl := createECMControl(ecFLoatingDecoy);
    end
    else
    if (ecm is TT3MountedDefensiveJammer)  then
    begin
      EmptyField;
      FECMControl := createECMControl(ecSelfDefense);
    end
    else
    if (ecm is TT3MountedAcousticDeploy)  then
    begin
      EmptyField;
      FECMControl := createECMControl(ecAcousticDecoy);
    end
    else
    if (ecm is TT3MountedAirbubble)  then
    begin
      EmptyField;
      FECMControl := createECMControl(ecAirBubble);
    end
    else
      EmptyField
  end;

  if Assigned(FECMControl) then
    FECMControl.SetControlledECM(ecm);

end;

procedure TfrmPanelCM.RefreshPlatformECMs(Pf: TT3Vehicle);
var
  ecm : TT3MountedECM;
  i : integer;
begin
  lvECM.Clear;

  { add acoustic }
  for I := 0 to simManager.SimAcousticDecoysOnBoards.ItemCount - 1 do
  begin
    ecm := simManager.SimAcousticDecoysOnBoards.getObject(I) as TT3MountedECM;

    if ecm.PlatformParentInstanceIndex = Pf.InstanceIndex then
      addECM(ecm);
  end;

  { add air bubble }
  for I := 0 to simManager.SimAirBubblesOnBoards.ItemCount - 1 do
  begin
    ecm := simManager.SimAirBubblesOnBoards.getObject(I) as TT3MountedECM;

    if ecm.PlatformParentInstanceIndex = Pf.InstanceIndex then
      addECM(ecm);
  end;

  { add floating decoy }
  for I := 0 to simManager.SimFloatingDecoysOnBoards.ItemCount - 1 do
  begin
    ecm := simManager.SimFloatingDecoysOnBoards.getObject(I) as TT3MountedECM;

    if ecm.PlatformParentInstanceIndex = Pf.InstanceIndex then
      addECM(ecm);
  end;

  { add self defense }
  for I := 0 to simManager.SimJammersOnBoards.ItemCount - 1 do
  begin
    ecm := simManager.SimJammersOnBoards.getObject(I) as TT3MountedECM;

    if ecm.PlatformParentInstanceIndex = Pf.InstanceIndex then
      addECM(ecm);
  end;

  { add self defense }
  for I := 0 to simManager.SimDefensiveJammersOnBoards.ItemCount - 1 do
  begin
    ecm := simManager.SimDefensiveJammersOnBoards.getObject(I) as TT3MountedECM;

    if ecm.PlatformParentInstanceIndex = Pf.InstanceIndex then
      addECM(ecm);
  end;

  { add towed jammer }
  for I := 0 to simManager.SimTowedJammersOnBoards.ItemCount - 1 do
  begin
    ecm := simManager.SimTowedJammersOnBoards.getObject(I) as TT3MountedECM;

    if ecm.PlatformParentInstanceIndex = Pf.InstanceIndex then
      addECM(ecm);
  end;

  { add chaff }
  for I := 0 to simManager.SimChaffsOnBoards.ItemCount - 1 do
  begin
    ecm := simManager.SimChaffsOnBoards.getObject(I) as TT3MountedECM;

    if ecm.PlatformParentInstanceIndex = Pf.InstanceIndex then
      addECM(ecm);
  end;

end;

procedure TfrmPanelCM.SetControlledObject(ctrlObj: TT3Track);
begin
  { if same with previous platform, ignore it}
  if ASsigned(FControlledTrack) and FControlledTrack.Equals(ctrlObj) then
    Exit;

  inherited;

  { set empty }
  EmptyField;

  lvECM.Clear;

  { fill platform sensor list }
  if FControlledPlatform is TT3Vehicle then
    RefreshPlatformECMs(TT3Vehicle(FControlledPlatform));


end;

procedure TfrmPanelCM.UpdateForm;
begin
  inherited;
  { fill platform sensor list -> must be fixed }
  if FControlledPlatform is TT3Vehicle then
    updatePlatformECMs(TT3Vehicle(FControlledPlatform));

  if Assigned(FECMControl) then
    FECMControl.UpdateForm;

end;

procedure TfrmPanelCM.updatePlatformECMs(Pf: TT3Vehicle);
var
  i: integer;
  li: TListItem;
  data : TObject;
begin

  for i := 0 to lvECM.Items.Count - 1 do
  begin
    li := lvECM.Items[i];
    data := li.Data;
    if Assigned(data) then
    begin
      case TT3MountedECM(data).Status of
        esAvailable:
          li.SubItems[0] := 'Available';
        esLaunchingChaff:
          li.SubItems[0] := 'Launcing Chaff';
        esUnavailable:
          li.SubItems[0] := 'Unavailable';
        esDamaged:
          li.SubItems[0] := 'Damage';
        esOn:
          li.SubItems[0] := 'On';
        esOff:
          li.SubItems[0] := 'Off';
        esEMCON:
          li.SubItems[0] := 'EMCON';
        esAutomatic:
          li.SubItems[0] := 'Automatic';
        esManual:
          li.SubItems[0] := 'Manual';
        esDeployed:
          li.SubItems[0] := 'Deployed';
        esStowed:
          li.SubItems[0] := 'Stowing';
      end;
    end;
  end;
end;

end.
