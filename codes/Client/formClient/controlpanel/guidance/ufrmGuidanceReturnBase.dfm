object frmGuidanceReturnBase: TfrmGuidanceReturnBase
  Left = 0
  Top = 0
  Caption = 'Return To Base'
  ClientHeight = 300
  ClientWidth = 307
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object grpReturnToBase: TGroupBox
    Left = 0
    Top = 0
    Width = 307
    Height = 300
    Align = alClient
    Color = clBtnFace
    ParentBackground = False
    ParentColor = False
    TabOrder = 0
    object lbl1: TLabel
      Left = 10
      Top = 26
      Width = 80
      Height = 13
      Caption = 'Destination Base'
    end
    object lblReturnToBaseDestinationBase: TLabel
      Left = 135
      Top = 26
      Width = 78
      Height = 13
      Caption = 'van Galen F 834'
    end
    object lbl2: TLabel
      Left = 10
      Top = 55
      Width = 111
      Height = 13
      Caption = 'Ordered Ground Speed'
    end
    object lbl3: TLabel
      Left = 235
      Top = 55
      Width = 26
      Height = 13
      Caption = 'knots'
    end
    object lbl4: TLabel
      Left = 10
      Top = 80
      Width = 101
      Height = 13
      Caption = 'Actual Ground Speed'
    end
    object lbl5: TLabel
      Left = 235
      Top = 80
      Width = 26
      Height = 13
      Caption = 'knots'
    end
    object lblReturnToBaseActualGroundSpeed: TLabel
      Left = 135
      Top = 80
      Width = 22
      Height = 13
      Caption = '14.0'
    end
    object lbl6: TLabel
      Left = 10
      Top = 111
      Width = 80
      Height = 13
      Caption = 'Ordered Altitude'
    end
    object lbl7: TLabel
      Left = 233
      Top = 111
      Width = 28
      Height = 13
      Caption = 'meter'
    end
    object lbl8: TLabel
      Left = 10
      Top = 136
      Width = 70
      Height = 13
      Caption = 'Actual Altitude'
    end
    object lbl9: TLabel
      Left = 233
      Top = 136
      Width = 28
      Height = 13
      Caption = 'meter'
    end
    object lblReturnToBaseActualAltitude: TLabel
      Left = 135
      Top = 136
      Width = 22
      Height = 13
      Caption = '14.0'
    end
    object txt1: TStaticText
      Left = 125
      Top = 24
      Width = 8
      Height = 17
      Caption = ':'
      TabOrder = 0
    end
    object txt2: TStaticText
      Left = 125
      Top = 53
      Width = 8
      Height = 17
      Caption = ':'
      TabOrder = 1
    end
    object edtReturnToBaseOrderedGroundSpeed: TEdit
      Left = 135
      Top = 51
      Width = 74
      Height = 21
      MaxLength = 11
      TabOrder = 2
      OnKeyPress = edtReturnToBaseOrderedGroundSpeedKeyPress
    end
    object txt3: TStaticText
      Left = 125
      Top = 78
      Width = 8
      Height = 17
      Caption = ':'
      TabOrder = 3
    end
    object txt4: TStaticText
      Left = 125
      Top = 111
      Width = 8
      Height = 17
      Caption = ':'
      TabOrder = 4
    end
    object edtReturnToBaseOrderedAltitude: TEdit
      Tag = 1
      Left = 135
      Top = 107
      Width = 74
      Height = 21
      TabOrder = 5
    end
    object txt5: TStaticText
      Left = 125
      Top = 134
      Width = 8
      Height = 17
      Caption = ':'
      TabOrder = 6
    end
    object pnlCoverAltitudeReturnToBase: TPanel
      Left = 3
      Top = 100
      Width = 364
      Height = 54
      BevelOuter = bvNone
      Enabled = False
      ParentBackground = False
      TabOrder = 7
    end
  end
end
