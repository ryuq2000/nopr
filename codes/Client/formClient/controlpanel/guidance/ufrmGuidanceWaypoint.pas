unit ufrmGuidanceWaypoint;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, ufrmGuidanceType, Math, DateUtils, uT3Track;

type
  TfrmGuidanceWaypoint = class(TfrmGuidanceType)
    grpWaypoint: TGroupBox;
    lbl1: TLabel;
    lblName: TLabel;
    lblLongitude: TLabel;
    lbl2: TLabel;
    lblGroundSpeed: TLabel;
    lbl3: TLabel;
    lblEta: TLabel;
    lbl4: TLabel;
    lbl5: TLabel;
    lbl6: TLabel;
    lblDistance: TLabel;
    lbl7: TLabel;
    lbl8: TLabel;
    lbl9: TLabel;
    lblLatitude: TLabel;
    lblActualGroundWaypoint: TLabel;
    bvl1: TBevel;
    bvl2: TBevel;
    txt1: TStaticText;
    txt2: TStaticText;
    txt3: TStaticText;
    btnWaypoint: TButton;
    txt4: TStaticText;
    txt5: TStaticText;
    txt6: TStaticText;
    txt7: TStaticText;
    txt8: TStaticText;
    txt9: TStaticText;
    txt10: TStaticText;
    edtWaypointOrderedGroundSpeed: TEdit;
    procedure edtWaypointOrderedGroundSpeedKeyPress(Sender: TObject;
      var Key: Char);
    procedure btnWaypointClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure UpdateForm; override;
    procedure SetControlledObject(aObj : TT3Track); override;
  end;

var
  frmGuidanceWaypoint: TfrmGuidanceWaypoint;

implementation

uses uT3Vehicle, uBaseCoordSystem, uT3WaypointGuide,
  uGameData_TTT, uT3ClientManager, {uWaypointEditor,}
  uT3PlatformInstance;

{$R *.dfm}

{ TfrmGuidanceWaypoint }

procedure TfrmGuidanceWaypoint.btnWaypointClick(Sender: TObject);
begin
//  if Assigned(FControlledTrack) and not Assigned(frmWaypointEditor) then
//  begin
//    frmWaypointEditor := TfrmWaypointEditor.Create(nil);
//    frmWaypointEditor.HookedTrack := FControlledTrack;
//    frmWaypointEditor.Show;
//  end;

end;

procedure TfrmGuidanceWaypoint.edtWaypointOrderedGroundSpeedKeyPress(
  Sender: TObject; var Key: Char);
var
  ValKey : set of AnsiChar;
  Speed : double;
  v : TT3Vehicle;
  rec: TRecCmd_PlatformGuidance;
begin
  ValKey := [#48 .. #57, #8, #13, #46];
  if not(CharInSet(Key, ValKey)) then
    Key := #0;

  if Key = #13 then
  begin
    if Assigned(FControlledPlatform) and (FControlledPlatform is TT3Vehicle) then
    begin
      v := FControlledPlatform as TT3Vehicle;

      TryStrToFloat(edtWaypointOrderedGroundSpeed.Text, Speed);
      { TODO -oRyu : Embarkedplatform }
//      if frmTacticalDisplay.ControlEmbarkedPlatform then
//      begin
//        if Assigned(frmLaunchPlaform) then
//        begin
//          frmLaunchPlaform.EmbarkedSpeed := Speed;
//          exit;
//        end;
//      end;
      { TODO -oRyu : FuelRemaining }
//      if (v.FuelRemaining <= 0 ) then
//      begin
//        if simMgrClient.ISInstructor then
//          frmTacticalDisplay.addStatus(v.TrackLabel + ' Out of Fuel')
//        else
//          frmTacticalDisplay.addStatus(IntToStr(v.TrackNumber)+ ' Out of Fuel');
//        Exit;
//      end;

      if (Speed > v.Mover.MaxSpeed) then
      begin
        edtWaypointOrderedGroundSpeed.Text := FloatToStr(v.Mover.MaxSpeed);
        TacticalDisplayStatus(TT3Track(FControlledTrack).TrackLabelInfo + ' Over Speed = '+FloatToStr(Speed));
        Speed := v.Mover.MaxSpeed;     //mk test
      end
      else if (Speed < v.Mover.MinSpeed) then //choco
      begin
        edtWaypointOrderedGroundSpeed.Text := FloatToStr(v.Mover.MinSpeed);
        TacticalDisplayStatus(TT3Track(FControlledTrack).TrackLabelInfo + ' MIN SPEED');
        Speed := v.Mover.MinSpeed;
      end;

       //------------ send record speed straightline ------------------//
      rec.PlatfomID := v.InstanceIndex;
      rec.OrderID := CORD_ID_MOVE;
      rec.OrderType := CORD_TYPE_SPEED;
      rec.OrderParam := Speed;

      clientManager.NetCmdSender.CmdPlatformGuidance(rec);
    end;
  end;
end;

procedure TfrmGuidanceWaypoint.SetControlledObject(aObj: TT3Track);
begin
  inherited;

  if (FControlledPlatform = nil) then
    exit;

  if (FControlledPlatform is TT3Vehicle) then
    with TT3Vehicle(FControlledPlatform) do
      edtWaypointOrderedGroundSpeed.Text := FormatSpeed(OrderedSpeed);
end;

procedure TfrmGuidanceWaypoint.UpdateForm;
  function FormatNumber(val : integer) : string;
  var tmp : string;
  begin
    tmp := IntToStr(val);
    if Length(tmp) < 2 then tmp := '0' + tmp;
    result := tmp;
  end;
var
  range : double;
  timeToGo : integer;
begin
  inherited;

  if (FControlledPlatform is TT3Vehicle) then
  begin
    with TT3Vehicle(FControlledPlatform) do
    begin

//      if Assigned(Waypoints.NextWaypoint) then
//      begin
//        lblName.Caption        := 'Waypoint ' + IntToStr(Waypoints.NextWaypoint.FData.Scripted_Event_Index);
//        lblLongitude.Caption  := formatDMS_long(Waypoints.NextWaypoint.FData.Waypoint_Longitude);
//        lblLatitude.Caption   := formatDMS_latt(Waypoints.NextWaypoint.FData.Waypoint_Latitude);
//        lblGroundSpeed.Caption := FormatSpeed(Waypoints.NextWaypoint.FData.Speed);
//
//        range := CalcRange(Waypoints.NextWaypoint.FData.Waypoint_Longitude,
//                           Waypoints.NextWaypoint.FData.Waypoint_Latitude,
//                           PosX, PosY);
//
//        lblDistance.Caption    := FormatFloat('000.0', range);
//
//
//        if Speed > 0 then
//        begin
//          range := range + CalcRange(PosX,PosY,
//            Waypoints.NextWaypoint.FData.Waypoint_Longitude,
//            Waypoints.NextWaypoint.FData.Waypoint_Latitude);
//
//          timeToGo := Ceil((range / Speed) * 3600);
//
//          lblEta.Caption := FormatDateTime('ddhhnnss',
//            IncSecond(clientManager.GameTIME, timeToGo)) + 'Z' +
//            FormatDateTime('mmmyyyy', IncSecond(clientManager.GameTIME, timeToGo));
//        end
//        else
//          lblEta.Caption := '---';
//      end
//      else
//      begin
//        lblName.Caption        := '---';
//        lblLongitude.Caption  := '---';
//        lblLatitude.Caption   := '---';
//        lblGroundSpeed.Caption := '0.0';
//        lblDistance.Caption    := '000';
//        lblEta.Caption         := '---';
//      end;
//
//      lblActualGroundWaypoint.Caption  := FormatSpeed(Speed);
    end;
  end;
end;

end.
