object frmGuidanceFormation: TfrmGuidanceFormation
  Left = 0
  Top = 0
  Caption = 'Formation'
  ClientHeight = 403
  ClientWidth = 345
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object grpFormation: TGroupBox
    Left = 0
    Top = 0
    Width = 345
    Height = 403
    Align = alClient
    Color = clBtnFace
    ParentBackground = False
    ParentColor = False
    TabOrder = 0
    object lbl1: TLabel
      Left = 10
      Top = 26
      Width = 27
      Height = 13
      Caption = 'Name'
    end
    object lbl2: TLabel
      Left = 10
      Top = 47
      Width = 33
      Height = 13
      Caption = 'Leader'
    end
    object lblLeaderFormation: TLabel
      Left = 141
      Top = 47
      Width = 64
      Height = 13
      Caption = 'Invicible R 05'
    end
    object lblNameFormation: TLabel
      Left = 141
      Top = 26
      Width = 18
      Height = 13
      Caption = 'TF1'
    end
    object lbl3: TLabel
      Left = 10
      Top = 79
      Width = 36
      Height = 13
      Caption = 'Bearing'
    end
    object lbl4: TLabel
      Left = 10
      Top = 100
      Width = 31
      Height = 13
      Caption = 'Range'
    end
    object lbl5: TLabel
      Left = 221
      Top = 100
      Width = 14
      Height = 13
      Caption = 'nm'
    end
    object lblRangeFormation: TLabel
      Left = 141
      Top = 100
      Width = 22
      Height = 13
      Caption = '0.00'
    end
    object lblBearingFormation: TLabel
      Left = 141
      Top = 79
      Width = 18
      Height = 13
      Caption = '000'
    end
    object lbl6: TLabel
      Left = 221
      Top = 79
      Width = 48
      Height = 13
      Caption = 'degrees T'
    end
    object pnlDepth: TPanel
      Left = 2
      Top = 116
      Width = 319
      Height = 24
      BevelOuter = bvNone
      TabOrder = 5
      Visible = False
      object lblFormationDepth: TLabel
        Left = 140
        Top = 8
        Width = 12
        Height = 13
        Caption = '---'
      end
      object lbl7: TLabel
        Left = 219
        Top = 9
        Width = 8
        Height = 13
        Caption = 'm'
      end
      object txt1: TStaticText
        Left = 7
        Top = 5
        Width = 66
        Height = 17
        Caption = 'Actual Depth'
        TabOrder = 0
      end
      object txt2: TStaticText
        Left = 120
        Top = 6
        Width = 8
        Height = 17
        Caption = ':'
        TabOrder = 1
      end
    end
    object pnlAltitude: TPanel
      Left = 3
      Top = 115
      Width = 319
      Height = 26
      BevelOuter = bvNone
      TabOrder = 4
      Visible = False
      object lblFormationAltitude: TLabel
        Left = 138
        Top = 8
        Width = 12
        Height = 13
        Caption = '---'
      end
      object lbl8: TLabel
        Left = 218
        Top = 9
        Width = 20
        Height = 13
        Caption = 'feet'
      end
      object txt3: TStaticText
        Left = 6
        Top = 4
        Width = 74
        Height = 17
        Caption = 'Actual Altitude'
        TabOrder = 0
      end
      object txt4: TStaticText
        Left = 118
        Top = 6
        Width = 8
        Height = 17
        Caption = ':'
        TabOrder = 1
      end
    end
    object txt5: TStaticText
      Left = 122
      Top = 24
      Width = 8
      Height = 17
      Caption = ':'
      TabOrder = 0
    end
    object txt6: TStaticText
      Left = 122
      Top = 45
      Width = 8
      Height = 17
      Caption = ':'
      TabOrder = 1
    end
    object txt7: TStaticText
      Left = 122
      Top = 77
      Width = 8
      Height = 17
      Caption = ':'
      TabOrder = 2
    end
    object txt8: TStaticText
      Left = 122
      Top = 98
      Width = 8
      Height = 17
      Caption = ':'
      TabOrder = 3
    end
  end
end
