unit ufmGuidanceHelm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, 
  Dialogs, ufmGuidanceType, uT3Track, StdCtrls, ExtCtrls, VrControls, VrWheel;

type
  TfmGuidanceHelm = class(TfmGuidanceType)
    grpHelm: TGroupBox;
    lbl1: TLabel;
    lblHelmActualTurnRate: TLabel;
    lbl2: TLabel;
    lbl3: TLabel;
    lbl4: TLabel;
    lbl5: TLabel;
    lbl6: TLabel;
    lbl7: TLabel;
    lblHelmActualGroundSpeed: TLabel;
    img1: TImage;
    lbl8: TLabel;
    lblActualHelmAngle: TLabel;
    lblHelmActualHeading: TLabel;
    lbl9: TLabel;
    lblHelmActualCourse: TLabel;
    lbl10: TLabel;
    lblHelmActualDepth: TLabel;
    wheelHelmAngle: TVrWheel;
    lbl11: TLabel;
    lbl12: TLabel;
    pnl2: TPanel;
    lbl17: TLabel;
    lbl18: TLabel;
    lbl19: TLabel;
    lblActuaAltitudeHelm: TLabel;
    edtEdtHelmOrderedAltitude: TEdit;
    txt15: TStaticText;
    txt16: TStaticText;
    txt17: TStaticText;
    pnl1: TPanel;
    lbl13: TLabel;
    lbl14: TLabel;
    lbl15: TLabel;
    lbl16: TLabel;
    edtHelmOrderedDepth: TEdit;
    txt1: TStaticText;
    txt2: TStaticText;
    txt3: TStaticText;
    txt4: TStaticText;
    txt5: TStaticText;
    txt6: TStaticText;
    txt7: TStaticText;
    txt8: TStaticText;
    txt9: TStaticText;
    txt10: TStaticText;
    edtOrderedHelmAngle: TEdit;
    txt11: TStaticText;
    txt12: TStaticText;
    txt13: TStaticText;
    txt14: TStaticText;
    edtHelmOrderedGroundSpeed: TEdit;
    procedure wheelHelmAngleChange(Sender: TObject);
    procedure wheelHelmAngleMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure OnKeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure EmptyField; override;
    procedure UpdateForm; override;
    procedure SetControlledObject(aObj : TT3Track); override;
  end;

implementation

uses uT3Vehicle, uT3HelmGuide, uBaseCoordSystem, uGameData_TTT,
  uT3ClientManager;
{$R *.dfm}

{ TfmGuidanceHelm }

procedure TfmGuidanceHelm.EmptyField;
begin
  inherited;

end;

procedure TfmGuidanceHelm.OnKeyPress(Sender: TObject; var Key: Char);
var
  ValKey : set of AnsiChar;
  val : double;
  max_angle, min_angle : double;
  v : TT3Vehicle;
  rec: TRecCmd_Platform;
begin

  ValKey := [#48 .. #57, #8, #13, #46];
  if not(CharInSet(Key, ValKey)) then
    Key := #0;

  if Key = #13 then
  begin
    case TComponent(Sender).Tag of
      1 :
      begin
        TryStrToFloat(TEdit(Sender).Text, val);

        v := FControlledPlatform as TT3Vehicle;

        max_angle := v.PlatformMotion.FData.Max_Helm_Angle;
        min_angle := - v.PlatformMotion.FData.Max_Helm_Angle;

        if(val > max_angle)then
          val := max_angle
        else if (val < min_angle) then
          val := min_angle;

        rec.PlatfomID   := v.InstanceIndex;
        rec.OrderID     := CORD_ID_MOVE;
        rec.OrderType   := CORD_TYPE_COURSE;
        rec.OrderParam  := val;

        clientManager.NetCmdSender.CmdPlatform(rec);
      end;
      2 : OnOrderGroundSpeedChange(Sender);
      3 : OnOrderAltitudeChange(Sender,'f');
      4 : OnOrderAltitudeChange(Sender,'m');
    end;
  end;
end;

procedure TfmGuidanceHelm.SetControlledObject(aObj: TT3Track);
begin
  inherited;

  if (FControlledPlatform <> nil) and (FControlledPlatform is TT3Vehicle) then
  begin
    with TT3Vehicle(FControlledPlatform) do
    begin
      edtHelmOrderedGroundSpeed.Text  := FormatSpeed(OrderedSpeed);
      edtEdtHelmOrderedAltitude.Text  := FormatAltitude(OrderedAltitude / C_Feet_To_Meter);
      edtHelmOrderedDepth.Text        := FormatAltitude(OrderedAltitude);

      if VehicleGuidance is TT3HelmGuidance then
      begin
        if TT3HelmGuidance(VehicleGuidance).FirstHeading > 0 then
        begin
          edtOrderedHelmAngle.Text  := IntToStr(round(TT3HelmGuidance(VehicleGuidance).FirstHeading));
          wheelHelmAngle.Position   := Round(TT3HelmGuidance(VehicleGuidance).FirstHeading + 180);
        end
        else
        begin
          edtOrderedHelmAngle.Text  := IntToStr(round(TT3HelmGuidance(VehicleGuidance).FirstHeading));
          wheelHelmAngle.Position   := Round(180 - (-1 * TT3HelmGuidance(VehicleGuidance).FirstHeading));
        end;
      end;
    // --------------------------------------------------------------

      case VehicleDefinition.FData.Platform_Domain of
        0 :
          begin
            pnl1.Visible := false;
            pnl2.Visible := true;
          end;
        1:
          begin
            pnl1.Visible := false;
            pnl2.Visible := false;
          end;
        2:
          begin
            pnl1.Visible := true;
            pnl2.Visible := false;
          end;
        3:
          begin
            pnl1.Visible := false;
            pnl2.Visible := false;
          end;
      end;
    end;
  end;
end;

procedure TfmGuidanceHelm.UpdateForm;
var
  turnrate : Double;
begin
  inherited;

  if (FControlledPlatform <> nil) and (FControlledPlatform is TT3Vehicle) then
  begin
    with TT3Vehicle(FControlledPlatform) do
    begin
      if VehicleGuidance is TT3HelmGuidance then
        with TT3HelmGuidance(VehicleGuidance) do
        begin
          turnrate := HelmDegree * PlatformMotion.FData.Tight_Turn_Rate /
                       PlatformMotion.FData.Max_Helm_Angle;

          lblActualHelmAngle.Caption      := FormatCourse((FirstHeading));
          lblHelmActualTurnRate.Caption   := FormatFloat('000.0', abs(turnrate));
          lblHelmActualHeading.Caption    := FormatCourse(Heading);
          lblHelmActualCourse.Caption     := FormatCourse(Course);

          lblHelmActualGroundSpeed.Caption:= FormatSpeed(Speed);
          lblActuaAltitudeHelm.Caption    := FormatAltitude(Altitude / C_Feet_To_Meter);
          lblActuaAltitudeHelm.Caption    := FormatAltitude(Altitude);
        end;
    end;
  end;
end;

procedure TfmGuidanceHelm.wheelHelmAngleChange(Sender: TObject);
var
  newheading: integer;
  max_angle : single;
  left : Boolean;
begin
  if (FControlledPlatform <> nil) and (FControlledPlatform is TT3Vehicle) then
  begin
    with TT3Vehicle(FControlledPlatform) do
    begin
      max_angle := PlatformMotion.FData.Max_Helm_Angle;
      if wheelHelmAngle.Position < 180 then  //sebelah kiri
      begin
        newheading := (180 - wheelHelmAngle.Position);
        lbl7.Caption := 'degrees Port';
        left := True;
      end
      else //sebelah kanan
      begin
        newheading := (wheelHelmAngle.Position - 180);
        lbl7.Caption := 'degrees Starboard';
        left := False;
      end;

      if(newheading > max_angle)then
        begin
          edtOrderedHelmAngle.Text := FloatToStr(max_angle);
          if(lbl7.Caption = 'degrees Port')then
            wheelHelmAngle.Position := (180 - round(max_angle))
          else
            wheelHelmAngle.Position := (180 + round(max_angle)) ;
        end
      else
      begin
        if left then
          edtOrderedHelmAngle.Text := IntToStr(newheading) // 04/ 04/ 2012
        else
          edtOrderedHelmAngle.Text := IntToStr(newheading);
      end;
    end;
  end;
end;

procedure TfmGuidanceHelm.wheelHelmAngleMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  v : TT3Vehicle;
  newheading : Double;
  max_angle, min_angle : single;
  rec: TRecCmd_Platform;
begin
  if (FControlledPlatform <> nil) and (FControlledPlatform is TT3Vehicle) then
  begin
    v := FControlledPlatform as TT3Vehicle;

    with TT3Vehicle(FControlledPlatform) do
    begin
      max_angle := PlatformMotion.FData.Max_Helm_Angle;
      min_angle := - PlatformMotion.FData.Max_Helm_Angle;

      {Putar ke Kiri}
      if wheelHelmAngle.Position < 180 then
      begin
        newheading := -1 * (180 - wheelHelmAngle.Position);

        if newheading <  min_angle then
          newheading := min_angle
        else
          newheading := newheading;
      end
      {Putar ke Kanan}
      else
      begin
        newheading := (wheelHelmAngle.Position - 180);

        if newheading > max_angle then
          newheading := max_angle
        else
          newheading := newheading;
      end;

      rec.PlatfomID   := v.InstanceIndex;
      rec.OrderID     := CORD_ID_MOVE;
      rec.OrderType   := CORD_TYPE_COURSE;
      rec.OrderParam  := newheading;

      clientManager.NetCmdSender.CmdPlatform(rec);

    end;
  end;
end;

end.
