unit ufrmGuidanceReturnBase;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls,ufrmGuidanceType, uT3Track;

type
  TfrmGuidanceReturnBase = class(TfrmGuidanceType)
    grpReturnToBase: TGroupBox;
    lbl1: TLabel;
    lblReturnToBaseDestinationBase: TLabel;
    lbl2: TLabel;
    lbl3: TLabel;
    lbl4: TLabel;
    lbl5: TLabel;
    lblReturnToBaseActualGroundSpeed: TLabel;
    lbl6: TLabel;
    lbl7: TLabel;
    lbl8: TLabel;
    lbl9: TLabel;
    lblReturnToBaseActualAltitude: TLabel;
    txt1: TStaticText;
    txt2: TStaticText;
    edtReturnToBaseOrderedGroundSpeed: TEdit;
    txt3: TStaticText;
    txt4: TStaticText;
    edtReturnToBaseOrderedAltitude: TEdit;
    txt5: TStaticText;
    pnlCoverAltitudeReturnToBase: TPanel;
    procedure edtReturnToBaseOrderedGroundSpeedKeyPress(Sender: TObject;
      var Key: Char);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure UpdateForm; override;
    procedure SetControlledObject(aObj : TT3Track); override;
  end;

var
  frmGuidanceReturnBase: TfrmGuidanceReturnBase;

implementation

uses uT3Vehicle, uBaseCoordSystem, uGameData_TTT, uT3EvasionGuide,
  uT3ClientManager, uT3PlatformInstance;

{$R *.dfm}

{ TfrmGuidanceReturnBase }

procedure TfrmGuidanceReturnBase.edtReturnToBaseOrderedGroundSpeedKeyPress(
  Sender: TObject; var Key: Char);
var
  ValKey : set of AnsiChar;
  val : Double;
begin
  inherited;

  ValKey := [#48 .. #57, #8, #13, #46];
  if not(CharInSet(Key, ValKey)) then
    Key := #0;

  if Key = #13 then
    OnOrderGroundSpeedChange(Sender);
end;

procedure TfrmGuidanceReturnBase.SetControlledObject(aObj: TT3Track);
var
  v : TT3PlatformInstance;
begin
  inherited;
  if (FControlledPlatform <> nil) and (FControlledPlatform is TT3Vehicle) then
  begin
    with TT3Vehicle(FControlledPlatform) do
    begin
//      if Assigned(FSelectedTrack) then
//      begin
//        if FSelectedTrack = FControlledTrack then
//          Exit;
//
//        v := clientManager.FindT3PlatformByID(FSelectedTrack.ObjectInstanceIndex);
//        if Assigned(v) and ( v is TT3PlatformInstance ) then
//        begin
//          lblReturnToBaseDestinationBase.Caption := v.InstanceName;
//        end;
//      end;
//
//      lblReturnToBaseActualGroundSpeed.Caption  := FormatSpeed(Speed);
//      lblReturnToBaseActualAltitude.Caption := FormatAltitude(Altitude);
    end;
  end;

end;

procedure TfrmGuidanceReturnBase.UpdateForm;
begin
  inherited;
  if (FControlledPlatform <> nil) and (FControlledPlatform is TT3Vehicle) then
  begin
    with TT3Vehicle(FControlledPlatform) do
    begin
      lblReturnToBaseActualGroundSpeed.Caption  := FormatSpeed(Speed);
      lblReturnToBaseActualAltitude.Caption := FormatAltitude(Altitude);
    end;
  end;
end;

end.
