unit ufrmGuidanceOutrun;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons,ufrmGuidanceType, uT3Track;

type
  TfrmGuidanceOutrun = class(TfrmGuidanceType)
    grpOutrun: TGroupBox;
    lbl1: TLabel;
    lbl2: TLabel;
    lbl3: TLabel;
    lblOutrunActualGroundSpeed: TLabel;
    btnTrackToOutrun: TSpeedButton;
    lbl4: TLabel;
    lblOutrunAltitudeDepth: TLabel;
    lbl5: TLabel;
    txt1: TStaticText;
    edtTrackToOutrun: TEdit;
    txt2: TStaticText;
    txt3: TStaticText;
    procedure btnTrackToOutrunClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure UpdateForm; override;
    procedure SetControlledObject(aObj : TT3Track); override;
  end;

var
  frmGuidanceOutrun: TfrmGuidanceOutrun;

implementation

uses uT3Vehicle, uBaseCoordSystem, uGameData_TTT, uT3EvasionGuide,
  uT3ClientManager;


{$R *.dfm}

{ TfrmGuidanceOutrun }

procedure TfrmGuidanceOutrun.btnTrackToOutrunClick(Sender: TObject);
var
  track : TT3Track;
  rec: TRecCmd_PlatformGuidance;
  v: TT3Vehicle;
  bearing, range : Double;
begin
  inherited;
  if (Assigned(FControlledPlatform)) and (FControlledPlatform is TT3Vehicle) then
  begin
    if Assigned(FSelectedTrack) then
    begin
      if FSelectedTrack = FControlledTrack then
        Exit;

      v := FControlledPlatform as TT3Vehicle;

      if Assigned(v.VehicleGuidance) and (v.VehicleGuidance is TT3EvasionGuidance) then
      begin
        edtTrackToOutrun.Text := FSelectedTrack.TrackLabelInfo;

//    bearing := CalcBearing(target.PosX, target.PosY, v.PosX, v.PosY);
//    randBearing := Random * 90;
//
//    if Boolean(Random(2)) then
//      bearing := bearing - randBearing
//    else
//      bearing := bearing + randBearing;
//
//    if bearing > 360 then
//      bearing := bearing - 360
//    else if bearing < 0 then
//      bearing := bearing + 360;
//
//    SimMgrClient.netSend_CmdPlatformGuidance(v.InstanceIndex, CORD_ID_MOVE,
//      CORD_TYPE_COURSE, bearing);
//
//    SimMgrClient.netSend_CmdPlatformGuidance(v.InstanceIndex, CORD_ID_MOVE,
//      CORD_TYPE_SPEED, v.Mover.MaxSpeed);
//
//    SimMgrClient.netSend_CmdPlatformGuidance(v.InstanceIndex, CORD_ID_MOVE,
//      CORD_TYPE_EVASION_TRACK, target.InstanceIndex);

        if FSelectedTrack.ObjectInstanceIndex > 0 then
        begin
          rec.PlatfomID   := v.InstanceIndex;
          rec.OrderID     := CORD_ID_MOVE;
          rec.OrderType   := CORD_TYPE_OUTRUN_TRACK;
          rec.OrderParam  := FSelectedTrack.ObjectInstanceIndex;

          clientManager.NetCmdSender.CmdPlatformGuidance(rec)
        end
        else
          TacticalDisplayStatus('Target is not valid');
      end
      else
        TacticalDisplayStatus('Target is not selected');
    end;
  end;
end;

procedure TfrmGuidanceOutrun.SetControlledObject(aObj: TT3Track);
begin
  inherited;

  if (FControlledPlatform <> nil) and (FControlledPlatform is TT3Vehicle) then
  begin
    with TT3Vehicle(FControlledPlatform) do
    begin
      lblOutrunActualGroundSpeed.Caption  := FormatSpeed(Speed);

      case PlatformDomain of
        0 : //air
        begin
          lbl4.Visible := True;
          txt3.Visible := True;
          lblOutrunAltitudeDepth.Visible := True;
          lbl5.Visible := True;

          lbl4.Caption := 'Actual Altitude';
          lbl5.Caption := 'feet';

          lblOutrunAltitudeDepth.Caption := FormatAltitude(Altitude * C_Meter_To_Feet);
        end;
        1,3,4 : //surface, land, amphibi
        begin
          lbl4.Visible := False;
          txt3.Visible := False;
          lblOutrunAltitudeDepth.Visible := False;
          lbl5.Visible := False;
        end;
        2 : //sub-surface
        begin
          lbl4.Visible := True;
          txt3.Visible := True;
          lblOutrunAltitudeDepth.Visible := True;
          lbl5.Visible := True;

          lbl4.Caption := 'Actual Depth';
          lbl5.Caption := 'metres';

          lblOutrunAltitudeDepth.Caption := FormatAltitude(Altitude);
        end;
      end;
    end;
  end;
end;

procedure TfrmGuidanceOutrun.UpdateForm;
begin
  inherited;

  if (FControlledPlatform <> nil) and (FControlledPlatform is TT3Vehicle) then
  begin
    with TT3Vehicle(FControlledPlatform) do
    begin
      lblOutrunActualGroundSpeed.Caption  := FormatSpeed(Speed);

      case PlatformDomain of
        0 : //air
        begin
          lblOutrunAltitudeDepth.Caption := FormatAltitude(Altitude * C_Meter_To_Feet);
        end;
        2 : //sub-surface
        begin
          lblOutrunAltitudeDepth.Caption := FormatAltitude(Altitude);
        end;
      end;
    end;
  end;
end;

end.
