unit ufrmGuidanceSinuation;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ufrmGuidanceType, uT3Track;

type
  TfrmGuidanceSinuation = class(TfrmGuidanceType)
    grpSinuation: TGroupBox;
    lbl1: TLabel;
    lbl2: TLabel;
    lbl3: TLabel;
    lbl4: TLabel;
    lbl5: TLabel;
    lbl6: TLabel;
    lbl7: TLabel;
    lbl8: TLabel;
    lblSinuationActualGroundSpeed: TLabel;
    lbl9: TLabel;
    lbl10: TLabel;
    txt1: TStaticText;
    edtSinuationBaseCourse: TEdit;
    txt2: TStaticText;
    edtSinuationBasePeriod: TEdit;
    txt3: TStaticText;
    edtSinuationAmplitude: TEdit;
    txt4: TStaticText;
    edtSinuationOrderedgroundSpeed: TEdit;
    txt5: TStaticText;
    procedure OnKeyPressed(Sender: TObject; var Key: Char);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure UpdateForm; override;
    procedure SetControlledObject(aObj : TT3Track); override;
  end;

var
  frmGuidanceSinuation: TfrmGuidanceSinuation;

implementation

uses uT3Vehicle, uBaseCoordSystem, uT3SinuationGuide,
  uT3ClientManager, uGameData_TTT;

{$R *.dfm}

{ TfrmGuidanceSinuation }

procedure TfrmGuidanceSinuation.OnKeyPressed(Sender: TObject; var Key: Char);
var
  ValKey : set of AnsiChar;
  Course, Period, Amplitudo, Speed : Double;
  v : TT3Vehicle;
  validInput : Boolean;
  rec: TRecCmd_PlatformGuidance;
begin
  inherited;

  ValKey := [#48 .. #57, #8, #13, #46];
  if not(CharInSet(Key, ValKey)) then
    Key := #0;

  if Key = #13 then
  begin
    if FControlledPlatform is TT3Vehicle then
    begin
      v := FControlledPlatform as TT3Vehicle;

      case TEdit(sender).Tag of
        1 : begin
              //Sinuation Course
              Course := StrToFloat(edtSinuationBaseCourse.Text);
              if Course > 360 then
              begin
                Course := 0;
              end;

              rec.PlatfomID := v.InstanceIndex;
              rec.OrderID := CORD_ID_MOVE;
              rec.OrderType := CORD_TYPE_COURSE;
              rec.OrderParam := Course;

              clientManager.NetCmdSender.CmdPlatformGuidance(rec);

            end;
        2 : begin
              //Period Sinuation
              validInput := False;
              if TryStrToFloat(edtSinuationBasePeriod.Text, Period) then
              begin
                if Period > 0 then
                begin
                  rec.PlatfomID := v.InstanceIndex;
                  rec.OrderID := CORD_ID_MOVE;
                  rec.OrderType := CORD_TYPE_SINUATION_PERIOD;
                  rec.OrderParam := Period;

                  clientManager.NetCmdSender.CmdPlatformGuidance(rec);
                  validInput := True;
                end
              end;

              if not validInput then
              begin
                if v.VehicleGuidance is TT3SinuationGuidance then
                  edtSinuationBasePeriod.Text := FormatFloat('0.00', TT3SinuationGuidance(v.VehicleGuidance).PeriodSinuation)
                else
                  edtSinuationBasePeriod.Text := '---';
                TacticalDisplayStatus('Sinuation Invalid Periode Value');
              end;

            end;
        3 : begin
              //Amplitudo Sinuation
              validInput := False;
              if TryStrToFloat(edtSinuationAmplitude.Text, Amplitudo) then
              begin
                if Amplitudo > 0 then
                begin
                  rec.PlatfomID := v.InstanceIndex;
                  rec.OrderID := CORD_ID_MOVE;
                  rec.OrderType := CORD_TYPE_SINUATION_AMPLITUDO;
                  rec.OrderParam := Amplitudo;

                  clientManager.NetCmdSender.CmdPlatformGuidance(rec);
                  validInput := True;
                end
              end;

              if not validInput then
              begin
                if v.VehicleGuidance is TT3SinuationGuidance then
                  edtSinuationAmplitude.Text := FormatFloat('0.00', TT3SinuationGuidance(v.VehicleGuidance).AmplitudoSinuation)
                else
                  edtSinuationAmplitude.Text := '---';
                TacticalDisplayStatus('Sinuation Invalid Amplitudo Value');
              end;
            end;

        4 : begin
              //Speed Sinuation
              Speed := StrToFloat(edtSinuationOrderedgroundSpeed.Text);

//              if (v.FuelRemaining <= 0 ) then
//              begin
//                if simMgrClient.ISInstructor then
//                  frmTacticalDisplay.addStatus(v.TrackLabel+' Out of Fuel')
//                else
//                  frmTacticalDisplay.addStatus(IntToStr(v.TrackNumber)+' Out of Fuel');
//
//                Exit;
//              end;

              if (Speed > v.Mover.MaxSpeed) then
              begin
                edtSinuationOrderedgroundSpeed.Text := FloatToStr(v.Mover.MaxSpeed);
                TacticalDisplayStatus(TT3Track(FControlledTrack).TrackLabelInfo +
                  ' Over Speed = '+ FloatToStr(Speed));

                Speed := v.Mover.MaxSpeed;     //mk test
              end
              else if (Speed < v.Mover.MinSpeed) then //choco
              begin
                edtSinuationOrderedgroundSpeed.Text := FloatToStr(v.Mover.MinSpeed);
                TacticalDisplayStatus(TT3Track(FControlledTrack).TrackLabelInfo +
                  ' MIN SPEED');

                Speed := v.Mover.MinSpeed;
              end;

              rec.PlatfomID := v.InstanceIndex;
              rec.OrderID := CORD_ID_MOVE;
              rec.OrderType := CORD_TYPE_SPEED;
              rec.OrderParam := Speed;

              clientManager.NetCmdSender.CmdPlatformGuidance(rec);
            end;
      end;
    end;
  end;
end;

procedure TfrmGuidanceSinuation.SetControlledObject(aObj: TT3Track);
begin
  inherited;

  if (FControlledPlatform <> nil) and (FControlledPlatform is TT3Vehicle) then
  with TT3Vehicle(FControlledPlatform) do
  begin
    if VehicleGuidance is TT3SinuationGuidance then
    begin
      edtSinuationBaseCourse.Text          := IntToStr(round(TT3SinuationGuidance(VehicleGuidance).OrderedSinuation));
      edtSinuationBasePeriod.Text          := FormatFloat('0.00', TT3SinuationGuidance(VehicleGuidance).PeriodSinuation);
      edtSinuationAmplitude.Text           := FormatFloat('0.00', TT3SinuationGuidance(VehicleGuidance).AmplitudoSinuation);
    end;

    edtSinuationOrderedgroundSpeed.Text  := FloatToStr(OrderedSpeed);
    lblSinuationActualGroundSpeed.Caption:= FormatSpeed(Speed);
  end;
end;

procedure TfrmGuidanceSinuation.UpdateForm;
begin
  inherited;

  if (FControlledPlatform <> nil) and (FControlledPlatform is TT3Vehicle) then
  with TT3Vehicle(FControlledPlatform) do
  begin
    lblSinuationActualGroundSpeed.Caption:= FormatSpeed(Speed);
  end;
end;

end.
