unit ufmGuidanceSL;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, 
  Dialogs, ufmGuidanceType, uT3Track, StdCtrls, ExtCtrls, VrControls, VrWheel;

type
  TfmGuidanceSL = class(TfmGuidanceType)
    ScrollBox5: TScrollBox;
    whHeading: TVrWheel;
    lblStraightLineActualHeading: TLabel;
    Label128: TLabel;
    lblStraightLineActualGroundSpeed: TLabel;
    Label126: TLabel;
    Label125: TLabel;
    Label124: TLabel;
    Label123: TLabel;
    Label122: TLabel;
    Label121: TLabel;
    lblStraightLineActuaCourse: TLabel;
    Label119: TLabel;
    panAltitude: TPanel;
    Label57: TLabel;
    lblActualAltitude: TLabel;
    Label63: TLabel;
    Label65: TLabel;
    StaticText105: TStaticText;
    StaticText107: TStaticText;
    StaticText111: TStaticText;
    edOrderAltitude: TEdit;
    panDepth: TPanel;
    lbl1: TLabel;
    lbl2: TLabel;
    lbl3: TLabel;
    lbl4: TLabel;
    txt1: TStaticText;
    txt2: TStaticText;
    txt3: TStaticText;
    edt1: TEdit;
    edtStraightLineOrderedGroundSpeed: TEdit;
    edtStraightLineOrderedHeading: TEdit;
    StaticText87: TStaticText;
    StaticText86: TStaticText;
    StaticText85: TStaticText;
    StaticText84: TStaticText;
    StaticText83: TStaticText;
    StaticText82: TStaticText;
    StaticText81: TStaticText;
    procedure whHeadingChange(Sender: TObject);
    procedure whHeadingMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure edtStraightLineOrderedHeadingKeyPress(Sender: TObject;
      var Key: Char);
    procedure edtStraightLineOrderedGroundSpeedKeyPress(Sender: TObject;
      var Key: Char);
    procedure edOrderAltitudeKeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure EmptyField; override;
    procedure UpdateForm; override;
    procedure SetControlledObject(aObj : TT3Track); override;
  end;

implementation

uses uT3Vehicle, uBaseCoordSystem, uDBAsset_Vehicle, uT3ClientManager,
  uGameData_TTT, tttData;

{$R *.dfm}

{ TfrmGuidanceSL }

procedure TfmGuidanceSL.edOrderAltitudeKeyPress(Sender: TObject; var Key: Char);
var
  ValKey : set of AnsiChar;
  OrderAltitude, Altitude: double;
  v : TT3Vehicle;
  rec: TRecCmd_Platform;
begin
  ValKey := [#48 .. #57, #8, #13, #46];
  if not(CharInSet(Key, ValKey)) then
    Key := #0;

  if Key = #13 then
  begin
    Altitude := 0;

    case TEdit(Sender).Tag of
      1 :
      begin
        TryStrToFloat(TEdit(Sender).Text, OrderAltitude);
        Altitude := OrderAltitude * C_Feet_To_Meter
      end;
      2 :
      begin
        TryStrToFloat(edt1.Text, OrderAltitude);
        Altitude := OrderAltitude;
      end;
    end;

//    if frmTacticalDisplay.ControlEmbarkedPlatform then
//    begin
//      if Assigned(frmLaunchPlaform) then
//      begin
//        frmLaunchPlaform.EmbarkedAltitude := Altitude;
//        exit;
//      end;
//    end;

    if Assigned(FControlledPlatform) and (FControlledPlatform is TT3Vehicle) then
    begin
      v := FControlledPlatform as TT3Vehicle;
      v.OrderedAltitude := Altitude;

      rec.PlatfomID := v.InstanceIndex;
      rec.OrderID := CORD_ID_MOVE;
      rec.OrderType := CORD_TYPE_ALTITUDE;
      rec.OrderParam := Altitude;

      clientManager.NetCmdSender.CmdPlatform(rec);
    end;
  end;
end;

procedure TfmGuidanceSL.edtStraightLineOrderedGroundSpeedKeyPress(
  Sender: TObject; var Key: Char);
var
  ValKey : set of AnsiChar;
  Speed : double;
  v : TT3Vehicle;
  rec: TRecCmd_Platform;
begin
  inherited;

  ValKey := [#48 .. #57, #8, #13, #46];
  if not(CharInSet(Key, ValKey)) then
    Key := #0;

  if Key = #13 then
  begin
    if Assigned(FControlledPlatform) and (FControlledPlatform is TT3Vehicle) then
    begin
      v := FControlledPlatform as TT3Vehicle;

      TryStrToFloat(TEdit(Sender).Text, Speed);

//      if frmTacticalDisplay.ControlEmbarkedPlatform then
//      begin
//        if Assigned(frmLaunchPlaform) then
//        begin
//          frmLaunchPlaform.EmbarkedSpeed := Speed;
//          exit;
//        end;
//      end;
//      if (v.FuelRemaining <= 0 ) then
//      begin
//        if simMgrClient.ISInstructor then
//          frmTacticalDisplay.addStatus(v.TrackLabel + ' Out of Fuel')
//        else
//          frmTacticalDisplay.addStatus(IntToStr(v.TrackNumber)+ ' Out of Fuel');
//        Exit;
//      end;

      if (Speed > v.Mover.MaxSpeed) then
      begin
        TEdit(Sender).Text := FloatToStr(v.Mover.MaxSpeed);
        TacticalDisplayStatus(TT3Track(FControlledTrack).TrackLabelInfo +
          ' Over Speed = '+ FloatToStr(Speed));

        Speed := v.Mover.MaxSpeed;     //mk test
      end
      else if (Speed < v.Mover.MinSpeed) then //choco
      begin
        edtStraightLineOrderedGroundSpeed.Text := FloatToStr(v.Mover.MinSpeed);
        TacticalDisplayStatus(TT3Track(FControlledTrack).TrackLabelInfo +
          ' MIN SPEED');

        Speed := v.Mover.MinSpeed;
      end;

       //------------ send record speed straightline ------------------//

      rec.PlatfomID := v.InstanceIndex;
      rec.OrderID := CORD_ID_MOVE;
      rec.OrderType := CORD_TYPE_SPEED;
      rec.OrderParam := Speed;

      clientManager.NetCmdSender.CmdPlatform(rec);
    end;
  end;
end;

procedure TfmGuidanceSL.edtStraightLineOrderedHeadingKeyPress(Sender: TObject;
  var Key: Char);
var
  ValKey : set of AnsiChar;
  heading: double;
  v: TT3Vehicle;
  rec: TRecCmd_Platform;
begin
  inherited;

  ValKey := [#48 .. #57, #8, #13, #46];
  if not(CharInSet(Key, ValKey)) then
    Key := #0;

  if Key = #13 then
  begin
    TryStrToFloat(edtStraightLineOrderedHeading.Text, heading);

	  if heading > 360 then
    begin
      heading := 0;
    end;

    if Assigned(FControlledPlatform) and (FControlledPlatform is TT3Vehicle) then
    begin
      v := FControlledPlatform as TT3Vehicle;

      rec.PlatfomID := v.InstanceIndex;
      rec.OrderID := CORD_ID_MOVE;
      rec.OrderType := CORD_TYPE_COURSE;
      rec.OrderParam := heading;

      clientManager.NetCmdSender.CmdPlatform(rec);
    end;

    if heading > 180 then
      whHeading.Position := Round(heading - 180)
    else
      whHeading.Position := Round(heading + 180);
  end;
end;

procedure TfmGuidanceSL.EmptyField;
begin
  inherited;

end;

procedure TfmGuidanceSL.SetControlledObject(aObj: TT3Track);
begin
  inherited;

  if (FControlledPlatform <> nil) and (FControlledPlatform is TT3Vehicle) then
  begin
    with TT3Vehicle(FControlledPlatform) do
    begin

    edtStraightLineOrderedGroundSpeed.Text:= FormatSpeed(OrderedSpeed);
    edtStraightLineOrderedHeading.Text    := FormatCourse(OrderedHeading); //lblStraightLineActualHeading.Caption;
    edOrderAltitude.Text                  := FormatAltitude(OrderedAltitude / C_Feet_To_Meter);
    edt1.Text                             := FormatAltitude(OrderedAltitude);

    whHeading.OnChange := nil;
    if Heading > 180 then
      whHeading.Position := Round(Course - 180)     //  Round(Course - 180)
    else
      whHeading.Position := Round(Course + 180);    //  Round(Course + 180);

    whHeading.OnChange := whHeadingChange;

    case TVehicle_Definition(PlatformDefinition).FData.Platform_Domain of
      0 :
        begin
          // Result := 'Air';
          panAltitude.Visible := True;
          panDepth.Visible    := False;
        end;
      1:
        begin
          // Result := 'Surface';
          panAltitude.Visible := False;
          panDepth.Visible    := False;
        end;
      2:
        begin
          // Result := 'Subsurface';
          panAltitude.Visible := False;
          panDepth.Visible    := True;
        end;
      3:
        begin
          // Result := 'Land';
          panAltitude.Visible := False;
          panDepth.Visible    := False;
        end;
      4:
        begin
          //result := Amphibi
          panAltitude.Visible := False;
          panDepth.Visible    := True;
        end;
    else
      begin
        panAltitude.Visible := False;
        panDepth.Visible    := False;
      end;
    end;
  end;
  end;

end;

procedure TfmGuidanceSL.UpdateForm;
begin
  inherited;

  if (FControlledPlatform <> nil) and (FControlledPlatform is TT3Vehicle) then
  begin
    with TT3Vehicle(FControlledPlatform) do
    begin
      lblStraightLineActualHeading.Caption    := FormatCourse(Heading);
      lblStraightLineActuaCourse.Caption      := FormatCourse(Course);
      lblStraightLineActualGroundSpeed.Caption:= FormatSpeed(Speed);
      lblActualAltitude.Caption               := FormatAltitude(Altitude / C_Feet_To_Meter);
      lbl2.Caption                            := FormatAltitude(Altitude);

    end;
  end;

end;

procedure TfmGuidanceSL.whHeadingChange(Sender: TObject);
var
  newheading : integer;
begin
  if whHeading.Position < 180 then
    newheading := (180 + whHeading.Position)
  else
    newheading := (whHeading.Position - 180);

  edtStraightLineOrderedHeading.Text := IntToStr(newheading);
end;

procedure TfmGuidanceSL.whHeadingMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var
  newheading : integer;
  v : TT3Vehicle;
  rec: TRecCmd_Platform;
begin

  if whHeading.Position < 180 then
    newheading := (180 + whHeading.Position)
  else
    newheading := (whHeading.Position - 180);

//  if frmTacticalDisplay.ControlEmbarkedPlatform then
//  begin
//    if Assigned(frmLaunchPlaform) then
//    begin
//      frmLaunchPlaform.EmbarkedHeading := newheading;
//      exit;
//    end;
//  end;

  if Assigned(FControlledPlatform) and (FControlledPlatform is TT3Vehicle) then
  begin
    v := FControlledPlatform as TT3Vehicle;

    rec.PlatfomID := v.InstanceIndex;
    rec.OrderID := CORD_ID_MOVE;
    rec.OrderType := CORD_TYPE_COURSE;
    rec.OrderParam := newheading;

    clientManager.NetCmdSender.CmdPlatform(rec);
  end;
end;

end.
