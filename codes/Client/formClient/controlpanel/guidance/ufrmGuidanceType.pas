unit ufrmGuidanceType;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, uT3PlatformInstance, uT3Track, StdCtrls;

type
  TfrmGuidanceType = class(TForm)
  private
    FOnTacticalDisplayStatus: TGetStrProc;
    procedure SetOnTacticalDisplayStatus(const Value: TGetStrProc);
    { Private declarations }
  protected
    { refer to track being controlled }
    FControlledTrack : TT3Track;
    { refer to track being selected / hooked }
    FSelectedTrack : TT3Track;
    { refer to platform instance of track }
    FControlledPlatform : TT3PlatformInstance;
    procedure TacticalDisplayStatus(str : String) ;
    procedure OnOrderGroundSpeedChange(Sender : TObject);
    procedure OnOrderAltitudeChange(Sender : TObject; unitMetric : string );
  public
    { Public declarations }
    procedure UpdateForm; virtual;
    procedure EmptyField;virtual;
    procedure SetControlledObject(aObj : TT3Track); virtual;
    procedure SetSelectedObject(aObj : TT3Track);
    property OnTacticalDisplayStatus : TGetStrProc read FOnTacticalDisplayStatus write SetOnTacticalDisplayStatus;
  end;

var
  frmGuidanceType: TfrmGuidanceType;

implementation

uses
  uGlobalvar, uT3Vehicle, uGameData_TTT, uT3ClientManager,
  uBaseCoordSystem;

{$R *.dfm}

{ TfrmGuidanceType }

procedure TfrmGuidanceType.EmptyField;
begin

end;

procedure TfrmGuidanceType.OnOrderAltitudeChange(Sender: TObject;
  unitMetric: string);
var
  val : Double;
  v: TT3Vehicle;
  rec: TRecCmd_PlatformGuidance;
begin
  v := FControlledPlatform as TT3Vehicle;
  if TryStrToFloat(TEdit(Sender).Text, val) then
  begin
    if unitMetric = 'm' then // meter
    begin
      // defaullt is m
    end
    else
    if unitMetric = 'f' then // feet
    begin
      val := val * C_Feet_To_Meter
    end;

    rec.PlatfomID   := v.InstanceIndex;
    rec.OrderID     := CORD_ID_MOVE;
    rec.OrderType   := CORD_TYPE_ALTITUDE;
    rec.OrderParam  := val;

    clientManager.NetCmdSender.CmdPlatformGuidance(rec);
  end
  else
    TEdit(Sender).Text := FormatFloat('00.0',v.Altitude);
end;

procedure TfrmGuidanceType.OnOrderGroundSpeedChange(Sender: TObject);
var
  v: TT3Vehicle;
  rec: TRecCmd_PlatformGuidance;
  val : double;
begin
  v := FControlledPlatform as TT3Vehicle;
  if TryStrToFloat(TEdit(Sender).Text, val) then
  begin
//    if (v.FuelRemaining <= 0 ) then
//    begin
//      if simMgrClient.ISInstructor then
//        frmTacticalDisplay.addStatus(v.TrackLabel+' Out of Fuel')
//      else
//        frmTacticalDisplay.addStatus(IntToStr(v.TrackNumber)+' Out of Fuel');
//
//      Exit;
//    end;
//
    if (v.Mover.MaxSpeed < val ) then
    begin
      TEdit(Sender).Text := FloatToStr(v.Mover.MaxSpeed);
      TacticalDisplayStatus(TT3Track(FControlledTrack).TrackLabel+
            ' Over Speed = ' + FloatToStr(val));

      val := v.Mover.MaxSpeed;     //mk test
    end
    else if (val < v.Mover.MinSpeed) then //choco
    begin
      TEdit(Sender).Text := FloatToStr(v.Mover.MinSpeed);
      TacticalDisplayStatus(TT3Track(FControlledTrack).TrackLabel+
            ' MIN SPEED' );

      val := v.Mover.MinSpeed;
    end;

    rec.PlatfomID   := v.InstanceIndex;
    rec.OrderID     := CORD_ID_MOVE;
    rec.OrderType   := CORD_TYPE_SPEED;
    rec.OrderParam  := val;

    clientManager.NetCmdSender.CmdPlatformGuidance(rec);
  end
  else
    TacticalDisplayStatus('It is not valid input');

end;

procedure TfrmGuidanceType.SetControlledObject(aObj: TT3Track);
begin
  FControlledTrack := aObj;

  FControlledPlatform := nil;
  if Assigned(aObj) and (aObj is TT3Track) then
    FControlledPlatform := simManager.FindT3PlatformByID(TT3Track(aObj).ObjectInstanceIndex);

end;

procedure TfrmGuidanceType.SetOnTacticalDisplayStatus(const Value: TGetStrProc);
begin
  FOnTacticalDisplayStatus := Value;
end;

procedure TfrmGuidanceType.SetSelectedObject(aObj: TT3Track);
begin
  FSelectedTrack := aObj;
end;

procedure TfrmGuidanceType.TacticalDisplayStatus(str: String);
begin
  if Assigned(FOnTacticalDisplayStatus) then
    FOnTacticalDisplayStatus(str);
end;

procedure TfrmGuidanceType.UpdateForm;
begin

end;

end.
