unit ufrmGuidanceEngagement;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, Buttons, ufrmGuidanceType, uT3Track;

type
  TfrmGuidanceEngagement = class(TfrmGuidanceType)
    grpEngagement: TGroupBox;
    lbl1: TLabel;
    lbl2: TLabel;
    lbl3: TLabel;
    lbl4: TLabel;
    lbl5: TLabel;
    lbl6: TLabel;
    lblEngageActualGroundSpeed: TLabel;
    lbl7: TLabel;
    btnEngageTrackToEngage: TSpeedButton;
    txt1: TStaticText;
    edtTrackToEngage: TEdit;
    txt2: TStaticText;
    txt3: TStaticText;
    txt4: TStaticText;
    edtEngageStandOffDistance: TEdit;
    edtEngageOrderedGroundSpeed: TEdit;
    pnlDepthEngagement: TPanel;
    lbl8: TLabel;
    lblDeptEngagMOde: TLabel;
    lbl9: TLabel;
    lbl10: TLabel;
    txt5: TStaticText;
    txt6: TStaticText;
    txt7: TStaticText;
    edt1: TEdit;
    pnlAltitudeEngagement: TPanel;
    lbl11: TLabel;
    lblAltitudeEngagement: TLabel;
    lbl12: TLabel;
    lbl13: TLabel;
    txt8: TStaticText;
    txt9: TStaticText;
    txt10: TStaticText;
    edtAltitudeEngagement: TEdit;
    procedure OnKeyPress(Sender: TObject; var Key: Char);
    procedure btnEngageTrackToEngageClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure UpdateForm; override;
    procedure SetControlledObject(aObj : TT3Track); override;
  end;

var
  frmGuidanceEngagement: TfrmGuidanceEngagement;

implementation

uses uT3Vehicle, uBaseCoordSystem, uT3EngagementGuide,
  uGameData_TTT, uT3ClientManager;

{$R *.dfm}

{ TfrmGuidanceEngagement }

procedure TfrmGuidanceEngagement.btnEngageTrackToEngageClick(Sender: TObject);
var
  track : TT3Track;
  rec: TRecCmd_PlatformGuidance;
  v: TT3Vehicle;
  bearing, range : Double;
begin
  inherited;

  if (Assigned(FControlledPlatform)) and (FControlledPlatform is TT3Vehicle) then
  begin
    if Assigned(FSelectedTrack) then
    begin
      if FSelectedTrack = FControlledTrack then
        Exit;

      v := FControlledPlatform as TT3Vehicle;

      if Assigned(v.VehicleGuidance) and (v.VehicleGuidance is TT3EngagementGuidance) then
      begin
        edtTrackToEngage.Text := FSelectedTrack.TrackLabelInfo;

        //bearing := CalcBearing(v.PosX, v.PosY, target.PosX, target.PosY);
        //range := CalcRange(v.PosX, v.PosY, target.PosX, target.PosY);

        //SimMgrClient.netSend_CmdPlatformGuidance(v.InstanceIndex, CORD_ID_MOVE,
        //  CORD_TYPE_ENGAGE_TRACK, target.InstanceIndex);

        //SimMgrClient.netSend_CmdPlatformGuidance(v.InstanceIndex, CORD_ID_MOVE,
        //  CORD_TYPE_COURSE, bearing);

        //SimMgrClient.netSend_CmdPlatformGuidance(v.InstanceIndex, CORD_ID_MOVE,
        //  CORD_TYPE_ENGAGE_DISTANCE, (range * 0.8));

        if FSelectedTrack.ObjectInstanceIndex > 0 then
        begin
          rec.PlatfomID   := v.InstanceIndex;
          rec.OrderID     := CORD_ID_MOVE;
          rec.OrderType   := CORD_TYPE_ENGAGE_TRACK;
          rec.OrderParam  := FSelectedTrack.ObjectInstanceIndex;

          clientManager.NetCmdSender.CmdPlatformGuidance(rec)
        end
        else
          TacticalDisplayStatus('Target is not valid');
      end
      else
        TacticalDisplayStatus('Target is not selected');
    end;
  end;
end;

procedure TfrmGuidanceEngagement.OnKeyPress(Sender: TObject; var Key: Char);
var
  ValKey : set of AnsiChar;
  val : double;
  v : TT3Vehicle;
  rec: TRecCmd_PlatformGuidance;
begin

  ValKey := [#48 .. #57, #8, #13, #46];
  if not(CharInSet(Key, ValKey)) then
    Key := #0;

  if Key = #13 then
  begin
    if Assigned(FControlledPlatform) and (FControlledPlatform is TT3Vehicle) then
    begin

      case TComponent(Sender).Tag of
        2 :
        begin

          if TryStrToFloat(TEdit(Sender).Text, val) then
          begin
            v := FControlledPlatform as TT3Vehicle;

            rec.PlatfomID   := v.InstanceIndex;
            rec.OrderID     := CORD_ID_MOVE;
            rec.OrderType   := CORD_TYPE_ENGAGE_DISTANCE;
            rec.OrderParam  := val;

            clientManager.NetCmdSender.CmdPlatformGuidance(rec);
          end
          else
            TEdit(Sender).Text := FormatFloat('00.0',
                TT3EngagementGuidance(v.VehicleGuidance).StandOffDistance)

        end ;
        3 : OnOrderGroundSpeedChange(Sender);
        4 : OnOrderAltitudeChange(Sender,'f');
      end;
    end;
  end;
end;

procedure TfrmGuidanceEngagement.SetControlledObject(aObj: TT3Track);
begin
  inherited;
  if (FControlledPlatform <> nil) and (FControlledPlatform is TT3Vehicle) then
  begin
    with TT3Vehicle(FControlledPlatform) do
    begin
      lblEngageActualGroundSpeed.Caption := FormatSpeed(Speed);

      case PlatformDomain of
        0 : lblAltitudeEngagement.Caption := FormatAltitude(Altitude / C_Feet_To_Meter);
        2 : lblDeptEngagMOde.Caption      := FormatAltitude(Altitude);
      end;

      edtEngageOrderedGroundSpeed.Text  := FormatSpeed(OrderedSpeed);
      edtAltitudeEngagement.Text        := FormatAltitude(OrderedAltitude);

      case PlatformDomain of
        0 :
          begin
            // Result := 'Air';
            pnlAltitudeEngagement.Visible := True;
            pnlDepthEngagement.Visible    := False;
          end;
        1:
          begin
            // Result := 'Surface';
            pnlAltitudeEngagement.Visible := False;
            pnlDepthEngagement.Visible    := False;
          end;
        2:
          begin
            // Result := 'Subsurface';
            pnlAltitudeEngagement.Visible := False;
            pnlDepthEngagement.Visible    := True;
          end;
        3:
          begin
            // Result := 'Land';
            pnlAltitudeEngagement.Visible := False;
            pnlDepthEngagement.Visible    := False;
          end;
        4: //Amphibi
          begin
            pnlAltitudeEngagement.Visible := False;
            pnlDepthEngagement.Visible    := True;
          end;
      else
        begin
          pnlAltitudeEngagement.Visible := False;
          pnlDepthEngagement.Visible    := False;
        end;
      end;
    end;
  end;
end;

procedure TfrmGuidanceEngagement.UpdateForm;
begin
  inherited;

  if (FControlledPlatform <> nil) and (FControlledPlatform is TT3Vehicle) then
  begin
    with TT3Vehicle(FControlledPlatform) do
    begin
      lblEngageActualGroundSpeed.Caption := FormatSpeed(Speed);

      case PlatformDomain of
        0 : lblAltitudeEngagement.Caption := FormatAltitude(Altitude / C_Feet_To_Meter);
        2 : lblDeptEngagMOde.Caption      := FormatAltitude(Altitude);
      end;
    end;
  end;

end;

end.
