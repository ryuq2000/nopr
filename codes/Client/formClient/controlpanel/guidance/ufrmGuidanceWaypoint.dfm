object frmGuidanceWaypoint: TfrmGuidanceWaypoint
  Left = 0
  Top = 0
  Caption = 'Waypoint'
  ClientHeight = 300
  ClientWidth = 329
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object grpWaypoint: TGroupBox
    Left = 0
    Top = 0
    Width = 329
    Height = 300
    Align = alClient
    Color = clBtnFace
    ParentBackground = False
    ParentColor = False
    TabOrder = 0
    object lbl1: TLabel
      Left = 15
      Top = 26
      Width = 27
      Height = 13
      Caption = 'Name'
    end
    object lblName: TLabel
      Left = 150
      Top = 26
      Width = 12
      Height = 13
      Caption = '---'
    end
    object lblLongitude: TLabel
      Left = 150
      Top = 45
      Width = 12
      Height = 13
      Caption = '---'
    end
    object lbl2: TLabel
      Left = 15
      Top = 45
      Width = 37
      Height = 13
      Caption = 'Position'
    end
    object lblGroundSpeed: TLabel
      Left = 150
      Top = 64
      Width = 16
      Height = 13
      Caption = '0.0'
    end
    object lbl3: TLabel
      Left = 15
      Top = 64
      Width = 68
      Height = 13
      Caption = 'Ground Speed'
    end
    object lblEta: TLabel
      Left = 150
      Top = 104
      Width = 12
      Height = 13
      Caption = '---'
    end
    object lbl4: TLabel
      Left = 15
      Top = 83
      Width = 70
      Height = 13
      Caption = 'Distance to Go'
    end
    object lbl5: TLabel
      Left = 230
      Top = 83
      Width = 14
      Height = 13
      Caption = 'nm'
    end
    object lbl6: TLabel
      Left = 15
      Top = 141
      Width = 111
      Height = 13
      Caption = 'Ordered Ground Speed'
    end
    object lblDistance: TLabel
      Left = 150
      Top = 83
      Width = 18
      Height = 13
      Caption = '000'
    end
    object lbl7: TLabel
      Left = 230
      Top = 141
      Width = 26
      Height = 13
      Caption = 'knots'
    end
    object lbl8: TLabel
      Left = 230
      Top = 162
      Width = 26
      Height = 13
      Caption = 'knots'
    end
    object lbl9: TLabel
      Left = 230
      Top = 64
      Width = 26
      Height = 13
      Caption = 'knots'
    end
    object lblLatitude: TLabel
      Left = 230
      Top = 45
      Width = 12
      Height = 13
      Caption = '---'
    end
    object lblActualGroundWaypoint: TLabel
      Left = 150
      Top = 162
      Width = 16
      Height = 13
      Caption = '0.0'
    end
    object bvl1: TBevel
      Left = 83
      Top = 13
      Width = 220
      Height = 3
    end
    object bvl2: TBevel
      Left = 12
      Top = 127
      Width = 295
      Height = 3
    end
    object txt1: TStaticText
      Left = 15
      Top = 102
      Width = 23
      Height = 17
      Caption = 'ETA'
      TabOrder = 0
    end
    object txt2: TStaticText
      Left = 8
      Top = 7
      Width = 74
      Height = 17
      Caption = 'Next waypoint'
      TabOrder = 1
    end
    object txt3: TStaticText
      Left = 15
      Top = 160
      Width = 105
      Height = 17
      Caption = 'Actual Ground Speed'
      TabOrder = 2
    end
    object btnWaypoint: TButton
      Left = 184
      Top = 193
      Width = 120
      Height = 25
      Caption = 'Edit Waypoints..'
      TabOrder = 3
      OnClick = btnWaypointClick
    end
    object txt4: TStaticText
      Left = 136
      Top = 24
      Width = 8
      Height = 17
      Caption = ':'
      TabOrder = 4
    end
    object txt5: TStaticText
      Left = 136
      Top = 43
      Width = 8
      Height = 17
      Caption = ':'
      TabOrder = 5
    end
    object txt6: TStaticText
      Left = 136
      Top = 62
      Width = 8
      Height = 17
      Caption = ':'
      TabOrder = 6
    end
    object txt7: TStaticText
      Left = 136
      Top = 81
      Width = 8
      Height = 17
      Caption = ':'
      TabOrder = 7
    end
    object txt8: TStaticText
      Left = 136
      Top = 102
      Width = 8
      Height = 17
      Caption = ':'
      TabOrder = 8
    end
    object txt9: TStaticText
      Left = 136
      Top = 160
      Width = 8
      Height = 17
      Caption = ':'
      TabOrder = 9
    end
    object txt10: TStaticText
      Left = 136
      Top = 139
      Width = 8
      Height = 17
      Caption = ':'
      TabOrder = 10
    end
    object edtWaypointOrderedGroundSpeed: TEdit
      Left = 150
      Top = 137
      Width = 60
      Height = 21
      BevelInner = bvNone
      BevelOuter = bvNone
      MaxLength = 9
      TabOrder = 11
      OnKeyPress = edtWaypointOrderedGroundSpeedKeyPress
    end
  end
end
