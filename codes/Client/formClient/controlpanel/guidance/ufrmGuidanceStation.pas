unit ufrmGuidanceStation;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, Buttons, ufrmGuidanceType, Menus, uT3Track;

type
  TfrmGuidanceStation = class(TfrmGuidanceType)
    grpStation: TGroupBox;
    lbl1: TLabel;
    bvl2: TBevel;
    btnOnTrackAnchorMode: TSpeedButton;
    bvl1: TBevel;
    pnlStationDepth: TPanel;
    lbl2: TLabel;
    lbl3: TLabel;
    lbl4: TLabel;
    lblStationActualDepth: TLabel;
    txt1: TStaticText;
    txt2: TStaticText;
    txt3: TStaticText;
    edtStationOrderedDepth: TEdit;
    pnlStationAltitude: TPanel;
    lbl6: TLabel;
    lbl7: TLabel;
    lbl8: TLabel;
    lblStationActualAltitude: TLabel;
    txt5: TStaticText;
    txt6: TStaticText;
    txt7: TStaticText;
    edtStationOrderedAltitude: TEdit;
    pnlStationPosition: TPanel;
    lbl5: TLabel;
    btnStationAnchorPosition: TSpeedButton;
    edtStationPosition: TEdit;
    txt4: TStaticText;
    chkStationDrift: TCheckBox;
    pnlStationTrack: TPanel;
    lbl9: TLabel;
    lbl10: TLabel;
    lbl11: TLabel;
    lblStationBearingState: TLabel;
    lbl12: TLabel;
    btnOnTrackAnchorTrack: TSpeedButton;
    btnStationOnTrackBearing: TSpeedButton;
    edtOnTrackAnchorBearing: TEdit;
    edtOnTrackAnchorRange: TEdit;
    edtOnTrackAnchorTrack: TEdit;
    txt8: TStaticText;
    txt9: TStaticText;
    txt10: TStaticText;
    txt11: TStaticText;
    edtOnTrackAnchorMode: TEdit;
    txt12: TStaticText;
    pmCircleMode: TPopupMenu;
    Position1: TMenuItem;
    track1: TMenuItem;
    pmCircleBearingDegree: TPopupMenu;
    True1: TMenuItem;
    Relative1: TMenuItem;
    procedure OnModeClick(Sender: TObject);
    procedure OnBearingClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure UpdateForm; override;
    procedure SetControlledObject(aObj : TT3Track); override;
  end;

var
  frmGuidanceStation: TfrmGuidanceStation;

implementation

uses
  uT3Vehicle, uBaseCoordSystem, uT3StationGuide,
  uGameData_TTT, uT3ClientManager;

{$R *.dfm}

{ TfrmGuidanceStation }

procedure TfrmGuidanceStation.SetControlledObject(aObj: TT3Track);
begin
  inherited;

  if (FControlledPlatform <> nil) and (FControlledPlatform is TT3Vehicle) then
  begin
    with TT3Vehicle(FControlledPlatform) do
    begin
      lblStationActualAltitude.Caption := FormatAltitude(Altitude / C_Feet_To_Meter);
      lblStationActualDepth.Caption    := FormatAltitude(Altitude);

      if VehicleGuidance is TT3StationGuidance then
      case TT3StationGuidance(VehicleGuidance).StationMode of
        1 :
        begin
          pnlStationPosition.BringToFront;
          edtStationPosition.Text := formatDM_longitude(TT3StationGuidance(VehicleGuidance).StationPostX)
             + ' ' + formatDM_latitude(TT3StationGuidance(VehicleGuidance).StationPostY);
        end;
        2 :
        begin
          pnlStationTrack.BringToFront;
          //edtOnTrackAnchorTrack.Text
        end;
      end;

      case PlatformDomain of
        0 :  //air
        begin
          pnlStationAltitude.Visible := True;
          pnlStationDepth.Visible    := false;

          edtStationOrderedAltitude.Text  := FormatAltitude(OrderedAltitude / C_Feet_To_Meter);
        end;
        2 : //Subsurface
        begin
          pnlStationAltitude.Visible := false;
          pnlStationDepth.Visible    := True;

          edtStationOrderedDepth.Text     := FormatAltitude(OrderedAltitude);
        end;
        1,3 :
        begin
          pnlStationAltitude.Visible := false;
          pnlStationDepth.Visible    := false;
        end;
      end;
    end;
  end;
end;

procedure TfrmGuidanceStation.OnBearingClick(Sender: TObject);
var
  v: TT3Vehicle;
  rec: TRecCmd_PlatformGuidance;
begin
  if Assigned(FControlledPlatform) and (FControlledPlatform is TT3Vehicle) then
  begin
    v := FControlledPlatform as TT3Vehicle;

    case TMenuItem(Sender).Tag of
      1 :
      begin
        lblStationBearingState.Caption  := 'degrees T';

        rec.PlatfomID := v.InstanceIndex;
        rec.OrderID := CORD_ID_MOVE;
        rec.OrderType := CORD_TYPE_STATION_BEARING_STATE;
        rec.OrderParam := 1;
      end;
      2 :
      begin
        lblStationBearingState.Caption := 'degrees R';

        rec.PlatfomID := v.InstanceIndex;
        rec.OrderID := CORD_ID_MOVE;
        rec.OrderType := CORD_TYPE_STATION_BEARING_STATE;
        rec.OrderParam := 2;

      end;
    end;

    clientManager.NetCmdSender.CmdPlatformGuidance(rec);
  end;
end;

procedure TfrmGuidanceStation.OnModeClick(Sender: TObject);
var
  v: TT3Vehicle;
  rec: TRecCmd_PlatformGuidance;
begin
  if Assigned(FControlledPlatform) and (FControlledPlatform is TT3Vehicle) then
  begin
    v := FControlledPlatform as TT3Vehicle;

    case TMenuItem(Sender).Tag of
      1 :
      begin
        edtOnTrackAnchorMode.Text  := 'Position';
        pnlStationPosition.Visible := true;
        pnlStationTrack.Visible    := false;

        rec.PlatfomID := v.InstanceIndex;
        rec.OrderID := CORD_ID_MOVE;
        rec.OrderType := CORD_TYPE_STATION_MODE;
        rec.OrderParam := 1;
      end;
      2 :
      begin
        edtOnTrackAnchorMode.Text  := 'Track';
        pnlStationPosition.Visible := false;
        pnlStationTrack.Visible    := true;

        rec.PlatfomID := v.InstanceIndex;
        rec.OrderID := CORD_ID_MOVE;
        rec.OrderType := CORD_TYPE_STATION_MODE;
        rec.OrderParam := 2;
      end;
    end;

    clientManager.NetCmdSender.CmdPlatformGuidance(rec);
  end;
end;

procedure TfrmGuidanceStation.UpdateForm;
begin
  inherited;

  if (FControlledPlatform <> nil) and (FControlledPlatform is TT3Vehicle) then
  begin
    with TT3Vehicle(FControlledPlatform) do
    begin
      lblStationActualAltitude.Caption := FormatAltitude(Altitude / C_Feet_To_Meter);
      lblStationActualDepth.Caption    := FormatAltitude(Altitude);
    end;
  end;

end;

end.
