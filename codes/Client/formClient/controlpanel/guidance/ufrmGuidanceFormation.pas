unit ufrmGuidanceFormation;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, ufrmGuidanceType, uT3Track;

type
  TfrmGuidanceFormation = class(TfrmGuidanceType)
    grpFormation: TGroupBox;
    lbl1: TLabel;
    lbl2: TLabel;
    lblLeaderFormation: TLabel;
    lblNameFormation: TLabel;
    lbl3: TLabel;
    lbl4: TLabel;
    lbl5: TLabel;
    lblRangeFormation: TLabel;
    lblBearingFormation: TLabel;
    lbl6: TLabel;
    pnlDepth: TPanel;
    lblFormationDepth: TLabel;
    lbl7: TLabel;
    txt1: TStaticText;
    txt2: TStaticText;
    pnlAltitude: TPanel;
    lblFormationAltitude: TLabel;
    lbl8: TLabel;
    txt3: TStaticText;
    txt4: TStaticText;
    txt5: TStaticText;
    txt6: TStaticText;
    txt7: TStaticText;
    txt8: TStaticText;
  private
    { Private declarations }
  public
    { Public declarations }
    procedure UpdateForm; override;
    procedure SetControlledObject(aObj : TT3Track); override;
  end;

var
  frmGuidanceFormation: TfrmGuidanceFormation;

implementation

uses uT3Vehicle, uBaseCoordSystem, uT3EngagementGuide,
  uGameData_TTT, uT3ClientManager, uT3PlatformInstance;

{$R *.dfm}

{ TfrmGuidanceFormation }

procedure TfrmGuidanceFormation.SetControlledObject(aObj: TT3Track);
begin
  inherited;
end;

procedure TfrmGuidanceFormation.UpdateForm;
var
  pf : TT3PlatformInstance;
begin
  inherited;

  if (FControlledPlatform <> nil) and (FControlledPlatform is TT3Vehicle) then
  begin
    with TT3Vehicle(FControlledPlatform) do
    begin

//      if FormationStatus.isFormationLeader then
//      begin
//        lblNameFormation.Caption := FormationStatus.FormationName;
//        lblLeaderFormation.Caption := InstanceName;
//      end
//      else begin
//        if FormationStatus.FormationLeader > 0 then
//        begin
//          lblNameFormation.Caption := FormationStatus.FormationName;
//          pf := clientManager.FindT3PlatformByID(FormationStatus.FormationLeader);
//          lblLeaderFormation.Caption := pf.InstanceName;
//        end else
//        begin
//          lblNameFormation.Caption := '---';
//          lblLeaderFormation.Caption := '---';
//        end;
//      end;
//
//      lblBearingFormation.Caption := FormatCourse(FormationStatus.FormationBearing);
//      lblRangeFormation.Caption   := FormatFloat('0.00', FormationStatus.FormationRange);
//
//      case VehicleDefinition.FData.Platform_Domain of
//        0 :  //air
//        begin
//          pnlAltitude.Visible := True;
//          pnlDepth.Visible    := false;
//
//          lblFormationAltitude.Caption  := FormatAltitude(Altitude / C_Feet_To_Meter);
//        end;
//        2 : //Subsurface
//        begin
//          pnlAltitude.Visible := false;
//          pnlDepth.Visible    := True;
//
//          lblFormationDepth.Caption     := FormatAltitude(Altitude);
//        end
//        else
//        begin
//          pnlAltitude.Visible := false;
//          pnlDepth.Visible    := false;
//        end;
//      end;
    end;
  end;
end;

end.
