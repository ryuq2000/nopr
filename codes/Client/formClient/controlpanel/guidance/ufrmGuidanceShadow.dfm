object frmGuidanceShadow: TfrmGuidanceShadow
  Left = 0
  Top = 0
  Caption = 'Shadow'
  ClientHeight = 435
  ClientWidth = 316
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object grpShadow: TGroupBox
    Left = 0
    Top = 0
    Width = 316
    Height = 435
    Align = alClient
    Color = clBtnFace
    ParentBackground = False
    ParentColor = False
    TabOrder = 0
    object lbl1: TLabel
      Left = 10
      Top = 26
      Width = 79
      Height = 13
      Caption = 'Track to shadow'
    end
    object lbl2: TLabel
      Left = 10
      Top = 50
      Width = 85
      Height = 13
      Caption = 'Standoff distance'
    end
    object lbl3: TLabel
      Left = 10
      Top = 74
      Width = 111
      Height = 13
      Caption = 'Ordered Ground Speed'
    end
    object lbl4: TLabel
      Left = 216
      Top = 74
      Width = 26
      Height = 13
      Caption = 'knots'
    end
    object lbl5: TLabel
      Left = 10
      Top = 99
      Width = 101
      Height = 13
      Caption = 'Actual Ground Speed'
    end
    object lbl6: TLabel
      Left = 216
      Top = 99
      Width = 26
      Height = 13
      Caption = 'knots'
    end
    object lblShadowActualGroundSpeed: TLabel
      Left = 136
      Top = 99
      Width = 22
      Height = 13
      Caption = '14.0'
    end
    object lbl7: TLabel
      Left = 216
      Top = 50
      Width = 14
      Height = 13
      Caption = 'nm'
    end
    object btnTrackToshadow: TSpeedButton
      Left = 216
      Top = 21
      Width = 23
      Height = 22
      Glyph.Data = {
        36090000424D360900000000000036000000280000001F000000180000000100
        18000000000000090000000000000000000000000000000000006161613E3E3E
        3737374040403B3B3B3A3A3A4141414141413A3A3A3A3A3A4040403737373E3E
        3E3D3D3D3838384141413A3A3A3B3B3B4040403737373E3E3E3D3D3D38383841
        41413A3A3A3B3B3B4040403737373E3E3E3C3C3C383838000000B2B2B27C7C7C
        7474747F7F7F7878787777778080808080807878787878788080807474747C7C
        7C7B7B7B7474748080807A7A7A7A7A7A7F7F7F7474747C7C7C7B7B7B74747480
        80807777777979797F7F7F7474747D7D7D7A7A7A757575000000D0D0D0B2B2B2
        B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
        B2B2B2B2B2B2B2B2B2B2BDBDBDBBBBBBB2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
        B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000D0D0D0B2B2B2
        B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2C0C0C04949
        491111111111111111111212121212121A1A1AB2B2B2B2B2B2B2B2B2B2B2B2B2
        B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B4B4B4000000D0D0D0B2B2B2
        B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B29191914D4D4D5454545F5F
        5F6565656262620000004747476565656363634D4D4D4D4D4D797979BABABAB2
        B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B3B3B3000000D0D0D0B2B2B2
        B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B29797976969692929292929298686
        86B2B2B2ADADAD0000007D7D7DB2B2B2ABABAB292929292929545454979797B2
        B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000D0D0D0B2B2B2
        B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B23D3D3D3B3B3BB2B2B2B2B2B2B2B2
        B2B2B2B2ADADAD0000007D7D7DB2B2B2B2B2B2B2B2B2B2B2B26A6A6A0E0E0EB2
        B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000D0D0D0B2B2B2
        B2B2B2B2B2B2B2B2B2B2B2B2B2B2B21616166F6F6FA5A5A5B2B2B2B2B2B2B2B2
        B2B2B2B2ADADAD0000007D7D7DB2B2B2B2B2B2B2B2B2B2B2B2AAAAAA94949413
        13138A8A8AB2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000D0D0D0B2B2B2
        B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2030303757575B2B2B2B2B2B2B2B2B2B2B2
        B2B2B2B2ADADAD0000007D7D7DB2B2B2B2B2B2B2B2B2B2B2B2B2B2B2A4A4A400
        0000868686B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B3B3B3000000D0D0D0B2B2B2
        B2B2B2B2B2B2B2B2B2A6A6A68B8B8B2C2C2C838383B2B2B2B2B2B2B2B2B2B2B2
        B2B2B2B2ADADAD0000007F7F7FB4B4B4B2B2B2B2B2B2B2B2B2B2B2B2A7A7A727
        27277272729C9C9CB2B2B2B2B2B2B2B2B2B2B2B2B3B3B3000000D0D0D0B2B2B2
        B2B2B2B2B2B2B2B2B27B7B7B000000BDBDBDB7B7B7B2B2B2B2B2B2B2B2B2B2B2
        B2B2B2B2ADADAD000000888888BBBBBBB2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
        B2B22C2C2C4C4C4CB2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000D0D0D0B2B2B2
        B2B2B2B2B2B2B2B2B27B7B7B000000B0B0B0B3B3B3B2B2B2B2B2B2BFBFBFB6B6
        B6B2B2B2B1B1B19D9D9DADADADB3B3B3B2B2B2B2B2B2B2B2B2B2B2B2B3B3B3BF
        BFBF3030304C4C4CB2B2B2B2B2B2B2B2B2B2B2B2B4B4B4000000D0D0D0B2B2B2
        B2B2B2B2B2B2B2B2B27B7B7B0000004F4F4F5151515151515151515858585353
        53737373B2B2B2B2B2B2B2B2B28D8D8D51515151515151515151515152525258
        58581616164C4C4CB2B2B2B2B2B2B2B2B2B2B2B2B3B3B3000000D0D0D0B2B2B2
        B2B2B2B2B2B2B2B2B27B7B7B0000002424242525252525252525252525252525
        25575757B2B2B2B2B2B2B2B2B27D7D7D25252525252525252525252525252525
        25250909094C4C4CB2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000D0D0D0B2B2B2
        B2B2B2B2B2B2B2B2B27B7B7B000000AFAFAFB2B2B2B2B2B2B2B2B2B2B2B2B2B2
        B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
        B2B22C2C2C4C4C4CB2B2B2B2B2B2B2B2B2B2B2B2B4B4B4000000D0D0D0B2B2B2
        B2B2B2B2B2B2B2B2B27B7B7B000000BBBBBBB6B6B6B2B2B2B2B2B2B2B2B2B2B2
        B2B2B2B2ADADAD171717848484B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
        B2B22C2C2C4C4C4CB2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000D0D0D0B2B2B2
        B2B2B2B2B2B2B2B2B29999995F5F5F5A5A5A949494B2B2B2B2B2B2B2B2B2B2B2
        B2B2B2B2ADADAD0000007D7D7DB2B2B2B2B2B2B2B2B2B2B2B2B5B5B5B2B2B253
        53535C5C5C828282B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000D0D0D0B2B2B2
        B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2030303757575B2B2B2B2B2B2B2B2B2B2B2
        B2B2B2B2ADADAD0000007D7D7DB2B2B2B2B2B2B2B2B2B2B2B2B7B7B7AFAFAF00
        0000868686B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000D0D0D0B2B2B2
        B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2030303757575B2B2B2B2B2B2B2B2B2B2B2
        B2B2B2B2ADADAD0000007D7D7DB2B2B2B2B2B2B2B2B2B2B2B2B2B2B2A4A4A400
        0000868686B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000D0D0D0B2B2B2
        B2B2B2B2B2B2B2B2B2B2B2B2B2B2B29A9A9A4545454B4B4BB2B2B2B2B2B2B2B2
        B2B2B2B2ADADAD0000007D7D7DB2B2B2B2B2B2B2B2B2B2B2B274747423232399
        9999ACACACB2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B4B4B4000000D0D0D0B2B2B2
        B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B27A7A7A5A5A5A5555555555559494
        94B5B5B5B4B4B4000000838383B7B7B7ADADAD555555555555585858646464B2
        B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B3B3B3000000D0D0D0B2B2B2
        B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B28282822121212121216D6D
        6D9595959898980000006E6E6E9898988B8B8B2121212121215B5B5BB2B2B2B2
        B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000D0D0D0B2B2B2
        B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B23939
        39000000000000000000000000000000090909B2B2B2B2B2B2B2B2B2B2B2B2B2
        B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B4B4B4000000D0D0D0B2B2B2
        B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2A0A0
        A0979797979797979797979797979797999999B2B2B2B2B2B2B2B2B2B2B2B2B2
        B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000}
    end
    object pnlShadowDepth: TPanel
      Left = 2
      Top = 116
      Width = 319
      Height = 58
      BevelOuter = bvNone
      TabOrder = 8
      Visible = False
      object lbl8: TLabel
        Left = 8
        Top = 6
        Width = 72
        Height = 13
        Caption = 'Ordered Depth'
      end
      object lblActShadowDepth: TLabel
        Left = 134
        Top = 33
        Width = 12
        Height = 13
        Caption = '---'
      end
      object lbl9: TLabel
        Left = 214
        Top = 7
        Width = 8
        Height = 13
        Caption = 'm'
      end
      object lbl10: TLabel
        Left = 214
        Top = 34
        Width = 8
        Height = 13
        Caption = 'm'
      end
      object txt1: TStaticText
        Left = 8
        Top = 29
        Width = 66
        Height = 17
        Caption = 'Actual Depth'
        TabOrder = 0
      end
      object txt2: TStaticText
        Left = 120
        Top = 6
        Width = 8
        Height = 17
        Caption = ':'
        TabOrder = 1
      end
      object txt3: TStaticText
        Left = 120
        Top = 31
        Width = 8
        Height = 17
        Caption = ':'
        TabOrder = 2
      end
      object edtOrdShadowDepth: TEdit
        Tag = 2
        Left = 134
        Top = 3
        Width = 74
        Height = 21
        MaxLength = 9
        TabOrder = 3
        Text = '0'
      end
    end
    object pnlShadowAltitude: TPanel
      Left = 3
      Top = 116
      Width = 319
      Height = 58
      BevelOuter = bvNone
      TabOrder = 7
      Visible = False
      object lbl11: TLabel
        Left = 7
        Top = 6
        Width = 80
        Height = 13
        Caption = 'Ordered Altitude'
      end
      object lblActShadowAltitude: TLabel
        Left = 132
        Top = 33
        Width = 12
        Height = 13
        Caption = '---'
      end
      object lbl12: TLabel
        Left = 213
        Top = 7
        Width = 20
        Height = 13
        Caption = 'feet'
      end
      object lbl13: TLabel
        Left = 212
        Top = 34
        Width = 20
        Height = 13
        Caption = 'feet'
      end
      object txt4: TStaticText
        Left = 7
        Top = 29
        Width = 74
        Height = 17
        Caption = 'Actual Altitude'
        TabOrder = 0
      end
      object txt5: TStaticText
        Left = 119
        Top = 6
        Width = 8
        Height = 17
        Caption = ':'
        TabOrder = 1
      end
      object txt6: TStaticText
        Left = 118
        Top = 31
        Width = 8
        Height = 17
        Caption = ':'
        TabOrder = 2
      end
      object edtOrdShadowAltitude: TEdit
        Tag = 1
        Left = 133
        Top = 4
        Width = 74
        Height = 21
        MaxLength = 9
        TabOrder = 3
        Text = '0'
      end
    end
    object txt7: TStaticText
      Left = 122
      Top = 24
      Width = 8
      Height = 17
      Caption = ':'
      TabOrder = 0
    end
    object edtTrackToShadow: TEdit
      Left = 136
      Top = 22
      Width = 74
      Height = 21
      Enabled = False
      TabOrder = 1
    end
    object txt8: TStaticText
      Left = 122
      Top = 48
      Width = 8
      Height = 17
      Caption = ':'
      TabOrder = 2
    end
    object edtStandoffDistanceShadow: TEdit
      Left = 136
      Top = 46
      Width = 74
      Height = 21
      MaxLength = 11
      TabOrder = 3
    end
    object txt9: TStaticText
      Left = 122
      Top = 72
      Width = 8
      Height = 17
      Caption = ':'
      TabOrder = 4
    end
    object edtShadowOrdeeredGroundSpeed: TEdit
      Left = 136
      Top = 70
      Width = 74
      Height = 21
      MaxLength = 11
      TabOrder = 5
    end
    object txt10: TStaticText
      Left = 122
      Top = 97
      Width = 8
      Height = 17
      Caption = ':'
      TabOrder = 6
    end
  end
end
