object frmGuidanceSinuation: TfrmGuidanceSinuation
  Left = 0
  Top = 0
  Caption = 'Sinuation'
  ClientHeight = 352
  ClientWidth = 313
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object grpSinuation: TGroupBox
    Left = 0
    Top = 0
    Width = 313
    Height = 352
    Align = alClient
    Color = clBtnFace
    ParentBackground = False
    ParentColor = False
    TabOrder = 0
    object lbl1: TLabel
      Left = 10
      Top = 26
      Width = 60
      Height = 13
      Caption = 'Base Course'
    end
    object lbl2: TLabel
      Left = 10
      Top = 50
      Width = 30
      Height = 13
      Caption = 'Period'
    end
    object lbl3: TLabel
      Left = 10
      Top = 74
      Width = 47
      Height = 13
      Caption = 'Amplitude'
    end
    object lbl4: TLabel
      Left = 216
      Top = 26
      Width = 48
      Height = 13
      Caption = 'degrees T'
    end
    object lbl5: TLabel
      Left = 10
      Top = 98
      Width = 111
      Height = 13
      Caption = 'Ordered Ground Speed'
    end
    object lbl6: TLabel
      Left = 216
      Top = 98
      Width = 26
      Height = 13
      Caption = 'knots'
    end
    object lbl7: TLabel
      Left = 10
      Top = 123
      Width = 101
      Height = 13
      Caption = 'Actual Ground Speed'
    end
    object lbl8: TLabel
      Left = 216
      Top = 123
      Width = 26
      Height = 13
      Caption = 'knots'
    end
    object lblSinuationActualGroundSpeed: TLabel
      Left = 136
      Top = 123
      Width = 22
      Height = 13
      Caption = '14.0'
    end
    object lbl9: TLabel
      Left = 216
      Top = 50
      Width = 14
      Height = 13
      Caption = 'nm'
    end
    object lbl10: TLabel
      Left = 216
      Top = 74
      Width = 14
      Height = 13
      Caption = 'nm'
    end
    object txt1: TStaticText
      Left = 122
      Top = 24
      Width = 8
      Height = 17
      Caption = ':'
      TabOrder = 0
    end
    object edtSinuationBaseCourse: TEdit
      Tag = 1
      Left = 136
      Top = 22
      Width = 74
      Height = 21
      MaxLength = 5
      TabOrder = 1
      OnKeyPress = OnKeyPressed
    end
    object txt2: TStaticText
      Left = 122
      Top = 48
      Width = 8
      Height = 17
      Caption = ':'
      TabOrder = 2
    end
    object edtSinuationBasePeriod: TEdit
      Tag = 2
      Left = 136
      Top = 46
      Width = 74
      Height = 21
      MaxLength = 11
      TabOrder = 3
      OnKeyPress = OnKeyPressed
    end
    object txt3: TStaticText
      Left = 122
      Top = 72
      Width = 8
      Height = 17
      Caption = ':'
      TabOrder = 4
    end
    object edtSinuationAmplitude: TEdit
      Tag = 3
      Left = 136
      Top = 70
      Width = 74
      Height = 21
      MaxLength = 11
      TabOrder = 5
      OnKeyPress = OnKeyPressed
    end
    object txt4: TStaticText
      Left = 122
      Top = 96
      Width = 8
      Height = 17
      Caption = ':'
      TabOrder = 6
    end
    object edtSinuationOrderedgroundSpeed: TEdit
      Tag = 4
      Left = 136
      Top = 94
      Width = 74
      Height = 21
      MaxLength = 11
      TabOrder = 7
      OnKeyPress = OnKeyPressed
    end
    object txt5: TStaticText
      Left = 122
      Top = 121
      Width = 8
      Height = 17
      Caption = ':'
      TabOrder = 8
    end
  end
end
