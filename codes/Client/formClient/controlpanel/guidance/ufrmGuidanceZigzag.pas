unit ufrmGuidanceZigzag;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ufrmGuidanceType, uT3Track;

type
  TfrmGuidanceZigzag = class(TfrmGuidanceType)
    grpZigZag: TGroupBox;
    lbl1: TLabel;
    lbl2: TLabel;
    lbl3: TLabel;
    lbl4: TLabel;
    lbl5: TLabel;
    lbl6: TLabel;
    lbl7: TLabel;
    lbl8: TLabel;
    lblZigZagActualGroundSpeed: TLabel;
    lbl9: TLabel;
    lbl10: TLabel;
    txt1: TStaticText;
    edtZigZagBaseCourse: TEdit;
    txt2: TStaticText;
    edtZigZagPeriod: TEdit;
    txt3: TStaticText;
    edtZigZagAmplitude: TEdit;
    txt4: TStaticText;
    edtZigZagOrderedGroundSpeed: TEdit;
    txt5: TStaticText;
    procedure OnKeyPressZigzag(Sender: TObject; var Key: Char);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure UpdateForm; override;
    procedure SetControlledObject(aObj : TT3Track); override;
  end;

var
  frmGuidanceZigzag: TfrmGuidanceZigzag;

implementation

uses uT3Vehicle, uBaseCoordSystem, uT3ZigzagGuide,
  uGameData_TTT, uT3ClientManager;

{$R *.dfm}

{ TfrmGuidanceZigzag }

procedure TfrmGuidanceZigzag.OnKeyPressZigzag(Sender: TObject; var Key: Char);
var
  ValKey : set of AnsiChar;
  Course, Period, Amplitudo, Speed : Double;
  v : TT3Vehicle;
  rec: TRecCmd_PlatformGuidance;
begin
  inherited;

  ValKey := [#48 .. #57, #8, #13, #46];
  if not(CharInSet(Key, ValKey)) then
    Key := #0;

  if Key = #13 then
  begin
    if FControlledPlatform is TT3Vehicle then
    begin
      v := FControlledPlatform as TT3Vehicle;

      case TEdit(sender).Tag of
        1 : begin
              //Zigzag Course
              Course := StrToFloat(edtZigZagBaseCourse.Text);
              if Course > 360 then
              begin
                Course := 0;
              end;

              rec.PlatfomID := v.InstanceIndex;
              rec.OrderID := CORD_ID_MOVE;
              rec.OrderType := CORD_TYPE_COURSE;
              rec.OrderParam := Course;

              clientManager.NetCmdSender.CmdPlatformGuidance(rec);
            end;
        2 : begin
              //Period Zigzag
              if TryStrToFloat(edtZigZagPeriod.Text, Period) then
              begin
                if Period > 0 then
                begin
                  rec.PlatfomID := v.InstanceIndex;
                  rec.OrderID := CORD_ID_MOVE;
                  rec.OrderType := CORD_TYPE_ZIGZAG_PERIOD;
                  rec.OrderParam := Period;

                  clientManager.NetCmdSender.CmdPlatformGuidance(rec);
                end
                else
                begin
                  edtZigZagPeriod.Text := FormatFloat('0.00', TT3ZigZagGuidance(v.VehicleGuidance).PeriodZigzag);
                  TacticalDisplayStatus('Zigzag Invalid Periode Value');
                end;
              end
              else
              begin
                edtZigZagPeriod.Text := FormatFloat('0.00', TT3ZigZagGuidance(v.VehicleGuidance).PeriodZigzag);
                TacticalDisplayStatus('Zigzag Invalid Periode Value');
              end;
            end;
        3 : begin
              //Amplitudo Zigzaq
              if TryStrToFloat(edtZigZagAmplitude.Text, Amplitudo) then
              begin
                if Amplitudo > 0 then
                begin
                  rec.PlatfomID := v.InstanceIndex;
                  rec.OrderID := CORD_ID_MOVE;
                  rec.OrderType := CORD_TYPE_ZIGZAG_AMPLITUDO;
                  rec.OrderParam := Amplitudo;

                  clientManager.NetCmdSender.CmdPlatformGuidance(rec);
                end
                else
                begin
                  edtZigZagAmplitude.Text := FormatFloat('0.00',  TT3ZigZagGuidance(v.VehicleGuidance).AmplitudoZigzag);
                  TacticalDisplayStatus('Zigzag Invalid Amplitudo Value');
                end;
              end
              else
              begin
                edtZigZagAmplitude.Text := FormatFloat('0.00',  TT3ZigZagGuidance(v.VehicleGuidance).AmplitudoZigzag);
                TacticalDisplayStatus('Zigzag Invalid Amplitudo Value');
              end;

            end;
        4 : begin
              //Speed Zigzaq
              Speed := StrToFloat(edtZigZagOrderedGroundSpeed.Text);

//              if (v.FuelRemaining <= 0 ) then
//              begin
//                if simMgrClient.ISInstructor then
//                  frmTacticalDisplay.addStatus(v.TrackLabel+' Out of Fuel')
//                else
//                  frmTacticalDisplay.addStatus(IntToStr(v.TrackNumber)+' Out of Fuel');
//
//                Exit;
//              end;
//
              if (v.Mover.MaxSpeed < Speed ) then
              begin
                edtZigZagOrderedGroundSpeed.Text := FloatToStr(v.Mover.MaxSpeed);
                TacticalDisplayStatus(TT3Track(FControlledTrack).TrackLabel+
                      ' Over Speed = ' + FloatToStr(Speed));

                Speed := v.Mover.MaxSpeed;     //mk test
              end
              else if (Speed < v.Mover.MinSpeed) then //choco
              begin
                edtZigZagOrderedGroundSpeed.Text := FloatToStr(v.Mover.MinSpeed);
                TacticalDisplayStatus(TT3Track(FControlledTrack).TrackLabel+
                      ' MIN SPEED' );

                Speed := v.Mover.MinSpeed;
              end;

              rec.PlatfomID := v.InstanceIndex;
              rec.OrderID := CORD_ID_MOVE;
              rec.OrderType := CORD_TYPE_SPEED;
              rec.OrderParam := Speed;

              clientManager.NetCmdSender.CmdPlatformGuidance(rec);
            end;
      end;
    end;
  end;
end;

procedure TfrmGuidanceZigzag.SetControlledObject(aObj: TT3Track);
begin
  inherited;

  if (FControlledPlatform <> nil) and (FControlledPlatform is TT3Vehicle) then
  begin
    with TT3Vehicle(FControlledPlatform) do
    begin
      if VehicleGuidance is TT3ZigZagGuidance then
      begin
        edtZigZagBaseCourse.Text            := IntToStr(round(TT3ZigZagGuidance(VehicleGuidance).OrderedZigzag));
        edtZigZagPeriod.Text                := FormatFloat('0.00', TT3ZigZagGuidance(VehicleGuidance).PeriodZigzag);
        edtZigZagAmplitude.Text             := FormatFloat('0.00', TT3ZigZagGuidance(VehicleGuidance).AmplitudoZigzag);
      end
      else
      begin
        edtZigZagBaseCourse.Text            := IntToStr(0);
        edtZigZagPeriod.Text                := FormatFloat('0.00', 0);
        edtZigZagAmplitude.Text             := FormatFloat('0.00', 0);
      end;

      edtZigZagOrderedGroundSpeed.Text    := FloatToStr(OrderedSpeed);
      lblZigZagActualGroundSpeed.Caption  := FormatSpeed(Speed);

    end;
  end;

end;

procedure TfrmGuidanceZigzag.UpdateForm;
begin
  inherited;

  if (FControlledPlatform <> nil) and (FControlledPlatform is TT3Vehicle) then
  begin
    with TT3Vehicle(FControlledPlatform) do
    begin
      lblZigZagActualGroundSpeed.Caption := FormatSpeed(Speed);
    end;
  end;
end;

end.
