unit ufrmGuidanceShadow;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, Buttons, ufrmGuidanceType, uT3Track;

type
  TfrmGuidanceShadow = class(TfrmGuidanceType)
    grpShadow: TGroupBox;
    lbl1: TLabel;
    lbl2: TLabel;
    lbl3: TLabel;
    lbl4: TLabel;
    lbl5: TLabel;
    lbl6: TLabel;
    lblShadowActualGroundSpeed: TLabel;
    lbl7: TLabel;
    btnTrackToshadow: TSpeedButton;
    pnlShadowDepth: TPanel;
    lbl8: TLabel;
    lblActShadowDepth: TLabel;
    lbl9: TLabel;
    lbl10: TLabel;
    txt1: TStaticText;
    txt2: TStaticText;
    txt3: TStaticText;
    edtOrdShadowDepth: TEdit;
    pnlShadowAltitude: TPanel;
    lbl11: TLabel;
    lblActShadowAltitude: TLabel;
    lbl12: TLabel;
    lbl13: TLabel;
    txt4: TStaticText;
    txt5: TStaticText;
    txt6: TStaticText;
    edtOrdShadowAltitude: TEdit;
    txt7: TStaticText;
    edtTrackToShadow: TEdit;
    txt8: TStaticText;
    edtStandoffDistanceShadow: TEdit;
    txt9: TStaticText;
    edtShadowOrdeeredGroundSpeed: TEdit;
    txt10: TStaticText;
  private
    { Private declarations }
  public
    { Public declarations }
    procedure UpdateForm; override;
    procedure SetControlledObject(aObj : TT3Track); override;
  end;

var
  frmGuidanceShadow: TfrmGuidanceShadow;

implementation

{$R *.dfm}

{ TfrmGuidanceShadow }

procedure TfrmGuidanceShadow.SetControlledObject(aObj: TT3Track);
begin
  inherited;

end;

procedure TfrmGuidanceShadow.UpdateForm;
begin
  inherited;

end;

end.
