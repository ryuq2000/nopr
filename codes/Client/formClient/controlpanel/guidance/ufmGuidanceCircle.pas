unit ufmGuidanceCircle;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, 
  Dialogs, Menus, StdCtrls, ExtCtrls, Buttons, ufmGuidanceType, uT3Track;

type
  TfmGuidanceCircle = class(TfmGuidanceType)
    scrlbx1: TScrollBox;
    lbl1: TLabel;
    lbl2: TLabel;
    lbl3: TLabel;
    bvl1: TBevel;
    lbl4: TLabel;
    lbl5: TLabel;
    lbl6: TLabel;
    lbl7: TLabel;
    lblCircleActualGroundSpeed: TLabel;
    lbllb1: TLabel;
    btnCircleMode: TSpeedButton;
    btnCircleDirection: TSpeedButton;
    pnlPosition: TPanel;
    lbl8: TLabel;
    btnCircleOnPositionPosition: TSpeedButton;
    txt1: TStaticText;
    edtCircleOnPositionPosition: TEdit;
    pnlTrack: TPanel;
    lbl9: TLabel;
    btnCircleOnTrackTrack: TSpeedButton;
    lbl10: TLabel;
    lblCircleDegree: TLabel;
    lbl11: TLabel;
    lbl12: TLabel;
    btnCircleOnTrackBearing: TSpeedButton;
    txt2: TStaticText;
    edtCircleOnTrackRange: TEdit;
    txt3: TStaticText;
    edtCircleOnTrackBearing: TEdit;
    txt4: TStaticText;
    edtCircleOnTrackTrack: TEdit;
    txt5: TStaticText;
    edtCircleMode: TEdit;
    txt6: TStaticText;
    txt7: TStaticText;
    edtCircleRadius: TEdit;
    txt8: TStaticText;
    edtCircleDirection: TEdit;
    txt9: TStaticText;
    edtCircleOrderedGroundSpeed: TEdit;
    txt10: TStaticText;
    pmCircleMode: TPopupMenu;
    Position1: TMenuItem;
    track1: TMenuItem;
    pmCircleBearingDegree: TPopupMenu;
    True1: TMenuItem;
    Relative1: TMenuItem;
    pmCircleDirection: TPopupMenu;
    Clockwise1: TMenuItem;
    Counterclockwise1: TMenuItem;
    procedure btnMenuClick(Sender: TObject);
    procedure OnKeyPress(Sender: TObject; var Key: Char);
    procedure onItemModeClick(Sender: TObject);
    procedure onItemBearingClick(Sender: TObject);
    procedure onItemDirectionClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure EmptyField; override;
    procedure UpdateForm; override;
    procedure SetControlledObject(aObj : TT3Track); override;
  end;

implementation

uses
  uT3Vehicle, uBaseCoordSystem, uGameData_TTT, uT3ClientManager, uT3CircleGuide;
{$R *.dfm}

procedure TfmGuidanceCircle.btnMenuClick(Sender: TObject);
var
  pos: TPoint;
begin
  inherited;
  GetCursorPos(pos);
  case TButton(Sender).Tag of
    1 : pmCircleMode.Popup(pos.X, pos.Y);
    2 : pmCircleBearingDegree.Popup(pos.X, pos.Y);
    3 : pmCircleDirection.Popup(pos.X, pos.Y);
  end;
end;

procedure TfmGuidanceCircle.EmptyField;
begin
  inherited;

end;

procedure TfmGuidanceCircle.onItemBearingClick(Sender: TObject);
var
  rec: TRecCmd_Platform;
  v: TT3Vehicle;
begin
  inherited;

  if Assigned(FControlledPlatform) and (FControlledPlatform is TT3Vehicle) then
  begin

    v := FControlledPlatform as TT3Vehicle;
    case TMenuItem(Sender).Tag of
      1 :
      begin
        lblCircleDegree.Caption  := 'degrees T';

        rec.PlatfomID   := v.InstanceIndex;
        rec.OrderID     := CORD_ID_MOVE;
        rec.OrderType   := CORD_TYPE_CIRCLE_BEARING_STATE;
        rec.OrderParam  := 1;

        clientManager.NetCmdSender.CmdPlatform(rec);
      end;
      2 :
      begin
        lblCircleDegree.Caption := 'degrees R';

        rec.PlatfomID   := v.InstanceIndex;
        rec.OrderID     := CORD_ID_MOVE;
        rec.OrderType   := CORD_TYPE_CIRCLE_BEARING_STATE;
        rec.OrderParam  := 2;

        clientManager.NetCmdSender.CmdPlatform(rec);
      end;
    end;

  end;
end;

procedure TfmGuidanceCircle.onItemDirectionClick(Sender: TObject);
var
  rec: TRecCmd_Platform;
  v: TT3Vehicle;
begin
  inherited;

  if Assigned(FControlledPlatform) and (FControlledPlatform is TT3Vehicle) then
  begin
    v := FControlledPlatform as TT3Vehicle;
    case TMenuItem(Sender).Tag of
      1 :
      begin
        edtCircleDirection.Text := 'Clockwise';

        rec.PlatfomID   := v.InstanceIndex;
        rec.OrderID     := CORD_ID_MOVE;
        rec.OrderType   := CORD_TYPE_COURSE;
        rec.OrderParam  := 90;

        clientManager.NetCmdSender.CmdPlatform(rec);

      end;
      2 :
      begin
        edtCircleDirection.Text := 'Counter-clockwise';

        rec.PlatfomID   := v.InstanceIndex;
        rec.OrderID     := CORD_ID_MOVE;
        rec.OrderType   := CORD_TYPE_COURSE;
        rec.OrderParam  := -90;

        clientManager.NetCmdSender.CmdPlatform(rec);

      end;
    end;
  end;
end;

procedure TfmGuidanceCircle.onItemModeClick(Sender: TObject);
var
  rec: TRecCmd_Platform;
  v: TT3Vehicle;
begin
  inherited;

  if Assigned(FControlledPlatform) and (FControlledPlatform is TT3Vehicle) then
  begin
    v := FControlledPlatform as TT3Vehicle;
    case TMenuItem(Sender).Tag of
      1 :
      begin
        edtCircleMode.Text  := 'Position';
        pnlPosition.Visible := true;
        pnlTrack.Visible    := false;

        rec.PlatfomID   := v.InstanceIndex;
        rec.OrderID     := CORD_ID_MOVE;
        rec.OrderType   := CORD_TYPE_CIRCLE_MODE;
        rec.OrderParam  := 1;

        clientManager.NetCmdSender.CmdPlatform(rec);
      end;
      2 :
      begin
        edtCircleMode.Text  := 'Track';
        pnlPosition.Visible := False;
        pnlTrack.Visible    := True;

        rec.PlatfomID   := v.InstanceIndex;
        rec.OrderID     := CORD_ID_MOVE;
        rec.OrderType   := CORD_TYPE_CIRCLE_MODE;
        rec.OrderParam  := 2;

        clientManager.NetCmdSender.CmdPlatform(rec);
      end;
    end;
  end;
end;

procedure TfmGuidanceCircle.OnKeyPress(Sender: TObject; var Key: Char);
var
  ValKey : set of AnsiChar;
  val : Double;
  validInput : Boolean;
  v: TT3Vehicle;
  rec: TRecCmd_Platform;
begin

  ValKey := [#48 .. #57, #8, #13, #46];
  if not(CharInSet(Key, ValKey)) then
    Key := #0;

  if Key = #13 then
  begin
    if Assigned(FControlledPlatform) and (FControlledPlatform is TT3Vehicle) then
    begin
      v := FControlledPlatform as TT3Vehicle;

      case TEdit(Sender).Tag of

        1 :
        begin
          if TryStrToFloat(TEdit(Sender).Text, val) then
          begin
            rec.PlatfomID   := v.InstanceIndex;
            rec.OrderID     := CORD_ID_MOVE;
            rec.OrderType   := CORD_TYPE_CIRCLE_BEARING;
            rec.OrderParam  := val;

            clientManager.NetCmdSender.CmdPlatform(rec);
          end
          else
          begin
            TacticalDisplayStatus('It is not valid input');
            if Assigned(v.VehicleGuidance) and (v.VehicleGuidance is TT3CircleGuidance) then
              TEdit(Sender).Text := FormatFloat('0.00',
                    TT3CircleGuidance(v.VehicleGuidance).CircleBearing);
          end;
        end;
        2 :
        begin
          if TryStrToFloat(TEdit(Sender).Text, val) then
          begin
            rec.PlatfomID   := v.InstanceIndex;
            rec.OrderID     := CORD_ID_MOVE;
            rec.OrderType   := CORD_TYPE_CIRCLE_RANGE;
            rec.OrderParam  := val;

            clientManager.NetCmdSender.CmdPlatform(rec);
          end
          else
          begin
            TacticalDisplayStatus('It is not valid input');
            if Assigned(v.VehicleGuidance) and (v.VehicleGuidance is TT3CircleGuidance) then
              TEdit(Sender).Text := FormatFloat('0.00',
                    TT3CircleGuidance(v.VehicleGuidance).CircleRange);
          end;
        end;
        3 :
        begin
          validInput := False;
          if TryStrToFloat(edtCircleRadius.Text, val) then
          begin
            if val >= 0 then
            begin

              rec.PlatfomID   := v.InstanceIndex;
              rec.OrderID     := CORD_ID_MOVE;
              rec.OrderType   := CORD_TYPE_CIRCLE_RANGE;
              rec.OrderParam  := val;

              clientManager.NetCmdSender.CmdPlatform(rec);
              validInput := True;
            end;
          end;

          if not validInput then
          begin
            TacticalDisplayStatus('It is not valid input');
            if Assigned(v.VehicleGuidance) and (v.VehicleGuidance is TT3CircleGuidance) then
              TEdit(Sender).Text := FormatFloat('0.00',
                    TT3CircleGuidance(v.VehicleGuidance).CircleRadius);
          end;
        end;
        4 :
        begin
          OnOrderGroundSpeedChange(Sender);
        end;
      end;
    end;
  end;
end;

procedure TfmGuidanceCircle.SetControlledObject(aObj: TT3Track);
var
  track: TT3Track;
begin
  inherited;

  if (FControlledPlatform <> nil) and (FControlledPlatform is TT3Vehicle) then
  begin
    with TT3Vehicle(FControlledPlatform) do
    begin
      lblCircleActualGroundSpeed.Caption  := FormatSpeed(Speed);

      if Assigned(VehicleGuidance) and (VehicleGuidance is TT3CircleGuidance) then
      begin
        with TT3CircleGuidance(VehicleGuidance) do
        begin

          case (TT3CircleGuidance(VehicleGuidance).CircleMode) of

            cModePosition :
              begin
                edtCircleMode.Text := 'Position';
                pnlTrack.Visible   := false;
                pnlPosition.Visible:= true;

                edtCircleOnPositionPosition.Text := formatDM_longitude(CenterCirclePointX)
                                    + ' '+ formatDM_latitude(CenterCirclePointY);
              end;
            cModeTrack :
              begin
                edtCircleMode.Text := 'Track';
                pnlTrack.Visible   := true;
                pnlPosition.Visible:= false;

                if TargetInstanceIndex > 0 then
                begin
                  track := clientManager.FindTrack(TargetInstanceIndex);
                  if Assigned(track) then
                    edtCircleOnTrackTrack.Text := track.TrackLabelInfo;

                  edtCircleOnTrackBearing.Text := FormatFloat('0.00', CircleBearing);

                  case CircleBearingState of
                    cBearingTrue      : lblCircleDegree.Caption  := 'degrees T';
                    cBearingRelative  : lblCircleDegree.Caption  := 'degrees R';
                  end;

                  edtCircleOnTrackRange.Text   := FormatFloat('0.00', CircleRange);
                end;
              end;
          end;

          if CircleRadius <= 0 then
            CircleRadius := 1;

          edtCircleRadius.Text  := FormatFloat('0.0', CircleRadius);

          if CircleDirection = 90 then
            edtCircleDirection.Text := 'Clockwise'
          else
          if CircleDirection = -90 then
            edtCircleDirection.Text := 'Counter-clockwise';
        end;

        edtCircleOrderedGroundSpeed.Text := FloatToStr(OrderedSpeed);
      end;

    end;
  end;
end;

procedure TfmGuidanceCircle.UpdateForm;
begin
  inherited;
  if (FControlledPlatform <> nil) and (FControlledPlatform is TT3Vehicle) then
  begin
    with TT3Vehicle(FControlledPlatform) do
    begin
      lblCircleActualGroundSpeed.Caption  := FormatSpeed(Speed);
    end;
  end;

end;

end.
