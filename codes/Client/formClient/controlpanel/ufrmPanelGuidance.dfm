object frmPanelGuidance: TfrmPanelGuidance
  Left = 0
  Top = 0
  Caption = 'frmPanelGuidance'
  ClientHeight = 435
  ClientWidth = 305
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object PanelGuidanceControlChoices: TPanel
    Left = 0
    Top = 0
    Width = 305
    Height = 41
    Align = alTop
    BevelOuter = bvNone
    ParentBackground = False
    TabOrder = 0
    object btnMenuGuidance: TSpeedButton
      Tag = 1
      Left = 211
      Top = 8
      Width = 23
      Height = 22
      Glyph.Data = {
        D6050000424DD605000000000000360000002800000017000000140000000100
        180000000000A005000000000000000000000000000000000000B2B2B2B2B2B2
        B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
        B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B200
        0000B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
        B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
        B2B2B2B2B2B2B2000000B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
        B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
        B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000B2B2B2B2B2B2B2B2B2B2B2B2B2B2
        B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
        B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000B2B2B2B2B2B2
        B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
        B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B200
        0000B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2C1C1C1B2B2B2B2B2B2
        B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
        B2B2B2B2B2B2B2000000B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B200000000
        0000B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
        B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000B2B2B2B2B2B2B2B2B2B2B2B2B2B2
        B2B2B2B2000000000000000000000000C1C1C1B2B2B2B2B2B2B2B2B2B2B2B2B2
        B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000B2B2B2B2B2B2
        B2B2B2B2B2B2B2B2B2B2B2B2000000000000000000000000000000000000B2B2
        B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B200
        0000B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000000000000000000000
        000000000000000000000000C1C1C1B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
        B2B2B2B2B2B2B2000000B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B200000000
        0000000000000000000000000000000000000000000000000000B2B2B2B2B2B2
        B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000B2B2B2B2B2B2B2B2B2B2B2B2B2B2
        B2B2B2B2000000000000000000000000000000000000000000000000B2B2B2B2
        B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000B2B2B2B2B2B2
        B2B2B2B2B2B2B2B2B2B2B2B2000000000000000000000000000000000000B2B2
        B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B200
        0000B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000000000000000000000
        B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
        B2B2B2B2B2B2B2000000B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B200000000
        0000B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
        B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000B2B2B2B2B2B2B2B2B2B2B2B2B2B2
        B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
        B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000B2B2B2B2B2B2
        B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
        B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B200
        0000B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
        B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
        B2B2B2B2B2B2B2000000B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
        B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
        B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000B2B2B2B2B2B2B2B2B2B2B2B2B2B2
        B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
        B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000}
      OnClick = btnMenuGuidanceClick
    end
    object edGuidance: TEdit
      Left = 6
      Top = 9
      Width = 199
      Height = 21
      Enabled = False
      TabOrder = 0
      Text = 'Straight Line'
    end
  end
  object pmGuidance: TPopupMenu
    Left = 242
    Top = 6
    object mnStraightLine1: TMenuItem
      Tag = 1
      Caption = 'Straight Line'
      OnClick = OnGuidanceClick
    end
    object mnHelm1: TMenuItem
      Tag = 2
      Caption = 'Helm'
      OnClick = OnGuidanceClick
    end
    object mnCircle: TMenuItem
      Tag = 3
      Caption = 'Circle'
      OnClick = OnGuidanceClick
    end
    object mnStation: TMenuItem
      Tag = 4
      Caption = 'Station'
      OnClick = OnGuidanceClick
    end
    object mnZigzag1: TMenuItem
      Tag = 5
      Caption = 'Zigzag'
      OnClick = OnGuidanceClick
      object Short1: TMenuItem
        Tag = 14
        Caption = 'Short'
      end
      object Long1: TMenuItem
        Tag = 15
        Caption = 'Long'
      end
      object VeryLong1: TMenuItem
        Tag = 16
        Caption = 'Very Long'
      end
    end
    object mnSinuation1: TMenuItem
      Tag = 6
      Caption = 'Sinuation'
      OnClick = OnGuidanceClick
    end
    object mnFormation2: TMenuItem
      Tag = 7
      Caption = 'Formation'
      Enabled = False
      OnClick = OnGuidanceClick
    end
    object mnEvasion1: TMenuItem
      Tag = 8
      Caption = 'Evasion'
      OnClick = OnGuidanceClick
    end
    object mnWaypoint1: TMenuItem
      Tag = 9
      Caption = 'Waypoint'
      OnClick = OnGuidanceClick
    end
    object mnOutrun1: TMenuItem
      Tag = 10
      Caption = 'Outrun'
      OnClick = OnGuidanceClick
    end
    object mnEngagement1: TMenuItem
      Tag = 11
      Caption = 'Engagement'
      Enabled = False
      OnClick = OnGuidanceClick
    end
    object mnShadow1: TMenuItem
      Tag = 12
      Caption = 'Shadow'
      OnClick = OnGuidanceClick
    end
    object mnReturntoBase1: TMenuItem
      Tag = 13
      Caption = 'Return to Base'
      OnClick = OnGuidanceClick
    end
  end
end
