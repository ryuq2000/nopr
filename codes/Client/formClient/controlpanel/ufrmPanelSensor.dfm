object frmPanelSensor: TfrmPanelSensor
  Left = 0
  Top = 0
  Caption = 'frmPanelSensor'
  ClientHeight = 416
  ClientWidth = 342
  Color = clBtnFace
  DoubleBuffered = True
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object PanelSensorChoices: TPanel
    Left = 0
    Top = 0
    Width = 342
    Height = 115
    Align = alTop
    BevelOuter = bvNone
    ParentBackground = False
    TabOrder = 0
    object lstSensor: TListView
      Left = 0
      Top = 0
      Width = 342
      Height = 115
      Align = alClient
      Color = clMenu
      Columns = <
        item
          AutoSize = True
          Caption = 'Name'
        end
        item
          Alignment = taCenter
          AutoSize = True
          Caption = 'Status'
        end>
      DoubleBuffered = True
      GridLines = True
      HideSelection = False
      ReadOnly = True
      RowSelect = True
      ParentDoubleBuffered = False
      TabOrder = 0
      ViewStyle = vsReport
      OnSelectItem = lstSensorSelectItem
    end
  end
end
