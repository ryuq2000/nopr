unit ufrmFloatingDecoy;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls,ufrmECMType, uGameData_TTT, uT3MountedECM,
  uT3MountedFloatingDecoy;

type
  TfrmFloatingDecoy = class(TfrmECMType)
    grpFloatingDecoy: TGroupBox;
    lbl1: TLabel;
    bvl1: TBevel;
    lbl2: TLabel;
    lblFloatingDecoyQuantity: TLabel;
    btnFloatingDecoyDeploy: TButton;
    procedure btnFloatingDecoyDeployClick(Sender: TObject);
  private
    { Private declarations }
    procedure InitPacket(var rec : TRecCmdFloatingDecoyOnBoard);
  public
    { Public declarations }
    procedure UpdateForm; override;
    procedure SetControlledECM(aObj : TObject); override;
  end;

var
  frmFloatingDecoy: TfrmFloatingDecoy;

implementation

uses
  uGlobalVar;

{$R *.dfm}

{ TfrmFloatingDecoy }

procedure TfrmFloatingDecoy.btnFloatingDecoyDeployClick(Sender: TObject);
var
  rec : TRecCmdFloatingDecoyOnBoard;
begin
  inherited;

  if FControlledECM = nil then
    exit;

  if not (FControlledECM is TT3MountedFloatingDecoy) then
    exit;

  if TT3MountedFloatingDecoy(FControlledECM).Status = esDamaged then
    Exit;

  if TT3MountedFloatingDecoy(FControlledECM).Quantity <= 0 then
    exit;

  InitPacket(rec);
  rec.OrderID := CORD_FLOATING_DEPLOY;
  simManager.NetCmdSender.CmdFloatingDecoyOnBoard(rec);
end;

procedure TfrmFloatingDecoy.InitPacket(var rec: TRecCmdFloatingDecoyOnBoard);
begin
  if FControlledECM is TT3MountedFloatingDecoy then
  begin
    rec.PlatformIndex := TT3MountedFloatingDecoy(FControlledECM).PlatformParentInstanceIndex;
    rec.AssetIndex    := TT3MountedFloatingDecoy(FControlledECM).InstanceIndex;
    rec.OrderID       := 0;
    rec.Value         := 0;
  end;
end;

procedure TfrmFloatingDecoy.SetControlledECM(aObj: TObject);
begin
  inherited;
  UpdateForm;
end;

procedure TfrmFloatingDecoy.UpdateForm;
begin
  inherited;

  if Assigned( FControlledECM ) then

  with TT3MountedFloatingDecoy(FControlledECM) do
  begin
    lblFloatingDecoyQuantity.Caption := IntToStr(Quantity);

    if Status = esDamaged then
      btnFloatingDecoyDeploy.Enabled := False
    else
      btnFloatingDecoyDeploy.Enabled := True
  end;

end;

end.
