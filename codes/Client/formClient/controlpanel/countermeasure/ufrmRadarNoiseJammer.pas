unit ufrmRadarNoiseJammer;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, Buttons, ufrmECMType, uGameData_TTT, uT3MountedECM,
  uT3MountedRadarNoiseJammer, Menus, uBaseCoordSystem;

type
  TfrmRadarNoiseJammer = class(TfrmECMType)
    grpRadarNoiseJammer: TGroupBox;
    scrlbx1: TScrollBox;
    bvl1: TBevel;
    btnComboRadarJammingControlMode: TSpeedButton;
    lbl1: TLabel;
    lbl2: TLabel;
    lbl3: TLabel;
    btnRadarJammingControlActivationOff: TSpeedButton;
    btnRadarJammingControlActivationOn: TSpeedButton;
    pnlRadarJammingMode: TPanel;
    grpRadarJammingBarrageMode: TGroupBox;
    lbl4: TLabel;
    lbl5: TLabel;
    lbl6: TLabel;
    lblFrequency: TLabel;
    lblBandwith: TLabel;
    lbl9: TLabel;
    edtRadarJammingBarrageCenter: TEdit;
    edtRadarJammingBarrageBearing: TEdit;
    edtRadarJammingBarrageBandwidth: TEdit;
    grpRadarJammingSpotNumberMode: TGroupBox;
    lbl10: TLabel;
    lbl11: TLabel;
    lbl12: TLabel;
    lbl13: TLabel;
    lbl14: TLabel;
    lbl15: TLabel;
    lbl16: TLabel;
    edtRadarJammingSpotNumberCenter: TEdit;
    edtRadarJammingSpotNumberBearing: TEdit;
    edtRadarJammingSpotNumberBandwidth: TEdit;
    edtRadarJammingSpotNumberSpot: TEdit;
    grpRadarJammingSelectedTrackMode: TGroupBox;
    lbl17: TLabel;
    lbl18: TLabel;
    lbl19: TLabel;
    lbl20: TLabel;
    lbl21: TLabel;
    lbl22: TLabel;
    lbl23: TLabel;
    btnRadarJammingModeSelectedTrack: TSpeedButton;
    edtRadarJammingModeSelectedTrackCenterFreq: TEdit;
    edtRadarJammingModeSelectedTrackBearing: TEdit;
    edtRadarJammingModeSelectedTrackBandwidth: TEdit;
    edtRadarJammingSelectedTrackModeTrack: TEdit;
    pmNoiseJammerMode: TPopupMenu;
    mniJammBarrage: TMenuItem;
    mniJammFreq: TMenuItem;
    mniJammSpotNumber: TMenuItem;
    mniJammSelectedTrack: TMenuItem;
    edtRadarJammingControlMode: TEdit;
    procedure btnComboRadarJammingControlModeClick(Sender: TObject);
    procedure OnMenuItemClick(Sender: TObject);
    procedure btnRadarJammingModeSelectedTrackClick(Sender: TObject);
  private
    { Private declarations }
    FLastState      : TECMStatus;
    FLastMode       : Integer;
    FLastTrackID    : integer;
    FLastBearing    : Double;
    FLastBandwith   : Double;
    FLastFrequency  : Double;
    FLastSpotNumber : Double;
    procedure InitPacket(var rec : TRecCmdRadarNoiseOnBoard);
    procedure DisableAllControl;override;
    procedure EnableAllControl;override;
  public
    { Public declarations }
    procedure UpdateForm; override;
    procedure SetControlledECM(aObj : TObject); override;
  end;

var
  frmRadarNoiseJammer: TfrmRadarNoiseJammer;

implementation

uses uGlobalVar, uT3ClientManager, uT3PlatformInstance, uT3Vehicle;

{$R *.dfm}

{ TfrmRadarNoiseJammer }

procedure TfrmRadarNoiseJammer.btnComboRadarJammingControlModeClick(
  Sender: TObject);
var
  pt : TPoint;
begin
  inherited;

  GetCursorPos(pt);
  pmNoiseJammerMode.Popup(pt.X, pt.Y);
end;

procedure TfrmRadarNoiseJammer.btnRadarJammingModeSelectedTrackClick(
  Sender: TObject);
var
  recEcmCmd : TRecCmdRadarNoiseOnBoard;
  pf : TT3PlatformInstance;
  bearing : double;
begin
  inherited;

  if not Assigned(TT3ClientManager(simManager).SelectedTrack) then
  begin
//    frmTacticalDisplay.addStatus('Vehicle Target Not Found');
    Exit;
  end;

  if not (FControlledECM is TT3MountedRadarNoiseJammer) then
    exit;


  if Assigned(TT3ClientManager(simManager).SelectedTrack) then
  begin
    { set default value }
    InitPacket(recEcmCmd);

    pf := simManager.FindT3PlatformByID(TT3ClientManager(simManager).SelectedTrack.ObjectInstanceIndex);
    if Assigned(pf) and (pf is TT3Vehicle) then
    begin

      bearing := CalcBearing(TT3MountedRadarNoiseJammer(FControlledECM).PosX,
                 TT3MountedRadarNoiseJammer(FControlledECM).PosY,
                 pf.getPositionX,
                 pf.getPositionY);

      recEcmCmd.TrackId   := pf.InstanceIndex;
      recEcmCmd.Frequency :=
        (TT3MountedRadarNoiseJammer(FControlledECM).JammerDefinition.Upper_Freq_Limit +
        TT3MountedRadarNoiseJammer(FControlledECM).JammerDefinition.Lower_Freq_Limit) / 2;

      recEcmCmd.Bearing  := bearing;
      recEcmCmd.Bandwith := 2;
      recEcmCmd.OrderID  := CORD_RADARNOISE_TRACKID;

      simManager.NetCmdSender.CmdRadarNoiseOnBoard(recEcmCmd);
    end
  end
  else
  begin
//    frmTacticalDisplay.addStatus('Vehicle Target Not Vehicle!');
    Exit;
  end;

end;

procedure TfrmRadarNoiseJammer.DisableAllControl;
begin
  inherited;

end;

procedure TfrmRadarNoiseJammer.EnableAllControl;
begin
  inherited;

end;

procedure TfrmRadarNoiseJammer.InitPacket(var rec: TRecCmdRadarNoiseOnBoard);
begin
  if FControlledECM is TT3MountedRadarNoiseJammer then
  begin
    rec.PlatformIndex := TT3MountedRadarNoiseJammer(FControlledECM).PlatformParentInstanceIndex;
    rec.AssetIndex    := TT3MountedRadarNoiseJammer(FControlledECM).InstanceIndex;
    rec.OrderID       := 0;
    rec.Mode          := 0;
    rec.Activation    := 0;
    rec.Bearing       := 0;
    rec.SpotNumber    := 0;
    rec.TrackId       := 0;
    rec.Frequency     := 0;
    rec.Bandwith      := 0;
  end;
end;

procedure TfrmRadarNoiseJammer.OnMenuItemClick(Sender: TObject);
var
  recECMCommand : TRecCmdRadarNoiseOnBoard;
begin
  inherited;

  if not (FControlledECM is TT3MountedRadarNoiseJammer) then
    exit;

  InitPacket(recECMCommand);

  case TMenuItem(Sender).Tag of
    1 :  { Barrage }
      begin
        recECMCommand.Mode    := 0;
      end;
    2 : { Center Frequency }
      begin
        recECMCommand.Mode    := 3;
      end;
    3 : { Spot Number }
      begin
        recECMCommand.Mode    := 2;
      end;
    4 : { Selected Track }
      begin
        recECMCommand.Mode    := 1;
      end
  end;

  recECMCommand.OrderID := CORD_RADARNOISE_MODE;
  simManager.NetCmdSender.CmdRadarNoiseOnBoard(recECMCommand);
end;

procedure TfrmRadarNoiseJammer.SetControlledECM(aObj: TObject);
begin
  inherited;

  FLastState      := esUnavailable;
  FLastMode       := -1;
  FLastTrackID    := 0;
  FLastBearing    := -1;
  FLastBandwith   := -1;
  FLastFrequency  := -1;
  FLastSpotNumber := -1;

  UpdateForm;
end;

procedure TfrmRadarNoiseJammer.UpdateForm;
begin
  inherited;

  if Assigned( FControlledECM ) then
  with TT3MountedRadarNoiseJammer(FControlledECM) do
  begin
    case Status of
      esOn      :
      begin
        btnRadarJammingControlActivationOn.Down := True;
        btnRadarJammingControlActivationOff.Down := False;
      end;
      esOff     :
      begin
        btnRadarJammingControlActivationOn.Down := False;
        btnRadarJammingControlActivationOff.Down := True;
      end;
    end;

    if FLastTrackID <> JamTrackInstanceIndex then
    begin
      {klo di client harusnya track number}
      edtRadarJammingSelectedTrackModeTrack.Text := IntToStr(JamTrackInstanceIndex);
      FLastTrackID := JamTrackInstanceIndex;
    end;

    if FLastBearing <> JamBearing then
    begin
      case JamTargeting of
        ettNJBarrage : edtRadarJammingBarrageBearing.Text := FloatToStr(JamBearing);
        ettNJTrack   : edtRadarJammingModeSelectedTrackBearing.Text := FloatToStr(JamBearing);
      end;

      FLastBearing := JamBearing;
    end;


    {update when targeting mode change}
    if FLastMode <> Byte(JamTargeting) then
    begin
      case JamTargeting Of
        ettNJBarrage :
        begin
          edtRadarJammingControlMode.Text := 'Barrage';
          grpRadarJammingBarrageMode.BringToFront;
          edtRadarJammingBarrageBandwidth.Text := FloatToStr(JamBandWidth);
          edtRadarJammingBarrageBandwidth.Enabled := true;
        end;
        ettNJTrack :
        begin
          edtRadarJammingControlMode.Text := 'Spot Jamming - Selected Track';
          grpRadarJammingSelectedTrackMode.BringToFront;


          edtRadarJammingModeSelectedTrackBearing.Enabled := False;
          edtRadarJammingModeSelectedTrackCenterFreq.Enabled := False;
          edtRadarJammingModeSelectedTrackBandwidth.Enabled := False;

          edtRadarJammingModeSelectedTrackBearing.Text := '----';
          edtRadarJammingModeSelectedTrackCenterFreq.Text := '----';
          edtRadarJammingModeSelectedTrackBandwidth.Text := '----';
        end;
        ettNJSpotNum :
        begin
          edtRadarJammingControlMode.Text := 'Spot Jamming - Spot Number';
          grpRadarJammingSpotNumberMode.BringToFront;

          edtRadarJammingSpotNumberCenter.Enabled := False;
          edtRadarJammingSpotNumberBandwidth.Enabled := False;
        end;
        ettNJFreq :
        begin
          edtRadarJammingControlMode.Text := 'Spot Jamming - Frequency';
          grpRadarJammingSpotNumberMode.BringToFront;
        end;
      end;

      FLastMode := Byte(JamTargeting);
    end;

    if FLastState <> Status then
    begin
      if Status = esDamaged then
        DisableAllControl
      else
        EnableAllControl;

      FLastState := Status;
    end;
  end;

end;

end.
