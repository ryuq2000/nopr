unit ufrmECMType;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, uT3MountedECM;

type
  TfrmECMType = class(TForm)
  private
    { Private declarations }
  protected
    FControlledECM : TObject;
    procedure DisableAllControl; virtual;
    procedure EnableAllControl;virtual;
  public
    { Public declarations }
    procedure UpdateForm; virtual;
    procedure SetControlledECM(aObj : TObject); virtual;

    property ControlledECM : TObject read FControlledECM;
  end;

var
  frmECMType: TfrmECMType;

implementation

{$R *.dfm}

{ TfrmECMType }

procedure TfrmECMType.DisableAllControl;
begin

end;

procedure TfrmECMType.EnableAllControl;
begin

end;

procedure TfrmECMType.SetControlledECM(aObj: TObject);
begin
  FControlledECM := aObj;
  UpdateForm;
end;

procedure TfrmECMType.UpdateForm;
begin

end;

end.
