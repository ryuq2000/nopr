unit ufrmTowedJammerDecoy;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls, ufrmECMType;

type
  TfrmTowedJammerDecoy = class(TfrmECMType)
    grpTowedJammerDecoy: TGroupBox;
    scrlbx1: TScrollBox;
    bvl1: TBevel;
    bvl2: TBevel;
    bvl3: TBevel;
    lbl1: TLabel;
    lbl2: TLabel;
    lbl3: TLabel;
    lbl4: TLabel;
    lbl5: TLabel;
    lbl6: TLabel;
    lbl7: TLabel;
    lbl8: TLabel;
    lbl9: TLabel;
    lbl10: TLabel;
    lbl11: TLabel;
    lbl12: TLabel;
    lbl13: TLabel;
    lbl14: TLabel;
    lblTowedJammerDecoyActual: TLabel;
    lblTowedJammerDecoyQuantity: TLabel;
    btnTowedJammerDecoyActionDeploy: TSpeedButton;
    btnTowedJammerDecoyActionStow: TSpeedButton;
    btnTowedJammerDecoyModeAuto: TSpeedButton;
    btnTowedJammerDecoyModeManual: TSpeedButton;
    btnTowedJammerDecoyModeOff: TSpeedButton;
    btnTowedJammerDecoyTargetingSpot: TSpeedButton;
    btnTowedJammerDecoyTargetingTrack: TSpeedButton;
    edtTowedJammerDecoyOrdered: TEdit;
    edtTowedJammerDecoyBearing: TEdit;
    edtTowedJammerDecoySpotNumb: TEdit;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmTowedJammerDecoy: TfrmTowedJammerDecoy;

implementation

{$R *.dfm}

end.
