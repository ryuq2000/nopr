unit ufrmOnBoardSelfDefenseJammer;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls, ufrmECMType, uGameData_TTT, uT3MountedECM,
  uT3MountedDefensiveJammer;

type
  TfrmOnBoardSelfDefenseJammer = class(TfrmECMType)
    grpOnBoardSelfDefenseJammer: TGroupBox;
    scrlbx1: TScrollBox;
    bvl1: TBevel;
    lbl1: TLabel;
    lbl2: TLabel;
    lbl3: TLabel;
    btnOnBoardSelfDefenseJammerControlModeAuto: TSpeedButton;
    btnOnBoardSelfDefenseJammerControlModeManual: TSpeedButton;
    btnOnBoardSelfDefenseJammerControlModeOff: TSpeedButton;
    btnOnBoardSelfDefenseJammerControlTargetingSpot: TSpeedButton;
    btnOnBoardSelfDefenseJammerControlTargetingTrack: TSpeedButton;
    grpManualSpot: TGroupBox;
    lbl4: TLabel;
    lbl5: TLabel;
    lbl6: TLabel;
    edtOnBoardSelfDefenseJammerSpotNumber: TEdit;
    edtOnBoardSelfDefenseJammerBearing: TEdit;
    grpManualTrack: TGroupBox;
    lbl7: TLabel;
    btnSDJammerTarget: TSpeedButton;
    edtSDJammerTarget: TEdit;
    procedure btnOnBoardSelfDefenseJammerControlModeAutoClick(Sender: TObject);
    procedure btnSDJammerTargetClick(Sender: TObject);
    procedure OnSelfDefenseKeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
    FLastState : TECMStatus;
    procedure InitPacket(var rec : TRecCmdSelfDefenseOnBoard);
    procedure DisableAllControl;override;
    procedure EnableAllControl;override;
  public
    { Public declarations }
    procedure UpdateForm; override;
    procedure SetControlledECM(aObj : TObject); override;
  end;

var
  frmOnBoardSelfDefenseJammer: TfrmOnBoardSelfDefenseJammer;

implementation

uses
  uGlobalVar;

{$R *.dfm}

{ TfrmOnBoardSelfDefenseJammer }

procedure TfrmOnBoardSelfDefenseJammer.btnOnBoardSelfDefenseJammerControlModeAutoClick(
  Sender: TObject);
var
  recECMCommand : TRecCmdSelfDefenseOnBoard;
begin
  inherited;

  if FControlledECM = nil then
    exit;

  if not (FControlledECM is TT3MountedDefensiveJammer) then
    exit;

  if TT3MountedDefensiveJammer(FControlledECM).Status = esDamaged then
    Exit;

  InitPacket(recECMCommand);

  case TSpeedButton(Sender).Tag of
    1 :
      begin
        { automatic mode }
        recECMCommand.OrderID := CORD_SELFDEFENSE_MODE;
        recECMCommand.Mode  := 2;
      end;
    2 :
      begin
        { manual mode }
        recECMCommand.OrderID := CORD_SELFDEFENSE_MODE;
        recECMCommand.Mode  := 1;
      end;
    3 :
      begin
        { automatic mode }
        recECMCommand.OrderID := CORD_SELFDEFENSE_MODE;
        recECMCommand.Mode  := 0;
      end;
    4 :
      begin
        { spot targeting }
        recECMCommand.OrderID := CORD_SELFDEFENSE_TARGETING;
        recECMCommand.Targeting  := 1;
        TryStrToInt(edtOnBoardSelfDefenseJammerSpotNumber.Text,recECMCommand.SpotNumber );
        TryStrToFloat(edtOnBoardSelfDefenseJammerBearing.Text,recECMCommand.Bearing );
      end;
    5 :
      begin
        { track targeting }
        recECMCommand.OrderID := CORD_SELFDEFENSE_TARGETING;
        recECMCommand.Targeting := 0;
        TryStrToInt(edtSDJammerTarget.Text,recECMCommand.TrackId );
      end;
  end;

  simManager.NetCmdSender.CmdSelfDefenceOnBoard(recECMCommand);
end;

procedure TfrmOnBoardSelfDefenseJammer.btnSDJammerTargetClick(Sender: TObject);
var
  recEcmCmd : TRecCmdSelfDefenseOnBoard;
begin
  inherited;

//  if not Assigned(focused_platform) then
//  begin
//    frmTacticalDisplay.addStatus('Vehicle Target Not Found');
//    Exit;
//  end;

  { set default value }
  InitPacket(recEcmCmd);

//  if Focused_Platform is TT3Missile then
//  begin
//    recEcmCmd.JammedObjectType  := 1;
//    recEcmCmd.Value2  := TT3Missile(Focused_Platform).InstanceIndex;
//  end
//  else if Focused_Platform is TT3Vehicle then begin
//    recEcmCmd.JammedObjectType  := 0;
//    recEcmCmd.Value2  := TT3Vehicle(Focused_Platform).InstanceIndex;
//  end
//  else
//  begin
//    frmTacticalDisplay.addStatus('Vehicle Target Not Missile or Vehicle!');
//    Exit;
//  end;

  recEcmCmd.OrderID  := CORD_SELFDEFENSE_TARGETING;
  recEcmCmd.Targeting   := 0;
  simManager.NetCmdSender.CmdSelfDefenceOnBoard(recEcmCmd);
end;

procedure TfrmOnBoardSelfDefenseJammer.DisableAllControl;
var
  i : integer;
begin
  inherited;

  for I := 0 to ComponentCount - 1 do
  begin
    if Components[i].ClassType = TSpeedButton then
      TSpeedButton(Components[i]).Enabled := False;

    if Components[i].ClassType = TEdit then
      TEdit(Components[i]).Enabled := False;
  end;

end;

procedure TfrmOnBoardSelfDefenseJammer.EnableAllControl;
var
  i : integer;
begin
  inherited;

  for I := 0 to ComponentCount - 1 do
  begin
    if Components[i].ClassType = TSpeedButton then
      TSpeedButton(Components[i]).Enabled := True;

    if Components[i].ClassType = TEdit then
      TEdit(Components[i]).Enabled := True;
  end;
end;

procedure TfrmOnBoardSelfDefenseJammer.InitPacket(
  var rec: TRecCmdSelfDefenseOnBoard);
begin
  if FControlledECM is TT3MountedDefensiveJammer then
  begin
    rec.PlatformIndex := TT3MountedDefensiveJammer(FControlledECM).PlatformParentInstanceIndex;
    rec.AssetIndex    := TT3MountedDefensiveJammer(FControlledECM).InstanceIndex;
    rec.OrderID       := 0;
    rec.Mode          := 0;
    rec.Targeting     := 0;
    rec.Bearing       := 0;
    rec.SpotNumber    := 0;
    rec.TrackId       := 0;
    rec.JammedObjectType := 0;
  end;

end;

procedure TfrmOnBoardSelfDefenseJammer.OnSelfDefenseKeyPress(Sender: TObject;
  var Key: Char);
var
  IntVal    : integer;
  FloatVal  : Double;
  ValKey    : set of AnsiChar;
  StrVal    : string;
  recEcmCmd : TRecCmdSelfDefenseOnBoard;
begin
  inherited;

  if not (FControlledECM is TT3MountedDefensiveJammer) then
    exit;

  ValKey := [#48 .. #57, #8, #13, #46];
  if not(CharInSet(Key, ValKey)) then
    Key := #0;

  if Key = #13 then
  begin
    StrVal  := TEdit(Sender).Text;
    case TEdit(Sender).Tag of
      1 : TryStrToFloat(StrVal,FloatVal);
      2 : TryStrToInt(StrVal,IntVal);
    end;

    InitPacket(recEcmCmd);

    case TEdit(Sender).Tag of
      1 :
        begin
          recEcmCmd.Bearing  := FloatVal;
        end;
      2 :
        begin
          recEcmCmd.SpotNumber  := IntVal;
        end;
    end;

    recEcmCmd.OrderID  := CORD_SELFDEFENSE_TARGETING;
    recEcmCmd.Targeting   := 1;
    simManager.NetCmdSender.CmdSelfDefenceOnBoard(recEcmCmd);
  end;
end;

procedure TfrmOnBoardSelfDefenseJammer.SetControlledECM(aObj: TObject);
begin
  inherited;
  FLastState := esUnavailable;
  UpdateForm;
end;

procedure TfrmOnBoardSelfDefenseJammer.UpdateForm;
begin
  inherited;

  if Assigned( FControlledECM ) then
  with TT3MountedDefensiveJammer(FControlledECM) do
  begin
    case Status of
      esAutomatic :
      begin
        btnOnBoardSelfDefenseJammerControlModeAuto.Down := True;
        btnOnBoardSelfDefenseJammerControlModeManual.Down := False;
        btnOnBoardSelfDefenseJammerControlModeOff.Down := False;
      end;
      esManual    :
      begin
        btnOnBoardSelfDefenseJammerControlModeAuto.Down := False;
        btnOnBoardSelfDefenseJammerControlModeManual.Down := True;
        btnOnBoardSelfDefenseJammerControlModeOff.Down := False;
      end;
      esOff       :
      begin
        btnOnBoardSelfDefenseJammerControlModeAuto.Down := False;
        btnOnBoardSelfDefenseJammerControlModeManual.Down := False;
        btnOnBoardSelfDefenseJammerControlModeOff.Down := True;
      end;
    end;

    case TargetingType of
      ettTrack :
      begin
        btnOnBoardSelfDefenseJammerControlTargetingTrack.Down := True;
        btnOnBoardSelfDefenseJammerControlTargetingSpot.Down := False;

        grpManualTrack.BringToFront;
      end;
      ettSpot  :
      begin
        btnOnBoardSelfDefenseJammerControlTargetingTrack.Down := False;
        btnOnBoardSelfDefenseJammerControlTargetingSpot.Down := True;

        grpManualTrack.BringToFront;
      end;
    end;

    edtOnBoardSelfDefenseJammerSpotNumber.Text := IntToStr(SpotNumber);
    edtOnBoardSelfDefenseJammerBearing.Text := FloatToStr(Bearing);

    { klo di controller muncul instance index, klo di client track number }
    edtSDJammerTarget.Text := IntToStr(TargetInstanceIndex);

    if FLastState <> Status then
    begin
      if Status = esDamaged then
        DisableAllControl
      else
        EnableAllControl;

      FLastState := Status;
    end;
  end;
end;

end.
