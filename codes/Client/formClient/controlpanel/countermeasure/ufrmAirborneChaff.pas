unit ufrmAirborneChaff;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls,ufrmECMType, Menus, uGameData_TTT,
  uT3MountedAirborneChaff, tttData, uT3MountedECM;

type
  TfrmAirborneChaff = class(TfrmECMType)
    grpAirborneChaff: TGroupBox;
    lbl1: TLabel;
    lbl2: TLabel;
    lbl3: TLabel;
    bvl1: TBevel;
    lblChaffAirboneQuantity: TLabel;
    btnAirboneChaffType: TSpeedButton;
    edtChaffAirboneType: TEdit;
    btnChaffAirboneDeploy: TButton;
    pmChaffType: TPopupMenu;
    Barrier1: TMenuItem;
    Helicopter1: TMenuItem;
    Seduction1: TMenuItem;
    procedure btnAirboneChaffTypeClick(Sender: TObject);
    procedure btnChaffAirboneDeployClick(Sender: TObject);
    procedure OnDeployTypeClick(Sender: TObject);
  private
    { Private declarations }
    procedure InitPacket(var rec : TRecCmdChaffOnBoard);
  public
    { Public declarations }
    procedure UpdateForm; override;
    procedure SetControlledECM(aObj : TObject); override;
  end;

var
  frmAirborneChaff: TfrmAirborneChaff;

implementation

uses
  uGlobalVar;
{$R *.dfm}

{ TfrmAirborneChaff }

procedure TfrmAirborneChaff.btnAirboneChaffTypeClick(Sender: TObject);
var
  pt : TPoint;
begin
  inherited;

  GetCursorPos(pt);
  pmChaffType.Popup(pt.X, pt.Y);

end;

procedure TfrmAirborneChaff.btnChaffAirboneDeployClick(Sender: TObject);
var
  rChaff : TRecCmdChaffOnBoard;
begin
  inherited;

  if FControlledECM = nil then
    exit;

  if not (FControlledECM is TT3MountedAirborneChaff) then
    exit;

  if TT3MountedAirborneChaff(FControlledECM).Status = esDamaged then
    Exit;

  if TT3MountedAirborneChaff(FControlledECM).Quantity <= 0 then
    exit;

  InitPacket(rChaff);
  rChaff.OrderID := CORD_CHAFF_DEPLOY;
  simManager.NetCmdSender.CmdChaffOnBoard(rChaff);

end;

procedure TfrmAirborneChaff.InitPacket(var rec: TRecCmdChaffOnBoard);
begin
  if FControlledECM is TT3MountedAirborneChaff then
  begin
    rec.PlatformIndex := TT3MountedAirborneChaff(FControlledECM).PlatformParentInstanceIndex;
    rec.AssetIndex    := TT3MountedAirborneChaff(FControlledECM).InstanceIndex;
    rec.OrderID       := 0;
    rec.Value         := 0;
  end;
end;

procedure TfrmAirborneChaff.OnDeployTypeClick(Sender: TObject);
var
  rec : TRecCmdChaffOnBoard;
begin
  inherited;

  if Assigned( FControlledECM ) then

  with TT3MountedAirborneChaff(FControlledECM) do
  begin
    InitPacket(rec);
    case TMenuItem(Sender).Tag of
      0 :
      begin
        rec.OrderID := CORD_CHAFF_TYPE;
        rec.Value   := Byte(atBarrier);
        simManager.NetCmdSender.CmdChaffOnBoard(rec);
        edtChaffAirboneType.Text := 'Barrier';
      end;
      1 :
      begin
        rec.OrderID := CORD_CHAFF_TYPE;
        rec.Value   := Byte(atHelicopter);
        simManager.NetCmdSender.CmdChaffOnBoard(rec);
        edtChaffAirboneType.Text := 'Helicopter';
      end;
      2 :
      begin
        rec.OrderID := CORD_CHAFF_TYPE;
        rec.Value   := Byte(atSeduction);
        simManager.NetCmdSender.CmdChaffOnBoard(rec);
        edtChaffAirboneType.Text := 'Seduction';
      end;
    end;
  end;
end;

procedure TfrmAirborneChaff.SetControlledECM(aObj: TObject);
begin
  inherited;

  if Assigned( FControlledECM ) then

  with TT3MountedAirborneChaff(FControlledECM) do
  begin
    case DeploymentType of
      atBarrier    : edtChaffAirboneType.Text    := 'Barrier';
      atHelicopter : edtChaffAirboneType.Text    := 'Helicopter';
      atSeduction  : edtChaffAirboneType.Text    := 'Seduction';
    end;
    lblChaffAirboneQuantity.Caption := IntToStr(Quantity);
  end;

end;

procedure TfrmAirborneChaff.UpdateForm;
begin
  inherited;

  if Assigned( FControlledECM ) then

  with TT3MountedAirborneChaff(FControlledECM) do
  begin
    case DeploymentType of
      atBarrier    : edtChaffAirboneType.Text    := 'Barrier';
      atHelicopter : edtChaffAirboneType.Text    := 'Helicopter';
      atSeduction  : edtChaffAirboneType.Text    := 'Seduction';
    end;
    lblChaffAirboneQuantity.Caption := IntToStr(Quantity);
  end;

end;

end.
