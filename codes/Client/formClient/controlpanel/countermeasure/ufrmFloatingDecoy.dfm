object frmFloatingDecoy: TfrmFloatingDecoy
  Left = 0
  Top = 0
  Caption = 'frmFloatingDecoy'
  ClientHeight = 202
  ClientWidth = 315
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object grpFloatingDecoy: TGroupBox
    Left = 0
    Top = 0
    Width = 315
    Height = 202
    Align = alClient
    TabOrder = 0
    object lbl1: TLabel
      Left = 3
      Top = 3
      Width = 57
      Height = 13
      Caption = 'Deployment'
    end
    object bvl1: TBevel
      Left = 65
      Top = 10
      Width = 240
      Height = 3
    end
    object lbl2: TLabel
      Left = 22
      Top = 22
      Width = 49
      Height = 13
      Caption = 'Quantity :'
    end
    object lblFloatingDecoyQuantity: TLabel
      Left = 84
      Top = 22
      Width = 8
      Height = 13
      Caption = '--'
    end
    object btnFloatingDecoyDeploy: TButton
      Left = 226
      Top = 16
      Width = 80
      Height = 25
      Caption = 'Deploy'
      TabOrder = 0
      OnClick = btnFloatingDecoyDeployClick
    end
  end
end
