unit ufrmAirBubble;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls, ufrmECMType, uGameData_TTT,
  uT3MountedAirBubble, uT3MountedECM;

type
  TfrmAirBubble = class(TfrmECMType)
    grpAirBubble: TGroupBox;
    lblDeploy: TLabel;
    lblQuant: TLabel;
    lblType: TLabel;
    bvl1: TBevel;
    lblBubblelQuantity: TLabel;
    btnType: TSpeedButton;
    edtBubble: TEdit;
    btnAirBubbleDeploy: TButton;
    procedure btnAirBubbleDeployClick(Sender: TObject);
  private
    { Private declarations }
    procedure InitPacket(var rec : TRecCmdBubbleOnBoard);
  public
    { Public declarations }
    procedure UpdateForm; override;
    procedure SetControlledECM(aObj : TObject); override;
  end;

var
  frmAirBubble: TfrmAirBubble;

implementation

uses
  uGlobalVar;

{$R *.dfm}

{ TfrmAirBubble }

procedure TfrmAirBubble.btnAirBubbleDeployClick(Sender: TObject);
var
  rChaff : TRecCmdBubbleOnBoard;
begin
  inherited;

  if FControlledECM = nil then
    exit;

  if not (FControlledECM is TT3MountedAirbubble) then
    exit;

  if TT3MountedAirbubble(FControlledECM).Status = esDamaged then
    Exit;

  if TT3MountedAirbubble(FControlledECM).Quantity <= 0 then
    exit;

  InitPacket(rChaff);
  rChaff.OrderID := CORD_BUBBLE_DEPLOY;
  simManager.NetCmdSender.CmdBubbleOnBoard(rChaff);
end;

procedure TfrmAirBubble.InitPacket(var rec: TRecCmdBubbleOnBoard);
begin
  if FControlledECM is TT3MountedAirbubble then
  begin
    rec.PlatformIndex := TT3MountedAirbubble(FControlledECM).PlatformParentInstanceIndex;
    rec.AssetIndex    := TT3MountedAirbubble(FControlledECM).InstanceIndex;
    rec.OrderID       := 0;
    rec.Value         := 0;
  end;

end;

procedure TfrmAirBubble.SetControlledECM(aObj: TObject);
begin
  inherited;

  UpdateForm;
end;

procedure TfrmAirBubble.UpdateForm;
begin
  inherited;

  if Assigned( FControlledECM ) then

  with TT3MountedAirbubble(FControlledECM) do
  begin
    lblBubblelQuantity.Caption := IntToStr(Quantity);
  end;

end;

end.
