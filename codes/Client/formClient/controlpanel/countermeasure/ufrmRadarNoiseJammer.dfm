object frmRadarNoiseJammer: TfrmRadarNoiseJammer
  Left = 0
  Top = 0
  Caption = 'frmRadarNoiseJammer'
  ClientHeight = 273
  ClientWidth = 325
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object grpRadarNoiseJammer: TGroupBox
    Left = 0
    Top = 0
    Width = 325
    Height = 273
    Align = alClient
    TabOrder = 0
    object scrlbx1: TScrollBox
      Left = 2
      Top = 15
      Width = 321
      Height = 256
      Align = alClient
      BevelOuter = bvNone
      BorderStyle = bsNone
      TabOrder = 0
      object bvl1: TBevel
        Left = 44
        Top = 6
        Width = 250
        Height = 3
      end
      object btnComboRadarJammingControlMode: TSpeedButton
        Tag = 3
        Left = 279
        Top = 94
        Width = 23
        Height = 22
        Glyph.Data = {
          D6050000424DD605000000000000360000002800000017000000140000000100
          180000000000A005000000000000000000000000000000000000B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B200
          0000B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2000000B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B200
          0000B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2C1C1C1B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2000000B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B200000000
          0000B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2000000000000000000000000C1C1C1B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2000000000000000000000000000000000000B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B200
          0000B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000000000000000000000
          000000000000000000000000C1C1C1B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2000000B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B200000000
          0000000000000000000000000000000000000000000000000000B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2000000000000000000000000000000000000000000000000B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2000000000000000000000000000000000000B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B200
          0000B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000000000000000000000
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2000000B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B200000000
          0000B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B200
          0000B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2000000B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000}
        Visible = False
        OnClick = btnComboRadarJammingControlModeClick
      end
      object lbl1: TLabel
        Left = 25
        Top = 100
        Width = 33
        Height = 13
        Caption = 'Mode :'
      end
      object lbl2: TLabel
        Left = 3
        Top = -3
        Width = 35
        Height = 13
        Caption = 'Control'
      end
      object lbl3: TLabel
        Left = 24
        Top = 14
        Width = 48
        Height = 13
        Caption = 'Activation'
      end
      object btnRadarJammingControlActivationOff: TSpeedButton
        Tag = 2
        Left = 11
        Top = 51
        Width = 80
        Height = 25
        AllowAllUp = True
        GroupIndex = 1
        Down = True
        Caption = 'Off'
      end
      object btnRadarJammingControlActivationOn: TSpeedButton
        Tag = 1
        Left = 11
        Top = 28
        Width = 80
        Height = 25
        AllowAllUp = True
        GroupIndex = 1
        Caption = 'On'
      end
      object pnlRadarJammingMode: TPanel
        Left = 11
        Top = 125
        Width = 290
        Height = 106
        BevelOuter = bvNone
        TabOrder = 1
        object grpRadarJammingSpotNumberMode: TGroupBox
          Left = 0
          Top = 0
          Width = 290
          Height = 106
          TabOrder = 2
          object lbl10: TLabel
            Left = 14
            Top = 31
            Width = 43
            Height = 13
            Caption = 'Bearing :'
          end
          object lbl11: TLabel
            Left = 184
            Top = 31
            Width = 48
            Height = 13
            Caption = 'degrees T'
          end
          object lbl12: TLabel
            Left = 184
            Top = 53
            Width = 19
            Height = 13
            Caption = 'Mhz'
          end
          object lbl13: TLabel
            Left = 14
            Top = 53
            Width = 94
            Height = 13
            Caption = 'Center Frequency :'
          end
          object lbl14: TLabel
            Left = 14
            Top = 75
            Width = 57
            Height = 13
            Caption = 'Bandwidth :'
          end
          object lbl15: TLabel
            Left = 184
            Top = 75
            Width = 19
            Height = 13
            Caption = 'Mhz'
          end
          object lbl16: TLabel
            Left = 14
            Top = 9
            Width = 69
            Height = 13
            Caption = 'Spot Number :'
          end
          object edtRadarJammingSpotNumberCenter: TEdit
            Tag = 3
            Left = 126
            Top = 49
            Width = 53
            Height = 21
            TabOrder = 2
            Text = '10.0'
          end
          object edtRadarJammingSpotNumberBearing: TEdit
            Tag = 2
            Left = 126
            Top = 27
            Width = 53
            Height = 21
            TabOrder = 1
            Text = '000'
          end
          object edtRadarJammingSpotNumberBandwidth: TEdit
            Tag = 4
            Left = 126
            Top = 71
            Width = 53
            Height = 21
            TabOrder = 3
            Text = '2.0'
          end
          object edtRadarJammingSpotNumberSpot: TEdit
            Tag = 1
            Left = 126
            Top = 5
            Width = 53
            Height = 21
            TabOrder = 0
            Text = '---'
          end
        end
        object grpRadarJammingBarrageMode: TGroupBox
          Left = 0
          Top = 0
          Width = 290
          Height = 106
          Align = alClient
          TabOrder = 1
          object lbl4: TLabel
            Left = 13
            Top = 9
            Width = 43
            Height = 13
            Caption = 'Bearing :'
          end
          object lbl5: TLabel
            Left = 184
            Top = 9
            Width = 48
            Height = 13
            Caption = 'degrees T'
          end
          object lbl6: TLabel
            Left = 184
            Top = 31
            Width = 19
            Height = 13
            Caption = 'Mhz'
          end
          object lblFrequency: TLabel
            Left = 13
            Top = 31
            Width = 94
            Height = 13
            Caption = 'Center Frequency :'
          end
          object lblBandwith: TLabel
            Left = 13
            Top = 53
            Width = 57
            Height = 13
            Caption = 'Bandwidth :'
          end
          object lbl9: TLabel
            Left = 184
            Top = 53
            Width = 19
            Height = 13
            Caption = 'Mhz'
          end
          object edtRadarJammingBarrageCenter: TEdit
            Tag = 2
            Left = 126
            Top = 27
            Width = 53
            Height = 21
            TabOrder = 1
            Text = '---'
          end
          object edtRadarJammingBarrageBearing: TEdit
            Tag = 1
            Left = 126
            Top = 5
            Width = 53
            Height = 21
            TabOrder = 0
            Text = '000'
          end
          object edtRadarJammingBarrageBandwidth: TEdit
            Tag = 3
            Left = 126
            Top = 49
            Width = 53
            Height = 21
            TabOrder = 2
            Text = '---'
          end
        end
        object grpRadarJammingSelectedTrackMode: TGroupBox
          Left = 0
          Top = 0
          Width = 290
          Height = 106
          Align = alClient
          TabOrder = 0
          object lbl17: TLabel
            Left = 14
            Top = 31
            Width = 43
            Height = 13
            Caption = 'Bearing :'
          end
          object lbl18: TLabel
            Left = 184
            Top = 31
            Width = 48
            Height = 13
            Caption = 'degrees T'
          end
          object lbl19: TLabel
            Left = 184
            Top = 53
            Width = 19
            Height = 13
            Caption = 'Mhz'
          end
          object lbl20: TLabel
            Left = 14
            Top = 53
            Width = 94
            Height = 13
            Caption = 'Center Frequency :'
          end
          object lbl21: TLabel
            Left = 14
            Top = 75
            Width = 57
            Height = 13
            Caption = 'Bandwidth :'
          end
          object lbl22: TLabel
            Left = 184
            Top = 75
            Width = 19
            Height = 13
            Caption = 'Mhz'
          end
          object lbl23: TLabel
            Left = 14
            Top = 8
            Width = 33
            Height = 13
            Caption = 'Track :'
          end
          object btnRadarJammingModeSelectedTrack: TSpeedButton
            Left = 184
            Top = 4
            Width = 23
            Height = 22
            Glyph.Data = {
              36090000424D360900000000000036000000280000001F000000180000000100
              18000000000000090000000000000000000000000000000000006161613E3E3E
              3737374040403B3B3B3A3A3A4141414141413A3A3A3A3A3A4040403737373E3E
              3E3D3D3D3838384141413A3A3A3B3B3B4040403737373E3E3E3D3D3D38383841
              41413A3A3A3B3B3B4040403737373E3E3E3C3C3C383838000000B2B2B27C7C7C
              7474747F7F7F7878787777778080808080807878787878788080807474747C7C
              7C7B7B7B7474748080807A7A7A7A7A7A7F7F7F7474747C7C7C7B7B7B74747480
              80807777777979797F7F7F7474747D7D7D7A7A7A757575000000D0D0D0B2B2B2
              B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
              B2B2B2B2B2B2B2B2B2B2BDBDBDBBBBBBB2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
              B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000D0D0D0B2B2B2
              B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2C0C0C04949
              491111111111111111111212121212121A1A1AB2B2B2B2B2B2B2B2B2B2B2B2B2
              B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B4B4B4000000D0D0D0B2B2B2
              B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B29191914D4D4D5454545F5F
              5F6565656262620000004747476565656363634D4D4D4D4D4D797979BABABAB2
              B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B3B3B3000000D0D0D0B2B2B2
              B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B29797976969692929292929298686
              86B2B2B2ADADAD0000007D7D7DB2B2B2ABABAB292929292929545454979797B2
              B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000D0D0D0B2B2B2
              B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B23D3D3D3B3B3BB2B2B2B2B2B2B2B2
              B2B2B2B2ADADAD0000007D7D7DB2B2B2B2B2B2B2B2B2B2B2B26A6A6A0E0E0EB2
              B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000D0D0D0B2B2B2
              B2B2B2B2B2B2B2B2B2B2B2B2B2B2B21616166F6F6FA5A5A5B2B2B2B2B2B2B2B2
              B2B2B2B2ADADAD0000007D7D7DB2B2B2B2B2B2B2B2B2B2B2B2AAAAAA94949413
              13138A8A8AB2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000D0D0D0B2B2B2
              B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2030303757575B2B2B2B2B2B2B2B2B2B2B2
              B2B2B2B2ADADAD0000007D7D7DB2B2B2B2B2B2B2B2B2B2B2B2B2B2B2A4A4A400
              0000868686B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B3B3B3000000D0D0D0B2B2B2
              B2B2B2B2B2B2B2B2B2A6A6A68B8B8B2C2C2C838383B2B2B2B2B2B2B2B2B2B2B2
              B2B2B2B2ADADAD0000007F7F7FB4B4B4B2B2B2B2B2B2B2B2B2B2B2B2A7A7A727
              27277272729C9C9CB2B2B2B2B2B2B2B2B2B2B2B2B3B3B3000000D0D0D0B2B2B2
              B2B2B2B2B2B2B2B2B27B7B7B000000BDBDBDB7B7B7B2B2B2B2B2B2B2B2B2B2B2
              B2B2B2B2ADADAD000000888888BBBBBBB2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
              B2B22C2C2C4C4C4CB2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000D0D0D0B2B2B2
              B2B2B2B2B2B2B2B2B27B7B7B000000B0B0B0B3B3B3B2B2B2B2B2B2BFBFBFB6B6
              B6B2B2B2B1B1B19D9D9DADADADB3B3B3B2B2B2B2B2B2B2B2B2B2B2B2B3B3B3BF
              BFBF3030304C4C4CB2B2B2B2B2B2B2B2B2B2B2B2B4B4B4000000D0D0D0B2B2B2
              B2B2B2B2B2B2B2B2B27B7B7B0000004F4F4F5151515151515151515858585353
              53737373B2B2B2B2B2B2B2B2B28D8D8D51515151515151515151515152525258
              58581616164C4C4CB2B2B2B2B2B2B2B2B2B2B2B2B3B3B3000000D0D0D0B2B2B2
              B2B2B2B2B2B2B2B2B27B7B7B0000002424242525252525252525252525252525
              25575757B2B2B2B2B2B2B2B2B27D7D7D25252525252525252525252525252525
              25250909094C4C4CB2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000D0D0D0B2B2B2
              B2B2B2B2B2B2B2B2B27B7B7B000000AFAFAFB2B2B2B2B2B2B2B2B2B2B2B2B2B2
              B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
              B2B22C2C2C4C4C4CB2B2B2B2B2B2B2B2B2B2B2B2B4B4B4000000D0D0D0B2B2B2
              B2B2B2B2B2B2B2B2B27B7B7B000000BBBBBBB6B6B6B2B2B2B2B2B2B2B2B2B2B2
              B2B2B2B2ADADAD171717848484B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
              B2B22C2C2C4C4C4CB2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000D0D0D0B2B2B2
              B2B2B2B2B2B2B2B2B29999995F5F5F5A5A5A949494B2B2B2B2B2B2B2B2B2B2B2
              B2B2B2B2ADADAD0000007D7D7DB2B2B2B2B2B2B2B2B2B2B2B2B5B5B5B2B2B253
              53535C5C5C828282B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000D0D0D0B2B2B2
              B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2030303757575B2B2B2B2B2B2B2B2B2B2B2
              B2B2B2B2ADADAD0000007D7D7DB2B2B2B2B2B2B2B2B2B2B2B2B7B7B7AFAFAF00
              0000868686B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000D0D0D0B2B2B2
              B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2030303757575B2B2B2B2B2B2B2B2B2B2B2
              B2B2B2B2ADADAD0000007D7D7DB2B2B2B2B2B2B2B2B2B2B2B2B2B2B2A4A4A400
              0000868686B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000D0D0D0B2B2B2
              B2B2B2B2B2B2B2B2B2B2B2B2B2B2B29A9A9A4545454B4B4BB2B2B2B2B2B2B2B2
              B2B2B2B2ADADAD0000007D7D7DB2B2B2B2B2B2B2B2B2B2B2B274747423232399
              9999ACACACB2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B4B4B4000000D0D0D0B2B2B2
              B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B27A7A7A5A5A5A5555555555559494
              94B5B5B5B4B4B4000000838383B7B7B7ADADAD555555555555585858646464B2
              B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B3B3B3000000D0D0D0B2B2B2
              B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B28282822121212121216D6D
              6D9595959898980000006E6E6E9898988B8B8B2121212121215B5B5BB2B2B2B2
              B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000D0D0D0B2B2B2
              B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B23939
              39000000000000000000000000000000090909B2B2B2B2B2B2B2B2B2B2B2B2B2
              B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B4B4B4000000D0D0D0B2B2B2
              B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2A0A0
              A0979797979797979797979797979797999999B2B2B2B2B2B2B2B2B2B2B2B2B2
              B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000}
            OnClick = btnRadarJammingModeSelectedTrackClick
          end
          object edtRadarJammingModeSelectedTrackCenterFreq: TEdit
            Left = 126
            Top = 49
            Width = 53
            Height = 21
            TabOrder = 2
            Text = '10.0'
          end
          object edtRadarJammingModeSelectedTrackBearing: TEdit
            Left = 126
            Top = 27
            Width = 53
            Height = 21
            TabOrder = 1
            Text = '000'
          end
          object edtRadarJammingModeSelectedTrackBandwidth: TEdit
            Left = 126
            Top = 71
            Width = 53
            Height = 21
            TabOrder = 3
            Text = '2.0'
          end
          object edtRadarJammingSelectedTrackModeTrack: TEdit
            Left = 126
            Top = 5
            Width = 53
            Height = 21
            Enabled = False
            TabOrder = 0
            Text = '1022'
          end
        end
      end
      object edtRadarJammingControlMode: TEdit
        Left = 64
        Top = 96
        Width = 208
        Height = 21
        ReadOnly = True
        TabOrder = 0
        Text = 'Barrage'
      end
    end
  end
  object pmNoiseJammerMode: TPopupMenu
    Left = 274
    Top = 62
    object mniJammBarrage: TMenuItem
      Tag = 1
      Caption = 'Barrage'
      OnClick = OnMenuItemClick
    end
    object mniJammFreq: TMenuItem
      Tag = 2
      Caption = 'Spot Jamming - Frequency'
      OnClick = OnMenuItemClick
    end
    object mniJammSpotNumber: TMenuItem
      Tag = 3
      Caption = 'Spot Jamming - Spot Number'
      OnClick = OnMenuItemClick
    end
    object mniJammSelectedTrack: TMenuItem
      Tag = 4
      Caption = 'Spot Jamming - Selected Track'
      OnClick = OnMenuItemClick
    end
  end
end
