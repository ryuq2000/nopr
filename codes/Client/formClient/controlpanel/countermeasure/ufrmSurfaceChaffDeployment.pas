unit ufrmSurfaceChaffDeployment;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls, ufrmECMType, uGameData_TTT,
  uT3MountedSurfaceChaff,uT3MountedECM;

type
  TfrmSurfaceChaffDeployment = class(TfrmECMType)
    grpSurfaceChaffDeployment: TGroupBox;
    scrlbx1: TScrollBox;
    bvl1: TBevel;
    lbl1: TLabel;
    lbl2: TLabel;
    lbl3: TLabel;
    lbl4: TLabel;
    lbl5: TLabel;
    lbl6: TLabel;
    lbl7: TLabel;
    lbl8: TLabel;
    lbl9: TLabel;
    lblSurfaceChaffQuantity: TLabel;
    btnSurfaceChaffLauncher: TSpeedButton;
    btnSurfaceChaffType: TSpeedButton;
    btnSurfaceChaffCopy: TSpeedButton;
    btnSurfaceChaffLaunch: TSpeedButton;
    btnSurfaceChaffAbort: TSpeedButton;
    lbl10: TLabel;
    bvl2: TBevel;
    lbl11: TLabel;
    lbl12: TLabel;
    lbl13: TLabel;
    lbl14: TLabel;
    edtSurfaceChaffLauncher: TEdit;
    edtSurfaceChaffBearing: TEdit;
    edtSurfaceChaffType: TEdit;
    edtSurfaceChaffBloomRange: TEdit;
    edtSurfaceChaffBloomAltitude: TEdit;
    edtSurfaceChaffSalvoSize: TEdit;
    edtSurfaceChaffDelay: TEdit;
    chkSurfaceChaffEnabled: TCheckBox;
    chkSurfaceChaffSeductionEnabled: TCheckBox;
    procedure onBtnSurfaceCHaffClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure UpdateForm; override;
    procedure SetControlledECM(aObj : TObject); override;
  end;

var
  frmSurfaceChaffDeployment: TfrmSurfaceChaffDeployment;

implementation

{$R *.dfm}

{ TfrmSurfaceChaffDeployment }

procedure TfrmSurfaceChaffDeployment.onBtnSurfaceCHaffClick(Sender: TObject);
var
  pt: TPoint;
  rLauncher : TRecCmd_LaunchChaffState;
begin
  inherited;

  if FControlledECM = nil then
    Exit;

  if not (FControlledECM is TT3MountedSurfaceChaff) then
    Exit;

  if TT3MountedSurfaceChaff(FControlledECM).Status = esDamaged then
    Exit;

  rLauncher.ParentPlatformID := TT3MountedSurfaceChaff(FControlledECM).PlatformParent.InstanceIndex;
  rLauncher.ChaffOnVehicleID := TT3MountedSurfaceChaff(FControlledECM).InstanceIndex;
  //rLauncher.LauncherID := TT3ChaffOnVehicle(focusedECM).Launcher;

  case TSpeedButton(Sender).Tag of
    1 : //Menu Launcher Click
      begin
        GetCursorPos(pt);
        //mChaffLauncher.Popup(pt.X, pt.Y);
      end;
    2 : //Menu Type Click
      begin
        GetCursorPos(pt);
        //pmChaffType.Popup(pt.X, pt.Y);
      end;
    3 : //Copy to All Launcher
      begin
        //simMgrClient.netSend_CopyChaffLauncherProperty(rLauncher);
      end;
    4 : //Launch Chaff
      begin
//        if not (TT3ChaffOnVehicle(focusedECM).Quantity > 0) or
//           not (TT3ChaffOnVehicle(focusedECM).ChaffLaunchers.Count > 0) then
//          Exit;
//
//        rLauncher.StateID := 1;
//        SimMgrClient.netSend_CmdLaunchChaffState(rLauncher);
      end;
    5 : //Abort Launch
      begin
//        if not (TT3ChaffOnVehicle(focusedECM).ChaffLaunchers.Count > 0) then
//          Exit;
//
//        rLauncher.StateID := 0;
//        SimMgrClient.netSend_CmdLaunchChaffState(rLauncher);
      end;
  end;
end;

procedure TfrmSurfaceChaffDeployment.SetControlledECM(aObj: TObject);
begin
  inherited;

  if Assigned( FControlledECM ) then
  with TT3MountedSurfaceChaff(FControlledECM) do
  begin
    lblSurfaceChaffQuantity.Caption := IntToStr(Quantity);
  end;

end;

procedure TfrmSurfaceChaffDeployment.UpdateForm;
begin
  inherited;
  if Assigned( FControlledECM ) then
  with TT3MountedSurfaceChaff(FControlledECM) do
  begin
    lblSurfaceChaffQuantity.Caption := IntToStr(Quantity);
  end;

end;

end.
