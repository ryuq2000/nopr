unit ufrmAcousticDecoy;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls, ufrmECMType, menus,
  uT3MountedAcousticDeploy, uT3MountedECM, uGameData_TTT;

type
  TfrmAcousticDecoy = class(TfrmECMType)
    grpAcousticDecoy: TGroupBox;
    scrlbx1: TScrollBox;
    lbl1: TLabel;
    bvl1: TBevel;
    lbl2: TLabel;
    lbl3: TLabel;
    lbl4: TLabel;
    lbl5: TLabel;
    bvl2: TBevel;
    lbl6: TLabel;
    lbl7: TLabel;
    lbl8: TLabel;
    lbl9: TLabel;
    btnComboAcousticDecoyMode: TSpeedButton;
    btnComboAcousticDecoyFilter: TSpeedButton;
    btnAcousticDecoyActionDeploy: TSpeedButton;
    btnAcousticDecoyActionStow: TSpeedButton;
    btnAcousticDecoyActivationOn: TSpeedButton;
    btnAcousticDecoyActivationOff: TSpeedButton;
    btnAcousticDecoyCycleTimerOn: TSpeedButton;
    btnAcousticDecoyCycleTimerOff: TSpeedButton;
    edtAcousticDecoyMode: TEdit;
    edtAcousticDecoyFilter: TEdit;
    procedure btnECMAcousticDecoyOnClick(Sender: TObject);
  private
    { Private declarations }
    mnPopup : TPopupMenu;
    procedure InitPacket(var rec : TRecCmdAcousticDecoyOnBoard);

    procedure ClearMenuItem(menu : TPopupMenu);
    procedure CreateModeMenuItem(menu : TPopupMenu);
    procedure CreateFilterMenuItem(menu : TPopupMenu);
    procedure MenuItemAcousticSelected(Sender : TObject);
  public
    { Public declarations }
    procedure UpdateForm; override;
    procedure SetControlledECM(aObj : TObject); override;
  end;

var
  frmAcousticDecoy: TfrmAcousticDecoy;

implementation

uses
  uGlobalVar;

{$R *.dfm}

const
  AcousticMode : array [0..3] of string =
                 ('Swept Frequency','Noise','Pulsed Noise','Alternating');

  ECMStatus    : array [0..10] of string =
                 ('Available', 'Launching Chaff', 'Unavailable', 'Damaged', 'On',
                  'Off', 'EMCON', 'Automatic', 'Manual', 'Deployed', 'Stowed');

{ TfrmAcousticDecoy }

procedure TfrmAcousticDecoy.btnECMAcousticDecoyOnClick(Sender: TObject);
var
  pt: TPoint;
  rec : TRecCmdAcousticDecoyOnBoard;
begin
  inherited;

  if FControlledECM = nil then
    exit;
  if not (FControlledECM is TT3MountedAcousticDeploy) then
    exit;
  if TT3MountedAcousticDeploy(FControlledECM).Status = esDamaged then
    Exit;

  with TT3MountedAcousticDeploy(FControlledECM) do
  begin
    InitPacket(rec);

    case TSpeedButton(Sender).Tag of
      { ini harusnya kirim ke server dulu }
      1 :
      begin
        rec.OrderID := CORD_ACOUSTIC_ACTION;
        rec.Value := Byte(edaDeploy);
        simManager.NetCmdSender.CmdAcousticDecoyOnBoard(rec);
      end;
      2 :
      begin
        rec.OrderID := CORD_ACOUSTIC_ACTION;
        rec.Value := Byte(edaStow);
        simManager.NetCmdSender.CmdAcousticDecoyOnBoard(rec);
      end;
      3 :
      begin
        rec.OrderID := CORD_ACOUSTIC_ACTIVATION;
        rec.Value := Byte(ecaOn);
        simManager.NetCmdSender.CmdAcousticDecoyOnBoard(rec);
      end;
      4 :
      begin
        rec.OrderID := CORD_ACOUSTIC_ACTIVATION;
        rec.Value := Byte(ecaOff);
        simManager.NetCmdSender.CmdAcousticDecoyOnBoard(rec);
      end;
      5 :
      begin
        rec.OrderID := CORD_ACOUSTIC_CYCLE_TIMER;
        rec.Value := Byte(ectOn);
        simManager.NetCmdSender.CmdAcousticDecoyOnBoard(rec);
      end;
      6 :
      begin
        rec.OrderID := CORD_ACOUSTIC_CYCLE_TIMER;
        rec.Value := Byte(ectOff);
        simManager.NetCmdSender.CmdAcousticDecoyOnBoard(rec);
      end;
      7 :
      begin
        GetCursorPos(pt);
        CreateModeMenuItem(mnPopup);
        mnPopup.Popup(pt.X, pt.Y);
      end;
      8 :
      begin
        GetCursorPos(pt);
        CreateFilterMenuItem(mnPopup);
        mnPopup.Popup(pt.X, pt.Y);
      end;
    end;
  end;
end;

procedure TfrmAcousticDecoy.ClearMenuItem(menu: TPopupMenu);
begin
  menu.Items.Clear;
end;

procedure TfrmAcousticDecoy.CreateFilterMenuItem(menu: TPopupMenu);
var
  item : TMenuItem;
  i : integer;
begin
  if not Assigned(mnPopup) then
    mnPopup := TPopupMenu.Create(Self);

  ClearMenuItem(mnPopup);
  for I := 0 to 2 do
  begin
    item := TMenuItem.Create(Self);
    item.Caption := IntToStr(i + 1);
    item.Tag     := i + 5;
    item.OnClick := MenuItemAcousticSelected;

    mnPopup.Items.Add(item);
  end;
end;

procedure TfrmAcousticDecoy.CreateModeMenuItem(menu: TPopupMenu);
var
  item : TMenuItem;
  i : integer;
begin
  if not Assigned(mnPopup) then
    mnPopup := TPopupMenu.Create(Self);

  ClearMenuItem(mnPopup);
  for I := 0 to Length(AcousticMode) - 1 do
  begin
    item := TMenuItem.Create(Self);
    item.Caption := AcousticMode[i];
    item.Tag     := i + 1;
    item.OnClick := MenuItemAcousticSelected;

    mnPopup.Items.Add(item);
  end;
end;

procedure TfrmAcousticDecoy.InitPacket(var rec: TRecCmdAcousticDecoyOnBoard);
begin
  if FControlledECM is TT3MountedAcousticDeploy then
  begin
    rec.PlatformIndex := TT3MountedAcousticDeploy(FControlledECM).PlatformParentInstanceIndex;
    rec.AssetIndex    := TT3MountedAcousticDeploy(FControlledECM).InstanceIndex;
  end;
end;

procedure TfrmAcousticDecoy.MenuItemAcousticSelected(Sender: TObject);
var
  rec : TRecCmdAcousticDecoyOnBoard;
begin
  InitPacket(rec);
  case TMenuItem(Sender).Tag of
    1, 2, 3, 4 :
    begin
      edtAcousticDecoyMode.Text := AcousticMode[TMenuItem(Sender).Tag - 1];

      if FControlledECM is TT3MountedAcousticDeploy then
      begin
        rec.OrderID := CORD_ACOUSTIC_MODE;
        rec.Value := TMenuItem(Sender).Tag - 1;
        simManager.NetCmdSender.CmdAcousticDecoyOnBoard(rec);
      end;
    end;
    5,6,7 :
    begin
      edtAcousticDecoyFilter.Text := IntToStr(TMenuItem(Sender).Tag - 4);

      if FControlledECM is TT3MountedAcousticDeploy then
      begin
        rec.OrderID := CORD_ACOUSTIC_FILTER;
        rec.Value := TMenuItem(Sender).Tag - 4;
        simManager.NetCmdSender.CmdAcousticDecoyOnBoard(rec);
      end;
    end;
  end;

end;

procedure TfrmAcousticDecoy.SetControlledECM(aObj: TObject);
begin
  inherited;

  if not (aObj is TT3MountedAcousticDeploy) then
    exit;

  with TT3MountedAcousticDeploy(aObj) do
  begin
    case DeploymentAction of
      edaDeploy : btnAcousticDecoyActionDeploy.Down := true;
      edaStow   : btnAcousticDecoyActionStow.Down   := true;
    end;

    case Control of
      ecaOn   : btnAcousticDecoyActivationOn.Down  := true;
      ecaOff  : btnAcousticDecoyActivationOff.Down := true;
    end;

    case CycleTimer of
      ectOn   : btnAcousticDecoyCycleTimerOn.Down  := true;
      ectOff  : btnAcousticDecoyCycleTimerOff.Down := true;
    end;

    edtAcousticDecoyMode.Text   := AcousticMode[Byte(Mode)];
    edtAcousticDecoyFilter.Text := IntToStr(FilterSetting);
  end;

end;

procedure TfrmAcousticDecoy.UpdateForm;
begin
  inherited;

  if not (FControlledECM  is TT3MountedAcousticDeploy) then
    exit;

  with TT3MountedAcousticDeploy(FControlledECM) do
  begin
    case DeploymentAction of
      edaDeploy : btnAcousticDecoyActionDeploy.Down := true;
      edaStow   : btnAcousticDecoyActionStow.Down   := true;
    end;

    case Control of
      ecaOn   : btnAcousticDecoyActivationOn.Down  := true;
      ecaOff  : btnAcousticDecoyActivationOff.Down := true;
    end;

    case CycleTimer of
      ectOn   : btnAcousticDecoyCycleTimerOn.Down  := true;
      ectOff  : btnAcousticDecoyCycleTimerOff.Down := true;
    end;

    edtAcousticDecoyMode.Text   := AcousticMode[Byte(Mode)];
    edtAcousticDecoyFilter.Text := IntToStr(FilterSetting);
  end;
end;

end.
