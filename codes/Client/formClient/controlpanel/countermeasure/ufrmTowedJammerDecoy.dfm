object frmTowedJammerDecoy: TfrmTowedJammerDecoy
  Left = 0
  Top = 0
  Caption = 'frmTowedJammerDecoy'
  ClientHeight = 286
  ClientWidth = 317
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object grpTowedJammerDecoy: TGroupBox
    Left = 0
    Top = 0
    Width = 317
    Height = 286
    Align = alClient
    TabOrder = 0
    object scrlbx1: TScrollBox
      Left = 2
      Top = 15
      Width = 313
      Height = 269
      Align = alClient
      BevelOuter = bvNone
      BorderStyle = bsNone
      TabOrder = 0
      object bvl1: TBevel
        Left = 212
        Top = 203
        Width = 90
        Height = 2
      end
      object bvl2: TBevel
        Left = 44
        Top = 7
        Width = 260
        Height = 3
      end
      object bvl3: TBevel
        Left = 63
        Top = 168
        Width = 241
        Height = 3
      end
      object lbl1: TLabel
        Left = 3
        Top = 0
        Width = 35
        Height = 13
        Caption = 'Control'
      end
      object lbl2: TLabel
        Left = 45
        Top = 18
        Width = 26
        Height = 13
        Caption = 'Mode'
      end
      object lbl3: TLabel
        Left = 18
        Top = 132
        Width = 69
        Height = 13
        Caption = 'Spot Number :'
      end
      object lbl4: TLabel
        Left = 150
        Top = 18
        Width = 46
        Height = 13
        Caption = 'Targeting'
      end
      object lbl5: TLabel
        Left = 18
        Top = 111
        Width = 43
        Height = 13
        Caption = 'Bearing :'
      end
      object lbl6: TLabel
        Left = 189
        Top = 113
        Width = 48
        Height = 13
        Caption = 'degrees T'
      end
      object lbl7: TLabel
        Left = 41
        Top = 180
        Width = 30
        Height = 13
        Caption = 'Action'
      end
      object lbl8: TLabel
        Left = 130
        Top = 180
        Width = 49
        Height = 13
        Caption = 'Quantity :'
      end
      object lbl9: TLabel
        Left = 130
        Top = 197
        Width = 56
        Height = 13
        Caption = 'Tow Length'
      end
      object lbl10: TLabel
        Left = 3
        Top = 161
        Width = 57
        Height = 13
        Caption = 'Deployment'
      end
      object lbl11: TLabel
        Left = 130
        Top = 215
        Width = 47
        Height = 13
        Caption = 'Ordered :'
      end
      object lbl12: TLabel
        Left = 130
        Top = 233
        Width = 37
        Height = 13
        Caption = 'Actual :'
      end
      object lbl13: TLabel
        Left = 260
        Top = 233
        Width = 33
        Height = 13
        Caption = 'metres'
      end
      object lbl14: TLabel
        Left = 260
        Top = 215
        Width = 33
        Height = 13
        Caption = 'metres'
      end
      object lblTowedJammerDecoyActual: TLabel
        Left = 212
        Top = 233
        Width = 8
        Height = 13
        Caption = '--'
      end
      object lblTowedJammerDecoyQuantity: TLabel
        Left = 212
        Top = 180
        Width = 6
        Height = 13
        Caption = '1'
      end
      object btnTowedJammerDecoyActionDeploy: TSpeedButton
        Left = 18
        Top = 195
        Width = 80
        Height = 25
        AllowAllUp = True
        GroupIndex = 3
        Down = True
        Caption = 'Deploy'
        Enabled = False
      end
      object btnTowedJammerDecoyActionStow: TSpeedButton
        Left = 18
        Top = 218
        Width = 80
        Height = 25
        AllowAllUp = True
        GroupIndex = 3
        Caption = 'Stow'
        Enabled = False
      end
      object btnTowedJammerDecoyModeAuto: TSpeedButton
        Left = 18
        Top = 32
        Width = 80
        Height = 25
        AllowAllUp = True
        GroupIndex = 1
        Caption = 'Automatic'
        Enabled = False
      end
      object btnTowedJammerDecoyModeManual: TSpeedButton
        Left = 18
        Top = 55
        Width = 80
        Height = 25
        AllowAllUp = True
        GroupIndex = 1
        Down = True
        Caption = 'Manual'
        Enabled = False
      end
      object btnTowedJammerDecoyModeOff: TSpeedButton
        Left = 18
        Top = 78
        Width = 80
        Height = 25
        AllowAllUp = True
        GroupIndex = 1
        Caption = 'Off'
        Enabled = False
      end
      object btnTowedJammerDecoyTargetingSpot: TSpeedButton
        Left = 130
        Top = 32
        Width = 80
        Height = 25
        AllowAllUp = True
        GroupIndex = 2
        Down = True
        Caption = 'Spot'
        Enabled = False
      end
      object btnTowedJammerDecoyTargetingTrack: TSpeedButton
        Left = 130
        Top = 55
        Width = 80
        Height = 25
        AllowAllUp = True
        GroupIndex = 2
        Caption = 'Track'
        Enabled = False
      end
      object edtTowedJammerDecoyOrdered: TEdit
        Left = 212
        Top = 211
        Width = 40
        Height = 21
        Enabled = False
        TabOrder = 0
      end
      object edtTowedJammerDecoyBearing: TEdit
        Left = 93
        Top = 111
        Width = 88
        Height = 21
        Enabled = False
        TabOrder = 1
        Text = '000'
      end
      object edtTowedJammerDecoySpotNumb: TEdit
        Left = 93
        Top = 133
        Width = 88
        Height = 21
        Enabled = False
        TabOrder = 2
        Text = '---'
      end
    end
  end
end
