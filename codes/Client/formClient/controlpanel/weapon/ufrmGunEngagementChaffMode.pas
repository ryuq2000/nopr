unit ufrmGunEngagementChaffMode;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls,ufrmWeaponType;

type
  TfrmGunEngagementChaff = class(TfrmWeaponType)
    grpGunEngagementChaffMode: TGroupBox;
    scrlbx1: TScrollBox;
    lbl1: TLabel;
    bvl1: TBevel;
    lbl2: TLabel;
    lbl3: TLabel;
    lbl4: TLabel;
    lbl5: TLabel;
    lbl6: TLabel;
    lblChaffControlQuantity: TLabel;
    lbl7: TLabel;
    lbl8: TLabel;
    bvl2: TBevel;
    lbl9: TLabel;
    lbl10: TLabel;
    btnChaffType: TSpeedButton;
    btnChaffBloomPosition: TSpeedButton;
    btnGunEngagementChaffContolAuto: TSpeedButton;
    btnGunEngagementChaffContolManual: TSpeedButton;
    btnChaffDisplayShow: TSpeedButton;
    btnChaffDisplayHide: TSpeedButton;
    btnChaffBlindZoneShow: TSpeedButton;
    btnChaffBlindZoneHide: TSpeedButton;
    btnGunEngagementChaffContolChaff: TSpeedButton;
    edtChaffControlChaff: TEdit;
    edtChaffControlBloomPosition: TEdit;
    edtChaffControlBloomAltitude: TEdit;
    btnChaffFire: TButton;
    btnChaffCeaseFire: TButton;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmGunEngagementChaff: TfrmGunEngagementChaff;

implementation

{$R *.dfm}

end.
