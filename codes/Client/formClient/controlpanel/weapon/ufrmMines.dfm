object frmMines: TfrmMines
  Left = 0
  Top = 0
  Caption = 'frmMines'
  ClientHeight = 235
  ClientWidth = 324
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object grpMines: TGroupBox
    Left = 0
    Top = 0
    Width = 324
    Height = 235
    Align = alClient
    TabOrder = 0
    object lbl1: TLabel
      Left = 3
      Top = 3
      Width = 31
      Height = 13
      Caption = 'Status'
    end
    object lblStatusMines: TLabel
      Left = 22
      Top = 31
      Width = 43
      Height = 13
      Caption = 'Available'
    end
    object lbl2: TLabel
      Left = 94
      Top = 31
      Width = 49
      Height = 13
      Caption = 'Quantity :'
    end
    object lblMinesQuantity: TLabel
      Left = 150
      Top = 31
      Width = 12
      Height = 13
      Caption = '---'
    end
    object lbl3: TLabel
      Left = 3
      Top = 60
      Width = 35
      Height = 13
      Caption = 'Control'
    end
    object bvl1: TBevel
      Left = 40
      Top = 67
      Width = 260
      Height = 3
    end
    object lbl4: TLabel
      Left = 22
      Top = 90
      Width = 36
      Height = 13
      Caption = 'Depth :'
    end
    object bvl2: TBevel
      Left = 40
      Top = 10
      Width = 260
      Height = 3
    end
    object lbl84: TLabel
      Left = 172
      Top = 90
      Width = 33
      Height = 13
      Caption = 'metres'
    end
    object edtEdtMinesDepth: TEdit
      Left = 99
      Top = 86
      Width = 53
      Height = 21
      TabOrder = 0
      Text = '0'
    end
    object btnMinesDeploy: TButton
      Left = 236
      Top = 193
      Width = 80
      Height = 25
      Caption = 'Deploy'
      TabOrder = 1
    end
  end
end
