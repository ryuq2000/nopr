unit ufrmHybridMissile;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, Buttons,ufrmWeaponType, uT3MountedHybrid;

type
  TfrmHybridMissile = class(TfrmWeaponType)
    grpHybridMissile: TGroupBox;
    scrlbx1: TScrollBox;
    btnHybridMissileDisplayBlindZonesHide: TSpeedButton;
    btnHybridMissileDisplayBlindZonesShow: TSpeedButton;
    lbl87: TLabel;
    lbl88: TLabel;
    btnHybridMissileDisplayRangeShow: TSpeedButton;
    btnHybridMissileDisplayRangeHide: TSpeedButton;
    lbl89: TLabel;
    bvl21: TBevel;
    lbl90: TLabel;
    lbl91: TLabel;
    lbl92: TLabel;
    lbl93: TLabel;
    bvl22: TBevel;
    lbl94: TLabel;
    lbl95: TLabel;
    lbl96: TLabel;
    btnSearchHybridMissileTargetTrack: TSpeedButton;
    btnAddHybridMissileTargetAimpoint: TSpeedButton;
    bvl23: TBevel;
    lbl97: TLabel;
    lblHybridMissileStatus: TLabel;
    lbl98: TLabel;
    lblHybridMissileQuantity: TLabel;
    bvl24: TBevel;
    lbl99: TLabel;
    btnHybridMissileLaunch: TButton;
    edtHybridMissileControlSalvoSize: TEdit;
    edtHybridMissileControlCruiseAltitude: TEdit;
    edtHybridMissileControlSeekerRange: TEdit;
    btnDefaultHybridMissileControlSeekerRange: TButton;
    btnDefaultHybridMissileControlCruiseAltitude: TButton;
    edtHybridMissiletargetBearing: TEdit;
    rbHybridMissileTargetBearing: TRadioButton;
    rbHybridMissileTargetAimpoint: TRadioButton;
    rbHybridMissileTargetTrack: TRadioButton;
    edtHybridMissiletargetTrack: TEdit;
    edtHybridMissiletargetAimpoint: TEdit;
    btnBringToHookHybridMissileTargetBearing: TButton;
    procedure OnHybridMissileClick(Sender: TObject);
  private
    { Private declarations }
    FMountedMissile    : TT3MountedHybrid;
    function CheckedFunctionality : Boolean;
    procedure ShowRange;
    procedure HideRange;
    procedure ShowBlindZone;
    procedure HideBlindZone;
    procedure Launch;
    procedure TargetTrack;
    procedure TargetAimPoint;
    procedure BearingToHook;
    procedure CruiseDefault;
    procedure RangeDefault;
  public
    { Public declarations }
    procedure SetControlledWeapon(aObj : TObject); override;
    procedure UpdateForm; override;
  end;

var
  frmHybridMissile: TfrmHybridMissile;

implementation

uses
  uGameData_TTT, uT3ClientManager, tttData, uBaseCoordSystem,
  uGlobalVar, uT3PlatformInstance;

{$R *.dfm}

procedure TfrmHybridMissile.BearingToHook;
begin

end;

function TfrmHybridMissile.CheckedFunctionality: Boolean;
begin
  Result := True;
  if ControlledWeapon = nil then
  begin
    UpdatePanelStatus('Not Found Selected Weapon');
    Result := False;
    Exit;
  end;

  if not Assigned(clientManager.ControlledTrack) then
  begin
    UpdatePanelStatus('Controlled Vehicle Not Valid');
    Result := False;
    Exit;
  end;

  if not(ControlledWeapon is TT3MountedHybrid) then
  begin
    UpdatePanelStatus('Selected Weapon Is Not Missile');
    Result := False;
    Exit;
  end;

  if TT3MountedHybrid(ControlledWeapon).WeaponStatus = wsDamaged then
  begin
    UpdatePanelStatus('Selected Weapon Is Damaged');
    Result := False;
    Exit;
  end;

  if clientManager.GameState <> gsPlaying then
  begin
    UpdatePanelStatus('Game Frozen');
    Result := False;
    Exit;
  end;

end;

procedure TfrmHybridMissile.CruiseDefault;
begin
//  edtHybridMissileControlCruiseAltitude.Text :=
//    FloatToStr(FMountedMissile.HybridDefinition.FMissile_Def.Default_Altitude);
end;

procedure TfrmHybridMissile.HideBlindZone;
begin
  FMountedMissile.ShowBlindZone := False;
//  FVisibleShowBlind := True;
//  HybridMissile.ShowBlindSelected := FVisibleShowBlind and HybridMissile.ShowBlind;

end;

procedure TfrmHybridMissile.HideRange;
begin
  FMountedMissile.ShowRange := False;
//  FVisibleShowRange := True;
//  HybridMissile.ShowRangeSelected := FVisibleShowRange and
//  HybridMissile.ShowRange;
end;

procedure TfrmHybridMissile.Launch;
var
  Range          : Double;
begin
//  Range := CalcRange(
//              HybridMissile.getPositionX,
//              HybridMissile.getPositionY,
//              HybridMissile.Aimpoint_Movement.PositionX,
//              HybridMissile.Aimpoint_Movement.PositionY);
//
//
//  if Range <= HybridMissile.HybridDefinition.FMissile_Def.Min_Range then
//    frmTacticalDisplay.addStatus('Target is too close')
//  else
//  if Range >= HybridMissile.HybridDefinition.FMissile_Def.Max_Range then
//    frmTacticalDisplay.addStatus('Target is too far')
//  else begin
//    if HybridMissile.Quantity <> 0 then
//    begin
//      with RecMissileEnvi do
//      begin
//        platformID        := TT3PlatformInstance(FControlled).InstanceIndex;
//        WeaponIndex       := HybridMissile.InstanceIndex;
//        WeaponName        := HybridMissile.InstanceName;
//        Missile_Kind      := Integer(HybridMissile.WeaponCategory);
//        MIssile_Method    := -1;
//        Missile_PositionX := HybridMissile.Aimpoint_Movement.PositionX;
//        Missile_PositionY := HybridMissile.Aimpoint_Movement.PositionY;
//        Bearing           := 0;
//        isHybrid          := True;
//      end;
//
//      simMgrClient.netSend_CmdSyncMissileEnvi(RecMissileEnvi);
//      sleep(100);
//
//      rLaunchMis.Order   := CORD_ID_launch_hybrid;
//      rLaunchMis.ParentPlatformID := TT3PlatformInstance(FControlled).InstanceIndex;
//      rLaunchMis.MissileID        := HybridMissile.InstanceIndex;
//      rLaunchMis.MissileName      := HybridMissile.InstanceName;
//      // other set default
//      rLaunchMis.TargetPlatformID := 0;
//      rLaunchMis.ProjectileInstanceIndex := 0;
//      rLaunchMis.FiringMode       := 0;
//  //            rLaunchMis.SalvoSize  = StrToInt(edtHybridMissileControlSalvoSize.Text);
//
//      simMgrClient.netSend_CmdLaunch_Missile(rLaunchMis);
//
//      // tombol launch didisable, enable waktu setelah terima launch dari server
//      TButton(Sender).Enabled := False;
//    end;
//        end;
end;

procedure TfrmHybridMissile.OnHybridMissileClick(Sender: TObject);
begin
  if not CheckedFunctionality then
    Exit;

  if Sender.ClassType = TSpeedButton then
  begin
    case TSpeedButton(Sender).Tag of
      1 : TargetTrack;
      2 : TargetAimPoint;
      3 : ShowRange;
      4 : HideRange;
      5 : ShowBlindZone;
      6 : HideBlindZone;
    end;
  end
  else
  if Sender.ClassType = TButton then
  begin
    case TButton(Sender).Tag of
      1 : BearingToHook;
      2 : CruiseDefault;
      3 : RangeDefault;
      4 : Launch;
    end;
  end;
end;

procedure TfrmHybridMissile.RangeDefault;
begin
//  edtHybridMissileControlSeekerRange.Text :=
//    FloatToStr(FMountedMissile.HybridDefinition.FMissile_Def.Seeker_TurnOn_Range);
end;

procedure TfrmHybridMissile.SetControlledWeapon(aObj: TObject);
begin
  inherited;

  if Assigned(ControlledWeapon) and (ControlledWeapon is TT3MountedHybrid) then
  with (TT3MountedHybrid(ControlledWeapon)) do
  begin
    FMountedMissile := TT3MountedHybrid(ControlledWeapon);
  end;

end;

procedure TfrmHybridMissile.ShowBlindZone;
begin
  FMountedMissile.ShowBlindZone := true;
//  FVisibleShowBlind := True;
//  HybridMissile.ShowBlindSelected := FVisibleShowBlind and HybridMissile.ShowBlind;
end;

procedure TfrmHybridMissile.ShowRange;
begin
  FMountedMissile.ShowRange := True;
//  FVisibleShowRange := True;
//  HybridMissile.ShowRangeSelected := FVisibleShowRange and
//  HybridMissile.ShowRange;

end;

procedure TfrmHybridMissile.TargetAimPoint;
begin

end;

procedure TfrmHybridMissile.TargetTrack;
begin

end;

procedure TfrmHybridMissile.UpdateForm;
begin
  inherited;

end;

end.
