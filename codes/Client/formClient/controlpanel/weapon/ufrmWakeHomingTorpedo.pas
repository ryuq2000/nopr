unit ufrmWakeHomingTorpedo;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls, ufrmWeaponType;

type
  TfrmWakeHomingTorpedo = class(TfrmWeaponType)
    grpWakeHomingTorpedos: TGroupBox;
    scrlbx1: TScrollBox;
    bvl1: TBevel;
    bvl2: TBevel;
    bvl3: TBevel;
    bvl4: TBevel;
    btnWakeHomingTargetTrack: TSpeedButton;
    lbl1: TLabel;
    lbl2: TLabel;
    lbl3: TLabel;
    lbl4: TLabel;
    lbl5: TLabel;
    lbl6: TLabel;
    lbl7: TLabel;
    lbl8: TLabel;
    lbl9: TLabel;
    lbl10: TLabel;
    lbl11: TLabel;
    lbl12: TLabel;
    lbl13: TLabel;
    lbl14: TLabel;
    lbl15: TLabel;
    lbl16: TLabel;
    lblWakeHomingStatus: TLabel;
    lblWakeHomingTargetIdentity: TLabel;
    lblWHQuantity: TLabel;
    lblWakeHomingTargetTarget: TLabel;
    btnWakeHomingDisplayBlindHide: TSpeedButton;
    btnWakeHomingDisplayBlindShow: TSpeedButton;
    btnWakeHomingDisplayRangeHide: TSpeedButton;
    btnWakeHomingDisplayRangeShow: TSpeedButton;
    btnLaunchWH: TButton;
    btnTargetSeekerWH: TButton;
    edtEdtLaunchBearingWH: TEdit;
    edtEdtSalvoWH: TEdit;
    edtEdtSeekerRangeWH: TEdit;
    edtEdtWHTargetTrack: TEdit;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmWakeHomingTorpedo: TfrmWakeHomingTorpedo;

implementation

{$R *.dfm}

end.
