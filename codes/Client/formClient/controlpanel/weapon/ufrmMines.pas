unit ufrmMines;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls,ufrmWeaponType;

type
  TfrmMines = class(TfrmWeaponType)
    grpMines: TGroupBox;
    lbl1: TLabel;
    lblStatusMines: TLabel;
    lbl2: TLabel;
    lblMinesQuantity: TLabel;
    lbl3: TLabel;
    bvl1: TBevel;
    lbl4: TLabel;
    bvl2: TBevel;
    lbl84: TLabel;
    edtEdtMinesDepth: TEdit;
    btnMinesDeploy: TButton;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmMines: TfrmMines;

implementation

{$R *.dfm}

end.
