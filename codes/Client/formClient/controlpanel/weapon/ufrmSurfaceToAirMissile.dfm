object frmSurfaceToAirMissile: TfrmSurfaceToAirMissile
  Left = 0
  Top = 0
  Caption = 'frmSurfaceToAirMissile'
  ClientHeight = 427
  ClientWidth = 342
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object grpSurfaceToAirMissile: TGroupBox
    Left = 0
    Top = 0
    Width = 342
    Height = 427
    Align = alClient
    TabOrder = 0
    object scrlbx1: TScrollBox
      Left = 2
      Top = 15
      Width = 338
      Height = 410
      Align = alClient
      BevelInner = bvNone
      BorderStyle = bsNone
      TabOrder = 0
      object lbl1: TLabel
        Left = 4
        Top = -2
        Width = 31
        Height = 13
        Caption = 'Status'
      end
      object bvl1: TBevel
        Left = 41
        Top = 5
        Width = 260
        Height = 3
      end
      object lblSurfaceToAirStatus: TLabel
        Left = 24
        Top = 17
        Width = 43
        Height = 13
        Caption = 'Available'
      end
      object lbl2: TLabel
        Left = 127
        Top = 17
        Width = 49
        Height = 13
        Caption = 'Quantity :'
      end
      object lblSurfaceToAirStatusQuantity: TLabel
        Left = 183
        Top = 17
        Width = 12
        Height = 13
        Caption = '---'
      end
      object lbl3: TLabel
        Left = 3
        Top = 34
        Width = 35
        Height = 13
        Caption = 'Control'
      end
      object bvl2: TBevel
        Left = 40
        Top = 41
        Width = 190
        Height = 2
      end
      object lbl4: TLabel
        Left = 26
        Top = 54
        Width = 54
        Height = 13
        Caption = 'Salvo size :'
      end
      object lbl5: TLabel
        Left = 4
        Top = 71
        Width = 32
        Height = 13
        Caption = 'Target'
      end
      object bvl3: TBevel
        Left = 41
        Top = 78
        Width = 260
        Height = 3
      end
      object lbl6: TLabel
        Left = 25
        Top = 90
        Width = 33
        Height = 13
        Caption = 'Track :'
      end
      object lbl7: TLabel
        Left = 25
        Top = 112
        Width = 41
        Height = 13
        Caption = 'Course :'
      end
      object lbl8: TLabel
        Left = 25
        Top = 132
        Width = 75
        Height = 13
        Caption = 'Ground Speed :'
      end
      object lbl9: TLabel
        Left = 25
        Top = 152
        Width = 44
        Height = 13
        Caption = 'Altitude :'
      end
      object lbl10: TLabel
        Left = 25
        Top = 173
        Width = 41
        Height = 13
        Caption = 'Priority :'
      end
      object lbl11: TLabel
        Left = 25
        Top = 193
        Width = 38
        Height = 13
        Caption = 'Status :'
      end
      object lbl12: TLabel
        Left = 25
        Top = 214
        Width = 65
        Height = 13
        Caption = 'Time to wait :'
      end
      object lbl13: TLabel
        Left = 24
        Top = 235
        Width = 88
        Height = 13
        Caption = 'Time to intercept :'
      end
      object lbl14: TLabel
        Left = 4
        Top = 257
        Width = 34
        Height = 13
        Caption = 'Display'
      end
      object bvl4: TBevel
        Left = 41
        Top = 265
        Width = 260
        Height = 3
      end
      object lbl15: TLabel
        Left = 44
        Top = 273
        Width = 31
        Height = 13
        Caption = 'Range'
      end
      object lbl16: TLabel
        Left = 151
        Top = 273
        Width = 54
        Height = 13
        Caption = 'Blind Zones'
      end
      object lblSurfaceToAirCourse: TLabel
        Left = 127
        Top = 112
        Width = 12
        Height = 13
        Caption = '---'
      end
      object lblSurfaceToAirGround: TLabel
        Left = 127
        Top = 131
        Width = 12
        Height = 13
        Caption = '---'
      end
      object lblSurfaceToAirAltitude: TLabel
        Left = 127
        Top = 152
        Width = 12
        Height = 13
        Caption = '---'
      end
      object lblSurfaceToAirStatus1: TLabel
        Left = 127
        Top = 193
        Width = 12
        Height = 13
        Caption = '---'
      end
      object lblSurfaceToAirTimeToWait: TLabel
        Left = 127
        Top = 214
        Width = 12
        Height = 13
        Caption = '---'
      end
      object lblSurfaceToAirTimeToIntercept: TLabel
        Left = 127
        Top = 235
        Width = 12
        Height = 13
        Caption = '---'
      end
      object lbl17: TLabel
        Left = 173
        Top = 112
        Width = 48
        Height = 13
        Caption = 'degrees T'
      end
      object lbl18: TLabel
        Left = 173
        Top = 131
        Width = 26
        Height = 13
        Caption = 'knots'
      end
      object lbl19: TLabel
        Left = 173
        Top = 152
        Width = 20
        Height = 13
        Caption = 'feet'
      end
      object lbl20: TLabel
        Left = 173
        Top = 214
        Width = 30
        Height = 13
        Caption = 'mm:ss'
      end
      object lbl21: TLabel
        Left = 173
        Top = 235
        Width = 30
        Height = 13
        Caption = 'mm:ss'
      end
      object btnSurfaceToAirTargetTrack: TSpeedButton
        Tag = 5
        Left = 173
        Top = 87
        Width = 23
        Height = 22
        Glyph.Data = {
          36090000424D360900000000000036000000280000001F000000180000000100
          18000000000000090000000000000000000000000000000000006161613E3E3E
          3737374040403B3B3B3A3A3A4141414141413A3A3A3A3A3A4040403737373E3E
          3E3D3D3D3838384141413A3A3A3B3B3B4040403737373E3E3E3D3D3D38383841
          41413A3A3A3B3B3B4040403737373E3E3E3C3C3C383838000000B2B2B27C7C7C
          7474747F7F7F7878787777778080808080807878787878788080807474747C7C
          7C7B7B7B7474748080807A7A7A7A7A7A7F7F7F7474747C7C7C7B7B7B74747480
          80807777777979797F7F7F7474747D7D7D7A7A7A757575000000D0D0D0B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2BDBDBDBBBBBBB2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000D0D0D0B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2C0C0C04949
          491111111111111111111212121212121A1A1AB2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B4B4B4000000D0D0D0B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B29191914D4D4D5454545F5F
          5F6565656262620000004747476565656363634D4D4D4D4D4D797979BABABAB2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B3B3B3000000D0D0D0B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B29797976969692929292929298686
          86B2B2B2ADADAD0000007D7D7DB2B2B2ABABAB292929292929545454979797B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000D0D0D0B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B23D3D3D3B3B3BB2B2B2B2B2B2B2B2
          B2B2B2B2ADADAD0000007D7D7DB2B2B2B2B2B2B2B2B2B2B2B26A6A6A0E0E0EB2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000D0D0D0B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B21616166F6F6FA5A5A5B2B2B2B2B2B2B2B2
          B2B2B2B2ADADAD0000007D7D7DB2B2B2B2B2B2B2B2B2B2B2B2AAAAAA94949413
          13138A8A8AB2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000D0D0D0B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2030303757575B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2ADADAD0000007D7D7DB2B2B2B2B2B2B2B2B2B2B2B2B2B2B2A4A4A400
          0000868686B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B3B3B3000000D0D0D0B2B2B2
          B2B2B2B2B2B2B2B2B2A6A6A68B8B8B2C2C2C838383B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2ADADAD0000007F7F7FB4B4B4B2B2B2B2B2B2B2B2B2B2B2B2A7A7A727
          27277272729C9C9CB2B2B2B2B2B2B2B2B2B2B2B2B3B3B3000000D0D0D0B2B2B2
          B2B2B2B2B2B2B2B2B27B7B7B000000BDBDBDB7B7B7B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2ADADAD000000888888BBBBBBB2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B22C2C2C4C4C4CB2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000D0D0D0B2B2B2
          B2B2B2B2B2B2B2B2B27B7B7B000000B0B0B0B3B3B3B2B2B2B2B2B2BFBFBFB6B6
          B6B2B2B2B1B1B19D9D9DADADADB3B3B3B2B2B2B2B2B2B2B2B2B2B2B2B3B3B3BF
          BFBF3030304C4C4CB2B2B2B2B2B2B2B2B2B2B2B2B4B4B4000000D0D0D0B2B2B2
          B2B2B2B2B2B2B2B2B27B7B7B0000004F4F4F5151515151515151515858585353
          53737373B2B2B2B2B2B2B2B2B28D8D8D51515151515151515151515152525258
          58581616164C4C4CB2B2B2B2B2B2B2B2B2B2B2B2B3B3B3000000D0D0D0B2B2B2
          B2B2B2B2B2B2B2B2B27B7B7B0000002424242525252525252525252525252525
          25575757B2B2B2B2B2B2B2B2B27D7D7D25252525252525252525252525252525
          25250909094C4C4CB2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000D0D0D0B2B2B2
          B2B2B2B2B2B2B2B2B27B7B7B000000AFAFAFB2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B22C2C2C4C4C4CB2B2B2B2B2B2B2B2B2B2B2B2B4B4B4000000D0D0D0B2B2B2
          B2B2B2B2B2B2B2B2B27B7B7B000000BBBBBBB6B6B6B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2ADADAD171717848484B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B22C2C2C4C4C4CB2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000D0D0D0B2B2B2
          B2B2B2B2B2B2B2B2B29999995F5F5F5A5A5A949494B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2ADADAD0000007D7D7DB2B2B2B2B2B2B2B2B2B2B2B2B5B5B5B2B2B253
          53535C5C5C828282B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000D0D0D0B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2030303757575B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2ADADAD0000007D7D7DB2B2B2B2B2B2B2B2B2B2B2B2B7B7B7AFAFAF00
          0000868686B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000D0D0D0B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2030303757575B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2ADADAD0000007D7D7DB2B2B2B2B2B2B2B2B2B2B2B2B2B2B2A4A4A400
          0000868686B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000D0D0D0B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B29A9A9A4545454B4B4BB2B2B2B2B2B2B2B2
          B2B2B2B2ADADAD0000007D7D7DB2B2B2B2B2B2B2B2B2B2B2B274747423232399
          9999ACACACB2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B4B4B4000000D0D0D0B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B27A7A7A5A5A5A5555555555559494
          94B5B5B5B4B4B4000000838383B7B7B7ADADAD555555555555585858646464B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B3B3B3000000D0D0D0B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B28282822121212121216D6D
          6D9595959898980000006E6E6E9898988B8B8B2121212121215B5B5BB2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000D0D0D0B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B23939
          39000000000000000000000000000000090909B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B4B4B4000000D0D0D0B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2A0A0
          A0979797979797979797979797979797999999B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000}
        OnClick = BtnSurfaceToAirClick
      end
      object btnSurfaceToAirDisplayRangeShow: TSpeedButton
        Tag = 1
        Left = 24
        Top = 292
        Width = 80
        Height = 25
        AllowAllUp = True
        GroupIndex = 2
        Caption = 'Show'
        OnClick = BtnSurfaceToAirClick
      end
      object btnSurfaceToAirDisplayRangeHide: TSpeedButton
        Tag = 2
        Left = 24
        Top = 316
        Width = 80
        Height = 25
        AllowAllUp = True
        GroupIndex = 2
        Down = True
        Caption = 'Hide'
        OnClick = BtnSurfaceToAirClick
      end
      object btnSurfaceToAirDisplayBlindShow: TSpeedButton
        Tag = 3
        Left = 139
        Top = 292
        Width = 80
        Height = 25
        AllowAllUp = True
        GroupIndex = 1
        Caption = 'Show'
        OnClick = BtnSurfaceToAirClick
      end
      object btnSurfaceToAirDisplayBlindHide: TSpeedButton
        Tag = 4
        Left = 139
        Top = 316
        Width = 80
        Height = 25
        AllowAllUp = True
        GroupIndex = 1
        Down = True
        Caption = 'Hide'
        OnClick = BtnSurfaceToAirClick
      end
      object edtSurfaceToAirSalvo: TEdit
        Left = 127
        Top = 50
        Width = 70
        Height = 21
        TabOrder = 0
        OnKeyPress = edtSurfaceToAirSalvoKeyPress
      end
      object edtSurfaceToAirTrack: TEdit
        Left = 127
        Top = 87
        Width = 39
        Height = 21
        ReadOnly = True
        TabOrder = 1
      end
      object btnSurfaceToAirPlan: TButton
        Tag = 1
        Left = 12
        Top = 372
        Width = 80
        Height = 25
        Caption = 'Plan'
        Enabled = False
        TabOrder = 2
        OnClick = BtnSurfaceToAirClick
      end
      object btnSurfaceToAirLaunch: TButton
        Tag = 3
        Left = 251
        Top = 372
        Width = 80
        Height = 25
        Caption = 'Launch'
        Enabled = False
        TabOrder = 3
        OnClick = BtnSurfaceToAirClick
      end
      object btnSurfaceToAirCancel: TButton
        Tag = 2
        Left = 98
        Top = 372
        Width = 80
        Height = 25
        Caption = 'Cancel'
        Enabled = False
        TabOrder = 4
        OnClick = BtnSurfaceToAirClick
      end
    end
  end
end
