object frmSurfaceToSurfaceMissile: TfrmSurfaceToSurfaceMissile
  Left = 0
  Top = 0
  Caption = 'frmSurfaceToSurfaceMissile'
  ClientHeight = 429
  ClientWidth = 344
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object grpSurfaceToSurfaceMissile: TGroupBox
    Left = 0
    Top = 0
    Width = 344
    Height = 429
    Align = alClient
    TabOrder = 0
    object scrlbx1: TScrollBox
      Left = 2
      Top = 15
      Width = 340
      Height = 412
      Align = alClient
      BevelInner = bvNone
      BevelOuter = bvNone
      BorderStyle = bsNone
      TabOrder = 0
      object lbl1: TLabel
        Left = 3
        Top = 0
        Width = 31
        Height = 13
        Caption = 'Status'
      end
      object bvl1: TBevel
        Left = 40
        Top = 7
        Width = 260
        Height = 3
      end
      object lblSurfaceToSurfaceMissileStatus: TLabel
        Left = 20
        Top = 18
        Width = 43
        Height = 13
        Caption = 'Available'
      end
      object lbl2: TLabel
        Left = 126
        Top = 19
        Width = 49
        Height = 13
        Caption = 'Quantity :'
      end
      object lblSurfaceToSurfaceMissileQuantity: TLabel
        Left = 182
        Top = 19
        Width = 12
        Height = 13
        Caption = '---'
      end
      object lbl3: TLabel
        Left = 3
        Top = 37
        Width = 32
        Height = 13
        Caption = 'Target'
      end
      object bvl2: TBevel
        Left = 40
        Top = 44
        Width = 260
        Height = 3
      end
      object lbl4: TLabel
        Left = 20
        Top = 56
        Width = 33
        Height = 13
        Caption = 'Track :'
      end
      object lbl5: TLabel
        Left = 3
        Top = 75
        Width = 35
        Height = 13
        Caption = 'Control'
      end
      object bvl3: TBevel
        Left = 40
        Top = 82
        Width = 195
        Height = 3
      end
      object lbl6: TLabel
        Left = 3
        Top = 299
        Width = 68
        Height = 13
        Caption = 'Display Range'
      end
      object bvl4: TBevel
        Left = 77
        Top = 306
        Width = 260
        Height = 3
      end
      object lbl7: TLabel
        Left = 20
        Top = 142
        Width = 51
        Height = 13
        Caption = 'Launcher :'
      end
      object lbl8: TLabel
        Left = 20
        Top = 93
        Width = 62
        Height = 13
        Caption = 'Firing Mode :'
      end
      object lbl9: TLabel
        Left = 20
        Top = 117
        Width = 73
        Height = 13
        Caption = 'Engangement :'
      end
      object btnSurfaceToSurfaceMissileFiring: TSpeedButton
        Left = 176
        Top = 88
        Width = 23
        Height = 22
        Glyph.Data = {
          D6050000424DD605000000000000360000002800000017000000140000000100
          180000000000A005000000000000000000000000000000000000B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B200
          0000B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2000000B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B200
          0000B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2C1C1C1B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2000000B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B200000000
          0000B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2000000000000000000000000C1C1C1B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2000000000000000000000000000000000000B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B200
          0000B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000000000000000000000
          000000000000000000000000C1C1C1B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2000000B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B200000000
          0000000000000000000000000000000000000000000000000000B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2000000000000000000000000000000000000000000000000B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2000000000000000000000000000000000000B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B200
          0000B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000000000000000000000
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2000000B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B200000000
          0000B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B200
          0000B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2000000B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000}
        OnClick = btnSurfaceToSurfaceMissileFiringClick
      end
      object btnSurfaceToSurfaceMissileEngagement: TSpeedButton
        Left = 176
        Top = 112
        Width = 23
        Height = 22
        Glyph.Data = {
          D6050000424DD605000000000000360000002800000017000000140000000100
          180000000000A005000000000000000000000000000000000000B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B200
          0000B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2000000B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B200
          0000B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2C1C1C1B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2000000B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B200000000
          0000B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2000000000000000000000000C1C1C1B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2000000000000000000000000000000000000B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B200
          0000B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000000000000000000000
          000000000000000000000000C1C1C1B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2000000B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B200000000
          0000000000000000000000000000000000000000000000000000B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2000000000000000000000000000000000000000000000000B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2000000000000000000000000000000000000B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B200
          0000B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000000000000000000000
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2000000B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B200000000
          0000B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B200
          0000B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2000000B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000}
        OnClick = btnSurfaceToSurfaceMissileEngagementClick
      end
      object btnSurfaceToSurfaceMissileTargetTrack: TSpeedButton
        Tag = 3
        Left = 176
        Top = 51
        Width = 23
        Height = 22
        Glyph.Data = {
          36090000424D360900000000000036000000280000001F000000180000000100
          18000000000000090000000000000000000000000000000000006161613E3E3E
          3737374040403B3B3B3A3A3A4141414141413A3A3A3A3A3A4040403737373E3E
          3E3D3D3D3838384141413A3A3A3B3B3B4040403737373E3E3E3D3D3D38383841
          41413A3A3A3B3B3B4040403737373E3E3E3C3C3C383838000000B2B2B27C7C7C
          7474747F7F7F7878787777778080808080807878787878788080807474747C7C
          7C7B7B7B7474748080807A7A7A7A7A7A7F7F7F7474747C7C7C7B7B7B74747480
          80807777777979797F7F7F7474747D7D7D7A7A7A757575000000D0D0D0B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2BDBDBDBBBBBBB2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000D0D0D0B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2C0C0C04949
          491111111111111111111212121212121A1A1AB2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B4B4B4000000D0D0D0B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B29191914D4D4D5454545F5F
          5F6565656262620000004747476565656363634D4D4D4D4D4D797979BABABAB2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B3B3B3000000D0D0D0B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B29797976969692929292929298686
          86B2B2B2ADADAD0000007D7D7DB2B2B2ABABAB292929292929545454979797B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000D0D0D0B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B23D3D3D3B3B3BB2B2B2B2B2B2B2B2
          B2B2B2B2ADADAD0000007D7D7DB2B2B2B2B2B2B2B2B2B2B2B26A6A6A0E0E0EB2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000D0D0D0B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B21616166F6F6FA5A5A5B2B2B2B2B2B2B2B2
          B2B2B2B2ADADAD0000007D7D7DB2B2B2B2B2B2B2B2B2B2B2B2AAAAAA94949413
          13138A8A8AB2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000D0D0D0B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2030303757575B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2ADADAD0000007D7D7DB2B2B2B2B2B2B2B2B2B2B2B2B2B2B2A4A4A400
          0000868686B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B3B3B3000000D0D0D0B2B2B2
          B2B2B2B2B2B2B2B2B2A6A6A68B8B8B2C2C2C838383B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2ADADAD0000007F7F7FB4B4B4B2B2B2B2B2B2B2B2B2B2B2B2A7A7A727
          27277272729C9C9CB2B2B2B2B2B2B2B2B2B2B2B2B3B3B3000000D0D0D0B2B2B2
          B2B2B2B2B2B2B2B2B27B7B7B000000BDBDBDB7B7B7B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2ADADAD000000888888BBBBBBB2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B22C2C2C4C4C4CB2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000D0D0D0B2B2B2
          B2B2B2B2B2B2B2B2B27B7B7B000000B0B0B0B3B3B3B2B2B2B2B2B2BFBFBFB6B6
          B6B2B2B2B1B1B19D9D9DADADADB3B3B3B2B2B2B2B2B2B2B2B2B2B2B2B3B3B3BF
          BFBF3030304C4C4CB2B2B2B2B2B2B2B2B2B2B2B2B4B4B4000000D0D0D0B2B2B2
          B2B2B2B2B2B2B2B2B27B7B7B0000004F4F4F5151515151515151515858585353
          53737373B2B2B2B2B2B2B2B2B28D8D8D51515151515151515151515152525258
          58581616164C4C4CB2B2B2B2B2B2B2B2B2B2B2B2B3B3B3000000D0D0D0B2B2B2
          B2B2B2B2B2B2B2B2B27B7B7B0000002424242525252525252525252525252525
          25575757B2B2B2B2B2B2B2B2B27D7D7D25252525252525252525252525252525
          25250909094C4C4CB2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000D0D0D0B2B2B2
          B2B2B2B2B2B2B2B2B27B7B7B000000AFAFAFB2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B22C2C2C4C4C4CB2B2B2B2B2B2B2B2B2B2B2B2B4B4B4000000D0D0D0B2B2B2
          B2B2B2B2B2B2B2B2B27B7B7B000000BBBBBBB6B6B6B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2ADADAD171717848484B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B22C2C2C4C4C4CB2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000D0D0D0B2B2B2
          B2B2B2B2B2B2B2B2B29999995F5F5F5A5A5A949494B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2ADADAD0000007D7D7DB2B2B2B2B2B2B2B2B2B2B2B2B5B5B5B2B2B253
          53535C5C5C828282B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000D0D0D0B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2030303757575B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2ADADAD0000007D7D7DB2B2B2B2B2B2B2B2B2B2B2B2B7B7B7AFAFAF00
          0000868686B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000D0D0D0B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2030303757575B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2ADADAD0000007D7D7DB2B2B2B2B2B2B2B2B2B2B2B2B2B2B2A4A4A400
          0000868686B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000D0D0D0B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B29A9A9A4545454B4B4BB2B2B2B2B2B2B2B2
          B2B2B2B2ADADAD0000007D7D7DB2B2B2B2B2B2B2B2B2B2B2B274747423232399
          9999ACACACB2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B4B4B4000000D0D0D0B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B27A7A7A5A5A5A5555555555559494
          94B5B5B5B4B4B4000000838383B7B7B7ADADAD555555555555585858646464B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B3B3B3000000D0D0D0B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B28282822121212121216D6D
          6D9595959898980000006E6E6E9898988B8B8B2121212121215B5B5BB2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000D0D0D0B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B23939
          39000000000000000000000000000000090909B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B4B4B4000000D0D0D0B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2A0A0
          A0979797979797979797979797979797999999B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000}
        OnClick = btnSurfaceToSurfaceClick
      end
      object btnSurfaceToSurfaceMissileDisplayRangeShow: TSpeedButton
        Tag = 1
        Left = 25
        Top = 316
        Width = 80
        Height = 25
        AllowAllUp = True
        GroupIndex = 1
        Caption = 'Show'
        OnClick = btnSurfaceToSurfaceClick
      end
      object btnSurfaceToSurfaceMissileDisplayRangeHide: TSpeedButton
        Tag = 2
        Left = 25
        Top = 340
        Width = 80
        Height = 25
        AllowAllUp = True
        GroupIndex = 1
        Down = True
        Caption = 'Hide'
        OnClick = btnSurfaceToSurfaceClick
      end
      object lblDestruckRange: TLabel
        Left = 20
        Top = 225
        Width = 76
        Height = 13
        Caption = 'Destruck Range'
      end
      object lblCrossOverRange: TLabel
        Left = 20
        Top = 202
        Width = 85
        Height = 13
        Caption = 'CrossOver Range'
      end
      object lblNmCrossOverRange: TLabel
        Left = 159
        Top = 204
        Width = 15
        Height = 13
        Caption = 'Nm'
      end
      object lblNmDestruckRange: TLabel
        Left = 159
        Top = 224
        Width = 15
        Height = 13
        Caption = 'Nm'
      end
      object edtSurfaceToSurfaceMissileTargetTrack: TEdit
        Left = 110
        Top = 52
        Width = 51
        Height = 21
        ReadOnly = True
        TabOrder = 0
      end
      object btnSurfaceToSurfacePlan: TButton
        Tag = 1
        Left = 13
        Top = 378
        Width = 80
        Height = 25
        Caption = 'Plan'
        Enabled = False
        TabOrder = 1
        OnClick = btnSurfaceToSurfaceClick
      end
      object btnSurfaceToSurfaceLaunch: TButton
        Tag = 3
        Left = 242
        Top = 378
        Width = 80
        Height = 25
        Caption = 'Launch'
        Enabled = False
        TabOrder = 2
        OnClick = btnSurfaceToSurfaceClick
      end
      object btnSurfaceToSurfaceCancel: TButton
        Tag = 2
        Left = 99
        Top = 378
        Width = 80
        Height = 25
        Caption = 'Cancel'
        Enabled = False
        TabOrder = 3
        OnClick = btnSurfaceToSurfaceClick
      end
      object edtSurfaceToSurfaceMissileFiring: TEdit
        Left = 110
        Top = 89
        Width = 52
        Height = 21
        ReadOnly = True
        TabOrder = 4
      end
      object edtSurfaceToSurfaceMissileEngangement: TEdit
        Left = 110
        Top = 114
        Width = 52
        Height = 21
        ReadOnly = True
        TabOrder = 5
      end
      object btnSurfaceToSurfaceMissileTargetTrackDetails: TButton
        Left = 237
        Top = 50
        Width = 80
        Height = 25
        Caption = 'Details...'
        TabOrder = 7
        OnClick = btnSurfaceToSurfaceMissileTargetTrackDetailsClick
      end
      object pnlSurfaceToSurfaceWp: TPanel
        Left = 14
        Top = 240
        Width = 305
        Height = 59
        BevelOuter = bvNone
        TabOrder = 8
        object bvl5: TBevel
          Left = 60
          Top = 19
          Width = 240
          Height = 3
        end
        object lbl10: TLabel
          Left = 3
          Top = 9
          Width = 51
          Height = 13
          Caption = 'Waypoints'
        end
        object btnSurfaceToSurfaceMissileWaypointsEdit: TButton
          Tag = 10
          Left = 4
          Top = 28
          Width = 58
          Height = 25
          Caption = 'Edit'
          TabOrder = 0
        end
        object btnSurfaceToSurfaceMissileWaypointsAdd: TButton
          Tag = 11
          Left = 63
          Top = 28
          Width = 58
          Height = 25
          Caption = 'Add'
          TabOrder = 1
        end
        object btnSurfaceToSurfaceMissileWaypointsDelete: TButton
          Tag = 12
          Left = 122
          Top = 28
          Width = 58
          Height = 25
          Caption = 'Delete'
          TabOrder = 2
        end
        object btnSurfaceToSurfaceMissileWaypointsApply: TButton
          Tag = 13
          Left = 180
          Top = 28
          Width = 58
          Height = 25
          Caption = 'Apply'
          TabOrder = 3
        end
        object btnSurfaceToSurfaceMissileWaypointsCancel: TButton
          Tag = 14
          Left = 239
          Top = 28
          Width = 58
          Height = 25
          Caption = 'Cancel'
          TabOrder = 4
        end
      end
      object btnSurfaceToSurfaceMissileLauncherMore: TButton
        Left = 237
        Top = 219
        Width = 80
        Height = 25
        Caption = 'More...'
        TabOrder = 6
      end
      object pnlLaunch1: TPanel
        Tag = 1
        Left = 107
        Top = 140
        Width = 23
        Height = 52
        Caption = '1'
        TabOrder = 9
        OnClick = btnLaunchClick
        object vbl1: TVrBlinkLed
          Tag = 1
          Left = 1
          Top = 1
          Width = 21
          Height = 20
          Palette1.Low = clRed
          Palette1.High = clLime
          Palette2.Low = clBlack
          Palette2.High = clAqua
          Palette3.Low = clBlack
          Palette3.High = clYellow
          Palette4.Low = clBlack
          Palette4.High = clLime
          BlinkLedType = ltLargeRect
          Margin = 2
          Spacing = 3
          Align = alTop
          Color = clMaroon
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentColor = False
          ParentFont = False
          OnClick = btnLaunchClick
          BlinkSpeed = 250
        end
      end
      object pnlLaunch2: TPanel
        Tag = 2
        Left = 132
        Top = 140
        Width = 23
        Height = 52
        Caption = '2'
        TabOrder = 10
        OnClick = btnLaunchClick
        object vbl2: TVrBlinkLed
          Tag = 2
          Left = 1
          Top = 1
          Width = 21
          Height = 20
          Palette1.Low = clRed
          Palette1.High = clLime
          Palette2.Low = clBlack
          Palette2.High = clAqua
          Palette3.Low = clBlack
          Palette3.High = clYellow
          Palette4.Low = clBlack
          Palette4.High = clLime
          BlinkLedType = ltLargeRect
          Margin = 2
          Spacing = 3
          Align = alTop
          Color = clMaroon
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentColor = False
          ParentFont = False
          OnClick = btnLaunchClick
          BlinkSpeed = 250
          ExplicitLeft = 14
          ExplicitTop = 6
        end
      end
      object pnlLaunch3: TPanel
        Tag = 3
        Left = 158
        Top = 140
        Width = 23
        Height = 52
        Caption = '3'
        TabOrder = 11
        OnClick = btnLaunchClick
        object vbl3: TVrBlinkLed
          Tag = 3
          Left = 1
          Top = 1
          Width = 21
          Height = 20
          Palette1.Low = clRed
          Palette1.High = clLime
          Palette2.Low = clBlack
          Palette2.High = clAqua
          Palette3.Low = clBlack
          Palette3.High = clYellow
          Palette4.Low = clBlack
          Palette4.High = clLime
          BlinkLedType = ltLargeRect
          Margin = 2
          Spacing = 3
          Align = alTop
          Color = clMaroon
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentColor = False
          ParentFont = False
          OnClick = btnLaunchClick
          BlinkSpeed = 250
        end
      end
      object pnlLaunch4: TPanel
        Tag = 4
        Left = 184
        Top = 140
        Width = 23
        Height = 52
        Caption = '4'
        TabOrder = 12
        OnClick = btnLaunchClick
        object vbl4: TVrBlinkLed
          Tag = 4
          Left = 1
          Top = 1
          Width = 21
          Height = 20
          Palette1.Low = clRed
          Palette1.High = clLime
          Palette2.Low = clBlack
          Palette2.High = clAqua
          Palette3.Low = clBlack
          Palette3.High = clYellow
          Palette4.Low = clBlack
          Palette4.High = clLime
          BlinkLedType = ltLargeRect
          Margin = 2
          Spacing = 3
          Align = alTop
          Color = clMaroon
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentColor = False
          ParentFont = False
          OnClick = btnLaunchClick
          BlinkSpeed = 250
          ExplicitLeft = -24
          ExplicitTop = 18
          ExplicitWidth = 50
        end
      end
      object pnlLaunch5: TPanel
        Tag = 5
        Left = 210
        Top = 140
        Width = 23
        Height = 52
        Caption = '5'
        TabOrder = 13
        OnClick = btnLaunchClick
        object vbl5: TVrBlinkLed
          Tag = 5
          Left = 1
          Top = 1
          Width = 21
          Height = 20
          Palette1.Low = clRed
          Palette1.High = clLime
          Palette2.Low = clBlack
          Palette2.High = clAqua
          Palette3.Low = clBlack
          Palette3.High = clYellow
          Palette4.Low = clBlack
          Palette4.High = clLime
          BlinkLedType = ltLargeRect
          Margin = 2
          Spacing = 3
          Align = alTop
          Color = clMaroon
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentColor = False
          ParentFont = False
          OnClick = btnLaunchClick
          BlinkSpeed = 250
          ExplicitLeft = -24
          ExplicitTop = 18
          ExplicitWidth = 50
        end
      end
      object pnlLaunch6: TPanel
        Tag = 6
        Left = 236
        Top = 140
        Width = 23
        Height = 52
        Caption = '6'
        TabOrder = 14
        OnClick = btnLaunchClick
        object vbl6: TVrBlinkLed
          Tag = 6
          Left = 1
          Top = 1
          Width = 21
          Height = 20
          Palette1.Low = clRed
          Palette1.High = clLime
          Palette2.Low = clBlack
          Palette2.High = clAqua
          Palette3.Low = clBlack
          Palette3.High = clYellow
          Palette4.Low = clBlack
          Palette4.High = clLime
          BlinkLedType = ltLargeRect
          Margin = 2
          Spacing = 3
          Align = alTop
          Color = clMaroon
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentColor = False
          ParentFont = False
          OnClick = btnLaunchClick
          BlinkSpeed = 250
          ExplicitLeft = -24
          ExplicitTop = 18
          ExplicitWidth = 50
        end
      end
      object pnlLaunch7: TPanel
        Tag = 7
        Left = 262
        Top = 140
        Width = 23
        Height = 52
        Caption = '7'
        TabOrder = 15
        OnClick = btnLaunchClick
        object vbl7: TVrBlinkLed
          Tag = 7
          Left = 1
          Top = 1
          Width = 21
          Height = 20
          Palette1.Low = clRed
          Palette1.High = clLime
          Palette2.Low = clBlack
          Palette2.High = clAqua
          Palette3.Low = clBlack
          Palette3.High = clYellow
          Palette4.Low = clBlack
          Palette4.High = clLime
          BlinkLedType = ltLargeRect
          Margin = 2
          Spacing = 3
          Align = alTop
          Color = clMaroon
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentColor = False
          ParentFont = False
          OnClick = btnLaunchClick
          BlinkSpeed = 250
          ExplicitLeft = -24
          ExplicitTop = 18
          ExplicitWidth = 50
        end
      end
      object pnlLaunch8: TPanel
        Tag = 8
        Left = 288
        Top = 140
        Width = 23
        Height = 52
        Caption = '8'
        TabOrder = 16
        OnClick = btnLaunchClick
        object vbl8: TVrBlinkLed
          Tag = 8
          Left = 1
          Top = 1
          Width = 21
          Height = 20
          Palette1.Low = clRed
          Palette1.High = clLime
          Palette2.Low = clBlack
          Palette2.High = clAqua
          Palette3.Low = clBlack
          Palette3.High = clYellow
          Palette4.Low = clBlack
          Palette4.High = clLime
          BlinkLedType = ltLargeRect
          Margin = 2
          Spacing = 3
          Align = alTop
          Color = clMaroon
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentColor = False
          ParentFont = False
          OnClick = btnLaunchClick
          BlinkSpeed = 250
          ExplicitLeft = -24
          ExplicitTop = 18
          ExplicitWidth = 50
        end
      end
      object edtDestructRange: TEdit
        Tag = 2
        Left = 110
        Top = 221
        Width = 41
        Height = 21
        TabOrder = 17
        OnKeyPress = edtDestructRangeKeyPress
      end
      object edtCrossOverRange: TEdit
        Tag = 1
        Left = 110
        Top = 198
        Width = 41
        Height = 21
        TabOrder = 18
        Text = '0'
        OnKeyPress = edtDestructRangeKeyPress
      end
    end
  end
  object pmenuFiring: TPopupMenu
    Left = 212
    Top = 23
    object RBL1: TMenuItem
      Tag = 1
      Caption = 'RBL'
      OnClick = OnMenuFiringClick
    end
    object RBLW1: TMenuItem
      Tag = 2
      Caption = 'RBLW'
      Enabled = False
      OnClick = OnMenuFiringClick
    end
    object BOL1: TMenuItem
      Tag = 3
      Caption = 'BOL'
      OnClick = OnMenuFiringClick
    end
    object BOLW1: TMenuItem
      Tag = 4
      Caption = 'BOLW'
      Enabled = False
      OnClick = OnMenuFiringClick
    end
  end
  object pmenuEngagement: TPopupMenu
    Left = 252
    Top = 24
    object Ripple1: TMenuItem
      Tag = 1
      Caption = 'Ripple'
      OnClick = OnMenuEngageClick
    end
    object STOT1: TMenuItem
      Tag = 2
      Caption = 'STOT'
      OnClick = OnMenuEngageClick
    end
    object DTOT1: TMenuItem
      Tag = 3
      Caption = 'DTOT'
      OnClick = OnMenuEngageClick
    end
  end
end
