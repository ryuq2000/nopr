object frmGunEngagementChaff: TfrmGunEngagementChaff
  Left = 0
  Top = 0
  Caption = 'frmGunEngagementChaff'
  ClientHeight = 425
  ClientWidth = 345
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object grpGunEngagementChaffMode: TGroupBox
    Left = 0
    Top = 0
    Width = 345
    Height = 425
    Align = alClient
    TabOrder = 0
    object scrlbx1: TScrollBox
      Left = 2
      Top = 15
      Width = 341
      Height = 408
      Align = alClient
      BevelInner = bvNone
      BevelOuter = bvNone
      BorderStyle = bsNone
      TabOrder = 0
      object lbl1: TLabel
        Left = 3
        Top = 6
        Width = 35
        Height = 13
        Caption = 'Control'
      end
      object bvl1: TBevel
        Left = 40
        Top = 13
        Width = 260
        Height = 3
      end
      object lbl2: TLabel
        Left = 50
        Top = 22
        Width = 26
        Height = 13
        Caption = 'Mode'
      end
      object lbl3: TLabel
        Left = 25
        Top = 121
        Width = 61
        Height = 13
        Caption = 'Chaff Type :'
      end
      object lbl4: TLabel
        Left = 25
        Top = 149
        Width = 49
        Height = 13
        Caption = 'Quantity :'
      end
      object lbl5: TLabel
        Left = 25
        Top = 177
        Width = 75
        Height = 13
        Caption = 'Bloom Position :'
      end
      object lbl6: TLabel
        Left = 25
        Top = 206
        Width = 75
        Height = 13
        Caption = 'Bloom Altitude :'
      end
      object lblChaffControlQuantity: TLabel
        Left = 146
        Top = 149
        Width = 12
        Height = 13
        Caption = '---'
      end
      object lbl7: TLabel
        Left = 200
        Top = 206
        Width = 20
        Height = 13
        Caption = 'feet'
      end
      object lbl8: TLabel
        Left = 3
        Top = 233
        Width = 34
        Height = 13
        Caption = 'Display'
      end
      object bvl2: TBevel
        Left = 40
        Top = 241
        Width = 260
        Height = 3
      end
      object lbl9: TLabel
        Left = 43
        Top = 259
        Width = 31
        Height = 13
        Caption = 'Range'
      end
      object lbl10: TLabel
        Left = 158
        Top = 258
        Width = 49
        Height = 13
        Caption = 'Blind Zone'
      end
      object btnChaffType: TSpeedButton
        Left = 227
        Top = 116
        Width = 23
        Height = 22
        Glyph.Data = {
          D6050000424DD605000000000000360000002800000017000000140000000100
          180000000000A005000000000000000000000000000000000000B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B200
          0000B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2000000B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B200
          0000B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2C1C1C1B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2000000B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B200000000
          0000B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2000000000000000000000000C1C1C1B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2000000000000000000000000000000000000B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B200
          0000B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000000000000000000000
          000000000000000000000000C1C1C1B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2000000B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B200000000
          0000000000000000000000000000000000000000000000000000B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2000000000000000000000000000000000000000000000000B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2000000000000000000000000000000000000B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B200
          0000B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000000000000000000000
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2000000B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B200000000
          0000B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B200
          0000B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2000000B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000}
      end
      object btnChaffBloomPosition: TSpeedButton
        Left = 227
        Top = 172
        Width = 23
        Height = 22
        Caption = '+'
      end
      object btnGunEngagementChaffContolAuto: TSpeedButton
        Tag = 1
        Left = 25
        Top = 39
        Width = 80
        Height = 25
        AllowAllUp = True
        GroupIndex = 4
        Down = True
        Caption = 'Automatic'
      end
      object btnGunEngagementChaffContolManual: TSpeedButton
        Tag = 2
        Left = 25
        Top = 63
        Width = 80
        Height = 25
        AllowAllUp = True
        GroupIndex = 4
        Caption = 'Manual'
      end
      object btnChaffDisplayShow: TSpeedButton
        Tag = 4
        Left = 25
        Top = 280
        Width = 80
        Height = 25
        AllowAllUp = True
        GroupIndex = 1
        Caption = 'Show'
      end
      object btnChaffDisplayHide: TSpeedButton
        Tag = 5
        Left = 25
        Top = 304
        Width = 80
        Height = 25
        AllowAllUp = True
        GroupIndex = 1
        Down = True
        Caption = 'Hide'
      end
      object btnChaffBlindZoneShow: TSpeedButton
        Tag = 6
        Left = 146
        Top = 280
        Width = 80
        Height = 25
        AllowAllUp = True
        GroupIndex = 2
        Caption = 'Show'
      end
      object btnChaffBlindZoneHide: TSpeedButton
        Tag = 7
        Left = 146
        Top = 304
        Width = 80
        Height = 25
        AllowAllUp = True
        GroupIndex = 2
        Down = True
        Caption = 'Hide'
      end
      object btnGunEngagementChaffContolChaff: TSpeedButton
        Tag = 3
        Left = 25
        Top = 87
        Width = 80
        Height = 25
        AllowAllUp = True
        GroupIndex = 4
        Caption = 'Chaff'
      end
      object edtChaffControlChaff: TEdit
        Left = 146
        Top = 117
        Width = 75
        Height = 21
        TabOrder = 0
      end
      object edtChaffControlBloomPosition: TEdit
        Left = 146
        Top = 173
        Width = 74
        Height = 21
        TabOrder = 1
      end
      object edtChaffControlBloomAltitude: TEdit
        Left = 146
        Top = 202
        Width = 47
        Height = 21
        TabOrder = 2
      end
      object btnChaffFire: TButton
        Tag = 1
        Left = 154
        Top = 366
        Width = 80
        Height = 25
        Caption = 'Fire'
        Enabled = False
        TabOrder = 3
      end
      object btnChaffCeaseFire: TButton
        Tag = 2
        Left = 251
        Top = 366
        Width = 80
        Height = 25
        Caption = 'Cease Fire'
        TabOrder = 4
      end
    end
  end
end
