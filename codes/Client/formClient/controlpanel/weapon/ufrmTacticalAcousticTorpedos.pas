unit ufrmTacticalAcousticTorpedos;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls,ufrmWeaponType;

type
  TfrmTacticalAcousticTorpedos = class(TfrmWeaponType)
    grpTacticalAcousticTorpedos: TGroupBox;
    lbl1: TLabel;
    lblTacticalAcousticTorpedosStatus: TLabel;
    lbl2: TLabel;
    lblTacticalAcousticTorpedosQuantity: TLabel;
    lbl3: TLabel;
    bvl1: TBevel;
    lbl4: TLabel;
    lbl5: TLabel;
    bvl2: TBevel;
    lbl6: TLabel;
    lbl7: TLabel;
    lbl8: TLabel;
    lblTacticalAcousticTorpedosTargetIdentity: TLabel;
    bvl3: TBevel;
    lbl9: TLabel;
    bvl4: TBevel;
    lbl10: TLabel;
    btnTacticalAcousticTorpedosTargetTrack: TSpeedButton;
    btnTacticalAcousticTorpedosDisplayRangeShow: TSpeedButton;
    btnTacticalAcousticTorpedosDisplayRangeHide: TSpeedButton;
    btnTacticalAcousticTorpedosDisplayBlindShow: TSpeedButton;
    btnTacticalAcousticTorpedosDisplayBlindHide: TSpeedButton;
    edtTacticalAcousticTorpedosTargetTrack: TEdit;
    btnTacticalAcousticTorpedosLaunch: TButton;
    edtTacticalAcousticTorpedosSalvo: TEdit;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmTacticalAcousticTorpedos: TfrmTacticalAcousticTorpedos;

implementation

{$R *.dfm}

end.
