unit ufrmAcousticTorpedo;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls,ufrmWeaponType, uT3MountedActiveAcousticTorp,
  Menus;

type
  TfrmAcousticTorpedo = class(TfrmWeaponType)
    grpAcousticTorpedo: TGroupBox;
    scrlbx1: TScrollBox;
    lblAcousticTorpedoStatus: TLabel;
    lblAcousticTorpedoQuantity: TLabel;
    lbl4: TLabel;
    lbl37: TLabel;
    lbl36: TLabel;
    lbl35: TLabel;
    lbl34: TLabel;
    lbl33: TLabel;
    lbl32: TLabel;
    lbl31: TLabel;
    lbl30: TLabel;
    lbl29: TLabel;
    lbl28: TLabel;
    lbl27: TLabel;
    lbl26: TLabel;
    lbl25: TLabel;
    lbl24: TLabel;
    lbl23: TLabel;
    lbl2: TLabel;
    lbl16: TLabel;
    lbl11: TLabel;
    lbl10: TLabel;
    bvl8: TBevel;
    bvl7: TBevel;
    bvl6: TBevel;
    bvl5: TBevel;
    btnSearchTarget: TSpeedButton;
    btnRunOutAT: TSpeedButton;
    btnGyroAngleAT: TSpeedButton;
    btnFiringModeAT: TSpeedButton;
    btnDisplayBlindZonesShow: TSpeedButton;
    btnDisplayBlindZonesHide: TSpeedButton;
    btnAccousticDisplayRangeShow: TSpeedButton;
    btnAccousticDisplayRangeHide: TSpeedButton;
    btnTargetDetails: TButton;
    edtEdtSeekerRangeAT: TEdit;
    edtEdtSearchRadiusAT: TEdit;
    edtEdtSearchDepthAT: TEdit;
    edtEdtSafetyCeilingAT: TEdit;
    edtEdtRunOutAT: TEdit;
    edtEdtGyroAngleAT: TEdit;
    edtEdtFiringModeAT: TEdit;
    edtEdtATTargetTrack: TEdit;
    btnTube4AT: TButton;
    btnTube3AT: TButton;
    btnTube2AT: TButton;
    btnTube1AT: TButton;
    btntControlGyroAdvised: TButton;
    btnPlanAT: TButton;
    btnLaunchAT: TButton;
    btnControlSeeker: TButton;
    btnControlSearchRadius: TButton;
    btnControlSearchDepth: TButton;
    btnControlSafety: TButton;
    btnControlControlRunAdvised: TButton;
    btnCancelAT: TButton;
    pmTorpedoFiring: TPopupMenu;
    pmTorpedoRunOut: TPopupMenu;
    pmTorpedoGyroAngle: TPopupMenu;
    procedure AcousticTubeOnClick(Sender: TObject);
    procedure btnFiringModeATClick(Sender: TObject);
    procedure btnRunOutATClick(Sender: TObject);
    procedure btnGyroAngleATClick(Sender: TObject);
    procedure OnBtnTorpedoClick(Sender: TObject);
  private
    { Private declarations }
      procedure OnTorpedoFiringClick(Sender : TObject);
      procedure onTorpedoRunOutClick(Sender : TObject);
      procedure onTorpedoGyroAngle(Sender : TObject);
  public
    { Public declarations }
    procedure UpdateForm; override;
    procedure SetControlledWeapon(aObj : TObject); override;
  end;

var
  frmAcousticTorpedo: TfrmAcousticTorpedo;

implementation

{$R *.dfm}

{ TfrmAcousticTorpedo }

procedure TfrmAcousticTorpedo.AcousticTubeOnClick(Sender: TObject);
begin
  inherited;
  with TT3MountedActiveAcousticTorp(FControlledWeapon) do
  begin
//    case TButton(Sender).Tag of
//      11 : SimMgrClient.netSend_CmdTorpedoProperty(ParentPlatformID, 6, 1,
//           InstanceIndex, InstanceName, 1);
//      12 : SimMgrClient.netSend_CmdTorpedoProperty(ParentPlatformID, 6, 1,
//           InstanceIndex, InstanceName, 2);
//      13 : SimMgrClient.netSend_CmdTorpedoProperty(ParentPlatformID, 6, 1,
//           InstanceIndex, InstanceName, 3);
//      14 : SimMgrClient.netSend_CmdTorpedoProperty(ParentPlatformID, 6, 1,
//           InstanceIndex, InstanceName, 4);
//    end;
  end;
end;

procedure TfrmAcousticTorpedo.btnFiringModeATClick(Sender: TObject);
var
  Pos : TPoint;
begin
  inherited;
  GetCursorPos(pos);
  pmTorpedoFiring.Popup(pos.X, pos.Y);
end;

procedure TfrmAcousticTorpedo.btnGyroAngleATClick(Sender: TObject);
var
  Pos : TPoint;
begin
  inherited;
  GetCursorPos(pos);
  pmTorpedoGyroAngle.Popup(pos.X, pos.Y);
end;

procedure TfrmAcousticTorpedo.btnRunOutATClick(Sender: TObject);
var
  Pos : TPoint;
begin
  inherited;
  GetCursorPos(pos);
  pmTorpedoRunOut.Popup(pos.X, pos.Y);
end;



procedure TfrmAcousticTorpedo.OnBtnTorpedoClick(Sender: TObject);
begin
  inherited;

  if Sender is TSpeedButton then
  with TT3MountedActiveAcousticTorp(ControlledWeapon) do begin
    case TSpeedButton(Sender).Tag of
      1 : begin
            //FVisibleShowRange := True;
            ShowRange         := True;
            //ShowRangeSelected := (ShowRange and FVisibleShowRange);
            //HideRangeSensor;
          end;
      2 : begin
            //FVisibleShowRange := False;
            ShowRange         := False;
            //ShowRangeSelected := (ShowRange and FVisibleShowRange);
          end;
      3 : begin
            //FVisibleShowBlind := True;
            ShowBlindZone     := True;
            //ShowBlindSelected := (ShowBlind and FVisibleShowBlind);
            //HideBlindSensor;
          end;
      4 : begin
            //FVisibleShowBlind := False;
            ShowBlindZone     := False;
            //ShowBlindSelected := (ShowBlind and FVisibleShowBlind);
          end;
      5 : begin
//            {Ngecek Track ID self}
//            if Assigned(FControlled) then
//            begin
//              strParentTrackId := TT3PlatformInstance(FControlled).Track_ID;
//              strParentTrackNumber := FormatTrackNumber(TT3PlatformInstance(
//                                      FControlled).TrackNumber);
//              intParentPlatformID := TT3PlatformInstance(FControlled).InstanceIndex;
//              EdtATTargetTrack.Text := '';
//            end
//            else begin
//              frmTacticalDisplay.addStatus('Controlled platform not defined');
//              exit;
//            end;
//
//            {Ngecek Validasi target}
//            if Assigned(focused_platform) then
//            begin
//              if not TorpedoTargetCheck(focused_platform,focused_weapon) then
//              begin
//                frmTacticalDisplay.addStatus('Invalid target domain');
//                exit;
//              end;
//
//              {Kalo yg di track Detected track}
//              if focused_platform is TT3DetectedTrack then
//              begin
//                strTargetTrackId := TT3PlatformInstance(TT3DetectedTrack(
//                                    focused_platform).TrackObject).Track_ID;
//                strTargetCourse := FormatCourse(TT3PlatformInstance(TT3DetectedTrack(
//                                   focused_platform).TrackObject).Course);
//                strTargetGroundSpeed := FormatSpeed(TT3PlatformInstance(TT3DetectedTrack(
//                                        focused_platform).TrackObject).Speed);
//                strTargetAltitude := FormatCourse(TT3PlatformInstance(TT3DetectedTrack(
//                                     focused_platform).TrackObject).Altitude);
//                IntTargetPlatformID := TT3PlatformInstance(TT3DetectedTrack(
//                                       focused_platform).TrackObject).InstanceIndex;
//
//                target := simMgrClient.FindT3PlatformByID(IntTargetPlatformID);
//              end
//              {Kalo yg di track Non Real Time}
//              else if focused_platform is TT3NonRealVehicle then
//              begin
//                strTargetTrackId := IntToStr(TT3PlatformInstance(focused_platform).InstanceIndex);
//                strTargetCourse := FormatCourse(TT3NonRealVehicle(focused_platform).Course);
//                strTargetGroundSpeed := FormatSpeed(TT3NonRealVehicle(focused_platform).Speed);
//                strTargetAltitude := FormatCourse(TT3NonRealVehicle(focused_platform).Altitude);
//                IntTargetPlatformID := TT3PlatformInstance(focused_platform).InstanceIndex;
//
//                target := simMgrClient.FindNonRealPlatformByID(IntTargetPlatformID);
//              end
//              {Kalo yg di track Platform Instance}
//              else
//              begin
//                strTargetTrackId := TT3PlatformInstance(focused_platform).Track_ID;
//                strTargetCourse := FormatCourse(TT3PlatformInstance(focused_platform).Course);
//                strTargetGroundSpeed := FormatSpeed(TT3PlatformInstance(focused_platform).Speed);
//                strTargetAltitude := FormatCourse(TT3PlatformInstance(focused_platform).Altitude);
//                IntTargetPlatformID := TT3PlatformInstance(focused_platform).InstanceIndex;
//
//                target := simMgrClient.FindT3PlatformByID(IntTargetPlatformID);
//              end;
//            end
//            else begin
//              frmTacticalDisplay.addStatus('Target platform not defined');
//              exit;
//            end;
//
//            if not Assigned(target) then exit;
//
//            if Assigned(simMgrClient.findDetectedTrack(target)) then
//               strTargetTrackId := FormatTrackNumber(TT3DetectedTrack(simMgrClient.
//                   findDetectedTrack(target)).TrackNumber)
//            else begin
//              if TT3PlatformInstance(target).TrackNumber = 0 then
//                strTargetTrackId := strTargetTrackId
//              else
//                strTargetTrackId := FormatTrackNumber(TT3PlatformInstance(target).TrackNumber);
//            end;
//
//            if simMgrClient.ISInstructor then
//              intNoCubicle  := 0
//            else
//              intNoCubicle  := simMgrClient.FMyCubGroup.FData.Group_Index;
//
//            if (strParentTrackId <> strTargetTrackId)
//                and (strParentTrackNumber <> strTargetTrackId) then
//            begin
//              TargetObject             := focused_platform;
//
//              {Paket Synch Panel Weapon Ketika Track Target}
//              rPanel.NoCubicle := intNoCubicle;
//              rPanel.ParentPlatformID := intParentPlatformID;
//              rPanel.TargetPlatformID := IntTargetPlatformID;
//              rPanel.TorpType := 6;
//              rPanel.WeaponIndex := InstanceIndex;
//              rPanel.WeaponName := InstanceName;
//              rPanel.TargetTrack := strTargetTrackId;
//              rPanel.Param1 := '---';
//              rPanel.Param2 := strTargetCourse;
//              rPanel.Param3 := strTargetGroundSpeed;
//              rPanel.Param4 := strTargetAltitude;
//              rPanel.FiringMode := 1;
//              rPanel.RunOut := 1;
//
//              rPanel.SearchRadius := Round(TorpedoDefinition.FDef.Terminal_Circle_Radius
//                                     * C_NauticalMile_To_Yards);
//              rPanel.SearchDepth := Round(TorpedoDefinition.FDef.Default_Depth
//                                    * C_Feet_To_Meter);
//              rPanel.SafetyCeiling := Round(TorpedoDefinition.FDef.Acoustic_Torp_Ceiling_Depth
//                                      * C_Feet_To_Meter);
//              rPanel.SeekerRange := Round(TorpedoDefinition.FDef.Seeker_TurnOn_Range
//                                   * C_NauticalMile_To_Yards);
//
//              rPanel.GyroAngle := 0;
//              rPanel.ButtonPlan := True;
//              rPanel.ButtonLaunch := False;
//
//              SimMgrClient.netSend_CmdTorpedoSyncPanelWeapon(rPanel);
//              Sleep(100);
//              {----------------------------------------------------------------}
//            end
//            else
//            begin
//              frmTacticalDisplay.addStatus('Cannot target own platform');
//              exit;
//            end;

          end;
    end;
  end;

  if Sender is TButton then
  begin
    with TT3MountedActiveAcousticTorp(FControlledWeapon) do
    begin
//      if (edtEdtATTargetTrack.Text = '') or (edtEdtATTargetTrack.Text = '---')then
//        Exit;
//
//      focusTarget := simMgrClient.FindT3PlatformByID(TT3TorpedoesOnVehicle
//                    (focused_weapon).TargetPlatformID);
//      if not Assigned(focusTarget) then
//      begin
//        focusTarget := simMgrClient.FindNonRealPlatformByID(TT3TorpedoesOnVehicle
//                       (focused_weapon).TargetPlatformID);
//        strTargetAltitude := FloatToStr(TT3NonRealVehicle(focusTarget).Altitude);
//      end
//      else
//      begin
//        strTargetAltitude := FloatToStr(TT3PlatformInstance(focusTarget).Altitude);
//      end;

      {$REGION 'Konstanta Acoustic'}
      {(1)Tube, (2)Firing Mode, (3)RunOut Mode, (4)SearchRadius, (5)SearchDepth,
      (6)Safety Ceiling, (7)SeekerRange, (8)GyroAngle }
      {$ENDREGION}
//      case TButton(Sender).Tag of
//        1 : begin
//              if TorpedoDefinition.FDef.Sinuation_Runout = 1 then
//                SimMgrClient.netSend_CmdTorpedoProperty(ParentPlatformID, 6, 3,
//                     InstanceIndex, InstanceName, 1)
//              else
//                SimMgrClient.netSend_CmdTorpedoProperty(ParentPlatformID, 6, 3,
//                     InstanceIndex, InstanceName, 0);
//            end;
//        2 : begin
//              intTemp := Round(TorpedoDefinition.FDef.Terminal_Circle_Radius
//                         * C_NauticalMile_To_Yards);
//              SimMgrClient.netSend_CmdTorpedoProperty(ParentPlatformID, 6, 4,
//                         InstanceIndex, InstanceName, intTemp);
//            end;
//        3 : begin
//              intTemp := StrToInt(strTargetAltitude);
//              SimMgrClient.netSend_CmdTorpedoProperty(ParentPlatformID, 6, 5,
//                         InstanceIndex, InstanceName, intTemp);
//            end;
//        4 : begin
//              intTemp := Round(TorpedoDefinition.FDef.Acoustic_Torp_Ceiling_Depth
//                         * C_Feet_To_Meter);
//              SimMgrClient.netSend_CmdTorpedoProperty(ParentPlatformID, 6, 6,
//                         InstanceIndex, InstanceName, intTemp);
//            end;
//        5 : begin
//              intTemp := Round(TorpedoDefinition.FDef.Seeker_TurnOn_Range
//                         * C_NauticalMile_To_Yards);
//              SimMgrClient.netSend_CmdTorpedoProperty(ParentPlatformID, 6, 7,
//                         InstanceIndex, InstanceName, intTemp);
//            end;
//        6 : begin
//              intTemp := 0;
//              SimMgrClient.netSend_CmdTorpedoProperty(ParentPlatformID, 6, 8,
//                         InstanceIndex, InstanceName, intTemp);
//            end;
//        7 : begin
//              if btnPlanAT.Enabled = True then
//              begin
//                fPlanDetail := TPlanDetail.Create(self);
//
//                with fPlanDetail do
//                begin
//                  lblNeme.Caption := InstanceName;
//                  lblTrack.Caption:= TargetTrack;
//                  lblCourse.Caption:= TargetCourse;
//                  lblSpeed.Caption := TargetGroundSpeed;
//                  lblDepth.Caption := TargetAltitude;
//                end;
//
//                fPlanDetail.show;
//              end;
//            end;
//        8 : begin
//              intTemp := 1;
//              SimMgrClient.netSend_CmdTorpedoProperty(ParentPlatformID, 6, 9,
//                         InstanceIndex, InstanceName, intTemp);
//            end;
//        9 : begin
//              {Paket Synch Panel Weapon Setelah Ditembakkan}
//              rPanel.NoCubicle := 0;
//              rPanel.ParentPlatformID := ParentPlatformID;
//              rPanel.TargetPlatformID := 0;
//              rPanel.TorpType := 6;
//              rPanel.WeaponIndex := InstanceIndex;
//              rPanel.WeaponName := InstanceName;
//              rPanel.TargetTrack := '---';
//              rPanel.Param1 := '---';
//              rPanel.Param2 := '---';
//              rPanel.Param3 := '---';
//              rPanel.Param4 := '---';
//              rPanel.FiringMode := 1;
//              rPanel.RunOut := 1;
//              rPanel.SearchRadius := 0;
//              rPanel.SearchDepth := 0;
//              rPanel.SafetyCeiling := 0;
//              rPanel.SeekerRange := 0;
//              rPanel.GyroAngle := 0;
//              rPanel.ButtonPlan := False;
//              rPanel.ButtonLaunch := False;
//
//              SimMgrClient.netSend_CmdTorpedoSyncPanelWeapon(rPanel);
//              Sleep(100);
//              {----------------------------------------------------------------}
//            end;
//        10: begin
//               if (focusTarget = nil) or (EdtATTargetTrack.Text = '')then
//               begin
//                  frmTacticalDisplay.addStatus('Target is not selected!');
//                  exit;
//               end;
//
//               EdtATTargetTrack.Text := '';
//               intbatasAT := Quantity;
//
//               if (TorpedoPrelaunchCheck(focusTarget,focused_weapon, 6))
//                  and (intbatasAT > 0) then
//               begin
//                  if (EdtSearchRadiusAT.Text = '') or (EdtSearchDepthAT.Text = '') or
//                     (EdtSafetyCeilingAT.Text = '') or (EdtSeekerRangeAT.Text = '') then
//                    ShowMessage('Data Torpedo belum diisi')
//                  else
//                  begin
//                    {Paket Launch Torpedo}
//                    rMis.ParentPlatformID := ParentPlatformID;
//                    rMis.TargetPlatformID := TargetPlatformID;
//                    rMis.ProjectileInstanceIndex := 0;
//                    rMis.NoCubicle := NoCubicle;
//                    rMis.MissileID := InstanceIndex;
//
//                    SimMgrClient.netSend_CmdLaunch_Torpedo(rMis);
//                    Sleep(100);
//                    {------------------------------------------------------------------}
//
//                    {Paket Synch Panel Weapon Setelah Ditembakkan}
//                    rPanel.NoCubicle := 0;
//                    rPanel.ParentPlatformID := ParentPlatformID;
//                    rPanel.TargetPlatformID := 0;
//                    rPanel.TorpType := 6;
//                    rPanel.WeaponIndex := InstanceIndex;
//                    rPanel.WeaponName := InstanceName;
//                    rPanel.TargetTrack := '---';
//                    rPanel.Param1 := '---';
//                    rPanel.Param2 := '---';
//                    rPanel.Param3 := '---';
//                    rPanel.Param4 := '---';
//                    rPanel.FiringMode := 1;
//                    rPanel.RunOut := 1;
//                    rPanel.SearchRadius := 0;
//                    rPanel.SearchDepth := 0;
//                    rPanel.SafetyCeiling := 0;
//                    rPanel.SeekerRange := 0;
//                    rPanel.GyroAngle := 0;
//                    rPanel.ButtonPlan := False;
//                    rPanel.ButtonLaunch := False;
//
//                    SimMgrClient.netSend_CmdTorpedoSyncPanelWeapon(rPanel);
//                    Sleep(100);
//                    {----------------------------------------------------------------}
//
//                    SimMgrClient.netSend_CmdSetQuantity(TT3PlatformInstance(
//                       FControlled).InstanceIndex,
//                    InstanceIndex, CORD_ID_QUANTITY, CORD_TYPE_WEAPON, (Quantity - 1));
//
//                    lblAcousticTorpedoQuantity.Caption  := IntToStr(Quantity-1);
//                    btnLaunchAT.Enabled    := false;
//                  end;
//               end
//               else
//               begin
//                 if intbatasAT <= 0 then
//                  ShowMessage('Cek Quantity');
//               end;
//            end
//      end;
    end;
  end;
end;

procedure TfrmAcousticTorpedo.OnTorpedoFiringClick(Sender: TObject);
begin
  with TT3MountedActiveAcousticTorp(ControlledWeapon) do
  begin
//    if TMenuItem(sender).Caption = '&Deliberate' then
//      SimMgrClient.netSend_CmdTorpedoProperty(TT3PlatformInstance(
//            FControlled).InstanceIndex, 6, 2, InstanceIndex, InstanceName, 1)
//    else
//      SimMgrClient.netSend_CmdTorpedoProperty(TT3PlatformInstance(FControlled).
//            InstanceIndex, 6, 2, InstanceIndex, InstanceName, 0);
  end

end;

procedure TfrmAcousticTorpedo.onTorpedoGyroAngle(Sender: TObject);
begin
  with TT3MountedActiveAcousticTorp(ControlledWeapon) do
  begin
//    if TMenuItem(sender).Caption = '&0' then
//      SimMgrClient.netSend_CmdTorpedoProperty(TT3PlatformInstance(FControlled).
//          InstanceIndex, 6, 8, InstanceIndex, InstanceName, 0)
//    else if TMenuItem(sender).Caption = '&45 pt'  then
//      SimMgrClient.netSend_CmdTorpedoProperty(TT3PlatformInstance(FControlled).
//         InstanceIndex, InstanceIndex, 6, 8, InstanceName, 315)
//    else if TMenuItem(sender).Caption = '&60 pt'  then
//      SimMgrClient.netSend_CmdTorpedoProperty(TT3PlatformInstance(FControlled).
//         InstanceIndex, InstanceIndex, 6, 8, InstanceName, 300)
//    else if TMenuItem(sender).Caption = '4&5 sb'  then
//      SimMgrClient.netSend_CmdTorpedoProperty(TT3PlatformInstance(FControlled).
//         InstanceIndex, InstanceIndex, 6, 8, InstanceName, 45)
//    else if TMenuItem(sender).Caption = '60 &sb'  then
//      SimMgrClient.netSend_CmdTorpedoProperty(TT3PlatformInstance(FControlled).
//         InstanceIndex, InstanceIndex, 6, 8, InstanceName, 60)
  end;

end;

procedure TfrmAcousticTorpedo.onTorpedoRunOutClick(Sender: TObject);
begin
  with TT3MountedActiveAcousticTorp(ControlledWeapon) do
  begin
//    if TMenuItem(sender).Caption = '&RunOut' then
//      SimMgrClient.netSend_CmdTorpedoProperty(TT3PlatformInstance(FControlled).
//          InstanceIndex, 6, 3, InstanceIndex, InstanceName, 1)
//    else
//      SimMgrClient.netSend_CmdTorpedoProperty(TT3PlatformInstance(FControlled).
//        InstanceIndex, 6, 3, InstanceIndex, InstanceName, 0);
  end;

end;

procedure TfrmAcousticTorpedo.SetControlledWeapon(aObj: TObject);
var
  menuItem    : TMenuItem;
  i, j, k     : Integer;
begin
  inherited;

  pmTorpedoFiring.Items.Clear;
  pmTorpedoRunOut.Items.Clear;
  pmTorpedoGyroAngle.Items.Clear;

  for i := 0 to 1 do
  begin
    menuItem := TMenuItem.Create(nil);

    if i = 0 then
      menuItem.Caption :='Deliberate' else
    if i = 1 then
      menuItem.Caption :='Urgent';

    menuItem.Tag     := i;
    menuItem.OnClick := onTorpedoFiringClick;
    pmTorpedoFiring.Items.Add(menuItem);
  end;

  for j := 0 to 1 do
  begin
    menuItem := TMenuItem.Create(nil);

    if j = 0 then
      menuItem.Caption :='RunOut' else
    if j = 1 then
      menuItem.Caption :='No RunOut';

    menuItem.Tag     := j;
    menuItem.OnClick := onTorpedoRunOutClick;
    pmTorpedoRunOut.Items.Add(menuItem);
  end;

  for k := 0 to 4 do
  begin
    menuItem := TMenuItem.Create(nil);

    if k = 0 then
      menuItem.Caption :='0' else
    if k = 1 then
      menuItem.Caption :='45 pt';
    if k = 2 then
      menuItem.Caption :='60 pt';
    if k = 3 then
      menuItem.Caption :='45 sb';
    if k = 4 then
      menuItem.Caption :='60 sb';

    menuItem.Tag     := k;
    menuItem.OnClick := onTorpedoGyroAngle;
    pmTorpedoGyroAngle.Items.Add(menuItem);
  end;

  if Assigned(ControlledWeapon) then
  with TT3MountedActiveAcousticTorp(ControlledWeapon) do
  begin
    btnDisplayBlindZonesShow.Enabled := BlindZones.Count > 0;
    btnDisplayBlindZonesHide.Enabled := BlindZones.Count > 0;

    if ShowBlindZone then
      btnDisplayBlindZonesShow.Down := True
    else
      btnDisplayBlindZonesHide.Down :=True;

    if ShowRange then
      btnAccousticDisplayRangeShow.Down := True
    else
      btnAccousticDisplayRangeHide.Down := True;

    if FiringMode = 1 then
      edtEdtFiringModeAT.Text := 'Deliberate'
    else
      edtEdtFiringModeAT.Text := 'Urgent';

    if RunOutMode = 1 then
      edtEdtRunOutAT.Text := 'RunOut'
    else
      edtEdtRunOutAT.Text := 'No RunOut';

    edtEdtSearchRadiusAT.Text   := FloatToStr(SearchRadius);
    edtEdtSearchDepthAT.Text    := FloatToStr(SearchDepth);
    edtEdtSafetyCeilingAT.Text  := FloatToStr(SafetyCeiling);
    edtEdtSeekerRangeAT.Text    := FloatToStr(SeekerRange);
    edtEdtGyroAngleAT.Text      := IntToStr(GyroAngle);

    btnTube1AT.Enabled := True;
    btnTube2AT.Enabled := True;
    btnTube3AT.Enabled := True;
    btnTube4AT.Enabled := True;

    case TubeOn of
      1 : btnTube1AT.Enabled := False;
      2 : btnTube2AT.Enabled := False;
      3 : btnTube3AT.Enabled := False;
      4 : btnTube4AT.Enabled := False;
    end;

  end;

end;

procedure TfrmAcousticTorpedo.UpdateForm;
begin
  inherited;

  if Assigned(ControlledWeapon) then
  with TT3MountedActiveAcousticTorp(ControlledWeapon) do
  begin
    lblAcousticTorpedoQuantity.Caption := IntToStr(Quantity);

    btnPlanAT.Enabled   := ButtonPlan;
    btnLaunchAT.Enabled := ButtonLaunch;
  end;
end;

end.
