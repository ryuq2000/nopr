object frmAirDroppedTorpedo: TfrmAirDroppedTorpedo
  Left = 0
  Top = 0
  Caption = 'frmAirDroppedTorpedo'
  ClientHeight = 440
  ClientWidth = 305
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object grpAirDroppedTorpedo: TGroupBox
    Left = 0
    Top = 0
    Width = 305
    Height = 440
    Align = alClient
    TabOrder = 0
    object scrlbx1: TScrollBox
      Left = 2
      Top = 15
      Width = 301
      Height = 423
      Align = alClient
      BevelOuter = bvNone
      BorderStyle = bsNone
      TabOrder = 0
      object btnAirDropeedBilndZonesHide: TSpeedButton
        Tag = 4
        Left = 136
        Top = 350
        Width = 80
        Height = 25
        AllowAllUp = True
        GroupIndex = 1
        Down = True
        Caption = 'Hide'
      end
      object btnAirDroppesDisplayBilndZonesShow: TSpeedButton
        Tag = 3
        Left = 136
        Top = 327
        Width = 80
        Height = 25
        AllowAllUp = True
        GroupIndex = 1
        Caption = 'Show'
      end
      object btnAirDroppesDisplayRangeHide: TSpeedButton
        Tag = 2
        Left = 24
        Top = 350
        Width = 80
        Height = 25
        AllowAllUp = True
        GroupIndex = 2
        Down = True
        Caption = 'Hide'
      end
      object btnAirDroppesDisplayRangeShow: TSpeedButton
        Tag = 1
        Left = 24
        Top = 327
        Width = 80
        Height = 25
        AllowAllUp = True
        GroupIndex = 2
        Caption = 'Show'
      end
      object btnAirDroppesTargetTrack: TSpeedButton
        Tag = 5
        Left = 173
        Top = 130
        Width = 23
        Height = 22
        Glyph.Data = {
          36090000424D360900000000000036000000280000001F000000180000000100
          18000000000000090000000000000000000000000000000000006161613E3E3E
          3737374040403B3B3B3A3A3A4141414141413A3A3A3A3A3A4040403737373E3E
          3E3D3D3D3838384141413A3A3A3B3B3B4040403737373E3E3E3D3D3D38383841
          41413A3A3A3B3B3B4040403737373E3E3E3C3C3C383838000000B2B2B27C7C7C
          7474747F7F7F7878787777778080808080807878787878788080807474747C7C
          7C7B7B7B7474748080807A7A7A7A7A7A7F7F7F7474747C7C7C7B7B7B74747480
          80807777777979797F7F7F7474747D7D7D7A7A7A757575000000D0D0D0B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2BDBDBDBBBBBBB2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000D0D0D0B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2C0C0C04949
          491111111111111111111212121212121A1A1AB2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B4B4B4000000D0D0D0B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B29191914D4D4D5454545F5F
          5F6565656262620000004747476565656363634D4D4D4D4D4D797979BABABAB2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B3B3B3000000D0D0D0B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B29797976969692929292929298686
          86B2B2B2ADADAD0000007D7D7DB2B2B2ABABAB292929292929545454979797B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000D0D0D0B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B23D3D3D3B3B3BB2B2B2B2B2B2B2B2
          B2B2B2B2ADADAD0000007D7D7DB2B2B2B2B2B2B2B2B2B2B2B26A6A6A0E0E0EB2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000D0D0D0B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B21616166F6F6FA5A5A5B2B2B2B2B2B2B2B2
          B2B2B2B2ADADAD0000007D7D7DB2B2B2B2B2B2B2B2B2B2B2B2AAAAAA94949413
          13138A8A8AB2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000D0D0D0B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2030303757575B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2ADADAD0000007D7D7DB2B2B2B2B2B2B2B2B2B2B2B2B2B2B2A4A4A400
          0000868686B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B3B3B3000000D0D0D0B2B2B2
          B2B2B2B2B2B2B2B2B2A6A6A68B8B8B2C2C2C838383B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2ADADAD0000007F7F7FB4B4B4B2B2B2B2B2B2B2B2B2B2B2B2A7A7A727
          27277272729C9C9CB2B2B2B2B2B2B2B2B2B2B2B2B3B3B3000000D0D0D0B2B2B2
          B2B2B2B2B2B2B2B2B27B7B7B000000BDBDBDB7B7B7B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2ADADAD000000888888BBBBBBB2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B22C2C2C4C4C4CB2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000D0D0D0B2B2B2
          B2B2B2B2B2B2B2B2B27B7B7B000000B0B0B0B3B3B3B2B2B2B2B2B2BFBFBFB6B6
          B6B2B2B2B1B1B19D9D9DADADADB3B3B3B2B2B2B2B2B2B2B2B2B2B2B2B3B3B3BF
          BFBF3030304C4C4CB2B2B2B2B2B2B2B2B2B2B2B2B4B4B4000000D0D0D0B2B2B2
          B2B2B2B2B2B2B2B2B27B7B7B0000004F4F4F5151515151515151515858585353
          53737373B2B2B2B2B2B2B2B2B28D8D8D51515151515151515151515152525258
          58581616164C4C4CB2B2B2B2B2B2B2B2B2B2B2B2B3B3B3000000D0D0D0B2B2B2
          B2B2B2B2B2B2B2B2B27B7B7B0000002424242525252525252525252525252525
          25575757B2B2B2B2B2B2B2B2B27D7D7D25252525252525252525252525252525
          25250909094C4C4CB2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000D0D0D0B2B2B2
          B2B2B2B2B2B2B2B2B27B7B7B000000AFAFAFB2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B22C2C2C4C4C4CB2B2B2B2B2B2B2B2B2B2B2B2B4B4B4000000D0D0D0B2B2B2
          B2B2B2B2B2B2B2B2B27B7B7B000000BBBBBBB6B6B6B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2ADADAD171717848484B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B22C2C2C4C4C4CB2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000D0D0D0B2B2B2
          B2B2B2B2B2B2B2B2B29999995F5F5F5A5A5A949494B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2ADADAD0000007D7D7DB2B2B2B2B2B2B2B2B2B2B2B2B5B5B5B2B2B253
          53535C5C5C828282B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000D0D0D0B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2030303757575B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2ADADAD0000007D7D7DB2B2B2B2B2B2B2B2B2B2B2B2B7B7B7AFAFAF00
          0000868686B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000D0D0D0B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2030303757575B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2ADADAD0000007D7D7DB2B2B2B2B2B2B2B2B2B2B2B2B2B2B2A4A4A400
          0000868686B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000D0D0D0B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B29A9A9A4545454B4B4BB2B2B2B2B2B2B2B2
          B2B2B2B2ADADAD0000007D7D7DB2B2B2B2B2B2B2B2B2B2B2B274747423232399
          9999ACACACB2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B4B4B4000000D0D0D0B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B27A7A7A5A5A5A5555555555559494
          94B5B5B5B4B4B4000000838383B7B7B7ADADAD555555555555585858646464B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B3B3B3000000D0D0D0B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B28282822121212121216D6D
          6D9595959898980000006E6E6E9898988B8B8B2121212121215B5B5BB2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000D0D0D0B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B23939
          39000000000000000000000000000000090909B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B4B4B4000000D0D0D0B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2A0A0
          A0979797979797979797979797979797999999B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000}
      end
      object bvl10: TBevel
        Left = 39
        Top = 305
        Width = 240
        Height = 3
      end
      object bvl11: TBevel
        Left = 40
        Top = 6
        Width = 240
        Height = 3
      end
      object bvl12: TBevel
        Left = 41
        Top = 37
        Width = 239
        Height = 3
      end
      object bvl13: TBevel
        Left = 82
        Top = 245
        Width = 198
        Height = 3
      end
      object bvl9: TBevel
        Left = 41
        Top = 125
        Width = 239
        Height = 3
      end
      object lbl38: TLabel
        Left = 3
        Top = -2
        Width = 31
        Height = 13
        Caption = 'Status'
      end
      object lbl39: TLabel
        Left = 130
        Top = 16
        Width = 49
        Height = 13
        Caption = 'Quantity :'
      end
      object lbl40: TLabel
        Left = 3
        Top = 117
        Width = 32
        Height = 13
        Caption = 'Target'
      end
      object lbl41: TLabel
        Left = 22
        Top = 135
        Width = 33
        Height = 13
        Caption = 'Track :'
      end
      object lbl42: TLabel
        Left = 3
        Top = 296
        Width = 34
        Height = 13
        Caption = 'Display'
      end
      object lbl43: TLabel
        Left = 45
        Top = 310
        Width = 31
        Height = 13
        Caption = 'Range'
      end
      object lbl44: TLabel
        Left = 148
        Top = 310
        Width = 54
        Height = 13
        Caption = 'Blind Zones'
      end
      object lbl45: TLabel
        Left = 22
        Top = 155
        Width = 34
        Height = 13
        Caption = 'Force :'
      end
      object lbl46: TLabel
        Left = 3
        Top = 30
        Width = 35
        Height = 13
        Caption = 'Control'
      end
      object lbl47: TLabel
        Left = 22
        Top = 50
        Width = 72
        Height = 13
        Caption = 'Search radius :'
      end
      object lbl48: TLabel
        Left = 22
        Top = 74
        Width = 71
        Height = 13
        Caption = 'Search depth :'
      end
      object lbl49: TLabel
        Left = 22
        Top = 98
        Width = 71
        Height = 13
        Caption = 'Safety ceiling :'
      end
      object lbl50: TLabel
        Left = 181
        Top = 50
        Width = 27
        Height = 13
        Caption = 'yards'
      end
      object lbl51: TLabel
        Left = 181
        Top = 74
        Width = 33
        Height = 13
        Caption = 'metres'
      end
      object lbl52: TLabel
        Left = 181
        Top = 98
        Width = 33
        Height = 13
        Caption = 'metres'
      end
      object lbl53: TLabel
        Left = 22
        Top = 174
        Width = 41
        Height = 13
        Caption = 'Course :'
      end
      object lbl54: TLabel
        Left = 22
        Top = 193
        Width = 75
        Height = 13
        Caption = 'Ground Speed :'
      end
      object lbl55: TLabel
        Left = 22
        Top = 213
        Width = 36
        Height = 13
        Caption = 'Depth :'
      end
      object lbl56: TLabel
        Left = 172
        Top = 174
        Width = 48
        Height = 13
        Caption = 'degrees T'
      end
      object lbl57: TLabel
        Left = 172
        Top = 193
        Width = 26
        Height = 13
        Caption = 'knots'
      end
      object lbl58: TLabel
        Left = 172
        Top = 213
        Width = 33
        Height = 13
        Caption = 'metres'
      end
      object lbl59: TLabel
        Left = 3
        Top = 236
        Width = 73
        Height = 13
        Caption = 'Launch Bearing'
      end
      object lbl60: TLabel
        Left = 24
        Top = 279
        Width = 43
        Height = 13
        Caption = 'Bearing :'
      end
      object lbl61: TLabel
        Left = 181
        Top = 279
        Width = 48
        Height = 13
        Caption = 'degrees T'
      end
      object lbl85: TLabel
        Left = 217
        Top = 152
        Width = 69
        Height = 13
        Caption = 'without target'
      end
      object lblADQuantity: TLabel
        Left = 195
        Top = 16
        Width = 12
        Height = 13
        Caption = '---'
      end
      object lblAirDproppedTargetCourse: TLabel
        Left = 115
        Top = 174
        Width = 12
        Height = 13
        Caption = '---'
      end
      object lblAirDproppedTargetDepth: TLabel
        Left = 115
        Top = 213
        Width = 12
        Height = 13
        Caption = '---'
      end
      object lblAirDproppedTargetForce: TLabel
        Left = 115
        Top = 155
        Width = 12
        Height = 13
        Caption = '---'
      end
      object lblAirDproppedTargetGroundSpeed: TLabel
        Left = 115
        Top = 193
        Width = 12
        Height = 13
        Caption = '---'
      end
      object lblAirDroppedStatus: TLabel
        Left = 22
        Top = 16
        Width = 43
        Height = 13
        Caption = 'Available'
      end
      object btnDefaultAirDroppedControlSearchCeiling: TButton
        Tag = 2
        Left = 217
        Top = 92
        Width = 65
        Height = 23
        Caption = '< Default'
        TabOrder = 0
      end
      object btnDefaultAirDroppedSearchDepth: TButton
        Tag = 1
        Left = 217
        Top = 66
        Width = 65
        Height = 23
        Caption = '< Default'
        TabOrder = 1
      end
      object btnLauchAD: TButton
        Tag = 3
        Left = 200
        Top = 381
        Width = 82
        Height = 26
        Caption = 'Drop'
        TabOrder = 2
      end
      object chkAirDroppedLaunchWhithoutTarget: TCheckBox
        Left = 199
        Top = 133
        Width = 81
        Height = 17
        Caption = 'Launch'
        TabOrder = 3
      end
      object chkAirDroppedUseLaunchPlatformHeading: TCheckBox
        Left = 24
        Top = 255
        Width = 166
        Height = 17
        Caption = 'Use launch platform heading'
        TabOrder = 4
      end
      object edtEdtADTargetTrack: TEdit
        Left = 115
        Top = 131
        Width = 53
        Height = 21
        Enabled = False
        ReadOnly = True
        TabOrder = 5
      end
      object edtEdtLaunchBearingAD: TEdit
        Tag = 4
        Left = 115
        Top = 274
        Width = 60
        Height = 21
        TabOrder = 6
      end
      object edtEdtSearchCeilingAD: TEdit
        Tag = 3
        Left = 115
        Top = 94
        Width = 60
        Height = 21
        TabOrder = 7
      end
      object edtEdtSearchDepthAD: TEdit
        Tag = 2
        Left = 115
        Top = 70
        Width = 60
        Height = 21
        TabOrder = 8
      end
      object edtEdtSearchRadiusAD: TEdit
        Tag = 1
        Left = 115
        Top = 46
        Width = 60
        Height = 21
        TabOrder = 9
      end
    end
  end
end
