unit ufrmSurfaceToSurfaceMissile;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, VrControls, VrBlinkLed, ExtCtrls, StdCtrls, Buttons, ufrmWeaponType,
  Menus, uT3mountedWeapon, uT3MountedSurfaceToSurfaceMissile, uGameData_TTT;

type
  TfrmSurfaceToSurfaceMissile = class(TfrmWeaponType)
    grpSurfaceToSurfaceMissile: TGroupBox;
    scrlbx1: TScrollBox;
    lbl1: TLabel;
    bvl1: TBevel;
    lblSurfaceToSurfaceMissileStatus: TLabel;
    lbl2: TLabel;
    lblSurfaceToSurfaceMissileQuantity: TLabel;
    lbl3: TLabel;
    bvl2: TBevel;
    lbl4: TLabel;
    lbl5: TLabel;
    bvl3: TBevel;
    lbl6: TLabel;
    bvl4: TBevel;
    lbl7: TLabel;
    lbl8: TLabel;
    lbl9: TLabel;
    btnSurfaceToSurfaceMissileFiring: TSpeedButton;
    btnSurfaceToSurfaceMissileEngagement: TSpeedButton;
    btnSurfaceToSurfaceMissileTargetTrack: TSpeedButton;
    btnSurfaceToSurfaceMissileDisplayRangeShow: TSpeedButton;
    btnSurfaceToSurfaceMissileDisplayRangeHide: TSpeedButton;
    lblDestruckRange: TLabel;
    lblCrossOverRange: TLabel;
    lblNmCrossOverRange: TLabel;
    lblNmDestruckRange: TLabel;
    edtSurfaceToSurfaceMissileTargetTrack: TEdit;
    btnSurfaceToSurfacePlan: TButton;
    btnSurfaceToSurfaceLaunch: TButton;
    btnSurfaceToSurfaceCancel: TButton;
    edtSurfaceToSurfaceMissileFiring: TEdit;
    edtSurfaceToSurfaceMissileEngangement: TEdit;
    btnSurfaceToSurfaceMissileTargetTrackDetails: TButton;
    pnlSurfaceToSurfaceWp: TPanel;
    bvl5: TBevel;
    lbl10: TLabel;
    btnSurfaceToSurfaceMissileWaypointsEdit: TButton;
    btnSurfaceToSurfaceMissileWaypointsAdd: TButton;
    btnSurfaceToSurfaceMissileWaypointsDelete: TButton;
    btnSurfaceToSurfaceMissileWaypointsApply: TButton;
    btnSurfaceToSurfaceMissileWaypointsCancel: TButton;
    btnSurfaceToSurfaceMissileLauncherMore: TButton;
    pnlLaunch1: TPanel;
    vbl1: TVrBlinkLed;
    pnlLaunch2: TPanel;
    vbl2: TVrBlinkLed;
    pnlLaunch3: TPanel;
    vbl3: TVrBlinkLed;
    pnlLaunch4: TPanel;
    vbl4: TVrBlinkLed;
    pnlLaunch5: TPanel;
    vbl5: TVrBlinkLed;
    pnlLaunch6: TPanel;
    vbl6: TVrBlinkLed;
    pnlLaunch7: TPanel;
    vbl7: TVrBlinkLed;
    pnlLaunch8: TPanel;
    vbl8: TVrBlinkLed;
    edtDestructRange: TEdit;
    edtCrossOverRange: TEdit;
    pmenuFiring: TPopupMenu;
    RBL1: TMenuItem;
    RBLW1: TMenuItem;
    BOL1: TMenuItem;
    BOLW1: TMenuItem;
    pmenuEngagement: TPopupMenu;
    Ripple1: TMenuItem;
    STOT1: TMenuItem;
    DTOT1: TMenuItem;
    procedure btnSurfaceToSurfaceMissileFiringClick(Sender: TObject);
    procedure btnSurfaceToSurfaceMissileEngagementClick(Sender: TObject);
    procedure btnSurfaceToSurfaceClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btnLaunchClick(Sender: TObject);
    procedure btnSurfaceToSurfaceMissileTargetTrackDetailsClick(
      Sender: TObject);
    procedure OnMenuEngageClick(Sender: TObject);
    procedure OnMenuFiringClick(Sender: TObject);
    procedure edtDestructRangeKeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
    FMountedMissile    : TT3MountedSurfaceToSurfaceMissile;
    function CheckedFunctionality : Boolean;
    procedure TargetTrack;
    procedure ShowRange;
    procedure HideRange;
    procedure Plan;
    procedure Cancel;
    procedure Launch;
    procedure setThisLauncherState;
    procedure ClickThisLauncher(id : integer);
  public
    { Public declarations }
    procedure SetControlledWeapon(aObj : TObject); override;
    procedure UpdateForm; override;
  end;

var
  frmSurfaceToSurfaceMissile: TfrmSurfaceToSurfaceMissile;

implementation

uses
  tttData, uGlobalVar, uT3ClientManager, uT3PlatformInstance,
  uT3Track, uT3PointTrack, uBaseCoordSystem, uDBClassDefinition,
  uToteSurfaceToSurface, ufToteDisplay, uTotePanelManager,
  uSimObjects, ufTrackTargetDetail;

{$R *.dfm}

procedure TfrmSurfaceToSurfaceMissile.btnLaunchClick(Sender: TObject);
var
  pfCtrl : TT3PlatformInstance;
  rec : TRecCmd_LaunchMissile;
begin
  if simManager.GameState <> gsPlaying then
  begin
    UpdatePanelStatus('Game Frozen');
    Exit;
  end;

  if (FMountedMissile.TargetInstanceIdx = 0) or (FMountedMissile.TargetTrackNumber = 0) then
  begin
    UpdatePanelStatus('Target is not valid');
    Exit;
  end;

  pfCtrl := simManager.FindT3PlatformByID(TT3ClientManager(simManager).ControlledTrack.ObjectInstanceIndex);

  if Assigned(pfCtrl) and (pfCtrl.PlatformDomain <> 0) and (not FMountedMissile.Planned) then
  begin
    rec.MissileID   := FMountedMissile.InstanceIndex;
    rec.MissileName := FMountedMissile.InstanceName;
    rec.Order       := CORD_ID_MissileLauncher;

    { jk sama dengan sebelumnya, berarti batal }
    if FMountedMissile.LauncherActive = TPanel(Sender).Tag then
      rec.ValInteger  := 0
    else
      rec.ValInteger  := TPanel(Sender).Tag;

    rec.ParentPlatformID  := clientManager.ControlledTrack.ObjectInstanceIndex;
    clientManager.NetCmdSender.CmdMissile(rec);
  end;
end;

procedure TfrmSurfaceToSurfaceMissile.btnSurfaceToSurfaceClick(Sender: TObject);
begin
  inherited;

  if not CheckedFunctionality then
    Exit;

  if Sender.ClassType = TSpeedButton then
  begin
    case TSpeedButton(Sender).Tag of
      1 : ShowRange;
      2 : HideRange;
      3 : TargetTrack;
    end;
  end
  else
  if Sender.ClassType = TButton then
  begin
    case TButton(Sender).Tag of
      1 : Plan;
      2 : Cancel;
      3 : Launch;
    end;
  end;
end;

procedure TfrmSurfaceToSurfaceMissile.btnSurfaceToSurfaceMissileEngagementClick(
  Sender: TObject);
var pt: TPoint;
begin
  GetCursorPos(pt);

  if pmenuEngagement.Items.Count > 0 then
    pmenuEngagement.Popup(pt.X, pt.Y);
end;

procedure TfrmSurfaceToSurfaceMissile.btnSurfaceToSurfaceMissileFiringClick(
  Sender: TObject);
var pt: TPoint;
begin
  GetCursorPos(pt);

  if pmenuFiring.Items.Count > 0 then
    pmenuFiring.Popup(pt.X, pt.Y);
end;

procedure TfrmSurfaceToSurfaceMissile.btnSurfaceToSurfaceMissileTargetTrackDetailsClick(
  Sender: TObject);
var
  parent : TSimObject;
  target : TSimObject;
  wpn    : TT3MountedWeapon;
begin
  if simManager.GameState <> gsPlaying then
  begin
    UpdatePanelStatus('Game Frozen');
    Exit;
  end;

  if not Assigned(ControlledWeapon) then exit;

  wpn := TT3MountedWeapon(ControlledWeapon);
  parent := wpn.PlatformParent;
  if parent = nil then exit;

  if not (ControlledWeapon is TT3MountedSurfaceToSurfaceMissile) then exit;

  target := simManager.FindT3PlatformByID(FMountedMissile.TargetInstanceIdx);
  if target = nil then exit;

  if not Assigned(frmDetailTrackTarget) then
    frmDetailTrackTarget := TfrmDetailTrackTarget.Create(self);

  frmDetailTrackTarget.SetDetailedTarget(parent,target);
  frmDetailTrackTarget.FormStyle := fsStayOnTop;
  frmDetailTrackTarget.Show;
end;

procedure TfrmSurfaceToSurfaceMissile.Cancel;
begin

end;

function TfrmSurfaceToSurfaceMissile.CheckedFunctionality: Boolean;
begin
  Result := True;
  if ControlledWeapon = nil then
  begin
    UpdatePanelStatus('Not Found Selected Weapon');
    Result := False;
    Exit;
  end;

  if not Assigned(TT3ClientManager(simManager).ControlledTrack) then
  begin
    UpdatePanelStatus('Controlled Vehicle Not Valid');
    Result := False;
    Exit;
  end;

  if not(ControlledWeapon is TT3MountedSurfaceToSurfaceMissile) then
  begin
    UpdatePanelStatus('Selected Weapon Is Not Missile');
    Result := False;
    Exit;
  end;

  if TT3MountedSurfaceToSurfaceMissile(ControlledWeapon).WeaponStatus = wsDamaged then
  begin
    UpdatePanelStatus('Selected Weapon Is Damaged');
    Result := False;
    Exit;
  end;

  if FMountedMissile.TargetInstanceIdx = 0 then
  begin
    UpdatePanelStatus('Target not assigned');
    Result := False;
    Exit;
  end;

  if simManager.GameState <> gsPlaying then
  begin
    UpdatePanelStatus('Game Frozen');
    Result := False;
    Exit;
  end;
end;

procedure TfrmSurfaceToSurfaceMissile.ClickThisLauncher(id: integer);
var
  i : Integer;
  LauncherAngle : Integer;
  launcher : TDBFitted_Weap_Launcher_On_Board;
begin
  LauncherAngle := 0;

//  for I := 0 to TT3MissilesOnVehicle(focused_weapon).MissileDefinition.FLaunchs.Count - 1 do
//  begin
//    launcher := TT3MissilesOnVehicle(focused_weapon).MissileDefinition.FLaunchs.Items[i];
//    if launcher.FData.Launcher_Type > 8 then
//    begin
//      if id = (launcher.FData.Launcher_Type - 8) then
//      begin
//        LauncherAngle := launcher.FData.Launcher_Angle;
//        Break;
//      end;
//    end
//    else
//    begin
//      if id = launcher.FData.Launcher_Type then
//      begin
//        LauncherAngle := launcher.FData.Launcher_Angle;
//        Break;
//      end;
//    end;
//  end;
//
////  setThisLauncherState;
//
//  case id of
//    1 :
//    begin
//      if vbl1.EnableBlinking = True then
//        vbl1.EnableBlinking := False
//      else
//        vbl1.EnableBlinking := True;
//
//      if vbl1.EnableBlinking then
//        begin
//          vbl1.Palette1.Low   := clLime;
//          vbl1.Palette1.High  := clRed;
//
//          FRippleCountArray[0] := True;
//          FRippleDegreeLauncer[0]  := LauncherAngle;
//        end
//      else
//      begin
//        vbl1.Palette1.Low   := clLime;
//        vbl1.Palette1.High  := clLime;
//
//        FRippleCountArray[0] := False;
//        FRippleDegreeLauncer[0]  := LauncherAngle;
//      end;
//    end;
//    2 :
//    begin
//      if vbl2.EnableBlinking = True then
//        vbl2.EnableBlinking := False
//      else
//        vbl2.EnableBlinking := True;
//
//      if vbl2.EnableBlinking then
//        begin
//          vbl2.Palette1.Low   := clLime;
//          vbl2.Palette1.High  := clRed;
//
//          FRippleCountArray[1] := True;
//          FRippleDegreeLauncer[1]  := LauncherAngle;
//        end
//      else
//      begin
//        vbl2.Palette1.Low   := clLime;
//        vbl2.Palette1.High  := clLime;
//
//        FRippleCountArray[1] := False;
//        FRippleDegreeLauncer[1]  := LauncherAngle;
//      end;
//    end;
//    3 :
//    begin
//      if vbl3.EnableBlinking = True then
//        vbl3.EnableBlinking := False
//      else
//        vbl3.EnableBlinking := True;
//
//      if vbl3.EnableBlinking then
//        begin
//          vbl3.Palette1.Low   := clLime;
//          vbl3.Palette1.High  := clRed;
//
//          FRippleCountArray[2] := True;
//          FRippleDegreeLauncer[2]  := LauncherAngle;
//        end
//      else
//      begin
//        vbl3.Palette1.Low   := clLime;
//        vbl3.Palette1.High  := clLime;
//
//        FRippleCountArray[2] := False;
//        FRippleDegreeLauncer[2]  := LauncherAngle;
//      end;
//    end;
//    4 :
//    begin
//      if vbl4.EnableBlinking = True then
//        vbl4.EnableBlinking := False
//      else
//        vbl4.EnableBlinking := True;
//
//      if vbl4.EnableBlinking then
//        begin
//          vbl4.Palette1.Low   := clLime;
//          vbl4.Palette1.High  := clRed;
//
//          FRippleCountArray[3] := True;
//          FRippleDegreeLauncer[3]  := LauncherAngle;
//        end
//      else
//      begin
//        vbl4.Palette1.Low   := clLime;
//        vbl4.Palette1.High  := clLime;
//
//        FRippleCountArray[3] := False;
//        FRippleDegreeLauncer[3]  := LauncherAngle;
//      end;
//    end;
//    5 :
//    begin
//      if vbl5.EnableBlinking = True then
//        vbl5.EnableBlinking := False
//      else
//        vbl5.EnableBlinking := True;
//
//      if vbl5.EnableBlinking then
//        begin
//          vbl5.Palette1.Low   := clLime;
//          vbl5.Palette1.High  := clRed;
//
//          FRippleCountArray[4] := True;
//          FRippleDegreeLauncer[4]  := LauncherAngle;
//        end
//      else
//      begin
//        vbl5.Palette1.Low   := clLime;
//        vbl5.Palette1.High  := clLime;
//
//        FRippleCountArray[4] := False;
//        FRippleDegreeLauncer[4]  := LauncherAngle;
//      end;
//    end;
//    6 :
//    begin
//      if vbl6.EnableBlinking = True then
//        vbl6.EnableBlinking := False
//      else
//        vbl6.EnableBlinking := True;
//
//      if vbl6.EnableBlinking then
//        begin
//          vbl6.Palette1.Low   := clLime;
//          vbl6.Palette1.High  := clRed;
//
//          FRippleCountArray[5] := True;
//          FRippleDegreeLauncer[5]  := LauncherAngle;
//        end
//      else
//      begin
//        vbl6.Palette1.Low   := clLime;
//        vbl6.Palette1.High  := clLime;
//
//        FRippleCountArray[5] := False;
//        FRippleDegreeLauncer[5]  := LauncherAngle;
//      end;
//    end;
//    7 :
//    begin
//      if vbl7.EnableBlinking = True then
//        vbl7.EnableBlinking := False
//      else
//        vbl7.EnableBlinking := True;
//
//      if vbl7.EnableBlinking then
//        begin
//          vbl7.Palette1.Low   := clLime;
//          vbl7.Palette1.High  := clRed;
//
//          FRippleCountArray[6] := True;
//          FRippleDegreeLauncer[6]  := LauncherAngle;
//        end
//      else
//      begin
//        vbl7.Palette1.Low   := clLime;
//        vbl7.Palette1.High  := clLime;
//
//        FRippleCountArray[6] := False;
//        FRippleDegreeLauncer[6]  := LauncherAngle;
//      end;
//    end;
//    8 :
//    begin
//      if vbl8.EnableBlinking = True then
//        vbl8.EnableBlinking := False
//      else
//        vbl8.EnableBlinking := True;
//
//      if vbl8.EnableBlinking then
//        begin
//          vbl8.Palette1.Low   := clLime;
//          vbl8.Palette1.High  := clRed;
//
//          FRippleCountArray[7] := True;
//          FRippleDegreeLauncer[7]  := LauncherAngle;
//        end
//      else
//      begin
//        vbl8.Palette1.Low   := clLime;
//        vbl8.Palette1.High  := clLime;
//
//        FRippleCountArray[7] := False;
//        FRippleDegreeLauncer[7]  := LauncherAngle;
//      end;
//    end;
//  end;
//
//  FLauncherIDLastOn := id;
//  rMis.LaunchAngle := LauncherAngle;
end;

procedure TfrmSurfaceToSurfaceMissile.edtDestructRangeKeyPress(Sender: TObject;
  var Key: Char);
var
  ValKey : set of AnsiChar;
  Value : Double;
  rec : TRecCmd_LaunchMissile;
begin
  inherited;

  ValKey := [#48 .. #57, #8, #13, #46];
  if not(CharInSet(Key, ValKey)) then
    Key := #0;

  if Key = #13 then
  begin
    if TryStrToFloat(TEdit(sender).Text,Value) then
    begin
      case TEdit(Sender).Tag of
        { cross over range }
        1 :
        begin
          if Value > 0 then
          begin
            with TT3MountedSurfaceToSurfaceMissile(ControlledWeapon) do
            begin
              rec.MissileID   := InstanceIndex;
              rec.MissileName := InstanceName;
              rec.Order       := CORD_ID_MissileXoverRng;
              rec.ValSingle   := Value;
              rec.ParentPlatformID  := clientManager.ControlledTrack.ObjectInstanceIndex;
              clientManager.NetCmdSender.CmdMissile(rec);
            end;
          end
          else
          begin
            UpdatePanelStatus('Input must be > 0');
            TEdit(sender).Text := FloatToStr(FMountedMissile.CrossOverRange);
          end
        end;
        { destruck range }
        2 :
        begin
          if Value - 1 > FMountedMissile.SeekerRangeTurn then
          begin
            with TT3MountedSurfaceToSurfaceMissile(ControlledWeapon) do
            begin
              rec.MissileID   := InstanceIndex;
              rec.MissileName := InstanceName;
              rec.Order       := CORD_ID_MissileDestruckRng;
              rec.ValSingle   := Value;
              rec.ParentPlatformID  := clientManager.ControlledTrack.ObjectInstanceIndex;
              clientManager.NetCmdSender.CmdMissile(rec);
            end;
          end
          else
          if Value > FMountedMissile.WeaponRange then
          begin
            UpdatePanelStatus('Input Reach Maximum Seeker On');
            TEdit(sender).Text := FloatToStr(FMountedMissile.DestruckRange);
          end
          else
          begin
            UpdatePanelStatus('Input Reach Minimum Seeker On');
            TEdit(sender).Text := FloatToStr(FMountedMissile.DestruckRange);
          end;
        end;
      end;
    end else
    begin
      case TEdit(Sender).Tag of
        { cross over range }
        1 : TEdit(sender).Text := FloatToStr(FMountedMissile.CrossOverRange);
        { destruck range }
        2 : TEdit(sender).Text := FloatToStr(FMountedMissile.DestruckRange);
      end;
    end;

  end;
end;

procedure TfrmSurfaceToSurfaceMissile.FormCreate(Sender: TObject);
begin
  inherited;
  FMountedMissile := nil;
end;

procedure TfrmSurfaceToSurfaceMissile.HideRange;
begin
  if Assigned(FMountedMissile) then
  begin
    FMountedMissile.ShowRange := False;
    //FVisibleShowRange := False;
    //missile.ShowRange := False;
    //missile.ShowRangeSelected := FVisibleShowRange and missile.ShowRange;
  end;
end;

procedure TfrmSurfaceToSurfaceMissile.Launch;
begin

end;

procedure TfrmSurfaceToSurfaceMissile.OnMenuEngageClick(Sender: TObject);
var
  rec : TRecCmd_LaunchMissile;
begin
  if Assigned(ControlledWeapon) and
     (ControlledWeapon is TT3MountedSurfaceToSurfaceMissile) then
  begin
    with TT3MountedSurfaceToSurfaceMissile(ControlledWeapon) do
    begin
      rec.MissileID   := InstanceIndex;
      rec.MissileName := InstanceName;
      rec.Order       := CORD_ID_MissileEngageType;

      case TMenuItem(Sender).Tag of
        1 : begin
              rec.EngagementMode := Byte(meRipple);
            end;
        2 : begin
              rec.EngagementMode := Byte(meSTOT);
            end;
        3 : begin
              rec.EngagementMode := Byte(meDTOT);
            end;
      end;
    end;

    rec.ParentPlatformID := clientManager.ControlledTrack.ObjectInstanceIndex;
    clientManager.NetCmdSender.CmdMissile(rec);
  end;
end;

procedure TfrmSurfaceToSurfaceMissile.OnMenuFiringClick(Sender: TObject);
var
  r : TRecCmd_LaunchMissile;
begin
  if Assigned(FMountedMissile) then
  begin
    with FMountedMissile do
    begin
      r.MissileID   := InstanceIndex;
      r.MissileName := InstanceName;
      r.Order       := CORD_ID_MissileFiringType;

      case TMenuItem(Sender).Tag of
        1 : begin
              r.FiringMode := byte(mfmRBL);
            end;
        2 : begin
              r.FiringMode := byte(mfmRBLW);
            end;
        3 : begin
              r.FiringMode := byte(mfmBOL);
            end;
        4 : begin
              r.FiringMode := byte(mfmBOLW);
            end;
      end;
    end;

    r.ParentPlatformID := clientManager.ControlledTrack.ObjectInstanceIndex;;
    clientManager.NetCmdSender.CmdMissile(r);
  end;
end;

procedure TfrmSurfaceToSurfaceMissile.Plan;
var
  Range, bearing : Double;
  pfTarget : TT3PlatformInstance;
  pfCtrl : TT3PlatformInstance;
  i : integer;
  CoordBearingX, CoordBearingY : Double ;
  rec : TRecCmd_LaunchMissile;
begin
  pfTarget := simManager.FindT3PlatformByID(FMountedMissile.TargetInstanceIdx);
  pfCtrl   := simManager.FindT3PlatformByID(TT3ClientManager(simManager).ControlledTrack.ObjectInstanceIndex);

  if not Assigned(pfTarget) then
  begin
    UpdatePanelStatus('Target not valid');    
    Exit;
  end;                                   

  Range := CalcRange(
    pfCtrl.getPositionX, pfCtrl.getPositionY,  
    pfTarget.getPositionX, pfTarget.getPositionY);
 
  if Range <= FMountedMissile.MissileDefinition.Min_Range then
  begin
    UpdatePanelStatus('Target is to close');
    setThisLauncherState;
  end
  else
  if Range >= FMountedMissile.MissileDefinition.Max_Range then
  begin
    UpdatePanelStatus('Target is too far');
    setThisLauncherState;
  end
  else
  if (Range > FMountedMissile.MissileDefinition.Min_Range) and
     (Range < FMountedMissile.MissileDefinition.Max_Range) then
  begin

    rec.MissileID        := FMountedMissile.InstanceIndex;
    rec.MissileName      := FMountedMissile.InstanceName;
    rec.Order            := CORD_ID_MissilePlan;
    rec.ValInteger       := 1;
    rec.ParentPlatformID := clientManager.ControlledTrack.ObjectInstanceIndex;
    clientManager.NetCmdSender.CmdMissile(rec);

{ semua prosedur ini harusnya ada di model TT3MountedSurfaceToSurface }
//    FMountedMissile.Planned := True;
//    missile.VehiclePlatform   := simMgrClient.SimPlatforms;
//    missile.VehicleDetect     := simMgrClient.SimDetectedTrackList;
//    missile.isControllerPlat  := simMgrClient.ISInstructor;
//    missile.VehicleOnGroup    := simMgrClient.FMyCubGroup;
//    missile.RangeRBLCircle    := 5;

//    btnSurfaceToSurfaceMissileWaypointsEdit.Enabled := True;
//    btnSurfaceToSurfaceCancel.Enabled               := True;
//    btnSurfaceToSurfaceLaunch.Enabled               := True;
//    btnSurfaceToSurfacePlan.Enabled                 := False;
//
//    btnSurfaceToSurfaceMissileLauncherMore.Enabled := True;
//    btnSurfaceToSurfaceMissileTargetTrackDetails.Enabled := True;

//    TT3Vehicle(TT3PlatformInstance(FControlled)).isReadyToCalculate := False;

//    case FMountedMissile.FiringMode of
//      mfmRBL, mfmBOL:
//      begin
//        for i := 0 to 7 do
//        begin
//          if FRippleCountArray[i] then
//          begin
//            //frmToteDisplay.ClearAllSurfaceToSurfaceList(TT3PlatformInstance(FControlled));
//            //frmToteDisplay.OnWeaponEngaged(missile, missile.TargetObject, i+1);
//            frmToteDisplay.TotePanelManager.ShowTote(C_SurfaceToSurface);
//            Break;
//          end;
//        end;
//      end;
//      mfmRBLW:
//      begin
//        //frmToteDisplay.ClearAllSurfaceToSurfaceList(TT3PlatformInstance(FControlled));
//        for i := 0 to 7 do
//        begin
//          if FRippleCountArray[i] then
//          begin
////            frmToteDisplay.OnweaponEngageWaypoint(TT3PlatformInstance(FControlled),
////                missile, TT3PlatformInstance(missile.TargetObject),
////                DistanceToTarget, FRippleDegreeLauncer[i], i+1, 0, 0);
//          end;
//        end;
//
//        frmToteDisplay.TotePanelManager.ShowTote(C_SurfaceToSurface);
//      end;
//    end;

//    case FMissile.FiringMode of
//      mfmRBLW :
//      begin
//        for i := 0 to 7 do
//        begin
//          if not FRippleCountArray[i] then
//            Continue;
//
//          bearing := CalcBearing(pfTarget.PosX,pfTarget.PosY,pfCtrl.PosX,pfCtrl.PosY);
//          RangeBearingToCoord(
//            FMissile.MissileDefinition.FDef.Min_Final_Leg_Length,
//            bearing, CoordBearingX, CoordBearingY);
//
//          FMissile.RBLW_PointX := pfTarget.PosX + CoordBearingX;
//          FMissile.RBLW_PointY := pfTarget.PosY + CoordBearingY;

//          //Sincron
//          with RecSend do
//          begin
//            cmd := 0;
//            PlatformID := TT3PlatformInstance(FControlled).InstanceIndex;
//            WeaponIndex := missile.InstanceIndex;
//            WeaponName := missile.InstanceName;
//            RBLWIndex := CheckIDRBLW(i);
//            RBLWLauncherIndex := i + 1;
//            RBLW_PointX := missile.RBLW_PointX;
//            RBLW_POintY := missile.RBLW_PointY;
//            IsAdd := False;
//          end;
//          simMgrClient.netSend_CmdSyncRBLW(RecSend);
//        end;
//      end;
//      mfmBOLW:
//      begin
//        for i := 0 to 7 do
//        begin
//          if not FRippleCountArray[i] then
//            Continue;
//
//          bearing := CalcBearing(pfTarget.PosX,pfTarget.PosY,pfCtrl.PosX,pfCtrl.PosY);
//          RangeBearingToCoord(
//            FMissile.MissileDefinition.FDef.Min_Final_Leg_Length,
//            bearing, CoordBearingX, CoordBearingY);
//          FMissile.BOLW_PointX := pfTarget.PosX + CoordBearingX;
//          FMissile.BOLW_PointY := pfTarget.PosY + CoordBearingY;
//
//          with RecSend2 do
//          begin
//            cmd := 0;
//            PlatformID := TT3PlatformInstance(FControlled).InstanceIndex;
//            WeaponIndex := missile.InstanceIndex;
//            WeaponName := missile.InstanceName;
//            BOLWIndex := CheckIDBOLW(i);
//            BOLWLauncherIndex := i + 1;
//            BOLW_PointX := missile.BOLW_PointX;
//            BOLW_POintY := missile.BOLW_PointY;
//            IsAdd := False;
//          end;
//          simMgrClient.netSend_CmdSyncBOLW(RecSend2);
//        end;
//      end;
//    end;
  end;
end;

procedure TfrmSurfaceToSurfaceMissile.SetControlledWeapon(aObj: TObject);
begin
  inherited;

  if Assigned(ControlledWeapon) and (ControlledWeapon is TT3MountedSurfaceToSurfaceMissile) then
  with (TT3MountedSurfaceToSurfaceMissile(ControlledWeapon)) do
  begin
    FMountedMissile := TT3MountedSurfaceToSurfaceMissile(ControlledWeapon);

    edtCrossOverRange.Text := FloatToStr(CrossOverRange);
    edtDestructRange.Text  := FloatToStr(DestruckRange);
  end;
end;

procedure TfrmSurfaceToSurfaceMissile.setThisLauncherState;
var
  i : Integer;
  launcher : TDBFitted_Weap_Launcher_On_Board;
begin
//  if ControlledWeapon is TT3MountedSurfaceToSurfaceMissile then
//  begin
//    for i := 0 to FMountedMissile.MissileDefinition.FLaunchs.Count - 1 do
//    begin
//      launcher := FMountedMissile.MissileDefinition.FLaunchs.Items[i];
//      if launcher.FData.Launcher_Qty > 0 then
//      begin
//        case launcher.FData.Launcher_Type of
//          1:
//          begin
//            vbl1.EnableBlinking := False;
//            vbl1.Palette1.Low   := clLime;
//            vbl1.Palette1.High  := clLime;
//            pnlLaunch1.Hint     := 'Qty : ' +IntToStr(launcher.FData.Launcher_Qty)
//                                   +Chr(13) + 'Angle : '
//                                   +IntToStr(launcher.FData.Launcher_Angle);
//            pnlLaunch1.Enabled  := True;
//          end;
//          2:
//          begin
//            vbl2.EnableBlinking := False;
//            vbl2.Palette1.Low   := clLime;
//            vbl2.Palette1.High  := clLime;
//            pnlLaunch2.Hint     := 'Qty : ' +IntToStr(launcher.FData.Launcher_Qty)
//                                   + Chr(13) + 'Angle : ' +IntToStr(launcher.FData.Launcher_Angle);
//            pnlLaunch2.Enabled  := True;
//          end;
//          3:
//          begin
//            vbl3.EnableBlinking := False;
//            vbl3.Palette1.Low   := clLime;
//            vbl3.Palette1.High  := clLime;
//            pnlLaunch3.Hint     := 'Qty : ' + IntToStr(launcher.FData.Launcher_Qty)
//                                   + Chr(13) + 'Angle : ' + IntToStr(launcher.FData.Launcher_Angle);
//            pnlLaunch3.Enabled  := True;
//          end;
//          4:
//          begin
//            vbl4.EnableBlinking := False;
//            vbl4.Palette1.Low   := clLime;
//            vbl4.Palette1.High  := clLime;
//            pnlLaunch4.Hint     := 'Qty : ' + IntToStr(launcher.FData.Launcher_Qty)
//                                   + Chr(13) + 'Angle : ' + IntToStr(launcher.FData.Launcher_Angle);
//            pnlLaunch4.Enabled  := True;
//          end;
//          5:
//          begin
//            vbl5.EnableBlinking := False;
//            vbl5.Palette1.Low   := clLime;
//            vbl5.Palette1.High  := clLime;
//            pnlLaunch5.Hint     := 'Qty : ' + IntToStr(launcher.FData.Launcher_Qty)
//                                   + Chr(13) + 'Angle : ' + IntToStr(launcher.FData.Launcher_Angle);
//            pnlLaunch5.Enabled  := True;
//          end;
//          6:
//          begin
//            vbl6.EnableBlinking := False;
//            vbl6.Palette1.Low   := clLime;
//            vbl6.Palette1.High  := clLime;
//            pnlLaunch6.Hint     := 'Qty : ' + IntToStr(launcher.FData.Launcher_Qty)
//                                   + Chr(13) + 'Angle : ' + IntToStr(launcher.FData.Launcher_Angle);
//            pnlLaunch6.Enabled  := True;
//          end;
//          7:
//          begin
//            vbl7.EnableBlinking := False;
//            vbl7.Palette1.Low   := clLime;
//            vbl7.Palette1.High  := clLime;
//            pnlLaunch7.Hint     := 'Qty : ' + IntToStr(launcher.FData.Launcher_Qty)
//                                   + Chr(13) + 'Angle : ' + IntToStr(launcher.FData.Launcher_Angle);
//            pnlLaunch7.Enabled  := True;
//          end;
//          8:
//          begin
//            vbl8.EnableBlinking := False;
//            vbl8.Palette1.Low   := clLime;
//            vbl8.Palette1.High  := clLime;
//            pnlLaunch8.Hint     := 'Qty : ' + IntToStr(launcher.FData.Launcher_Qty)
//                                   + Chr(13) + 'Angle : ' + IntToStr(launcher.FData.Launcher_Angle);
//            pnlLaunch8.Enabled  := True;
//          end;
//          9 :
//          begin
//            vbl1.EnableBlinking := False;
//            vbl1.Palette1.Low   := clLime;
//            vbl1.Palette1.High  := clLime;
//            pnlLaunch1.Hint     := 'Qty : ' + IntToStr(launcher.FData.Launcher_Qty)
//                                   + Chr(13) + 'Angle : ' + IntToStr(launcher.FData.Launcher_Angle);
//            pnlLaunch1.Enabled  := True;
//          end;
//          10:
//          begin
//            vbl2.EnableBlinking := False;
//            vbl2.Palette1.Low   := clLime;
//            vbl2.Palette1.High  := clLime;
//            pnlLaunch2.Hint     := 'Qty : ' + IntToStr(launcher.FData.Launcher_Qty)
//                                   + Chr(13) + 'Angle : ' + IntToStr(launcher.FData.Launcher_Angle);
//            pnlLaunch2.Enabled  := True;
//          end;
//          11:
//          begin
//            vbl3.EnableBlinking := False;
//            vbl3.Palette1.Low   := clLime;
//            vbl3.Palette1.High  := clLime;
//            pnlLaunch3.Hint     := 'Qty : ' + IntToStr(launcher.FData.Launcher_Qty)
//                                   + Chr(13) + 'Angle : ' + IntToStr(launcher.FData.Launcher_Angle);
//            pnlLaunch3.Enabled  := True;
//          end;
//          12:
//          begin
//            vbl4.EnableBlinking := False;
//            vbl4.Palette1.Low   := clLime;
//            vbl4.Palette1.High  := clLime;
//            pnlLaunch4.Hint     := 'Qty : ' + IntToStr(launcher.FData.Launcher_Qty)
//                                   + Chr(13) + 'Angle : ' + IntToStr(launcher.FData.Launcher_Angle);
//            pnlLaunch4.Enabled  := True;
//          end;
//          13:
//          begin
//            vbl5.EnableBlinking := False;
//            vbl5.Palette1.Low   := clLime;
//            vbl5.Palette1.High  := clLime;
//            pnlLaunch5.Hint     := 'Qty : ' + IntToStr(launcher.FData.Launcher_Qty)
//                                   + Chr(13) + 'Angle : ' + IntToStr(launcher.FData.Launcher_Angle);
//            pnlLaunch5.Enabled  := True;
//          end;
//          14:
//          begin
//            vbl6.EnableBlinking := False;
//            vbl6.Palette1.Low   := clLime;
//            vbl6.Palette1.High  := clLime;
//            pnlLaunch6.Hint     := 'Qty : ' + IntToStr(launcher.FData.Launcher_Qty)
//                                   + Chr(13) + 'Angle : ' + IntToStr(launcher.FData.Launcher_Angle);
//            pnlLaunch6.Enabled  := True;
//          end;
//          15:
//          begin
//            vbl7.EnableBlinking := False;
//            vbl7.Palette1.Low   := clLime;
//            vbl7.Palette1.High  := clLime;
//            pnlLaunch7.Hint     := 'Qty : ' + IntToStr(launcher.FData.Launcher_Qty)
//                                   + Chr(13) + 'Angle : ' + IntToStr(launcher.FData.Launcher_Angle);
//            pnlLaunch7.Enabled  := True;
//          end;
//          16:
//          begin
//            vbl8.EnableBlinking := False;
//            vbl8.Palette1.Low   := clLime;
//            vbl8.Palette1.High  := clLime;
//            pnlLaunch8.Hint     := 'Qty : ' + IntToStr(launcher.FData.Launcher_Qty)
//                                   + Chr(13) + 'Angle : ' + IntToStr(launcher.FData.Launcher_Angle);
//            pnlLaunch8.Enabled  := True;
//          end;
//        end;
//      end
//      else
//      begin
//        case launcher.FData.Launcher_Type of
//          1:
//          begin
//            vbl1.EnableBlinking := False;
//            vbl1.Palette1.Low   := clGray;
//            vbl1.Palette1.High  := clGray;
//            pnlLaunch1.Hint     := 'Qty : ' + IntToStr(launcher.FData.Launcher_Qty)
//                                   + Chr(13) + 'Angle : ' + IntToStr(launcher.FData.Launcher_Angle);
//            pnlLaunch1.Enabled  := False;
//          end;
//          2:
//          begin
//            vbl2.EnableBlinking := False;
//            vbl2.Palette1.Low   := clGray;
//            vbl2.Palette1.High  := clGray;
//            pnlLaunch2.Hint     := 'Qty : ' + IntToStr(launcher.FData.Launcher_Qty)
//                                   + Chr(13) + 'Angle : ' + IntToStr(launcher.FData.Launcher_Angle);
//            pnlLaunch2.Enabled  := False;
//          end;
//          3:
//          begin
//            vbl3.EnableBlinking := False;
//            vbl3.Palette1.Low   := clGray;
//            vbl3.Palette1.High  := clGray;
//            pnlLaunch3.Hint     := 'Qty : ' + IntToStr(launcher.FData.Launcher_Qty)
//                                   + Chr(13) + 'Angle : ' + IntToStr(launcher.FData.Launcher_Angle);
//            pnlLaunch3.Enabled  := False;
//          end;
//          4:
//          begin
//            vbl4.EnableBlinking := False;
//            vbl4.Palette1.Low   := clGray;
//            vbl4.Palette1.High  := clGray;
//            pnlLaunch4.Hint     := 'Qty : ' + IntToStr(launcher.FData.Launcher_Qty)
//                                   + Chr(13) + 'Angle : ' + IntToStr(launcher.FData.Launcher_Angle);
//            pnlLaunch4.Enabled  := False;
//          end;
//          5:
//          begin
//            vbl5.EnableBlinking := False;
//            vbl5.Palette1.Low   := clGray;
//            vbl5.Palette1.High  := clGray;
//            pnlLaunch5.Hint     := 'Qty : ' + IntToStr(launcher.FData.Launcher_Qty)
//                                   + Chr(13) + 'Angle : ' + IntToStr(launcher.FData.Launcher_Angle);
//            pnlLaunch5.Enabled  := False;
//          end;
//          6:
//          begin
//            vbl6.EnableBlinking := False;
//            vbl6.Palette1.Low   := clGray;
//            vbl6.Palette1.High  := clGray;
//            pnlLaunch6.Hint     := 'Qty : ' + IntToStr(launcher.FData.Launcher_Qty)
//                                   + Chr(13) + 'Angle : ' + IntToStr(launcher.FData.Launcher_Angle);
//            pnlLaunch6.Enabled  := False;
//          end;
//          7:
//          begin
//            vbl7.EnableBlinking := False;
//            vbl7.Palette1.Low   := clGray;
//            vbl7.Palette1.High  := clGray;
//            pnlLaunch7.Hint     := 'Qty : ' + IntToStr(launcher.FData.Launcher_Qty)
//                                   + Chr(13) + 'Angle : ' + IntToStr(launcher.FData.Launcher_Angle);
//            pnlLaunch7.Enabled  := False;
//          end;
//          8:
//          begin
//            vbl8.EnableBlinking := False;
//            vbl8.Palette1.Low   := clGray;
//            vbl8.Palette1.High  := clGray;
//            pnlLaunch8.Hint     := 'Qty : ' + IntToStr(launcher.FData.Launcher_Qty)
//                                   + Chr(13) + 'Angle : ' + IntToStr(launcher.FData.Launcher_Angle);
//            pnlLaunch8.Enabled  := False;
//          end;
//          9 :
//          begin
//            vbl1.EnableBlinking := False;
//            vbl1.Palette1.Low   := clGray;
//            vbl1.Palette1.High  := clGray;
//            pnlLaunch1.Hint     := 'Qty : ' + IntToStr(launcher.FData.Launcher_Qty)
//                                   + Chr(13) + 'Angle : ' + IntToStr(launcher.FData.Launcher_Angle);
//            pnlLaunch1.Enabled  := False;
//          end;
//          10:
//          begin
//            vbl2.EnableBlinking := False;
//            vbl2.Palette1.Low   := clGray;
//            vbl2.Palette1.High  := clGray;
//            pnlLaunch2.Hint     := 'Qty : ' + IntToStr(launcher.FData.Launcher_Qty)
//                                   + Chr(13) + 'Angle : ' + IntToStr(launcher.FData.Launcher_Angle);
//            pnlLaunch2.Enabled  := False;
//          end;
//          11:
//          begin
//            vbl3.EnableBlinking := False;
//            vbl3.Palette1.Low   := clGray;
//            vbl3.Palette1.High  := clGray;
//            pnlLaunch3.Hint     := 'Qty : ' + IntToStr(launcher.FData.Launcher_Qty)
//                                   + Chr(13) + 'Angle : ' + IntToStr(launcher.FData.Launcher_Angle);
//            pnlLaunch3.Enabled  := False;
//          end;
//          12:
//          begin
//            vbl4.EnableBlinking := False;
//            vbl4.Palette1.Low   := clGray;
//            vbl4.Palette1.High  := clGray;
//            pnlLaunch4.Hint     := 'Qty : ' + IntToStr(launcher.FData.Launcher_Qty)
//                                   + Chr(13) + 'Angle : ' + IntToStr(launcher.FData.Launcher_Angle);
//            pnlLaunch4.Enabled  := False;
//          end;
//          13:
//          begin
//            vbl5.EnableBlinking := False;
//            vbl5.Palette1.Low   := clGray;
//            vbl5.Palette1.High  := clGray;
//            pnlLaunch5.Hint     := 'Qty : ' + IntToStr(launcher.FData.Launcher_Qty)
//                                   + Chr(13) + 'Angle : ' + IntToStr(launcher.FData.Launcher_Angle);
//            pnlLaunch5.Enabled  := False;
//          end;
//          14:
//          begin
//            vbl6.EnableBlinking := False;
//            vbl6.Palette1.Low   := clGray;
//            vbl6.Palette1.High  := clGray;
//            pnlLaunch6.Hint     := 'Qty : ' + IntToStr(launcher.FData.Launcher_Qty)
//                                   + Chr(13) + 'Angle : ' + IntToStr(launcher.FData.Launcher_Angle);
//            pnlLaunch6.Enabled  := False;
//          end;
//          15:
//          begin
//            vbl7.EnableBlinking := False;
//            vbl7.Palette1.Low   := clGray;
//            vbl7.Palette1.High  := clGray;
//            pnlLaunch7.Hint     := 'Qty : ' + IntToStr(launcher.FData.Launcher_Qty)
//                                   + Chr(13) + 'Angle : ' + IntToStr(launcher.FData.Launcher_Angle);
//            pnlLaunch7.Enabled  := False;
//          end;
//          16:
//          begin
//            vbl8.EnableBlinking := False;
//            vbl8.Palette1.Low   := clGray;
//            vbl8.Palette1.High  := clGray;
//            pnlLaunch8.Hint     := 'Qty : ' + IntToStr(launcher.FData.Launcher_Qty)
//                                   + Chr(13) + 'Angle : ' + IntToStr(launcher.FData.Launcher_Angle);
//            pnlLaunch8.Enabled  := False;
//          end;
//        end;
//      end;
//    end;
//  end;
end;

procedure TfrmSurfaceToSurfaceMissile.ShowRange;
begin
  if Assigned(FMountedMissile) then
  begin
    FMountedMissile.ShowRange := True;
//  FVisibleShowRange := True;
//  Fmissile.ShowRangeSelected := FVisibleShowRange and missile.ShowRange;
//  HideRangeSensor;
  end;
end;

procedure TfrmSurfaceToSurfaceMissile.TargetTrack;
var
  r : TRecCmd_LaunchMissile;
  track : TT3Track;
  CanTargetPlatform : Boolean;
  pfTarget : TT3PlatformInstance;
begin

  if (not Assigned(TT3ClientManager(simManager).SelectedTrack)) or
    (not (TT3ClientManager(simManager).SelectedTrack is TT3PointTrack)) then
  begin
    UpdatePanelStatus('Target Track Not Valid');
    Exit;
  end;

  { track target must be point track }
  track := TT3PointTrack(TT3ClientManager(simManager).SelectedTrack);

  { target track must not itself }
  if TT3ClientManager(simManager).ControlledTrack.TrackNumber = track.TrackNumber then
  begin
    UpdatePanelStatus('Target Track Not Valid');
    edtSurfaceToSurfaceMissileTargetTrack.Text := '';
    Exit;
  end;

  //if platformdomain = Air; 0 = Air, 1 = Surface, 2 = Subsurface
  case track.TrackDomain of
    0:
    begin
      if FMountedMissile.MissileDefinition.Anti_Air_Capable = 1 then
        CanTargetPlatform := True;
    end;
    1:
    begin
      if FMountedMissile.MissileDefinition.Anti_Sur_Capable = 1 then
        CanTargetPlatform := True;
    end;
    2:
    begin
      if FMountedMissile.MissileDefinition.Anti_SubSur_Capable = 1 then
        CanTargetPlatform := True;
    end;
    3:
    begin
      if FMountedMissile.MissileDefinition.Anti_Land_Capable = 1 then
        CanTargetPlatform := True;
    end;
    vhdAmphibious :
    begin
      if Boolean(FMountedMissile.MissileDefinition.Anti_Amphibious_Capable) then
        CanTargetPlatform := True;
    end;
  end;

  if not CanTargetPlatform then
  begin
    UpdatePanelStatus('Target Domain Not Same With Missile');
    edtSurfaceToSurfaceMissileTargetTrack.Text := '';
    Exit;
  end;

  { is missile has been planned to launch }
  if FMountedMissile.Planned then
  begin
    UpdatePanelStatus('Target Has Been Planned');
    Exit;
  end;

  { then this is valid target track }
  edtSurfaceToSurfaceMissileTargetTrack.Text := track.TrackLabelInfo;

  { set target track }
  with FMountedMissile do
  begin
    r.MissileID        := InstanceIndex;
    r.MissileName      := InstanceName;
    r.Order            := CORD_ID_MissileTargetId;
    r.ParentPlatformID := clientManager.ControlledTrack.ObjectInstanceIndex;
    r.TargetPlatformID := clientManager.SelectedTrack.ObjectInstanceIndex;
    r.ValInteger       := clientManager.SelectedTrack.TrackNumber;
    clientManager.NetCmdSender.CmdMissile(r);
  end;

  { semua prosedur dibawah harusnya ad di model TT3MountedSurfaceToSurfaceMissile }
//  FMissileRec.TargetPlatformID := TT3PlatformInstance(focused_platform).InstanceIndex;
//  FMissileRec.MissileID        := FMissile.InstanceIndex;
//  if machineRole = crController then
//    FMissileRec.NoCubicle  := 0
//  else
//    FMissileRec.NoCubicle  :=  simMgrClient.FMyCubGroup.FData.Group_Index;

  //if not (focused_platform is TT3PlatformInstance) then
  //  Exit;

//  myTrackId := TT3PlatformInstance(FControlled).Track_ID;
//
//  if focused_platform is TT3NonRealVehicle then
//    targetTrackId := IntToStr(TT3PlatformInstance(focused_platform).TrackNumber)
//  else if focused_platform is TT3Vehicle then
//    targetTrackId := TT3PlatformInstance(focused_platform).Track_ID;
//
//  sObject := simMgrClient.findDetectedTrack(focused_platform);
//  if Assigned(sObject) then
//    trackNum := FormatTrackNumber(TT3DetectedTrack(sObject).TrackNumber)
//  else
//    trackNum := TT3PlatformInstance(focused_platform).TrackLabel;
//
//  if myTrackId <> targetTrackId then
//  begin
//    if simMgrClient.ISInstructor then
//      editSurfaceToSurfaceMissileTargetTrack.Text := targetTrackId
//    else
//      editSurfaceToSurfaceMissileTargetTrack.Text := trackNum;  {ambil Track Numbernya}
//
//    missile.TargetObject := focused_platform;
//    FisTrack := True;
//    //btnSurfaceToSurfacePlan.Enabled := true;
//    //btnSurfaceToSurfacePlan.Enabled := true;
//
//    rMis.ParentPlatformID := TT3PlatformInstance(FControlled).InstanceIndex;
//    rMis.TargetPlatformID := TT3PlatformInstance(focused_platform).InstanceIndex;
//    rMis.MissileID        := missile.InstanceIndex;
//
//    if simMgrClient.ISInstructor then
//      rMis.NoCubicle  := 0
//    else
//      rMis.NoCubicle  := simMgrClient.FMyCubGroup.FData.Group_Index;
//  end
//  else
//  begin
//    frmTacticalDisplay.addStatus('Cant Track Own Vehicle');
//    editSurfaceToSurfaceMissileTargetTrack.Text := '';
//    FisTrack := False;
//  end;
//
//  setThisLauncherState; // sementara
end;

procedure TfrmSurfaceToSurfaceMissile.UpdateForm;
begin
  inherited;

  if Assigned(ControlledWeapon) and (ControlledWeapon is TT3MountedSurfaceToSurfaceMissile) then
  with (TT3MountedSurfaceToSurfaceMissile(ControlledWeapon)) do
  begin
    lblSurfaceToSurfaceMissileQuantity.Caption := IntToStr(Quantity);
  end;

end;

end.
