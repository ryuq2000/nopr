unit ufrmWireGuidedTorpedo;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls, ufrmWeaponType;

type
  TfrmWireGuidedTorpedo = class(TfrmWeaponType)
    grpWireGuidedTorpedo: TGroupBox;
    scrlbx1: TScrollBox;
    bvl1: TBevel;
    bvl2: TBevel;
    bvl3: TBevel;
    btnlWireGuidedTorpedoTargetTrack: TSpeedButton;
    lbl1: TLabel;
    lbl2: TLabel;
    lbl3: TLabel;
    lbl4: TLabel;
    lbl5: TLabel;
    lbl6: TLabel;
    lbl7: TLabel;
    lbl8: TLabel;
    lbl9: TLabel;
    lbl10: TLabel;
    lbl11: TLabel;
    lbl12: TLabel;
    lbl13: TLabel;
    lbl14: TLabel;
    lblWGQuantity: TLabel;
    lblWireGuidedTorpedoStatus: TLabel;
    lblWireGuidedTorpedoTargetCourse: TLabel;
    lblWireGuidedTorpedoTargetDepth: TLabel;
    lblWireGuidedTorpedoTargetGround: TLabel;
    lblWireGuidedTorpedoTargetIdentity: TLabel;
    btnWireGuidedTorpedodDisplayRangeHide: TSpeedButton;
    btnWireGuidedTorpedoDisplayBlindHide: TSpeedButton;
    btnWireGuidedTorpedoDisplayBlindShow: TSpeedButton;
    btnWireGuidedTorpedoDisplayRangeShow: TSpeedButton;
    btnLaunchWG: TButton;
    edtEdtWGTargetTrack: TEdit;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmWireGuidedTorpedo: TfrmWireGuidedTorpedo;

implementation

{$R *.dfm}

end.
