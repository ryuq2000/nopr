unit ufrmBombDepthCharge;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls,ufrmWeaponType;

type
  TfrmBombDepthCharge = class(TfrmWeaponType)
    grpBombDepthCharge: TGroupBox;
    bvl17: TBevel;
    bvl18: TBevel;
    bvl19: TBevel;
    bvl20: TBevel;
    btnBombTarget: TSpeedButton;
    lbl76: TLabel;
    lbl77: TLabel;
    lbl78: TLabel;
    lbl79: TLabel;
    lbl80: TLabel;
    lbl81: TLabel;
    lbl82: TLabel;
    lbl83: TLabel;
    lblBombQuantity: TLabel;
    lblBombStatus: TLabel;
    btnBombDisplayRangeHide: TSpeedButton;
    btnBombDisplayRangeShow: TSpeedButton;
    lbl86: TLabel;
    btnBombDrop: TButton;
    edtEdtBombControlSalvo: TEdit;
    edtEdtBombTargetTrack: TEdit;
    chkBombDropWhitoutTarget: TCheckBox;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmBombDepthCharge: TfrmBombDepthCharge;

implementation

{$R *.dfm}

end.
