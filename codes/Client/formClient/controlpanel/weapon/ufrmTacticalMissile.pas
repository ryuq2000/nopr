unit ufrmTacticalMissile;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls, ufrmWeaponType, uT3MountedTacticalMissiles;

type
  TfrmTacticalMissile = class(TfrmWeaponType)
    grpTacticalMissiles: TGroupBox;
    scrlbx1: TScrollBox;
    bvl1: TBevel;
    bvl2: TBevel;
    bvl3: TBevel;
    bvl4: TBevel;
    bvl5: TBevel;
    btnTacticalMissileTargetAimpoint: TSpeedButton;
    btnTacticalMissileTargetTrack: TSpeedButton;
    lbl1: TLabel;
    lbl2: TLabel;
    lbl3: TLabel;
    lbl4: TLabel;
    lbl5: TLabel;
    lbl6: TLabel;
    lbl7: TLabel;
    lbl8: TLabel;
    lbl9: TLabel;
    lbl10: TLabel;
    lbl11: TLabel;
    lbl12: TLabel;
    lbl13: TLabel;
    lbl14: TLabel;
    lblTacticalMissileStatus: TLabel;
    lblTacticalMissileStatusQuantity: TLabel;
    btnTacticalMissileDisplayBlindHide: TSpeedButton;
    btnTacticalMissileDisplayBlindShow: TSpeedButton;
    btnTacticalMissileDisplayRangeHide: TSpeedButton;
    btnTacticalMissileDisplayRangeShow: TSpeedButton;
    btnTacticalMissileControlCruise: TButton;
    btnTacticalMissileControlSeeker: TButton;
    btnTacticalMissileLaunch: TButton;
    btnTacticalMissileTargetBearing: TButton;
    btnTacticalMissileWaypointAdd: TButton;
    btnTacticalMissileWaypointCancel: TButton;
    btnTacticalMissileWaypointEdit: TButton;
    edtTacticalMissileControlCruise: TEdit;
    edtTacticalMissileControlSalvo: TEdit;
    edtTacticalMissileControlSeeker: TEdit;
    rbTacticalMissileTargetAimpoint: TRadioButton;
    rbTacticalMissileTargetBearing: TRadioButton;
    rbTacticalMissileTargetTrack: TRadioButton;
    edtTacticalMissileTargetAimpoint: TEdit;
    edtTacticalMissileTargetTrack: TEdit;
    edtTacticalMissileTargetBearing: TEdit;
    procedure BtnTacticalClick(Sender: TObject);
    procedure edtTacticalMissileControlSalvoKeyPress(Sender: TObject;
      var Key: Char);
    procedure OnTacticalKeyPress(Sender: TObject; var Key: Char);
    procedure OnChecked(Sender: TObject);
  private
    { Private declarations }
    FMountedMissile    : TT3MountedTacticalMissiles;
    function CheckedFunctionality : Boolean;
    procedure ShowRange;
    procedure HideRange;
    procedure ShowBlindZone;
    procedure HideBlindZone;
    procedure Launch;
    procedure TargetTrack;
    procedure TargetAimPoint;
    procedure BearingToHook;
    procedure CruiseDefault;
    procedure RangeDefault;
    procedure EditWaypoint;
    procedure AddWaypoint;
    procedure CancelWaypoint;
  public
    { Public declarations }
    procedure SetControlledWeapon(aObj : TObject); override;
    procedure UpdateForm; override;
  end;

var
  frmTacticalMissile: TfrmTacticalMissile;

implementation

uses
  uGameData_TTT, uT3ClientManager, tttData, uBaseCoordSystem,
  uGlobalVar, uT3PlatformInstance, uT3Vehicle;

{$R *.dfm}

{ TfrmTacticalMissile }

procedure TfrmTacticalMissile.AddWaypoint;
begin

end;

procedure TfrmTacticalMissile.BearingToHook;
//var
//  RecMissileEnvi : TrecMissile_Envi;
//  BearingMissile : Double;
begin
//
//  Missile := TT3MissilesOnVehicle(focused_weapon);
//  BearingMissile := CalcBearing(TSimObject(FControlled).PosX, TSimObject(FControlled).PosY,
//              focused_platform.PosX, focused_platform.PosY);
//
//  with RecMissileEnvi do
//  begin
//    platformID        := TT3PlatformInstance(FControlled).InstanceIndex;
//    WeaponIndex       := missile.InstanceIndex;
//    WeaponName        := missile.InstanceName;
//    isHybrid          := False;
//    Missile_Kind      := Integer(Missile.WeaponCategory);
//    MIssile_Method    := Integer(missile.FiringMode);
//    Missile_PositionX := Missile.Aimpoint_Movement.PositionX;
//    Missile_PositionY := Missile.Aimpoint_Movement.PositionY;
//    Bearing           := BearingMissile;
//  end;
//  simMgrClient.netSend_CmdSyncMissileEnvi(RecMissileEnvi);
//
//  editTacticalMissileTargetBearing.Text := FormatFloat('0.00', BearingMissile);
end;

procedure TfrmTacticalMissile.BtnTacticalClick(Sender: TObject);
begin
  if not CheckedFunctionality then
    Exit;

  if Sender.ClassType = TSpeedButton then
  begin
    case TSpeedButton(Sender).Tag of
      1 : TargetTrack;
      2 : TargetAimPoint;
      3 : ShowRange;
      4 : HideRange;
      5 : ShowBlindZone;
      6 : HideBlindZone;
    end;
  end
  else
  if Sender.ClassType = TButton then
  begin
    case TButton(Sender).Tag of
      1 : BearingToHook;
      2 : CruiseDefault;
      3 : RangeDefault;
      4 : EditWaypoint;
      5 : AddWaypoint;
      6 : CancelWaypoint;
      7 : Launch;
    end;
  end;
end;

procedure TfrmTacticalMissile.CancelWaypoint;
begin

end;

function TfrmTacticalMissile.CheckedFunctionality: Boolean;
begin
  Result := True;
  if ControlledWeapon = nil then
  begin
    UpdatePanelStatus('Not Found Selected Weapon');
    Result := False;
    Exit;
  end;

  if not Assigned(clientManager.ControlledTrack) then
  begin
    UpdatePanelStatus('Controlled Vehicle Not Valid');
    Result := False;
    Exit;
  end;

  if not(ControlledWeapon is TT3MountedTacticalMissiles) then
  begin
    UpdatePanelStatus('Selected Weapon Is Not Missile');
    Result := False;
    Exit;
  end;

  if TT3MountedTacticalMissiles(ControlledWeapon).WeaponStatus = wsDamaged then
  begin
    UpdatePanelStatus('Selected Weapon Is Damaged');
    Result := False;
    Exit;
  end;

  if clientManager.GameState <> gsPlaying then
  begin
    UpdatePanelStatus('Game Frozen');
    Result := False;
    Exit;
  end;
end;

procedure TfrmTacticalMissile.CruiseDefault;
var
  value     : Double;
  rec       : TRecCmd_LaunchMissile;
begin
//  rec.MissileID   := FMountedMissile.InstanceIndex;
//  rec.MissileName := FMountedMissile.InstanceName;
//  rec.Order       := CORD_ID_MissileCruiseAlt;
//  rec.ValSingle   := FMountedMissile.MissileDefinition.FDef.;
//  rec.ParentPlatformID  := clientManager.ControlledTrack.ObjectInstanceIndex;
//  clientManager.NetCmdSender.CmdMissile(rec);
end;

procedure TfrmTacticalMissile.EditWaypoint;
begin

end;

procedure TfrmTacticalMissile.edtTacticalMissileControlSalvoKeyPress(
  Sender: TObject; var Key: Char);
var
  ValKey    : set of AnsiChar;
  salvoSize : integer;
  rec       : TRecCmd_LaunchMissile;
begin
  inherited;

  if not CheckedFunctionality then
    Exit;

  ValKey := [#48 .. #57, #8, #13, #46];
  if not(CharInSet(Key, ValKey)) then
    Key := #0;

  if Key = #13 then
  begin

    if TryStrToInt(TEdit(Sender).Text, salvoSize) then
    begin

      if not(ControlledWeapon is TT3MountedTacticalMissiles) then
        exit;

      if salvoSize > 0 then
      begin
        rec.MissileID   := FMountedMissile.InstanceIndex;
        rec.MissileName := FMountedMissile.InstanceName;
        rec.Order       := CORD_ID_MissileSalvo;
        rec.ValInteger  := salvoSize;
        rec.ParentPlatformID  := clientManager.ControlledTrack.ObjectInstanceIndex;
        clientManager.NetCmdSender.CmdMissile(rec);
      end else
        TEdit(Sender).Text := IntToStr(FMountedMissile.SalvoSize);
    end else
      TEdit(Sender).Text := IntToStr(FMountedMissile.SalvoSize);
  end;
end;

procedure TfrmTacticalMissile.HideBlindZone;
begin
  FMountedMissile.ShowBlindZone := False;

//  FVisibleShowBlind := False;
//  TT3MissilesOnVehicle(focused_weapon).ShowBlindSelected := FVisibleShowBlind and
//      TT3MissilesOnVehicle(focused_weapon).ShowBlind;
end;

procedure TfrmTacticalMissile.HideRange;
begin
  FMountedMissile.ShowRange := False;
//  FVisibleShowRange := False;
//  TT3MissilesOnVehicle(focused_weapon).ShowRangeSelected := FVisibleShowRange and
//      TT3MissilesOnVehicle(focused_weapon).ShowRange;

end;

procedure TfrmTacticalMissile.Launch;
begin

end;

procedure TfrmTacticalMissile.OnChecked(Sender: TObject);
begin
  inherited;

  case TCheckBox(Sender).Tag of
    1 :
    begin
      edtTacticalMissileTargetTrack.Enabled := True;
      btnTacticalMissileTargetTrack.Enabled := True;

      edtTacticalMissileTargetAimpoint.Enabled := False;
      btnTacticalMissileTargetAimpoint.Enabled := False;

      edtTacticalMissileTargetBearing.Enabled := False;
      btnTacticalMissileTargetBearing.Enabled := False;
    end;
    2 :
    begin
      edtTacticalMissileTargetTrack.Enabled := False;
      btnTacticalMissileTargetTrack.Enabled := False;

      edtTacticalMissileTargetAimpoint.Enabled := True;
      btnTacticalMissileTargetAimpoint.Enabled := True;

      edtTacticalMissileTargetBearing.Enabled := False;
      btnTacticalMissileTargetBearing.Enabled := False;
    end;
    3 :
    begin
      edtTacticalMissileTargetTrack.Enabled := False;
      btnTacticalMissileTargetTrack.Enabled := False;

      edtTacticalMissileTargetAimpoint.Enabled := False;
      btnTacticalMissileTargetAimpoint.Enabled := False;

      edtTacticalMissileTargetBearing.Enabled := True;
      btnTacticalMissileTargetBearing.Enabled := True;
    end;
  end;
end;

procedure TfrmTacticalMissile.OnTacticalKeyPress(Sender: TObject;
  var Key: Char);
var
  ValKey    : set of AnsiChar;
  value     : Double;
  rec       : TRecCmd_LaunchMissile;
begin
  inherited;

  if not CheckedFunctionality then
    Exit;

  ValKey := [#48 .. #57, #8, #13, #46];
  if not(CharInSet(Key, ValKey)) then
    Key := #0;

  if Key = #13 then
  begin

    if TryStrToFloat(TEdit(Sender).Text, value) then
    begin

      if not(ControlledWeapon is TT3MountedTacticalMissiles) then
        exit;

      if value > 0 then
      begin
        case TEdit(Sender).Tag of
          1 :
          begin
            rec.MissileID   := FMountedMissile.InstanceIndex;
            rec.MissileName := FMountedMissile.InstanceName;
            rec.Order       := CORD_ID_MissileCruiseAlt;
            rec.ValSingle   := value;
            rec.ParentPlatformID  := clientManager.ControlledTrack.ObjectInstanceIndex;
            clientManager.NetCmdSender.CmdMissile(rec);
          end;
          2 :
          begin
            rec.MissileID   := FMountedMissile.InstanceIndex;
            rec.MissileName := FMountedMissile.InstanceName;
            rec.Order       := CORD_ID_MissileSeekRng;
            rec.ValSingle   := value;
            rec.ParentPlatformID  := clientManager.ControlledTrack.ObjectInstanceIndex;
            clientManager.NetCmdSender.CmdMissile(rec);
          end;
        end;
      end else
      begin
        case TEdit(Sender).Tag of
          1 : TEdit(Sender).Text := FloatToStr(FMountedMissile.CruiseAltitude);
          2 : TEdit(Sender).Text := FloatToStr(FMountedMissile.SeekerRange);
        end;
      end;
    end else
    begin
      case TEdit(Sender).Tag of
        1 : TEdit(Sender).Text := FloatToStr(FMountedMissile.CruiseAltitude);
        2 : TEdit(Sender).Text := FloatToStr(FMountedMissile.SeekerRange);
      end;
    end;
  end;
end;

procedure TfrmTacticalMissile.RangeDefault;
var
  value     : Double;
  rec       : TRecCmd_LaunchMissile;
begin
  rec.MissileID   := FMountedMissile.InstanceIndex;
  rec.MissileName := FMountedMissile.InstanceName;
  rec.Order       := CORD_ID_MissileCruiseAlt;
  rec.ValSingle   := FMountedMissile.MissileDefinition.Seeker_TurnOn_Range;
  rec.ParentPlatformID  := clientManager.ControlledTrack.ObjectInstanceIndex;
  clientManager.NetCmdSender.CmdMissile(rec);
end;

procedure TfrmTacticalMissile.SetControlledWeapon(aObj: TObject);
begin
  if Assigned(ControlledWeapon) and (ControlledWeapon is TT3MountedTacticalMissiles) then
  with (TT3MountedTacticalMissiles(ControlledWeapon)) do
  begin
    FMountedMissile := TT3MountedTacticalMissiles(ControlledWeapon);

    case FMountedMissile.TargetType of
      1 : rbTacticalMissileTargetTrack.Checked := True;
      2 : rbTacticalMissileTargetAimpoint.Checked := True;
      3 : rbTacticalMissileTargetBearing.Checked := True;
    end;

  end;
end;

procedure TfrmTacticalMissile.ShowBlindZone;
begin
  FMountedMissile.ShowBlindZone := true;

//  FVisibleShowBlind := True;
//  TT3MissilesOnVehicle(focused_weapon).ShowBlindSelected := FVisibleShowBlind and
//      TT3MissilesOnVehicle(focused_weapon).ShowBlind;

end;

procedure TfrmTacticalMissile.ShowRange;
begin
  FMountedMissile.ShowRange := True;

//  FVisibleShowRange := True;
//  TT3MissilesOnVehicle(focused_weapon).ShowRangeSelected := FVisibleShowRange and
//      TT3MissilesOnVehicle(focused_weapon).ShowRange;
end;

procedure TfrmTacticalMissile.TargetAimPoint;
begin
  //frmTacticalDisplay.fmMapWindow1.Map.CurrentTool := mtAimpoint;
end;

procedure TfrmTacticalMissile.TargetTrack;
var
  r : TRecCmd_LaunchMissile;
  pfTarget : TT3PlatformInstance;
begin
  if not Assigned(clientManager.ControlledTrack) then
  begin
    UpdatePanelStatus('Vehicle Sender Not Found');
    Exit;
  end;

  if not Assigned(clientManager.SelectedTrack) then
  begin
    UpdatePanelStatus('Vehicle Target Not Found');
    Exit;
  end;

  if clientManager.SelectedTrack.TrackDomain <> 0 then
  begin
    UpdatePanelStatus('Target Domain Not Same With Missile');
    Exit;
  end;

  if clientManager.SelectedTrack.Equals(clientManager.ControlledTrack) then
  begin
    UpdatePanelStatus('Can''t Track Own Vehicle');
    Exit;
  end;

  pfTarget := simManager.FindT3PlatformByID(clientManager.SelectedTrack.ObjectInstanceIndex);

  if not Assigned(pfTarget) then
  begin
    UpdatePanelStatus('Vehicle Target Not Valid');
    Exit;
  end;

  if pfTarget is TT3Vehicle then
  begin
    with TT3Vehicle(pfTarget).VehicleDefinition do
      with FMountedMissile.MissileDefinition do
      begin
        if ((Platform_Domain = 0) and (Boolean(Anti_Air_Capable))) or
           ((Platform_Domain = 1) and (Boolean(Anti_Sur_Capable))) or
           ((Platform_Domain = 2) and (Boolean(Anti_SubSur_Capable))) or
           ((Platform_Domain = 3) and (Boolean(Anti_Land_Capable))) or
           ((Platform_Domain = vhdAmphibious) and (Boolean(Anti_Amphibious_Capable))) then

            { set target track }
            with FMountedMissile do
            begin
              r.MissileID        := InstanceIndex;
              r.MissileName      := InstanceName;
              r.Order            := CORD_ID_MissileTargetId;
              r.ParentPlatformID := clientManager.ControlledTrack.ObjectInstanceIndex;
              r.TargetPlatformID := clientManager.SelectedTrack.ObjectInstanceIndex;
              r.ValInteger       := clientManager.SelectedTrack.TrackNumber;

              clientManager.NetCmdSender.CmdMissile(r);
            end
        else
        begin
          UpdatePanelStatus('Target Domain Not Same With Missile');
          edtTacticalMissileTargetTrack.Text := '';
          Exit;
        end;
      end;
  end;


end;

procedure TfrmTacticalMissile.UpdateForm;
begin
  inherited;

  if Assigned(ControlledWeapon) and (ControlledWeapon is TT3MountedTacticalMissiles) then
  with (TT3MountedTacticalMissiles(ControlledWeapon)) do
  begin

    lblTacticalMissileStatusQuantity.Caption  := IntToStr(Quantity);

  end;
end;

end.
