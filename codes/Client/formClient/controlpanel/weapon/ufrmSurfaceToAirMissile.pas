unit ufrmSurfaceToAirMissile;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls, ufrmWeaponType, uT3MountedSurfaceToAirMissile;

type
  TfrmSurfaceToAirMissile = class(TfrmWeaponType)
    grpSurfaceToAirMissile: TGroupBox;
    scrlbx1: TScrollBox;
    lbl1: TLabel;
    bvl1: TBevel;
    lblSurfaceToAirStatus: TLabel;
    lbl2: TLabel;
    lblSurfaceToAirStatusQuantity: TLabel;
    lbl3: TLabel;
    bvl2: TBevel;
    lbl4: TLabel;
    lbl5: TLabel;
    bvl3: TBevel;
    lbl6: TLabel;
    lbl7: TLabel;
    lbl8: TLabel;
    lbl9: TLabel;
    lbl10: TLabel;
    lbl11: TLabel;
    lbl12: TLabel;
    lbl13: TLabel;
    lbl14: TLabel;
    bvl4: TBevel;
    lbl15: TLabel;
    lbl16: TLabel;
    lblSurfaceToAirCourse: TLabel;
    lblSurfaceToAirGround: TLabel;
    lblSurfaceToAirAltitude: TLabel;
    lblSurfaceToAirStatus1: TLabel;
    lblSurfaceToAirTimeToWait: TLabel;
    lblSurfaceToAirTimeToIntercept: TLabel;
    lbl17: TLabel;
    lbl18: TLabel;
    lbl19: TLabel;
    lbl20: TLabel;
    lbl21: TLabel;
    btnSurfaceToAirTargetTrack: TSpeedButton;
    btnSurfaceToAirDisplayRangeShow: TSpeedButton;
    btnSurfaceToAirDisplayRangeHide: TSpeedButton;
    btnSurfaceToAirDisplayBlindShow: TSpeedButton;
    btnSurfaceToAirDisplayBlindHide: TSpeedButton;
    edtSurfaceToAirSalvo: TEdit;
    edtSurfaceToAirTrack: TEdit;
    btnSurfaceToAirPlan: TButton;
    btnSurfaceToAirLaunch: TButton;
    btnSurfaceToAirCancel: TButton;
    procedure edtSurfaceToAirSalvoKeyPress(Sender: TObject; var Key: Char);
    procedure BtnSurfaceToAirClick(Sender: TObject);
  private
    { Private declarations }
    FMountedMissile    : TT3MountedSurfaceToAirMissile;
    function CheckedFunctionality : Boolean;
    procedure TargetTrack;
    procedure ShowRange;
    procedure HideRange;
    procedure ShowBlindZone;
    procedure HideBlindZone;
    procedure Plan;
    procedure Cancel;
    procedure Launch;
  public
    { Public declarations }
    procedure SetControlledWeapon(aObj : TObject); override;
    procedure UpdateForm; override;
  end;

var
  frmSurfaceToAirMissile: TfrmSurfaceToAirMissile;

implementation

uses
  uGameData_TTT, uT3ClientManager, tttData, uBaseCoordSystem,
  uGlobalVar, uT3PlatformInstance;
{$R *.dfm}

{ TfrmSurfaceToAirMissile }

procedure TfrmSurfaceToAirMissile.BtnSurfaceToAirClick(Sender: TObject);
begin
  if not CheckedFunctionality then
    Exit;

  if Sender.ClassType = TSpeedButton then
  begin
    case TSpeedButton(Sender).Tag of
      1 : ShowRange;
      2 : HideRange;
      3 : ShowBlindZone;
      4 : HideBlindZone;
      5 : TargetTrack;
    end;
  end
  else
  if Sender.ClassType = TButton then
  begin
    case TButton(Sender).Tag of
      1 : Plan;
      2 : Cancel;
      3 : Launch;
    end;
  end;
end;

procedure TfrmSurfaceToAirMissile.Cancel;
var
  rec : TRecCmd_LaunchMissile;
begin
  rec.MissileID        := FMountedMissile.InstanceIndex;
  rec.MissileName      := FMountedMissile.InstanceName;
  rec.Order            := CORD_ID_MissilePlan;
  rec.ValInteger       := 0;
  rec.ParentPlatformID := clientManager.ControlledTrack.ObjectInstanceIndex;
  clientManager.NetCmdSender.CmdMissile(rec);

//  TT3MissilesOnVehicle(focused_weapon).Planned := False;
//  btSurfaceToAirCancel.Enabled := false;
//  btSurfaceToAirLaunch.Enabled := false;
//  btSurfaceToAirPlan.Enabled   := true;
//
//  frmToteDisplay.ClearAllSurfaceToAirList(TT3PlatformInstance(FControlled));
//  lbSurfaceToAirStatus.Caption          := '---';
//  lbSurfaceToAirTimeToWait.Caption      := '---';
//  lbSurfaceToAirTimeToIntercept.Caption := '---';

end;

function TfrmSurfaceToAirMissile.CheckedFunctionality: Boolean;
begin
  Result := True;
  if ControlledWeapon = nil then
  begin
    UpdatePanelStatus('Not Found Selected Weapon');
    Result := False;
    Exit;
  end;

  if not Assigned(clientManager.ControlledTrack) then
  begin
    UpdatePanelStatus('Controlled Vehicle Not Valid');
    Result := False;
    Exit;
  end;

  if not(ControlledWeapon is TT3MountedSurfaceToAirMissile) then
  begin
    UpdatePanelStatus('Selected Weapon Is Not Missile');
    Result := False;
    Exit;
  end;

  if TT3MountedSurfaceToAirMissile(ControlledWeapon).WeaponStatus = wsDamaged then
  begin
    UpdatePanelStatus('Selected Weapon Is Damaged');
    Result := False;
    Exit;
  end;

  if clientManager.GameState <> gsPlaying then
  begin
    UpdatePanelStatus('Game Frozen');
    Result := False;
    Exit;
  end;
end;

procedure TfrmSurfaceToAirMissile.edtSurfaceToAirSalvoKeyPress(Sender: TObject;
  var Key: Char);
var
  ValKey    : set of AnsiChar;
  salvoSize : integer;
  rec       : TRecCmd_LaunchMissile;
begin
  inherited;

  if not CheckedFunctionality then
    Exit;

  ValKey := [#48 .. #57, #8, #13, #46];
  if not(CharInSet(Key, ValKey)) then
    Key := #0;

  if Key = #13 then
  begin

    if TryStrToInt(TEdit(Sender).Text, salvoSize) then
    begin

      if not(ControlledWeapon is TT3MountedSurfaceToAirMissile) then
        exit;

      if salvoSize > 0 then
      begin
        rec.MissileID   := FMountedMissile.InstanceIndex;
        rec.MissileName := FMountedMissile.InstanceName;
        rec.Order       := CORD_ID_MissileSalvo;
        rec.ValInteger  := salvoSize;
        rec.ParentPlatformID  := clientManager.ControlledTrack.ObjectInstanceIndex;
        clientManager.NetCmdSender.CmdMissile(rec);
      end else
        TEdit(Sender).Text := IntToStr(FMountedMissile.SalvoSize);
    end else
      TEdit(Sender).Text := IntToStr(FMountedMissile.SalvoSize);
  end;
end;

procedure TfrmSurfaceToAirMissile.HideBlindZone;
begin
  FMountedMissile.ShowBlindZone := False;

//  FVisibleShowBlind := False;
//  TT3MissilesOnVehicle(focused_weapon).ShowBlindSelected := FVisibleShowBlind and TT3MissilesOnVehicle(focused_weapon).ShowBlind;
//  HideBlindSensor;

end;

procedure TfrmSurfaceToAirMissile.HideRange;
begin
  if Assigned(FMountedMissile) then
  begin
    FMountedMissile.ShowRange := False;
    //FVisibleShowRange := False;
    //missile.ShowRange := False;
    //missile.ShowRangeSelected := FVisibleShowRange and missile.ShowRange;
  end;

end;

procedure TfrmSurfaceToAirMissile.Launch;
var
  rec : TRecCmd_LaunchMissile;
  pfTarget : TT3PlatformInstance;
begin
  pfTarget := simManager.FindT3PlatformByID(FMountedMissile.TargetInstanceIdx);

  if not Assigned(pfTarget) then
    Exit;

  with FMountedMissile do
  begin
    if InsideBlindZone(pfTarget) then
    begin
      rec.MissileID        := FMountedMissile.InstanceIndex;
      rec.MissileName      := FMountedMissile.InstanceName;
      rec.Order            := CORD_ID_MissilePlan;
      rec.ValInteger       := 0;
      rec.ParentPlatformID := clientManager.ControlledTrack.ObjectInstanceIndex;
      clientManager.NetCmdSender.CmdMissile(rec);
    end else
    begin
      rec.MissileID        := FMountedMissile.InstanceIndex;
      rec.MissileName      := FMountedMissile.InstanceName;
      rec.Order            := CORD_ID_MissileLaunch;
      rec.ParentPlatformID := clientManager.ControlledTrack.ObjectInstanceIndex;
      clientManager.NetCmdSender.CmdMissile(rec);
    end;
  end;

end;

procedure TfrmSurfaceToAirMissile.Plan;
var
  Range: Double;
  pfTarget : TT3PlatformInstance;
  pfCtrl : TT3PlatformInstance;
  rec : TRecCmd_LaunchMissile;
begin
  pfTarget := simManager.FindT3PlatformByID(FMountedMissile.TargetInstanceIdx);
  pfCtrl   := simManager.FindT3PlatformByID(TT3ClientManager(simManager).ControlledTrack.ObjectInstanceIndex);

  if not Assigned(pfTarget) then
    Exit;

  Range := CalcRange(
    pfCtrl.getPositionX, pfCtrl.getPositionY,
    pfTarget.getPositionX, pfTarget.getPositionY);

  if Range <= FMountedMissile.MissileDefinition.Min_Range then
  begin
    UpdatePanelStatus('Target is to close');
  end
  else
  if Range >= FMountedMissile.MissileDefinition.Max_Range then
  begin
    UpdatePanelStatus('Target is too far');
  end
  else
  if (Range > FMountedMissile.MissileDefinition.Min_Range) and
     (Range < FMountedMissile.MissileDefinition.Max_Range) then
  begin
    rec.MissileID        := FMountedMissile.InstanceIndex;
    rec.MissileName      := FMountedMissile.InstanceName;
    rec.Order            := CORD_ID_MissilePlan;
    rec.ValInteger       := 1;
    rec.ParentPlatformID := clientManager.ControlledTrack.ObjectInstanceIndex;
    clientManager.NetCmdSender.CmdMissile(rec);
  end;

//    btSurfaceToAirCancel.Enabled := true;
//    btSurfaceToAirLaunch.Enabled := true;
//    btSurfaceToAirPlan.Enabled   := false;
//
//    Missile.RangeRBLCircle := 5;
//
//    frmToteDisplay.OnWeaponEngaged(Missile, Missile.TargetObject, 0);
//
//    lbSurfaceToAirStatus.Caption := 'Engaged';
//    TimeEngaged := Missile.LaunchDelay;
//    lbSurfaceToAirTimeToWait.Caption := FormatDateTime('hh : mm : ss',
//      timeof(TimeEngaged / (3600*24)));
//    Distance := CalcRange(Missile.getPositionX, Missile.getPositionY,
//                    Missile.TargetObject.getPositionX, Missile.TargetObject.getPositionY);
//    TimeIntercept := (Distance/Missile.UnitMotion.FData.Max_Ground_Speed)*3600;
//    lbSurfaceToAirTimeToIntercept.Caption := FormatDateTime('hh : mm : ss',
//      timeof(TimeIntercept / (3600*24)));
end;

procedure TfrmSurfaceToAirMissile.SetControlledWeapon(aObj: TObject);
begin
  inherited;

  if Assigned(ControlledWeapon) and (ControlledWeapon is TT3MountedSurfaceToAirMissile) then
  with (TT3MountedSurfaceToAirMissile(ControlledWeapon)) do
  begin
    FMountedMissile := TT3MountedSurfaceToAirMissile(ControlledWeapon);

    edtSurfaceToAirSalvo.Text  := IntToStr(SalvoSize);
    edtSurfaceToAirTrack.Text  := IntToStr(TargetTrackNumber);

  end;
end;

procedure TfrmSurfaceToAirMissile.ShowBlindZone;
begin
  FMountedMissile.ShowBlindZone := true;

//  FVisibleShowBlind := True;
//  FMountedMissile.ShowBlindSelected := FVisibleShowBlind and FMountedMissile.ShowBlind;
//  HideBlindSensor;
end;

procedure TfrmSurfaceToAirMissile.ShowRange;
begin
  if Assigned(FMountedMissile) then
  begin
    FMountedMissile.ShowRange := True;
//  FVisibleShowRange := True;
//  Fmissile.ShowRangeSelected := FVisibleShowRange and missile.ShowRange;
//  HideRangeSensor;
  end;
end;

procedure TfrmSurfaceToAirMissile.TargetTrack;
var
  r : TRecCmd_LaunchMissile;
begin
  if not Assigned(clientManager.ControlledTrack) then
  begin
    UpdatePanelStatus('Vehicle Sender Not Found');
    Exit;
  end;

  if not Assigned(clientManager.SelectedTrack) then
  begin
    UpdatePanelStatus('Vehicle Target Not Found');
    Exit;
  end;

  if clientManager.SelectedTrack.TrackDomain <> 0 then
  begin
    UpdatePanelStatus('Target Domain Not Same With Missile');
    Exit;
  end;

  if clientManager.SelectedTrack.Equals(clientManager.ControlledTrack) then
  begin
    UpdatePanelStatus('Can''t Track Own Vehicle');
    Exit;
  end;

  { set target track }
  with FMountedMissile do
  begin
    r.MissileID        := InstanceIndex;
    r.MissileName      := InstanceName;
    r.Order            := CORD_ID_MissileTargetId;
    r.ParentPlatformID := clientManager.ControlledTrack.ObjectInstanceIndex;
    r.TargetPlatformID := clientManager.SelectedTrack.ObjectInstanceIndex;
    r.ValInteger       := clientManager.SelectedTrack.TrackNumber;

    clientManager.NetCmdSender.CmdMissile(r);
  end;

//  lbSurfaceToAirCourse.Caption  := FormatCourse(TT3Vehicle(focused_platform).Course);
//  lbSurfaceToAirGround.Caption  := FormatSpeed(TT3Vehicle(focused_platform).Speed);
//  lbSurfaceToAirAltitude.Caption := FormatAltitudeTrack(TT3Vehicle(focused_platform).Altitude);
//  btSurfaceToAirPlan.Enabled := True;
end;

procedure TfrmSurfaceToAirMissile.UpdateForm;
begin
  inherited;

  with FMountedMissile do begin



  end;

end;

end.
