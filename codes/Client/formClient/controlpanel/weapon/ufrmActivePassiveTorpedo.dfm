object frmActivePassiveTorpedo: TfrmActivePassiveTorpedo
  Left = 0
  Top = 0
  Caption = 'frmActivePassiveTorpedo'
  ClientHeight = 408
  ClientWidth = 348
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object grpActivePasiveTorpedo: TGroupBox
    Left = 0
    Top = 0
    Width = 348
    Height = 408
    Align = alClient
    TabOrder = 0
    object scrlbx1: TScrollBox
      Left = 2
      Top = 15
      Width = 344
      Height = 391
      Align = alClient
      BevelInner = bvNone
      BevelOuter = bvNone
      BorderStyle = bsNone
      TabOrder = 0
      object btnHideBlindZoneAPG: TSpeedButton
        Tag = 4
        Left = 146
        Top = 310
        Width = 80
        Height = 23
        AllowAllUp = True
        GroupIndex = 1
        Down = True
        Caption = 'Hide'
        OnClick = OnBtnAPClick
      end
      object btnHideRangeAPG: TSpeedButton
        Tag = 2
        Left = 29
        Top = 310
        Width = 80
        Height = 23
        AllowAllUp = True
        GroupIndex = 2
        Down = True
        Caption = 'Hide'
        OnClick = OnBtnAPClick
      end
      object btnShowBlindZoneAPG: TSpeedButton
        Tag = 3
        Left = 146
        Top = 286
        Width = 80
        Height = 23
        AllowAllUp = True
        GroupIndex = 1
        Caption = 'Show'
        OnClick = OnBtnAPClick
      end
      object btnShowRangeAPG: TSpeedButton
        Tag = 1
        Left = 29
        Top = 285
        Width = 80
        Height = 23
        AllowAllUp = True
        GroupIndex = 2
        Caption = 'Show'
        OnClick = OnBtnAPClick
      end
      object btnTargetTrackAPG: TSpeedButton
        Tag = 5
        Left = 172
        Top = 68
        Width = 23
        Height = 22
        Glyph.Data = {
          36090000424D360900000000000036000000280000001F000000180000000100
          18000000000000090000000000000000000000000000000000006161613E3E3E
          3737374040403B3B3B3A3A3A4141414141413A3A3A3A3A3A4040403737373E3E
          3E3D3D3D3838384141413A3A3A3B3B3B4040403737373E3E3E3D3D3D38383841
          41413A3A3A3B3B3B4040403737373E3E3E3C3C3C383838000000B2B2B27C7C7C
          7474747F7F7F7878787777778080808080807878787878788080807474747C7C
          7C7B7B7B7474748080807A7A7A7A7A7A7F7F7F7474747C7C7C7B7B7B74747480
          80807777777979797F7F7F7474747D7D7D7A7A7A757575000000D0D0D0B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2BDBDBDBBBBBBB2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000D0D0D0B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2C0C0C04949
          491111111111111111111212121212121A1A1AB2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B4B4B4000000D0D0D0B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B29191914D4D4D5454545F5F
          5F6565656262620000004747476565656363634D4D4D4D4D4D797979BABABAB2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B3B3B3000000D0D0D0B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B29797976969692929292929298686
          86B2B2B2ADADAD0000007D7D7DB2B2B2ABABAB292929292929545454979797B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000D0D0D0B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B23D3D3D3B3B3BB2B2B2B2B2B2B2B2
          B2B2B2B2ADADAD0000007D7D7DB2B2B2B2B2B2B2B2B2B2B2B26A6A6A0E0E0EB2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000D0D0D0B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B21616166F6F6FA5A5A5B2B2B2B2B2B2B2B2
          B2B2B2B2ADADAD0000007D7D7DB2B2B2B2B2B2B2B2B2B2B2B2AAAAAA94949413
          13138A8A8AB2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000D0D0D0B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2030303757575B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2ADADAD0000007D7D7DB2B2B2B2B2B2B2B2B2B2B2B2B2B2B2A4A4A400
          0000868686B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B3B3B3000000D0D0D0B2B2B2
          B2B2B2B2B2B2B2B2B2A6A6A68B8B8B2C2C2C838383B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2ADADAD0000007F7F7FB4B4B4B2B2B2B2B2B2B2B2B2B2B2B2A7A7A727
          27277272729C9C9CB2B2B2B2B2B2B2B2B2B2B2B2B3B3B3000000D0D0D0B2B2B2
          B2B2B2B2B2B2B2B2B27B7B7B000000BDBDBDB7B7B7B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2ADADAD000000888888BBBBBBB2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B22C2C2C4C4C4CB2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000D0D0D0B2B2B2
          B2B2B2B2B2B2B2B2B27B7B7B000000B0B0B0B3B3B3B2B2B2B2B2B2BFBFBFB6B6
          B6B2B2B2B1B1B19D9D9DADADADB3B3B3B2B2B2B2B2B2B2B2B2B2B2B2B3B3B3BF
          BFBF3030304C4C4CB2B2B2B2B2B2B2B2B2B2B2B2B4B4B4000000D0D0D0B2B2B2
          B2B2B2B2B2B2B2B2B27B7B7B0000004F4F4F5151515151515151515858585353
          53737373B2B2B2B2B2B2B2B2B28D8D8D51515151515151515151515152525258
          58581616164C4C4CB2B2B2B2B2B2B2B2B2B2B2B2B3B3B3000000D0D0D0B2B2B2
          B2B2B2B2B2B2B2B2B27B7B7B0000002424242525252525252525252525252525
          25575757B2B2B2B2B2B2B2B2B27D7D7D25252525252525252525252525252525
          25250909094C4C4CB2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000D0D0D0B2B2B2
          B2B2B2B2B2B2B2B2B27B7B7B000000AFAFAFB2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B22C2C2C4C4C4CB2B2B2B2B2B2B2B2B2B2B2B2B4B4B4000000D0D0D0B2B2B2
          B2B2B2B2B2B2B2B2B27B7B7B000000BBBBBBB6B6B6B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2ADADAD171717848484B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B22C2C2C4C4C4CB2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000D0D0D0B2B2B2
          B2B2B2B2B2B2B2B2B29999995F5F5F5A5A5A949494B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2ADADAD0000007D7D7DB2B2B2B2B2B2B2B2B2B2B2B2B5B5B5B2B2B253
          53535C5C5C828282B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000D0D0D0B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2030303757575B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2ADADAD0000007D7D7DB2B2B2B2B2B2B2B2B2B2B2B2B7B7B7AFAFAF00
          0000868686B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000D0D0D0B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2030303757575B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2ADADAD0000007D7D7DB2B2B2B2B2B2B2B2B2B2B2B2B2B2B2A4A4A400
          0000868686B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000D0D0D0B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B29A9A9A4545454B4B4BB2B2B2B2B2B2B2B2
          B2B2B2B2ADADAD0000007D7D7DB2B2B2B2B2B2B2B2B2B2B2B274747423232399
          9999ACACACB2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B4B4B4000000D0D0D0B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B27A7A7A5A5A5A5555555555559494
          94B5B5B5B4B4B4000000838383B7B7B7ADADAD555555555555585858646464B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B3B3B3000000D0D0D0B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B28282822121212121216D6D
          6D9595959898980000006E6E6E9898988B8B8B2121212121215B5B5BB2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000D0D0D0B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B23939
          39000000000000000000000000000000090909B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B4B4B4000000D0D0D0B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2A0A0
          A0979797979797979797979797979797999999B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000}
        OnClick = OnBtnAPClick
      end
      object bvl1: TBevel
        Left = 40
        Top = 16
        Width = 278
        Height = 3
      end
      object bvl2: TBevel
        Left = 41
        Top = 57
        Width = 278
        Height = 3
      end
      object bvl3: TBevel
        Left = 42
        Top = 100
        Width = 278
        Height = 3
      end
      object bvl4: TBevel
        Left = 45
        Top = 262
        Width = 190
        Height = 2
      end
      object lbl1: TLabel
        Left = 3
        Top = 9
        Width = 31
        Height = 13
        Caption = 'Status'
      end
      object lbl12: TLabel
        Left = 23
        Top = 151
        Width = 75
        Height = 13
        Caption = 'Search Radius :'
      end
      object lbl13: TLabel
        Left = 23
        Top = 177
        Width = 72
        Height = 13
        Caption = 'Search Depth :'
      end
      object lbl14: TLabel
        Left = 23
        Top = 203
        Width = 73
        Height = 13
        Caption = 'Safety Ceiling :'
      end
      object lbl15: TLabel
        Left = 23
        Top = 231
        Width = 74
        Height = 13
        Caption = 'Seeker Range :'
      end
      object lbl17: TLabel
        Left = 171
        Top = 151
        Width = 27
        Height = 13
        Caption = 'yards'
      end
      object lbl18: TLabel
        Left = 171
        Top = 177
        Width = 33
        Height = 13
        Caption = 'metres'
      end
      object lbl19: TLabel
        Left = 171
        Top = 203
        Width = 33
        Height = 13
        Caption = 'meters'
      end
      object lbl20: TLabel
        Left = 171
        Top = 231
        Width = 27
        Height = 13
        Caption = 'yards'
      end
      object lbl21: TLabel
        Left = 50
        Top = 270
        Width = 31
        Height = 13
        Caption = 'Range'
      end
      object lbl22: TLabel
        Left = 163
        Top = 270
        Width = 54
        Height = 13
        Caption = 'Blind Zones'
      end
      object lbl3: TLabel
        Left = 95
        Top = 28
        Width = 49
        Height = 13
        Caption = 'Quantity :'
      end
      object lbl5: TLabel
        Left = 3
        Top = 50
        Width = 32
        Height = 13
        Caption = 'Target'
      end
      object lbl6: TLabel
        Left = 26
        Top = 74
        Width = 33
        Height = 13
        Caption = 'Track :'
      end
      object lbl7: TLabel
        Left = 5
        Top = 93
        Width = 35
        Height = 13
        Caption = 'Control'
      end
      object lbl8: TLabel
        Left = 5
        Top = 254
        Width = 34
        Height = 13
        Caption = 'Display'
      end
      object lbl9: TLabel
        Left = 25
        Top = 116
        Width = 55
        Height = 13
        Caption = 'Salvo Size :'
      end
      object lblQuantityAPG: TLabel
        Left = 151
        Top = 28
        Width = 12
        Height = 13
        Caption = '---'
      end
      object lblStatusAPG: TLabel
        Left = 25
        Top = 28
        Width = 43
        Height = 13
        Caption = 'Available'
      end
      object btn4: TButton
        Tag = 1
        Left = 235
        Top = 149
        Width = 80
        Height = 23
        Caption = '< Default'
        TabOrder = 0
        OnClick = OnBtnAPClick
      end
      object btn5: TButton
        Tag = 2
        Left = 235
        Top = 175
        Width = 80
        Height = 23
        Caption = '< Default'
        TabOrder = 1
        OnClick = OnBtnAPClick
      end
      object btn6: TButton
        Tag = 3
        Left = 235
        Top = 201
        Width = 80
        Height = 23
        Caption = '< Default'
        TabOrder = 2
        OnClick = OnBtnAPClick
      end
      object btn7: TButton
        Tag = 4
        Left = 235
        Top = 230
        Width = 80
        Height = 23
        Caption = '< Default'
        TabOrder = 3
        OnClick = OnBtnAPClick
      end
      object btnLaunchAP: TButton
        Tag = 5
        Left = 235
        Top = 346
        Width = 80
        Height = 25
        Caption = 'Launch'
        TabOrder = 4
        OnClick = OnBtnAPClick
      end
      object edtEdtAPTargetTrack: TEdit
        Left = 113
        Top = 68
        Width = 50
        Height = 21
        Enabled = False
        TabOrder = 5
        Text = '---'
      end
      object edtEdtSafetyCeilingAP: TEdit
        Tag = 4
        Left = 113
        Top = 201
        Width = 50
        Height = 21
        TabOrder = 6
      end
      object edtEdtSalvoAP: TEdit
        Tag = 1
        Left = 113
        Top = 112
        Width = 50
        Height = 21
        TabOrder = 7
      end
      object edtEdtSearchDepthAP: TEdit
        Tag = 3
        Left = 113
        Top = 175
        Width = 50
        Height = 21
        TabOrder = 8
      end
      object edtEdtSearchRadiusAP: TEdit
        Tag = 2
        Left = 113
        Top = 148
        Width = 50
        Height = 21
        TabOrder = 9
      end
      object edtEdtSeekerRangeAP: TEdit
        Tag = 5
        Left = 113
        Top = 228
        Width = 50
        Height = 21
        TabOrder = 10
      end
    end
  end
end
