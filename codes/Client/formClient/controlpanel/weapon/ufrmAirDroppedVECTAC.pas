unit ufrmAirDroppedVECTAC;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, Buttons, ufrmWeaponType;

type
  TfrmAirDroppedVECTAC = class(TfrmWeaponType)
    grpAirDroppedVECTAC: TGroupBox;
    scrlbx1: TScrollBox;
    bvl1: TBevel;
    bvl2: TBevel;
    bvl3: TBevel;
    btnVectacTargetTrack: TSpeedButton;
    btnVectacWeaponCarrierName: TSpeedButton;
    btnVectacWeaponName: TSpeedButton;
    lbl1: TLabel;
    lbl2: TLabel;
    lbl3: TLabel;
    lbl4: TLabel;
    lbl5: TLabel;
    lbl6: TLabel;
    lbl7: TLabel;
    lbl8: TLabel;
    lbl9: TLabel;
    lbl10: TLabel;
    lbl11: TLabel;
    lbl12: TLabel;
    lbl13: TLabel;
    lbl14: TLabel;
    lbl15: TLabel;
    lbl16: TLabel;
    lbl17: TLabel;
    lbl18: TLabel;
    lbl19: TLabel;
    lbl20: TLabel;
    lbl21: TLabel;
    lbl22: TLabel;
    lbl23: TLabel;
    lbl24: TLabel;
    lbl25: TLabel;
    lblVectacTargetCourse: TLabel;
    lblVectacTargetDepth: TLabel;
    lblVectacTargetDoppler: TLabel;
    lblVectacTargetGround: TLabel;
    lblVectacTargetIdentity: TLabel;
    lblVectacTargetPropulsion: TLabel;
    lblVectacWeaponCarrierAdviced: TLabel;
    lblVectacWeaponCarrierTime: TLabel;
    lblVectacWeaponExpiry: TLabel;
    btnVectacCancel: TButton;
    btnVectacConfirm: TButton;
    btnVectacPlan: TButton;
    btnVectacWeaponCarrierDrop: TButton;
    btnVectacWeaponCarrierGround: TButton;
    edtVectacTargetTrack: TEdit;
    edtVectacWeaponCarrierDrop: TEdit;
    edtVectacWeaponCarrierGround: TEdit;
    edtVectacWeaponCarrierName: TEdit;
    edtVectacWeaponName: TEdit;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmAirDroppedVECTAC: TfrmAirDroppedVECTAC;

implementation

{$R *.dfm}

end.
