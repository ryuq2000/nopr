unit ufrmWeaponType;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs;

type
  TfrmWeaponType = class(TForm)
  private
    FOnUpdatePanelStatus: TGetStrProc;
    procedure SetOnUpdatePanelStatus(const Value: TGetStrProc);
    { Private declarations }
  protected
    FControlledWeapon : TObject;
    procedure UpdatePanelStatus(Value : String);
  public
    { Public declarations }
    procedure UpdateForm; virtual;
    procedure SetControlledWeapon(aObj : TObject); virtual;

    property ControlledWeapon : TObject read FControlledWeapon;
    property OnUpdatePanelStatus : TGetStrProc read FOnUpdatePanelStatus write SetOnUpdatePanelStatus;
  end;

var
  frmWeaponType: TfrmWeaponType;

implementation

{$R *.dfm}

{ TfrmWeaponType }

procedure TfrmWeaponType.SetControlledWeapon(aObj: TObject);
begin
  FControlledWeapon := aObj;
  UpdateForm;
end;

procedure TfrmWeaponType.SetOnUpdatePanelStatus(const Value: TGetStrProc);
begin
  FOnUpdatePanelStatus := Value;
end;

procedure TfrmWeaponType.UpdateForm;
begin

end;

procedure TfrmWeaponType.UpdatePanelStatus(Value: String);
begin
  if Assigned(FOnUpdatePanelStatus) then
    FOnUpdatePanelStatus(Value);
end;

end.
