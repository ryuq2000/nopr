unit ufrmStraightRunTorpedo;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, Buttons,ufrmWeaponType;

type
  TfrmStraigthRunTorpedo = class(TfrmWeaponType)
    grpStraightRunningTorpedos: TGroupBox;
    scrlbx1: TScrollBox;
    btnStraightRunningTorpedosDisplayBlindHide: TSpeedButton;
    btnStraightRunningTorpedosDisplayBlindShow: TSpeedButton;
    btnStraightRunningTorpedosDisplayRangeHide: TSpeedButton;
    btnStraightRunningTorpedosDisplayRangeShow: TSpeedButton;
    btnStraightRunningTorpedosTargetTrack: TSpeedButton;
    bvl14: TBevel;
    bvl15: TBevel;
    bvl16: TBevel;
    lbl62: TLabel;
    lbl63: TLabel;
    lbl64: TLabel;
    lbl65: TLabel;
    lbl66: TLabel;
    lbl67: TLabel;
    lbl68: TLabel;
    lbl69: TLabel;
    lbl70: TLabel;
    lbl71: TLabel;
    lbl72: TLabel;
    lbl73: TLabel;
    lbl74: TLabel;
    lbl75: TLabel;
    lblSRQuantity: TLabel;
    lblStatusStraightRunningTorpedos: TLabel;
    lblStraightRunningTorpedosTargetCourse: TLabel;
    lblStraightRunningTorpedosTargetDepth: TLabel;
    lblStraightRunningTorpedosTargetGround: TLabel;
    lblStraightRunningTorpedosTargetIdentity: TLabel;
    btnLaunchSR: TButton;
    edtEdtSRTargetTrack: TEdit;
    procedure OnBtnClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmStraigthRunTorpedo: TfrmStraigthRunTorpedo;

implementation

uses
  uGameData_TTT, uT3MountedStraigthTorpedo;

{$R *.dfm}

procedure TfrmStraigthRunTorpedo.OnBtnClick(Sender: TObject);
begin
  inherited;

  if Sender is TSpeedButton then
  with TT3MountedStraigthTorpedo(FControlledWeapon) do begin
    case TSpeedButton(Sender).Tag of
      1 : begin
            //FVisibleShowRange := True;
            ShowRange         := True;
            //ShowRangeSelected := (ShowRange and FVisibleShowRange);
            //HideRangeSensor;
          end;
      2 : begin
            //FVisibleShowRange := False;
            ShowRange         := False;
            //ShowRangeSelected := (ShowRange and FVisibleShowRange);
          end;
      3 : begin
            //FVisibleShowBlind := True;
            ShowBlindZone     := True;
            //ShowBlindSelected := (ShowBlind and FVisibleShowBlind);
            //HideBlindSensor;
          end;
      4 : begin
            //FVisibleShowBlind := False;
            ShowBlindZone     := False;
            //ShowBlindSelected := (ShowBlind and FVisibleShowBlind);
          end;
      5 : begin

      end;
    end;
  end;
  if Sender is TButton then
  begin

  end;


            {Ngecek Validasi target}
//            if Assigned(focused_platform) then begin
//              if not TorpedoTargetCheck(focused_platform,focused_weapon) then
//              begin
//                frmTacticalDisplay.addStatus('Invalid target domain');
//                exit;
//              end;

//              {Kalo yg di track Detected track}
//              if focused_platform is TT3DetectedTrack then
//              begin
//                strTargetTrackId := TT3PlatformInstance(TT3DetectedTrack(focused_platform).TrackObject).Track_ID;
//                strTargetCourse := FormatCourse(TT3PlatformInstance(TT3DetectedTrack(focused_platform).TrackObject).Course);
//                strTargetGroundSpeed := FormatSpeed(TT3PlatformInstance(TT3DetectedTrack(focused_platform).TrackObject).Speed);
//                strTargetAltitude := FormatSpeed(TT3PlatformInstance(TT3DetectedTrack(focused_platform).TrackObject).Altitude);
//                IntTargetPlatformID := TT3PlatformInstance(TT3DetectedTrack(focused_platform).TrackObject).InstanceIndex;
//
//                target := simMgrClient.FindT3PlatformByID(IntTargetPlatformID);
//              end
//              {Kalo yg di track Non Real Time}
//              else if focused_platform is TT3NonRealVehicle then
//              begin
//                strTargetTrackId := IntToStr(TT3PlatformInstance(focused_platform).InstanceIndex);
//                strTargetCourse := FormatCourse(TT3PlatformInstance(focused_platform).Course);
//                strTargetGroundSpeed := FormatSpeed(TT3PlatformInstance(focused_platform).Speed);
//                strTargetAltitude := FormatSpeed(TT3PlatformInstance(focused_platform).Altitude);
//                IntTargetPlatformID := TT3PlatformInstance(focused_platform).InstanceIndex;
//
//                target := simMgrClient.FindNonRealPlatformByID(IntTargetPlatformID);
//              end
//              {Kalo yg di track Platform Instance}
//              else
//              begin
//                strTargetTrackId := TT3PlatformInstance(focused_platform).Track_ID;
//                strTargetCourse := FormatCourse(TT3PlatformInstance(focused_platform).Course);
//                strTargetGroundSpeed := FormatSpeed(TT3PlatformInstance(focused_platform).Speed);
//                strTargetAltitude := FormatSpeed(TT3PlatformInstance(focused_platform).Altitude);
//                IntTargetPlatformID := TT3PlatformInstance(focused_platform).InstanceIndex;
//
//                target := simMgrClient.FindT3PlatformByID(IntTargetPlatformID);
//              end;
//            end
//            else
//            begin
//              frmTacticalDisplay.addStatus('Target platform not defined');
//              exit;
//            end;

//            if not Assigned(target) then
//              exit;
//
//            if Assigned(simMgrClient.findDetectedTrack(target)) then
//               strTargetTrackId := FormatTrackNumber(TT3DetectedTrack(simMgrClient.findDetectedTrack(target)).TrackNumber)
//            else
//            begin
//              if TT3PlatformInstance(target).TrackNumber = 0 then
//                strTargetTrackId := strTargetTrackId
//              else
//                strTargetTrackId := FormatTrackNumber(TT3PlatformInstance(target).TrackNumber);
//            end;
//
//            if simMgrClient.ISInstructor then
//              intNoCubicle := 0
//            else
//              intNoCubicle := simMgrClient.FMyCubGroup.FData.Group_Index;
//
//            if (strParentTrackId <> strTargetTrackId)  and (strParentTrackNumber <> strTargetTrackId) then
//            begin
//              TargetObject              := focused_platform;
//
//              {Paket Synch Panel Weapon Ketika Track Target}
//              rPanel.NoCubicle := intNoCubicle;
//              rPanel.ParentPlatformID := intParentPlatformID;
//              rPanel.TargetPlatformID := IntTargetPlatformID;
//              rPanel.TorpType := 5;
//              rPanel.WeaponIndex := InstanceIndex;
//              rPanel.WeaponName := InstanceName;
//              rPanel.TargetTrack := strTargetTrackId;
//              rPanel.Param1 := '---';
//              rPanel.Param2 := strTargetCourse;
//              rPanel.Param3 := strTargetGroundSpeed;
//              rPanel.Param4 := strTargetAltitude;
//              rPanel.ButtonLaunch := True;
//
//              SimMgrClient.netSend_CmdTorpedoSyncPanelWeapon(rPanel);
//              Sleep(100);
//              {----------------------------------------------------------------}
//            end
//            else
//            begin
//              frmTacticalDisplay.addStatus('Cannot target own platform');
//              exit;
//            end;
//          end;
//    end;
//  end;
//
//  if Sender is TButton then
//  begin
//    with TT3TorpedoesOnVehicle(focused_weapon) do
//    begin
//      if (EdtSRTargetTrack.Text = '') or (EdtSRTargetTrack.Text = '---') then
//        Exit;
//
//      focusTarget := simMgrClient.FindT3PlatformByID(TT3TorpedoesOnVehicle(focused_weapon).TargetPlatformID);
//      if not Assigned(focusTarget) then
//        focusTarget := simMgrClient.FindNonRealPlatformByID(TT3TorpedoesOnVehicle(focused_weapon).TargetPlatformID);
//
//      if (focusTarget = nil) or (EdtSRTargetTrack.Text = '')then begin
//        frmTacticalDisplay.addStatus('Target is not selected!');
//        exit;
//      end;
//
//      EdtSRTargetTrack.Text := '';
//      intbatasSR := Quantity;
//
//      if (TorpedoPrelaunchCheck(focusTarget,focused_weapon, 5)) and (intbatasSR > 0) then
//      begin
//        {Paket Launch Torpedo}
//        rMis.ParentPlatformID := ParentPlatformID;
//        rMis.TargetPlatformID := TargetPlatformID;
//        rMis.ProjectileInstanceIndex := 0;
//        rMis.NoCubicle := NoCubicle;
//        rMis.MissileID := InstanceIndex;
//        SimMgrClient.netSend_CmdLaunch_Torpedo(rMis);
//        Sleep(100);
//        {------------------------------------------------------------------}
//
//        {Paket Synch Panel Weapon Setelah Ditembakkan}
//        rPanel.NoCubicle := 0;
//        rPanel.ParentPlatformID := ParentPlatformID;
//        rPanel.TargetPlatformID := 0;
//        rPanel.TorpType := 5;
//        rPanel.WeaponIndex := InstanceIndex;
//        rPanel.WeaponName := InstanceName;
//        rPanel.TargetTrack := '';
//        rPanel.Param1 := '---';
//        rPanel.Param2 := '---';
//        rPanel.Param3 := '---';
//        rPanel.Param4 := '---';
//        rPanel.ButtonLaunch := False;
//
//        SimMgrClient.netSend_CmdTorpedoSyncPanelWeapon(rPanel);
//        Sleep(100);
//        {----------------------------------------------------------------}
//
//        SimMgrClient.netSend_CmdSetQuantity(ParentPlatformID, InstanceIndex, CORD_ID_QUANTITY, CORD_TYPE_WEAPON, (Quantity - 1));
//        Sleep(100);
//
//        lblSRQuantity.Caption := IntToStr(Quantity-1);
//      end
//      else begin
//        if intbatasSR <= 0 then
//        ShowMessage('Cek Quantity');
//      end;
//    end;
end;

end.
