unit ufrmAirDroppedTorpedo;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, Buttons, ufrmWeaponType;

type
  TfrmAirDroppedTorpedo = class(TfrmWeaponType)
    grpAirDroppedTorpedo: TGroupBox;
    scrlbx1: TScrollBox;
    btnAirDropeedBilndZonesHide: TSpeedButton;
    btnAirDroppesDisplayBilndZonesShow: TSpeedButton;
    btnAirDroppesDisplayRangeHide: TSpeedButton;
    btnAirDroppesDisplayRangeShow: TSpeedButton;
    btnAirDroppesTargetTrack: TSpeedButton;
    bvl10: TBevel;
    bvl11: TBevel;
    bvl12: TBevel;
    bvl13: TBevel;
    bvl9: TBevel;
    lbl38: TLabel;
    lbl39: TLabel;
    lbl40: TLabel;
    lbl41: TLabel;
    lbl42: TLabel;
    lbl43: TLabel;
    lbl44: TLabel;
    lbl45: TLabel;
    lbl46: TLabel;
    lbl47: TLabel;
    lbl48: TLabel;
    lbl49: TLabel;
    lbl50: TLabel;
    lbl51: TLabel;
    lbl52: TLabel;
    lbl53: TLabel;
    lbl54: TLabel;
    lbl55: TLabel;
    lbl56: TLabel;
    lbl57: TLabel;
    lbl58: TLabel;
    lbl59: TLabel;
    lbl60: TLabel;
    lbl61: TLabel;
    lbl85: TLabel;
    lblADQuantity: TLabel;
    lblAirDproppedTargetCourse: TLabel;
    lblAirDproppedTargetDepth: TLabel;
    lblAirDproppedTargetForce: TLabel;
    lblAirDproppedTargetGroundSpeed: TLabel;
    lblAirDroppedStatus: TLabel;
    btnDefaultAirDroppedControlSearchCeiling: TButton;
    btnDefaultAirDroppedSearchDepth: TButton;
    btnLauchAD: TButton;
    chkAirDroppedLaunchWhithoutTarget: TCheckBox;
    chkAirDroppedUseLaunchPlatformHeading: TCheckBox;
    edtEdtADTargetTrack: TEdit;
    edtEdtLaunchBearingAD: TEdit;
    edtEdtSearchCeilingAD: TEdit;
    edtEdtSearchDepthAD: TEdit;
    edtEdtSearchRadiusAD: TEdit;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmAirDroppedTorpedo: TfrmAirDroppedTorpedo;

implementation

{$R *.dfm}

end.
