object frmHybridMissile: TfrmHybridMissile
  Left = 0
  Top = 0
  Caption = 'frmHybridMissile'
  ClientHeight = 449
  ClientWidth = 328
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object grpHybridMissile: TGroupBox
    Left = 0
    Top = 0
    Width = 328
    Height = 449
    Align = alClient
    TabOrder = 0
    object scrlbx1: TScrollBox
      Left = 2
      Top = 15
      Width = 324
      Height = 432
      Align = alClient
      BorderStyle = bsNone
      TabOrder = 0
      object btnHybridMissileDisplayBlindZonesHide: TSpeedButton
        Tag = 6
        Left = 187
        Top = 346
        Width = 80
        Height = 25
        AllowAllUp = True
        GroupIndex = 1
        Down = True
        Caption = 'Hide'
        OnClick = OnHybridMissileClick
      end
      object btnHybridMissileDisplayBlindZonesShow: TSpeedButton
        Tag = 5
        Left = 187
        Top = 322
        Width = 80
        Height = 25
        AllowAllUp = True
        GroupIndex = 1
        Caption = 'Show'
        OnClick = OnHybridMissileClick
      end
      object lbl87: TLabel
        Left = 198
        Top = 306
        Width = 54
        Height = 13
        Caption = 'Blind Zones'
      end
      object lbl88: TLabel
        Left = 45
        Top = 306
        Width = 31
        Height = 13
        Caption = 'Range'
      end
      object btnHybridMissileDisplayRangeShow: TSpeedButton
        Tag = 3
        Left = 22
        Top = 322
        Width = 80
        Height = 25
        AllowAllUp = True
        GroupIndex = 2
        Caption = 'Show'
        OnClick = OnHybridMissileClick
      end
      object btnHybridMissileDisplayRangeHide: TSpeedButton
        Tag = 4
        Left = 22
        Top = 346
        Width = 80
        Height = 25
        AllowAllUp = True
        GroupIndex = 2
        Down = True
        Caption = 'Hide'
        OnClick = OnHybridMissileClick
      end
      object lbl89: TLabel
        Left = 9
        Top = 290
        Width = 34
        Height = 13
        Caption = 'Display'
      end
      object bvl21: TBevel
        Left = 46
        Top = 298
        Width = 190
        Height = 2
      end
      object lbl90: TLabel
        Left = 22
        Top = 257
        Width = 74
        Height = 13
        Caption = 'Seeker Range :'
      end
      object lbl91: TLabel
        Left = 22
        Top = 229
        Width = 77
        Height = 13
        Caption = 'Cruise Altitude :'
      end
      object lbl92: TLabel
        Left = 22
        Top = 199
        Width = 55
        Height = 13
        Caption = 'Salvo Size :'
      end
      object lbl93: TLabel
        Left = 9
        Top = 173
        Width = 35
        Height = 13
        Caption = 'Control'
      end
      object bvl22: TBevel
        Left = 50
        Top = 180
        Width = 190
        Height = 2
      end
      object lbl94: TLabel
        Left = 177
        Top = 229
        Width = 20
        Height = 13
        Caption = 'feet'
      end
      object lbl95: TLabel
        Left = 177
        Top = 257
        Width = 14
        Height = 13
        Caption = 'nm'
      end
      object lbl96: TLabel
        Left = 179
        Top = 138
        Width = 48
        Height = 13
        Caption = 'degrees T'
      end
      object btnSearchHybridMissileTargetTrack: TSpeedButton
        Tag = 1
        Left = 179
        Top = 76
        Width = 23
        Height = 22
        Enabled = False
        Glyph.Data = {
          36090000424D360900000000000036000000280000001F000000180000000100
          18000000000000090000000000000000000000000000000000006161613E3E3E
          3737374040403B3B3B3A3A3A4141414141413A3A3A3A3A3A4040403737373E3E
          3E3D3D3D3838384141413A3A3A3B3B3B4040403737373E3E3E3D3D3D38383841
          41413A3A3A3B3B3B4040403737373E3E3E3C3C3C383838000000B2B2B27C7C7C
          7474747F7F7F7878787777778080808080807878787878788080807474747C7C
          7C7B7B7B7474748080807A7A7A7A7A7A7F7F7F7474747C7C7C7B7B7B74747480
          80807777777979797F7F7F7474747D7D7D7A7A7A757575000000D0D0D0B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2BDBDBDBBBBBBB2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000D0D0D0B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2C0C0C04949
          491111111111111111111212121212121A1A1AB2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B4B4B4000000D0D0D0B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B29191914D4D4D5454545F5F
          5F6565656262620000004747476565656363634D4D4D4D4D4D797979BABABAB2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B3B3B3000000D0D0D0B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B29797976969692929292929298686
          86B2B2B2ADADAD0000007D7D7DB2B2B2ABABAB292929292929545454979797B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000D0D0D0B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B23D3D3D3B3B3BB2B2B2B2B2B2B2B2
          B2B2B2B2ADADAD0000007D7D7DB2B2B2B2B2B2B2B2B2B2B2B26A6A6A0E0E0EB2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000D0D0D0B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B21616166F6F6FA5A5A5B2B2B2B2B2B2B2B2
          B2B2B2B2ADADAD0000007D7D7DB2B2B2B2B2B2B2B2B2B2B2B2AAAAAA94949413
          13138A8A8AB2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000D0D0D0B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2030303757575B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2ADADAD0000007D7D7DB2B2B2B2B2B2B2B2B2B2B2B2B2B2B2A4A4A400
          0000868686B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B3B3B3000000D0D0D0B2B2B2
          B2B2B2B2B2B2B2B2B2A6A6A68B8B8B2C2C2C838383B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2ADADAD0000007F7F7FB4B4B4B2B2B2B2B2B2B2B2B2B2B2B2A7A7A727
          27277272729C9C9CB2B2B2B2B2B2B2B2B2B2B2B2B3B3B3000000D0D0D0B2B2B2
          B2B2B2B2B2B2B2B2B27B7B7B000000BDBDBDB7B7B7B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2ADADAD000000888888BBBBBBB2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B22C2C2C4C4C4CB2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000D0D0D0B2B2B2
          B2B2B2B2B2B2B2B2B27B7B7B000000B0B0B0B3B3B3B2B2B2B2B2B2BFBFBFB6B6
          B6B2B2B2B1B1B19D9D9DADADADB3B3B3B2B2B2B2B2B2B2B2B2B2B2B2B3B3B3BF
          BFBF3030304C4C4CB2B2B2B2B2B2B2B2B2B2B2B2B4B4B4000000D0D0D0B2B2B2
          B2B2B2B2B2B2B2B2B27B7B7B0000004F4F4F5151515151515151515858585353
          53737373B2B2B2B2B2B2B2B2B28D8D8D51515151515151515151515152525258
          58581616164C4C4CB2B2B2B2B2B2B2B2B2B2B2B2B3B3B3000000D0D0D0B2B2B2
          B2B2B2B2B2B2B2B2B27B7B7B0000002424242525252525252525252525252525
          25575757B2B2B2B2B2B2B2B2B27D7D7D25252525252525252525252525252525
          25250909094C4C4CB2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000D0D0D0B2B2B2
          B2B2B2B2B2B2B2B2B27B7B7B000000AFAFAFB2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B22C2C2C4C4C4CB2B2B2B2B2B2B2B2B2B2B2B2B4B4B4000000D0D0D0B2B2B2
          B2B2B2B2B2B2B2B2B27B7B7B000000BBBBBBB6B6B6B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2ADADAD171717848484B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B22C2C2C4C4C4CB2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000D0D0D0B2B2B2
          B2B2B2B2B2B2B2B2B29999995F5F5F5A5A5A949494B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2ADADAD0000007D7D7DB2B2B2B2B2B2B2B2B2B2B2B2B5B5B5B2B2B253
          53535C5C5C828282B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000D0D0D0B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2030303757575B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2ADADAD0000007D7D7DB2B2B2B2B2B2B2B2B2B2B2B2B7B7B7AFAFAF00
          0000868686B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000D0D0D0B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2030303757575B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2ADADAD0000007D7D7DB2B2B2B2B2B2B2B2B2B2B2B2B2B2B2A4A4A400
          0000868686B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000D0D0D0B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B29A9A9A4545454B4B4BB2B2B2B2B2B2B2B2
          B2B2B2B2ADADAD0000007D7D7DB2B2B2B2B2B2B2B2B2B2B2B274747423232399
          9999ACACACB2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B4B4B4000000D0D0D0B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B27A7A7A5A5A5A5555555555559494
          94B5B5B5B4B4B4000000838383B7B7B7ADADAD555555555555585858646464B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B3B3B3000000D0D0D0B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B28282822121212121216D6D
          6D9595959898980000006E6E6E9898988B8B8B2121212121215B5B5BB2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000D0D0D0B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B23939
          39000000000000000000000000000000090909B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B4B4B4000000D0D0D0B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2A0A0
          A0979797979797979797979797979797999999B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000}
        OnClick = OnHybridMissileClick
      end
      object btnAddHybridMissileTargetAimpoint: TSpeedButton
        Tag = 2
        Left = 247
        Top = 105
        Width = 23
        Height = 22
        Caption = '+'
        OnClick = OnHybridMissileClick
      end
      object bvl23: TBevel
        Left = 46
        Top = 60
        Width = 190
        Height = 2
      end
      object lbl97: TLabel
        Left = 9
        Top = 52
        Width = 32
        Height = 13
        Caption = 'Target'
      end
      object lblHybridMissileStatus: TLabel
        Left = 24
        Top = 25
        Width = 43
        Height = 13
        Caption = 'Available'
      end
      object lbl98: TLabel
        Left = 116
        Top = 25
        Width = 49
        Height = 13
        Caption = 'Quantity :'
      end
      object lblHybridMissileQuantity: TLabel
        Left = 172
        Top = 25
        Width = 12
        Height = 13
        Caption = '---'
      end
      object bvl24: TBevel
        Left = 46
        Top = 10
        Width = 190
        Height = 2
      end
      object lbl99: TLabel
        Left = 9
        Top = 3
        Width = 31
        Height = 13
        Caption = 'Status'
      end
      object btnHybridMissileLaunch: TButton
        Tag = 4
        Left = 232
        Top = 391
        Width = 80
        Height = 25
        Caption = 'Launch'
        TabOrder = 0
        OnClick = OnHybridMissileClick
      end
      object edtHybridMissileControlSalvoSize: TEdit
        Left = 118
        Top = 197
        Width = 53
        Height = 21
        TabOrder = 1
      end
      object edtHybridMissileControlCruiseAltitude: TEdit
        Left = 118
        Top = 225
        Width = 53
        Height = 21
        TabOrder = 2
      end
      object edtHybridMissileControlSeekerRange: TEdit
        Left = 118
        Top = 253
        Width = 53
        Height = 21
        Enabled = False
        TabOrder = 3
      end
      object btnDefaultHybridMissileControlSeekerRange: TButton
        Tag = 3
        Left = 235
        Top = 251
        Width = 80
        Height = 25
        Caption = '< Default'
        Enabled = False
        TabOrder = 4
        OnClick = OnHybridMissileClick
      end
      object btnDefaultHybridMissileControlCruiseAltitude: TButton
        Tag = 2
        Left = 235
        Top = 223
        Width = 80
        Height = 25
        Caption = '< Default'
        TabOrder = 5
        OnClick = OnHybridMissileClick
      end
      object edtHybridMissiletargetBearing: TEdit
        Left = 118
        Top = 134
        Width = 57
        Height = 21
        Enabled = False
        TabOrder = 6
      end
      object rbHybridMissileTargetBearing: TRadioButton
        Left = 24
        Top = 136
        Width = 87
        Height = 17
        Caption = 'Bearing :'
        Enabled = False
        TabOrder = 7
      end
      object rbHybridMissileTargetAimpoint: TRadioButton
        Left = 24
        Top = 108
        Width = 113
        Height = 17
        Caption = 'Aimpoint :'
        Checked = True
        TabOrder = 8
        TabStop = True
      end
      object rbHybridMissileTargetTrack: TRadioButton
        Left = 24
        Top = 79
        Width = 113
        Height = 17
        Caption = 'Track :'
        Enabled = False
        TabOrder = 9
      end
      object edtHybridMissiletargetTrack: TEdit
        Left = 118
        Top = 77
        Width = 57
        Height = 21
        Enabled = False
        TabOrder = 10
      end
      object edtHybridMissiletargetAimpoint: TEdit
        Left = 118
        Top = 106
        Width = 125
        Height = 21
        ReadOnly = True
        TabOrder = 11
      end
      object btnBringToHookHybridMissileTargetBearing: TButton
        Tag = 1
        Left = 235
        Top = 132
        Width = 80
        Height = 25
        Caption = 'Brg to Hook'
        Enabled = False
        TabOrder = 12
        OnClick = OnHybridMissileClick
      end
    end
  end
end
