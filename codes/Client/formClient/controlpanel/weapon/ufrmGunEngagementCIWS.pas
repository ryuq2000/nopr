unit ufrmGunEngagementCIWS;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls, ufrmWeaponType;

type
  TfrmGunEngagementCIWS = class(TfrmWeaponType)
    grbGunEngagementCIWS: TGroupBox;
    ScrollBox8: TScrollBox;
    Label468: TLabel;
    lblCIWSStatus: TLabel;
    Label474: TLabel;
    lbCIWSQuantity: TLabel;
    Label477: TLabel;
    Bevel99: TBevel;
    Label478: TLabel;
    Label479: TLabel;
    Bevel100: TBevel;
    Label480: TLabel;
    Bevel101: TBevel;
    Label481: TLabel;
    Label482: TLabel;
    Label483: TLabel;
    Label488: TLabel;
    Label489: TLabel;
    Bevel103: TBevel;
    sbCIWSControlModeAuto: TSpeedButton;
    sbCIWSControlModeManual: TSpeedButton;
    sbCIWSControlSalvoCont: TSpeedButton;
    sbCIWSControlSalvoSalvo: TSpeedButton;
    sbCIWSControlDisplayRangeShow: TSpeedButton;
    sbCIWSControlDisplayRangeHide: TSpeedButton;
    sbCIWSControlDisplayBlindZonesShow: TSpeedButton;
    sbCIWSControlDisplayBlindZonesHide: TSpeedButton;
    btnTargetSearch: TSpeedButton;
    editCIWSControlSalvoSize: TEdit;
    btnCIWSCease: TButton;
    editCIWSTargetTrack: TEdit;
    btnCIWSFire: TButton;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmGunEngagementCIWS: TfrmGunEngagementCIWS;

implementation

{$R *.dfm}

end.
