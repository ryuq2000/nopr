unit ufrmGunEngagementAutomaticManualMode;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls, ufrmWeaponType, uT3MountedGunAutoManual;

type
  TfrmGunEngagementAutomaticManualMode = class(TfrmWeaponType)
    grbGunEngagementAutomaticManualMode: TGroupBox;
    ScrollBox2: TScrollBox;
    Label12: TLabel;
    lblGunEngagementStatus: TLabel;
    Label13: TLabel;
    lbGunEngagementQuantity: TLabel;
    Label14: TLabel;
    Bevel3: TBevel;
    Label25: TLabel;
    lblSalvoMode: TLabel;
    Bevel6: TBevel;
    sbControlModeAuto: TSpeedButton;
    sbControlModeManual: TSpeedButton;
    sbControlModeChaff: TSpeedButton;
    sbControlSalvoModeCont: TSpeedButton;
    sbControlSalvoModeSalvo: TSpeedButton;
    Label15: TLabel;
    Bevel4: TBevel;
    Label16: TLabel;
    Label17: TLabel;
    lblIntercept: TLabel;
    sbGunEngagementDisplayRangeShow: TSpeedButton;
    sbGunEngagementDisplayRangeHide: TSpeedButton;
    sbGunEngagementDisplayBlindShow: TSpeedButton;
    sbGunEngagementDisplayBlindHide: TSpeedButton;
    sbGunEngagementDisplayInterceptShow: TSpeedButton;
    sbGunEngagementDisplayInterceptHide: TSpeedButton;
    sbControlModeNGS: TSpeedButton;
    ScrollBox4: TScrollBox;
    Label5: TLabel;
    Label7: TLabel;
    Bevel1: TBevel;
    Label9: TLabel;
    Edit1: TEdit;
    btnSetup: TButton;
    GroupBox1: TGroupBox;
    Label8: TLabel;
    Label29: TLabel;
    Spot1: TEdit;
    Spot2: TEdit;
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    Button4: TButton;
    GroupBox2: TGroupBox;
    lblDA: TLabel;
    lblLR: TLabel;
    Label27: TLabel;
    Label28: TLabel;
    btnConvert: TButton;
    ScrollBox1: TScrollBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    lblQuantityChaff: TLabel;
    Label6: TLabel;
    btnChaffType: TSpeedButton;
    btnPosition: TSpeedButton;
    edtChaffType: TEdit;
    edtBloomPosition: TEdit;
    edtBloomAltitude: TEdit;
    btnGunEngagementAssign: TButton;
    btnGunEngagementCease: TButton;
    btnGunEngagementBreak: TButton;
    btnGunEngagementFire: TButton;
    ScrollBox3: TScrollBox;
    Label10: TLabel;
    Label11: TLabel;
    Bevel2: TBevel;
    Label18: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    Label22: TLabel;
    Bevel5: TBevel;
    Label23: TLabel;
    btnTargetSearch: TSpeedButton;
    editControlSalvoSize: TEdit;
    editGunEngagementTargetTrack: TEdit;
    editAutofireIntercept: TEdit;
    editAutofireThreshold: TEdit;
  private
    { Private declarations }
  public
    { Public declarations }
    procedure SetControlledWeapon(ctrlObj: TObject); override;
    procedure UpdateForm; override;
  end;

var
  frmGunEngagementAutomaticManualMode: TfrmGunEngagementAutomaticManualMode;

implementation

{$R *.dfm}


procedure TfrmGunEngagementAutomaticManualMode.SetControlledWeapon(
  ctrlObj: TObject);
begin
  inherited;

  if Assigned(FControlledWeapon) then
  with TT3MountedGunAutoManual(FControlledWeapon) do
  begin
  end;
end;

procedure TfrmGunEngagementAutomaticManualMode.UpdateForm;
begin
  inherited;

end;

end.
