unit ufrmActivePassiveTorpedo;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, Buttons,ufrmWeaponType,uT3MountedActivePassiveTorp;

type
  TfrmActivePassiveTorpedo = class(TfrmWeaponType)
    grpActivePasiveTorpedo: TGroupBox;
    scrlbx1: TScrollBox;
    btnHideBlindZoneAPG: TSpeedButton;
    btnHideRangeAPG: TSpeedButton;
    btnShowBlindZoneAPG: TSpeedButton;
    btnShowRangeAPG: TSpeedButton;
    btnTargetTrackAPG: TSpeedButton;
    bvl1: TBevel;
    bvl2: TBevel;
    bvl3: TBevel;
    bvl4: TBevel;
    lbl1: TLabel;
    lbl12: TLabel;
    lbl13: TLabel;
    lbl14: TLabel;
    lbl15: TLabel;
    lbl17: TLabel;
    lbl18: TLabel;
    lbl19: TLabel;
    lbl20: TLabel;
    lbl21: TLabel;
    lbl22: TLabel;
    lbl3: TLabel;
    lbl5: TLabel;
    lbl6: TLabel;
    lbl7: TLabel;
    lbl8: TLabel;
    lbl9: TLabel;
    lblQuantityAPG: TLabel;
    lblStatusAPG: TLabel;
    btn4: TButton;
    btn5: TButton;
    btn6: TButton;
    btn7: TButton;
    btnLaunchAP: TButton;
    edtEdtAPTargetTrack: TEdit;
    edtEdtSafetyCeilingAP: TEdit;
    edtEdtSalvoAP: TEdit;
    edtEdtSearchDepthAP: TEdit;
    edtEdtSearchRadiusAP: TEdit;
    edtEdtSeekerRangeAP: TEdit;
    procedure OnBtnAPClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure UpdateForm; override;
    procedure SetControlledWeapon(aObj : TObject); override;
  end;

var
  frmActivePassiveTorpedo: TfrmActivePassiveTorpedo;

implementation

{$R *.dfm}

{ TfrmActivePassiveTorpedo }

procedure TfrmActivePassiveTorpedo.OnBtnAPClick(Sender: TObject);
begin
  inherited;
  //
end;

procedure TfrmActivePassiveTorpedo.SetControlledWeapon(aObj: TObject);
begin
  inherited;

  if Assigned(ControlledWeapon) then
  with TT3MountedActivePassiveTorp(ControlledWeapon) do
  begin
    btnHideBlindZoneAPG.Enabled := BlindZones.Count > 0;
    btnShowBlindZoneAPG.Enabled := BlindZones.Count > 0;

    if ShowBlindZone then
      btnShowBlindZoneAPG.Down := True
    else
      btnHideBlindZoneAPG.Down := True;

    if ShowRange then
      btnShowRangeAPG.Down := True
    else
      btnHideRangeAPG.Down := True;

    //edtEdtAPTargetTrack.Text := TargetTrack;
    edtEdtSalvoAP.Text         := IntToStr(SalvoSize);
    edtEdtSearchRadiusAP.Text  := FloatToStr(SearchRadius);
    edtEdtSearchDepthAP.Text   := FloatToStr(SearchDepth);
    edtEdtSafetyCeilingAP.Text := FloatToStr(SafetyCeiling);
    edtEdtSeekerRangeAP.Text   := FloatToStr(SeekerRange);
  end;
end;

procedure TfrmActivePassiveTorpedo.UpdateForm;
begin
  inherited;
  if Assigned(ControlledWeapon) then
  with TT3MountedActivePassiveTorp(ControlledWeapon) do
  begin
    lblQuantityAPG.Caption  := IntToStr(Quantity);
    btnLaunchAP.Enabled := ButtonLaunch;
  end;
end;

end.
