unit ufrmPanelGuidance;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, StdCtrls, Buttons, ExtCtrls, ufrmControlled, ufrmGuidanceType,
  tttData, uT3Track;

type
  TfrmPanelGuidance = class(TfrmControlled)
    PanelGuidanceControlChoices: TPanel;
    btnMenuGuidance: TSpeedButton;
    edGuidance: TEdit;
    pmGuidance: TPopupMenu;
    mnStraightLine1: TMenuItem;
    mnHelm1: TMenuItem;
    mnCircle: TMenuItem;
    mnStation: TMenuItem;
    mnZigzag1: TMenuItem;
    Short1: TMenuItem;
    Long1: TMenuItem;
    VeryLong1: TMenuItem;
    mnSinuation1: TMenuItem;
    mnFormation2: TMenuItem;
    mnEvasion1: TMenuItem;
    mnWaypoint1: TMenuItem;
    mnOutrun1: TMenuItem;
    mnEngagement1: TMenuItem;
    mnShadow1: TMenuItem;
    mnReturntoBase1: TMenuItem;
    procedure FormCreate(Sender: TObject);
    procedure btnMenuGuidanceClick(Sender: TObject);
    procedure OnGuidanceClick(Sender: TObject);
  private
    { Private declarations }
    FGuidanceType : TfrmGuidanceType;
    FLastGuidanceType : TVehicleGuidanceType;
    function guidanceFactory(guidanceType : TVehicleGuidanceType) : TfrmGuidanceType;
    procedure guidanceInit;
  public
    { Public declarations }
    procedure InitCreate(sender: TForm); override;
    procedure SetControlledObject(ctrlObj: TT3Track); override;
    procedure UpdateForm; override;
    procedure ReadOnlyMode; override;
    procedure UnReadOnlyMode; override;
    procedure EmptyField;override;

    {  }
    procedure updateGuidancePanel;
  end;

var
  frmPanelGuidance: TfrmPanelGuidance;

implementation

uses
  ufrmGuidanceSL, ufrmguidanceHelm, ufrmGuidanceStation,
  ufrmGUidancezigzag, ufrmGUidanceSinuation,ufrmguidanceWaypoint,
  ufrmGuidanceFormation, ufrmGuidancecircle, ufrmguidanceEngagement,
  ufrmGuidanceOutrun, ufrmGuidanceEvasion,ufrmGuidanceReturnBase,
  ufrmGuidanceShadow, uGameData_TTT, uT3ClientManager,
  uT3PlatformInstance, uT3Vehicle;

{$R *.dfm}

procedure TfrmPanelGuidance.EmptyField;
begin
  inherited;

end;

procedure TfrmPanelGuidance.FormCreate(Sender: TObject);
begin
  FGuidanceType := nil;
end;

function TfrmPanelGuidance.guidanceFactory(
  guidanceType: TVehicleGuidanceType): TfrmGuidanceType;
begin
  result := nil;

  case guidanceType of
    vgtStraightLine : result := TfrmGuidanceSL.Create(nil);
    vgtHelm         : result := TfrmGUidanceHelm.Create(nil);
    vgtCircle       : result := TfrmGuidanceCircle.Create(nil);
    vgtStation      : result := TfrmGuidanceStation.Create(nil);
    vgtZigzag       : result := TfrmGuidanceZigzag.Create(nil);
    vgtSinuation    : result := TfrmGuidanceSinuation.Create(nil);
    vgtFormation    : result := TfrmGuidanceFormation.Create(nil);
    vgtEvasion      : result := TfrmGuidanceEvasion.Create(nil);
    vgtWaypoint     : result := TfrmGuidanceWaypoint.Create(nil);
    vgtOutrun       : result := TfrmGuidanceOutrun.Create(nil);
    vgtEngagement   : result := TfrmGuidanceEngagement.Create(nil);
    vgtShadow       : result := TfrmGuidanceShadow.Create(nil);
    vgtReturnToBase : result := TfrmGuidanceReturnbase.Create(nil);
  end;

end;

procedure TfrmPanelGuidance.guidanceInit;
begin
  if Assigned(FGuidanceType) then
  begin
    FGuidanceType.SetControlledObject(FControlledTrack);
    FGuidanceType.UpdateForm;
    FGuidanceType.Parent := Self;
    FGuidanceType.Align := alClient;
    FGuidanceType.Show;
  end;
end;

procedure TfrmPanelGuidance.InitCreate(sender: TForm);
begin
  inherited;

end;

procedure TfrmPanelGuidance.OnGuidanceClick(Sender: TObject);
var
  rec: TRecCmd_PlatformGuidance;
begin
  inherited;
  { send guide command to server }
  if FControlledPlatform is TT3Vehicle then
  begin
    rec.PlatfomID := FControlledPlatform.InstanceIndex;
    rec.OrderID   := CORD_ID_MOVE;
    rec.OrderType := CORD_TYPE_GUIDANCE;

    case TMenuItem(Sender).Tag of
      1  : rec.OrderParam  := Byte(vgtStraightLine);
      2  : rec.OrderParam  := Byte(vgtHelm);
      3  : rec.OrderParam  := Byte(vgtCircle);
      4  : rec.OrderParam  := Byte(vgtStation);
      5  : rec.OrderParam  := Byte(vgtZigzagShort);
      6  : rec.OrderParam  := Byte(vgtSinuation);
      7  : rec.OrderParam  := Byte(vgtFormation);
      8  : rec.OrderParam  := Byte(vgtEvasion);
      9  : rec.OrderParam  := Byte(vgtWaypoint);
      10 : rec.OrderParam  := Byte(vgtOutrun);
      11 : rec.OrderParam  := Byte(vgtEngagement);
      12 : rec.OrderParam  := Byte(vgtShadow);
      13 : rec.OrderParam  := Byte(vgtReturnToBase);
      14 : rec.OrderParam  := Byte(vgtZigzagShort);
      15 : rec.OrderParam  := Byte(vgtZigzagLong);
      16 : rec.OrderParam  := Byte(vgtZigzagVeryLong);
    end;

    clientManager.NetCmdSender.CmdPlatformGuidance(rec);
  end;
end;

procedure TfrmPanelGuidance.ReadOnlyMode;
begin
  inherited;

end;

procedure TfrmPanelGuidance.SetControlledObject(ctrlObj: TT3Track);
begin
  inherited;
  updateGuidancePanel;
end;

procedure TfrmPanelGuidance.updateGuidancePanel;
var
  i   : integer;
  frm : TfrmGuidanceType;
begin
  if FControlledPlatform is TT3Vehicle then
  begin
    frm := guidanceFactory(TT3Vehicle(FControlledPlatform).VehicleGuidance.GuidanceType);

    if Assigned(frm) then
    begin
      if Assigned(FGuidanceType) then
        FGuidanceType.Free;

      FGuidanceType := frm;
      guidanceInit;

      edGuidance.Text := FGuidanceType.Caption;
    end;

    FLastGuidanceType := TT3Vehicle(FControlledPlatform).VehicleGuidance.GuidanceType;

  end;
end;

procedure TfrmPanelGuidance.btnMenuGuidanceClick(Sender: TObject);
var
  pos: TPoint;
  I : Integer;
//  formation : TFormation;
//  fm : TFormationRefine;
begin
  inherited;

  if Assigned(FControlledPlatform) then
  begin

    if (TT3PlatformInstance(FControlledPlatform).PlatformDomain = vhdAir) or
       (TT3PlatformInstance(FControlledPlatform).PlatformDomain = vhdSubsurface) then
      mnStation.Enabled := True
    else
      mnStation.Enabled := False;

    if FControlledPlatform is TT3Vehicle then
    begin
//      if simMgrClient.Scenario.Formation_List_rev.PlatformIsLeader(TT3Vehicle(FControlled).InstanceIndex) then
//        mnFormation2.Enabled := True
//      else
//        mnFormation2.Enabled := False;
    end;

  end;

//  if machineRole = crController then
//    SetReturnToBaseOption;

// ====
//  if Assigned(focused_platform) and Assigned(FControlled) then
//  begin
//    if TT3PlatformInstance(focused_platform).InstanceIndex
//        = TT3PlatformInstance(FControlled).InstanceIndex then
//    begin
//      mnEvasion1.Enabled := False;
//      //mnEngagement1.Enabled := False;
//      mnOutrun1.Enabled := False;
//      mnShadow1.Enabled := False;
//    end
//    else
//    begin
//      mnEvasion1.Enabled := True;
//      //mnEngagement1.Enabled := True;
//      mnOutrun1.Enabled := True;
//      mnShadow1.Enabled := True;
//    end;
//
//    if (TT3PlatformInstance(FControlled).PlatformDomain = vhdAir) or
//       (TT3PlatformInstance(FControlled).PlatformDomain = vhdSubsurface) then
//    begin
//      mnStation.Enabled := True;
//    end
//    else
//    mnStation.Enabled := False;
//
//    if FControlled is TT3Vehicle then
//    begin
//      if simMgrClient.Scenario.Formation_List_rev.PlatformIsLeader(TT3Vehicle(FControlled).InstanceIndex) then
//        mnFormation2.Enabled := True
//      else
//        mnFormation2.Enabled := False;
//    end;
//
//  end;
//
//  if simMgrClient.ISInstructor then
//    SetReturnToBaseOption;
//
  GetCursorPos(pos);
  pmGuidance.Popup(pos.X, pos.Y);

end;

procedure TfrmPanelGuidance.UnReadOnlyMode;
begin
  inherited;

end;

procedure TfrmPanelGuidance.UpdateForm;
begin
  inherited;
  if FControlledPlatform is TT3Vehicle then
    if Assigned(TT3Vehicle(FControlledPlatform).VehicleGuidance) then
      if FLastGuidanceType <> TT3Vehicle(FControlledPlatform).VehicleGuidance.GuidanceType then
        updateGuidancePanel;

  if Assigned(FGuidanceType) then
    FGuidanceType.UpdateForm;
end;

end.
