unit ufrmPanelWeapon;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls, ufrmControlled,ufrmWeaponType,
  uT3Vehicle, Menus, uT3MountedWeapon, tttData, uT3Track;

type
  
  TfrmPanelWeapon = class(TfrmControlled)
    PanelWeaponChoices: TPanel;
    btnWeapon: TSpeedButton;
    edtWeaponName: TEdit;
    pmenuWeapon: TPopupMenu;
    procedure btnWeaponClick(Sender: TObject);
  private
    { Private declarations }
    FWeaponControl : TfrmWeaponType;
    FWeaponObject  : TT3MountedWeapon;
    procedure addWeapon(aWeapon : TT3MountedWeapon);

    procedure OnWeaponItemSelected(Sender: TObject);
    procedure showWeaponController(weapon : TT3MountedWeapon);
    function createWeaponControl(aType : TWeaponCategory) : TfrmWeaponType;
    procedure RefreshPlatformWeapons(vehicle: TT3Vehicle);
  public
    { Public declarations }
    procedure SetControlledObject(ctrlObj: TT3Track); override;
    procedure UpdateForm; override;
    procedure EmptyField; override;
  end;

var
  frmPanelWeapon: TfrmPanelWeapon;

implementation

uses
  ufrmAcousticTorpedo,
  ufrmActivePassiveTorpedo,
  ufrmAirDroppedTorpedo,
  ufrmAirDroppedVECTAC,
  ufrmBombDepthCharge,
  ufrmGunEngagementChaffMode,
  ufrmHybridMissile,
  ufrmMines,
  ufrmStraightRunTorpedo,
  ufrmSurfaceToAirMissile,
  ufrmSurfaceToSurfaceMissile,
  ufrmTacticalAcousticTorpedos,
  ufrmTacticalMissile,
  ufrmWakeHomingTorpedo,
  ufrmWireGuidedTorpedo,
  ufrmGunEngagementAutomaticManualMode,
  ufrmGunEngagementCIWS,
  uT3MountedMissile,
  uT3MountedTorpedo,
  uT3MountedGun,
  uT3MountedBomb,
  uT3MountedMine,
  uT3MountedHybrid,
  uGlobalVar;


type
  TMenuItemData = class (TMenuItem)
  public
    Data : TObject;
  end;

{$R *.dfm}

{ TfrmPanelWeapon }

procedure TfrmPanelWeapon.addWeapon(aWeapon: TT3MountedWeapon);
var
  Item    : TMenuItemData;
begin
  Item         := TMenuItemData.Create(self);
  Item.OnClick := OnWeaponItemSelected;

  if aWeapon.InstanceName = '' then
   Item.Caption := aWeapon.ClassName
  else
   Item.Caption := aWeapon.InstanceName;

  item.Tag     := Byte(aWeapon.WeaponCategory);
  item.Data    := aWeapon;

  pmenuWeapon.Items.Add(Item);

//  if vehicle.MountedWeapons.Count - 1 >= vehicle.RecentlyUsedWeapon then
//  begin
//    showWeaponController(vehicle.MountedWeapons[vehicle.RecentlyUsedWeapon]);
//  end;

end;

procedure TfrmPanelWeapon.btnWeaponClick(Sender: TObject);
var
  pt: TPoint;
begin
  inherited;
  GetCursorPos(pt);
  if pmenuWeapon.Items.Count > 0 then
    pmenuWeapon.Popup(pt.X, pt.Y);

end;

function TfrmPanelWeapon.createWeaponControl(
  aType: TWeaponCategory): TfrmWeaponType;
begin
  result := nil;

  case aType of
    wcTorpedoActiveAcoustic   : result := TfrmAcousticTorpedo.Create(nil);

    wcTorpedoPassiveAcoustic,
    wcTorpedoActivePassive    : result := TfrmActivePassiveTorpedo.Create(nil);

    wcTorpedoAirDropped       : result := TfrmAirDroppedTorpedo.Create(nil);
    wcVectac                  : result := TfrmAirDroppedVECTAC.Create(nil);
    wcBomb                    : result := TfrmBombDepthCharge.Create(nil);
    wcGunGun,wcGunRocket      : result := TfrmGunEngagementAutomaticManualMode.Create(nil);
    wcHybrid                  : result := TfrmHybridMissile.Create(nil);
    wcMine                    : result := TfrmMines.Create(nil);
    wcTorpedoStraigth         : result := TfrmStraigthRunTorpedo.Create(nil);

    wcMissileSurfaceSubsurfaceToAir
                              : result := TfrmSurfaceToAirMissile.Create(nil);

    wcMissileSurfaceSubsurfaceToSurfaceSubsurface
                              : result := TfrmSurfaceToSurfaceMissile.Create(nil);

    wcMissileAirToSurfaceSubsurface,
    wcMissileAirToAir,
    wcMissileLandAttack       : result := TfrmTacticalMissile.Create(nil);

    wcTorpedoWakeHoming       : result := TfrmWakeHomingTorpedo.Create(nil);
    wcTorpedoWireGuided       : result := TfrmWireGuidedTorpedo.Create(nil);
    wcGunCIWS                 : result := TfrmGunEngagementCIWS.Create(nil);

  end;

  if Assigned(result) then
  begin
    result.Parent := Self;
    result.Align  := alClient;
    result.BorderStyle := bsNone;
    result.Show;
  end;
end;

procedure TfrmPanelWeapon.EmptyField;
begin
  inherited;

  { set empty }

  if ASsigned( FWeaponControl ) then
    FWeaponControl.Free;
  FWeaponControl := nil;

end;

procedure TfrmPanelWeapon.OnWeaponItemSelected(Sender: TObject);
begin
  if Sender is TMenuItemData then
    if TMenuItemData(Sender).Data is TT3MountedWeapon then
    begin
      showWeaponController(TT3MountedWeapon(TMenuItemData(Sender).Data));
      TT3Vehicle(FControlledPlatform).RecentlyUsedWeapon :=
        TT3MountedWeapon(TMenuItemData(Sender).Data).InstanceIndex;
    end;
end;

procedure TfrmPanelWeapon.RefreshPlatformWeapons(vehicle: TT3Vehicle);
var
  weapon  : TT3MountedWeapon;
  wpnExist : Boolean;
  i : integer;
begin

  pmenuWeapon.Items.Clear;
  wpnExist := False;
  btnWeapon.Enabled  := True;

  { add missile }
  for I := 0 to simManager.SimMissilesOnBoards.ItemCount - 1 do
  begin
    weapon := simManager.SimMissilesOnBoards.getObject(I) as TT3MountedWeapon;

    if weapon.PlatformParentInstanceIndex = vehicle.InstanceIndex then
    begin
      wpnExist := True;
      addWeapon(weapon);
    end;
  end;

  { add torpedo }
  for I := 0 to simManager.SimTorpedoesOnBoards.ItemCount - 1 do
  begin
    weapon := simManager.SimTorpedoesOnBoards.getObject(I) as TT3MountedWeapon;

    if weapon.PlatformParentInstanceIndex = vehicle.InstanceIndex then
    begin
      wpnExist := True;
      addWeapon(weapon);
    end;
  end;

  { add gun }
  for I := 0 to simManager.SimGunsOnBoards.ItemCount - 1 do
  begin
    weapon := simManager.SimGunsOnBoards.getObject(I) as TT3MountedWeapon;

    if weapon.PlatformParentInstanceIndex = vehicle.InstanceIndex then
    begin
      wpnExist := True;
      addWeapon(weapon);
    end;
  end;

  { add mine }
  for I := 0 to simManager.SimMinesOnBoards.ItemCount - 1 do
  begin
    weapon := simManager.SimMinesOnBoards.getObject(I) as TT3MountedWeapon;

    if weapon.PlatformParentInstanceIndex = vehicle.InstanceIndex then
    begin
      wpnExist := True;
      addWeapon(weapon);
    end;
  end;

  { add bomb }
  for I := 0 to simManager.SimBombsOnBoards.ItemCount - 1 do
  begin
    weapon := simManager.SimBombsOnBoards.getObject(I) as TT3MountedWeapon;

    if weapon.PlatformParentInstanceIndex = vehicle.InstanceIndex then
    begin
      wpnExist := True;
      addWeapon(weapon);
    end;
  end;

  if wpnExist then
    btnWeapon.Enabled  := True;
end;

procedure TfrmPanelWeapon.SetControlledObject(ctrlObj: TT3Track);
begin

  { if same with previous platform, ignore it}
  if ASsigned(FControlledTrack) and FControlledTrack.Equals(ctrlObj) then
    Exit;

  inherited;

  { set empty }
  EmptyField;

  edtWeaponName.Text := '';
  pmenuWeapon.Items.Clear;

  { fill platform sensor list }
  if FControlledPlatform is TT3Vehicle then
    RefreshPlatformWeapons(TT3Vehicle(FControlledPlatform));

end;

procedure TfrmPanelWeapon.showWeaponController(weapon: TT3MountedWeapon);
begin
  if Assigned(weapon) then
  begin
    edtWeaponName.Text := weapon.InstanceName;

    if weapon = FWeaponObject then
      Exit;

    if ASsigned(FWeaponControl) and Assigned(FWeaponControl.ControlledWeapon) then
    begin
      if not (TT3MountedWeapon(FWeaponControl.ControlledWeapon).WeaponCategory =
        weapon.WeaponCategory) then
      begin
        EmptyField;
        FWeaponControl := createWeaponControl(TT3MountedWeapon(weapon).WeaponCategory);
      end;
    end
    else
    begin
      EmptyField;
      FWeaponControl := createWeaponControl(TT3MountedWeapon(weapon).WeaponCategory);
    end;

    if Assigned(FWeaponControl) then
      FWeaponControl.SetControlledWeapon(weapon);


    if weapon.InstanceName = '' then
     edtWeaponName.Text := weapon.ClassName
    else
     edtWeaponName.Text := weapon.InstanceName

  end;
end;

procedure TfrmPanelWeapon.UpdateForm;
begin
  inherited;

  if Assigned(FWeaponControl) then
    FWeaponControl.UpdateForm;

end;

end.
