object frmPanelFC: TfrmPanelFC
  Left = 0
  Top = 0
  Caption = 'frmPanelFC'
  ClientHeight = 466
  ClientWidth = 310
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object PanelFCChoices: TPanel
    Left = 0
    Top = 0
    Width = 310
    Height = 97
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitWidth = 280
    object lstAssetsChoices: TListView
      Left = 0
      Top = 0
      Width = 310
      Height = 97
      Align = alTop
      Columns = <
        item
          AutoSize = True
          Caption = 'Name'
        end
        item
          Alignment = taCenter
          AutoSize = True
          Caption = 'Status'
        end>
      ReadOnly = True
      RowSelect = True
      TabOrder = 0
      ViewStyle = vsReport
      ExplicitWidth = 280
    end
  end
  object PanelALL: TPanel
    Left = 0
    Top = 97
    Width = 310
    Height = 369
    Align = alClient
    BevelOuter = bvNone
    ParentBackground = False
    TabOrder = 1
    ExplicitLeft = -40
    ExplicitTop = -47
    ExplicitWidth = 320
    ExplicitHeight = 417
    object PanelFC: TPanel
      Left = 0
      Top = 0
      Width = 310
      Height = 369
      Align = alClient
      BevelOuter = bvNone
      ParentBackground = False
      TabOrder = 0
      ExplicitWidth = 320
      ExplicitHeight = 417
      object ScrollBox3: TScrollBox
        Left = 0
        Top = 0
        Width = 310
        Height = 369
        Align = alClient
        BevelOuter = bvNone
        BorderStyle = bsNone
        TabOrder = 0
        ExplicitWidth = 320
        ExplicitHeight = 417
        object grbFireControl: TGroupBox
          Left = 0
          Top = 0
          Width = 310
          Height = 369
          Align = alClient
          TabOrder = 0
          ExplicitWidth = 320
          ExplicitHeight = 417
          object Bevel27: TBevel
            Left = 46
            Top = 12
            Width = 253
            Height = 3
          end
          object Bevel52: TBevel
            Left = 46
            Top = 120
            Width = 253
            Height = 3
          end
          object Bevel53: TBevel
            Left = 46
            Top = 284
            Width = 253
            Height = 3
          end
          object btnSearchFireControlAssetsTarget: TSpeedButton
            Left = 102
            Top = 131
            Width = 33
            Height = 22
            Glyph.Data = {
              36090000424D360900000000000036000000280000001F000000180000000100
              18000000000000090000000000000000000000000000000000006161613E3E3E
              3737374040403B3B3B3A3A3A4141414141413A3A3A3A3A3A4040403737373E3E
              3E3D3D3D3838384141413A3A3A3B3B3B4040403737373E3E3E3D3D3D38383841
              41413A3A3A3B3B3B4040403737373E3E3E3C3C3C383838000000B2B2B27C7C7C
              7474747F7F7F7878787777778080808080807878787878788080807474747C7C
              7C7B7B7B7474748080807A7A7A7A7A7A7F7F7F7474747C7C7C7B7B7B74747480
              80807777777979797F7F7F7474747D7D7D7A7A7A757575000000D0D0D0B2B2B2
              B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
              B2B2B2B2B2B2B2B2B2B2BDBDBDBBBBBBB2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
              B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000D0D0D0B2B2B2
              B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2C0C0C04949
              491111111111111111111212121212121A1A1AB2B2B2B2B2B2B2B2B2B2B2B2B2
              B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B4B4B4000000D0D0D0B2B2B2
              B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B29191914D4D4D5454545F5F
              5F6565656262620000004747476565656363634D4D4D4D4D4D797979BABABAB2
              B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B3B3B3000000D0D0D0B2B2B2
              B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B29797976969692929292929298686
              86B2B2B2ADADAD0000007D7D7DB2B2B2ABABAB292929292929545454979797B2
              B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000D0D0D0B2B2B2
              B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B23D3D3D3B3B3BB2B2B2B2B2B2B2B2
              B2B2B2B2ADADAD0000007D7D7DB2B2B2B2B2B2B2B2B2B2B2B26A6A6A0E0E0EB2
              B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000D0D0D0B2B2B2
              B2B2B2B2B2B2B2B2B2B2B2B2B2B2B21616166F6F6FA5A5A5B2B2B2B2B2B2B2B2
              B2B2B2B2ADADAD0000007D7D7DB2B2B2B2B2B2B2B2B2B2B2B2AAAAAA94949413
              13138A8A8AB2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000D0D0D0B2B2B2
              B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2030303757575B2B2B2B2B2B2B2B2B2B2B2
              B2B2B2B2ADADAD0000007D7D7DB2B2B2B2B2B2B2B2B2B2B2B2B2B2B2A4A4A400
              0000868686B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B3B3B3000000D0D0D0B2B2B2
              B2B2B2B2B2B2B2B2B2A6A6A68B8B8B2C2C2C838383B2B2B2B2B2B2B2B2B2B2B2
              B2B2B2B2ADADAD0000007F7F7FB4B4B4B2B2B2B2B2B2B2B2B2B2B2B2A7A7A727
              27277272729C9C9CB2B2B2B2B2B2B2B2B2B2B2B2B3B3B3000000D0D0D0B2B2B2
              B2B2B2B2B2B2B2B2B27B7B7B000000BDBDBDB7B7B7B2B2B2B2B2B2B2B2B2B2B2
              B2B2B2B2ADADAD000000888888BBBBBBB2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
              B2B22C2C2C4C4C4CB2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000D0D0D0B2B2B2
              B2B2B2B2B2B2B2B2B27B7B7B000000B0B0B0B3B3B3B2B2B2B2B2B2BFBFBFB6B6
              B6B2B2B2B1B1B19D9D9DADADADB3B3B3B2B2B2B2B2B2B2B2B2B2B2B2B3B3B3BF
              BFBF3030304C4C4CB2B2B2B2B2B2B2B2B2B2B2B2B4B4B4000000D0D0D0B2B2B2
              B2B2B2B2B2B2B2B2B27B7B7B0000004F4F4F5151515151515151515858585353
              53737373B2B2B2B2B2B2B2B2B28D8D8D51515151515151515151515152525258
              58581616164C4C4CB2B2B2B2B2B2B2B2B2B2B2B2B3B3B3000000D0D0D0B2B2B2
              B2B2B2B2B2B2B2B2B27B7B7B0000002424242525252525252525252525252525
              25575757B2B2B2B2B2B2B2B2B27D7D7D25252525252525252525252525252525
              25250909094C4C4CB2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000D0D0D0B2B2B2
              B2B2B2B2B2B2B2B2B27B7B7B000000AFAFAFB2B2B2B2B2B2B2B2B2B2B2B2B2B2
              B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
              B2B22C2C2C4C4C4CB2B2B2B2B2B2B2B2B2B2B2B2B4B4B4000000D0D0D0B2B2B2
              B2B2B2B2B2B2B2B2B27B7B7B000000BBBBBBB6B6B6B2B2B2B2B2B2B2B2B2B2B2
              B2B2B2B2ADADAD171717848484B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
              B2B22C2C2C4C4C4CB2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000D0D0D0B2B2B2
              B2B2B2B2B2B2B2B2B29999995F5F5F5A5A5A949494B2B2B2B2B2B2B2B2B2B2B2
              B2B2B2B2ADADAD0000007D7D7DB2B2B2B2B2B2B2B2B2B2B2B2B5B5B5B2B2B253
              53535C5C5C828282B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000D0D0D0B2B2B2
              B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2030303757575B2B2B2B2B2B2B2B2B2B2B2
              B2B2B2B2ADADAD0000007D7D7DB2B2B2B2B2B2B2B2B2B2B2B2B7B7B7AFAFAF00
              0000868686B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000D0D0D0B2B2B2
              B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2030303757575B2B2B2B2B2B2B2B2B2B2B2
              B2B2B2B2ADADAD0000007D7D7DB2B2B2B2B2B2B2B2B2B2B2B2B2B2B2A4A4A400
              0000868686B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000D0D0D0B2B2B2
              B2B2B2B2B2B2B2B2B2B2B2B2B2B2B29A9A9A4545454B4B4BB2B2B2B2B2B2B2B2
              B2B2B2B2ADADAD0000007D7D7DB2B2B2B2B2B2B2B2B2B2B2B274747423232399
              9999ACACACB2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B4B4B4000000D0D0D0B2B2B2
              B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B27A7A7A5A5A5A5555555555559494
              94B5B5B5B4B4B4000000838383B7B7B7ADADAD555555555555585858646464B2
              B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B3B3B3000000D0D0D0B2B2B2
              B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B28282822121212121216D6D
              6D9595959898980000006E6E6E9898988B8B8B2121212121215B5B5BB2B2B2B2
              B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000D0D0D0B2B2B2
              B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B23939
              39000000000000000000000000000000090909B2B2B2B2B2B2B2B2B2B2B2B2B2
              B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B4B4B4000000D0D0D0B2B2B2
              B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2A0A0
              A0979797979797979797979797979797999999B2B2B2B2B2B2B2B2B2B2B2B2B2
              B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000}
          end
          object Label265: TLabel
            Left = 46
            Top = 21
            Width = 26
            Height = 13
            Caption = 'Mode'
          end
          object Label266: TLabel
            Left = 151
            Top = 21
            Width = 97
            Height = 13
            Caption = 'Channels Available :'
          end
          object Label267: TLabel
            Left = 6
            Top = 6
            Width = 35
            Height = 13
            Caption = 'Control'
          end
          object Label268: TLabel
            Left = 6
            Top = 114
            Width = 32
            Height = 13
            Caption = 'Target'
          end
          object Label514: TLabel
            Left = 6
            Top = 278
            Width = 34
            Height = 13
            Caption = 'Display'
          end
          object Label515: TLabel
            Left = 70
            Top = 288
            Width = 31
            Height = 13
            Caption = 'Range'
          end
          object Label527: TLabel
            Left = 180
            Top = 288
            Width = 54
            Height = 13
            Caption = 'Blind Zones'
          end
          object lbControlChannel: TLabel
            Left = 254
            Top = 21
            Width = 8
            Height = 13
            Caption = '--'
          end
          object sbFireControlAssetsBlindZonesHide: TSpeedButton
            Tag = 10
            Left = 172
            Top = 329
            Width = 80
            Height = 25
            AllowAllUp = True
            GroupIndex = 2
            Down = True
            Caption = 'Hide'
          end
          object sbFireControlAssetsBlindZonesShow: TSpeedButton
            Tag = 9
            Left = 172
            Top = 304
            Width = 80
            Height = 25
            HelpContext = 1
            AllowAllUp = True
            GroupIndex = 2
            Caption = 'Show'
          end
          object sbFireControlAssetsDisplayHide: TSpeedButton
            Tag = 8
            Left = 50
            Top = 329
            Width = 80
            Height = 25
            AllowAllUp = True
            GroupIndex = 3
            Down = True
            Caption = 'Hide'
          end
          object sbFireControlAssetsDisplayShow: TSpeedButton
            Tag = 7
            Left = 50
            Top = 304
            Width = 80
            Height = 25
            HelpContext = 1
            AllowAllUp = True
            GroupIndex = 3
            Caption = 'Show'
          end
          object sbFireControlAssetsModeOff: TSpeedButton
            Tag = 3
            Left = 21
            Top = 85
            Width = 80
            Height = 25
            AllowAllUp = True
            GroupIndex = 1
            Down = True
            Caption = 'Off'
          end
          object sbFireControlAssetsModeSearch: TSpeedButton
            Tag = 1
            Left = 21
            Top = 36
            Width = 80
            Height = 25
            HelpContext = 1
            AllowAllUp = True
            GroupIndex = 1
            Caption = 'Search / Track'
          end
          object sbFireControlAssetsModeTrackOnly: TSpeedButton
            Tag = 2
            Left = 21
            Top = 61
            Width = 80
            Height = 25
            HelpContext = 1
            AllowAllUp = True
            GroupIndex = 1
            Caption = 'Track Only'
          end
          object btnFireControlAssetsTargetAssign: TButton
            Tag = 4
            Left = 164
            Top = 136
            Width = 80
            Height = 25
            Caption = 'Assign'
            TabOrder = 0
          end
          object btnFireControlAssetsTargetBreak: TButton
            Tag = 5
            Left = 164
            Top = 161
            Width = 80
            Height = 25
            Caption = 'Break'
            TabOrder = 1
          end
          object btnFireControlAssetsTargetBreakAll: TButton
            Tag = 6
            Left = 164
            Top = 186
            Width = 80
            Height = 25
            Caption = 'Break All'
            TabOrder = 2
          end
          object edtFireControlAssetsTarget: TEdit
            Left = 21
            Top = 131
            Width = 75
            Height = 21
            ReadOnly = True
            TabOrder = 3
          end
          object lstFireControlAssetsAssignedTracks: TListView
            Left = 21
            Top = 156
            Width = 114
            Height = 118
            Columns = <
              item
                Caption = 'Assigned Tracks'
                MaxWidth = 200
                MinWidth = 100
                Width = 100
              end>
            TabOrder = 4
            ViewStyle = vsReport
          end
        end
      end
    end
  end
end
