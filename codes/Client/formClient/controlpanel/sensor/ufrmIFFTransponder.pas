unit ufrmIFFTransponder;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls,ufrmSensorType;

type
  TfrmIFFTransponder = class(TfrmSensorType)
    grbIFFTransponderControl: TGroupBox;
    ScrollBox6: TScrollBox;
    Bevel8: TBevel;
    Bevel9: TBevel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    sbIFFTransponderControlModeOff: TSpeedButton;
    sbIFFTransponderControlModeOn: TSpeedButton;
    cbIFFTransponderControlMode1: TCheckBox;
    cbIFFTransponderControlMode2: TCheckBox;
    cbIFFTransponderControlMode3: TCheckBox;
    cbIFFTransponderControlMode3C: TCheckBox;
    cbIFFTransponderControlMode4: TCheckBox;
    edtIFFTransponderControlMode1: TEdit;
    edtIFFTransponderControlMode2: TEdit;
    edtIFFTransponderControlMode3: TEdit;
    procedure edtTransponderOnKeyPressed(Sender: TObject; var Key: Char);
    procedure OnIFFCheckedClick(Sender: TObject);
    procedure btnIFFOnClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure UpdateForm; override;
    procedure SetControlledSensor(aObj : TObject); override;
  end;

var
  frmIFFTransponder: TfrmIFFTransponder;

implementation

uses uT3MountedIFF, tttData, uT3clientManager, uT3PlatformInstance,
  uGameData_TTT, uGlobalVar ;


{$R *.dfm}

{ TfrmIFFTransponder }

procedure TfrmIFFTransponder.btnIFFOnClick(Sender: TObject);
var
  r : TRecCmd_Sensor;
begin
  inherited;
  if Assigned(FControlledSensor) and (FControlledSensor is TT3MountedIFF) and
     not (TT3MountedIFF(FControlledSensor).OperationalStatus = sopDamage) then
  begin
    with TT3MountedIFF(FControlledSensor) do
    begin
      case TSpeedButton(Sender).Tag of
        1: //TransponderOperateStatus  := sopOn;
          begin
            r.SensorType := CSENSOR_TYPE_IFF;
            r.SensorID   := InstanceIndex;
            r.PlatformID := PlatformParentInstanceIndex;
            r.OrderID    := IFFInterrogator.IFFType;
            r.OrderParam := byte(sopOn);

            clientManager.NetCmdSender.CmdSensor(r);
          end;
        2: //TransponderOperateStatus  := sopOff;
          begin
            r.SensorType := CSENSOR_TYPE_IFF;
            r.SensorID   := InstanceIndex;
            r.PlatformID := PlatformParentInstanceIndex;
            r.OrderID    := IFFInterrogator.IFFType;
            r.OrderParam := byte(sopOffIFF);

            clientManager.NetCmdSender.CmdSensor(r);
          end;
      end;
    end;
  end;

end;

procedure TfrmIFFTransponder.edtTransponderOnKeyPressed(Sender: TObject;
  var Key: Char);
var
  ValKey : set of AnsiChar;
  Value : integer;
  tmp : string;
begin
  inherited;
  ValKey := [#48 .. #55, #8, #13];
  if not(CharInSet(Key, ValKey)) then
    Key := #0;

  if Key = #13 then
  begin

    if Assigned(FControlledSensor) and (FControlledSensor is TT3MountedIFF) and
       not (TT3MountedIFF(FControlledSensor).OperationalStatus = sopDamage) then
    begin
      with TT3MountedIFF(FControlledSensor) do
      begin
        if TryStrToInt(TEdit(Sender).Text, Value) then
        begin
          tmp := OctToDec(TEdit(Sender).Text);
          Value := StrToInt(tmp);
        end;

        case TEdit(Sender).Tag of
          1 :
          begin
            //TransponderMode1Enabled := TCheckBox(Sender).Checked;
            TransponderMode1 := Value;
            clientManager.NetCmdSender.CmdSensorIFF(
                  PlatformParentInstanceIndex,
                  0,
                  InstanceIndex,
                  1,TransponderMode1Enabled, TransponderMode1);
          end;
          2 :
          begin
            // TransponderMode2Enabled := TCheckBox(Sender).Checked;
            TransponderMode2 := Value;
            clientManager.NetCmdSender.CmdSensorIFF(
                  PlatformParentInstanceIndex,
                  0,
                  InstanceIndex,
                  2,TransponderMode2Enabled, TransponderMode2);
          end;
          3 :
          begin
            //TransponderMode3Enabled := TCheckBox(Sender).Checked;
            TransponderMode3 := Value;
            clientManager.NetCmdSender.CmdSensorIFF(
                  PlatformParentInstanceIndex,
                  0,
                  InstanceIndex,
                  3,TransponderMode3Enabled, TransponderMode3);
          end;
        end;
      end;
    end;
  end;

end;

procedure TfrmIFFTransponder.OnIFFCheckedClick(Sender: TObject);
begin
  inherited;
  if Assigned(FControlledSensor) and (FControlledSensor is TT3MountedIFF) and
     not (TT3MountedIFF(FControlledSensor).OperationalStatus = sopDamage) then
  begin
    with TT3MountedIFF(FControlledSensor) do
    begin

      case TCheckBox(Sender).Tag of
        1:
        begin
          TransponderMode1Enabled := TCheckBox(Sender).Checked;
          clientManager.NetCmdSender.CmdSensorIFF(
                PlatformParentInstanceIndex,
                0,
                InstanceIndex,
                1,TransponderMode1Enabled, TransponderMode1);
        end;
        2:
        begin

          TransponderMode2Enabled := TCheckBox(Sender).Checked;
          clientManager.NetCmdSender.CmdSensorIFF(
                PlatformParentInstanceIndex,
                0,
                InstanceIndex,
                2,TransponderMode2Enabled, TransponderMode2);
        end;
        3:
        begin

          TransponderMode3Enabled := TCheckBox(Sender).Checked;
          clientManager.NetCmdSender.CmdSensorIFF(
                PlatformParentInstanceIndex,
                0,
                InstanceIndex,
                3,TransponderMode3Enabled, TransponderMode3);
        end;
        4:
        begin

          TransponderMode3CEnabled := TCheckBox(Sender).Checked;
          clientManager.NetCmdSender.CmdSensorIFF(
                PlatformParentInstanceIndex,
                0,
                InstanceIndex,
                8,TransponderMode3CEnabled, TransponderMode3C);

        end;
        5:
        begin

          TransponderMode4Enabled := TCheckBox(Sender).Checked;
          clientManager.NetCmdSender.CmdSensorIFF(
                PlatformParentInstanceIndex,
                0,
                InstanceIndex,
                9,TransponderMode4Enabled, 0);
        end;
      end;
    end;
  end;
end;

procedure TfrmIFFTransponder.SetControlledSensor(aObj: TObject);
begin
  inherited;

end;

procedure TfrmIFFTransponder.UpdateForm;
begin
  inherited;
  if Assigned(FControlledSensor) then
    with TT3MountedIFF(FControlledSensor) do
    begin
      case TransponderOperateStatus of
        sopOn:
          begin
            sbIFFTransponderControlModeOn.Enabled := true;
            sbIFFTransponderControlModeOff.Enabled := true;
            sbIFFTransponderControlModeOn.Down := true;

            cbIFFTransponderControlMode1.Enabled := True;
            cbIFFTransponderControlMode2.Enabled := True;
            cbIFFTransponderControlMode3.Enabled := True;
            cbIFFTransponderControlMode3C.Enabled := True;
            cbIFFTransponderControlMode4.Enabled := True;

            edtIFFTransponderControlMode1.Enabled := True;
            edtIFFTransponderControlMode2.Enabled := True;
            edtIFFTransponderControlMode3.Enabled := True;
          end;
        sopOff, sopOffIFF :
          begin
            sbIFFTransponderControlModeOn.Enabled := true;
            sbIFFTransponderControlModeOff.Enabled := true;
            sbIFFTransponderControlModeOff.Down := true;

            cbIFFTransponderControlMode1.Enabled := False;
            cbIFFTransponderControlMode2.Enabled := False;
            cbIFFTransponderControlMode3.Enabled := False;
            cbIFFTransponderControlMode3C.Enabled := False;
            cbIFFTransponderControlMode4.Enabled := False;

            edtIFFTransponderControlMode1.Enabled := False;
            edtIFFTransponderControlMode2.Enabled := False;
            edtIFFTransponderControlMode3.Enabled := False;
          end;
        sopDamage, sopTooDeep, sopEMCON:
          begin
            sbIFFTransponderControlModeOn.Enabled := false;
            sbIFFTransponderControlModeOff.Enabled := false;
          end;
      end;

//      cbIFFTransponderControlMode1.OnClick:=nil;
//      cbIFFTransponderControlMode2.OnClick:=nil;
//      cbIFFTransponderControlMode3.OnClick:=nil;
//      cbIFFTransponderControlMode3c.OnClick:=nil;
//      cbIFFTransponderControlMode4.OnClick:=nil;

      cbIFFTransponderControlMode1.Checked  := TransponderMode1Enabled;
      cbIFFTransponderControlMode2.Checked  := TransponderMode2Enabled;
      cbIFFTransponderControlMode3.Checked  := TransponderMode3Enabled;
      cbIFFTransponderControlMode3C.Checked := TransponderMode3CEnabled;
      cbIFFTransponderControlMode4.Checked  := TransponderMode4Enabled;

//      cbIFFTransponderControlMode1.OnClick:=OnIFFCheckedClick;
//      cbIFFTransponderControlMode2.OnClick:=OnIFFCheckedClick;
//      cbIFFTransponderControlMode3.OnClick:=OnIFFCheckedClick;
//      cbIFFTransponderControlMode3c.OnClick:=OnIFFCheckedClick;
//      cbIFFTransponderControlMode4.OnClick:=OnIFFCheckedClick;

      edtIFFTransponderControlMode1.Text  := DecToOct(IntToStr(TransponderMode1));
      edtIFFTransponderControlMode2.Text  := DecToOct(IntToStr(TransponderMode2));
      edtIFFTransponderControlMode3.Text  := DecToOct(IntToStr(TransponderMode3));
    end;
end;

end.
