unit ufrmSensorType;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs;

type
  TfrmSensorType = class(TForm)
  private
    { Private declarations }
  protected
    FControlledSensor : TObject;
  public
    { Public declarations }
    procedure UpdateForm; virtual;
    procedure SetControlledSensor(aObj : TObject); virtual;
  end;

var
  frmSensorType: TfrmSensorType;

implementation

{$R *.dfm}

{ TfrmSensorType }

procedure TfrmSensorType.SetControlledSensor(aObj: TObject);
begin
  FControlledSensor := aObj;
  UpdateForm;
end;

procedure TfrmSensorType.UpdateForm;
begin

end;

end.
