unit ufrmElectroOptical;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Buttons, ExtCtrls, StdCtrls, ufrmSensorType;

type
  TfrmElectroOptical = class(TfrmSensorType)
    grbElectroOpticalSensor: TGroupBox;
    btnElectroOpticalSensorExecuteSingleScan: TButton;
    ScrollBox3: TScrollBox;
    Label1: TLabel;
    Bevel1: TBevel;
    Label2: TLabel;
    Label3: TLabel;
    Bevel2: TBevel;
    Label4: TLabel;
    Label5: TLabel;
    sbElectroOpticalSensorDisplayRangeShow: TSpeedButton;
    sbElectroOpticalSensorDisplayRangeHide: TSpeedButton;
    sbElectroOpticalSensorBlindZoneShow: TSpeedButton;
    sbElectroOpticalSensorBlindZoneHide: TSpeedButton;
    sbElectroOpticalSensorControlModeOn: TSpeedButton;
    sbElectroOpticalSensorControlModeOff: TSpeedButton;
    procedure OnEOBtnClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure UpdateForm; override;
    procedure SetControlledSensor(aObj : TObject); override;
  end;

var
  frmElectroOptical: TfrmElectroOptical;

implementation

uses
  uT3MountedEO, tttData, uGameData_TTT, uT3ClientManager;

{$R *.dfm}

procedure TfrmElectroOptical.OnEOBtnClick(Sender: TObject);
var
  r : TRecCmd_Sensor;
  sensor : TT3MountedEO;
begin
  inherited;

  if Assigned(FControlledSensor) and (FControlledSensor is TT3MountedEO) and
     not (TT3MountedEO(FControlledSensor).OperationalStatus = sopDamage) then

    with TT3MountedEO(FControlledSensor) do
    begin

      case TSpeedButton(Sender).Tag of
        1:
        begin

          r.SensorType := CSENSOR_TYPE_EO;
          r.SensorID   := InstanceIndex;
          r.PlatformID := PlatformParentInstanceIndex;
          r.OrderID    := CORD_ID_OperationalStatus;
          r.OrderParam := Byte(sopOn);

          clientManager.NetCmdSender.CmdSensor(r);

        end;
        2:
        begin
          r.SensorType := CSENSOR_TYPE_EO;
          r.SensorID   := InstanceIndex;
          r.PlatformID := PlatformParentInstanceIndex;
          r.OrderID    := CORD_ID_OperationalStatus;
          r.OrderParam := Byte(sopOff);

          clientManager.NetCmdSender.CmdSensor(r);
        end;
        3:
        begin
          //FVisibleShowRange := True;
          ShowRange  := True;

          //ShowRangeSelected := ShowRange and FVisibleShowRange;

          //HideRangeWeapon;
        end;
        4:
        begin
          //FVisibleShowRange := False;
          ShowRange  := False;

          //ShowRangeSelected := ShowRange and FVisibleShowRange;
        end;
        5:
        begin
          //FVisibleShowBlind := True;

          ShowBlindZone := True;
          //ShowBlindSelected := ShowBlindZone and FVisibleShowBlind;
          //HideBlindWeapon;
        end;
        6:
        begin
          //FVisibleShowBlind := False;

          ShowBlindZone := False;
          //ShowBlindSelected := ShowBlindZone and FVisibleShowBlind;
        end;
      end;
    end;
end;

procedure TfrmElectroOptical.SetControlledSensor(aObj: TObject);
begin
  inherited;

  if Assigned(FControlledSensor) then
    with TT3MountedEO(FControlledSensor) do
    begin
      sbElectroOpticalSensorBlindZoneShow.Enabled := BlindZones.Count > 0;
      sbElectroOpticalSensorBlindZoneHide.Enabled := BlindZones.Count > 0;

      if EODefinition.Num_FC_Channels = 0 then
      begin
        sbElectroOpticalSensorControlModeOff.Caption := 'Down';
        sbElectroOpticalSensorControlModeOn.Caption := 'Up';
        Label2.Caption := 'Periscope';
      end
      else if EODefinition.Num_FC_Channels = 1  then
      begin
        sbElectroOpticalSensorControlModeOff.Caption := 'Off';
        sbElectroOpticalSensorControlModeOn.Caption := 'On';
        Label2.Caption := 'Laser/IR Sensor (Infrared)';
      end;

      if ShowBlindZone then
        sbElectroOpticalSensorBlindZoneShow.Down := True
      else
        sbElectroOpticalSensorBlindZoneHide.Down := True;

      if ShowRange then
        sbElectroOpticalSensorDisplayRangeShow.Down := True
      else
        sbElectroOpticalSensorDisplayRangeHide.Down := True;
    end;
end;

procedure TfrmElectroOptical.UpdateForm;
begin
  inherited;

  if Assigned(FControlledSensor) then
    with TT3MountedEO(FControlledSensor) do
    begin
      case OperationalStatus of
        sopOn:
          begin
            sbElectroOpticalSensorControlModeOn.Down := true;
            sbElectroOpticalSensorControlModeOn.Enabled := true;
            sbElectroOpticalSensorControlModeOff.Down := false;
            sbElectroOpticalSensorControlModeOff.Enabled := true;
          end;
        sopOff:
          begin
            sbElectroOpticalSensorControlModeOn.Down := false;
            sbElectroOpticalSensorControlModeOn.Enabled := true;
            sbElectroOpticalSensorControlModeOff.Down := true;
            sbElectroOpticalSensorControlModeOff.Enabled := true;
          end;
        sopTooDeep:
          begin
            sbElectroOpticalSensorControlModeOn.Enabled := false;
            sbElectroOpticalSensorControlModeOff.Enabled := false;
          end;
        sopDamage:
          begin
            sbElectroOpticalSensorControlModeOn.Enabled := false;
            sbElectroOpticalSensorControlModeOff.Enabled := false;
          end;
      end;
    end;
end;

end.
