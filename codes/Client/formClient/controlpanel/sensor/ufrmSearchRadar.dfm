object frmSearchRadar: TfrmSearchRadar
  Left = 0
  Top = 0
  Caption = 'frmSearchRadar'
  ClientHeight = 328
  ClientWidth = 308
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object grbSearchRadarControl: TGroupBox
    Left = 0
    Top = 0
    Width = 308
    Height = 328
    Align = alClient
    TabOrder = 0
    object ScrollBox1: TScrollBox
      Left = 2
      Top = 15
      Width = 304
      Height = 311
      Align = alClient
      BevelInner = bvNone
      BevelOuter = bvNone
      BorderStyle = bsNone
      TabOrder = 0
      object Label189: TLabel
        Left = 3
        Top = 0
        Width = 35
        Height = 13
        Caption = 'Control'
      end
      object Bevel28: TBevel
        Left = 43
        Top = 7
        Width = 190
        Height = 3
      end
      object Label193: TLabel
        Left = 44
        Top = 18
        Width = 26
        Height = 13
        Caption = 'Mode'
      end
      object Label194: TLabel
        Left = 243
        Top = 18
        Width = 28
        Height = 13
        Caption = 'ECCM'
      end
      object Label261: TLabel
        Left = 3
        Top = 192
        Width = 34
        Height = 13
        Caption = 'Display'
      end
      object Bevel31: TBevel
        Left = 51
        Top = 199
        Width = 192
        Height = 3
      end
      object Label262: TLabel
        Left = 43
        Top = 208
        Width = 31
        Height = 13
        Caption = 'Range'
      end
      object Label263: TLabel
        Left = 132
        Top = 208
        Width = 54
        Height = 13
        Caption = 'Blind Zones'
      end
      object Label264: TLabel
        Left = 225
        Top = 208
        Width = 57
        Height = 13
        Caption = 'Scan Sector'
        Visible = False
      end
      object btnControlComboInterval: TSpeedButton
        Left = 270
        Top = 96
        Width = 23
        Height = 22
        Enabled = False
        Glyph.Data = {
          D6050000424DD605000000000000360000002800000017000000140000000100
          180000000000A005000000000000000000000000000000000000B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B200
          0000B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2000000B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B200
          0000B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2C1C1C1B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2000000B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B200000000
          0000B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2000000000000000000000000C1C1C1B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2000000000000000000000000000000000000B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B200
          0000B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000000000000000000000
          000000000000000000000000C1C1C1B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2000000B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B200000000
          0000000000000000000000000000000000000000000000000000B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2000000000000000000000000000000000000000000000000B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2000000000000000000000000000000000000B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B200
          0000B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000000000000000000000
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2000000B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B200000000
          0000B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B200
          0000B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2000000B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000}
      end
      object sbControlEccmOn: TSpeedButton
        Tag = 12
        Left = 220
        Top = 34
        Width = 76
        Height = 22
        HelpContext = 1
        AllowAllUp = True
        GroupIndex = 3
        Caption = 'On'
        Enabled = False
        OnClick = sbControlModeSearchClick
      end
      object sbControlEccmOff: TSpeedButton
        Tag = 13
        Left = 220
        Top = 54
        Width = 76
        Height = 22
        AllowAllUp = True
        GroupIndex = 3
        Down = True
        Caption = 'Off'
        Enabled = False
        OnClick = sbControlModeSearchClick
      end
      object sbRangeShow: TSpeedButton
        Tag = 1
        Left = 22
        Top = 223
        Width = 76
        Height = 22
        HelpContext = 1
        AllowAllUp = True
        GroupIndex = 4
        Caption = 'Show'
        OnClick = sbControlModeSearchClick
      end
      object sbRangeHide: TSpeedButton
        Tag = 2
        Left = 22
        Top = 243
        Width = 76
        Height = 22
        AllowAllUp = True
        GroupIndex = 4
        Down = True
        Caption = 'Hide'
        OnClick = sbControlModeSearchClick
      end
      object sbBlindShow: TSpeedButton
        Tag = 3
        Left = 120
        Top = 223
        Width = 76
        Height = 22
        HelpContext = 1
        AllowAllUp = True
        GroupIndex = 5
        Caption = 'Show'
        OnClick = sbControlModeSearchClick
      end
      object sbBlindHide: TSpeedButton
        Tag = 4
        Left = 120
        Top = 243
        Width = 76
        Height = 22
        AllowAllUp = True
        GroupIndex = 5
        Down = True
        Caption = 'Hide'
        OnClick = sbControlModeSearchClick
      end
      object sbScanShow: TSpeedButton
        Tag = 5
        Left = 215
        Top = 223
        Width = 76
        Height = 22
        HelpContext = 1
        AllowAllUp = True
        GroupIndex = 6
        Caption = 'Show'
        Visible = False
        OnClick = sbControlModeSearchClick
      end
      object sbScanHide: TSpeedButton
        Tag = 6
        Left = 215
        Top = 243
        Width = 76
        Height = 22
        AllowAllUp = True
        GroupIndex = 6
        Down = True
        Caption = 'Hide'
        Visible = False
        OnClick = sbControlModeSearchClick
      end
      object pnlControlRadar: TPanel
        Left = 14
        Top = 29
        Width = 93
        Height = 68
        BevelOuter = bvNone
        TabOrder = 0
        object btnControlModeOff2: TSpeedButton
          Tag = 11
          Left = 1
          Top = 22
          Width = 81
          Height = 23
          AllowAllUp = True
          GroupIndex = 7
          Down = True
          Caption = 'Off'
          OnClick = sbControlModeSearchClick
        end
        object btnControlModeOn: TSpeedButton
          Tag = 9
          Left = 1
          Top = 2
          Width = 81
          Height = 22
          AllowAllUp = True
          GroupIndex = 7
          Caption = 'On'
          OnClick = sbControlModeSearchClick
        end
      end
      object pnlControlModeRadar2: TPanel
        Left = 14
        Top = 32
        Width = 90
        Height = 65
        BevelOuter = bvNone
        TabOrder = 1
        object sbControlModeSearch: TSpeedButton
          Tag = 9
          Left = 8
          Top = -1
          Width = 74
          Height = 22
          HelpContext = 1
          AllowAllUp = True
          GroupIndex = 7
          Caption = 'Search / Track'
          OnClick = sbControlModeSearchClick
        end
        object sbControlModeTrack: TSpeedButton
          Tag = 10
          Left = 8
          Top = 20
          Width = 74
          Height = 22
          AllowAllUp = True
          GroupIndex = 7
          Caption = 'Track'
          OnClick = sbControlModeSearchClick
        end
        object sbControlModeOff: TSpeedButton
          Tag = 11
          Left = 8
          Top = 40
          Width = 74
          Height = 22
          AllowAllUp = True
          GroupIndex = 7
          Down = True
          Caption = 'Off'
          OnClick = sbControlModeSearchClick
        end
      end
      object pnlScanSector: TPanel
        Left = 3
        Top = 120
        Width = 290
        Height = 79
        BevelOuter = bvNone
        TabOrder = 4
        Visible = False
        object Bevel30: TBevel
          Left = 76
          Top = 6
          Width = 175
          Height = 3
        end
        object Label200: TLabel
          Left = 3
          Top = -1
          Width = 57
          Height = 13
          Caption = 'Scan Sector'
        end
        object Label201: TLabel
          Left = 44
          Top = 13
          Width = 26
          Height = 13
          Caption = 'Mode'
        end
        object sbScanModePartial: TSpeedButton
          Tag = 8
          Left = 20
          Top = 27
          Width = 76
          Height = 22
          AllowAllUp = True
          GroupIndex = 1
          Caption = 'Partial'
          Enabled = False
          OnClick = sbControlModeSearchClick
        end
        object sbScanModeFull: TSpeedButton
          Tag = 7
          Left = 20
          Top = 47
          Width = 76
          Height = 22
          HelpContext = 1
          AllowAllUp = True
          GroupIndex = 1
          Down = True
          Caption = 'Full'
          Enabled = False
          OnClick = sbControlModeSearchClick
        end
        object Label202: TLabel
          Left = 136
          Top = 27
          Width = 31
          Height = 13
          Caption = 'Start :'
        end
        object Label203: TLabel
          Left = 136
          Top = 55
          Width = 25
          Height = 13
          Caption = 'End :'
        end
        object Label205: TLabel
          Left = 211
          Top = 33
          Width = 48
          Height = 13
          Caption = 'degrees T'
        end
        object Label204: TLabel
          Left = 211
          Top = 55
          Width = 48
          Height = 13
          Caption = 'degrees T'
        end
        object btnComboScanStrart: TSpeedButton
          Left = 267
          Top = 26
          Width = 23
          Height = 22
          Enabled = False
          Glyph.Data = {
            D6050000424DD605000000000000360000002800000017000000140000000100
            180000000000A005000000000000000000000000000000000000B2B2B2B2B2B2
            B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
            B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B200
            0000B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
            B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
            B2B2B2B2B2B2B2000000B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
            B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
            B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000B2B2B2B2B2B2B2B2B2B2B2B2B2B2
            B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
            B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000B2B2B2B2B2B2
            B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
            B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B200
            0000B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2C1C1C1B2B2B2B2B2B2
            B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
            B2B2B2B2B2B2B2000000B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B200000000
            0000B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
            B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000B2B2B2B2B2B2B2B2B2B2B2B2B2B2
            B2B2B2B2000000000000000000000000C1C1C1B2B2B2B2B2B2B2B2B2B2B2B2B2
            B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000B2B2B2B2B2B2
            B2B2B2B2B2B2B2B2B2B2B2B2000000000000000000000000000000000000B2B2
            B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B200
            0000B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000000000000000000000
            000000000000000000000000C1C1C1B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
            B2B2B2B2B2B2B2000000B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B200000000
            0000000000000000000000000000000000000000000000000000B2B2B2B2B2B2
            B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000B2B2B2B2B2B2B2B2B2B2B2B2B2B2
            B2B2B2B2000000000000000000000000000000000000000000000000B2B2B2B2
            B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000B2B2B2B2B2B2
            B2B2B2B2B2B2B2B2B2B2B2B2000000000000000000000000000000000000B2B2
            B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B200
            0000B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000000000000000000000
            B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
            B2B2B2B2B2B2B2000000B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B200000000
            0000B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
            B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000B2B2B2B2B2B2B2B2B2B2B2B2B2B2
            B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
            B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000B2B2B2B2B2B2
            B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
            B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B200
            0000B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
            B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
            B2B2B2B2B2B2B2000000B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
            B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
            B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000B2B2B2B2B2B2B2B2B2B2B2B2B2B2
            B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
            B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000}
        end
        object editScanStart: TEdit
          Tag = 1
          Left = 175
          Top = 27
          Width = 30
          Height = 21
          Enabled = False
          TabOrder = 0
          Text = '000'
          OnKeyPress = OnKeyPartialPress
        end
        object editScanEnd: TEdit
          Tag = 2
          Left = 175
          Top = 50
          Width = 30
          Height = 21
          Enabled = False
          TabOrder = 1
          Text = '000'
          OnKeyPress = OnKeyPartialPress
        end
      end
      object cbActivationInterval: TCheckBox
        Left = 24
        Top = 98
        Width = 106
        Height = 17
        Caption = 'Activation Interval'
        Enabled = False
        TabOrder = 3
      end
      object editComboInterval: TEdit
        Left = 136
        Top = 96
        Width = 128
        Height = 21
        Enabled = False
        TabOrder = 2
        Text = 'None'
      end
      object btShowRangeAltitude: TButton
        Left = 22
        Top = 276
        Width = 117
        Height = 20
        Caption = 'Show Range Altitude..'
        Enabled = False
        TabOrder = 5
      end
      object btExecuteSingleScan: TButton
        Left = 185
        Top = 277
        Width = 106
        Height = 20
        Caption = 'Execute Single Scan'
        Enabled = False
        TabOrder = 6
      end
    end
  end
end
