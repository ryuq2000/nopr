unit ufrmSonobuoy;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls,ufrmSensorType, Menus;

type
  TfrmSonobuoy = class(TfrmSensorType)
    grbSonobuoyControl: TGroupBox;
    ScrollBox7: TScrollBox;
    Bevel10: TBevel;
    Bevel11: TBevel;
    Bevel12: TBevel;
    btnSonobuoyControlCombo: TSpeedButton;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    Label22: TLabel;
    Label23: TLabel;
    Label24: TLabel;
    Label25: TLabel;
    Label26: TLabel;
    Label27: TLabel;
    lblSonobuoyMonitorCurrently: TLabel;
    lblSonobuoyMonitorPlatform: TLabel;
    lblStatusSonobuoy: TLabel;
    lbStatusQuantity: TLabel;
    btnSonobuoyControlDeploy: TButton;
    editControlDepth: TEdit;
    editControlMode: TEdit;
    pmModeSonobuoy: TPopupMenu;
  private
    { Private declarations }
    procedure OnMenuSonobuoyClick(Sender : TObject);
  public
    { Public declarations }
    procedure UpdateForm; override;
    procedure SetControlledSensor(aObj : TObject); override;
  end;

var
  frmSonobuoy: TfrmSonobuoy;

implementation

uses
  uT3MountedSonobuoy, uDBClassDefinition, uGlobalVar;

{$R *.dfm}

{ TfrmSonobuoy }

procedure TfrmSonobuoy.OnMenuSonobuoyClick(Sender: TObject);
begin
  if TMenuItem(sender).Caption = '&Active' then
    editControlMode.Text := 'Active' else
  if TMenuItem(sender).Caption = '&Passive'  then
     editControlMode.Text := 'Passive';

end;

procedure TfrmSonobuoy.SetControlledSensor(aObj: TObject);
var
  menuItem    : TMenuItem;
  i : integer;
  snrDef : TDBSonar_Definition;
begin
  inherited;

  if Assigned(FControlledSensor) then
    with TT3MountedSonobuoy(FControlledSensor) do
    begin
      lbStatusQuantity.Caption := IntToStr(Quantity);
      editControlDepth.Text := FloatToStr(ControlDepth);

      pmModeSonobuoy.Items.Clear;

      snrDef := simManager.Scenario.GetDBSonarDefinition(SonobuoyDefinition.Sonar_Index);

      if ASsigned(snrDef) then
      begin
        if snrDef.Sonar_Classification = 0 then
        begin
          editControlMode.Text := 'Active';
          menuItem := TMenuItem.Create(nil);
          menuItem.Caption :='Active';
          menuItem.Tag     := 0;
          menuItem.OnClick := onMenuSonobuoyClick;
          pmModeSonobuoy.Items.Add(menuItem);
        end
        else if snrDef.Sonar_Classification = 1 then
        begin
          editControlMode.Text := 'Passive';
          menuItem := TMenuItem.Create(nil);
          menuItem.Caption :='Passive';
          menuItem.Tag     := 0;
          menuItem.OnClick := onMenuSonobuoyClick;
          pmModeSonobuoy.Items.Add(menuItem);
        end
        else if snrDef.Sonar_Classification = 2 then
        begin
          if ControlMode = 1 then
            editControlMode.Text := 'Active'
          else
            editControlMode.Text := 'Passive';

          for I := 0 to 1 do
          begin
            menuItem := TMenuItem.Create(nil);

            if i = 0 then
              menuItem.Caption :='Active' else
            if i = 1 then
              menuItem.Caption :='Passive';

            menuItem.Tag     := i;
            menuItem.OnClick := onMenuSonobuoyClick;
            pmModeSonobuoy.Items.Add(menuItem);
          end;
        end
        else if snrDef.Sonar_Classification = 3 then
        begin
          editControlMode.Text := 'Passive';
          menuItem := TMenuItem.Create(nil);
          menuItem.Caption :='Passive';
          menuItem.Tag     := 0;
          menuItem.OnClick := onMenuSonobuoyClick;
          pmModeSonobuoy.Items.Add(menuItem);
        end;
      end;

      lblSonobuoyMonitorCurrently.Caption := IntToStr(MonitoringQuantity);

//      {Menentukan banyak sonobuoy yg dimonitor}
//      with simMgrClient do begin
//
//        for J := 0 to SimPlatforms.itemCount - 1 do begin
//          o := SimPlatforms.getObject(J);
//
//          if o is TT3Sonobuoy then begin
//            if (TT3Sonobuoy(o).InstanceName = TT3SonobuoyOnVehicle(sonobuoy).InstanceName) and
//               (TT3Sonobuoy(o).ParentIndex = TT3PlatformInstance(v).InstanceIndex) then
//            begin
//              inc(Jum)
//            end;
//          end;
//        end;
//      end;

//      lblSonobuoyMonitorCurrently.Caption := IntToStr(Jum);

    end;

end;

procedure TfrmSonobuoy.UpdateForm;
begin
  inherited;

  if Assigned(FControlledSensor) then
    with TT3MountedSonobuoy(FControlledSensor) do
    begin
      editControlDepth.Text := FloatToStr(ControlDepth);
      lbStatusQuantity.Caption := IntToStr(Quantity);
      lblSonobuoyMonitorCurrently.Caption := IntToStr(MonitoringQuantity);
    end;

end;

end.
