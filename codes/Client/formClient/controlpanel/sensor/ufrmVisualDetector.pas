unit ufrmVisualDetector;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Buttons, ExtCtrls, StdCtrls, ufrmSensorType, uT3MountedVisual;

type
  TfrmVisualDetector = class(TfrmSensorType)
    grbVisualDetectorSensor: TGroupBox;
    Label257: TLabel;
    Bevel51: TBevel;
    Label258: TLabel;
    Label259: TLabel;
    sbVisualDetectorDisplayRangeShow: TSpeedButton;
    sbVisualDetectorDisplayRangeHide: TSpeedButton;
    sbVisualDetectorDisplayBlindZonesShow: TSpeedButton;
    sbVisualDetectorDisplayBlindZonesHide: TSpeedButton;
    procedure OnButtonClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure UpdateForm; override;
    procedure SetControlledSensor(aObj : TObject); override;
  end;

var
  frmVisualDetector: TfrmVisualDetector;

implementation

uses tttData;

{$R *.dfm}

{ TfrmVisualDetector }

procedure TfrmVisualDetector.OnButtonClick(Sender: TObject);
begin
  inherited;

  if Assigned(FControlledSensor) and (FControlledSensor is TT3MountedVisual) and
     not (TT3MountedVisual(FControlledSensor).OperationalStatus = sopDamage) then
  begin
    with TT3MountedVisual(FControlledSensor) do
    begin
      case TSpeedButton(Sender).Tag of
        1:
        begin
          if sbVisualDetectorDisplayRangeShow.Down = False then
            sbVisualDetectorDisplayRangeShow.Down := True
          else
          begin
            //FVisibleShowRange := True;
            ShowRange := True;
            //ShowRangeSelected := ShowRange and FVisibleShowRange;
            //HideRangeWeapon;
          end;
        end;
        2:
        begin
          if sbVisualDetectorDisplayRangeHide.Down = False then
            sbVisualDetectorDisplayRangeHide.Down := True
          else
          begin
            //FVisibleShowRange := False;
            ShowRange := False;
            //ShowRangeSelected := ShowRange and FVisibleShowRange;
          end;
        end;
        3:
        begin
          if sbVisualDetectorDisplayBlindZonesShow.Down = False then
            sbVisualDetectorDisplayBlindZonesShow.Down := True
          else
          begin
            //FVisibleShowBlind := True;
            ShowBlindZone := True;
            //ShowBlindSelected := ShowBlindZone and FVisibleShowBlind;
            //HideBlindWeapon;
          end;
        end;
        4:
        begin
          if sbVisualDetectorDisplayBlindZonesHide.Down = False then
            sbVisualDetectorDisplayBlindZonesHide.Down := True
          else
          begin
            //FVisibleShowBlind := False;
            ShowBlindZone := False;
            //ShowBlindSelected := ShowBlindZone and FVisibleShowBlind;
          end;
        end;
      end;
    end;
  end;

end;

procedure TfrmVisualDetector.SetControlledSensor(aObj: TObject);
begin
  inherited;

  if Assigned(FControlledSensor) then
  with TT3MountedVisual(FControlledSensor) do
  begin
    sbVisualDetectorDisplayBlindZonesShow.Enabled := BlindZones.Count > 0;
    sbVisualDetectorDisplayBlindZonesHide.Enabled := BlindZones.Count > 0;

    if ShowRange then
      begin
        sbVisualDetectorDisplayRangeShow.Down := true;
      end
    else
      begin
        sbVisualDetectorDisplayRangeHide.Down := true;
      end;

    if ShowBlindZone then
      sbVisualDetectorDisplayBlindZonesShow.Down := true
    else
      sbVisualDetectorDisplayBlindZonesHide.Down := true;
  end;

end;

procedure TfrmVisualDetector.UpdateForm;
begin
  inherited;
end;

end.
