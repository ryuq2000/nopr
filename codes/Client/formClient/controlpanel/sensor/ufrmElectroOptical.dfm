object frmElectroOptical: TfrmElectroOptical
  Left = 0
  Top = 0
  Caption = 'frmElectroOptical'
  ClientHeight = 290
  ClientWidth = 327
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object grbElectroOpticalSensor: TGroupBox
    Left = 0
    Top = 0
    Width = 327
    Height = 290
    Align = alClient
    TabOrder = 0
    object btnElectroOpticalSensorExecuteSingleScan: TButton
      Left = 145
      Top = 367
      Width = 153
      Height = 25
      Caption = 'Execute Single Scan'
      TabOrder = 0
    end
    object ScrollBox3: TScrollBox
      Left = 2
      Top = 15
      Width = 323
      Height = 273
      Align = alClient
      BevelInner = bvNone
      BevelOuter = bvNone
      BorderStyle = bsNone
      TabOrder = 1
      object Label1: TLabel
        Left = 3
        Top = 3
        Width = 35
        Height = 13
        Caption = 'Control'
      end
      object Bevel1: TBevel
        Left = 43
        Top = 10
        Width = 260
        Height = 3
      end
      object Label2: TLabel
        Left = 21
        Top = 21
        Width = 46
        Height = 13
        Caption = 'Periscope'
      end
      object Label3: TLabel
        Left = 3
        Top = 90
        Width = 34
        Height = 13
        Caption = 'Display'
      end
      object Bevel2: TBevel
        Left = 45
        Top = 97
        Width = 260
        Height = 3
      end
      object Label4: TLabel
        Left = 153
        Top = 115
        Width = 57
        Height = 13
        Caption = 'Blind Zones '
      end
      object Label5: TLabel
        Left = 56
        Top = 115
        Width = 31
        Height = 13
        Caption = 'Range'
      end
      object sbElectroOpticalSensorDisplayRangeShow: TSpeedButton
        Left = 38
        Top = 134
        Width = 76
        Height = 22
        HelpContext = 1
        AllowAllUp = True
        GroupIndex = 1
        Caption = 'Show'
        OnClick = OnEOBtnClick
      end
      object sbElectroOpticalSensorDisplayRangeHide: TSpeedButton
        Left = 38
        Top = 155
        Width = 76
        Height = 22
        AllowAllUp = True
        GroupIndex = 1
        Down = True
        Caption = 'Hide'
        OnClick = OnEOBtnClick
      end
      object sbElectroOpticalSensorBlindZoneShow: TSpeedButton
        Left = 142
        Top = 134
        Width = 76
        Height = 22
        HelpContext = 1
        AllowAllUp = True
        GroupIndex = 2
        Caption = 'Show'
        OnClick = OnEOBtnClick
      end
      object sbElectroOpticalSensorBlindZoneHide: TSpeedButton
        Left = 142
        Top = 155
        Width = 76
        Height = 22
        AllowAllUp = True
        GroupIndex = 2
        Down = True
        Caption = 'Hide'
        OnClick = OnEOBtnClick
      end
      object sbElectroOpticalSensorControlModeOn: TSpeedButton
        Left = 7
        Top = 38
        Width = 76
        Height = 22
        HelpContext = 1
        AllowAllUp = True
        GroupIndex = 3
        Caption = 'Up'
        OnClick = OnEOBtnClick
      end
      object sbElectroOpticalSensorControlModeOff: TSpeedButton
        Left = 7
        Top = 59
        Width = 76
        Height = 22
        AllowAllUp = True
        GroupIndex = 3
        Down = True
        Caption = 'Down'
        OnClick = OnEOBtnClick
      end
    end
  end
end
