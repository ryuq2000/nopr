unit ufrmSearchRadar;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, Buttons,ufrmSensorType;

type
  TfrmSearchRadar = class(TfrmSensorType)
    grbSearchRadarControl: TGroupBox;
    ScrollBox1: TScrollBox;
    Label189: TLabel;
    Bevel28: TBevel;
    Label193: TLabel;
    Label194: TLabel;
    Label261: TLabel;
    Bevel31: TBevel;
    Label262: TLabel;
    Label263: TLabel;
    Label264: TLabel;
    btnControlComboInterval: TSpeedButton;
    sbControlEccmOn: TSpeedButton;
    sbControlEccmOff: TSpeedButton;
    sbRangeShow: TSpeedButton;
    sbRangeHide: TSpeedButton;
    sbBlindShow: TSpeedButton;
    sbBlindHide: TSpeedButton;
    sbScanShow: TSpeedButton;
    sbScanHide: TSpeedButton;
    pnlScanSector: TPanel;
    Bevel30: TBevel;
    Label200: TLabel;
    Label201: TLabel;
    sbScanModePartial: TSpeedButton;
    sbScanModeFull: TSpeedButton;
    Label202: TLabel;
    Label203: TLabel;
    Label205: TLabel;
    Label204: TLabel;
    btnComboScanStrart: TSpeedButton;
    editScanStart: TEdit;
    editScanEnd: TEdit;
    pnlControlRadar: TPanel;
    btnControlModeOff2: TSpeedButton;
    btnControlModeOn: TSpeedButton;
    pnlControlModeRadar2: TPanel;
    sbControlModeSearch: TSpeedButton;
    sbControlModeTrack: TSpeedButton;
    sbControlModeOff: TSpeedButton;
    cbActivationInterval: TCheckBox;
    editComboInterval: TEdit;
    btShowRangeAltitude: TButton;
    btExecuteSingleScan: TButton;
    procedure sbControlModeSearchClick(Sender: TObject);
    procedure OnKeyPartialPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure UpdateForm; override;
    procedure SetControlledSensor(aObj : TObject); override;
  end;

var
  frmSearchRadar: TfrmSearchRadar;

implementation

uses
  uT3MountedRadar, tttData, uT3ClientManager, uGameData_TTT;
{$R *.dfm}

{ TfrmSearchRadar }

procedure TfrmSearchRadar.OnKeyPartialPress(Sender: TObject; var Key: Char);
begin
  inherited;
//
end;

procedure TfrmSearchRadar.sbControlModeSearchClick(Sender: TObject);
var
  r : TRecCmd_Sensor;
begin
  inherited;

  if Assigned(FControlledSensor) and (FControlledSensor is TT3MountedRadar) and
     not (TT3MountedRadar(FControlledSensor).OperationalStatus = sopDamage) then
  begin
    with TT3MountedRadar(FControlledSensor) do
    begin
      case TSpeedButton(Sender).Tag of
        1:
        begin
          if sbRangeShow.Down = False then
            sbRangeShow.Down := True
          else
          begin
            //FVisibleShowRange := True;
            ShowRange := True;
            //ShowRangeSelected := ShowRange and FVisibleShowRange;
            //HideRangeWeapon;
          end;
        end;
        2:
        begin
          if sbRangeHide.Down = False then
            sbRangeHide.Down := True
          else
          begin
            //FVisibleShowRange := False;
            ShowRange := False;
            //ShowRangeSelected := ShowRange and FVisibleShowRange;
          end;
        end;
        3:
        begin
          if sbBlindShow.Down = False then
            sbBlindShow.Down := True
          else
          begin
            //FVisibleShowBlind := True;
            ShowBlindZone := True;
            //ShowBlindSelected := ShowBlindZone and FVisibleShowBlind;
            //HideBlindWeapon;
          end;
        end;
        4:
        begin
          if sbBlindHide.Down = False then
            sbBlindHide.Down := True
          else
          begin
            //FVisibleShowBlind := False;
            ShowBlindZone := False;
            //ShowBlindSelected := ShowBlindZone and FVisibleShowBlind;
          end;
        end;
        5:
        begin
          if sbScanShow.Down = False then
            sbScanShow.Down := True
          else
          begin
            ShowScanSector  := true;
          end;
        end;
        6:
        begin
          if sbScanHide.Down = False then
            sbScanHide.Down := True
          else
          begin
            ShowScanSector  := false;
          end;
        end;
        7:
        begin

          r.SensorType := CSENSOR_TYPE_RADAR;
          r.SensorID   := InstanceIndex;
          r.PlatformID := PlatformParentInstanceIndex;
          r.OrderID    := CORD_ID_PartialMode;
          r.OrderParam := Byte(rpmFull);

          clientManager.NetCmdSender.CmdSensor(r);

          if sbScanModeFull.Down = False then
            sbScanModeFull.Down := True;
        end;
        8:
        begin
          r.SensorType := CSENSOR_TYPE_RADAR;
          r.SensorID   := InstanceIndex;
          r.PlatformID := PlatformParentInstanceIndex;
          r.OrderID    := CORD_ID_PartialMode;
          r.OrderParam := Byte(rpmPartial);

          clientManager.NetCmdSender.CmdSensor(r);

          if sbScanModePartial.Down = False then
            sbScanModePartial.Down := True;
        end;
        9:
        begin
          r.SensorType := CSENSOR_TYPE_RADAR;
          r.SensorID   := InstanceIndex;
          r.PlatformID := PlatformParentInstanceIndex;
          r.OrderID    := CORD_ID_ControlMode;
          r.OrderParam := Byte(rcmSearchTrack);

          clientManager.NetCmdSender.CmdSensor(r);

        end;
        10:
        begin
          r.SensorType := CSENSOR_TYPE_RADAR;
          r.SensorID   := InstanceIndex;
          r.PlatformID := PlatformParentInstanceIndex;
          r.OrderID    := CORD_ID_ControlMode;
          r.OrderParam := Byte(rcmTrack);

          clientManager.NetCmdSender.CmdSensor(r);
        end;
        11:
        begin
          r.SensorType := CSENSOR_TYPE_RADAR;
          r.SensorID   := InstanceIndex;
          r.PlatformID := PlatformParentInstanceIndex;
          r.OrderID    := CORD_ID_ControlMode;
          r.OrderParam := Byte(rcmOff);

          clientManager.NetCmdSender.CmdSensor(r);
        end;
        12:
        begin
          r.SensorType := CSENSOR_TYPE_RADAR;
          r.SensorID   := InstanceIndex;
          r.PlatformID := PlatformParentInstanceIndex;
          r.OrderID    := CORD_ID_ECCM;
          r.OrderParam := Byte(remOn);

          clientManager.NetCmdSender.CmdSensor(r);
        end;
        13:
        begin
          r.SensorType := CSENSOR_TYPE_RADAR;
          r.SensorID   := InstanceIndex;
          r.PlatformID := PlatformParentInstanceIndex;
          r.OrderID    := CORD_ID_ECCM;
          r.OrderParam := Byte(remOff);

          clientManager.NetCmdSender.CmdSensor(r);
        end;
      end;
    end;
  end;

end;

procedure TfrmSearchRadar.SetControlledSensor(aObj: TObject);
begin
  inherited;

  if Assigned(FControlledSensor) then
  with TT3MountedRadar(FControlledSensor) do
  begin
    sbBlindShow.Enabled := BlindZones.Count > 0;
    sbBlindHide.Enabled := BlindZones.Count > 0;

    sbScanModeFull.Enabled      := RadarDefinition.Sector_Scan_Capable;
    sbScanModePartial.Enabled   := RadarDefinition.Sector_Scan_Capable;
    sbScanShow.Enabled          := RadarDefinition.Sector_Scan_Capable;
    sbScanHide.Enabled          := RadarDefinition.Sector_Scan_Capable;
    editScanStart.Enabled       := RadarDefinition.Sector_Scan_Capable;
    editScanEnd.Enabled         := RadarDefinition.Sector_Scan_Capable;
    btnComboScanStrart.Enabled  := RadarDefinition.Sector_Scan_Capable;
  end;

end;

procedure TfrmSearchRadar.UpdateForm;
begin
  inherited;

  if Assigned(FControlledSensor) then
  with TT3MountedRadar(FControlledSensor) do
  begin

//    case RadarDefinition.FType.Radar_Type_Index of
//      0   : begin
//              pnlControlModeRadar2.Visible := False;
//              pnlControlRadar.Visible := True;
//
//            end;
//      2,3 : begin
//              pnlControlModeRadar2.Visible := True;
//              pnlControlRadar.Visible := False;
//            end;
//    end;
//
//    if RadarDefinition.FDef.Sector_Scan_Capable then
//       pnlScanSector.Visible := True
//    else
//       pnlScanSector.Visible := False;

    if ShowRange then
      begin
        sbRangeShow.Down := true;
      end
    else
      begin
        sbRangeHide.Down := true;
      end;

    if ShowBlindZone then
      sbBlindShow.Down := true
    else
      sbBlindHide.Down := true;

    if ShowScanSector then
      sbScanShow.Down := true
    else
      sbScanHide.Down := true;

    if (OperationalStatus <> sopDamage) and (OperationalStatus <> sopOff) then
    begin
      case ControlMode of
        rcmOff:
              begin
                sbControlModeOff.Down := true;
                btnControlModeOff2.Down := true;
              end;
        rcmTrack:
              begin
                sbControlModeTrack.Down := true;
                btnControlModeOn.Down := true;
              end;
        rcmSearchTrack:
              begin
                sbControlModeSearch.Down := true;
                btnControlModeOn.Down := true;
              end;
      end;
    end
    else
    begin
      sbControlModeOff.Down := true;
      btnControlModeOff2.Down := true;
    end;

    case ScanSector of
      rscFull:
        sbScanModeFull.Down := true;
      rscPartial:
        sbScanModePartial.Down := true;
    end;


//    if RadarDefinition.FDef.Sector_Scan_Capable then
//    begin
//      editScanStart.Text := FormatFloat('#.##', StartScan);
//      editScanEnd.Text := FormatFloat('#.##', EndScan);
//      {case ScanSector of
//        rscFull:
//          begin
//            sbScanModeFull.Down := true;
//            editScanStart.Enabled := true;
//            editScanEnd.Enabled := true;
//            btnComboScanStrart.Enabled := true;
//          end;
//        rscPartial:
//          begin
//            sbScanModePartial.Down := true;
//            editScanStart.Enabled := false;
//            editScanEnd.Enabled := false;
//            btnComboScanStrart.Enabled := false;
//          end;
//      end;}
//    end
//    else
//    begin
//      editScanStart.Text := '0';
//      editScanEnd.Text := '0';
//    end;
  end;
end;

end.
