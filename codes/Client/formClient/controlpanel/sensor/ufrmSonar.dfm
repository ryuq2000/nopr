object frmSonar: TfrmSonar
  Left = 0
  Top = 0
  Caption = 'frmSonar'
  ClientHeight = 334
  ClientWidth = 319
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object grbSonarControl: TGroupBox
    Left = 0
    Top = 0
    Width = 319
    Height = 334
    Align = alClient
    TabOrder = 0
    object ScrollBox2: TScrollBox
      Left = 2
      Top = 15
      Width = 315
      Height = 317
      Align = alClient
      BevelInner = bvNone
      BevelOuter = bvNone
      BorderStyle = bsNone
      TabOrder = 0
      object Bevel37: TBevel
        Left = 44
        Top = 4
        Width = 262
        Height = 3
      end
      object Bevel43: TBevel
        Left = 45
        Top = 220
        Width = 265
        Height = 3
      end
      object Label213: TLabel
        Left = 3
        Top = -2
        Width = 35
        Height = 13
        Caption = 'Control'
      end
      object Label214: TLabel
        Left = 43
        Top = 10
        Width = 26
        Height = 13
        Caption = 'Mode'
      end
      object Label237: TLabel
        Left = 3
        Top = 212
        Width = 34
        Height = 13
        Caption = 'Display'
      end
      object Label238: TLabel
        Left = 43
        Top = 231
        Width = 31
        Height = 13
        Caption = 'Range'
      end
      object Label239: TLabel
        Left = 145
        Top = 231
        Width = 54
        Height = 13
        Caption = 'Blind Zones'
      end
      object sbDisplayBlindHide: TSpeedButton
        Tag = 8
        Left = 133
        Top = 267
        Width = 76
        Height = 22
        AllowAllUp = True
        GroupIndex = 4
        Down = True
        Caption = 'Hide'
        OnClick = OnSonarCmdClick
      end
      object sbDisplayBlindShow: TSpeedButton
        Tag = 9
        Left = 133
        Top = 246
        Width = 76
        Height = 22
        HelpContext = 1
        AllowAllUp = True
        GroupIndex = 4
        Caption = 'Show'
        OnClick = OnSonarCmdClick
      end
      object sbDisplayRangeHide: TSpeedButton
        Tag = 6
        Left = 26
        Top = 267
        Width = 76
        Height = 22
        AllowAllUp = True
        GroupIndex = 2
        Down = True
        Caption = 'Hide'
        OnClick = OnSonarCmdClick
      end
      object sbDisplayRangeShow: TSpeedButton
        Tag = 7
        Left = 26
        Top = 246
        Width = 76
        Height = 22
        HelpContext = 1
        AllowAllUp = True
        GroupIndex = 2
        Caption = 'Show'
        OnClick = OnSonarCmdClick
      end
      object sbSonarControlModeActive: TSpeedButton
        Tag = 1
        Left = 22
        Top = 24
        Width = 76
        Height = 22
        HelpContext = 1
        AllowAllUp = True
        GroupIndex = 1
        Caption = 'Active'
        OnClick = OnSonarCmdClick
      end
      object sbSonarControlModeOff: TSpeedButton
        Tag = 3
        Left = 22
        Top = 67
        Width = 76
        Height = 22
        AllowAllUp = True
        GroupIndex = 1
        Down = True
        Caption = 'Off'
        OnClick = OnSonarCmdClick
      end
      object sbSonarControlModePassive: TSpeedButton
        Tag = 2
        Left = 22
        Top = 45
        Width = 76
        Height = 22
        AllowAllUp = True
        GroupIndex = 1
        Caption = 'Passive'
        OnClick = OnSonarCmdClick
      end
      object lblRangeTIOW: TLabel
        Left = 138
        Top = 10
        Width = 64
        Height = 13
        Caption = 'Range (kyds)'
        Visible = False
      end
      object btnRange1: TSpeedButton
        Tag = 10
        Left = 132
        Top = 24
        Width = 76
        Height = 22
        HelpContext = 1
        AllowAllUp = True
        GroupIndex = 3
        Down = True
        Caption = 'Short'
        Visible = False
        OnClick = OnSonarCmdClick
      end
      object btnRAnge2: TSpeedButton
        Tag = 11
        Left = 132
        Top = 44
        Width = 76
        Height = 22
        AllowAllUp = True
        GroupIndex = 3
        Caption = 'Medium'
        Visible = False
        OnClick = OnSonarCmdClick
      end
      object btnRange3: TSpeedButton
        Tag = 12
        Left = 132
        Top = 65
        Width = 76
        Height = 22
        AllowAllUp = True
        GroupIndex = 3
        Caption = 'Long'
        Visible = False
        OnClick = OnSonarCmdClick
      end
      object pnlDeployment: TPanel
        Left = -1
        Top = 89
        Width = 311
        Height = 126
        BevelOuter = bvNone
        TabOrder = 0
        object Bevel39: TBevel
          Left = 65
          Top = 11
          Width = 245
          Height = 3
        end
        object Bevel41: TBevel
          Left = 202
          Top = 26
          Width = 105
          Height = 3
        end
        object Bevel42: TBevel
          Left = 170
          Top = 95
          Width = 140
          Height = 3
        end
        object Label215: TLabel
          Left = 43
          Top = 22
          Width = 30
          Height = 13
          Caption = 'Action'
        end
        object Label216: TLabel
          Left = 3
          Top = 3
          Width = 57
          Height = 13
          Caption = 'Deployment'
        end
        object Label221: TLabel
          Left = 132
          Top = 43
          Width = 47
          Height = 13
          Caption = 'Ordered :'
        end
        object Label222: TLabel
          Left = 132
          Top = 66
          Width = 37
          Height = 13
          Caption = 'Actual :'
        end
        object Label223: TLabel
          Left = 250
          Top = 43
          Width = 33
          Height = 13
          Caption = 'metres'
        end
        object Label224: TLabel
          Left = 250
          Top = 66
          Width = 33
          Height = 13
          Caption = 'metres'
        end
        object Label226: TLabel
          Left = 22
          Top = 92
          Width = 41
          Height = 13
          Caption = 'Status : '
        end
        object Label228: TLabel
          Left = 132
          Top = 109
          Width = 41
          Height = 13
          Caption = 'Settled :'
        end
        object Label229: TLabel
          Left = 132
          Top = 129
          Width = 37
          Height = 13
          Caption = 'Actual :'
        end
        object Label232: TLabel
          Left = 250
          Top = 109
          Width = 33
          Height = 13
          Caption = 'metres'
        end
        object Label233: TLabel
          Left = 250
          Top = 129
          Width = 33
          Height = 13
          Caption = 'metres'
        end
        object Label234: TLabel
          Left = 132
          Top = 149
          Width = 60
          Height = 13
          Caption = 'Tow Speed :'
        end
        object Label236: TLabel
          Left = 250
          Top = 149
          Width = 26
          Height = 13
          Caption = 'knots'
        end
        object LabelCablePayout: TLabel
          Left = 132
          Top = 19
          Width = 64
          Height = 13
          Caption = 'Cable Payout'
        end
        object LabelDepth: TLabel
          Left = 132
          Top = 89
          Width = 29
          Height = 13
          Caption = 'Depth'
        end
        object lbCableActual: TLabel
          Left = 187
          Top = 66
          Width = 12
          Height = 13
          Caption = '---'
        end
        object lbDepthActual: TLabel
          Left = 212
          Top = 129
          Width = 12
          Height = 13
          Caption = '---'
        end
        object lbDepthSettled: TLabel
          Left = 212
          Top = 109
          Width = 12
          Height = 13
          Caption = '---'
        end
        object lbDepthTow: TLabel
          Left = 212
          Top = 129
          Width = 12
          Height = 13
          Caption = '---'
        end
        object lblStatusDeployment: TLabel
          Left = 62
          Top = 92
          Width = 45
          Height = 13
          Caption = 'Deployed'
        end
        object sbDeploymentActiondeploy: TSpeedButton
          Tag = 4
          Left = 22
          Top = 41
          Width = 76
          Height = 22
          HelpContext = 1
          AllowAllUp = True
          Caption = 'Deploy'
          OnClick = OnSonarCmdClick
        end
        object sbDeploymentActionShow: TSpeedButton
          Tag = 5
          Left = 22
          Top = 61
          Width = 76
          Height = 22
          AllowAllUp = True
          Caption = 'Stow'
          OnClick = OnSonarCmdClick
        end
        object editCableOrdered: TEdit
          Left = 187
          Top = 39
          Width = 41
          Height = 21
          TabOrder = 0
          Text = '000'
        end
      end
    end
  end
end
