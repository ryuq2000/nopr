object frmIFFTransponder: TfrmIFFTransponder
  Left = 0
  Top = 0
  Caption = 'frmIFFTransponder'
  ClientHeight = 297
  ClientWidth = 329
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object grbIFFTransponderControl: TGroupBox
    Left = 0
    Top = 0
    Width = 329
    Height = 297
    Align = alClient
    TabOrder = 0
    object ScrollBox6: TScrollBox
      Left = 2
      Top = 15
      Width = 325
      Height = 280
      Align = alClient
      BevelInner = bvNone
      BevelOuter = bvNone
      BorderStyle = bsNone
      TabOrder = 0
      object Bevel8: TBevel
        Left = 43
        Top = 10
        Width = 260
        Height = 3
      end
      object Bevel9: TBevel
        Left = 105
        Top = 104
        Width = 200
        Height = 3
      end
      object Label14: TLabel
        Left = 3
        Top = 3
        Width = 35
        Height = 13
        Caption = 'Control'
      end
      object Label15: TLabel
        Left = 35
        Top = 22
        Width = 26
        Height = 13
        Caption = 'Mode'
      end
      object Label16: TLabel
        Left = 3
        Top = 96
        Width = 101
        Height = 13
        Caption = 'Mode/Code Selection'
      end
      object sbIFFTransponderControlModeOff: TSpeedButton
        Tag = 2
        Left = 14
        Top = 60
        Width = 76
        Height = 22
        AllowAllUp = True
        GroupIndex = 1
        Down = True
        Caption = 'Off'
        OnClick = btnIFFOnClick
      end
      object sbIFFTransponderControlModeOn: TSpeedButton
        Tag = 1
        Left = 14
        Top = 41
        Width = 76
        Height = 22
        HelpContext = 1
        AllowAllUp = True
        GroupIndex = 1
        Caption = 'On'
        OnClick = btnIFFOnClick
      end
      object cbIFFTransponderControlMode1: TCheckBox
        Tag = 1
        Left = 14
        Top = 122
        Width = 57
        Height = 17
        Caption = 'Mode 1 :'
        TabOrder = 0
        OnClick = OnIFFCheckedClick
      end
      object cbIFFTransponderControlMode2: TCheckBox
        Tag = 2
        Left = 14
        Top = 153
        Width = 57
        Height = 17
        Caption = 'Mode 2 :'
        TabOrder = 1
        OnClick = OnIFFCheckedClick
      end
      object cbIFFTransponderControlMode3: TCheckBox
        Tag = 3
        Left = 14
        Top = 184
        Width = 57
        Height = 17
        Caption = 'Mode 3 :'
        TabOrder = 2
        OnClick = OnIFFCheckedClick
      end
      object cbIFFTransponderControlMode3C: TCheckBox
        Tag = 4
        Left = 14
        Top = 215
        Width = 57
        Height = 17
        Caption = 'Mode 3C :'
        TabOrder = 3
        OnClick = OnIFFCheckedClick
      end
      object cbIFFTransponderControlMode4: TCheckBox
        Tag = 5
        Left = 14
        Top = 246
        Width = 57
        Height = 17
        Caption = 'Mode 4 :'
        TabOrder = 4
        OnClick = OnIFFCheckedClick
      end
      object edtIFFTransponderControlMode1: TEdit
        Tag = 1
        Left = 87
        Top = 120
        Width = 27
        Height = 21
        MaxLength = 2
        TabOrder = 5
        Text = '00'
        OnKeyPress = edtTransponderOnKeyPressed
      end
      object edtIFFTransponderControlMode2: TEdit
        Tag = 2
        Left = 88
        Top = 151
        Width = 85
        Height = 21
        MaxLength = 4
        TabOrder = 6
        Text = '0000'
        OnKeyPress = edtTransponderOnKeyPressed
      end
      object edtIFFTransponderControlMode3: TEdit
        Tag = 3
        Left = 88
        Top = 182
        Width = 85
        Height = 21
        MaxLength = 4
        TabOrder = 7
        Text = '0000'
        OnKeyPress = edtTransponderOnKeyPressed
      end
    end
  end
end
