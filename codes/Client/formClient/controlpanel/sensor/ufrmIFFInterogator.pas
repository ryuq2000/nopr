unit ufrmIFFInterogator;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls,ufrmSensorType;

type
  TfrmIFFInterogator = class(TfrmSensorType)
    grbIFFInterrogatorControl: TGroupBox;
    ScrollBox5: TScrollBox;
    Bevel5: TBevel;
    Bevel6: TBevel;
    Bevel7: TBevel;
    btnIFFInterrogatorTrackSearch: TSpeedButton;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    sbIFFInterrogatorControlModeOff: TSpeedButton;
    sbIFFInterrogatorControlModeOn: TSpeedButton;
    btnManual: TSpeedButton;
    btnAutomatic: TSpeedButton;
    lblInterrogation: TLabel;
    cbbtnIFFInterrogatorMode1: TCheckBox;
    cbbtnIFFInterrogatorMode2: TCheckBox;
    cbbtnIFFInterrogatorMode3: TCheckBox;
    cbbtnIFFInterrogatorMode3C: TCheckBox;
    cbbtnIFFInterrogatorMode4: TCheckBox;
    editbtnIFFInterrogatorTrack: TEdit;
    edtIFFInterrogatorMode1: TEdit;
    edtIFFInterrogatorMode2: TEdit;
    edtIFFInterrogatorMode3: TEdit;
    procedure OnIFFCheckedClick(Sender: TObject);
    procedure btnIFFOnClick(Sender: TObject);
    procedure btnIFFInterrogatorTrackSearchClick(Sender: TObject);
    procedure edtInterogatorOnKeyPressed(Sender: TObject; var Key: Char);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure UpdateForm; override;
    procedure SetControlledSensor(aObj : TObject); override;
  end;

var
  frmIFFInterogator: TfrmIFFInterogator;

implementation

uses uT3MountedIFF, tttData, uT3clientManager, uT3PlatformInstance,
  uGameData_TTT, uGlobalVar ;

{$R *.dfm}

{ TfrmIFFInterogator }

procedure TfrmIFFInterogator.btnIFFInterrogatorTrackSearchClick(
  Sender: TObject);
var
  tgt : TT3PlatformInstance;
  prnt : TT3PlatformInstance;
begin
  inherited;

  if not Assigned(clientManager.SelectedTrack) then
    Exit;

  if TT3MountedIFF(FControlledSensor).OperationalStatus = sopDamage then
    Exit;

  tgt :=  simManager.FindT3PlatformByID(clientManager.SelectedTrack.ObjectInstanceIndex);
  if not Assigned(tgt) then
    Exit;

  if (TT3PlatformInstance(tgt).PlatformDomain = 2) and
     (TT3PlatformInstance(tgt).Altitude > 0) then Exit;

  prnt :=  simManager.FindT3PlatformByID(TT3MountedIFF(FControlledSensor).PlatformParentInstanceIndex);
  if (prnt.PlatformDomain = 2) and
     (prnt.Altitude > 0) then Exit;

  clientManager.NetCmdSender.CmdTargetIFF(
        TT3MountedIFF(FControlledSensor).PlatformParentInstanceIndex,
        tgt.InstanceIndex,
        TT3MountedIFF(FControlledSensor).InstanceIndex);

  editbtnIFFInterrogatorTrack.Text := clientManager.SelectedTrack.TrackLabelInfo;

end;

procedure TfrmIFFInterogator.btnIFFOnClick(Sender: TObject);
var
  r : TRecCmd_Sensor;
begin
  inherited;

  if Assigned(FControlledSensor) and (FControlledSensor is TT3MountedIFF) and
     not (TT3MountedIFF(FControlledSensor).OperationalStatus = sopDamage) then
  begin
    with TT3MountedIFF(FControlledSensor) do
    begin
      case TSpeedButton(Sender).Tag of
        1: //InterrogatorOperateStatus := sopOn;
          begin
            r.SensorType := CSENSOR_TYPE_IFF;
            r.SensorID   := InstanceIndex;
            r.PlatformID := PlatformParent.InstanceIndex;
            r.OrderID    := IFFInterrogator.IFFType;
            r.OrderParam := byte(sopOn);

            clientManager.NetCmdSender.CmdSensor(r);
          end;
        2: //InterrogatorOperateStatus := sopOff;
          begin
            r.SensorType := CSENSOR_TYPE_IFF;
            r.SensorID   := InstanceIndex;
            r.PlatformID := PlatformParent.InstanceIndex;
            r.OrderID    := IFFInterrogator.IFFType;
            r.OrderParam := byte(sopOffIFF);

            clientManager.NetCmdSender.CmdSensor(r);
          end;
        3 : //mode automatic
        begin
            clientManager.NetCmdSender.CmdIFFSearchMode(
                  PlatformParentInstanceIndex,
                  InstanceIndex,
                  0);
        end;
        4 : //mode manual
        begin
            clientManager.NetCmdSender.CmdIFFSearchMode(
                  PlatformParentInstanceIndex,
                  InstanceIndex,
                  1);
        end;
      end;

      //UpdateObserve;
    end;
  end;
end;

procedure TfrmIFFInterogator.edtInterogatorOnKeyPressed(Sender: TObject;
  var Key: Char);
var
  ValKey : set of AnsiChar;
  Value,IndexFocusPlatform : integer;
  tmp : string;
begin
  inherited;

  ValKey := [#48 .. #55, #8, #13];
  if not(CharInSet(Key, ValKey)) then
    Key := #0;

  if Key = #13 then
  begin

	  if Assigned(FControlledSensor) and (FControlledSensor is TT3MountedIFF) and
       not (TT3MountedIFF(FControlledSensor).OperationalStatus = sopDamage) then
    begin
      with TT3MountedIFF(FControlledSensor) do
      begin
        if TryStrToInt(TEdit(Sender).Text, Value) then
        begin
          tmp := OctToDec(TEdit(Sender).Text);
          Value := StrToInt(tmp);
        end;

        if not Assigned(clientManager.SelectedTrack)  then
          IndexFocusPlatform := 0
        else
          IndexFocusPlatform := clientManager.SelectedTrack.ObjectInstanceIndex;

        case TEdit(Sender).Tag of
          1 :
          begin
            // InterrogatorMode1Enabled := TCheckBox(Sender).Checked;
            InterrogatorMode1 := Value;
            clientManager.NetCmdSender.CmdSensorIFF(
                  PlatformParentInstanceIndex,
                  IndexFocusPlatform,
                  InstanceIndex,
                  4,InterrogatorMode1Enabled, InterrogatorMode1);
          end;
          2 :
          begin
            //InterrogatorMode2Enabled := TCheckBox(Sender).Checked;
            InterrogatorMode2 := Value;
            clientManager.NetCmdSender.CmdSensorIFF(
                  PlatformParentInstanceIndex,
                  IndexFocusPlatform,
                  InstanceIndex,
                  5,InterrogatorMode2Enabled, InterrogatorMode2);
          end;
          3 :
          begin
            // InterrogatorMode3Enabled := TCheckBox(Sender).Checked;
            InterrogatorMode3 := Value;
            clientManager.NetCmdSender.CmdSensorIFF(
                  PlatformParentInstanceIndex,
                  IndexFocusPlatform,
                  InstanceIndex,
                  6,InterrogatorMode3Enabled, InterrogatorMode3);
          end;
        end;
      end;
    end;
  end;
end;

procedure TfrmIFFInterogator.OnIFFCheckedClick(Sender: TObject);
var
  IndexFocusPlatform : integer;
begin
  inherited;
  if Assigned(FControlledSensor) and (FControlledSensor is TT3MountedIFF) and
     not (TT3MountedIFF(FControlledSensor).OperationalStatus = sopDamage) then
  begin
    with TT3MountedIFF(FControlledSensor) do
    begin
      if not Assigned(clientManager.SelectedTrack)  then
        IndexFocusPlatform := 0
      else
        IndexFocusPlatform := clientManager.SelectedTrack.ObjectInstanceIndex;

      case TCheckBox(Sender).Tag of
        1:
        begin

          InterrogatorMode1Enabled := TCheckBox(Sender).Checked;
          clientManager.NetCmdSender.CmdSensorIFF(
                PlatformParentInstanceIndex,
                IndexFocusPlatform,
                InstanceIndex,
                4,InterrogatorMode1Enabled, InterrogatorMode1);
        end;
        2:
        begin

          InterrogatorMode2Enabled := TCheckBox(Sender).Checked;
          clientManager.NetCmdSender.CmdSensorIFF(
                PlatformParentInstanceIndex,
                IndexFocusPlatform,
                InstanceIndex,
                5,InterrogatorMode2Enabled, InterrogatorMode2);
        end;
        3:
        begin

          InterrogatorMode3Enabled := TCheckBox(Sender).Checked;
          clientManager.NetCmdSender.CmdSensorIFF(
                PlatformParentInstanceIndex,
                IndexFocusPlatform,
                InstanceIndex,
                6,InterrogatorMode3Enabled, InterrogatorMode3);
        end;
        4:
        begin

          InterrogatorMode3CEnabled := TCheckBox(Sender).Checked;
          clientManager.NetCmdSender.CmdSensorIFF(
                PlatformParentInstanceIndex,
                IndexFocusPlatform,
                InstanceIndex,
                7,InterrogatorMode3CEnabled, InterrogatorMode3C);

        end;

        5:
        begin

          InterrogatorMode4Enabled := TCheckBox(Sender).Checked;
          clientManager.NetCmdSender.CmdSensorIFF(
                PlatformParentInstanceIndex,
                IndexFocusPlatform,
                InstanceIndex,
                10,InterrogatorMode4Enabled, 0);
        end;
      end;
    end;
  end;
end;

procedure TfrmIFFInterogator.SetControlledSensor(aObj: TObject);
begin
  inherited;
end;

procedure TfrmIFFInterogator.UpdateForm;
begin
  inherited;
  if Assigned(FControlledSensor) then
    with TT3MountedIFF(FControlledSensor) do
    begin
      case InterrogatorOperateStatus of
        sopOn:
          begin
            if TargetObjectIdx = 0 then
              editbtnIFFInterrogatorTrack.Text := '';

            sbIFFInterrogatorControlModeOn.Enabled := true;
            sbIFFInterrogatorControlModeOff.Enabled := true;
            sbIFFInterrogatorControlModeOn.Down := true;

            editbtnIFFInterrogatorTrack.Enabled:=True;
            btnIFFInterrogatorTrackSearch.Enabled:=True;

            cbbtnIFFInterrogatorMode1.Enabled:=True;
            cbbtnIFFInterrogatorMode2.Enabled:=True;
            cbbtnIFFInterrogatorMode3.Enabled:=True;
            cbbtnIFFInterrogatorMode3C.Enabled:=True;
            cbbtnIFFInterrogatorMode4.Enabled:=True;

            edtIFFInterrogatorMode1.Enabled:=True;
            edtIFFInterrogatorMode2.Enabled:=True;
            edtIFFInterrogatorMode3.Enabled:=True;

            if IFFOnBoardDefinition.IFF_Capability = 0 then
            begin
              btnAutomatic.Enabled := true;
              btnManual.Enabled := true;
              if ModeSearchIFF = 0 then // automatic
              begin
                btnAutomatic.Down := true;
                btnIFFInterrogatorTrackSearch.Enabled := false;
                editbtnIFFInterrogatorTrack.Enabled := false;
              end
              else
              begin
                btnManual.Down := true;
                btnIFFInterrogatorTrackSearch.Enabled := true;
                editbtnIFFInterrogatorTrack.Enabled := true;
              end;
            end
            else
            begin
              btnAutomatic.Enabled := false;
              btnManual.Enabled := false;
              btnIFFInterrogatorTrackSearch.Enabled := true;
              editbtnIFFInterrogatorTrack.Enabled := true;
            end;

          end;
        sopOff, sopOffIFF:
          begin
            TargetObjectIdx := 0;

            sbIFFInterrogatorControlModeOn.Enabled := true;
            sbIFFInterrogatorControlModeOff.Enabled := true;
            sbIFFInterrogatorControlModeOff.Down := true;

            editbtnIFFInterrogatorTrack.Enabled:=False;
            btnIFFInterrogatorTrackSearch.Enabled:=False;
            editbtnIFFInterrogatorTrack.Text := '';

            cbbtnIFFInterrogatorMode1.Enabled:=False;
            cbbtnIFFInterrogatorMode2.Enabled:=False;
            cbbtnIFFInterrogatorMode3.Enabled:=False;
            cbbtnIFFInterrogatorMode3C.Enabled:=False;
            cbbtnIFFInterrogatorMode4.Enabled:=False;

            edtIFFInterrogatorMode1.Enabled:=False;
            edtIFFInterrogatorMode2.Enabled:=False;
            edtIFFInterrogatorMode3.Enabled:=False;

            btnAutomatic.Enabled := false;
            btnManual.Enabled := false;
          end;
        sopDamage, sopTooDeep, sopEMCON:
          begin
            sbIFFInterrogatorControlModeOn.Enabled := false;
            sbIFFInterrogatorControlModeOff.Enabled := false;
          end;
      end;

      cbbtnIFFInterrogatorMode1.Checked  := InterrogatorMode1Enabled;
      cbbtnIFFInterrogatorMode2.Checked  := InterrogatorMode2Enabled;
      cbbtnIFFInterrogatorMode3.Checked  := InterrogatorMode3Enabled;
      cbbtnIFFInterrogatorMode3C.Checked := InterrogatorMode3CEnabled;
      cbbtnIFFInterrogatorMode4.Checked  := InterrogatorMode4Enabled;

      edtIFFInterrogatorMode1.Text  := DecToOct(IntToStr(InterrogatorMode1));
      edtIFFInterrogatorMode2.Text  := DecToOct(IntToStr(InterrogatorMode2));
      edtIFFInterrogatorMode3.Text  := DecToOct(IntToStr(InterrogatorMode3));

    end;
end;

end.
