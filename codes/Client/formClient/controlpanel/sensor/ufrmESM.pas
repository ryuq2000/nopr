unit ufrmESM;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Buttons, ExtCtrls, StdCtrls,ufrmSensorType;

type
  TfrmESM = class(TfrmSensorType)
    grbESMSensorControl: TGroupBox;
    ScrollBox4: TScrollBox;
    Label6: TLabel;
    Bevel3: TBevel;
    Label7: TLabel;
    Label8: TLabel;
    Bevel4: TBevel;
    Label9: TLabel;
    sbESMSensorControlModeOn: TSpeedButton;
    sbESMSensorControlModeOff: TSpeedButton;
    sbESMSensorDisplayBlindZoneShow: TSpeedButton;
    sbESMSensorDisplayBlindZoneHide: TSpeedButton;
    procedure OnESMBtnClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure UpdateForm; override;
    procedure SetControlledSensor(aObj : TObject); override;
  end;

var
  frmESM: TfrmESM;

implementation

uses
  uT3MountedESM, tttData, uGameData_TTT, uT3ClientManager;

{$R *.dfm}

procedure TfrmESM.OnESMBtnClick(Sender: TObject);
var
  r : TRecCmd_Sensor;
  sensor : TT3MountedESM;
begin
  inherited;

  if Assigned(FControlledSensor) and (FControlledSensor is TT3MountedESM) and
     not (TT3MountedESM(FControlledSensor).OperationalStatus = sopDamage) then

    with TT3MountedESM(FControlledSensor) do
    begin
      case TSpeedButton(Sender).Tag of
        1:
        begin
          r.SensorType := CSENSOR_TYPE_ESM;
          r.SensorID   := InstanceIndex;
          r.PlatformID := PlatformParentInstanceIndex;
          r.OrderID    := CORD_ID_OperationalStatus;
          r.OrderParam := Byte(sopOn);

          clientManager.NetCmdSender.CmdSensor(r);
        end;
        2:
        begin
          r.SensorType := CSENSOR_TYPE_ESM;
          r.SensorID   := InstanceIndex;
          r.PlatformID := PlatformParentInstanceIndex;
          r.OrderID    := CORD_ID_OperationalStatus;
          r.OrderParam := Byte(sopOff);

          clientManager.NetCmdSender.CmdSensor(r);
        end;
        3:
        begin
          //FVisibleShowBlind := True;

          ShowBlindZone := True;
          //ShowBlindSelected := sensor.ShowBlindZone and FVisibleShowBlind;
        end;
        4:
        begin
          //FVisibleShowBlind := False;

          ShowBlindZone := False;
          //sensor.ShowBlindSelected := sensor.ShowBlindZone and FVisibleShowBlind;
        end;
    end;
  end;

  //UpdateObserve;

end;

procedure TfrmESM.SetControlledSensor(aObj: TObject);
begin
  inherited;

  if Assigned(FControlledSensor) then
    with TT3MountedESM(FControlledSensor) do
    begin
      sbESMSensorDisplayBlindZoneShow.Enabled := BlindZones.Count > 0;
      sbESMSensorDisplayBlindZoneHide.Enabled := BlindZones.Count > 0;

      if ShowBlindZone then
        sbESMSensorDisplayBlindZoneShow.Down := True
      else
        sbESMSensorDisplayBlindZoneHide.Down := True;
    end;
end;

procedure TfrmESM.UpdateForm;
begin
  inherited;

  if Assigned(FControlledSensor) then
    with TT3MountedESM(FControlledSensor) do
    begin
      case OperationalStatus of
        sopOn:
          begin
            sbESMSensorControlModeOn.Down := true;
            sbESMSensorControlModeOn.Enabled := true;
            sbESMSensorControlModeOff.Down := false;
            sbESMSensorControlModeOff.Enabled := true;
          end;
        sopOff:
          begin
            sbESMSensorControlModeOn.Down := false;
            sbESMSensorControlModeOn.Enabled := true;
            sbESMSensorControlModeOff.Down := true;
            sbESMSensorControlModeOff.Enabled := true;
          end;
        sopTooDeep:
          begin
            sbESMSensorControlModeOn.Enabled := false;
            sbESMSensorControlModeOff.Enabled := false;
          end;
        sopDamage:
          begin
            sbESMSensorControlModeOn.Enabled := false;
            sbESMSensorControlModeOff.Enabled := false;
          end;
      end;
    end;
end;

end.
