object frmESM: TfrmESM
  Left = 0
  Top = 0
  Caption = 'frmESM'
  ClientHeight = 311
  ClientWidth = 292
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object grbESMSensorControl: TGroupBox
    Left = 0
    Top = 0
    Width = 292
    Height = 311
    Align = alClient
    TabOrder = 0
    object ScrollBox4: TScrollBox
      Left = 2
      Top = 15
      Width = 288
      Height = 294
      Align = alClient
      BevelInner = bvNone
      BevelOuter = bvNone
      BorderStyle = bsNone
      TabOrder = 0
      object Label6: TLabel
        Left = 3
        Top = 3
        Width = 35
        Height = 13
        Caption = 'Control'
      end
      object Bevel3: TBevel
        Left = 43
        Top = 10
        Width = 232
        Height = 3
      end
      object Label7: TLabel
        Left = 35
        Top = 22
        Width = 26
        Height = 13
        Caption = 'Mode'
      end
      object Label8: TLabel
        Left = 3
        Top = 90
        Width = 34
        Height = 13
        Caption = 'Display'
      end
      object Bevel4: TBevel
        Left = 45
        Top = 97
        Width = 232
        Height = 3
      end
      object Label9: TLabel
        Left = 25
        Top = 115
        Width = 57
        Height = 13
        Caption = 'Blind Zones '
      end
      object sbESMSensorControlModeOn: TSpeedButton
        Tag = 1
        Left = 14
        Top = 40
        Width = 76
        Height = 22
        HelpContext = 1
        AllowAllUp = True
        GroupIndex = 3
        Caption = 'On'
        OnClick = OnESMBtnClick
      end
      object sbESMSensorControlModeOff: TSpeedButton
        Tag = 2
        Left = 14
        Top = 60
        Width = 76
        Height = 22
        AllowAllUp = True
        GroupIndex = 3
        Down = True
        Caption = 'Off'
        OnClick = OnESMBtnClick
      end
      object sbESMSensorDisplayBlindZoneShow: TSpeedButton
        Tag = 3
        Left = 14
        Top = 129
        Width = 76
        Height = 22
        HelpContext = 1
        AllowAllUp = True
        GroupIndex = 1
        Caption = 'Show'
        OnClick = OnESMBtnClick
      end
      object sbESMSensorDisplayBlindZoneHide: TSpeedButton
        Tag = 4
        Left = 14
        Top = 149
        Width = 76
        Height = 22
        AllowAllUp = True
        GroupIndex = 1
        Down = True
        Caption = 'Hide'
        OnClick = OnESMBtnClick
      end
    end
  end
end
