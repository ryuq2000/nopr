unit ufrmSonar;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, Buttons, ufrmSensorType;

type
  TfrmSonar = class(TfrmSensorType)
    grbSonarControl: TGroupBox;
    ScrollBox2: TScrollBox;
    Bevel37: TBevel;
    Bevel43: TBevel;
    Label213: TLabel;
    Label214: TLabel;
    Label237: TLabel;
    Label238: TLabel;
    Label239: TLabel;
    sbDisplayBlindHide: TSpeedButton;
    sbDisplayBlindShow: TSpeedButton;
    sbDisplayRangeHide: TSpeedButton;
    sbDisplayRangeShow: TSpeedButton;
    sbSonarControlModeActive: TSpeedButton;
    sbSonarControlModeOff: TSpeedButton;
    sbSonarControlModePassive: TSpeedButton;
    lblRangeTIOW: TLabel;
    btnRange1: TSpeedButton;
    btnRAnge2: TSpeedButton;
    btnRange3: TSpeedButton;
    pnlDeployment: TPanel;
    Bevel39: TBevel;
    Bevel41: TBevel;
    Bevel42: TBevel;
    Label215: TLabel;
    Label216: TLabel;
    Label221: TLabel;
    Label222: TLabel;
    Label223: TLabel;
    Label224: TLabel;
    Label226: TLabel;
    Label228: TLabel;
    Label229: TLabel;
    Label232: TLabel;
    Label233: TLabel;
    Label234: TLabel;
    Label236: TLabel;
    LabelCablePayout: TLabel;
    LabelDepth: TLabel;
    lbCableActual: TLabel;
    lbDepthActual: TLabel;
    lbDepthSettled: TLabel;
    lbDepthTow: TLabel;
    lblStatusDeployment: TLabel;
    sbDeploymentActiondeploy: TSpeedButton;
    sbDeploymentActionShow: TSpeedButton;
    editCableOrdered: TEdit;
    procedure OnSonarCmdClick(Sender: TObject);
  private
    { Private declarations }
    procedure SetTIOWRangeSonar(aValue : Boolean);
  public
    { Public declarations }
    procedure UpdateForm; override;
    procedure SetControlledSensor(aObj : TObject); override;
  end;

var
  frmSonar: TfrmSonar;

implementation

uses
  uT3MountedSonar, uGameData_TTT, uT3ClientManager, tttData,
  uBaseCoordSystem, uT3PlatformInstance, uGlobalVar;

{$R *.dfm}

{ TfrmSonar }

procedure TfrmSonar.OnSonarCmdClick(Sender: TObject);
var
  r                                : TRecCmd_Sensor;
  rec                              : TRecCmd_SonarDeploy;
  deployTime, stowTime             : Integer;
  orderCableLength, tmpActualCable : Double;
  pf                               : TT3PlatformInstance;
begin
  inherited;

  if Assigned(FControlledSensor) and (FControlledSensor is TT3MountedSonar) and
     not (TT3MountedSonar(FControlledSensor).OperationalStatus = sopDamage) then
  begin
    with TT3MountedSonar(FControlledSensor) do
    begin
      case TSpeedButton(Sender).Tag of
        1:
        begin
          r.SensorType := CSENSOR_TYPE_SONAR;
          r.SensorID   := InstanceIndex;
          r.PlatformID := PlatformParent.InstanceIndex;
          r.OrderID    := CORD_ID_ControlMode;
          r.OrderParam := byte(scmActive);

          clientManager.NetCmdSender.CmdSensor(r);

          SetTIOWRangeSonar(true);
        end;
        2:
        begin
          r.SensorType := CSENSOR_TYPE_SONAR;
          r.SensorID   := InstanceIndex;
          r.PlatformID := PlatformParent.InstanceIndex;
          r.OrderID    := CORD_ID_ControlMode;
          r.OrderParam := byte(scmPassive);

          clientManager.NetCmdSender.CmdSensor(r);

          SetTIOWRangeSonar(false);
        end;
        3:
        begin

          r.SensorType := CSENSOR_TYPE_SONAR;
          r.SensorID   := InstanceIndex;
          r.PlatformID := PlatformParent.InstanceIndex;
          r.OrderID    := CORD_ID_ControlMode;
          r.OrderParam := byte(scmOff);

          clientManager.NetCmdSender.CmdSensor(r);

          SetTIOWRangeSonar(false);
        end;

        4: //Deploy
        begin

          orderCableLength := StrToFloat(editCableOrdered.Text) *
            C_Meter_To_Feet;

          if orderCableLength < 0 then
            orderCableLength := 0
          else if orderCableLength > SonarDefinition.Cable_Length then
          begin
            orderCableLength := SonarDefinition.Cable_Length;
            editCableOrdered.Text := FloatToStr(orderCableLength *
              C_Feet_To_Meter);
          end;

          tmpActualCable := ActualCable * C_Meter_To_Feet;

          if tmpActualCable < orderCableLength then
          begin
            deployTime := Round((orderCableLength - tmpActualCable) /
              SonarDefinition.Sonar_Depth_Rate_of_Change);

            orderCableLength := orderCableLength * C_Feet_To_Meter;

            if PlatformParent is TT3PlatformInstance then
            begin
              clientManager.NetCmdSender.CmdSonarDeploy (
                PlatformParent.InstanceIndex, InstanceIndex,
                1, deployTime, ActualCable, orderCableLength);
            end;
          end
          else if tmpActualCable > orderCableLength then
          begin
            stowTime := Round((tmpActualCable - orderCableLength) /
              SonarDefinition.Sonar_Depth_Rate_of_Change);

            orderCableLength := orderCableLength * C_Feet_To_Meter;

            if PlatformParent is TT3PlatformInstance then
            begin
              clientManager.NetCmdSender.CmdSonarDeploy (
                PlatformParent.InstanceIndex, InstanceIndex,
                5, deployTime, ActualCable, orderCableLength);
                //param diisi 5 khusus untuk deploy keatas
            end;
          end;
        end;
        5: //Stow
        begin

          tmpActualCable := ActualCable * C_Meter_To_Feet;

          if tmpActualCable > 0 then
            stowTime := Round((tmpActualCable - 0) /
              SonarDefinition.Sonar_Depth_Rate_of_Change)
          else
            Exit;

          if PlatformParent is TT3PlatformInstance then
          begin
            clientManager.NetCmdSender.CmdSonarDeploy (
              PlatformParent.InstanceIndex, InstanceIndex,
              2, stowTime, ActualCable, 0);
          end;
        end;
        6: //Hide Range
        begin
          //FVisibleShowRange := False;
          ShowRange := False;
          //ShowRangeSelected := ShowRange and FVisibleShowRange;
        end;
        7: //Show Range
        begin
          if ControlMode = scmActive then
          begin
            if TIOWRange = strShort then
              DetectionRange := (1000 / C_NauticalMile_To_Yards) *
                SonarDefinition.TIOW_Short_Range
            else if TIOWRange = strMedium then
              DetectionRange := (1000 / C_NauticalMile_To_Yards) *
                SonarDefinition.TIOW_Medium_Range
            else if  TIOWRange = strLong then
              DetectionRange := (1000 / C_NauticalMile_To_Yards) *
                SonarDefinition.TIOW_Long_Range;
          end
          else
            DetectionRange := SonarDefinition.Max_Detect_Range;

          //FVisibleShowRange := True;
          ShowRange := True;
          //ShowRangeSelected := ShowRange and FVisibleShowRange;

          //HideRangeWeapon;
        end;
        8: //Hide Blindzone
        begin
          //FVisibleShowBlind := False;
          ShowBlindZone := False;
          //ShowBlindSelected := ShowBlindZone and FVisibleShowBlind;
          //HideBlindWeapon;
        end;
        9: //Show Blindzone
        begin
          //FVisibleShowBlind := True;
          ShowBlindZone := True;
          //ShowBlindSelected := ShowBlindZone and FVisibleShowBlind;
        end;
        10: //TIOW Short Range
        begin
          if PlatformParent is TT3PlatformInstance then
          begin
            r.SensorType := CSENSOR_TYPE_SONAR;
            r.SensorID   := InstanceIndex;
            r.PlatformID := PlatformParent.InstanceIndex;
            r.OrderID    := CORD_ID_ControlRangeSonar;
            r.OrderParam := byte(strShort);

            clientManager.NetCmdSender.CmdSensor(r);
          end;
        end;
        11: //TIOW Medium Range
        begin
          if PlatformParent is TT3PlatformInstance then
          begin
            r.SensorType := CSENSOR_TYPE_SONAR;
            r.SensorID   := InstanceIndex;
            r.PlatformID := PlatformParent.InstanceIndex;
            r.OrderID    := CORD_ID_ControlRangeSonar;
            r.OrderParam := byte(strMedium);

            clientManager.NetCmdSender.CmdSensor(r);
          end;
        end;
        12: //TIOW Long Range
        begin
          if PlatformParent is TT3PlatformInstance then
          begin
            r.SensorType := CSENSOR_TYPE_SONAR;
            r.SensorID   := InstanceIndex;
            r.PlatformID := PlatformParent.InstanceIndex;
            r.OrderID    := CORD_ID_ControlRangeSonar;
            r.OrderParam := byte(strLong);

            clientManager.NetCmdSender.CmdSensor(r);
          end;
        end;
      end;
    end;
  end;

end;

procedure TfrmSonar.SetControlledSensor(aObj: TObject);
begin
  inherited;

  if Assigned(FControlledSensor) then
  with TT3MountedSonar(FControlledSensor) do
  begin

    if ShowRange then
      sbDisplayRangeShow.Down := true
    else
      sbDisplayRangeHide.Down := true;

    if ShowBlindZone then
      sbDisplayBlindShow.Down := true
    else
      sbDisplayBlindHide.Down := true;

    if BlindZones.Count <= 0 then
    begin
      sbDisplayBlindHide.Enabled := False;
      sbDisplayBlindShow.Enabled := False;
    end else
    begin
      sbDisplayBlindHide.Enabled := True;
      sbDisplayBlindShow.Enabled := True;
    end;

    btnRange1.Caption := FormatFloat('0.00', SonarDefinition.TIOW_Short_Range);
    btnRange2.Caption := FormatFloat('0.00', SonarDefinition.TIOW_Medium_Range);
    btnRange3.Caption := FormatFloat('0.00', SonarDefinition.TIOW_Long_Range);

    case SonarCategory of
      scHMS, scSonobuoy: pnlDeployment.Visible := False;
      scVDS, scTAS, scDipping: pnlDeployment.Visible := True;
    end;

  end;

end;

procedure TfrmSonar.SetTIOWRangeSonar(aValue: Boolean);
begin
  lblRangeTIOW.Visible  := aValue;
  btnRange1.Visible     := aValue;
  btnRAnge2.Visible     := aValue;
  btnRange3.Visible     := aValue;

end;

procedure TfrmSonar.UpdateForm;
begin
  inherited;

  if Assigned(FControlledSensor) then
  with TT3MountedSonar(FControlledSensor) do
  begin
    lbCableActual.Caption := FloatToStr(ActualCable);
    editCableOrdered.Text := FloatToStr(OrderCable);

    case OperatingMode of
      somActive:
      begin
        sbSonarControlModeActive.Enabled := True;
        sbSonarControlModePassive.Enabled := False;
      end;
      somPassive, somPassiveIntercept:
      begin
        sbSonarControlModeActive.Enabled := False;
        sbSonarControlModePassive.Enabled := True;
      end;
      somActivePassive:
      begin
        sbSonarControlModeActive.Enabled := True;
        sbSonarControlModePassive.Enabled := True;
      end;
    end;

    case ControlMode of
      scmOff:
      begin
        sbSonarControlModeOff.Down := True;
        SetTIOWRangeSonar(False);
      end;
      scmPassive:
      begin
        sbSonarControlModePassive.Down := True;
        SetTIOWRangeSonar(False);
      end;
      scmActive:
      begin
        sbSonarControlModeActive.Down := True;

        SetTIOWRangeSonar(True);

        btnRange1.Caption := FormatFloat('0.00',
          SonarDefinition.TIOW_Short_Range);
        btnRange2.Caption := FormatFloat('0.00',
          SonarDefinition.TIOW_Medium_Range);
        btnRange3.Caption := FormatFloat('0.00',
          SonarDefinition.TIOW_Long_Range);

        case TIOWRange of
          strShort: btnRange1.Down := True;
          strMedium: btnRange2.Down := True;
          strLong: btnRange3.Down := True;
        end;
      end;
    end;

  end;

end;

end.
