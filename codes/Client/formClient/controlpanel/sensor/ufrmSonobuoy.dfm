object frmSonobuoy: TfrmSonobuoy
  Left = 0
  Top = 0
  Caption = 'frmSonobuoy'
  ClientHeight = 425
  ClientWidth = 340
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object grbSonobuoyControl: TGroupBox
    Left = 0
    Top = 0
    Width = 340
    Height = 425
    Align = alClient
    TabOrder = 0
    object ScrollBox7: TScrollBox
      Left = 2
      Top = 15
      Width = 336
      Height = 408
      Align = alClient
      BevelInner = bvNone
      BevelOuter = bvNone
      BorderStyle = bsNone
      TabOrder = 0
      object Bevel10: TBevel
        Left = 43
        Top = 10
        Width = 260
        Height = 3
      end
      object Bevel11: TBevel
        Left = 45
        Top = 56
        Width = 260
        Height = 3
      end
      object Bevel12: TBevel
        Left = 41
        Top = 169
        Width = 260
        Height = 3
      end
      object btnSonobuoyControlCombo: TSpeedButton
        Left = 280
        Top = 67
        Width = 23
        Height = 22
        Glyph.Data = {
          D6050000424DD605000000000000360000002800000017000000140000000100
          180000000000A005000000000000000000000000000000000000B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B200
          0000B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2000000B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B200
          0000B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2C1C1C1B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2000000B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B200000000
          0000B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2000000000000000000000000C1C1C1B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2000000000000000000000000000000000000B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B200
          0000B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000000000000000000000
          000000000000000000000000C1C1C1B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2000000B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B200000000
          0000000000000000000000000000000000000000000000000000B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2000000000000000000000000000000000000000000000000B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2000000000000000000000000000000000000B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B200
          0000B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000000000000000000000
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2000000B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B200000000
          0000B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B200
          0000B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2000000B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2
          B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2000000}
      end
      object Label17: TLabel
        Left = 3
        Top = 3
        Width = 31
        Height = 13
        Caption = 'Status'
      end
      object Label18: TLabel
        Left = 3
        Top = 49
        Width = 35
        Height = 13
        Caption = 'Control'
      end
      object Label19: TLabel
        Left = 35
        Top = 72
        Width = 33
        Height = 13
        Caption = 'Mode :'
      end
      object Label20: TLabel
        Left = 107
        Top = 22
        Width = 49
        Height = 13
        Caption = 'Quantity :'
      end
      object Label21: TLabel
        Left = 35
        Top = 103
        Width = 36
        Height = 13
        Caption = 'Depth :'
      end
      object Label22: TLabel
        Left = 159
        Top = 103
        Width = 33
        Height = 13
        Caption = 'metres'
      end
      object Label23: TLabel
        Left = 3
        Top = 161
        Width = 36
        Height = 13
        Caption = 'Monitor'
      end
      object Label24: TLabel
        Left = 35
        Top = 186
        Width = 128
        Height = 13
        Caption = 'Platform Monitor Capacity:'
      end
      object Label25: TLabel
        Left = 35
        Top = 215
        Width = 105
        Height = 13
        Caption = 'Currently Monitoring :'
      end
      object Label26: TLabel
        Left = 208
        Top = 186
        Width = 54
        Height = 13
        Caption = 'sonobuy(s)'
      end
      object Label27: TLabel
        Left = 208
        Top = 215
        Width = 54
        Height = 13
        Caption = 'sonobuy(s)'
      end
      object lblSonobuoyMonitorCurrently: TLabel
        Left = 190
        Top = 215
        Width = 6
        Height = 13
        Caption = '0'
      end
      object lblSonobuoyMonitorPlatform: TLabel
        Left = 188
        Top = 186
        Width = 12
        Height = 13
        Caption = '50'
      end
      object lblStatusSonobuoy: TLabel
        Left = 35
        Top = 22
        Width = 43
        Height = 13
        Caption = 'Available'
      end
      object lbStatusQuantity: TLabel
        Left = 162
        Top = 22
        Width = 18
        Height = 13
        Caption = '100'
      end
      object btnSonobuoyControlDeploy: TButton
        Left = 228
        Top = 134
        Width = 75
        Height = 25
        Caption = 'Deploy'
        TabOrder = 0
      end
      object editControlDepth: TEdit
        Left = 80
        Top = 99
        Width = 73
        Height = 21
        TabOrder = 1
        Text = '1'
      end
      object editControlMode: TEdit
        Left = 80
        Top = 68
        Width = 194
        Height = 21
        TabOrder = 2
        Text = 'Passive'
      end
    end
  end
  object pmModeSonobuoy: TPopupMenu
    Left = 280
    Top = 32
  end
end
