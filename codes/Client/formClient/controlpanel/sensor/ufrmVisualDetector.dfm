object frmVisualDetector: TfrmVisualDetector
  Left = 0
  Top = 0
  Caption = 'frmVisualDetector'
  ClientHeight = 428
  ClientWidth = 307
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object grbVisualDetectorSensor: TGroupBox
    Left = 0
    Top = 0
    Width = 307
    Height = 428
    Align = alClient
    TabOrder = 0
    object Label257: TLabel
      Left = 5
      Top = 3
      Width = 34
      Height = 13
      Caption = 'Display'
    end
    object Bevel51: TBevel
      Left = 43
      Top = 10
      Width = 260
      Height = 3
    end
    object Label258: TLabel
      Left = 145
      Top = 28
      Width = 57
      Height = 13
      Caption = 'Blind Zones '
    end
    object Label259: TLabel
      Left = 51
      Top = 28
      Width = 31
      Height = 13
      Caption = 'Range'
    end
    object sbVisualDetectorDisplayRangeShow: TSpeedButton
      Tag = 1
      Left = 30
      Top = 44
      Width = 76
      Height = 22
      HelpContext = 1
      AllowAllUp = True
      GroupIndex = 2
      Caption = 'Show'
      OnClick = OnButtonClick
    end
    object sbVisualDetectorDisplayRangeHide: TSpeedButton
      Tag = 2
      Left = 30
      Top = 64
      Width = 76
      Height = 22
      AllowAllUp = True
      GroupIndex = 2
      Down = True
      Caption = 'Hide'
      OnClick = OnButtonClick
    end
    object sbVisualDetectorDisplayBlindZonesShow: TSpeedButton
      Tag = 3
      Left = 132
      Top = 44
      Width = 76
      Height = 22
      HelpContext = 1
      AllowAllUp = True
      GroupIndex = 1
      Caption = 'Show'
      OnClick = OnButtonClick
    end
    object sbVisualDetectorDisplayBlindZonesHide: TSpeedButton
      Tag = 4
      Left = 132
      Top = 64
      Width = 76
      Height = 22
      AllowAllUp = True
      GroupIndex = 1
      Down = True
      Caption = 'Hide'
      OnClick = OnButtonClick
    end
  end
end
