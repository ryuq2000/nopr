object frmAnomalyDetector: TfrmAnomalyDetector
  Left = 0
  Top = 0
  Caption = 'frmAnomalyDetector'
  ClientHeight = 332
  ClientWidth = 296
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object grbAnomalyDetectorSensor: TGroupBox
    Left = 0
    Top = 0
    Width = 296
    Height = 332
    Align = alClient
    TabOrder = 0
    object Label225: TLabel
      Left = 3
      Top = 3
      Width = 35
      Height = 13
      Caption = 'Control'
    end
    object Bevel47: TBevel
      Left = 43
      Top = 10
      Width = 260
      Height = 3
    end
    object Label230: TLabel
      Left = 35
      Top = 22
      Width = 26
      Height = 13
      Caption = 'Mode'
    end
    object sbAnomalyDetectorControlModeOn: TSpeedButton
      Tag = 1
      Left = 14
      Top = 38
      Width = 76
      Height = 22
      HelpContext = 1
      AllowAllUp = True
      GroupIndex = 3
      Caption = 'On'
      OnClick = btnModeClick
    end
    object sbAnomalyDetectorControlModeOff: TSpeedButton
      Tag = 2
      Left = 14
      Top = 59
      Width = 76
      Height = 22
      AllowAllUp = True
      GroupIndex = 3
      Down = True
      Caption = 'Off'
      OnClick = btnModeClick
    end
  end
end
