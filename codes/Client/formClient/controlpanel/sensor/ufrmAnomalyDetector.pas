unit ufrmAnomalyDetector;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Buttons, ExtCtrls, StdCtrls, ufrmSensorType, uT3MountedMAD;

type
  TfrmAnomalyDetector = class(TfrmSensorType)
    grbAnomalyDetectorSensor: TGroupBox;
    Label225: TLabel;
    Bevel47: TBevel;
    Label230: TLabel;
    sbAnomalyDetectorControlModeOn: TSpeedButton;
    sbAnomalyDetectorControlModeOff: TSpeedButton;
    procedure btnModeClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure UpdateForm; override;
    procedure SetControlledSensor(aObj : TObject); override;
  end;

var
  frmAnomalyDetector: TfrmAnomalyDetector;

implementation

uses
   tttData, uGameData_TTT, uT3ClientManager;

{$R *.dfm}

procedure TfrmAnomalyDetector.btnModeClick(Sender: TObject);
var
  r : TRecCmd_Sensor;
  sensor : TT3MountedMAD;
begin
  inherited;

  if Assigned(FControlledSensor) and (FControlledSensor is TT3MountedMAD) and
     not (TT3MountedMAD(FControlledSensor).OperationalStatus = sopDamage) then

    with TT3MountedMAD(FControlledSensor) do
    begin

      case TSpeedButton(Sender).Tag of
        1:
        begin
          r.SensorType := CSENSOR_TYPE_MAD;
          r.SensorID   := InstanceIndex;
          r.PlatformID := PlatformParentInstanceIndex;
          r.OrderID    := CORD_ID_OperationalStatus;
          r.OrderParam := Byte(sopOn);

          clientManager.NetCmdSender.CmdSensor(r);

        end;
        2:
        begin
          r.SensorType := CSENSOR_TYPE_MAD;
          r.SensorID   := InstanceIndex;
          r.PlatformID := PlatformParentInstanceIndex;
          r.OrderID    := CORD_ID_OperationalStatus;
          r.OrderParam := Byte(sopOff);

          clientManager.NetCmdSender.CmdSensor(r);
        end;
      end;
    end;
//
//    frmToteDisplay.btnPlatformStatusClick(frmToteDisplay.btnPlatformStatus);
//    frmToteDisplay.lvPlatformsSelectItem(frmToteDisplay.lvPlatforms, frmToteDisplay.lvPlatforms.Selected, false);

end;

procedure TfrmAnomalyDetector.SetControlledSensor(aObj: TObject);
begin
  inherited;

end;

procedure TfrmAnomalyDetector.UpdateForm;
begin
  inherited;

  if Assigned(FControlledSensor) then
    with TT3MountedMAD(FControlledSensor) do
    begin

      if OperationalStatus = sopOn then
        sbAnomalyDetectorControlModeOn.Down := True
      else
        sbAnomalyDetectorControlModeOff.Down := True;
    end;
end;

end.
