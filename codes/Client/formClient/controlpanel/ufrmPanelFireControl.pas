unit ufrmPanelFireControl;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, ExtCtrls,ufrmControlled, StdCtrls, Buttons, uT3Track;

type
  TfrmPanelFC = class(TfrmControlled)
    PanelFCChoices: TPanel;
    lstAssetsChoices: TListView;
    PanelALL: TPanel;
    PanelFC: TPanel;
    ScrollBox3: TScrollBox;
    grbFireControl: TGroupBox;
    Bevel27: TBevel;
    Bevel52: TBevel;
    Bevel53: TBevel;
    btnSearchFireControlAssetsTarget: TSpeedButton;
    Label265: TLabel;
    Label266: TLabel;
    Label267: TLabel;
    Label268: TLabel;
    Label514: TLabel;
    Label515: TLabel;
    Label527: TLabel;
    lbControlChannel: TLabel;
    sbFireControlAssetsBlindZonesHide: TSpeedButton;
    sbFireControlAssetsBlindZonesShow: TSpeedButton;
    sbFireControlAssetsDisplayHide: TSpeedButton;
    sbFireControlAssetsDisplayShow: TSpeedButton;
    sbFireControlAssetsModeOff: TSpeedButton;
    sbFireControlAssetsModeSearch: TSpeedButton;
    sbFireControlAssetsModeTrackOnly: TSpeedButton;
    btnFireControlAssetsTargetAssign: TButton;
    btnFireControlAssetsTargetBreak: TButton;
    btnFireControlAssetsTargetBreakAll: TButton;
    edtFireControlAssetsTarget: TEdit;
    lstFireControlAssetsAssignedTracks: TListView;
  private
    { Private declarations }
  public
    { Public declarations }
    procedure SetControlledObject(ctrlObj: TT3Track); override;
    procedure UpdateForm; override;
    procedure EmptyField; override;
  end;

var
  frmPanelFC: TfrmPanelFC;

implementation

{$R *.dfm}

{ TfrmPanelFC }

procedure TfrmPanelFC.EmptyField;
begin
  inherited;

end;

procedure TfrmPanelFC.SetControlledObject(ctrlObj: TT3Track);
begin
  inherited;

end;

procedure TfrmPanelFC.UpdateForm;
begin
  inherited;

end;

end.
