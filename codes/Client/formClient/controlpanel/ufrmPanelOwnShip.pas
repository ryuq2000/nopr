unit ufrmPanelOwnShip;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ufrmControlled, ExtCtrls, StdCtrls, uGuidance, uT3Vehicle,
  uBaseCoordSystem, tttData, uGlobalVar, uT3Track;

type
  TfrmPanelOwnShip = class(TfrmControlled)
    ScrollBox4: TScrollBox;
    lbOwnShipPosition1: TLabel;
    Label22: TLabel;
    lbOrderHeading: TLabel;
    lbActualHeading: TLabel;
    Label23: TLabel;
    lbActualCourse: TLabel;
    Label25: TLabel;
    Label26: TLabel;
    Label27: TLabel;
    lbOwnShipOrderGround: TLabel;
    Label28: TLabel;
    lbOrderedAltitude: TLabel;
    lbOwnShipActualground: TLabel;
    Label29: TLabel;
    lbGuidance: TLabel;
    lbFuel: TLabel;
    Label46: TLabel;
    lbOwnShipPosition2: TLabel;
    Label30: TLabel;
    Label31: TLabel;
    Label32: TLabel;
    Label33: TLabel;
    lbActualAltitude: TLabel;
    Label34: TLabel;
    Label37: TLabel;
    lbName: TLabel;
    lbClass: TLabel;
    StaticText16: TStaticText;
    StaticText21: TStaticText;
    StaticText22: TStaticText;
    StaticText23: TStaticText;
    StaticText58: TStaticText;
    StaticText64: TStaticText;
    StaticText65: TStaticText;
    StaticText66: TStaticText;
    StaticText68: TStaticText;
    StaticText69: TStaticText;
    StaticText72: TStaticText;
    StaticText73: TStaticText;
    StaticText74: TStaticText;
    StaticText70: TStaticText;
    btnLaunch: TButton;
    lb1: TStaticText;
    lb2: TStaticText;
    lb4: TStaticText;
    lb5: TStaticText;
    btnLandPlatform: TButton;
    pnlInfoDepth: TPanel;
    StaticText1: TStaticText;
    StaticText2: TStaticText;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
  private
    { Private declarations }
  public
    { Public declarations }
    procedure InitCreate(sender: TForm); override;
    procedure SetControlledObject(ctrlObj: TT3Track); override;
    procedure UpdateForm; override;
    procedure ReadOnlyMode; override;
    procedure UnReadOnlyMode; override;
    procedure EmptyField;override;

  end;

var
  frmPanelOwnShip: TfrmPanelOwnShip;

implementation

uses uT3PlatformInstance, uDBClassDefinition, uSettingCoordinate,
  uT3ClientManager;

{$R *.dfm}

{ TfrmPanelOwnShip }

procedure TfrmPanelOwnShip.EmptyField;
begin
  inherited;

  lbName.Caption   := '---';
  lbClass.Caption  := '---';
  lbOwnShipPosition1.Caption:= '---';
  lbOrderHeading.Caption    := '---';
  lbActualHeading.Caption   := '---';
  lbActualCourse.Caption    := '---';
  lbOwnShipOrderGround.Caption:= '---';
  lbOrderedAltitude.Caption := '---';
  lbOwnShipActualground.Caption:= '---';
  lbGuidance.Caption        := '---';
  lbFuel.Caption            := '---';
  lbOwnShipPosition2.Caption:= '---';
  lbActualAltitude.Caption  := '---';

  btnLaunch.Enabled := False;
  btnLandPlatform.Enabled := False;
end;

procedure TfrmPanelOwnShip.InitCreate(sender: TForm);
begin
  inherited;

end;

procedure TfrmPanelOwnShip.ReadOnlyMode;
begin
  inherited;

end;

procedure TfrmPanelOwnShip.SetControlledObject(ctrlObj: TT3Track);
begin
  inherited;

  if Assigned(FControlledPlatform) then
    with TT3PlatformInstance(FControlledPlatform) do
      label1.Caption := IntToStr(InstanceIndex);

end;

procedure TfrmPanelOwnShip.UnReadOnlyMode;
begin
  inherited;

end;

procedure TfrmPanelOwnShip.UpdateForm;
var
  idCoordinat: integer;
  long, lat: double;
  pY, pX: Extended;
  vdef : TDBVehicle_Definition;
begin
  inherited;

  if Assigned(FControlledTrack) then
  begin
    label2.Caption  := formatDMS_long(FControlledTrack.getPositionX);
    label3.Caption  := formatDMS_latt(FControlledTrack.getPositionY);
  end;

  if not Assigned(FControlledPlatform) then
    EmptyField
  else
  if Assigned(FControlledPlatform) then
    with TT3PlatformInstance(FControlledPlatform) do
    begin

      if PlatformType = 1 then // vehicle
      begin
        lbGuidance.Caption := GetGuidanceStr(TT3Vehicle(FControlledPlatform).VehicleGuidance.GuidanceType)
      end
      else
        lbGuidance.Caption := '---';

      if FControlledPlatform is TT3Vehicle then
      begin
        if Assigned(TT3Vehicle(FControlledPlatform).VehicleDefinition) then
          lbClass.Caption := TT3Vehicle(FControlledPlatform).VehicleDefinition.Vehicle_Identifier;
      end
      else
        lbClass.Caption := '---';

      lbName.Caption := InstanceName;

      idCoordinat := fSettingCoordinate.IdCoordinat;
      long  := clientManager.Scenario.DBGameAreaDefinition.Game_Centre_Long;
      lat   := clientManager.Scenario.DBGameAreaDefinition.Game_Centre_Lat;

      case idCoordinat of
        1:
        begin
          lbOwnShipPosition1.Caption  := formatDMS_long(getPositionX);
          lbOwnShipPosition2.Caption  := formatDMS_latt(getPositionY);
        end;
        2:
        begin
          pX := CalcMove(getPositionX, long);
          pY := CalcMove(getPositionY, lat);

          if (pX >= 0) and (pY >=0) then
          begin
            lbOwnShipPosition1.Caption := 'White ' + FormatFloat('0.00', Abs(pX));  //kuadran 1
          end;
          if (pX <= 0) and (pY >=0) then
          begin
            lbOwnShipPosition1.Caption := 'Red ' + FormatFloat('0.00', Abs(pX));    //kuadran 2
          end;
          if (pX < 0) and (pY < 0) then
          begin
            lbOwnShipPosition1.Caption := 'Green ' + FormatFloat('0.00', Abs(pX));  //kuadran 3
          end;
          if (pX >= 0) and (pY <= 0) then
          begin
            lbOwnShipPosition1.Caption := 'Blue ' + FormatFloat('0.00', Abs(pX));   //kuadran 4
          end;

         lbOwnShipPosition2.Caption := FormatFloat('0.00', Abs(pY));
        end;
        3:
        begin
          lbOwnShipPosition1.Caption := ConvDegree_To_Georef(getPositionX,getPositionY);
          lbOwnShipPosition2.Caption := '---';
        end;
      end;

      if FControlledPlatform is TT3Vehicle then
        lbActualHeading.Caption       := FormatCourse(TT3Vehicle(FControlledPlatform).Heading)
      else
        lbActualHeading.Caption       := '---';

      lbActualCourse.Caption        := FormatCourse(Course);
      lbOwnShipActualground.Caption := FormatSpeed(Speed);

      if Speed = 0 then
        lbOwnShipActualground.Caption := '0.00';

      if Course = 0 then
      begin
        lbActualCourse.Caption  := '0.00';
        lbActualHeading.Caption := '0.00';
      end;

      {enabling launch and land button}
      btnLaunch.Enabled := false;

      if machineRole = crCubicle then
      begin
        btnLandPlatform.Visible := True;
      end
      else
        btnLandPlatform.Visible := false;

      if FControlledPlatform is TT3Vehicle then
      begin
        if (TT3Vehicle(FControlledPlatform).PlatformDomain = vhdSubsurface) then
        begin
          pnlInfoDepth.Visible := True;
          Label34.Caption := 'meter';
          Label37.Caption := 'meter';
          lbOrderedAltitude.Caption := FormatAltitude (TT3Vehicle(FControlledPlatform).OrderedAltitude);
          lbActualAltitude.Caption := FormatAltitude(Altitude);
        end
        else
        begin
          pnlInfoDepth.Visible := False;
          Label34.Caption := 'feet';
          Label37.Caption := 'feet';
          lbOrderedAltitude.Caption := FormatAltitude (TT3Vehicle(FControlledPlatform).OrderedAltitude * C_Meter_To_Feet);
          lbActualAltitude.Caption := FormatAltitude(Altitude * C_Meter_To_Feet);
        end;

        lbOrderHeading.Caption        := FormatCourse   (TT3Vehicle(FControlledPlatform).OrderedHeading);
        lbOwnShipOrderGround.Caption  := FormatSpeed    (TT3Vehicle(FControlledPlatform).OrderedSpeed);

        if TT3Vehicle(FControlledPlatform).OrderedHeading = 0 then
          lbOrderHeading.Caption  := '0.00';

        if TT3Vehicle(FControlledPlatform).OrderedSpeed = 0 then
          lbOwnShipOrderGround.Caption := '0.00';

        if TT3Vehicle(FControlledPlatform).OrderedHeading = 0 then
          lbOrderHeading.Caption := '0.00';

  //      if TT3Vehicle(FControlled).EmbarkedVehicles.Count > 0 then
  //        btnLaunch.Enabled := true
  //      else
          btnLaunch.Enabled := false;

  //      lbFuel.Caption := FloatToStr(Round(TT3Vehicle(FControlled).FuelPercentage)) + ' %';
      end
      else begin
        lbOrderHeading.Caption      := '---';
        lbOrderedAltitude.Caption   := '---';
        btnLaunch.Enabled := false;
      end;
    end;

end;

end.
