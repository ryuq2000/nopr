unit ufrmPanelSensor;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, ExtCtrls, ufrmControlled,ufrmSensorType,uT3MountedSensor,
  uT3Vehicle, uT3Track, uT3SimContainer;

type
  TSensorControlType = (scAnomaly, scEO, scESM, scRadar, scSonar, scSonobuouy, scVisual,
      scIFFInterogator, scIFFTransponder);

  TfrmPanelSensor = class(TfrmControlled)
    PanelSensorChoices: TPanel;
    lstSensor: TListView;
    procedure lstSensorSelectItem(Sender: TObject; Item: TListItem;
      Selected: Boolean);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    FSensorControl : TfrmSensorType;
    FSensorObject  : TT3MountedSensor;

    procedure addSensor(aSensor : TT3MountedSensor);

    function createSensorControl(aType : TSensorControlType) : TfrmSensorType;
    procedure RefreshPlatformSensors(Pf: TT3Vehicle);
    procedure updatePlatformSensors(Pf: TT3Vehicle);
    function searchItem(caption : String) : TListItem;

  public
    { Public declarations }

    procedure SetControlledObject(ctrlObj: TT3Track); override;
    procedure UpdateForm; override;
    procedure EmptyField;override;
  end;

var
  frmPanelSensor: TfrmPanelSensor;

implementation

uses
  ufrmAnomalyDetector, ufrmElectroOptical, ufrmESM, ufrmSearchRadar,
  ufrmSOnar, ufrmSonobuoy, ufrmVisualDetector, ufrmIFFInterogator,
  ufrmIFFTransponder, tttData,
  uT3MountedRadar, uT3MountedVisual, uT3MountedSonar,
  uT3MountedMAD, uT3MountedEO,uT3MountedIFF,
  uT3MountedESM, uGlobalVar;

{$R *.dfm}

{ TfrmSensor }

function TfrmPanelSensor.createSensorControl(
  aType: TSensorControlType): TfrmSensorType;
begin
  result := nil;

  case aType of
    scAnomaly        : result := TfrmAnomalyDetector.Create(nil);
    scEO             : result := TfrmElectroOptical.Create(nil);
    scESM            : result := TfrmESM.Create(nil);
    scRadar          : result := TfrmSearchRadar.Create(nil);
    scSonar          : result := TfrmSonar.Create(nil);
    scSonobuouy      : result := TfrmSonobuoy.Create(nil);
    scVisual         : result := TfrmVisualDetector.Create(nil);
    scIFFInterogator : result := TfrmIFFInterogator.Create(nil);
    scIFFTransponder : result := TfrmIFFTransponder.Create(nil);
  end;

  if Assigned(result) then
  begin
    result.Parent := Self;
    result.Align  := alClient;
    result.BorderStyle := bsNone;
    result.Show;
  end;
end;

procedure TfrmPanelSensor.EmptyField;
begin
  inherited;

  { set empty }
  if ASsigned( FSensorControl ) then
    FSensorControl.Free;
  FSensorControl := nil;

end;

procedure TfrmPanelSensor.FormCreate(Sender: TObject);
begin
  inherited;
  FSensorObject := nil;

end;

procedure TfrmPanelSensor.lstSensorSelectItem(Sender: TObject; Item: TListItem;
  Selected: Boolean);
var
  sensor : TObject;
begin

  sensor := Item.Data;

  if sensor = nil then
  begin
    EmptyField;
    exit;
  end;

  if sensor = FSensorObject then
  begin
    exit;
  end;

  EmptyField;

  if not Assigned(FSensorControl) then
  begin
    if (sensor is TT3MountedRadar)  then
    begin
      EmptyField;
      FSensorControl := createSensorControl(scRadar);
    end
    else
    if (sensor is TT3MountedVisual) then
    begin
      EmptyField;
      FSensorControl := createSensorControl(scVisual);
    end
    else
    if (sensor is TT3MountedSonar) then
    begin
      EmptyField;
      FSensorControl := createSensorControl(scSonar);
    end
    else
    if (sensor is TT3MountedEO)  then
    begin
      EmptyField;
      FSensorControl := createSensorControl(scEO);
    end
    else
    if (sensor is TT3MountedMAD)  then
    begin
      EmptyField;
      FSensorControl := createSensorControl(scAnomaly);
    end
    else
    if (sensor is TT3MountedESM) then
    begin
      EmptyField;
      FSensorControl := createSensorControl(scESM);
    end
    else
    if (sensor is TIFFType) then
    begin
      EmptyField;

      case TIFFType(sensor).IFFType of
        1 : FSensorControl := createSensorControl(scIFFTransponder);
        2 : FSensorControl := createSensorControl(scIFFInterogator);
      end;
    end
    else
      EmptyField
  end;

  if Assigned(FSensorControl) then
    FSensorControl.SetControlledSensor(sensor);

end;

procedure TfrmPanelSensor.RefreshPlatformSensors(Pf: TT3Vehicle);
var
  sensor : TT3MountedSensor;
  i : integer;
begin
  lstSensor.Clear;

  { add radar }
  for I := 0 to simManager.SimRadarsOnBoards.ItemCount - 1 do
  begin
    sensor := simManager.SimRadarsOnBoards.getObject(I) as TT3MountedSensor;

    if sensor.PlatformParentInstanceIndex = Pf.InstanceIndex then
      addSensor(sensor);
  end;

  { add sonar }
  for I := 0 to simManager.SimSonarsOnBoards.ItemCount - 1 do
  begin
    sensor := simManager.SimSonarsOnBoards.getObject(I) as TT3MountedSensor;

    if sensor.PlatformParentInstanceIndex = Pf.InstanceIndex then
      addSensor(sensor);
  end;

  { add esm }
  for I := 0 to simManager.SimESMsOnBoards.ItemCount - 1 do
  begin
    sensor := simManager.SimESMsOnBoards.getObject(I) as TT3MountedSensor;

    if sensor.PlatformParentInstanceIndex = Pf.InstanceIndex then
      addSensor(sensor);
  end;

  { add EO }
  for I := 0 to simManager.SimEOsOnBoards.ItemCount - 1 do
  begin
    sensor := simManager.SimEOsOnBoards.getObject(I) as TT3MountedSensor;

    if sensor.PlatformParentInstanceIndex = Pf.InstanceIndex then
      addSensor(sensor);
  end;

  { add IFF }
  for I := 0 to simManager.SimIFFsOnBoards.ItemCount - 1 do
  begin
    sensor := simManager.SimIFFsOnBoards.getObject(I) as TT3MountedSensor;

    if sensor.PlatformParentInstanceIndex = Pf.InstanceIndex then
      addSensor(sensor);
  end;

  { add MAD }
  for I := 0 to simManager.SimMADsOnBoards.ItemCount - 1 do
  begin
    sensor := simManager.SimMADsOnBoards.getObject(I) as TT3MountedSensor;

    if sensor.PlatformParentInstanceIndex = Pf.InstanceIndex then
      addSensor(sensor);
  end;

  { add IFF }
  for I := 0 to simManager.SimVisualsOnBoards.ItemCount - 1 do
  begin
    sensor := simManager.SimVisualsOnBoards.getObject(I) as TT3MountedSensor;

    if sensor.PlatformParentInstanceIndex = Pf.InstanceIndex then
      addSensor(sensor);
  end;
end;

procedure TfrmPanelSensor.addSensor(aSensor : TT3MountedSensor);
var
  li: TListItem;
begin

  if aSensor.SensorType = stIFF then
  begin
    li := lstSensor.Items.Add;
    li.Caption  := aSensor.InstanceName + 'Transponder';
    li.Data     := TT3MountedIFF(aSensor).IFFTransponder;

    if TT3MountedIFF(aSensor).EmconOperationalStatus = EmconOff then
    begin
      case TT3MountedIFF(aSensor).TransponderOperateStatus of
        sopOff:
          li.SubItems.Add('Off');
        sopOn:
          li.SubItems.Add('On');
        sopDamage:
          li.SubItems.Add('Damaged');
        sopTooDeep:
          li.SubItems.Add('Too Deep');
        sopEMCON:
          li.SubItems.Add('EMCON');
        sopActive:
          li.SubItems.Add('Active');
        sopPassive:
          li.SubItems.Add('Passive');
        sopTooFast:
          li.SubItems.Add('Too Fast');
        //------------------------------  //17042012 mk
        sopDeploying:
          li.SubItems.Add('Deploying');
        sopDeployed:
          li.SubItems.Add('Deployed');
        sopStowing:
          li.SubItems.Add('Stowing');
        sopStowed:
          li.SubItems.Add('Stowed');
        //------------------------------  //17042012 mk
        sopOffIFF:
          li.SubItems.Add('Off');
      end;
    end
    else
    begin
      case aSensor.EmconOperationalStatus of
        EmconOn:
          li.SubItems.Add('EMCON');
        EmconOff:
          li.SubItems.Add('off');
      end;
    end;

    li := lstSensor.Items.Add;
    li.Caption  := aSensor.InstanceName + 'Interrogator';
    li.Data     := TT3MountedIFF(aSensor).IFFInterrogator;

    if TT3MountedIFF(aSensor).EmconOperationalStatus = EmconOff then
    begin
      case TT3MountedIFF(aSensor).InterrogatorOperateStatus of
        sopOff:
          li.SubItems.Add('Off');
        sopOn:
          li.SubItems.Add('On');
        sopDamage:
          li.SubItems.Add('Damaged');
        sopTooDeep:
          li.SubItems.Add('Too Deep');
        sopEMCON:
          li.SubItems.Add('EMCON');
        sopActive:
          li.SubItems.Add('Active');
        sopPassive:
          li.SubItems.Add('Passive');
        sopTooFast:
          li.SubItems.Add('Too Fast');
        //------------------------------ //17042012 mk
        sopDeploying:
          li.SubItems.Add('Deploying');
        sopDeployed:
          li.SubItems.Add('Deployed');
        sopStowing:
          li.SubItems.Add('Stowing');
        sopStowed:
          li.SubItems.Add('Stowed');
        //------------------------------ //17042012 mk
        sopOffIFF:
          li.SubItems.Add('Off');
      end;
    end
    else
    begin
      case aSensor.EmconOperationalStatus of
          EmconOn:
            li.SubItems.Add('EMCON');
          EmconOff:
            li.SubItems.Add('off');
      end;
    end;

  end
  else
  begin
    li := lstSensor.Items.Add;
    li.Caption  := aSensor.InstanceName;
    li.Data     := aSensor;

    if aSensor.EmconOperationalStatus = EmconOff then
    begin
      case aSensor.OperationalStatus of
        sopOff:
          li.SubItems.Add('Off');
        sopOn:
          li.SubItems.Add('On');
        sopDamage:
          li.SubItems.Add('Damaged');
        sopTooDeep:
          li.SubItems.Add('Too Deep');
        sopEMCON:
          li.SubItems.Add('EMCON');
        sopActive:
          li.SubItems.Add('Active');
        sopPassive:
          li.SubItems.Add('Passive');
        sopTooFast:
          li.SubItems.Add('Too Fast');
        //------------------------------  //17042012 mk
        sopDeploying:
          li.SubItems.Add('Deploying');
        sopDeployed:
          li.SubItems.Add('Deployed');
        sopStowing:
          li.SubItems.Add('Stowing');
        sopStowed:
          li.SubItems.Add('Stowed');
        //------------------------------  //17042012 mk
      end;
    end
    else
    begin
      case aSensor.EmconOperationalStatus of
        EmconOn:
          li.SubItems.Add('EMCON');
        EmconOff:
          li.SubItems.Add('off');
      end;
    end;
  end;
end;

function TfrmPanelSensor.searchItem(caption : String): TListItem;
var
  i : integer;
  li : TListItem;
begin
  result := nil;
  for I := 0 to lstSensor.Items.Count - 1 do
  begin
    li := lstSensor.Items[I];
    if li.Caption = caption then
    begin
      Result := li;
      break;
    end;
  end;
end;

procedure TfrmPanelSensor.SetControlledObject(ctrlObj: TT3Track);
begin

  { if same with previous platform, ignore it}
  if ASsigned(FControlledTrack) and FControlledTrack.Equals(ctrlObj) then
    Exit;

  inherited;

  { set empty }
  EmptyField;

  lstSensor.Clear;

  { fill platform sensor list }
  if FControlledPlatform is TT3Vehicle then
    RefreshPlatformSensors(TT3Vehicle(FControlledPlatform));

end;

procedure TfrmPanelSensor.UpdateForm;
begin
  inherited;

  { fill platform sensor list -> must be fixed }
  if FControlledPlatform is TT3Vehicle then
    updatePlatformSensors(TT3Vehicle(FControlledPlatform));

  if Assigned(FSensorControl) then
    FSensorControl.UpdateForm;
end;

procedure TfrmPanelSensor.updatePlatformSensors(Pf: TT3Vehicle);
var
  sensor : TT3MountedSensor;
  data : TObject;
  li: TListItem;
  i : integer;
begin
  for I := 0 to lstSensor.Items.Count - 1 do
  begin

    li := lstSensor.Items[I];
    data :=  li.Data;

    if Assigned(data) then
    begin
      sensor := nil;
      if data is TT3MountedSensor then
        sensor := TT3MountedSensor(data)
      else
      if data is TIFFType then
        sensor := TIFFType(data).Data;

      if not Assigned(sensor) then
        continue;

      if sensor.EmconOperationalStatus = EmconOff then
      begin
        case sensor.OperationalStatus of
          sopOff:
            li.SubItems[0] := 'Off';
          sopOn:
            li.SubItems[0] := 'On';
          sopDamage:
            li.SubItems[0] := 'Damaged';
          sopTooDeep:
            li.SubItems[0] := 'Too Deep';
          sopEMCON:
            li.SubItems[0] := 'EMCON';
          sopActive:
            li.SubItems[0] := 'Active';
          sopPassive:
            li.SubItems[0] := 'Passive';
          sopTooFast:
            li.SubItems[0] := 'Too Fast';
          sopDeploying:
            li.SubItems[0] := 'Deploying';
          sopDeployed:
            li.SubItems[0] := 'Deployed';
          sopStowing:
            li.SubItems[0] := 'Stowing';
          sopStowed:
            li.SubItems[0] := 'Stowed';
        end;
      end
      else
      begin
        case sensor.EmconOperationalStatus of
          EmconOn:
            li.SubItems[0] := 'EMCON';
          EmconOff:
            li.SubItems[0] := 'Off';
        end;
      end;
    end ;

  end;

end;

end.
