unit uControlPanelManager;

interface

uses
  ufrmControlled, ufrmPanelOwnShip,
  ufrmPanelGuidance, ufrmPanelSensor,
  ufrmPanelWeapon, ufrmPanelCounterMeasure,
  ufrmPanelFireControl, ufrmPanelEMCON,
  uT3PlatformInstance,
  ComCtrls, Generics.Collections, Controls, Forms,
  ufrmHookPanel, uT3Track;

type

  { class untuk mengelola panel - panel contropanel tactical display }
  THookPanelType = (hpInfo, hpDetails, hpDetection, hpIFF);

  TControlPanelManager = class
  private
    FPanelList : TList<TfrmControlled>;
    FHookPanelList : TList<TfrmHookPanel>;
    { control panel }
    FControlPanel : TPageControl;
    { hook panel }
    FHookPanel : TPageControl;
    function panelFactory(panel : TControlPanelType) : TfrmControlled;
    function hookpanelFactory(panel : THookPanelType) : TfrmHookPanel;

  public
    constructor Create(controlPanel : TPageControl; hookPanel:TPageControl);
    destructor Destroy;override;

    { track controlled }
    procedure SetControlledTrack(track : TT3Track);
    { track selected only }
    procedure SetSelectedTrack(track : TT3Track);
    procedure SetHookedTrack(track : TT3Track);
    procedure EmptyField ;
    procedure PanelUpdate;
    { get specific panel on control panel }
    function getPanel(panel : TControlPanelType) : TfrmControlled;
  end;

implementation

uses
  ufrmHookInfo, ufrmHookDetails, ufrmhookDetection, ufrmHookIFF;


{ TControlPanelManager }

constructor TControlPanelManager.Create;
var
  panel: TfrmControlled;
  hpanel : TfrmHookPanel;
  enum : TControlPanelType;
  eHookPanel : THookPanelType;
begin
  if not Assigned(controlPanel) then
    Exit;

  if not Assigned(hookPanel) then
    Exit;

  FControlPanel := controlPanel;
  FHookPanel := hookPanel;

  FPanelList := TList<TfrmControlled>.Create;
  FHookPanelList := TList<TfrmHookPanel>.Create;

  { create each panel }
  for enum := cptOwnShip to cptEMCON do
  begin
    panel := panelFactory(enum);

    if Assigned(panel) then
    begin
      FPanelList.Add(panel);

      panel.ControlPanelType := enum;
      panel.Parent        := controlPanel.Pages[Byte(enum)];
      panel.Align         := alClient;
      panel.BorderStyle   := bsNone;
      panel.Show;

    end;

  end;

  { create each panel }
  for eHookPanel := hpInfo to hpIFF do
  begin
    hpanel := hookpanelFactory(eHookPanel);

    if Assigned(hpanel) then
    begin
      FHookPanelList.Add(hpanel);

      hpanel.Parent        := hookPanel.Pages[Byte(eHookPanel)];
      hpanel.Align         := alClient;
      hpanel.BorderStyle   := bsNone;
      hpanel.Show;

    end;

  end;

  controlPanel.ActivePageIndex := Byte(cptOwnShip);
  hookPanel.ActivePageIndex := Byte(hpInfo);

end;

destructor TControlPanelManager.Destroy;
var
  frm : TfrmControlled;
  hook : TfrmHookPanel;
begin

  for frm in FPanelList do
    frm.Free;

  for hook in FHookPanelList do
    hook.Free;

  FPanelList.Free;
  FHookPanelList.Free;

  inherited;
end;

procedure TControlPanelManager.EmptyField;
var
  frm : TfrmControlled;
  hook : TfrmHookPanel;
begin
  for frm in FPanelList do
    frm.EmptyField;

  for hook in FHookPanelList do
    hook.EmptyField;
end;

function TControlPanelManager.getPanel(
  panel: TControlPanelType): TfrmControlled;
var
  pnl : TfrmControlled;
begin
  result := nil;

  for pnl in FPanelList do
  begin
    if pnl.ControlPanelType = panel then
    begin
      Result := pnl;
      Break;
    end;
  end;
end;

function TControlPanelManager.hookpanelFactory(
  panel: THookPanelType): TfrmHookPanel;
begin
  result := nil;

  case panel of
    hpInfo      : result := TfrmHookInfo.Create(nil);
    hpDetails   : result := TfrmHookDetails.Create(nil);
    hpDetection : result := TfrmHookDetection.Create(nil);
    hpIFF       : result := TfrmHookIFF.Create(nil);
  end;
end;

function TControlPanelManager.panelFactory(panel: TControlPanelType): TfrmControlled;
begin
  result := nil;

  case panel of
    cptOwnShip        : result     := TfrmPanelOwnShip.Create(nil);
    cptGuidance       : result     := TfrmPanelGuidance.Create(nil);
    cptSensors        : result     := TfrmPanelSensor.Create(nil);
    cptWeapon         : result     := TfrmPanelWeapon.Create(nil);
    cptCounterMeasure : result     := TfrmPanelCM.Create(nil);
    cptFireControl    : result     := TfrmPanelFC.Create(nil);
    cptEMCON          : result     := TfrmPanelEMCON.Create(nil);
  end;
end;

procedure TControlPanelManager.PanelUpdate;
begin

  { be careful while choosing page index that doesnt exist }
  if FControlPanel.ActivePageIndex <= Byte(cptEMCON) then
    FPanelList.Items[FControlPanel.ActivePageIndex].UpdateForm;

  if FHookPanel.ActivePageIndex <= Byte(hpIFF) then
    FHookPanelList.Items[FHookPanel.ActivePageIndex].UpdateForm;
end;

procedure TControlPanelManager.SetControlledTrack(track : TT3Track);
var
  frm : TfrmControlled;
begin
  for frm in FPanelList do
  begin
    frm.SetControlledObject(track);
    frm.UpdateForm;
    if not Assigned(track) then
      frm.EmptyField;
  end;
end;

procedure TControlPanelManager.SetHookedTrack(track: TT3Track);
var
  hook : TfrmHookPanel;
begin

  for hook in FHookPanelList do
  begin
    hook.SetHookedTrack(track);
    hook.UpdateForm;
    if not Assigned(track) then
      hook.EmptyField;
  end;

end;

procedure TControlPanelManager.SetSelectedTrack(track: TT3Track);
var
  frm : TfrmControlled;
begin
  for frm in FPanelList do
  begin
    frm.SetSelectedObject(track);
  end;

end;

end.
