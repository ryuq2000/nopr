unit uTotePanelManager;

interface

uses
  Forms, SysUtils, Controls, Generics.Collections,
  uToteAudioTrack,
  uToteComChannelDefinition,
  uToteComChannelMapping,
  uToteComInterference,
  uToteCubicleGroups,
  uToteDataLinkControl,
  uToteDatalinkStatus,
  uToteEmitterStatus,
  uToteEnvironmentControl,
  uToteEnvironmentStatus,
  uToteMergedTracksStatus,
  uToteMessageHandlingSystem,
  uTotePlatformRemovalSummary,
  uTotePlatformStatus,
  uToteSensorOverride,
  uToteSettings,
  uToteSonarDetectionInformation,
  uToteSurfaceToAir,
  uToteSurfaceToSurface,
  uToteWeaponEngagementsSummary,
  uTotePanel;

{ list of panel }
const
  C_AudioRecordTracks       = 'AudioRecordTracks';
  C_ComChannelDefinition    = 'ComChannelDefinition';
  C_ComChannelMapping       = 'ComChannelMapping';
  C_ComInterference         = 'ComInterference';
  C_CubicleGroups           = 'CubicleGroups';
  C_DataLinkControl         = 'DataLinkControl';
  C_DataLinkStatus          = 'DataLinkStatus';
  C_EmmiterStatus           = 'EmmiterStatus';
  C_EnvironmentStatus       = 'EnvironmentStatus';
  C_EnvironmentControl      = 'EnvironmentControl';
  C_MergeTrackStatus        = 'MergeTrackStatus';
  C_MessageHandlingSystem   = 'MessageHandlingSystem';
  C_PlatformRemoveSummary   = 'PlatformRemoveSummary';
  C_PlatformStatus          = 'PlatformStatus';
  C_SensorOverride          = 'SensorOverride';
  C_Settings                = 'Settings';
  C_SonarDetectInfo         = 'SonarDetectInfo';
  C_SurfaceToAir            = 'SurfaceToAir';
  C_SurfaceToSurface        = 'SurfaceToSurface';
  C_WeaponEngagementSummary = 'WeaponEngagementSummary';

  C_ToteList : array [0..19] of String =
    (
      C_AudioRecordTracks       ,
      C_ComChannelDefinition    ,
      C_ComChannelMapping       ,
      C_ComInterference         ,
      C_CubicleGroups           ,
      C_DataLinkControl         ,
      C_DataLinkStatus          ,
      C_EmmiterStatus           ,
      C_EnvironmentStatus       ,
      C_EnvironmentControl      ,
      C_MergeTrackStatus        ,
      C_MessageHandlingSystem   ,
      C_PlatformRemoveSummary   ,
      C_PlatformStatus          ,
      C_SensorOverride          ,
      C_Settings                ,
      C_SonarDetectInfo         ,
      C_SurfaceToAir            ,
      C_SurfaceToSurface        ,
      C_WeaponEngagementSummary
    );

type
  TTotePanelManager = class

  private
    FActivePanel : TfrmTotePanel;
    FParent : TWinControl;
    FToteList : TDictionary<string,TfrmTotePanel>;

    procedure UpdatePanel(FPanel : TForm);
    function searchPanel(sID : String) : TfrmTotePanel;

    procedure createAllPanels;
    function createPanels(sID : String) : TfrmTotePanel;

  public
    constructor Create(parent : TWinControl);
    destructor Destroy;override;

    function ShowTote(sID : String) : TfrmTotePanel;

  end;

implementation

{ TTotePanelManager }

constructor TTotePanelManager.Create(parent : TWinControl);
begin
  FParent := parent;
  FToteList := TDictionary<string,TfrmTotePanel>.Create;
  FActivePanel := nil;

  createAllPanels;

end;

procedure TTotePanelManager.createAllPanels;
var
  I : integer;
  pnl : TfrmTotePanel;
begin
  for I := Low(C_ToteList) to High(C_ToteList) do
  begin
    pnl := createPanels(C_ToteList[I]);
    if Assigned(pnl) then
      FToteList.Add(C_ToteList[I],pnl);
  end;

end;

function TTotePanelManager.createPanels(sID : String): TfrmTotePanel;
begin

  if sId = C_AudioRecordTracks then
    result := TfrmAudioRecordTracks.Create(nil)
  else
  if sId = C_ComChannelDefinition then
    result := TfrmComChannelDefinition.Create(nil)
  else
  if sId = C_ComChannelMapping then
    result := TfrmComChannelMapping.Create(nil)
  else
  if sId = C_ComInterference then
    result := TfrmCommunicationsInterference.Create(nil)
  else
  if sId = C_CubicleGroups then
    result := TfrmCubicleGroups.Create(nil)
  else
  if sId = C_DataLinkControl then
    result := TfrmDatalinkControl.Create(nil)
  else
  if sId = C_DataLinkStatus then
    result := TfrmDataLinkStatus.Create(nil)
  else
  if sId = C_EmmiterStatus then
    result := TfrmEmitterStatus.Create(nil)
  else
  if sId = C_EnvironmentStatus then
    result := TfrmEnvironmentStatus.Create(nil)
  else
  if sId = C_EnvironmentControl then
    result := TfrmEnvironmentControl.Create(nil)
  else
  if sId = C_MergeTrackStatus then
    result := TfrmMergedTracksStatus.Create(nil)
  else
  if sId = C_MessageHandlingSystem then
    result := TfrmMessageHandlingSystem.Create(nil)
  else
  if sId = C_PlatformRemoveSummary then
    result := TfrmPlatformRemovalSummary.Create(nil)
  else
  if sId = C_PlatformStatus then
    result := TfrmPlatformStatus.Create(nil)
  else
  if sId = C_SensorOverride then
    result := TfrmSensorOverride.Create(nil)
  else
  if sId = C_Settings then
    result := TfrmSettings.Create(nil)
  else
  if sId = C_SonarDetectInfo then
    result := TfrmSonarDetectionInformation.Create(nil)
  else
  if sId = C_SurfaceToAir then
    result := TfrmSurfaceToAir.Create(nil)
  else
  if sId = C_SurfaceToSurface then
    result := TfrmSurfaceToSurface.Create(nil)
  else
  if sId = C_WeaponEngagementSummary then
    result := TfrmWeaponEngagementsSUmmary.Create(nil)
  else
    result := nil;

  if Assigned(result) then
  begin
    result.Parent := FParent;
    result.BorderStyle := bsNone;
    result.Align := alClient;
    result.Caption := sID;
  end;

end;

destructor TTotePanelManager.Destroy;
begin
  FToteList.Free;
  inherited;
end;

function TTotePanelManager.searchPanel(sID: String): TfrmTotePanel;
begin

end;

function TTotePanelManager.ShowTote(sID: String): TfrmTotePanel;
begin
  if Assigned(FActivePanel) then
  begin
    if FActivePanel.Caption <> sID then
      FActivePanel.Hide
    else
      Exit;
  end;

  if FToteList.TryGetValue(sID,FActivePanel) then
  begin
    FActivePanel.UpdateForm;
    FActivePanel.Show;
  end;

end;

procedure TTotePanelManager.UpdatePanel(FPanel: TForm);
begin

end;

end.
