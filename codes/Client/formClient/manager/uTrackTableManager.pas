unit uTrackTableManager;

interface

uses
  ComCtrls, uT3Track;

type
  TTrackTableManager = class
  private
    FTrackTable : TListView;
    procedure OnSelectItem  (Sender: TObject; Item: TListItem;
      Selected: Boolean);
    function TrackExist(track : TT3Track;out li : TListItem) : Boolean;
  public
    constructor Create(lv : TListView);

    procedure AddTrack(track : TT3Track);
    procedure RemoveTrack(track : TT3Track);

    procedure UpdateForm;
  end;

implementation

uses uBaseCoordSystem, uT3Common, uT3ClientManager, uGlobalVar, tttData,
  uT3PlatformInstance,
  uT3Vehicle, uT3Missile, uT3Torpedo,
  uT3Mine, uT3Sonobuoy;

{ TTrackTableManager }

procedure TTrackTableManager.AddTrack(track: TT3Track);
var
  sTrackNum, sDomain, sIdent: string;
  li : TListItem;
begin

  if TrackExist(track, li) then
    Exit;

  { add track here }

  sTrackNum := FormatTrackNumber(track.TrackNumber);
  sDomain := getDomain(track.TrackDomain);
  sIdent  := getIdentStr(track.TrackIdentifier);

  li := FTrackTable.Items.Add;
  li.Data := track;

  li.Caption := sDomain;
  li.SubItems.Add(sTrackNum);
  li.SubItems.Add(sIdent);
  li.SubItems.Add(FormatCourse(track.DetectedCourse));
  li.SubItems.Add(FormatSpeed(track.DetectedSpeed));

  if track.DetectedAltitude >= 0 then
  begin
    li.SubItems.Add(FormatAltitude(track.DetectedAltitude));
    li.SubItems.Add(' ');
  end
  else
  begin
    li.SubItems.Add(' ');
    li.SubItems.Add(FormatAltitude(track.DetectedAltitude));
  end;
end;

constructor TTrackTableManager.Create(lv: TListView);
begin
  FTrackTable := lv;
  lv.OnSelectItem := OnSelectItem;
end;

procedure TTrackTableManager.OnSelectItem(Sender: TObject; Item: TListItem;
  Selected: Boolean);
var
  obj: TObject;
begin
  if Item = nil then
    exit;

  obj := Item.Data;

  if obj = nil then
    Exit;

  clientManager.UnSelectAllTracks;
  TT3Track(obj).isSelected := True;

end;

procedure TTrackTableManager.RemoveTrack(track: TT3Track);
var
  li : TListItem;
begin
  if TrackExist(track, li) then
    li.Delete;
end;

function TTrackTableManager.TrackExist(track: TT3Track;out li : TListItem): Boolean;
var
  i: Integer;
  f: Boolean;
begin
  f := False;
  li := nil;
  i := 0;

  while not f and (i < FTrackTable.Items.Count) do
  begin
    li := FTrackTable.Items.Item[i];

    f := track.Equals(li.Data);

    Inc(i);
  end;

  Result := f;
  if not Result then
    li := nil;
end;

procedure TTrackTableManager.UpdateForm;
var
  i: Integer;
  li: TListItem;
  course, speed, altitude : Double;
  sTrackNum, sDomain, sIdent: string;
  obj: TObject;
  pi : TT3PlatformInstance;
  track : TT3Track;
begin
  for i := FTrackTable.Items.Count - 1 downto 0 do
  begin
    pi := nil;
    li := FTrackTable.Items[i];
    obj := li.Data;

    if obj = nil then
    begin
      FTrackTable.items.Delete(i);
      Continue;
    end;

    track := obj as TT3Track;

    if machineRole in [crWasdal, crController] then
    begin
      pi := simManager.FindT3PlatformByID(track.ObjectInstanceIndex);
      if Assigned(pi) then
      begin
        sTrackNum := pi.TrackID;
        sDomain   := getDomain(pi.PlatformDomain);
        sIdent    := '-';
        course    := pi.Course;
        speed     := pi.speed;
        altitude  := pi.Altitude;
      end;
    end else
    begin
      sTrackNum := FormatTrackNumber(track.trackNumber);
      sDomain   := getDomain(track.TrackDomain);
      sIdent    := getIdentStr(track.TrackIdentifier);
      course    := track.DetectedCourse;
      speed     := track.DetectedSpeed;
      altitude  := track.DetectedAltitude;
    end;

    li.Caption := sDomain;

    li.SubItems[0] := sTrackNum;
    li.SubItems[1] := sIdent;
    li.SubItems[2] := FormatCourse(Course);
    li.SubItems[3] := FormatSpeed(Speed);

    if altitude >= 0 then
    begin
      li.SubItems[4] := FormatAltitude(altitude);
      li.SubItems[5] := ' ';
    end
    else
    begin
      li.SubItems[4] := ' ';
      li.SubItems[5] := FormatAltitude(altitude);
    end;
  end;

end;

end.
