unit ufToteDisplay;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Buttons, StdCtrls, ExtCtrls, uTotePanelManager;

type
  TfrmToteDisplay = class(TForm)
    CategoryPanelGroup1: TCategoryPanelGroup;
    CategoryPanelManagementOp: TCategoryPanel;
    Label153: TLabel;
    btnCubicleGroups: TSpeedButton;
    Label1: TLabel;
    btnEnviroControl: TSpeedButton;
    Label155: TLabel;
    btnSensorOverride: TSpeedButton;
    Label156: TLabel;
    btnSetting: TSpeedButton;
    CategoryPanelCommCub: TCategoryPanel;
    Label14: TLabel;
    SpeedButton8: TSpeedButton;
    Label18: TLabel;
    SpeedButton10: TSpeedButton;
    btnDataLinkStatus: TSpeedButton;
    Label19: TLabel;
    CategoryPanelCommOp: TCategoryPanel;
    Label123: TLabel;
    btnCommInterfecene: TSpeedButton;
    Label124: TLabel;
    btnCommMapping: TSpeedButton;
    btnCommDefinition: TSpeedButton;
    Label126: TLabel;
    btnMessageHandling: TSpeedButton;
    btnDatalinkControl: TSpeedButton;
    Label127: TLabel;
    btnAudioRecord: TSpeedButton;
    Label128: TLabel;
    Label125: TLabel;
    CategoryPanelWeaponCub: TCategoryPanel;
    Label8: TLabel;
    SpeedButton5: TSpeedButton;
    Label12: TLabel;
    SpeedButton6: TSpeedButton;
    CategoryPanelWeaponOp: TCategoryPanel;
    Label88: TLabel;
    btnSurfaceToAir: TSpeedButton;
    Label122: TLabel;
    btnSurfaceToSurface: TSpeedButton;
    CategoryPanelStatusCub: TCategoryPanel;
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    btEmitterStatus: TSpeedButton;
    btnMergedTrack: TSpeedButton;
    Label2: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    CategoryPanelStatusOp: TCategoryPanel;
    btnPlatformStatus: TSpeedButton;
    btnEnviroStatus: TSpeedButton;
    btnWeaponEngagement: TSpeedButton;
    btnPlatformRemoval: TSpeedButton;
    Label71: TLabel;
    Label72: TLabel;
    Label73: TLabel;
    Label74: TLabel;
    btnSonarDetectionInformation: TSpeedButton;
    Label13: TLabel;
    procedure ToteOnClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    { Private declarations }
    FTotePanelManager : TTotePanelManager;
  public
    { Public declarations }
    property TotePanelManager : TTotePanelManager read FTotePanelManager;
  end;

var
  frmToteDisplay: TfrmToteDisplay;

implementation

{$R *.dfm}

procedure TfrmToteDisplay.FormCreate(Sender: TObject);
var
  i: Integer;
begin
  {create tote panel manager}
  FTotePanelManager := TTotePanelManager.Create(Self);

  { set desktop }
  DoubleBuffered := True;
  if Screen.MonitorCount > 1 then
    i := 1
  else
    i := 0;

  DefaultMonitor := dmDesktop;

  Width  := Screen.Monitors[i].Width;
  Height := Screen.Monitors[i].Height;
  Left   := Screen.Monitors[i].Left;
  Top    := Screen.Monitors[i].Top;

  Show;
end;

procedure TfrmToteDisplay.FormDestroy(Sender: TObject);
begin
  FTotePanelManager.Free;
end;

procedure TfrmToteDisplay.ToteOnClick(Sender: TObject);
begin
  case TControl(Sender).Tag of
    1  : FTotePanelManager.ShowTote(C_PlatformStatus);
    2  : FTotePanelManager.ShowTote(C_EnvironmentStatus);
    3  : FTotePanelManager.ShowTote(C_WeaponEngagementSummary);
    4  : FTotePanelManager.ShowTote(C_PlatformRemoveSummary);
    5  : FTotePanelManager.ShowTote(C_SonarDetectInfo);
    6  : FTotePanelManager.ShowTote(C_PlatformStatus);
    7  : FTotePanelManager.ShowTote(C_EnvironmentStatus);
    8  : FTotePanelManager.ShowTote(C_EmmiterStatus);
    9  : FTotePanelManager.ShowTote(C_MergeTrackStatus);
    10 : FTotePanelManager.ShowTote(C_SurfaceToAir);
    11 : FTotePanelManager.ShowTote(C_SurfaceToSurface);
    12 : FTotePanelManager.ShowTote(C_SurfaceToAir);
    13 : FTotePanelManager.ShowTote(C_SurfaceToSurface);
    14 : FTotePanelManager.ShowTote(C_MessageHandlingSystem);
    15 : FTotePanelManager.ShowTote(C_ComChannelDefinition);
    16 : FTotePanelManager.ShowTote(C_ComChannelMapping);
    17 : FTotePanelManager.ShowTote(C_ComInterference);
    18 : FTotePanelManager.ShowTote(C_DataLinkControl);
    19 : FTotePanelManager.ShowTote(C_AudioRecordTracks);
    20 : FTotePanelManager.ShowTote(C_MessageHandlingSystem);
    21 : FTotePanelManager.ShowTote(C_ComChannelMapping);
    22 : FTotePanelManager.ShowTote(C_DataLinkStatus);
    23 : FTotePanelManager.ShowTote(C_CubicleGroups);
    24 : FTotePanelManager.ShowTote(C_EnvironmentControl);
    25 : FTotePanelManager.ShowTote(C_SensorOverride);
    26 : FTotePanelManager.ShowTote(C_Settings);
  end;
end;

end.
