unit uT3ClientEventManager;

interface

uses
  uT3EventManager, tttData;

type
  TT3ClientEventManager = class(TT3EventManager)

  public
    procedure OnSensorOperationalStatus(Sender : TObject;Mode : TSensorOperationalStatus);  override;

    procedure OnTrackControlled(Sender : TObject);
    procedure OnTrackSelected(Sender : TObject);

    { vehicle event}
    procedure OnGuidanceChange(Sender : TObject; guidance : TVehicleGuidanceType);override;

  end;

implementation

uses ufTacticalDisplay, uT3ClientManager, ufrmControlled, ufrmPanelGuidance,
  uT3PlatformInstance;

{ TT3ClientEventManager }

procedure TT3ClientEventManager.OnGuidanceChange(Sender: TObject;
  guidance: TVehicleGuidanceType);
var
  ctrlPanel : TfrmControlled;
  p : TT3PlatformInstance;
begin
  inherited;

  p := nil;
  if Assigned(clientManager.ControlledTrack) then
    p := clientManager.FindT3PlatformByID(clientManager.ControlledTrack.ObjectInstanceIndex);

  if Assigned(p) and (p = Sender) then
  begin
    { update form guide control }
    with frmTacticalDisplay.CtrlPanelManager do
    begin
      ctrlPanel := getPanel(cptGuidance);
      if Assigned(ctrlPanel) and (ctrlPanel is TfrmPanelGuidance) then
        TfrmPanelGuidance(ctrlPanel).updateGuidancePanel;
    end;
  end;

end;

procedure TT3ClientEventManager.OnSensorOperationalStatus(Sender: TObject;
  Mode: TSensorOperationalStatus);
begin
  inherited;

end;

procedure TT3ClientEventManager.OnTrackControlled(Sender: TObject);
begin
  if not(Assigned(Sender)) then
    Exit;

//    pi := TT3PlatformInstance(Sender);

//    if pi.Controlled then
//    begin
//      if Assigned(simMgrClient.ControlledPlatform) then
//      begin
//        TT3Vehicle(simMgrClient.ControlledPlatform).Waypoints.IsOwnerHooked := False;
//        TT3Vehicle(simMgrClient.ControlledPlatform).Waypoints.IsOpenGuidanceTab := False;
//      end;
//
//      TT3Vehicle(pi).Waypoints.IsOwnerHooked := True;
//
//      frmTacticalDisplay.SetControlledPlatform(pi);
//
//      if Assigned(frmWaypointEditor) then
//        frmWaypointEditor.Close;
//    end;
//  end;

end;

procedure TT3ClientEventManager.OnTrackSelected(Sender: TObject);
begin

end;

end.
