unit uNetClientSender;

interface

uses
  uNetSender, uGameData_TTT;

type
  TNetClientCmdSender = class(TNetCmdSender)

  public
    procedure CmdGameControl(var r: TRecCmd_GameCtrl); override;
    procedure CmdPlatformGuidance(var r : TRecCmd_PlatformGuidance ); override;
    procedure CmdSensor(var r : TRecCmd_Sensor); overload;override;
    procedure CmdLaunchChaff(var r : TRecCmd_LaunchChaff); override;
    procedure CmdSyncWaypoint(var r : TrecSinc_Waypoint); override;
    procedure CmdTCPRequest(var r: TRecTCP_Request);override;
    procedure CmdMissile(var r : TRecCmd_LaunchMissile); override;
    procedure CmdLaunchRP(var r : TRecCmd_LaunchRP); override;

    {*------------------------------------------------------------------------------
      Client command from countermeasure control panel
    -------------------------------------------------------------------------------}
    procedure CmdAcousticDecoyOnBoard(var r : TRecCmdAcousticDecoyOnBoard); override;
    procedure CmdChaffOnBoard(var r : TRecCmdChaffOnBoard); override;
    procedure CmdBubbleOnBoard(var r : TRecCmdBubbleOnBoard); override;
    procedure CmdFloatingDecoyOnBoard(var r : TRecCmdFloatingDecoyOnBoard); override;
    procedure CmdSelfDefenceOnBoard(var r : TRecCmdSelfDefenseOnBoard); override;
    procedure CmdRadarNoiseOnBoard(var r : TRecCmdRadarNoiseOnBoard); override;
    {*------------------------------------------------------------------------------
    -------------------------------------------------------------------------------}

    procedure CmdRepos(var r : TRecCmd_Platform_MOVE); override;


    procedure CmdSonarDeploy(const aShipID, aSonarID: Integer;
      const aParam: Byte; const aTime : Integer; const aActualCable,
      aOrderCable: Double);override;
    procedure CmdIFFSearchMode(const pi_id : integer; const sensID: integer;
            const aMode : byte);override;
    procedure CmdTargetIFF(const pi_id: integer; const tgt_id: integer;
        const sensID: integer);override;
    procedure CmdSensorIFF(const pi_id: integer; const tgt_id: integer;
        const sensID: integer; const index_mod: Byte; const check_mod: Boolean;
        const value_mod: integer);override;
  end;

implementation

uses uNetHandle_Client, uGlobalVar;

{ TNetClientCmdSender }

procedure TNetClientCmdSender.CmdAcousticDecoyOnBoard(var r: TRecCmdAcousticDecoyOnBoard);
begin
  inherited;
  TNetHandler_Client(nethandle).SendCommand(CPID_CMD_ACOUSTIC_DECOY_ONBOARD, @r);
end;

procedure TNetClientCmdSender.CmdBubbleOnBoard(var r: TRecCmdBubbleOnBoard);
begin
  inherited;
  TNetHandler_Client(nethandle).SendCommand(CPID_CMD_BUBBLE_ONBOARD, @r);
end;

procedure TNetClientCmdSender.CmdChaffOnBoard(var r: TRecCmdChaffOnBoard);
begin
  inherited;
  TNetHandler_Client(nethandle).SendCommand(CPID_CMD_CHAFF_ONBOARD, @r);
end;

procedure TNetClientCmdSender.CmdFloatingDecoyOnBoard(
  var r: TRecCmdFloatingDecoyOnBoard);
begin
  inherited;
  TNetHandler_Client(nethandle).SendCommand(CPID_CMD_FLOATING_ONBOARD, @r);
end;

procedure TNetClientCmdSender.CmdGameControl(var r: TRecCmd_GameCtrl);
begin
  inherited;
  TNetHandler_Client(nethandle).SendCommand(CPID_CMD_GAME_CTRL, @r);
end;

procedure TNetClientCmdSender.CmdIFFSearchMode(const pi_id, sensID: integer;
  const aMode: byte);
begin
  inherited;
  TNetHandler_Client(nethandle).SendCommand(CPID_IFF_MODE_SEARCH, @recIFFSMode);
end;

procedure TNetClientCmdSender.CmdLaunchChaff(var r: TRecCmd_LaunchChaff);
begin
  inherited;
  TNetHandler_Client(nethandle).SendCommand(CPID_CMD_LAUNCH_CHAFF, @r);
end;

procedure TNetClientCmdSender.CmdLaunchRP(var r: TRecCmd_LaunchRP);
begin
  inherited;
  TNetHandler_Client(nethandle).SendCommand(CPID_CMD_LAUNCH_RUNTIME_PLATFORM, @r);
end;

procedure TNetClientCmdSender.CmdMissile(var r: TRecCmd_LaunchMissile);
begin
  inherited;
  TNetHandler_Client(nethandle).SendCommand(CPID_CMD_LAUNCH_MISSILE, @r);
end;

procedure TNetClientCmdSender.CmdPlatformGuidance(var r: TRecCmd_PlatformGuidance);
begin
  inherited;
  TNetHandler_Client(nethandle).SendCommand(CPID_CMD_PLATFORM, @r);
end;

procedure TNetClientCmdSender.CmdRadarNoiseOnBoard(
  var r: TRecCmdRadarNoiseOnBoard);
begin
  inherited;
  TNetHandler_Client(nethandle).SendCommand(CPID_CMD_RADARNOISE_ONBOARD, @r);
end;

procedure TNetClientCmdSender.CmdRepos(var r: TRecCmd_Platform_MOVE);
begin
  inherited;
  TNetHandler_Client(nethandle).SendCommand(CPID_CMD_PLATFORM_REPOS, @r);
end;

procedure TNetClientCmdSender.CmdSensor(var r : TRecCmd_Sensor);
begin
  inherited;
  TNetHandler_Client(nethandle).SendCommand(CPID_CMD_SENSOR, @r);
end;

procedure TNetClientCmdSender.CmdSelfDefenceOnBoard(
  var r: TRecCmdSelfDefenseOnBoard);
begin
  inherited;
  TNetHandler_Client(nethandle).SendCommand(CPID_CMD_SELFDEFENSE_ONBOARD, @r);
end;

procedure TNetClientCmdSender.CmdSensorIFF(const pi_id, tgt_id, sensID: integer;
  const index_mod: Byte; const check_mod: Boolean; const value_mod: integer);
begin
  inherited;
  TNetHandler_Client(nethandle).SendCommand(CPID_CMD_SENSOR_IFF, @recIFFSensor);
end;

procedure TNetClientCmdSender.CmdSonarDeploy(const aShipID, aSonarID: Integer;
      const aParam: Byte; const aTime : Integer; const aActualCable, aOrderCable: Double);
begin
  inherited;
  TNetHandler_Client(nethandle).SendCommand(CPID_CMD_SONAR_DEPLOY, @recSonarDeploy);
end;

procedure TNetClientCmdSender.CmdSyncWaypoint(var r: TrecSinc_Waypoint);
begin
  inherited;
  TNetHandler_Client(nethandle).SendCommand(CPID_SincWaypoint, @r);
end;

procedure TNetClientCmdSender.CmdTargetIFF(const pi_id, tgt_id,
  sensID: integer);
begin
  inherited;
  TNetHandler_Client(nethandle).SendCommand(CPID_CMD_TARGET_IFF, @recIFFTarget);
end;

procedure TNetClientCmdSender.CmdTCPRequest(var r: TRecTCP_Request);
begin
  inherited;
  TNetHandler_Client(nethandle).SendCommand(CPID_TCP_REQUEST, @r);
end;

end.
