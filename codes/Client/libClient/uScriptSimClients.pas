unit uScriptSimClients;


interface

  procedure BeginGame;
  procedure EndGame;

implementation

uses
   uScriptCommon,
   SysUtils, Controls, Forms,
   uLibSettingTTT,
   uT3ClientManager,
   uNetHandle_Client,
   uMapXHandler,
   ufTacticalDisplay,
   tttData,
   uGlobalVar,
   ufToteDisplay;
   //uT3Listener, uGameSetting, ufLog, uSRRFunction, uFilter


procedure BeginGame;
begin
  BeginGameCommon;

  if vMapSetting.FullScreen then
    frmTacticalDisplay.Be_A_FullMap(true);

  LoadFF_NetClientSetting(vSettingFile, vNetClientSetting);
  LoadFF_CubicalAssignSetting(vSettingFile, vCubicalAssignSetting);
  LoadFF_AppSetting(vSettingFile, vAppSetting);

  clientManager := TT3ClientManager.Create(frmTacticalDisplay.MapTouch);
  simManager    := clientManager;

  with clientManager do
  begin
    OnMapViewChange         := frmTacticalDisplay.MapViewChanged;
    SessionID               := vGameDataSetting.GameSessionID;
    OnUpdateForm            := frmTacticalDisplay.UpdateFormData;
    OnUpdateTime            := frmTacticalDisplay.UpdateGameTime;
    OnUpdateCenter          := frmTacticalDisplay.UpdateCenter;
    //Scenario.OnGetExternalCom := frmToteDisplay.ObjectCommunicationCreate;
//  simMgrClient.OnUpdateMessage := frmToteDisplay.UpdateMessageHandling;
  end;

  with clientManager.SimMap do
  begin
    OnMapMouseMove   := frmTacticalDisplay.MapMouseMove;
    OnMapMouseDown   := frmTacticalDisplay.MapMouseDown;
    OnMapMouseUp     := frmTacticalDisplay.MapMouseUp;
    FMap.CurrentTool := mtSelectObject;

//    OnDrawToCanvas  := simMgrClient.DrawAll;
    OnToolUsed      := frmTacticalDisplay.MapToolUsed;
    OnMapMouseMove  := frmTacticalDisplay.MapMouseMove;
    OnMapMouseDown  := frmTacticalDisplay.MapMouseDown;
    OnMapMouseUp    := frmTacticalDisplay.MapMouseUp;
    OnMapMouseDouble := frmTacticalDisplay.MapMouseDouble;
    OnMapMouseSingle := frmTacticalDisplay.MapMouseSingle;
    OnMapMouseExit := frmTacticalDisplay.MapMouseExit;
//    OnLogTemporary  := frmLog.LogTemporary;
  end;

  try
//    simMgrClient.EventManager.OnLogEventStr := frmLog.LogStr;
//    simMgrClient.OnLogEventStr  :=  frmLog.NetworkEventLogs;
//    simMgrClient.OnLogStr       :=  frmLog.LogStr;
//    simMgrClient.OnLogTemporary :=  frmLog.LogTemporary;
  finally

  end;

//  if UseSnapshot(vGameDataSetting) then
//  begin
//    simMgrClient.LoadFromSnapshot := True;
//    simMgrClient.SnapshotLoadFile(vGameDataSetting.SnapshotName);
//  end
//  else
//  begin
//    simMgrClient.LoadFromSnapshot := False;
    clientManager.LoadScenarioId(vGameDataSetting);
    if Assigned(clientManager.ControlledTrack)then
      frmTacticalDisplay.SetControlledTrack(clientManager.ControlledTrack);
//  end;
//

//
  frmTacticalDisplay.SetRoleClient(machineRole);
//  frmToteDisplay.setRoleClient(simMgrClient.ClientRole);
//  with frmToteDisplay do
//  begin
//    if CategoryPanelStatusOp.Enabled then
//      btnPlatformStatusClick(frmToteDisplay)
//    else
//      btnPlatformStatusClick(frmToteDisplay);
//  end;

  nethandle          := TNetHandler_Client.Create;
  with TNetHandler_Client(nethandle) do
  begin
    GamePort         := vNetSetting.GamePort;
    CommandPort      := vNetSetting.CommandPort;
    BroadCastAddress := vNetSetting.GameAddress;
    ServerIP         := vNetClientSetting.ServerIP;

    StartNetworking;
//  VNetClient.OnReceivedLog    := frmLog.LogNetworkEvent;
    OnConnected      := clientManager.netOnConnected;
//  VNetClient.OnLogRecv        := frmLog.LogDataBuffer;
//  VNetClient.OnFinishBuffer   := frmLog.FinishBuffer;
  end;

//  VNetVoipCtrlServer              :=  TNetVoipCtrl_Server.Create;
//  VNetVoipCtrlServer.VoipCtrlPort := vNetSetting.VoipCtrlPort;

  clientManager.InitNetwork;
  clientManager.GameStart;

  //bebe
//  simMgrClient.LoadPredefinedPattern;

//  vFilter := TFilter.Create;
end;

procedure EndGame;
begin

  clientManager.GamePause;
  clientManager.StopNetwork;

  TNetHandler_Client(nethandle).StopNetworking;
  TNetHandler_Client(nethandle).Free;
//
//  VNetVoipCtrlServer.StopNetworking;
//  VNetVoipCtrlServer.Free;
//
  clientManager.Free;

//  vFilter.Free
end;



end.


