unit uT3ObjectVisualManager;

interface

uses
  uObjectVisuals, Generics.Collections,
  tttData, Graphics, uCoordConvertor,
  uT3Track, Windows, SysUtils, uSimVisuals,
  uT3PointTrack;

type
  TBitmapSymbolDict = TObjectDictionary<String,TBitmapSymbol>;
  TDrawElementDict  = TObjectDictionary<String,TDrawElement>;

  {*------------------------------------------------------------------------------
    Class to set how object visual appear
  -------------------------------------------------------------------------------}
  TT3ObjectVisualManager = class
  private
    FBitmapSymbolDict : TBitmapSymbolDict;
    FDrawElementDict  : TDrawElementDict;
    FConverter        : TCoordConverter;

    FSpeedVector      : TSpeedVectorVisual;

    {*------------------------------------------------------------------------------
      Load all needed bitmap symbol
    -------------------------------------------------------------------------------}
    procedure LoadAllNeededBitmaps;
    procedure AddBitmapSymbol(aName : String);

    procedure DrawSpeedVector(track : TT3Track;aCnv : TCanvas);
    procedure DrawTrackSymbol(track : TT3Track;aCnv : TCanvas);
    procedure DrawSelectedTrack(track : TT3Track; bw, bh : Integer;aCnv : TCanvas);
    procedure DrawControlledTrack(track : TT3Track; bw, bh : Integer;aCnv : TCanvas);

    function getDrawElement(const Index: Integer): TDrawElement;
    function getLineVisual(const Index: Integer): TLineVisual;

    procedure initDrawElement;

  public
    constructor Create(aConverter : TCoordConverter);
    destructor Destroy;override;

    {*------------------------------------------------------------------------------
      Draw procedure
    -------------------------------------------------------------------------------}
    procedure DrawTrackVisual(track : TT3Track;aCnv : TCanvas);
    procedure Draw(aCnv : TCanvas);

    property Compass           : TDrawElement index 1 read getDrawElement;
    property RangeRing         : TDrawElement index 2 read getDrawElement;
    property LineVisual        : TLineVisual index 1 read getLineVisual;
    property RepositionLine    : TLineVisual index 2 read getLineVisual;
  end;
implementation

uses
  uLibSettingTTT, uGlobalVar, uT3Common, uCompassVisual;

{ TT3ObjectVisualManager }

procedure TT3ObjectVisualManager.AddBitmapSymbol(aName: String);
var
  FBitmapSymbol : TBitmapSymbol;
  fName : String;
begin
  fName := vSymbolSetting.ImgPath + aName + '.bmp';
  if not FBitmapSymbolDict.ContainsKey(aName) then
    if FileExists(fName) then
    begin
      FBitmapSymbol := TBitmapSymbol.Create;
      FBitmapSymbol.LoadBitmap(fName);
      FBitmapSymbolDict.Add(aName,FBitmapSymbol);
    end;
end;

constructor TT3ObjectVisualManager.Create;
begin
  FConverter        := aConverter;

  FBitmapSymbolDict := TBitmapSymbolDict.Create([doOwnsValues]);
  FDrawElementDict  := TDrawElementDict.Create([doOwnsValues]);

  { create singleton object visual and add to dict }
  FDrawElementDict.Add('Compass',TCompassVisual.Create);
  FDrawElementDict.Add('RangeRing',TRangeRingsVisual.Create);
  FDrawElementDict.Add('LineVisual',TLineVisual.Create);
  FDrawElementDict.Add('BlindZoneVisual',TBlindZoneVisual.Create);
  FDrawElementDict.Add('SpeedVector',TSpeedVectorVisual.Create);
  FDrawElementDict.Add('SeekerVisual',TSeekerVisual.Create);
  FDrawElementDict.Add('RepositionLine',TLineVisual.Create);

  LoadAllNeededBitmaps;

  initDrawElement;
end;

destructor TT3ObjectVisualManager.Destroy;
begin
  FBitmapSymbolDict.Free;
  FDrawElementDict.Free;

  inherited;
end;

procedure TT3ObjectVisualManager.Draw(aCnv: TCanvas);
begin
  { draw compass }
  with TCompassVisual(Compass) do
  begin
    Width   := FConverter.FMap.Width;
    Height  := FConverter.FMap.Height;
    ConvertCoord(FConverter);
    Draw(aCnv);
  end;

  { draw range ring }
  with TRangeRingsVisual(Compass) do
  begin
    ConvertCoord(FConverter);
    Draw(aCnv);
  end;

  { draw reposition line }
  with RepositionLine do
  begin
    ConvertCoord(FConverter);
    Draw(aCnv);
  end;

end;

procedure TT3ObjectVisualManager.DrawControlledTrack(track: TT3Track; bw,
  bh: Integer; aCnv: TCanvas);
Var
  w,h : integer;
  pt  : TPoint;
begin
  FConverter.ConvertToScreen(track.getPositionX, track.getPositionY, pt.X, pt.Y);

  with aCnv do
  begin
    Pen.Width   := 1;
    pen.Style   := psSolid;
    Pen.Color   := clWhite;
    Brush.Style := bsClear;

    w := (bw div 2)  + 2;
    h := (bh div 2) + 2;

    Rectangle(pt.X - w,pt.Y - h,pt.X + w,pt.Y + h );
  end;

end;

procedure TT3ObjectVisualManager.DrawSelectedTrack(track: TT3Track; bw,
  bh: Integer; aCnv: TCanvas);
Var
  w,h : integer;
  pt  : TPoint;
begin
  FConverter.ConvertToScreen(track.getPositionX, track.getPositionY, pt.X, pt.Y);

  with aCnv do
  begin
    Pen.Style := psSolid;
    Pen.Width := 1;
    Pen.Color := clrSelected;
    Brush.Style := bsClear;

    w := ((bw  + 1)  div 2) + 4;
    h := ((bh + 1) div 2) + 4;
    Rectangle(pt.X - w,pt.Y - h,pt.X + w,pt.Y + h );
  end;

end;

procedure TT3ObjectVisualManager.DrawSpeedVector(track: TT3Track;
  aCnv: TCanvas);
var
  pt: TPoint;
  symbol : string;
  acolor : TColor;
begin
  FConverter.ConvertToScreen(track.getPositionX, track.getPositionY, pt.X, pt.Y);

  if Assigned(FSpeedVector) then
  begin
    { get color }
    if machineRole in [crController, crWasdal] then
      symbol := getCorrectSymbol(track.TrackDomain, track.TrackTypeDomain,
        track.TrackDesignation, acolor)
    else
      symbol := getCorrectSymbol(track.TrackDomain, track.TrackTypeDomain, 3, acolor);

    with FSpeedVector do
    begin
      Center := pt;
      Color  := acolor;
      Course := track.DetectedCourse;
      Speed  := track.DetectedSpeed;
      Visible:= True;

      if Abs(track.DetectedSpeed) < 0.1 then
        Speed := 0
      else
        Speed := 20;

      ConvertCoord(FConverter);
      { draw }
      Draw(aCnv);
    end;
  end;
end;

procedure TT3ObjectVisualManager.DrawTrackSymbol(track: TT3Track;
  aCnv: TCanvas);
var
  acolor    : TColor;
  symbol   : String;
  FDormant : Boolean;
  pt       : TPoint;
  fSymbol  : TBitmapSymbol;
begin
  FDormant := False;

  FConverter.ConvertToScreen(track.getPositionX, track.getPositionY, pt.X, pt.Y);

  { get color and symbol }
  if machineRole in [crController, crWasdal] then
  begin
    if FDormant then
      symbol := getCorrectSymbol(5, 0, track.TrackDesignation, acolor)
    else
      symbol := getCorrectSymbol(track.TrackDomain, track.TrackTypeDomain,
        track.TrackDesignation, acolor);
  end
  else
  begin
    if FDormant then
      symbol := getCorrectSymbol(5, 0, 3, acolor)
    else
      symbol := getCorrectSymbol(track.TrackDomain, track.TrackTypeDomain, 3, acolor);
  end;

  FBitmapSymbolDict.TryGetValue(symbol,fSymbol);

  { Draw Symbol }
  if Assigned(fSymbol) then
    with fSymbol do
    begin
      Center := pt;
      Color  := acolor;
      Visible:= True;

      ConvertCoord(FConverter);
      { draw }
      Draw(aCnv);

      { draw selected }
      if track.isSelected then
        DrawSelectedTrack(track,BitmapWidth,BitmapHeigth,aCnv);

      { draw controlled }
      if (track is TT3PointTrack) then
        if TT3PointTrack(track).isControlled then
          DrawControlledTrack(track,BitmapWidth,BitmapHeigth,aCnv);
    end;
end;

procedure TT3ObjectVisualManager.DrawTrackVisual(track: TT3Track; aCnv: TCanvas);
begin
  DrawSpeedVector(track, aCnv);
  DrawTrackSymbol(track, aCnv);
end;

function TT3ObjectVisualManager.getDrawElement(
  const Index: Integer): TDrawElement;
begin
  Result := nil;
  case Index of
    1 : FDrawElementDict.TryGetValue('Compass',Result);
    2 : FDrawElementDict.TryGetValue('RangeRing',Result);
  end;
end;

function TT3ObjectVisualManager.getLineVisual(
  const Index: Integer): TLineVisual;
var
  drwElmt : TDrawElement;
begin
  Result := nil;
  case Index of
    1 : FDrawElementDict.TryGetValue('LineVisual',drwElmt);
    2 : FDrawElementDict.TryGetValue('RepositionLine',drwElmt);
  end;
  if Assigned(drwElmt) then
    Result := TLineVisual(drwElmt);
end;

procedure TT3ObjectVisualManager.initDrawElement;
var
  drawElmt : TDrawElement;
begin
  TCompassVisual(Compass).Width   := FConverter.FMap.Width;
  TCompassVisual(Compass).Height  := FConverter.FMap.Height;

  TRangeRingsVisual(RangeRing).Interval := 10;
  RepositionLine.Color := clDkGray;

  FDrawElementDict.TryGetValue('SpeedVector',drawElmt);
  FSpeedVector  := TSpeedVectorVisual(drawElmt);

end;

procedure TT3ObjectVisualManager.LoadAllNeededBitmaps;
var
  SymbolArray : array [0..8] of string;
  FBitmapSymbol : TBitmapSymbol;
  i : integer;
begin

  SymbolArray[0] := pctRotaryWing;
  SymbolArray[1] := pctFixedWing;
  SymbolArray[2] := pctAir;
  SymbolArray[3] := pctSurface;
  SymbolArray[4] := pctSubSurface;
  SymbolArray[5] := pctLand;
  SymbolArray[6] := pctGeneral;
  SymbolArray[7] := pctAmphibious;
  SymbolArray[8] := pctInfantry;

  { create all bitmap symbol here and save to dictionary }
  for I := 0 to Length(SymbolArray) - 1 do
    AddBitmapSymbol(SymbolArray[i] + pfHostile);

  for I := 0 to Length(SymbolArray) - 1 do
    AddBitmapSymbol(SymbolArray[i] + pfFriend);

  AddBitmapSymbol(pctWreck);

end;

end.
