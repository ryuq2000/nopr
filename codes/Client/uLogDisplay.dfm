object frmLogDisplay: TfrmLogDisplay
  Left = 0
  Top = 0
  BorderStyle = bsToolWindow
  Caption = 'Track Log'
  ClientHeight = 440
  ClientWidth = 556
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 0
    Top = 0
    Width = 556
    Height = 440
    ActivePage = TabSheet1
    Align = alClient
    TabOrder = 0
    object TabSheet1: TTabSheet
      Caption = 'Tracks'
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Panel2: TPanel
        Left = 0
        Top = 372
        Width = 548
        Height = 40
        Align = alBottom
        TabOrder = 1
        object Button1: TButton
          Left = 3
          Top = 6
          Width = 75
          Height = 25
          Caption = 'TrackUpdate'
          TabOrder = 0
          OnClick = Button1Click
        end
        object Button3: TButton
          Left = 84
          Top = 6
          Width = 75
          Height = 25
          Caption = 'Clear'
          TabOrder = 1
          OnClick = Button3Click
        end
      end
      object tvGroupTrack: TTreeView
        Left = 0
        Top = 0
        Width = 548
        Height = 372
        Align = alClient
        DoubleBuffered = True
        Indent = 19
        ParentDoubleBuffered = False
        TabOrder = 0
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'Debug Log'
      ImageIndex = 1
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Memo1: TMemo
        Left = 0
        Top = 0
        Width = 548
        Height = 412
        Align = alClient
        TabOrder = 0
      end
    end
  end
end
