unit uLogDisplay;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, ComCtrls, uT3ClientManager,
  uSimContainers;

type
  TfrmLogDisplay = class(TForm)
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Panel2: TPanel;
    Button1: TButton;
    Button3: TButton;
    tvGroupTrack: TTreeView;
    TabSheet2: TTabSheet;
    Memo1: TMemo;
    procedure Button1Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    FFlagUpdate, FFlagClear : Boolean;
    procedure refreshTree(data : TSimContainer); overload;
    procedure refreshTree; overload;
    procedure OnLogStr(const str : string);
  public
    { Public declarations }
    procedure updateForm;
  end;

var
  frmLogDisplay: TfrmLogDisplay;

implementation

uses  
  uT3Track, {uT3PointTrack,}uT3MountedSensor,
    Generics.Collections, uSensorInfo, uGlobalVar,
    tttData;

{$R *.dfm}

{ TfrmLogDisplay }

procedure TfrmLogDisplay.refreshTree(data: TSimContainer);
var
  tn,ch1,ch2,ch3: TTreeNode;
  track : TT3Track;
  i : integer;
  status : String;
  sensor : TPair<TT3MountedSensor,TSensorTrackInfo>;
begin      
  if Assigned(data) then
  begin
//    tvGroupTrack.Items.Clear;
//
//    if machineRole = crController then
//      status := 'Controller Tracks'
//    else
//      status := 'Cubicle Tracks [' + IntToStr(clientManager.MyGroupID) + ']';
//
//    ch1 := tvGroupTrack.Items.AddChild(nil,status);
//
//    for I := 0 to data.ItemCount - 1 do
//    begin
//      track := data.getObject(I) as TT3Track;
//
//
//      status := 'ID:' + IntToStr(track.UniqueID);
//      status := status + ' TrackNum:' + IntToStr(track.TrackNumber);
//
//      case track.TrackType of
//        trBearing : status := status + '[Bearing track]';
//        trPoint : status := status + '[point track]';
//        trAOP   : status := status + '[AOP track]';
//      end;
//
//      case track.TrackCategory of
//        tcRealTrack : status := status + '[RT track]';
//        tcNonRealTrack : status := status + '[NRT track]';
//      end;
//
//      status := status + ' [pid : ' + IntToStr(track.ObjectInstanceIndex) + ']';
//
//      if (track is TT3PointTrack) and (TT3PointTrack(track).isAlwaysTracking) then
//        status := status + ' [ownplatform]';
//
//      if track.isLostContact and (not (TT3PointTrack(track).isAlwaysTracking) ) then
//        status := status + ' [lost contact]';
//
//      status := status + ' [last detected : ' + TimeToStr(track.LastDetected) + ']';
//
//      ch2 := tvGroupTrack.Items.AddChild(ch1,status);
//      ch2.Data := track;
//
//      if track.DetectedBySensors.Count <= 0 then
//        status := '[NONE]'
//      else
//      begin
//        status := '';
//        for sensor in track.DetectedBySensors do
//        begin
//          status := status + sensor.Key.InstanceName + ' (' + TimeToStr(sensor.Value.LastDetected) + ')';
//          status := status + ',';
//        end;
//      end;
//
//      ch3 := tvGroupTrack.Items.AddChild(ch2,status);
//      ch3.Data := track.DetectedBySensors;
//
//    end;
  end;
end;

procedure TfrmLogDisplay.Button1Click(Sender: TObject);
begin
  FFlagUpdate := True;
end;

procedure TfrmLogDisplay.Button3Click(Sender: TObject);
begin
  FFlagClear := true;
end;

procedure TfrmLogDisplay.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caHide;
end;

procedure TfrmLogDisplay.FormCreate(Sender: TObject);
begin
  clientManager.OnCommonLogStr := OnLogStr;
end;

procedure TfrmLogDisplay.OnLogStr(const str: string);
begin
  if Memo1.Lines.Count > 200 then
    Memo1.Lines.Clear;

  Memo1.Lines.Insert(0,str);
end;

procedure TfrmLogDisplay.refreshTree;
var
  tn,ch2,ch3: TTreeNode;
  track : TT3Track;
  i : integer;
  status : String;
  sensor : TPair<TT3MountedSensor,TSensorTrackInfo>;
  sensordata :  TObjectDictionary<TT3MountedSensor,TSensorTrackInfo> ;
  data      : TObject;
begin
  tn := tvGroupTrack.Items.GetFirstNode;
  if Assigned(tn) then
  begin
//    {iterate}
//    ch2 := tn.getFirstChild;
//    while Assigned(ch2) do
//    begin
//      data := ch2.Data;
//      if data is TT3Track then
//      begin
//        status := 'ID: ' + IntToStr(TT3Track(data).UniqueID);
//        status := status + ' TrackNum:' + IntToStr(TT3Track(data).TrackNumber);
//
//        case TT3Track(data).TrackType of
//          trBearing : status := status + '[Bearing track]';
//          trPoint : status := status + '[point track]';
//          trAOP   : status := status + '[AOP track]';
//        end;
//
//        case TT3Track(data).TrackCategory of
//          tcRealTrack : status := status + '[RT track]';
//          tcNonRealTrack : status := status + '[NRT track]';
//        end;
//
//        status := status + ' [pid : ' + IntToStr(TT3Track(data).ObjectInstanceIndex) + ']';
//
//        if (data is TT3PointTrack) and (TT3PointTrack(data).isAlwaysTracking) then
//          status := status + ' [ownplatform/always detected]';
//
//        if TT3Track(data).isLostContact and (not (TT3PointTrack(data).isAlwaysTracking)) then
//          status := status + ' [lost contact]';
//
//        status := status + ' [last detected : ' + TimeToStr(TT3Track(data).LastDetected) + ']';
//        ch2.Text := status;
//
//        ch3 := ch2.getFirstChild;
//        while Assigned(ch3) do
//        begin
//          if Assigned(data) then
//          begin
//            sensordata := TT3Track(data).DetectedBySensors;
//            status := '';
//
//            for sensor in sensordata do
//            begin
//              status := status + sensor.Key.InstanceName;
//              status := status + ' (' + TimeToStr(sensor.Value.LastDetected) + ')';
//              status := status + ',';
//            end;
//
//            if status = '' then
//              ch3.Text := '[NONE]'
//            else
//              ch3.Text := Status;
//          end;
//
//          ch3 := ch3.getNextSibling;
//        end;
//      end;
//
//      ch2 := ch2.getNextSibling;
//    end;
  end;
  tvGroupTrack.Invalidate;
end;

procedure TfrmLogDisplay.updateForm;
begin

  if FFlagUpdate then
  begin
    refreshTree(clientManager.CubicleTracks);
    FFlagUpdate := False;
  end
  else
    refreshTree;

  if FFlagClear then
  begin
    tvGroupTrack.Items.Clear;
    FFlagClear := False;
  end;

end;

end.
