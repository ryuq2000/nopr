unit ufTacticalDisplay;

interface

uses
  MapXLib_TLB, Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uSimObjects, uT3Vehicle, tttData,
  Dialogs, Menus, ExtCtrls, ImgList, StdCtrls, ComCtrls, ToolWin,
  uControlPanelManager, OleCtrls, Math, uGameData_TTT, uT3Track,
  uT3PointTrack, uT3PlatformInstance, uTrackTableManager, uTMapTouch;

type
  TDragObject = class
  public
    FDraggedObject : tobject;
    FPointFrom : TPoint;
    FPointTO   : TPoint;
  end;

  TfrmTacticalDisplay = class(TForm)
    pnlLeft: TPanel;
    TacticalDisplayControlPanel: TPageControl;
    tsOwnShip: TTabSheet;
    tsPlatformGuidance: TTabSheet;
    tsSensor: TTabSheet;
    tsWeapon: TTabSheet;
    tsCounterMeasure: TTabSheet;
    tsFireControl: TTabSheet;
    tsEMCON: TTabSheet;
    HookContactInfoTraineeDisplay: TPageControl;
    tsHook: TTabSheet;
    tsDetails: TTabSheet;
    tsDetection: TTabSheet;
    tsIFF: TTabSheet;
    lvTrackTable: TListView;
    pnlMap: TPanel;
    pnlBottom: TPanel;
    Panel1: TPanel;
    Label55: TLabel;
    Label56: TLabel;
    lbCourseHooked: TLabel;
    lbSpeedHooked: TLabel;
    Label60: TLabel;
    Label62: TLabel;
    lbRangeRings: TLabel;
    Label64: TLabel;
    lblRangeScale: TLabel;
    Label66: TLabel;
    lbRangeAnchor: TLabel;
    Label68: TLabel;
    lbBearingAnchor: TLabel;
    Label70: TLabel;
    Label71: TLabel;
    Label73: TLabel;
    Label74: TLabel;
    Label72: TLabel;
    lbLongitude: TLabel;
    lbLatitude: TLabel;
    lbY: TLabel;
    lbX: TLabel;
    Label79: TLabel;
    Label80: TLabel;
    Bevel1: TBevel;
    Bevel2: TBevel;
    Bevel3: TBevel;
    Bevel4: TBevel;
    Label35: TLabel;
    Bevel5: TBevel;
    Bevel6: TBevel;
    lbColor: TLabel;
    Label10: TLabel;
    Label21: TLabel;
    Label22: TLabel;
    pnlStatusRed: TPanel;
    StatusBar1: TStatusBar;
    pnlStatusYellow: TPanel;
    MainMenu1: TMainMenu;
    View1: TMenuItem;
    Display1: TMenuItem;
    Tactical1: TMenuItem;
    Tote1: TMenuItem;
    mnFullScreen1: TMenuItem;
    Scale1: TMenuItem;
    Increase1: TMenuItem;
    Decrease1: TMenuItem;
    Zoom1: TMenuItem;
    Centre1: TMenuItem;
    Settings2: TMenuItem;
    OnHookedTrack2: TMenuItem;
    OnGameCentre1: TMenuItem;
    Pan1: TMenuItem;
    RangeRings1: TMenuItem;
    Settings1: TMenuItem;
    OnHookedTrack1: TMenuItem;
    Filters1: TMenuItem;
    Overrides1: TMenuItem;
    History1: TMenuItem;
    Debug1: TMenuItem;
    Hook1: TMenuItem;
    Next1: TMenuItem;
    Previous1: TMenuItem;
    rackTable1: TMenuItem;
    Add1: TMenuItem;
    Remove1: TMenuItem;
    AssumeControl1: TMenuItem;
    HookedTrack1: TMenuItem;
    CommandPlatform1: TMenuItem;
    Track1: TMenuItem;
    Characteristics1: TMenuItem;
    Domain1: TMenuItem;
    A1: TMenuItem;
    Surface1: TMenuItem;
    Subsurface1: TMenuItem;
    Land1: TMenuItem;
    General1: TMenuItem;
    IDentity1: TMenuItem;
    Pending1: TMenuItem;
    Unknown1: TMenuItem;
    AssumedFriend1: TMenuItem;
    Friend1: TMenuItem;
    Neutral1: TMenuItem;
    Suspect1: TMenuItem;
    Hostile1: TMenuItem;
    PlatformType1: TMenuItem;
    AircraftCarrierCVCVN1: TMenuItem;
    AmphibiousWarfare1: TMenuItem;
    Auxiliary1: TMenuItem;
    Chaff1: TMenuItem;
    CruiserGuidedMissileCGCGN1: TMenuItem;
    Destroyer1: TMenuItem;
    DestroyerGuidedMissileDOG1: TMenuItem;
    FrigateFF1: TMenuItem;
    FrigateGuidedMissileFFG1: TMenuItem;
    InfraredDecoy1: TMenuItem;
    JammerDecoy1: TMenuItem;
    Merchant1: TMenuItem;
    MainWarfare1: TMenuItem;
    PatrolCraftPTPTG1: TMenuItem;
    UtilityVessel1: TMenuItem;
    Other1: TMenuItem;
    Propulsion1: TMenuItem;
    Edit1: TMenuItem;
    MErge1: TMenuItem;
    Split1: TMenuItem;
    Datalink1: TMenuItem;
    o1: TMenuItem;
    From1: TMenuItem;
    Number1: TMenuItem;
    Automatic1: TMenuItem;
    Manual1: TMenuItem;
    History2: TMenuItem;
    InitiateTMA1: TMenuItem;
    Sonobuoys1: TMenuItem;
    OperatingMode1: TMenuItem;
    Depth1: TMenuItem;
    Monitor1: TMenuItem;
    Destroy1: TMenuItem;
    Break1: TMenuItem;
    RangeControlandBlindZone1: TMenuItem;
    ClearforHookedTracks1: TMenuItem;
    ClearforAllTracks1: TMenuItem;
    Remove2: TMenuItem;
    ools1: TMenuItem;
    Cursor1: TMenuItem;
    Anchor1: TMenuItem;
    Origin1: TMenuItem;
    Select1: TMenuItem;
    SendEndPointExactly1: TMenuItem;
    Overlays1: TMenuItem;
    Formation1: TMenuItem;
    argetIntercept1: TMenuItem;
    argetPriorityA1: TMenuItem;
    Opotions1: TMenuItem;
    Help1: TMenuItem;
    Contents1: TMenuItem;
    About1: TMenuItem;
    imglistPM: TImageList;
    imgListButton: TImageList;
    ImageList2: TImageList;
    ImageList1: TImageList;
    tmrMove: TTimer;
    ImageList3: TImageList;
    Panel3: TPanel;
    toolbar4: TToolBar;
    ToolButton2: TToolButton;
    ToolButton8: TToolButton;
    ToolButton15: TToolButton;
    ToolButton18: TToolButton;
    ToolButton21: TToolButton;
    ToolButton23: TToolButton;
    ToolButton25: TToolButton;
    ToolButton27: TToolButton;
    ToolButton31: TToolButton;
    ToolButton35: TToolButton;
    ToolButton42: TToolButton;
    ToolButton43: TToolButton;
    ToolButton44: TToolButton;
    ToolButton45: TToolButton;
    ToolButton69: TToolButton;
    ToolButton70: TToolButton;
    ToolButton71: TToolButton;
    ToolButton72: TToolButton;
    ToolButton73: TToolButton;
    ToolButton74: TToolButton;
    ToolButton75: TToolButton;
    ToolButton76: TToolButton;
    ToolButton77: TToolButton;
    ToolButton78: TToolButton;
    pnlTop: TPanel;
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    tbtnFullScreen: TToolButton;
    ToolButton3: TToolButton;
    cbSetScale: TComboBox;
    toolbtnDecreaseScale: TToolButton;
    toolbtnIncreaseScale: TToolButton;
    ToolButton4: TToolButton;
    toolbtnZoom: TToolButton;
    ToolBtnCentreOnHook: TToolButton;
    ToolBtnCentreOnGameCentre: TToolButton;
    ToolBtnPan: TToolButton;
    toolBtnFilterRangeRings: TToolButton;
    ToolBtnRangeRingsOnHook: TToolButton;
    ToolButton5: TToolButton;
    ToolBtnHookPrevious: TToolButton;
    ToolBtnHookNext: TToolButton;
    ToolButton6: TToolButton;
    ToolBtnAddToTrackTable: TToolButton;
    ToolBtnRemoveFromTrackTable: TToolButton;
    ToolButton7: TToolButton;
    cbAssumeControl: TComboBox;
    ToolBtnAssumeControlOfHook: TToolButton;
    ToolButton9: TToolButton;
    btn1: TToolButton;
    btn2: TToolButton;
    ToolBtnEdit: TToolButton;
    ToolButton10: TToolButton;
    ToolBtnMerge: TToolButton;
    ToolBtnSplit: TToolButton;
    btn5: TToolButton;
    btn6: TToolButton;
    btn7: TToolButton;
    ToolBtnTrackHistory: TToolButton;
    ToolBtnTransferSonobuoy: TToolButton;
    ToolBtnRemoveSonobuoy: TToolButton;
    btnToolAddMine: TToolButton;
    ToolButton11: TToolButton;
    ToolBtnFilterCursor: TToolButton;
    ToolBtnAnchorCursor: TToolButton;
    ToolBtnOptions: TToolButton;
    ToolButton12: TToolButton;
    ToolBtnContents: TToolButton;
    btn8: TToolButton;
    tBtnGameFreeze: TToolButton;
    tbtnStartGame: TToolButton;
    tBtnDoubleSpeed: TToolButton;
    ToolButton13: TToolButton;
    ToolBtnFind: TToolButton;
    ToolBtnAnnotate: TToolButton;
    ToolBtnSnapshot: TToolButton;
    ToolButton14: TToolButton;
    ToolBtnAddPlatform: TToolButton;
    ToolBtnRemovePlatformOrTrack: TToolButton;
    btnMapToolSeparator: TToolButton;
    btnAirMap: TToolButton;
    btnLandMap: TToolButton;
    btnSeaMap: TToolButton;
    btnLayerTool: TToolButton;
    btnBrowse: TToolButton;
    btnInfoTool: TToolButton;
    separator: TToolButton;
    btnLogistic: TToolButton;
    btn3: TToolButton;
    btnPlotting: TToolButton;
    ToolButton46: TToolButton;
    ToolButton47: TToolButton;
    ToolBtnSelectPlatform: TToolButton;
    btnMultiMode: TToolButton;
    ToolButton16: TToolButton;
    btnToolBtnSlide: TToolButton;
    ToolButtonCom: TToolButton;
    ToolButton17: TToolButton;
    ToolButtonPlatformView: TToolButton;
    ToolButtonTacticalInfoSet: TToolButton;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure toolbtnZoomClick(Sender: TObject);
    procedure ToolBtnPanClick(Sender: TObject);
    procedure tbtnFullScreenClick(Sender: TObject);
    procedure ToolBtnCentreOnGameCentreClick(Sender: TObject);
    procedure cbSetScaleChange(Sender: TObject);
    procedure tbtnStartGameClick(Sender: TObject);
    procedure ToolBtnCentreOnHookClick(Sender: TObject);
    procedure toolBtnFilterRangeRingsClick(Sender: TObject);
    procedure ToolBtnRangeRingsOnHookClick(Sender: TObject);
    procedure ToolBtnAssumeControlOfHookClick(Sender: TObject);
    procedure cbAssumeControlChange(Sender: TObject);
    procedure ToolBtnAddToTrackTableClick(Sender: TObject);
    procedure ToolBtnRemoveFromTrackTableClick(Sender: TObject);
    procedure Debug1Click(Sender: TObject);
    procedure StatusBar1DrawPanel(StatusBar: TStatusBar; Panel: TStatusPanel;
      const Rect: TRect);
    procedure tmrMoveTimer(Sender: TObject);
    procedure toolbtnDecreaseScaleClick(Sender: TObject);
    procedure toolbtnIncreaseScaleClick(Sender: TObject);
    procedure ToolBtnTrackHistoryClick(Sender: TObject);
    procedure btnToolAddMineClick(Sender: TObject);
    procedure ToolBtnAddPlatformClick(Sender: TObject);
    procedure ToolBtnFilterCursorClick(Sender: TObject);
    procedure ToolBtnAnchorCursorClick(Sender: TObject);
    procedure ToolBtnOptionsClick(Sender: TObject);
    procedure btnAirMapClick(Sender: TObject);
  private
    { Private declarations }
    FCtrlPanelManager  : TControlPanelManager;
    FTrackTableManager : TTrackTableManager;

    FPickedObject      : TObject;
    FDraggedObject     : TDragObject;
    FAnchorTrack       : TSimObject;
    FHookOnPlatform    : Boolean;
    FAnchorFilterEnabled: Boolean;

    { hooked track }
    FCenterHookedTrack : TObject;
    FLastMapCenterY,
    FLastMapCenterX    : double;

    { ring hooked track }
    FRingHookedTrack   : TObject;
    FRangeRingOnHook   : Boolean;

    FLeftMouseDown     : Boolean;
    isFullScreen       : Boolean;

    function PickObject(mx, my: integer): TObject;
    procedure TryControlMapObject(aObject: TObject);

    procedure DisplayGameTime(const gSpeed: single; const gTime: TDateTime);

    procedure objectSelectedOnMap(obj: TObject);
    function getSelectedTrack: TObject;

    procedure RepositionObject(aObj : TDragObject; beginDrag : Boolean = False);

    procedure NetSendPlatformReposition(const pi_id: integer;
        const x, y: double);

  public
    MapTouch : TMapXTouch;

    { Public declarations }
    procedure Be_A_FullMap(const full: Boolean);
    procedure Refresh_AssumeControl;
    procedure SetControlledTrack(track: TT3Track);
    procedure SetRoleClient(role : TMachineRole);

    { thread updating }
    procedure UpdateFormData(Sender: TObject);
    procedure UpdateGameTime(Sender: TObject);
    procedure UpdateCenter(Sender: TObject);

    { map event handle }
    procedure MapViewChanged(Sender: TObject);
    procedure MapToolUsed(ASender: TObject; ToolNum: Smallint; X1, Y1, X2, Y2,
      Distance: double; Shift, Ctrl: WordBool; var EnableDefault: WordBool);
    procedure MapMouseMove(Sender: TObject; Shift: TShiftState; x, y: integer);
    procedure MapMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; x, y: integer);
    procedure MapMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; x, y: integer);
    procedure MapMouseDouble(Sender: TObject);
    procedure MapMouseSingle(Sender: TObject);
    procedure MapMouseExit(Sender: TObject);
    { ==== }

    // ro prop
    property SelectedTrack : TObject read getSelectedTrack;//FSelectedTrack;
    property CtrlPanelManager: TControlPanelManager read FCtrlPanelManager;
  end;

var
  frmTacticalDisplay: TfrmTacticalDisplay;

implementation

uses
   uT3ClientManager, uGlobalVar, uLogDisplay, uBaseCoordSystem,
   uT3MountedWeapon, uT3MountedGun,uT3MountedGunAutoManual,
   uT3MountedGunCIWS, uRPLibrary, uSettingCoordinate;
{$R *.dfm}


const
  CMin_Z = 0;
  CMax_Z = 14;

function ZoomIndexToScale(const i: integer): double;
begin
  if i < -3 then
    result := 0.125
  else if i > 14 then
    result := 2500.0
  else
    result := Power(2.0, (i - 3));
end;

function FixMapZoom(z: double): double;
begin
  if z >= 1.0 then
    result := Round(z)
  else
    result := 0.001 * Round(z * 1000);
end;

function FindClosestZoomIndex(const z: double): integer;
var
  i: integer;
begin
  if z >= 2500 then
    result := CMax_Z
  else if z <= 0.125 then
    result := CMin_Z
  else
  begin
    i := Round(Log2(z));
    result := i + 3;
  end;
end;

procedure TfrmTacticalDisplay.Be_A_FullMap(const full: Boolean);
begin
//  pnlLeft.Visible := NOT full;

  if full then
  begin
    WindowState := wsMaximized;
    BorderStyle := bsNone;
  end
  else
  begin
    WindowState := wsNormal;
    BorderStyle := bsSingle;
  end;

end;

procedure TfrmTacticalDisplay.btnAirMapClick(Sender: TObject);
begin
  btnLandMap.Down := False;
  btnSeaMap.Down := False;

  clientManager.SwitchMap(1);

end;

procedure TfrmTacticalDisplay.btnToolAddMineClick(Sender: TObject);
begin
//  if not Assigned(frmDeployMine) then
//    frmDeployMine := TfrmDeployMine.Create(Self);
//
//  frmDeployMine.Show;

end;

procedure TfrmTacticalDisplay.cbAssumeControlChange(Sender: TObject);
var
  aObject: TSimObject;
begin
  aObject := TSimObject(cbAssumeControl.Items.Objects[cbAssumeControl.ItemIndex]);

  if aObject <> nil then
  begin
    if aObject is TT3Track then
    begin
      clientManager.UnSelectAllTracks;
      clientManager.SelectedTrack  := TT3Track(aObject);
      TT3Track(aObject).isSelected := true;
      FCtrlPanelManager.SetHookedTrack(TT3Track(aObject));
    end;
  end;

end;

procedure TfrmTacticalDisplay.cbSetScaleChange(Sender: TObject);
var
  z: double;
  s: string;
begin
  if cbSetScale.ItemIndex < 0 then
    exit;

  s := cbSetScale.Items[cbSetScale.ItemIndex];
  try
    z := StrToFloat(s);
    clientManager.SimMap.SetMapZoom(z * 2);
    lblRangeScale.Caption := cbSetScale.Text;
  finally

  end;
end;

procedure TfrmTacticalDisplay.Debug1Click(Sender: TObject);
begin
  if not Assigned(frmLogDisplay) then
    frmLogDisplay := TfrmLogDisplay.Create(Self);

  frmLogDisplay.Show;
end;

procedure TfrmTacticalDisplay.DisplayGameTime(const gSpeed: single;
  const gTime: TDateTime);
var
  i: integer;
begin
  if abs(gSpeed) < 0.000001 then
  begin
    StatusBar1.Panels[9].Text := 'FROZEN';
    StatusBar1.Repaint;
  end
  else
  begin
    if gSpeed < 1 then
    begin
      i := Round(1.0 / gSpeed);
      StatusBar1.Panels[9].Text := '1/' + IntToStr(i) + ' X';
    end
    else
      StatusBar1.Panels[9].Text := IntToStr(Round(gSpeed)) + ' X';

  end;

  StatusBar1.Panels[10].Text := FormatDateTime('ddhhnnss', gTime)
    + 'Z' + FormatDateTime('mmmyyyy', gTime);

  if machineRole = crController then
    StatusBar1.Panels[1].Text := 'Entities = ' + IntToStr
      (clientManager.SimPlatforms.itemCount);

  StatusBar1.Repaint;
end;

procedure TfrmTacticalDisplay.FormCreate(Sender: TObject);
var
  i: integer;
  z: double;
begin
  { create map touch }
  MapTouch         := TMapXTouch.Create(Self);
  MapTouch.Parent  := Self;
  MapTouch.Align   := alClient;
  MapTouch.Visible := True;

  if not MapTouch.WinXP then
    RegisterTouchWindow(MapTouch.Handle, 0);

  { create control panel manager }
  FCtrlPanelManager := TControlPanelManager.Create(TacticalDisplayControlPanel,
    HookContactInfoTraineeDisplay);
  FCtrlPanelManager.EmptyField;

  { create track table manager }
  FTrackTableManager := TTrackTableManager.Create(lvTrackTable);

  FLeftMouseDown  := False;
  FHookOnPlatform := False;

  { set scaling }
  cbSetScale.Items.Clear;
  for i := CMin_Z to CMax_Z do
  begin
    z := ZoomIndexToScale(i);
    cbSetScale.Items.Add(FloatToStr(z));
  end;
  lblRangeScale.Caption := cbSetScale.Text;

  StatusBar1.DoubleBuffered := true;
  StatusBar1.Panels[9].Text := 'FROZEN';
  StatusBar1.Repaint;

  { set desktop }
  width  := Screen.Monitors[0].width;
  height := Screen.Monitors[0].height;
  left   := Screen.Monitors[0].left;
  top    := Screen.Monitors[0].top;
end;

procedure TfrmTacticalDisplay.FormDestroy(Sender: TObject);
begin
  MapTouch.Disconnect;

  FCtrlPanelManager.Free;
  FTrackTableManager.Free;
end;

function TfrmTacticalDisplay.getSelectedTrack: TObject;
begin
  result := clientManager.SelectedTrack;
end;

procedure TfrmTacticalDisplay.TryControlMapObject(aObject: TObject);
begin
  { try to control the object }
  if (aObject <> nil) and (aObject is TT3Track) then
  begin
    case machineRole of
      crController, crCubicle:
        begin

          { this is detected track, cannot be controlled }
          if (aObject is TT3PointTrack) and (not TT3PointTrack(aObject).CanControl) then
            exit;

          if Assigned(clientManager.ControlledTrack) then
          begin
            if clientManager.ControlledTrack is TT3PointTrack then
              TT3PointTrack(clientManager.ControlledTrack).isControlled := False;

            clientManager.ControlledTrack.isSelected := False;
          end;

          TT3Track(aObject).isSelected := True;
          { object must be point track }
          TT3PointTrack(aObject).isControlled := True;

          clientManager.ControlledTrack := TT3Track(aObject);
          clientManager.SelectedTrack   := TT3Track(aObject);

          { set control panel to controlling track }
          SetControlledTrack(TT3Track(aObject));
        end;
    end;
  end;
end;

procedure TfrmTacticalDisplay.MapMouseDouble(Sender: TObject);
var
  pt: TPoint;
  aObject : TObject;
begin
  GetCursorPos(pt);
  pt := MapTouch.ScreenToClient(pt);

  aObject := PickObject(pt.x, pt.y);

  { try control object }
  TryControlMapObject(aObject);
end;

procedure TfrmTacticalDisplay.MapMouseDown
  (Sender: TObject; Button: TMouseButton; Shift: TShiftState; x, y: integer);
var
  mx, my: double;
  pos: TPoint;
begin
  clientManager.Converter.ConvertToMap(x, y, mx, my);
  GetCursorPos(pos);

  if Button = mbLeft then
    FLeftMouseDown := True;

  FPickedObject := PickObject(x, y);

  { try select object }
  objectSelectedOnMap(FPickedObject);

  if FLeftMouseDown and (MapTouch.CurrentTool = mtSelectObject) and Assigned(FPickedObject) then
  begin
    { start repos if possible }
    tmrMove.Enabled := True;
  end;

end;

procedure TfrmTacticalDisplay.MapMouseExit(Sender: TObject);
begin

end;

procedure TfrmTacticalDisplay.MapMouseMove(Sender: TObject; Shift: TShiftState;
  x, y: integer);
begin
  if FLeftMouseDown and (MapTouch.CurrentTool = mtSelectObject) and Assigned(FPickedObject) then
  begin
    { repos object }
    if machineRole in [crWasdal, crController] then
    begin
      if Assigned(FDraggedObject) then
      begin
        if FPickedObject is TT3Track then
          with TT3Track(FPickedObject) do
          begin
            clientManager.Converter.ConvertToScreen(getPositionX, getPositionY,
              FDraggedObject.FPointFrom.X, FDraggedObject.FPointFrom.Y);
          end;

        FDraggedObject.FPointTO.X := X;
        FDraggedObject.FPointTO.Y := Y;

        RepositionObject(FDraggedObject, True);
      end;
    end;
  end;

end;

procedure TfrmTacticalDisplay.MapMouseSingle(Sender: TObject);
var
  pt: TPoint;
  mx, my: double;
  posX1, posY1, RangeGun: Double;
  isInsideBlindzoneGun : Boolean;
begin
  GetCursorPos(pt);
  pt := MapTouch.ScreenToClient(pt);

  clientManager.Converter.ConvertToMap(pt.x, pt.y, mx, my);

  case MapTouch.CurrentTool of
    mtSelectObject:
      begin

      end;
    mtDeployMine:;

    mtDeployPosition:
      begin
        if (Assigned(frmRPLibrary)) and (frmRPLibrary.Visible) then
        begin
          frmRPLibrary.MapPositionX := mx;
          frmRPLibrary.MapPositionY := my;
        end;
//        else if (Assigned(frmlaunchCountRP)) and (frmlaunchCountRP.Visible) then
//        begin
//          frmlaunchCountRP.MapPositionX := mx;
//          frmlaunchCountRP.MapPositionY := my;
//        end
//        else
//        if (Assigned(frmEditNonRealTimeTrack)) and (frmEditNonRealTimeTrack.Visible) then
//        begin
//          frmEditNonRealTimeTrack.MapPositionX := mx;
//          frmEditNonRealTimeTrack.MapPositionY := my;
//
//          frmEditNonRealTimeTrack.edtNRBPosition.Text := formatDM_longitude(mx) + ';'
//          + formatDM_latitude(my);
//
//          frmEditNonRealTimeTrack.edtposition.Text := formatDM_longitude(mx) + ';'
//          + formatDM_latitude(my);
//
//          frmEditNonRealTimeTrack.edtAOPCenter.Text := formatDM_longitude(mx) + ';'
//          + formatDM_latitude(my);
//        end
//        else
//        if ((Assigned(fmPlatformGuidance1)) and (fmPlatformGuidance1.Visible)
//           and (fmPlatformGuidance1.grpCircle.Visible)
//           and (fmPlatformGuidance1.grpStation.Visible))
//           then
//        begin
//          fmPlatformGuidance1.MapPositionX := mx;
//          fmPlatformGuidance1.MapPositionY := my;
//        end
//        else
//        if ((Assigned(fmWeapon1)) and (fmWeapon1.Visible)
//            and (fmWeapon1.grbGunEngagementAutomaticManualMode.Visible)) then
//        begin
//          fmWeapon1.MapPositionX := mx;
//          fmWeapon1.MapPositionY := my;
//        end;


        {wasdal UI}
        if machineRole = crWasdal then
        begin
//          if Assigned(frmGuidance) and (frmGuidance.fmPlatformGuidance1.Visible)
//             and (frmGuidance.fmPlatformGuidance1.grpCircle.Visible)
//             and (frmGuidance.fmPlatformGuidance1.grpStation.Visible)
//             then
//          begin
//            frmGuidance.fmPlatformGuidance1.MapPositionX := mx;
//            frmGuidance.fmPlatformGuidance1.MapPositionY := my;
//          end
//          else
//          if Assigned(frmWeapon) and (frmWeapon.fmWeapon1.Visible)
//              and (frmWeapon.fmWeapon1.grbGunEngagementAutomaticManualMode.Visible)
//             then
//          begin
//            frmWeapon.fmWeapon1.MapPositionX := mx;
//            frmWeapon.fmWeapon1.MapPositionY := my;
//          end;
        end;


        MapTouch.CurrentTool := mtSelectObject;
      end;
  end;
end;

procedure TfrmTacticalDisplay.MapMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; x, y: integer);
var
  mx, my, range, bearing, newX, newY: Double;
begin
  clientManager.Converter.ConvertToMap(x, y, mx, my);

  if MapTouch.CurrentTool <> mtAddWaypoint then
    MapTouch.CurrentTool := mtSelectObject;

  FLeftMouseDown := False;

  { reset reposition }
  if Assigned(FDraggedObject) then
  begin
    if FDraggedObject.FDraggedObject is TT3Track then
      NetSendPlatformReposition(TT3Track(FDraggedObject.FDraggedObject).objectInstanceIndex,
        mx, my);
    RepositionObject(FDraggedObject);
    FreeAndNil(FDraggedObject);
  end;
  { finish reposition }

end;

procedure TfrmTacticalDisplay.MapToolUsed(ASender: TObject; ToolNum: Smallint;
  X1, Y1, X2, Y2, Distance: double; Shift, Ctrl: WordBool;
  var EnableDefault: WordBool);
begin

end;

procedure TfrmTacticalDisplay.MapViewChanged(Sender: TObject);
var
  i: integer;
  z, zCapt: double;
begin
  z := FixMapZoom(MapTouch.Zoom);

  zCapt := 0.5 * z;
  cbSetScale.Text := FloatToStr(zCapt);
  lblRangeScale.Caption := cbSetScale.Text;

  i := FindClosestZoomIndex(zCapt);

  toolbtnDecreaseScale.Enabled := i > 0;
  toolbtnIncreaseScale.Enabled := i < CMax_Z;

  if toolBtnFilterRangeRings.Down then
  begin
    i := FindClosestZoomIndex(zCapt);
    z := ZoomIndexToScale(i);
    clientManager.RangeRing.Interval := 0.25 * z;
  end;

end;

procedure TfrmTacticalDisplay.NetSendPlatformReposition(const pi_id: integer;
  const x, y: double);
var
  rec: TRecCmd_Platform_MOVE;
begin
  rec.PlatfomID := pi_id;
  rec.OrderID := CORD_ID_REPOS;
  rec.x := x;
  rec.y := y;

  simManager.NetCmdSender.CmdRepos(rec);
end;

procedure TfrmTacticalDisplay.objectSelectedOnMap(obj: TObject);
var
  iObj: TObject;
  i: integer;
begin
  if obj = nil then
    exit;

  if FLeftMouseDown and (MapTouch.CurrentTool = mtSelectObject) then
  begin
    clientManager.UnSelectAllTracks;

    if obj is TT3Track then
    begin
      TT3Track(obj).isSelected    := True;
      clientManager.SelectedTrack := TT3Track(obj);
      FCtrlPanelManager.SetHookedTrack(TT3Track(obj));
    end;

    for i := 0 to cbAssumeControl.Items.Count - 1 do
    begin
      iObj := cbAssumeControl.Items.Objects[i];

      if clientManager.SelectedTrack = iObj then
        break;
    end;

    cbAssumeControl.ItemIndex := i;
  end;

end;

function TfrmTacticalDisplay.PickObject(mx, my: integer): TObject;
begin
  result := clientManager.FindNearestTrack(mx, my, 10);
end;

procedure TfrmTacticalDisplay.Refresh_AssumeControl;
var
  i,j: Integer;
  aObject: TSimObject;
  pi : TT3PlatformInstance;
  name: string;
  index: Integer;
begin
  cbAssumeControl.Items.Clear;
  cbAssumeControl.Clear;

  index := 0;

  for i := 0 to clientManager.CubicleTracks.itemCount - 1 do
  begin
    aObject := TSimObject(clientManager.CubicleTracks.getObject(i));

    if (aObject is TT3PointTrack) and TT3PointTrack(aObject).CanControl then
    begin

      pi := clientManager.FindT3PlatformByID(TT3PointTrack(aObject).ObjectInstanceIndex);
      if Assigned(pi) and (not pi.FreeMe) then
        if pi is TT3Vehicle then
        begin

          name := pi.InstanceName;
          cbAssumeControl.Items.AddObject(name, aObject);

        end;

    end;

  end;

  if (clientManager.ControlledTrack <> nil) and
    (clientManager.ControlledTrack = aObject) then
  begin
    for j := 0 to cbAssumeControl.Items.Count - 1 do
    begin
      if TT3Track(cbAssumeControl.Items.Objects[j])
            = clientManager.ControlledTrack then
      begin
        index := j;
        break;
      end;
    end;
  end;

  if cbAssumeControl.Items.Count > 0 then
    cbAssumeControl.ItemIndex := index;
end;

procedure TfrmTacticalDisplay.RepositionObject(aObj : TDragObject; beginDrag : Boolean = False);
var
  mx,my,r,b : Double;
begin
  with clientManager.ObjectVisualManager.RepositionLine do
  begin
    if Assigned(aObj) then
    if aObj.FDraggedObject is TT3Track then
    begin
      X1 := TT3Track(aObj.FDraggedObject).PosX;
      Y1 := TT3Track(aObj.FDraggedObject).PosY;

      clientManager.Converter.ConvertToMap(aObj.FPointTO.X,
        aObj.FPointTO.Y, mx,my);

      r := CalcRange(TT3Track(aObj.FDraggedObject).PosX,
                  TT3Track(aObj.FDraggedObject).PosY,mx,my);
      b := CalcBearing(TT3Track(aObj.FDraggedObject).PosX,
                  TT3Track(aObj.FDraggedObject).PosY,mx,my);

      Range   := r;
      Bearing := b;
      IsOnce  := False;
      Visible := beginDrag;
    end;
  end;

end;

procedure TfrmTacticalDisplay.SetControlledTrack(track: TT3Track);
var
  pi: TT3PlatformInstance;
begin
  FCtrlPanelManager.SetControlledTrack(track);

  pi := clientManager.FindT3PlatformByID(track.ObjectInstanceIndex);
  if Assigned(pi) then
  begin
    Caption := 'Tactical Display - ' + clientManager.Scenario.ScenarioName +
      ' - ' + pi.InstanceName + ' on ' + clientManager.CubicleName + ' - ' +
      clientManager.ConsoleName;
  end;

end;

procedure TfrmTacticalDisplay.SetRoleClient(role : TMachineRole);
begin
  case role of
    crController:
      begin
        ToolButton27.Show;
        ToolButton31.Show;
        ToolButton35.Show;
        ToolBtnAddPlatform.Show;

        tBtnGameFreeze.Show;
        tbtnStartGame.Show;
        tBtnDoubleSpeed.Show;

        ToolBtnAnnotate.Show;
        ToolBtnFind.Show;
        ToolBtnSnapshot.Show;

		    ToolBtnRemovePlatformOrTrack.Show;   //mk
//        fmPlatformGuidance1.mnReturntoBase1.Enabled := True;
//
//        if not Assigned(fTransferSonobuoy) then
//          fTransferSonobuoy := TfTransferSonobuoy.Create(self);
//        fTransferSonobuoy.btnSonobuoyControlCombo.Enabled := True;

        cbAssumeControl.Items.Clear;
      end;
    crCubicle :
      begin
        ToolButton27.Hide;
        ToolButton31.Hide;
        ToolButton35.Hide;
        ToolBtnAddPlatform.Hide;

        tBtnGameFreeze.Hide;
        tbtnStartGame.Hide;
        tBtnDoubleSpeed.Hide;

        ToolBtnAnnotate.Hide;
        ToolBtnFind.Hide;
        ToolBtnSnapshot.Hide;

        ToolBtnRemovePlatformOrTrack.Hide;    //mk
//		    fmPlatformGuidance1.mnReturntoBase1.Enabled := False;
//
//        if not Assigned(fTransferSonobuoy) then
//          fTransferSonobuoy := TfTransferSonobuoy.Create(self);
//        fTransferSonobuoy.btnSonobuoyControlCombo.Enabled := False;
      end;
  end;

  Refresh_AssumeControl;

end;

procedure TfrmTacticalDisplay.StatusBar1DrawPanel(StatusBar: TStatusBar;
  Panel: TStatusPanel; const Rect: TRect);
var
  I: Integer;
  gunfire, LockFCR, Emcon, jamming: Boolean;
//  EmconCat : TEMCON_Category;
  pf : TT3PlatformInstance;
  wpn : TT3MountedWeapon;
begin
  case Panel.Index of
    4 : //emcon
      begin
        Emcon := False;
        if Assigned(clientManager.ControlledTrack) then
        begin
//          for I := 0 to TT3Vehicle(simMgrClient.ControlledPlatform).ListEMCON.Count - 1 do
//          begin
//            EmconCat := TT3Vehicle(simMgrClient.ControlledPlatform).ListEMCON.Items[i];
//
//            if TEMCON_ChekBoxStatus(EmconCat.Mode) = Checked then
//            begin
//              Emcon := True;
//              Break;
//            end;
//          end;
        end;

        if Emcon then
        begin
          StatusBar1.Canvas.Brush.Color := clBtnFace;
          StatusBar1.Canvas.FillRect(Rect);
          StatusBar1.Font.Color := clBlack;
          Panel.Bevel := pbLowered;
          StatusBar1.Canvas.TextOut(Rect.left + 4, Rect.top + 3, Panel.Text);
        end
        else
        begin
          StatusBar1.Canvas.Brush.Color := clBtnFace;
          StatusBar1.Canvas.FillRect(Rect);
          StatusBar1.Font.Color := clBlack;
          Panel.Bevel := pbLowered;
        end;
      end;
    5 : //jamming
      begin
        jamming := False;

        if Assigned(clientManager.ControlledTrack) then
        begin
//          jamming := simMgrClient.ControlledPlatform.VehicleOnJamming.Count <> 0
        end;

        if jamming then
        begin
          StatusBar1.Canvas.Brush.Color := clYellow;
          StatusBar1.Canvas.FillRect(Rect);
          StatusBar1.Font.Color := clBlack;
          Panel.Bevel := pbLowered;
          StatusBar1.Canvas.TextOut(Rect.left + 4, Rect.top + 3, Panel.Text);
        end
        else
        begin
          StatusBar1.Canvas.Brush.Color := clBtnFace;
          StatusBar1.Canvas.FillRect(Rect);
          StatusBar1.Font.Color := clBlack;
          Panel.Bevel := pbLowered;
        end;
      end;
    6 : //Gun Fire
      begin
        gunfire := False;
        if Assigned(clientManager.ControlledTrack) then
        begin
          pf := simManager.FindT3PlatformByID(clientManager.ControlledTrack.ObjectInstanceIndex);
//          if Assigned(pf) and (pf is TT3Vehicle) then
//            for wpn in TT3Vehicle(pf).MountedWeapons do
//            begin
//              if (wpn is TT3MountedGun) then
//              begin
//                if wpn.WeaponStatus = wsFiring then
//                begin
//                  gunfire := True;
//                  Break;
//                end;
//              end;
//          end;

          if gunfire then
          begin
            StatusBar1.Canvas.Brush.Color := clYellow;
            StatusBar1.Canvas.FillRect(Rect);
            StatusBar1.Font.Color := clBlack;
            StatusBar1.Canvas.TextOut(Rect.left + 4, Rect.top + 3, Panel.Text);
          end
          else
          begin
            StatusBar1.Canvas.Brush.Color := clBtnFace;
            StatusBar1.Canvas.FillRect(Rect);
            StatusBar1.Font.Color := clBlack;
          end;
        end;
      end;
    7 : //Fire Control
      begin
        if Assigned(clientManager.ControlledTrack) then
        begin
//        LockFCR := simMgrClient.ControlledPlatform.LockFCR
          if LockFCR then
          begin
            StatusBar1.Canvas.Brush.Color := clRed;
            StatusBar1.Canvas.FillRect(Rect);
            StatusBar1.Font.Color := clBlack;
            StatusBar1.Canvas.TextOut(Rect.left + 4, Rect.top + 3, Panel.Text);
          end
          else
          begin
            StatusBar1.Canvas.Brush.Color := clBtnFace;
            StatusBar1.Canvas.FillRect(Rect);
            StatusBar1.Font.Color := clBlack;
          end;
        end;
      end;
    9 : //State Game
      begin
        if Panel.Text = 'FROZEN' then
        begin
          StatusBar1.Canvas.Brush.Color := clRed;
          StatusBar1.Canvas.FillRect(Rect);
          StatusBar1.Font.Color := clBlack;
        end
        else if Panel.Text = '1 X' then
        begin
          StatusBar1.Canvas.Brush.Color := clBtnFace;
          StatusBar1.Canvas.FillRect(Rect);
          StatusBar1.Font.Color := clBlack;
        end
        else
        begin
          StatusBar1.Canvas.Brush.Color := clYellow;
          StatusBar1.Canvas.FillRect(Rect);
          StatusBar1.Font.Color := clBlack;

        end;

        StatusBar1.Canvas.TextOut(Rect.left + 4, Rect.top + 3, Panel.Text);
      end;
  end;
end;

procedure TfrmTacticalDisplay.tbtnFullScreenClick(Sender: TObject);
begin
  if not isFullScreen then
  begin
    Be_A_FullMap(True);
    isFullScreen := True;

  end
  else
  begin
    Be_A_FullMap(False);
    isFullScreen := False;
  end;
  mnFullScreen1.Checked := isFullScreen;

end;

procedure TfrmTacticalDisplay.tbtnStartGameClick(Sender: TObject);
var
  r: TRecCmd_GameCtrl;
  gs: double;
begin ;
  case TToolButton(Sender).Tag of
    // pause
    1:
      begin
        r.GameCtrl := CORD_ID_pause;
        r.GameSpeed := 0.0;
        clientManager.NetCmdSender.CmdGameControl(r);
      end;

    // start
    2:
      begin
        r.GameCtrl := CORD_ID_start;
        r.GameSpeed := 1.0;
        clientManager.NetCmdSender.CmdGameControl(r);
      end;

    // game speed
    3:
      if clientManager.GameSpeed >= 1.0 then
      begin
        gs := Round(clientManager.GameSpeed);
        if abs(gs) < 0.0001 then // nol
          gs := 1.0
        else
        begin
          gs := 2.0 * gs;
          if gs > 16.0 then
            exit;
        end;

        r.GameCtrl := CORD_ID_game_speed;
        r.GameSpeed := gs;

        clientManager.NetCmdSender.CmdGameControl(r);
      end;
  end;


  // //// untuk replay
  // if TGamePlayType(vGameDataSetting.GamePlayMode.GameType) = gpmReplay then
  // begin
  // inc(simMgrClient.InitFirstReplay);
  // if simMgrClient.InitFirstReplay < 1 then
  // begin
  // simMgrClient.SimDetectedTrackList.ClearAndRemove;
  // simMgrClient.ResetScenario;
  // simMgrClient.LoadScenarioID(vGameDataSetting);
  // end;
  // end;

end;

procedure TfrmTacticalDisplay.tmrMoveTimer(Sender: TObject);
begin
  if FLeftMouseDown and (MapTouch.CurrentTool = mtSelectObject) and Assigned(FPickedObject) then
  begin
    FDraggedObject := TDragObject.Create;

    { repos object }
    if machineRole in [crWasdal, crController] then
    begin
      if FPickedObject is TT3Track then
      with TT3Track(FPickedObject) do
      begin
        clientManager.Converter.ConvertToScreen(getPositionX, getPositionY,
          FDraggedObject.FPointFrom.X, FDraggedObject.FPointFrom.Y);
      end;

      FDraggedObject.FPointTO.X := FDraggedObject.FPointFrom.X;
      FDraggedObject.FPointTO.Y := FDraggedObject.FPointFrom.Y;
      FDraggedObject.FDraggedObject := FPickedObject;

      RepositionObject(FDraggedObject, True);
    end;

    tmrMove.Enabled := False;
  end;

end;

procedure TfrmTacticalDisplay.ToolBtnAddPlatformClick(Sender: TObject);
begin
  if not Assigned(frmRPLibrary) then
    frmRPLibrary := TfrmRPLibrary.Create(self);

  frmRPLibrary.SetUpGroupAndForce;
  frmRPLibrary.btnLaunch.Enabled := False;
  frmRPLibrary.Show;

end;

procedure TfrmTacticalDisplay.ToolBtnAddToTrackTableClick(Sender: TObject);
begin

  if not Assigned(clientManager.SelectedTrack) then
    Exit;

  FTrackTableManager.AddTrack(clientManager.SelectedTrack);

end;

procedure TfrmTacticalDisplay.ToolBtnAnchorCursorClick(Sender: TObject);
begin
  if clientManager.SelectedTrack <> nil then
  begin
    FAnchorTrack := clientManager.SelectedTrack;

    clientManager.ObjectVisualManager.LineVisual.X1 := FAnchorTrack.getPositionX;
    clientManager.ObjectVisualManager.LineVisual.Y1 := FAnchorTrack.getPositionY;
  end;

end;

procedure TfrmTacticalDisplay.ToolBtnAssumeControlOfHookClick(Sender: TObject);
begin
  if (clientManager.SelectedTrack <> nil) then
  begin
    if clientManager.SelectedTrack is TT3PointTrack then
    begin
      TT3PointTrack(clientManager.SelectedTrack).isControlled := True;
      TT3PointTrack(clientManager.SelectedTrack).isSelected   := True;
    end;

    if Assigned(clientManager.ControlledTrack) then
    begin
      if clientManager.ControlledTrack is TT3PointTrack then
        TT3PointTrack(clientManager.ControlledTrack).isControlled := False;

      clientManager.ControlledTrack.isSelected := False;
    end;

    clientManager.ControlledTrack := clientManager.SelectedTrack;

    SetControlledTrack(clientManager.SelectedTrack);
  end;

end;

procedure TfrmTacticalDisplay.ToolBtnCentreOnGameCentreClick(Sender: TObject);
var
  long, lat: double;
begin
  long := clientManager.Scenario.DBGameAreaDefinition.Game_Centre_Long;
  lat := clientManager.Scenario.DBGameAreaDefinition.Game_Centre_Lat;
  clientManager.SimMap.SetMapCenter(long, lat);

  StatusBar1.Panels[0].Text := TToolButton(Sender).Hint;
  if ToolBtnCentreOnHook.Down then          // mubdi - mengembalikan centeronhook
  begin                                     // button yang tertahan
    ToolBtnCentreOnHook.Down := False;
    FHookOnPlatform := not FHookOnPlatform;
  end;
end;

procedure TfrmTacticalDisplay.ToolBtnCentreOnHookClick(Sender: TObject);
begin
  if clientManager.SelectedTrack = nil then
    exit;

  FHookOnPlatform := not FHookOnPlatform;
  ToolBtnCentreOnHook.Down := FHookOnPlatform;

  if FHookOnPlatform then
  begin
    try
      FCenterHookedTrack := clientManager.SelectedTrack;

      clientManager.SimMap.SetMapCenter(TT3Track(FCenterHookedTrack).getPositionX,
         TT3Track(FCenterHookedTrack).getPositionY);

      FLastMapCenterY := TT3Track(FCenterHookedTrack).getPositionY;
      FLastMapCenterX := TT3Track(FCenterHookedTrack).getPositionX;
    except
      clientManager.SelectedTrack := nil;
      FCenterHookedTrack := nil;
    end;
  end
  else
  begin
    FCenterHookedTrack := nil;
  end;

  StatusBar1.Panels[0].Text := TToolButton(Sender).Hint;

end;

procedure TfrmTacticalDisplay.toolbtnDecreaseScaleClick(Sender: TObject);
var
  i: Integer; // zoom out
  z, zNow: double;
begin
  zNow := FixMapZoom(0.5 * clientManager.SimMap.FMap.Zoom);
  i := FindClosestZoomIndex(zNow);
  z := ZoomIndexToScale(i);

  if z >= zNow then
    i := i - 1;
  if i < 0 then
    i := 0;
  if i > CMax_Z then
    i := CMax_Z;

  // do the zoom in
  cbSetScale.ItemIndex := i;
  cbSetScaleChange(cbSetScale);

end;

procedure TfrmTacticalDisplay.ToolBtnFilterCursorClick(Sender: TObject);
begin
  FAnchorFilterEnabled := not FAnchorFilterEnabled;
  ToolBtnFilterCursor.Down := FAnchorFilterEnabled;
  clientManager.ObjectVisualManager.LineVisual.Visible := FAnchorFilterEnabled;

end;

procedure TfrmTacticalDisplay.toolBtnFilterRangeRingsClick(Sender: TObject);
var
  i: Integer;
  rrVis: Boolean;
  z: double;
begin
  rrVis := toolBtnFilterRangeRings.Down;

  if rrVis then
  begin
    z := FixMapZoom(MapTouch.Zoom);
    i := FindClosestZoomIndex(z);
    z := ZoomIndexToScale(i);
    clientManager.RangeRing.Interval := 0.125 * z;
  end;

  if Assigned(clientManager.SelectedTrack) then
    FRingHookedTrack := clientManager.SelectedTrack;

  if Assigned(FRingHookedTrack)then
  begin
    clientManager.RangeRing.mX := TT3Track(FRingHookedTrack).getPositionX;
    clientManager.RangeRing.mY := TT3Track(FRingHookedTrack).getPositionY;
    clientManager.RangeRing.Visible := rrVis;
  end;

  StatusBar1.Panels[0].Text := TToolButton(Sender).Hint;
end;

procedure TfrmTacticalDisplay.toolbtnIncreaseScaleClick(Sender: TObject);
var
  i: Integer;
  z, zNow: double;
begin
  zNow := FixMapZoom(0.5 * clientManager.SimMap.FMap.Zoom);
  i := FindClosestZoomIndex(zNow);
  z := ZoomIndexToScale(i);

  if z <= zNow then
    i := i + 1;
  if i < 0 then
    i := 0;
  if i > CMax_Z then
    i := CMax_Z;

  // do the zoom out
  cbSetScale.ItemIndex := i;
  cbSetScaleChange(cbSetScale);
end;

procedure TfrmTacticalDisplay.ToolBtnOptionsClick(Sender: TObject);
begin
  if clientManager.SelectedTrack <> nil then
  begin
    MapTouch.CurrentTool := mtAnchorTool;
    fSettingCoordinate.Show;
  end;

  if MapTouch.CurrentTool = mtAnchorTool then
    MapTouch.CurrentTool := mtSelectObject;

end;

procedure TfrmTacticalDisplay.ToolBtnPanClick(Sender: TObject);
begin
  if ToolBtnPan.Down then
  begin
    MapTouch.CurrentTool := miCenterTool;
    StatusBar1.Panels[0].Text := TToolButton(Sender).Hint;

    toolbtnZoom.Down := False;
  end
  else
  begin
    MapTouch.CurrentTool := mtSelectObject;
    StatusBar1.Panels[0].Text := 'Select';
  end;

end;

procedure TfrmTacticalDisplay.ToolBtnRangeRingsOnHookClick(Sender: TObject);
begin
  FRangeRingOnHook := ToolBtnRangeRingsOnHook.Down;

  if FRangeRingOnHook then
    FRingHookedTrack := clientManager.SelectedTrack;

  if FRangeRingOnHook and (FRingHookedTrack <> nil) then
  begin
    clientManager.RangeRing.mx := TT3Track(FRingHookedTrack).getPositionX;
    clientManager.RangeRing.my := TT3Track(FRingHookedTrack).getPositionY;
  end;

  StatusBar1.Panels[0].Text := TToolButton(Sender).Hint;

end;

procedure TfrmTacticalDisplay.ToolBtnRemoveFromTrackTableClick(Sender: TObject);
begin
  if Assigned(clientManager.SelectedTrack) then
     FTrackTableManager.RemoveTrack(clientManager.SelectedTrack);
end;

procedure TfrmTacticalDisplay.ToolBtnTrackHistoryClick(Sender: TObject);
var
  pf : TT3PlatformInstance;
begin
  if clientManager.SelectedTrack <> nil then
  begin
    if clientManager.SelectedTrack is TT3PointTrack then
    begin

        TT3PointTrack(clientManager.SelectedTrack).ShowTrails := not TT3PointTrack(clientManager.SelectedTrack)
          .ShowTrails;
        ToolBtnTrackHistory.Down := TT3PointTrack(clientManager.SelectedTrack).ShowTrails;

      end;
    end
end;

procedure TfrmTacticalDisplay.toolbtnZoomClick(Sender: TObject);
begin
  if toolbtnZoom.Down then
  begin
    MapTouch.CurrentTool := miZoomInTool;
    StatusBar1.Panels[0].Text := TToolButton(Sender).Hint;

    ToolBtnPan.Down := False;
  end
  else
  begin
    MapTouch.CurrentTool := mtSelectObject;
    StatusBar1.Panels[0].Text := 'Select';
  end;
end;

procedure TfrmTacticalDisplay.UpdateCenter(Sender: TObject);
var
  range: double;
begin
  if FHookOnPlatform and Assigned(FCenterHookedTrack) then
  begin
    range := CalcRange(FLastMapCenterX,FLastMapCenterY,
        TT3Track(FCenterHookedTrack).PosX,
        TT3Track(FCenterHookedTrack).PosY);

    range := range * C_NauticalMile_To_Metre;

    // default update center ketika dR = 100 meter
    if range > 10 then begin
      FLastMapCenterX := TT3Track(FCenterHookedTrack).PosX;
      FLastMapCenterY := TT3Track(FCenterHookedTrack).PosY;

      clientManager.SimMap.SetMapCenter(FLastMapCenterX,FLastMapCenterY);
    end;
  end;

  if Assigned(FRingHookedTrack)then
  begin
    if FRangeRingOnHook then
    begin
      clientManager.RangeRing.mX := TT3Track(FRingHookedTrack).getPositionX;
      clientManager.RangeRing.mY := TT3Track(FRingHookedTrack).getPositionY;
    end
  end;
 end;

procedure TfrmTacticalDisplay.UpdateFormData(Sender: TObject);
begin
  FCtrlPanelManager.PanelUpdate;
  FTrackTableManager.UpdateForm;

  if not Assigned(frmLogDisplay) then
    frmLogDisplay := TfrmLogDisplay.Create(Self);

  frmLogDisplay.updateForm;
end;

procedure TfrmTacticalDisplay.UpdateGameTime(Sender: TObject);
begin
  DisplayGameTime(clientManager.GameSpeed, clientManager.GameTIME);

end;

end.
