 unit uSimObjects;


interface

uses
  Classes,  uBaseSimObjects, uDataTypes,
  Graphics;

type

  { abstract simobject class }
  TSimObject = class abstract (TBaseSimObject)
  protected

    FPosition: t3DPoint;

    procedure SetFreeMe(const Value: boolean);override;

  public
    constructor Create;
    destructor Destroy; override;

    function getPositionX: double;
    function getPositionY: double;
    function getPositionZ: double;

    procedure setPositionX(const v: double); virtual;
    procedure setPositionY(const v: double); virtual;
    procedure setPositionZ(const v: double); virtual;

    procedure Move(const aDeltaMs: double); virtual;
    procedure FreeChilds; virtual;     //divirtual disimobject supaya bisa dipanggil dari container;

    property PosX: double read getPositionX write setPositionX;
    property PosY: double read getPositionY write setPositionY;
    property PosZ: double read getPositionZ write setPositionZ;
  end;

implementation

uses
  SysUtils, Windows;

{ TBaseSimClass }
constructor TSimObject.Create;
begin
  inherited;
  FPosition.X := 0.0;
  FPosition.Y := 0.0;
  FPosition.Z := 0.0;
end;

destructor TSimObject.Destroy;
begin

  inherited;
end;

function TSimObject.getPositionX: double;
begin
  result := FPosition.X;
end;

function TSimObject.getPositionY: double;
begin
  result := FPosition.Y;
end;

function TSimObject.getPositionZ: double;
begin
  result := FPosition.Z;
end;

procedure TSimObject.setPositionX(const v: double);
begin
  FPosition.X := v;
end;

procedure TSimObject.setPositionY(const v: double);
begin
  FPosition.Y := v;
end;
procedure TSimObject.setPositionZ(const v: double);
begin
  FPosition.Z := v;
end;

procedure TSimObject.SetFreeMe(const Value: boolean);
begin
  inherited;

end;

procedure TSimObject.FreeChilds;
begin

end;

procedure TSimObject.Move(const aDeltaMs: double);
begin
   // self update.
  //calc movement etc
end;

end.

