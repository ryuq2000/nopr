unit uT3EventManager;

interface

uses tttData;

type

  TT3EventManager = class abstract

  public

    { sensor event }
    procedure OnSensorRemoved(Sender : TObject); virtual;
    procedure OnSensorOperationalStatus(Sender : TObject;Mode : TSensorOperationalStatus);  virtual;
//    procedure OnRadarControlMode(Sender : TObject;Mode : TRadarControlMode);                virtual;
//    procedure OnSonarControlMode(Sender : TObject;Mode : TSonarControlMode);                virtual;
//    procedure onIFFTransponderStatus(Sender : TObject;Mode : TSensorOperationalStatus);   virtual;
//    procedure OnIFFInterrogatorStatus(Sender : TObject;Mode : TSensorOperationalStatus);  virtual;
//    procedure OnSonobuoyPassiveDetect(Sender, detected: TObject; aValue : Byte);   virtual;

    procedure OnPlatformOrderedControlChange(OrderID : Integer; OrderType : Integer;
                                             PlatfomID : Integer; OrderParam : Single); virtual;

    { vehicle event }
    procedure OnGuidanceChange(Sender : TObject; guidance : TVehicleGuidanceType);virtual;abstract;
  end;

implementation


{ TT3EventManager }

procedure TT3EventManager.OnPlatformOrderedControlChange(OrderID, OrderType,
  PlatfomID: Integer; OrderParam: Single);
begin

end;

procedure TT3EventManager.OnSensorOperationalStatus(Sender: TObject;
  Mode: TSensorOperationalStatus);
begin

end;

procedure TT3EventManager.OnSensorRemoved(Sender: TObject);
begin

end;

end.
