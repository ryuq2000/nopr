unit uT3ServerEventManager;

interface

uses uT3EventManager, tttData;

type
  TT3ServerEventManager = class(TT3EventManager)
  public

    { sensor event }
    procedure OnSensorRemoved(Sender : TObject); override;
    procedure OnSensorOperationalStatus(Sender : TObject;Mode : TSensorOperationalStatus);  override;
  end;

implementation

uses uGlobalVar, uT3MountedSensor, uT3ServerManager, uT3PlatformInstance;

{ TT3ServerEventManager }

procedure TT3ServerEventManager.OnSensorOperationalStatus(Sender: TObject;
  Mode: TSensorOperationalStatus);
begin
  inherited;

  if (Mode = sopOff) or (Mode = sopDamage) or
     (Mode = sopTooDeep) or
     (TT3MountedSensor(Sender).EmconOperationalStatus = EmconOn) then
  begin
    serverManager.TrackManager.RemoveTracks(TT3MountedSensor(Sender));
  end;
end;

procedure TT3ServerEventManager.OnSensorRemoved(Sender: TObject);
begin
  inherited;

end;

end.
