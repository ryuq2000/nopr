unit uT3ClientManager;

interface

uses uT3SimManager, uSteppers, MapXLib_TLB, uLibSettingTTT, uThreadTimer,
  uMapXHandler, Graphics, SysUtils, Classes, uT3GroupList, uDBClassDefinition,
  uNetBaseSocket, uT3ObjectVisualFactory, uSimVisuals, uT3SimContainer,
  uT3Track, tttData, uNetClientSender, uSimObjects, ut3ClientEventManager,
  uObjectVisuals, uGameData_TTT, uGlobalVar, uT3ObjectFactory,
  uT3PlatformInstance, uT3MountedSensor, uT3Vehicle, uT3BlindZone,
  uT3PointTrack, uT3BearingTrack, uT3DrawContainer, uT3ObjectVisualManager,
  uFilter, uTMapTouch;

type

  TT3ClientManager = class(TT3SimManager)

  private
    { Draw Thread only on client, for drawing symbol }
    FDrawThread        : TMSTImer;
    { update thread }
    FUpdateThread      : TMSTImer;
    { delay updating form data }
    FUpdateDelay       : TDelayer;
    { delay centering map on hooked platform }
    FUpdateCenterDelay : TDelayer;
    { delay connect to server }
    FConnectDelay      : TDelayer;
    { visualization filter }
    FFilter            : TFIlter;
    { sim map }
    FSimMap            : TSimMap;

    { cubicle assignemt }
    FCubicleList       : T3GroupList;

    { cubicle information }
    FMyIPAddress       : String;
    FMyConsoleName     : String;
    FMyCubicleName     : String;
    FMyGroupID         : integer;
    FMyForceDesignation: integer;

    FTimeFlag          : integer;
    FTimeReq           : Longword;

    { visual object factory }
    FObjectVisFact     : TT3ObjectVisualFactory;
    FObjectVisualManager : TT3ObjectVisualManager;

    { common visual ref. }
//    FTargetSymbol      : TDrawElement;
//    FFlashPointNGS     : TDrawElement;

    { track container, each cubicle has different track,  }
    FCubicleTracks     : TT3SimContainer;

    { controlled track }
    FControlledTrack   : TT3Track;
    FOnUpdateForm      : TNotifyEvent;
    FOnUpdateTime      : TNotifyEvent;
    FOnUpdateCenter    : TNotifyEvent;
    FOnMapViewChange   : TNotifyEvent;
    FSelectedTrack     : TT3Track;

    { draw container }
    FDrawContainer     : TT3DrawContainer;

    procedure FDrawThread_OnRunning(const dt: double);
    procedure OnMapChange(sender: TObject);

    procedure ApplyCubicleSetting;
    procedure LoadCubicleTracks;

    { create track for own platform }
    function CreateCubicleTrack(pf : TT3PlatformInstance) : TT3Track;overload;
    { create track from sync server }
    function  CreateCubicleTrack(pfIndex : integer; trackType : TTrackType) : TT3Track; overload;

    procedure OnConnectDelay(sender: TObject);
    procedure OnUpdateDelay(sender: TObject);
    procedure OnUpdateCenterDelay(sender: TObject);

    procedure FUpdateThread_OnTerminate(sender: TObject);
    procedure FUpdateThread_OnRunning(const dt: double);
    procedure FUpdateThread_OnPaused(const dt: double);

    procedure SetControlledTrack(const Value: TT3Track);
    function getRangeRing: TRangeRingsVisual;

    procedure SyncTrack(track : TT3Track; r : TRecTrack);

//    procedure CmdPlatformMovement(r : TRecUDP_PlatformMovement);
    procedure CmdTrackSync(r : TRecTrack);
//    procedure CmdGameTime(r : TRecUDP_GameTime);

    procedure SetSelectedTrack(const Value: TT3Track);

  protected
    procedure FGameThread_OnRunning(const dt: double); override;

    procedure CmdGameControlInfo(rec : TRecUDP_GameCtrl_info);virtual;
    function  CmdLaunchRuntimePlatform(var rec : TRecCmd_LaunchRP ): TT3PlatformInstance; virtual;

    { all packet received here }
    procedure PacketReceive(apRec: PAnsiChar; aSize: word); override;


  public
    constructor Create(Map: TMapXTouch); override;
    destructor Destroy; override;

    { manipulating tracks procedure }
    function FindNearestTrack(const px, py: integer; const maxDist: integer)
      : TSimObject;
    { default point track }
    function FindTrack(byObInsIndex : integer;category : TTrackCategory = tcRealTrack) : TT3Track;

    procedure UnSelectAllTracks;

    procedure LoadScenarioID(const vSet: TGameDataSetting); override;

    procedure netOnConnected(sender: TObject);
    procedure InitNetwork; override;
    procedure StopNetwork; override;

    procedure GameStart; override;
    procedure GamePause; override;

    procedure DrawAll(cnv: TCanvas);
    procedure SwitchMap(const aActiveMap: Integer);

    property ConsoleName: String read FMyConsoleName;
    property CubicleName: String read FMyCubicleName;

    property SimMap: TSimMap read FSimMap;

    property ObjectVisualManager : TT3ObjectVisualManager read FObjectVisualManager;

    property ControlledTrack: TT3Track read FControlledTrack write
      SetControlledTrack;
    property SelectedTrack: TT3Track read FSelectedTrack write SetSelectedTrack;

    property CubicleTracks: TT3SimContainer read FCubicleTracks;

    property OnUpdateForm: TNotifyEvent read FOnUpdateForm write FOnUpdateForm;
    property OnUpdateTime: TNotifyEvent read FOnUpdateTime write FOnUpdateTime;
    property OnUpdateCenter
      : TNotifyEvent read FOnUpdateCenter write FOnUpdateCenter;
    property OnMapViewChange
      : TNotifyEvent read FOnMapViewChange write FOnMapViewChange;

    property RangeRing: TRangeRingsVisual read getRangeRing;

    property MyGroupID : integer read FMyGroupID;
    property MyForceDesignation : integer read FMyForceDesignation;
  end;

var
  clientManager: TT3ClientManager;

implementation

uses
  uNetHandle_Client, uCompassVisual,
  uT3MountedRadar, uT3MountedVisual,
  {uDBCubicles,} uSensorInfo,
  Generics.Collections,
  Generics.Defaults;


type
  { simple class for item sorting }
  TSimpleClass = class
    SensorType : Byte;
    SensorID   : integer;
    SensorOwner : integer;
    SensorInfo : TSensorTrackInfo;
  end;
  TFieldCompare = (fcType, fcID, fcOwner);

{ theses 2 function use by compare list procedure, for sorting simpleclass by
  sensortype, sensorID, sensorowner;
}

function CompareField(const Left, Right: TSimpleClass; Field: TFieldCompare): Integer;
begin
  case Field of
  fcType:
    Result := Left.SensorType -  Right.SensorType;
  fcID:
    Result := Left.SensorID - Right.SensorID;
  fcOwner :
    Result := Left.SensorOwner - Right.SensorOwner;
  end;
end;

function Comparison(const Left, Right: TSimpleClass): Integer;
var
  i: Integer;
  FSortField : array [0..2] of TFieldCompare;
begin
  FSortField[0] := fcType;
  FSortField[1] := fcID ;
  FSortField[2] := fcOwner;

  for i := low(FSortField) to high(FSortField) do begin
    Result := CompareField(Left, Right, FSortField[i]);
    if Result<>0 then begin
      exit;
    end;
  end;
end;

{ TT3ClientManager }

procedure TT3ClientManager.ApplyCubicleSetting;
var
  i: integer;
  ss: TStrings;
  found: Boolean;
  ip, host: string;
begin
  FMyIPAddress := '';

  FCubicleList.LoadFromFile(vGameDataSetting.GroupSetting);

  ss := TStringList.Create;
  if GetHostandIPList(host, ss) then
  begin
    i := 0;
    found := false;
    while not found and (i < ss.Count) do
    begin
      ip := ss[i];
      found := FCubicleList.IsGroupMember(ip);

      inc(i);
    end;

    if found then
    begin
      FMyIPAddress := ip;
      if FCubicleList.IsController(FMyIPAddress) then
        machineRole := crController
      else
        machineRole := crCubicle;

      FMyConsoleName := FCubicleList.GetConsoleName(FMyIPAddress);
      FMyCubicleName := FCubicleList.GetCubicleName(FMyIPAddress);

    end;
  end;

  ss.Free;
end;

//procedure TT3ClientManager.ApplyVisualTracks;
//var
//  i : integer;
//  pf : TT3PlatformInstance;
//  sensor: TT3MountedSensor;
//  bz :  TT3BlindZone;
//  visual : TDrawElement;
//begin
//  visual := FObjectVisFact.createVisual('Taktis', track);
//  visual.Visible := True;
//  FDrawContainer.AddObject(visual);
//
//  { this track is either own by cubicle or controller which can be controlled }
//  if (track is TT3PointTrack) and TT3PointTrack(track).CanControl then
//  begin
//    pf := FindT3PlatformByID(track.ObjectInstanceIndex);
//    if ASsigned(pf) and (pf is TT3Vehicle) then
//    begin
//
////      {create platform sensor asset visual }
////      for sensor in TT3Vehicle(pf).MountedSensors do
////      begin
////
////        visual := FObjectVisFact.createRangeVisual(sensor);
////        FDrawContainer.AddObject(visual);
////
////        {blind zone visual}
////        for bz in sensor.BlindZones do
////        begin
////          visual := FObjectVisFact.createBlindZoneVisual(bz);
////          FDrawContainer.AddObject(visual);
////        end;
////
////      end;
//    end;
//  end;
//  if (track is TT3BearingTrack) then
//  begin
//
//  end;
//end;

procedure TT3ClientManager.CmdGameControlInfo(rec: TRecUDP_GameCtrl_info);
var
  dtNow : Longword;
  dt    : integer;
  gS    : TGamePlayState;
begin

  if rec.Flag = FTimeFlag then
  begin
    gS := TGamePlayState(rec.GameState);
    case gS of
      gsStop:
        begin // server is stoped
          FMainVTime.SetMilliSecond(rec.GameTimeMS);

        end;
      gsPlaying:
        begin
          dt := FMainVTime.GetMillisecond - FTimeReq;
          if rec.GameSpeed > 1.0 then
            dtNow := rec.GameTimeMS + Round(0.5 * rec.GameSpeed * dt)
          else
            dtNow := rec.GameTimeMS + LongWord((dt + 1) div 2);

          FMainVTime.SetMilliSecond(dtNow);
        end;
    end;
    FTimeFlag := 0;

    if gS <> GameState then
    begin
      case gS of
        gsStop:
          GamePause;
        gsPlaying:
        begin
          GameStart;
          //isFirstStart := false;
        end;
        // gsTerminated  : GameTerminate;
      end;
    end;

    if Abs(rec.GameSpeed - GameSpeed) > 0.01 then
      GameSpeed := rec.GameSpeed;

    if Assigned(FOnUpdateTime) then
      FOnUpdateTime(self);
  end;
end;

function TT3ClientManager.CmdLaunchRuntimePlatform(
  var rec: TRecCmd_LaunchRP): TT3PlatformInstance;
var
  pf : TT3PlatformInstance;
begin
  pf := inherited;

  if Assigned(pf) then
  begin
    if machineRole = crController then
      CreateCubicleTrack(pf)
    else
    if machineRole = crCubicle then
    begin
      //sesuaikan dengan groupnya, jika sama maka create track selain itu skip
      if rec.RPGroupID = MyGroupID then
      begin
        CreateCubicleTrack(pf);
      end;
    end;
  end;

  result := pf;
end;

procedure TT3ClientManager.CmdTrackSync(r: TRecTrack);
var
  track : TT3Track;
  i : integer;
begin
  if machineRole = crController then
  begin

    if r.TrackGroup = -1 then
      Exit;

    {
      default controller group id
      controller has it own tracks that cubicle cant see
    }
    if r.TrackGroup >= 0 then
    begin
      if r.TrackGroup = 0 then
      begin
        track := FCubicleTracks.FindObjectByUid(r.TrackID) as TT3track;

        { if track not exist, then create }
        if not Assigned(track) then
        begin
          track := CreateCubicleTrack(r.TrackPlatformID,
                TTrackType(r.TrackType));
          track.UniqueID := r.TrackID;

        end;
        SyncTrack(track,r);
      end
      else
      begin
        {sync uid and track number }
        if r.TrackGroupOwner then
        begin
          track := FindTrack(r.TrackPlatformID);
          { sync track uid, find track by platform id }
          if Assigned(track) then
          begin
            track.UniqueID    := r.TrackID;
            track.TrackNumber := r.TrackNum;
          end;
        end;
      end;

    end;
  end else
  if machineRole = crCubicle then
  begin
    { tracks is mine }
    if r.TrackGroup = FMyGroupID then
    begin

      {sync own uid and track number }
      if r.TrackGroupOwner then
      begin
        track := FindTrack(r.TrackPlatformID);
        { sync track uid, find track by platform id }
        if Assigned(track) then
        begin
          track.UniqueID    := r.TrackID;
          track.TrackNumber := r.TrackNum;
        end;
      end
      else
      { else is track by sensor / datalink }
      begin
        { update track i hav }
        track := FCubicleTracks.FindObjectByUid(r.TrackID) as TT3track;

        { if track not exist, then create }
        if not Assigned(track) then
        begin
          track := CreateCubicleTrack(r.TrackPlatformID,
              TTrackType(r.TrackType));
          track.UniqueID := r.TrackID;
        end;

        SyncTrack(track,r);
      end;

    end;
  end;
end;

constructor TT3ClientManager.Create(Map: TMapXTouch);
var
  k : integer;
begin
  inherited;

  FDrawContainer      := TT3DrawContainer.Create;

  FSimMap := TSimMap.Create(Map);
  FSimMap.OnDrawToCanvas := DrawAll;
  FSimMap.OnMapChange := OnMapChange;

  FNetCmdSender        := TNetClientCmdSender.Create;
  FEventManager        := TT3ClientEventManager.Create;

  FFilter              := TFilter.Create;
  FObjectVisFact       := TT3ObjectVisualFactory.Create(Map);
  FObjectVisualManager := TT3ObjectVisualManager.Create(FConverter);
  FCubicleTracks       := TT3SimContainer.Create;

  FMyGroupID := 0; // default for controller
  if machineRole = crCubicle then
  begin
    k := length(vCubicalAssignSetting.GroupIDs);
    if k > 0 then
      FMyGroupID  := vCubicalAssignSetting.GroupIDs[0];
  end;

  FDrawThread := TMSTImer.Create;
  FDrawThread.Interval := 100;
  FDrawThread.OnRunning := FDrawThread_OnRunning;
  FDrawThread.Start;

  FUpdateThread := TMSTImer.Create;
  FUpdateThread.Interval := 50;

  FCubicleList := T3GroupList.Create;
  ApplyCubicleSetting;

  FControlledTrack := nil;

  FConnectDelay := TDelayer.Create;
  with FConnectDelay do
  begin
    DInterval := 8.0;
    OnTime := OnConnectDelay;
    Enabled := false;
  end;

  FUpdateCenterDelay := TDelayer.Create;
  with FUpdateCenterDelay do
  begin
    DInterval := 0.5;
    OnTime := OnUpdateCenterDelay;
    Enabled := false;
  end;

  FUpdateDelay := TDelayer.Create;
  with FUpdateDelay do
  begin
    DInterval := 0.5;
    OnTime := OnUpdateDelay;
    Enabled := True;
  end;

  FMyGroupID := 0;
  FMyForceDesignation := -1;
end;

function TT3ClientManager.CreateCubicleTrack(pfIndex: integer;trackType : TTrackType): TT3Track;
var
  track: TT3Track;
begin
  result := nil;

  track :=  ObjectFactory.createTrack(trackType, pfIndex);

  FCubicleTracks.AddObject(track);

  { track event }
  track.OnSelected := TT3ClientEventManager(FEventManager).OnTrackSelected;

  result := track;
end;

function TT3ClientManager.CreateCubicleTrack(pf: TT3PlatformInstance) : TT3Track;
var
  track: TT3Track;
begin

  result := nil;

  track := ObjectFactory.createTrack(trPoint, pf);
  if track is TT3PointTrack then
    TT3PointTrack(track).isAlwaysTracking := True;

  if track is TT3PointTrack then
  begin
    TT3PointTrack(track).CanControl := True;
    { point track event }
    TT3PointTrack(track).OnControlled := TT3ClientEventManager(FEventManager).OnTrackControlled;
  end;

  FCubicleTracks.AddObject(track);

  result := track;
end;

destructor TT3ClientManager.Destroy;
begin

  FObjectVisFact.Free;
  FObjectVisualManager.Free;
  FFilter.Free;

  FDrawThread.Terminate;
  FDrawThread.OnRunning := nil;
  FDrawThread.Free;

  FUpdateThread.Terminate;
  FUpdateThread.OnRunning := nil;
  FUpdateThread.Free;

  FUpdateDelay.Free;
  FUpdateCenterDelay.Free;
  FConnectDelay.Free;

  FCubicleTracks.ClearAndFreeObject;
  FCubicleTracks.Free;

  FCubicleList.Free;
  FDrawContainer.Free;

  FSimMap.Free;

  inherited;
end;

procedure TT3ClientManager.DrawAll(cnv: TCanvas);
var
  track : TT3Track;
  cnt, I : Integer;
begin
  inherited;

  { draw client visual prop : rangering, compass, etc}
  FObjectVisualManager.Draw(cnv);

  { draw tracks }
  cnt := FCubicleTracks.ItemCount;
  for I := 0 to cnt - 1 do
  begin
    track := FCubicleTracks.getObject(I) as TT3Track;
    FObjectVisualManager.DrawTrackVisual(track,cnv);
  end;

end;

procedure TT3ClientManager.FDrawThread_OnRunning(const dt: double);
begin
  inherited;

  FSimMap.DrawMap;  // -> it will run drawall

end;

procedure TT3ClientManager.FGameThread_OnRunning(const dt: double);
var
  i, gS: integer;
begin
  inherited;
  if GameSPEED < 1.0 then
  begin
    // slow down baby...
  end
  else
  begin

    gS := Round(GameSPEED);
    for i := 0 to gS - 1 do
    begin
      FCubicleTracks.Move(dt);
    end;
  end;
end;

function TT3ClientManager.FindNearestTrack(const px, py: integer;
  const maxDist: integer): TSimObject;
var
  i: integer;
  Pi: TSimObject;
  rr: integer;
  m, r: double;
  ptx, pty: integer;
  found: Boolean;
begin

  m := 1000000.0;
  Result := nil;
  found := false;

  // for cubicle or controller track
  for i := 0 to FCubicleTracks.ItemCount - 1 do
  begin
    Pi := TSimObject(FCubicleTracks.getObject(i));

    if Pi.FreeMe then
      continue;

    Converter.ConvertToScreen(Pi.getPositionX, Pi.getPositionY, ptx, pty);
    try
      rr := sqr(ptx - px) + sqr(pty - py);
      if rr = 0 then
        r := 10000
      else
        r := Sqrt(Abs(rr));
    except
      r := 10000;
    end;

    if (r < m) and (r < maxDist) then
    begin
      m := r;
      Result := Pi;
      found := True;
      break;
    end;
  end;

  if found then
    Exit;
end;

function TT3ClientManager.FindTrack(byObInsIndex: integer;category : TTrackCategory = tcRealTrack): TT3Track;
var
  i : integer;
  f : Boolean;
  obj : TObject;
begin
  Result := nil;

  i := 0;
  f := False;
  obj := nil;

//  while not f and (i < FCubicleTracks.ItemCount - 1) do
//  begin
//    obj := FCubicleTracks.getObject(i);
//
//    f := TT3Track(obj).ObjectInstanceIndex = byObInsIndex;
//    if f then
//      f := (TT3Track(obj) is TT3PointTrack) and (TT3Track(obj).TrackCategory = category ) ;
//
//    inc(i)
//  end;
//
//  if f then
//    Result := TT3Track(obj);
end;

procedure TT3ClientManager.FUpdateThread_OnPaused(const dt: double);
begin
  FConnectDelay.Run(dt);

  FUpdateDelay.Run(dt);

  if Assigned(FOnUpdateTime) then
    FOnUpdateTime(self);

end;

procedure TT3ClientManager.FUpdateThread_OnRunning(const dt: double);
var
  t: TDateTime;
  hh, mm, ss, zz: word;
begin // 250 ms

  t := FMainVTime.GetTime;
  DecodeTime(t, hh, mm, ss, zz);

  TNetHandler_Client(nethandle).getPacket;
  FUpdateDelay.Run(dt);
  FUpdateCenterDelay.Run(dt);
  FConnectDelay.Run(dt);
  // FUpdateDataLink.Run(dt);
  // FUpdateForMessagehandling.Run(dt);
end;

procedure TT3ClientManager.FUpdateThread_OnTerminate(sender: TObject);
begin

end;

procedure TT3ClientManager.GamePause;
begin
  inherited;

  FUpdateThread.OnRunning := FUpdateThread_OnPaused;
end;

procedure TT3ClientManager.GameStart;
begin
  inherited;

  FUpdateThread.OnRunning := FUpdateThread_OnRunning;

  if FUpdateThread.Suspended then
    FUpdateThread.Start;

  FUpdateCenterDelay.Enabled := True;

end;

function TT3ClientManager.getRangeRing: TRangeRingsVisual;
begin
//  Result := TRangeRingsVisual(FRangeRing);
end;

procedure TT3ClientManager.InitNetwork;
begin
  inherited;

//  VNetVoipCtrlServer.StartNetworking;
  FConnectDelay.Enabled := True;
end;

procedure TT3ClientManager.LoadCubicleTracks;
var
  pf: TT3PlatformInstance;
  i,k: integer;
  myGroups : TDBCubicleGroupMemberList;
  grp : TDBCubicle_Group;
  assignment : TDBCubicle_Group_Assignment;
  track : TT3Track;
begin
  case machineRole of
    { cubicle only create cubicle tracks }
    crCubicle:
      begin
        myGroups := nil;
        grp := nil;

        k := length(vCubicalAssignSetting.GroupIDs);
        if k > 0 then
        begin
          FMyGroupID  := vCubicalAssignSetting.GroupIDs[0];

          for grp in FScenario.DBCubicleGroupList do
          begin

            if grp.Group_Index = FMyGroupID  then
            begin

              FScenario.DBCubicleAssignmentDict.TryGetValue(FMyGroupID,myGroups);

              { set client force designation }
              FMyForceDesignation := grp.Force_Designation;
              Break;
            end;
          end;

          if Assigned(myGroups) then
            { iterate all platform group }
            for assignment in myGroups do
            begin
              pf                    := FindT3PlatformByID(assignment.Platform_Instance_Index);
              track                 := CreateCubicleTrack(pf);
              track.TrackIdentifier := piFriend;
            end;
        end;
      end;

    { controller will create all platform tracks }
    crController, crWasdal:
      begin

        for i := 0 to FSimPlatforms.ItemCount - 1 do
        begin
          pf := TT3PlatformInstance(FSimPlatforms.getObject(i));
          CreateCubicleTrack(pf);
        end;
      end;
  end;

  if FCubicleTracks.ItemCount > 0 then
  begin
    FControlledTrack := TT3Track(FCubicleTracks.getObject(0));

    if (FControlledTrack is TT3PointTrack) and (TT3PointTrack(FControlledTrack).CanControl) then
      TT3PointTrack(FControlledTrack).isControlled := True;

    FControlledTrack.isSelected := True;
  end;

end;

procedure TT3ClientManager.LoadScenarioID(const vSet: TGameDataSetting);
var
  S, fGeo: String;
begin
  inherited;

  { load tracks for cubicle }
  LoadCubicleTracks;

  S := UpperCase(Trim(FScenario.DBGameAreaDefinition.Detail_Map));
  if S = 'ENC' then
    fGeo := vMapSetting.MapENCPath + FScenario.MapGeosetName
  else
    fGeo := vMapSetting.MapPath + FScenario.MapGeosetName;

  {bypass dulu}
  fGeo := vMapSetting.MapPath + 'Modul Presentation\Modul Presentation.gst';

  if FileExists(fGeo) then
  begin
    FSimMap.LoadMap(fGeo);

    if Assigned(FControlledTrack) then
      FSimMap.FMap.ZoomTo(50, FControlledTrack.getPositionX,
        FControlledTrack.getPositionY)
    else
      FSimMap.FMap.ZoomTo(50, FSimMap.FMap.CenterX, FSimMap.FMap.CenterY);
  end;

end;

procedure TT3ClientManager.netOnConnected(sender: TObject);
var
  rec: TRecTCP_Request;
begin
  rec.reqID    := REQ_CUBICLE_ASSIGNMENT;
  rec.reqFlag  := Random($FFFF);
  FNetCmdSender.CmdTCPRequest(rec);

  // ini karena dibales lewat udp, maka perlu 'paket id' tambahan
  // untuk memastikan paket yg diproses adalah paket hasil request sendiri.
  // 'sync time'
  rec.reqID    := REQ_SYNCH_GAMECTRL_INFO;
  FTimeFlag    := Random($FFFF);
  rec.reqParam := 0;
  rec.reqFlag  := FTimeFlag;
  FTimeReq     := FMainVTime.GetMillisecond;

  FNetCmdSender.CmdTCPRequest(rec);

  rec.reqID    := REQ_MISSED_PACKET;
  rec.reqFlag  := Random($FFFF);
  FNetCmdSender.CmdTCPRequest(rec);

end;

procedure TT3ClientManager.OnConnectDelay(sender: TObject);
begin
  TNetHandler_Client(nethandle).TryReconnect; // tcp only.
end;

procedure TT3ClientManager.OnMapChange(sender: TObject);
begin

//  TCompassVisual(FCompass).Width := FMap.Width;
//  TCompassVisual(FCompass).Height := FMap.Height;

end;

procedure TT3ClientManager.OnUpdateCenterDelay(sender: TObject);
begin
  if Assigned(FOnUpdateCenter) then
    FOnUpdateCenter(self);
end;

procedure TT3ClientManager.OnUpdateDelay(sender: TObject);
begin
  if Assigned(FOnUpdateTime) then
    FOnUpdateTime(self);

  if Assigned(FOnUpdateForm) then
    FOnUpdateForm(self);
end;

procedure TT3ClientManager.PacketReceive(apRec: PAnsiChar; aSize: word);
var
  { rec for catch pid / packet id and session id only }
  rec: ^TRecSynch_Vehicle;
  rCmdGCinfo   : ^TRecUDP_GameCtrl_info;
  rCmdLaunchRP : ^TRecCmd_LaunchRP;

  rTrackSync  : ^TRecTrack;
  txt : string;
  i : integer;
begin
  inherited;

  rec := @apRec^;

  if rec = nil then
    Exit;

  if rec^.SessionID <> SessionID then
    Exit;

  case rec^.pid.recID of

    CPID_CMD_LAUNCH_RUNTIME_PLATFORM :
    begin
      rCmdLaunchRP := @apRec^;
      CmdLaunchRuntimePlatform(rCmdLaunchRP^);
    end;

    CPID_UDP_GAMECTRL_INFO :
    begin
      rCmdGCinfo := @apRec^;
      CmdGameControlInfo(rCmdGCinfo^);
    end;

    CPID_UDP_TRACK_SYNC :
    begin
      rTrackSync := @apRec^;
      CmdTrackSync(rTrackSync^);
    end;
  end;
end;

procedure TT3ClientManager.SetControlledTrack(const Value: TT3Track);
begin
  FControlledTrack := Value;
end;

procedure TT3ClientManager.SetSelectedTrack(const Value: TT3Track);
begin
  FSelectedTrack := Value;
end;

procedure TT3ClientManager.StopNetwork;
begin
  inherited;

  FConnectDelay.Enabled := false;


end;

procedure TT3ClientManager.SwitchMap(const aActiveMap: Integer);
var
  s, geosetStr, zoomStr : string;
  lastZoom, lastCX, lastCY : Double;
  gDefRec  : TDBGame_Environment_Definition;
  gDefArea : string;
  gDefIndx1, gDefIndx2 : Integer;
  I : Integer;
begin
  s := UpperCase(Trim(Scenario.DBGameAreaDefinition.Detail_Map));

  case aActiveMap of
    1:
    begin
//      gDefIndx1 := Scenario.DBGameEnvironment.Game_Area_Index;
//
//      for I := 0 to FGDefList.Count - 1 do
//      begin
//        gDefRec := FGDefList.Items[I];
//        gDefIndx2 := gDefRec.FGameArea.Game_Area_Index;
//
//        if gDefIndx1 = gDefIndx2 then
//        begin
//          gDefArea := gDefRec.FGameArea.Game_Area_Identifier;
//
//          Break;
//        end;
//      end;
//
//      geosetStr := vMapSetting.MapGSTGame + '\' + gDefArea + '\' + gDefArea + '.gst';
    end;
    2:
    begin
//      gDefIndx1 := GameEnvironment.FData.Game_Area_Index_2;
//      for I := 0 to FGDefList.Count - 1 do
//      begin
//        gDefRec := FGDefList.Items[I];
//        gDefIndx2 := gDefRec.FGameArea.Game_Area_Index;
//
//        if gDefIndx1 = gDefIndx2 then
//        begin
//          gDefArea := gDefRec.FGameArea.Game_Area_Identifier;
//
//          Break;
//        end;
//      end;
//
//      geosetStr := vMapSetting.MapGSTGame + '\' + gDefArea + '\' + gDefArea + '.gst';
    end;
    3:
    begin
//      gDefIndx1 := GameEnvironment.FData.Game_Area_Index_3;
//      for I := 0 to FGDefList.Count - 1 do
//      begin
//        gDefRec := FGDefList.Items[I];
//        gDefIndx2 := gDefRec.FGameArea.Game_Area_Index;
//
//        if gDefIndx1 = gDefIndx2 then
//        begin
//          gDefArea := gDefRec.FGameArea.Game_Area_Identifier;
//
//          Break;
//        end;
//      end;
//
//      geosetStr := vMapSetting.MapGSTGame + '\' + gDefArea + '\' + gDefArea + '.gst';
    end;
  end;

  if FileExists(geosetStr) then
  begin
    lastZoom := SimMap.FMap.Zoom;
    lastCX := SimMap.FMap.CenterX;
    lastCY := SimMap.FMap.CenterY;

    SimMap.LoadMap(geosetStr);

    SimMap.FMap.ZoomTo(lastZoom, lastCX, lastCY);
  end;
end;

procedure TT3ClientManager.SyncTrack(track: TT3Track; r: TRecTrack);
var
  lbl : String;
  I,pid : integer;
  sensorPair : TPair<TT3MountedSensor,TSensorTrackInfo>;
  sensor : TT3MountedSensor;
  pf : TT3PlatformInstance;
  found, alwaysTrack : Boolean;
  sensorInfo : TSensorTrackInfo;
  toRemove : TList;
begin

  if Assigned(track) then
  begin
    MonitorEnter(track);
    try
    with track do begin
      lbl := '';
      if Length(r.TrackLabel)>0 then
        SetString(lbl, PChar(@r.TrackLabel[0]), Length(r.TrackLabel));

      TrackLabel        := lbl;
      TrackNumber       := r.TrackNum;
      ObjectInstanceIndex:= r.TrackPlatformID;
      FirstDetected     := r.TrackFirstDetected;
      LastDetected      := r.TrackLastDetected;
      PosX              := r.TrackPosX;
      PosY              := r.TrackPosY;
      PosZ              := r.TrackPosZ;
      DetectedSpeed     := r.TrackSpeed;
      DetectedCourse    := r.TrackCourse;
      TrackType         := TTrackType(r.TrackType);
      TrackCategory     := TTrackCategory(r.TrackCategory);
      TrackTypeDomain   := r.TrackTypeDomain;
      TrackDesignation  := r.TrackDesignation;
      TrackIdentifier   := r.TrackIdentifier;
      TrackDomain       := r.TrackDomain;
      isLostContact     := r.TrackisLostContact;

      if TrackType = trBearing then
      begin
//        TT3BearingTrack(track).ObjectSensorId := r.TrackESMObjectSensorId;
//        TT3BearingTrack(track).Bearing        := r.TrackESMBearing;
//        TT3BearingTrack(track).Range          := r.TrackESMRange;
//        TT3BearingTrack(track).RangeInterval  := r.TrackESMRangeInterval;
      end;

      { bypass if possible }
      if isLostContact then
        Exit;

      { flag update false }
      for sensorPair in DetectedBySensors do
      begin
        sensorPair.Value.FlagUpdate := False;
      end;

      { update existing, add if not exist sensor }
      for I := 0 to r.TrackSensorCount - 1 do
      begin

        found := False;

        for sensorPair in DetectedBySensors do
        begin

//          if (Byte(sensorPair.Key.SensorType)             = r.TrackSensorBy[I].SensorType) and
//             (sensorPair.Key.InstanceIndex                = r.TrackSensorBy[I].SensorID) and
//             (sensorPair.Key.PlatformParent.InstanceIndex = r.TrackSensorBy[I].SensorOwner) then
//          begin
//
//            sensor      := sensorPair.Key;
//            sensorInfo  := sensorPair.Value;
//            found := True;
//
//            break;
//          end;
        end;

        { if not found, add sensor }
        if not found then
        begin
          { default search by owner platform }
//          pf       := FindT3PlatformByID(r.TrackSensorBy[I].SensorOwner);
//          if Assigned(pf) and (pf is TT3Vehicle) then
//          begin
//            sensor := TT3Vehicle(pf).getMountedSensor(r.TrackSensorBy[I].SensorID,
//                TESensorType(r.TrackSensorBy[I].SensorType))
//                  as TT3MountedSensor;
//
//            if Assigned(sensor) then
//              addDetectBy(sensor,r.TrackSensorBy[I].SensorFirstDetected, sensorInfo);
//          end;
        end;

        { update sensor info }
        if Assigned(sensorInfo) then
        begin
          with sensorInfo do
          begin
            FlagUpdate     := True;
            LastDetected   := r.TrackSensorBy[I].SensorLastDetected;
            FirstDetected  := r.TrackSensorBy[I].SensorFirstDetected;
            if Assigned(sensor) then
              case sensor.SensorType of
                stRadar :
                  begin
                    TRadarTrackInfo(sensorInfo).DetectedSpeed   := r.TrackSensorBy[I].SensorDetectedSpeed;
                    TRadarTrackInfo(sensorInfo).DetectedCourse  := r.TrackSensorBy[I].SensorDetectedCourse;
                    TRadarTrackInfo(sensorInfo).DetectedAltitude:= r.TrackSensorBy[I].SensorDetectedAltitude;
                  end;
                stIFF :
                  begin
                    TIFFTrackInfo(sensorInfo).isShowIFF   := r.TrackSensorBy[I].isShowIFF;
                    TIFFTrackInfo(sensorInfo).isMode4IFF  := r.TrackSensorBy[I].isMode4IFF;
                    lbl := '';
                    if Length(r.TrackSensorBy[I].TransMode1Detected)>0 then
                      SetString(lbl, PChar(@r.TrackSensorBy[I].TransMode1Detected[0]),
                                Length(r.TrackSensorBy[I].TransMode1Detected));
                    TIFFTrackInfo(sensorInfo).TransMode1Detected:= lbl;

                    lbl := '';
                    if Length(r.TrackSensorBy[I].TransMode2Detected)>0 then
                      SetString(lbl, PChar(@r.TrackSensorBy[I].TransMode2Detected[0]),
                                Length(r.TrackSensorBy[I].TransMode2Detected));
                    TIFFTrackInfo(sensorInfo).TransMode2Detected:= lbl;

                    lbl := '';
                    if Length(r.TrackSensorBy[I].TransMode3Detected)>0 then
                      SetString(lbl, PChar(@r.TrackSensorBy[I].TransMode3Detected[0]),
                                Length(r.TrackSensorBy[I].TransMode3Detected));
                    TIFFTrackInfo(sensorInfo).TransMode3Detected:= lbl;

                    lbl := '';
                    if Length(r.TrackSensorBy[I].TransMode3CDetected)>0 then
                      SetString(lbl, PChar(@r.TrackSensorBy[I].TransMode3CDetected[0]),
                                Length(r.TrackSensorBy[I].TransMode3CDetected));
                    TIFFTrackInfo(sensorInfo).TransMode3CDetected:= lbl;
                  end;
              end;
          end;
        end;

      end;

      alwaysTrack := False;
      { clean up unused sensor }
      toRemove := TList.create;
      for sensorPair in DetectedBySensors do
      begin
        { remove unused }
        if not (sensorPair.Value.FlagUpdate) then
        begin
//          if Assigned(pf) and (pf is TT3Vehicle) then
//          begin
//            sensor := TT3Vehicle(pf).getMountedSensor(sensorPair.Key.InstanceIndex,
//                sensorPair.Key.SensorType) as TT3MountedSensor;
//            toRemove.add(sensor);
//          end;

        end
        else
        if (sensorPair.Key is TT3MountedVisual) and (TrackType = trPoint) then
          alwaysTrack := True;

      end;

      { set always tracking when detect by visual }
//      if tracktype = trpoint then
//        TT3PointTrack(track).isAlwaysTracking := alwaysTrack;

      for i:= 0 to toRemove.count - 1 do
      begin
        sensor := toRemove.Items[i];
        removeDetectBy(sensor);
      end;

      toRemove.Clear;
      toRemove.Free;

    end;
    finally
      MonitorExit(track);
    end;
  end;

end;

procedure TT3ClientManager.UnSelectAllTracks;
var
  i, J: integer;
  dt: TT3Track;
begin
  for i := 0 to FCubicleTracks.ItemCount - 1 do
  begin
    dt := FCubicleTracks.getObject(i) as TT3Track;
    dt.isSelected := false;
  end;
end;

end.
