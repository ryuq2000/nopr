unit uT3SimManager;

interface

uses MapXLib_TLB, uSimManager, uT3SimContainer,
  Classes, uNetSender, SysUtils,uGameData_TTT,
  uLibSettingTTT, uDBScenario, uCoordConvertor,
  uT3ObjectFactory,uDBClassDefinition,uT3Unit,
  uT3PlatformInstance, uT3Environment,
  uT3Vehicle, uT3MountedSensor, uT3mountedWeapon,
  uT3MountedECM, uT3EventManager, uTMapTouch;

type


  TT3SimManager = class abstract(TSimManager)
  private
    FSessionID: integer;
    FOnLogStr: TGetStrProc;
    FOnCommonLogStr: TGetStrProc;

    {*------------------------------------------------------------------------------
      ObjectFactory object for creating all kind of sim object entities
    -------------------------------------------------------------------------------}
    FObjectFactory : TT3ObjectFactory;

//    FDataLinkManager: TT3DatalinkManager;

  {*------------------------------------------------------------------------------
    Create sim objects
  -------------------------------------------------------------------------------}
    procedure CreateSimPlatforms;overload;        // create all platform after load from DB
    function CreateSimPlatforms(db_inst : TDBPlatformInstance) : TT3Unit;overload;
    function CreateSimMountedSensor(const pf_inst_id, pf_id : integer) : TT3Unit;
    function CreateSimMountedWeapon(const pf_inst_id, pf_id : integer) : TT3Unit;
    function CreateSimMountedECM(const pf_inst_id, pf_id : integer) : TT3Unit;

//    { create TT3PlatformInstance from DB PlatformInstance }
//    function SimAssetAssignment(Pi: TPlatform_Instance): TT3PlatformInstance;
//    { create mounted sensor }
//    procedure MountedSensorAssets(Pi: TPlatform_Instance; vehicle: TT3Vehicle);
//    procedure MountedSensorCreator(aList: TList; aType: TEnumMountedObjectType;
//      aParent: TT3Vehicle);
//    { create mounted weapon }
//    procedure MountedWeaponAssets(Pi: TPlatform_Instance; vehicle: TT3Vehicle);
//    procedure MountedWeaponCreator(aList: TList; aType: TEnumMountedObjectType;
//      aParent: TT3Vehicle);
//    { create mounted ecm }
//    procedure MountedECMAssets(Pi: TPlatform_Instance; vehicle: TT3Vehicle);
//    procedure MountedECMCreator(aList: TList; aType: TEnumMountedObjectType;
//      aParent: TT3Vehicle);

    procedure setSessionID(const Value: integer);
    procedure SetOnCommonLogStr(const Value: TGetStrProc);
  protected
    { db scenario }
    FScenario        : tT3DBScenario;
    FGameEnvironment : TT3GameEnvironment;

    {*------------------------------------------------------------------------------
      Each entity has its own container
    -------------------------------------------------------------------------------}
    FSimPlatforms                : TT3SimContainer;     {platforms containers}

    FSimRadarsOnBoards           : TT3SimContainer;     {RadarsOnBoards containers}
    FSimSonarsOnBoards           : TT3SimContainer;     {SonarsOnBoards  containers}
    FSimESMsOnBoards             : TT3SimContainer;     {ESMsOnBoards containers}
    FSimEOsOnBoards              : TT3SimContainer;     {EOsOnBoards containers}
    FSimIFFsOnBoards             : TT3SimContainer;     {IFFsOnBoards containers}
    FSimMADsOnBoards             : TT3SimContainer;     {MADsOnBoards containers}
    FSimVisualsOnBoards          : TT3SimContainer;     {VisualsOnBoards containers}

    FSimAcousticDecoysOnBoards   : TT3SimContainer;     {AcousticDecoysOnBoards containers}
    FSimAirBubblesOnBoards       : TT3SimContainer;     {AirBubblesOnBoards containers}
    FSimChaffsOnBoards           : TT3SimContainer;     {ChaffsOnBoards containers}
    FSimChaffLaunchersOnBoards   : TT3SimContainer;     {ChaffLaunchersOnBoards containers}
    FSimDefensiveJammersOnBoards : TT3SimContainer;     {DefensiveJammersOnBoards containers}
    FSimFloatingDecoysOnBoards   : TT3SimContainer;     {FloatingDecoysOnBoards containers}
    FSimInfraredDecoysOnBoards   : TT3SimContainer;     {InfraredDecoysOnBoards containers}
    FSimJammersOnBoards          : TT3SimContainer;     {JammersOnBoards containers}
    FSimTowedJammersOnBoards     : TT3SimContainer;     {TowedJammersOnBoards containers}
    FSimSonobuoysOnBoards        : TT3SimContainer;     {SonobuoysOnBoards containers}
    FSimFCRsOnBoards             : TT3SimContainer;     {FCRsOnBoards containers}

    FSimMinesOnBoards            : TT3SimContainer;     {MinesOnBoards containers}
    FSimMissilesOnBoards         : TT3SimContainer;     {MissilesOnBoards containers}
    FSimTorpedoesOnBoards        : TT3SimContainer;     {TorpedoesOnBoards containers}
    FSimBombsOnBoards            : TT3SimContainer;     {BombsOnBoards containers}
    FSimGunsOnBoards             : TT3SimContainer;     {GunsOnBoards containers}
    FSimWeaponLauncherOnBoards   : TT3SimContainer;     {weapon launcher}

    {*------------------------------------------------------------------------------
      NetCmdSender are abstract class, can be act as client or server cmd,
      create this object in either server manager or client manager
      main objective : send network command
    -------------------------------------------------------------------------------}
    FNetCmdSender: TNetCmdSender;

    { manage event object }
    FEventManager: TT3EventManager;

    { map and converter }
    FMap: TMap;
    FConverter: TCoordConverter;

    { packet queue list}

    procedure CreateEntitiesContainer;
    procedure DestroyEntitesContainer;

    procedure FGameThread_OnTerminate(sender: TObject);
    procedure FGameThread_OnRunning(const dt: double); virtual;
    procedure FGameThread_OnPaused(const dt: double); virtual;

    procedure FNetworkThread_OnRunning(const dt: double); virtual;
    procedure FNetworkThread_OnPaused(const dt: double); virtual;
    procedure FNetworkThread_OnTerminate(sender: TObject);

    procedure ResetScenario; override;

    procedure AssignMountedSensorEvent(sensor: TT3MountedSensor); virtual;
//    procedure AssignMountedWeaponEvent(sensor: TT3MountedWeapon); virtual;

    { list of network cmd prcedure handle }
    procedure CmdSensor(rec: TRecCmd_Sensor); virtual;
    procedure CmdDataLink(rec : TRecCmd_DataLink); virtual;

    { platform guidance cmd }
    procedure CmdCircleGuide(rec: TRecCmd_PlatformGuidance);
    procedure CmdShadowGuide(rec: TRecCmd_PlatformGuidance);
    procedure CmdEngagementGuide(rec: TRecCmd_PlatformGuidance);
    procedure CmdHelmGuide(rec: TRecCmd_PlatformGuidance);
    procedure CmdOutrunGuide(rec: TRecCmd_PlatformGuidance);

    procedure CmdGameControl(rec: TRecCmd_GameCtrl);
    procedure CmdPlatformReposition(rec : TRecCmd_Platform_MOVE);
    procedure CmdPlatformGuidance(rec : TRecCmd_PlatformGuidance);
    procedure CmdAcousticDecoyOnBoard(rec : TRecCmdAcousticDecoyOnBoard);
    procedure CmdChaffOnBoard(rec :TRecCmdChaffOnBoard);virtual;

  protected

    { all netpacket received here }
    procedure PacketReceive(apRec: PAnsiChar; aSize: word); virtual;
    { packets will process  }
    procedure ProcessPacket;

    { logger specific purpose }
    procedure LogNetwork(const str : String);
    procedure LogEvent(const str : String);

    { missile procedures }
    procedure CmdMissile(var rec : TRecCmd_LaunchMissile); virtual;

    {*------------------------------------------------------------------------------
      List of received cmd procedure
    -------------------------------------------------------------------------------}
    procedure CmdTCPRequest(var rec : TRecTCP_Request; ipRequestFrom : string ); virtual;
    procedure CmdGameControlInfo(rec : TRecUDP_GameCtrl_info);virtual;

    procedure CmdGameTime(apRec: PAnsiChar; aSize: word);
    procedure CmdPlatformMovement(apRec: PAnsiChar; aSize: word);
    function  CmdLaunchRuntimePlatform(var rec : TRecCmd_LaunchRP ): TT3PlatformInstance; virtual;
    {*------------------------------------------------------------------------------
      End List of received cmd procedure
    -------------------------------------------------------------------------------}
  public

    constructor Create(Map: TMapXTouch); virtual;
    destructor Destroy; override;

    procedure GameStart; override;
    procedure GamePause; override;
    procedure GameTerminate; override;

    procedure InitNetwork; virtual;
    procedure StopNetwork; virtual;

    procedure LoadScenarioID(const vSet: TGameDataSetting); virtual;
    procedure OnAssignedPlatform(sender: TObject);

    function FindT3PlatformByID(const id: integer): TT3PlatformInstance;
    function FindT3SensorOnBoardID(const pf_id, ass_id, snsr_type: integer): TT3MountedSensor;
    function FindT3WeaponOnBoardID(const pf_id, ass_id, wpn_type: integer): TT3MountedWeapon;
    function FindT3ECMOnBoardID(const pf_id, ass_id, ecm_type: integer): TT3Mountedecm;

    property SessionID      : integer read FSessionID write setSessionID;
    property OnLogStr       : TGetStrProc read FOnLogStr write FOnLogStr;
    property OnCommonLogStr : TGetStrProc  read FOnCommonLogStr write SetOnCommonLogStr;

    { read only property }
    property Scenario         : tT3DBScenario read FScenario;
    property Converter        : TCoordConverter read FConverter;
    property NetCmdSender     : TNetCmdSender read FNetCmdSender;
    property ObjectFactory    : TT3ObjectFactory read FObjectFactory;
    property GameEnvironment  : TT3GameEnvironment read FGameEnvironment;

    property SimPlatforms     : TT3SimContainer read FSimPlatforms;
    property SimRadarsOnBoards: TT3SimContainer read FSimRadarsOnBoards;
    property SimSonarsOnBoards: TT3SimContainer read FSimSonarsOnBoards;
    property SimESMsOnBoards    : TT3SimContainer read FSimESMsOnBoards   ;
    property SimEOsOnBoards     : TT3SimContainer read FSimEOsOnBoards    ;
    property SimIFFsOnBoards    : TT3SimContainer read FSimIFFsOnBoards   ;
    property SimMADsOnBoards    : TT3SimContainer read FSimMADsOnBoards   ;
    property SimVisualsOnBoards : TT3SimContainer read FSimVisualsOnBoards;

    property SimMinesOnBoards          : TT3SimContainer read FSimMinesOnBoards          ;
    property SimMissilesOnBoards       : TT3SimContainer read FSimMissilesOnBoards       ;
    property SimTorpedoesOnBoards      : TT3SimContainer read FSimTorpedoesOnBoards      ;
    property SimBombsOnBoards          : TT3SimContainer read FSimBombsOnBoards          ;
    property SimGunsOnBoards           : TT3SimContainer read FSimGunsOnBoards           ;
    property SimWeaponLauncherOnBoards : TT3SimContainer read FSimWeaponLauncherOnBoards ;

    property SimAcousticDecoysOnBoards   : TT3SimContainer read FSimAcousticDecoysOnBoards    ;
    property SimAirBubblesOnBoards       : TT3SimContainer read FSimAirBubblesOnBoards        ;
    property SimChaffsOnBoards           : TT3SimContainer read FSimChaffsOnBoards            ;
    property SimChaffLaunchersOnBoards   : TT3SimContainer read FSimChaffLaunchersOnBoards    ;
    property SimDefensiveJammersOnBoards : TT3SimContainer read FSimDefensiveJammersOnBoards  ;
    property SimFloatingDecoysOnBoards   : TT3SimContainer read FSimFloatingDecoysOnBoards    ;
    property SimInfraredDecoysOnBoards   : TT3SimContainer read FSimInfraredDecoysOnBoards    ;
    property SimJammersOnBoards          : TT3SimContainer read FSimJammersOnBoards           ;
    property SimTowedJammersOnBoards     : TT3SimContainer read FSimTowedJammersOnBoards      ;
    property SimSonobuoysOnBoards        : TT3SimContainer read FSimSonobuoysOnBoards         ;
    property SimFCRsOnBoards             : TT3SimContainer read FSimFCRsOnBoards              ;

//    property EventManager     : TT3EventManager read FEventManager;
//    property DataLinkManager  : TT3DatalinkManager read FDataLinkManager;

  end;

implementation

uses tttData,
  Generics.Collections,
  uT3HelmGuide,
  uT3Torpedo, uT3Sonobuoy,

  uT3MountedRadar, uT3MountedSonar,
  uT3MountedESM, uT3mountedIFF,
  uT3MountedEO,ut3MountedMAD,

  uT3CircleGuide,

  uT3MountedSurfaceToSurfaceMissile,
  uT3MountedHybrid,
  uT3MountedMissile, uT3MountedTacticalMissiles,

  uT3MountedAcousticDeploy, uT3mountedAirborneChaff,
  uT3mountedSurfaceChaff, uT3MountedChaff,

  uGlobalVar, uTCPDatatype;


{ TT3SimManager }

procedure TT3SimManager.CmdPlatformGuidance(rec : TRecCmd_PlatformGuidance);
var
  p: TT3PlatformInstance;
begin

  p := FindT3PlatformByID(rec.PlatfomID);

  if not Assigned(p) then
    Exit;

  case rec.OrderID of
    CORD_ID_MOVE:
      begin
        case rec.OrderType of

          CORD_TYPE_CIRCLE_MODE, CORD_TYPE_CIRCLE_CENTER_X,
          CORD_TYPE_CIRCLE_RADIUS, CORD_TYPE_CIRCLE_BEARING,
          CORD_TYPE_CIRCLE_BEARING_STATE, CORD_TYPE_CIRCLE_RANGE,
          CORD_TYPE_CIRCLE_TRACK:
            CmdCircleGuide(rec);

          CORD_TYPE_GUIDANCE :
            begin
              if p is TT3Vehicle then
              begin
                TT3Vehicle(p).changeGuidance(TVehicleGuidanceType(Round(rec.OrderParam)));
                //FEventManager.OnGuidanceChange(p,TVehicleGuidanceType(Round(rCmd^.OrderParam)));
              end;
            end;
          CORD_TYPE_SPEED:
            begin
              if p is TT3Vehicle then
              begin
                TT3Vehicle(p).VehicleGuidance.OrderedSpeed := rec.OrderParam;
              end;
            end;
          CORD_TYPE_COURSE:
            begin
              if p is TT3Vehicle then
              begin
                case TT3Vehicle(p).VehicleGuidance.GuidanceType of
                  vgtHelm : TT3HelmGuidance(TT3Vehicle(p).VehicleGuidance).HelmDegree := rec.OrderParam;
                  else
                    TT3Vehicle(p).VehicleGuidance.OrderedHeading := rec.OrderParam;
                end;

              end
            end;
          CORD_TYPE_ALTITUDE:
            begin
              if p is TT3Vehicle then
              begin
                TT3Vehicle(p).OrderedAltitude := rec.OrderParam;

//                EventManager.OnPlatformOrderedControlChange(rec.OrderID, rec.OrderType,
//                    rec.PlatfomID, rec.OrderParam);
              end
              else if p is TT3Torpedo then
              begin
//                if (TT3Torpedo(p).TipeTorpedo = 8) then
//                begin
//                  if not(TT3Torpedo(p).WireBreak) then
//                  begin
//                    TT3Torpedo(p).OrderedAltitude := rec^.OrderParam;
//                    TT3Torpedo(p).IsInittialGuidance := False;
//                  end;
//                end;
              end
              else if p is TT3Sonobuoy then
              begin
                //TT3Sonobuoy(p).OrderDepth := rec^.OrderParam;
              end;
            end;

        end;
      end;
    CORD_ID_ACTIVATION:
      begin

      end;
  end;
end;

procedure TT3SimManager.CmdPlatformMovement(apRec: PAnsiChar; aSize: word);
var
  rec: ^TRecUDP_PlatformMovement;
  pi: TT3PlatformInstance;
begin
  rec := @apRec^;
  if rec^.SessionID <> FSessionID then
    Exit;

  pi := FindT3PlatformByID(rec^.PlatformID);

  // jika tidak ditemukan di SimPlatform di cari di NonRealPlatform
//  if not Assigned(pi) then
//    pi := FindNonRealPlatformByID(rec^.PlatformID);

  if pi <> nil then
    pi.SetMovementData(rec^);
end;

procedure TT3SimManager.CmdPlatformReposition(rec : TRecCmd_Platform_MOVE);
var
  rCmd : ^TRecCmd_Platform_MOVE;
  pi: TT3PlatformInstance;
begin


  pi := FindT3PlatformByID(rec.PlatfomID);

  // jika tidak ditemukan di SimPlatform di cari di NonRealPlatform
//  if not Assigned(pi) then
//    pi := FindNonRealPlatformByID(rec^.PlatfomID);

  if (pi <> nil) then
  begin
    if pi is TT3PlatformInstance then
    begin
      pi.setPositionX(rec.x);
      pi.setPositionY(rec.y);
    end
//    else
//    begin
//      if (ISInstructor and (rec^.GroupID = 0)) or
//         (ISWasdal and (rec^.GroupID = 0)) then
//      begin
//        pi.RepositionTo(rec^.x, rec^.y);
//        VSimMap.DrawMap;
//      end
//      else if Assigned(FMyCubGroup) then
//      begin
//        if FMyCubGroup.FData.Group_Index = rec^.GroupID then
//        begin
//          pi.RepositionTo(rec^.x, rec^.y);
//          VSimMap.DrawMap;
//        end;
//      end;
//    end;
  end;



end;

procedure TT3SimManager.CmdSensor(rec: TRecCmd_Sensor);
var
  Pi: TT3PlatformInstance;
  obj: TObject;
begin

  Pi := FindT3PlatformByID(rec.PlatformID);

  case rec.SensorType of
    CSENSOR_TYPE_RADAR:
      begin
        obj := FindT3SensorOnBoardID(rec.PlatformID,rec.SensorID,1);

        if (obj = nil) or (TT3MountedRadar(obj).OperationalStatus = sopDamage)
          then
          Exit;

        case rec.OrderID of
          CORD_ID_ControlMode:
            TT3MountedRadar(obj).ControlMode := TRadarControlMode
              (rec.OrderParam);
          CORD_ID_PartialMode:
            ;
          CORD_ID_ECCM :;
        end;
      end;

    CSENSOR_TYPE_SONAR:
      begin
        obj := FindT3SensorOnBoardID(rec.PlatformID,rec.SensorID,2);

        if (obj = nil) or (TT3MountedSonar(obj).OperationalStatus = sopDamage) then
          Exit;

        case rec.OrderID of
          CORD_ID_ControlMode:
            TT3MountedSonar(obj).ControlMode := TSonarControlMode(rec.OrderParam);
          CORD_ID_ControlRangeSonar:
            begin
            TT3MountedSonar(obj).TIOWRange := TSonarTIOWRange(rec.OrderParam);

//            if Assigned(OnLogEventStr) then
//              OnLogEventStr('Sensor ' + TT3Sensor(obj)
//              .InstanceName + ', TIOW : ' + SonarTIOWRangeToString
//              (TSonarTIOWRange(cmd.OrderParam)));
            end;
        end;

//          EventManager.OnSonarControlMode(TT3Sensor(obj), TT3Sonar(obj)
//          .ControlMode);
      end;

    CSENSOR_TYPE_ESM:
      begin
        obj := FindT3SensorOnBoardID(rec.PlatformID,rec.SensorID,7);

        if (obj = nil) or (TT3MountedESM(obj).OperationalStatus = sopDamage) then
          Exit;

      case rec.OrderID of
        CORD_ID_OperationalStatus:
          TT3MountedESM(obj).OperationalStatus := TSensorOperationalStatus(rec.OrderParam);
      end;
    end;

    CSENSOR_TYPE_EO:
      begin
        obj := FindT3SensorOnBoardID(rec.PlatformID,rec.SensorID,7);

        if (obj = nil) or (TT3MountedEO(obj).OperationalStatus = sopDamage) then
          Exit;

        case rec.OrderID of
          CORD_ID_OperationalStatus:
            TT3MountedEO(obj).OperationalStatus := TSensorOperationalStatus(rec.OrderParam);
        end;
      end;

    CSENSOR_TYPE_MAD:
      begin
        obj := FindT3SensorOnBoardID(rec.PlatformID,rec.SensorID,7);

        if (obj = nil) or (TT3MountedMAD(obj).OperationalStatus = sopDamage) then
          Exit;

        case rec.OrderID of
          CORD_ID_OperationalStatus:
            TT3MountedMAD(obj).OperationalStatus := TSensorOperationalStatus(rec.OrderParam);
        end;
      end;

     CSENSOR_TYPE_IFF:
       begin
        obj := FindT3SensorOnBoardID(rec.PlatformID,rec.SensorID,7);

         if (obj = nil) or (TT3MountedIFF(obj).OperationalStatus = sopDamage) then
           Exit;

         case rec.OrderID of
           1: TT3MountedIFF(obj).TransponderOperateStatus := TSensorOperationalStatus(rec.OrderParam);
           2: TT3MountedIFF(obj).InterrogatorOperateStatus := TSensorOperationalStatus(rec.OrderParam);
       end;
     end;
  end;
end;

procedure TT3SimManager.CmdShadowGuide(rec: TRecCmd_PlatformGuidance);
begin

end;

procedure TT3SimManager.CmdTCPRequest(var rec: TRecTCP_Request; ipRequestFrom : string);
begin
  // defined in server side;
end;

constructor TT3SimManager.Create(Map: TMapXTouch);
begin
  inherited Create;

  FMap            := Map;

  FObjectFactory  := TT3ObjectFactory.Create;
  FScenario       := tT3DBScenario.Create;
  FScenario.OnAssignedPlatform := OnAssignedPlatform;

  FConverter      := TCoordConverter.Create;
  FConverter.FMap := FMap;
  FGameEnvironment:= TT3GameEnvironment.Create(FConverter);

  CreateEntitiesContainer;

  FGameThread.Interval       := 10;
  FGameThread.OnRunning      := FGameThread_OnPaused;
  FGameThread.OnTerminate    := FGameThread_OnTerminate;

  FNetworkThread.OnRunning   := FNetworkThread_OnRunning;
  FNetworkThread.OnTerminate := FNetworkThread_OnTerminate;
  FNetworkThread.Enabled     := true;

//  FDataLinkManager    := TT3DatalinkManager.Create;
//  FDataLinkManager.SetSimPlatforms(SimPlatforms);

  FGameState := gsStop;

end;

procedure TT3SimManager.CreateEntitiesContainer;
begin
  FSimPlatforms                := TT3SimContainer.Create;

  FSimRadarsOnBoards           := TT3SimContainer.Create;
  FSimSonarsOnBoards           := TT3SimContainer.Create;
  FSimESMsOnBoards             := TT3SimContainer.Create;
  FSimEOsOnBoards              := TT3SimContainer.Create;
  FSimIFFsOnBoards             := TT3SimContainer.Create;
  FSimMADsOnBoards             := TT3SimContainer.Create;
  FSimVisualsOnBoards          := TT3SimContainer.Create;

  FSimAcousticDecoysOnBoards   := TT3SimContainer.Create;
  FSimAirBubblesOnBoards       := TT3SimContainer.Create;
  FSimChaffsOnBoards           := TT3SimContainer.Create;
  FSimChaffLaunchersOnBoards   := TT3SimContainer.Create;
  FSimDefensiveJammersOnBoards := TT3SimContainer.Create;
  FSimFloatingDecoysOnBoards   := TT3SimContainer.Create;
  FSimInfraredDecoysOnBoards   := TT3SimContainer.Create;
  FSimJammersOnBoards          := TT3SimContainer.Create;
  FSimTowedJammersOnBoards     := TT3SimContainer.Create;
  FSimSonobuoysOnBoards        := TT3SimContainer.Create;
  FSimFCRsOnBoards             := TT3SimContainer.Create;

  FSimMinesOnBoards            := TT3SimContainer.Create;
  FSimMissilesOnBoards         := TT3SimContainer.Create;
  FSimTorpedoesOnBoards        := TT3SimContainer.Create;
  FSimBombsOnBoards            := TT3SimContainer.Create;
  FSimGunsOnBoards             := TT3SimContainer.Create;
  FSimWeaponLauncherOnBoards   := TT3SimContainer.Create;

end;

function TT3SimManager.CreateSimPlatforms(db_inst : TDBPlatformInstance): TT3Unit;
var
  unt : TT3Unit;
begin
  result := nil;
  unt := FObjectFactory.createPlatform(TPlatformType(db_inst.Platform_Type),
            db_inst.Platform_Instance_Index );
  if Assigned(unt) then
  begin
    FSimPlatforms.AddObject(unt);

    CreateSimMountedSensor(db_inst.Platform_Instance_Index,db_inst.Platform_Index);
    CreateSimMountedWeapon(db_inst.Platform_Instance_Index,db_inst.Platform_Index);
    CreateSimMountedECM(db_inst.Platform_Instance_Index,db_inst.Platform_Index);
  end;

  result := unt;
end;

function TT3SimManager.CreateSimMountedECM(const pf_inst_id, pf_id: integer) : TT3Unit;
var
  dbAccDecoy : TDBAcousticDecoyOnBoardList;
  dbBubbles : TDBAirBubbleOnBoardList;
  dbFloat   : TDBFloatingDecoyOnBoardList;
  dbChaff   : TDBChaffOnBoardList;
  dbChafflauncher : TDBChaffLauncherOnBoardList;
  dbDefJammer:TDBDefensiveJammerOnBoardList;
  dbInfra   : TDBInfraredDecoyOnBoardList;
  dbJammer  : TDBJammerOnBoardList;
  dbTowed   : TDBTowedJammerOnBoardList;
  dbSono    : TDBSonobuoyOnBoardList;

  decoy : TDBAcoustic_Decoy_On_Board;
  bubble: TDBAir_Bubble_Mount;
  float : TDBFloating_Decoy_On_Board;
  chaff : TDBChaff_On_Board;
  chafflauncher : TDBChaff_Launcher_On_Board;
  defj  : TDBDefensive_Jammer_On_Board;
  infra : TDBInfrared_Decoy_On_Board;
  jammer: TDBJammer_On_Board;
  towed : TDBTowed_Jammer_Decoy_On_Board;
  sono  : TDBSonobuoy_On_Board;

  unt : TT3Unit;
  dbVeh : TDBVehicle_Definition;
begin
  result := nil;
  {*------------------------------------------------------------------------------
    add accoustic decoy on board
  -------------------------------------------------------------------------------}
  Scenario.DBAcousticDecoyOnBoardDict.TryGetValue(pf_id,dbAccDecoy);
  if Assigned(dbAccDecoy) then
    for decoy in dbAccDecoy do
    begin
      unt := FObjectFactory.createMountedCM(otMeAcoustic,
              decoy.Acoustic_Instance_Index, decoy.Decoy_Index,
              pf_inst_id);
      if Assigned(unt) then
        FSimAcousticDecoysOnBoards.AddObject(unt);
    end;

  {*------------------------------------------------------------------------------
    add air bubble on board
  -------------------------------------------------------------------------------}
  Scenario.DBAirBubbleOnBoardDict.TryGetValue(pf_id,dbBubbles);
  if Assigned(dbBubbles) then
    for bubble in dbBubbles do
    begin
      unt := FObjectFactory.createMountedCM(otMeAirBubble,
              bubble.Air_Bubble_Instance_Index, bubble.Air_Bubble_Index,
              pf_inst_id);
      if Assigned(unt) then
        FSimAirBubblesOnBoards.AddObject(unt);
    end;

  {*------------------------------------------------------------------------------
    add floating decoy on board
  -------------------------------------------------------------------------------}
  Scenario.DBFloatingDecoyOnBoardDict.TryGetValue(pf_id,dbFloat);
  if Assigned(dbFloat) then
    for float in dbFloat do
    begin
      unt := FObjectFactory.createMountedCM(otMeFloating,
              float.Floating_Decoy_Instance_Index, float.Floating_Decoy_Index,
              pf_inst_id);
      if Assigned(unt) then
        FSimFloatingDecoysOnBoards.AddObject(unt);
    end;

  {*------------------------------------------------------------------------------
    add chaff on board
  -------------------------------------------------------------------------------}
  Scenario.DBChaffOnBoardDict.TryGetValue(pf_id,dbChaff);
  if Assigned(dbChaff) then
    for chaff in dbChaff do
    begin

      dbVeh := Scenario.GetDBVehicleDefinition(chaff.Vehicle_Index);
      if Assigned(dbVeh) then
      begin
        case dbVeh.Platform_Domain of
          { airborne chaff }
          0 :
            unt := FObjectFactory.createMountedCM(otMeAirborneChaff,
                chaff.Chaff_Instance_Index, chaff.Chaff_Index,
                pf_inst_id);
          else
          { surface chaff }
            unt := FObjectFactory.createMountedCM(otMeSurfaceChaff,
                chaff.Chaff_Instance_Index, chaff.Chaff_Index,
                pf_inst_id);
        end;
        if Assigned(unt) then
          FSimChaffsOnBoards.AddObject(unt);
      end;
    end;

  {*------------------------------------------------------------------------------
    add chaff launcher on board
  -------------------------------------------------------------------------------}
  Scenario.DBChaffLauncherOnBoardDict.TryGetValue(pf_id,dbChafflauncher);
  if Assigned(dbChafflauncher) then
    for chafflauncher in dbChafflauncher do
    begin
      unt := FObjectFactory.createMountedCM(otMeChaffLauncher,
          chafflauncher.Launcher_Number, 0,
          pf_inst_id);

      if Assigned(unt) then
        FSimChaffLaunchersOnBoards.AddObject(unt);
    end;


  {*------------------------------------------------------------------------------
    add DefensiveJammer on board
  -------------------------------------------------------------------------------}
  Scenario.DBDefensiveJammerOnBoardDict.TryGetValue(pf_id,dbDefJammer);
  if Assigned(dbDefJammer) then
    for defj in dbDefJammer do
    begin
      unt := FObjectFactory.createMountedCM(otMeDefensiveJammer,
              defj.Defensive_Jammer_Instance_Index, defj.Defensive_Jammer_Index,
              pf_inst_id);
      if Assigned(unt) then
        FSimDefensiveJammersOnBoards.AddObject(unt);
    end;

  {*------------------------------------------------------------------------------
    add Infrared on board
  -------------------------------------------------------------------------------}
  Scenario.DBInfraredDecoyOnBoardDict.TryGetValue(pf_id,dbInfra);
  if Assigned(dbInfra) then
    for infra in dbInfra do
    begin
      unt := FObjectFactory.createMountedCM(otMeInfrared,
              infra.Infrared_Decoy_Instance_Index, infra.Infrared_Decoy_Index,
              pf_inst_id);
      if Assigned(unt) then
        FSimInfraredDecoysOnBoards.AddObject(unt);
    end;

  {*------------------------------------------------------------------------------
    add Jammer on board
  -------------------------------------------------------------------------------}
  Scenario.DBJammerOnBoardDict.TryGetValue(pf_id,dbJammer);
  if Assigned(dbJammer) then
    for jammer in dbJammer do
    begin
      unt := FObjectFactory.createMountedCM(otMeRadarNoiseJammer,
              jammer.Jammer_Instance_Index, jammer.Jammer_Index,
              pf_inst_id);
      if Assigned(unt) then
        FSimJammersOnBoards.AddObject(unt);
    end;

  {*------------------------------------------------------------------------------
    add Towed Jammer on board
  -------------------------------------------------------------------------------}
  Scenario.DBTowedJammerOnBoardDict.TryGetValue(pf_id,dbTowed);
  if Assigned(dbTowed) then
    for towed in dbTowed do
    begin
      unt := FObjectFactory.createMountedCM(otMeTowedJammer,
              towed.Towed_Decoy_Instance_Index, towed.Towed_Decoy_Index,
              pf_inst_id);
      if Assigned(unt) then
        FSimTowedJammersOnBoards.AddObject(unt);
    end;

  {*------------------------------------------------------------------------------
    add sonobuoy on board
  -------------------------------------------------------------------------------}
  Scenario.DBSonobuoyOnBoardDict.TryGetValue(pf_id,dbSono);
  if Assigned(dbSono) then
    for sono in dbSono do
    begin
      unt := FObjectFactory.createMountedCM(otMeSonobuoy,
              sono.Sonobuoy_Instance_Index, sono.Sonobuoy_Index,
              pf_inst_id);
      if Assigned(unt) then
        FSimSonobuoysOnBoards.AddObject(unt);
    end;

  result := unt;
end;

function TT3SimManager.CreateSimMountedSensor(const pf_inst_id, pf_id: integer) : TT3Unit;
var
  dbRdr : TDBRadarOnBoardList;
  dbSnr : TDBSonarOnBoardList;
  dbEO  : TDBEOOnBoardList;
  dbESM : TDBESMOnBoardList;
  dbVis : TDBVisualOnBoardList;
  dbMAD : TDBMADOnBoardList;
  dbIFF : TDBIFFOnBoardList;
  dbFCR : TDBFCROnBoardList;

  rdr : TDBRadar_On_Board;
  snr : TDBSonar_On_Board;
  eo  : TDBEO_On_Board;
  esm : TDBESM_On_Board;
  vis : TDBVisual_Sensor_On_Board;
  mad : TDBMAD_Sensor_On_Board;
  iff : TDBIFF_Sensor_On_Board;
  fcr : TDBFCR_On_Board;

  unt : TT3Unit;
begin
  result := nil;
  {*------------------------------------------------------------------------------
    add radar on board
  -------------------------------------------------------------------------------}
  Scenario.DBRadarOnBoardDict.TryGetValue(pf_id,dbRdr);
  if Assigned(dbRdr) then
    for rdr in dbRdr do
    begin
      unt := FObjectFactory.createMountedSensor(otMsRadar,
              rdr.Radar_Instance_Index, rdr.Radar_Index,
              pf_inst_id);
      if Assigned(unt) then
      begin
        FSimRadarsOnBoards.AddObject(unt);
        AssignMountedSensorEvent(TT3MountedSensor(unt));
      end;
    end;

  {*------------------------------------------------------------------------------
    add sonar on board
  -------------------------------------------------------------------------------}
  Scenario.DBSonarOnBoardDict.TryGetValue(pf_id,dbSnr);
  if Assigned(dbSnr) then
    for snr in dbSnr do
    begin
      unt := FObjectFactory.createMountedSensor(otMsSonar,
              snr.Sonar_Instance_Index, snr.Sonar_Index,
              pf_inst_id );
      if Assigned(unt) then
      begin
        FSimSonarsOnBoards.AddObject(unt);
        AssignMountedSensorEvent(TT3MountedSensor(unt));
      end;
    end;

  {*------------------------------------------------------------------------------
    add EO on board
  -------------------------------------------------------------------------------}
  Scenario.DBEOOnBoardDict.TryGetValue(pf_id,dbEO);
  if Assigned(dbEO) then
    for eo in dbEO do
    begin
      unt := FObjectFactory.createMountedSensor(otMsEO,
              eo.EO_Instance_Index, eo.EO_Index,
              pf_inst_id );
      if Assigned(unt) then
      begin
        FSimEOsOnBoards.AddObject(unt);
        AssignMountedSensorEvent(TT3MountedSensor(unt));
      end;
    end;

  {*------------------------------------------------------------------------------
    add esm on board
  -------------------------------------------------------------------------------}
  Scenario.DBESMOnBoardDict.TryGetValue(pf_id,dbESM);
  if Assigned(dbESM) then
    for esm in dbESM do
    begin
      unt := FObjectFactory.createMountedSensor(otMsESM,
              esm.ESM_Instance_Index, esm.ESM_Index,
              pf_inst_id);
      if Assigned(unt) then
      begin
        FSimESMsOnBoards.AddObject(unt);
        AssignMountedSensorEvent(TT3MountedSensor(unt));
      end;
    end;

  {*------------------------------------------------------------------------------
    add visual on board
  -------------------------------------------------------------------------------}
  Scenario.DBVisualOnBoardDict.TryGetValue(pf_id,dbVis);
  if Assigned(dbVis) then
    for vis in dbVis do
    begin
      unt := FObjectFactory.createMountedSensor(otMsVisual,
              vis.Visual_Instance_Index, 0,
              pf_inst_id );
      if Assigned(unt) then
      begin
        FSimVisualsOnBoards.AddObject(unt);
        AssignMountedSensorEvent(TT3MountedSensor(unt));
      end;
    end;

  {*------------------------------------------------------------------------------
    add mad on board
  -------------------------------------------------------------------------------}
  Scenario.DBMADOnBoardDict.TryGetValue(pf_id,dbMAD);
  if Assigned(dbMAD) then
    for mad in dbMAD do
    begin
      unt := FObjectFactory.createMountedSensor(otMsMAD,
              mad.MAD_Instance_Index, mad.MAD_Index,
              pf_inst_id );
      if Assigned(unt) then
      begin
        FSimMADsOnBoards.AddObject(unt);
        AssignMountedSensorEvent(TT3MountedSensor(unt));
      end;
    end;

  {*------------------------------------------------------------------------------
    add iff on board
  -------------------------------------------------------------------------------}
  Scenario.DBIFFOnBoardDict.TryGetValue(pf_id,dbIFF);
  if Assigned(dbIFF) then
    for iff in dbIFF do
    begin
      unt := FObjectFactory.createMountedSensor(otMsIFF,
              iff.IFF_Instance_Index, 0,
              pf_inst_id );
      if Assigned(unt) then
      begin
        FSimIFFsOnBoards.AddObject(unt);
        AssignMountedSensorEvent(TT3MountedSensor(unt));
      end;
    end;

  {*------------------------------------------------------------------------------
    add fcr on board
  -------------------------------------------------------------------------------}
  Scenario.DBFCROnBoardDict.TryGetValue(pf_id,dbFCR);
  if Assigned(dbFCR) then
    for fcr in dbFCR do
    begin
      unt := FObjectFactory.createMountedSensor(otMsFCR,
              fcr.FCR_Instance_Index, fcr.Radar_Index,
              pf_inst_id );
      if Assigned(unt) then
      begin
        FSimFCRsOnBoards.AddObject(unt);
        AssignMountedSensorEvent(TT3MountedSensor(unt));
      end;
    end;

  result := unt;
end;

function TT3SimManager.CreateSimMountedWeapon(const pf_inst_id,
  pf_id: integer) : TT3Unit;
var
  dbWpn : TDBFitWeaponOnBoardList;
  dbPnt : TDBPointEffectOnBoardList;
  wpn : TDBFitted_Weapon_On_Board;
  pnt : TDBPoint_Effect_On_Board;
  unt : TT3Unit;
begin
  result := nil;
  {*------------------------------------------------------------------------------
    add fitted weapon on board
  -------------------------------------------------------------------------------}
  Scenario.DBWeaponOnBoardDict.TryGetValue(pf_id,dbWpn);
  if Assigned(dbWpn) then
    for wpn in dbWpn do
    begin
      case wpn.Mount_Type of
        {missile or hybrid}
        1 :
        begin
          unt := FObjectFactory.createMountedWeapon(otMwMissile,
              wpn.Fitted_Weap_Index, wpn.Weapon_Index,
              pf_inst_id );
          if Assigned(unt) then
            FSimMissilesOnBoards.AddObject(unt);

        end;
        {torpedo}
        2 :
        begin
          unt := FObjectFactory.createMountedWeapon(otMwTorpedo,
              wpn.Fitted_Weap_Index, wpn.Weapon_Index,
              pf_inst_id );
          if Assigned(unt) then
            FSimTorpedoesOnBoards.AddObject(unt);
        end;
        {mine}
        3 :
        begin
          unt := FObjectFactory.createMountedWeapon(otMwMine,
              wpn.Fitted_Weap_Index, wpn.Weapon_Index,
              pf_inst_id );
          if Assigned(unt) then
            FSimMinesOnBoards.AddObject(unt);
        end;
      end;
    end;
  {*------------------------------------------------------------------------------
    add point effect weapon on board
  -------------------------------------------------------------------------------}
  Scenario.DBPointEffectOnBoardDict.TryGetValue(pf_id,dbPnt);
  if Assigned(dbPnt) then
    for pnt in dbPnt do
    begin
      case pnt.Mount_Type of
        {gun}
        1 :
        begin
          unt := FObjectFactory.createMountedWeapon(otMwGun,
              pnt.Point_Effect_Index, pnt.Weapon_Index,
              pf_inst_id );
          if Assigned(unt) then
            FSimGunsOnBoards.AddObject(unt);
        end;
        {bomb}
        2 :
        begin
          unt := FObjectFactory.createMountedWeapon(otMwBomb,
              pnt.Point_Effect_Index, pnt.Weapon_Index,
              pf_inst_id );
          if Assigned(unt) then
            FSimBombsOnBoards.AddObject(unt);
        end;
      end;
    end;
  result := unt;
end;

procedure TT3SimManager.CreateSimPlatforms;
var
  dbPF  : TDBPairPlatformInstanceDict;
begin
  {*------------------------------------------------------------------------------
    add sim platform
  -------------------------------------------------------------------------------}
  for dbPF in Scenario.DBPlatformInstance do
    CreateSimPlatforms(dbPF.Value);
end;

destructor TT3SimManager.Destroy;
begin
  DestroyEntitesContainer;

  FScenario.Free;
  FConverter.Free;
  FObjectFactory.Free;
  FNetCmdSender.Free;
  FGameEnvironment.Free;
  FEventManager.Free;


  inherited;
end;

procedure TT3SimManager.DestroyEntitesContainer;
begin
  FSimRadarsOnBoards.Free;
  FSimSonarsOnBoards.Free;
  FSimESMsOnBoards.Free;
  FSimEOsOnBoards.Free;
  FSimIFFsOnBoards.Free;
  FSimMADsOnBoards.Free;
  FSimVisualsOnBoards.Free;

  FSimAcousticDecoysOnBoards.Free;
  FSimAirBubblesOnBoards.Free;
  FSimChaffsOnBoards.Free;
  FSimChaffLaunchersOnBoards.Free;
  FSimDefensiveJammersOnBoards.Free;
  FSimFloatingDecoysOnBoards.Free;
  FSimInfraredDecoysOnBoards.Free;
  FSimJammersOnBoards.Free;
  FSimTowedJammersOnBoards.Free;
  FSimSonobuoysOnBoards.Free;
  FSimFCRsOnBoards.Free;

  FSimWeaponLauncherOnBoards.Free;
  FSimMinesOnBoards.Free;
  FSimMissilesOnBoards.Free;
  FSimTorpedoesOnBoards.Free;
  FSimBombsOnBoards.Free;
  FSimGunsOnBoards.Free;

  FSimPlatforms.Free;
end;

procedure TT3SimManager.FGameThread_OnPaused(const dt: double);
begin

end;

procedure TT3SimManager.FGameThread_OnRunning(const dt: double);
var
  i, gS: integer;
begin
  if GameSPEED < 1.0 then
  begin
    // slow down baby...
  end
  else
  begin

    gS := Round(GameSPEED);
    for i := 0 to gS - 1 do
    begin
      FMainVTime.IncreaseMillisecond(dt * 1000.0);

      { move platforms }
      FSimPlatforms.Move(dt);

      FSimMinesOnBoards.Move(dt);
      FSimMissilesOnBoards.Move(dt);
      FSimTorpedoesOnBoards.Move(dt);
      FSimBombsOnBoards.Move(dt);
      FSimGunsOnBoards.Move(dt);

      FSimRadarsOnBoards.Move(dt);
      FSimSonarsOnBoards.Move(dt);
      FSimESMsOnBoards.Move(dt);
      FSimEOsOnBoards.Move(dt);
      FSimIFFsOnBoards.Move(dt);
      FSimMADsOnBoards.Move(dt);
      FSimVisualsOnBoards.Move(dt);

      FSimAcousticDecoysOnBoards.Move(dt);
      FSimAirBubblesOnBoards.Move(dt);
      FSimChaffsOnBoards.Move(dt);
      FSimChaffLaunchersOnBoards.Move(dt);
      FSimDefensiveJammersOnBoards.Move(dt);
      FSimFloatingDecoysOnBoards.Move(dt);
      FSimInfraredDecoysOnBoards.Move(dt);
      FSimJammersOnBoards.Move(dt);
      FSimTowedJammersOnBoards.Move(dt);
      FSimSonobuoysOnBoards.Move(dt);
      FSimFCRsOnBoards.Move(dt);

    end;

  end;

end;

procedure TT3SimManager.FGameThread_OnTerminate(sender: TObject);
begin

end;

function TT3SimManager.FindT3ECMOnBoardID(const pf_id, ass_id,
  ecm_type: integer): TT3Mountedecm;
var
  i, c: integer;
  Pi: TT3MountedECM;
  f: Boolean;
begin
  f := false;
  i := 0;
  case ecm_type of
    1 : c := FSimAcousticDecoysOnBoards.ItemCount;
    2 : c := FSimAirBubblesOnBoards.ItemCount;
    3 : c := FSimChaffsOnBoards.ItemCount;
    4 : c := FSimDefensiveJammersOnBoards.ItemCount;
    5 : c := FSimFloatingDecoysOnBoards.ItemCount;
    6 : c := FSimInfraredDecoysOnBoards.ItemCount;
    7 : c := FSimJammersOnBoards.ItemCount;
    8 : c := FSimTowedJammersOnBoards.ItemCount;
    9 : c := FSimSonobuoysOnBoards.ItemCount;
  end;

  Pi := nil;

  while not f and (i < c) do
  begin
    case ecm_type of
      1 : Pi := TT3MountedECM(FSimAcousticDecoysOnBoards.getObject(i));
      2 : Pi := TT3MountedECM(FSimAirBubblesOnBoards.getObject(i));
      3 : Pi := TT3MountedECM(FSimChaffsOnBoards.getObject(i));
      4 : Pi := TT3MountedECM(FSimDefensiveJammersOnBoards.getObject(i));
      5 : Pi := TT3MountedECM(FSimFloatingDecoysOnBoards.getObject(i));
      6 : Pi := TT3MountedECM(FSimInfraredDecoysOnBoards.getObject(i));
      7 : Pi := TT3MountedECM(FSimJammersOnBoards.getObject(i));
      8 : Pi := TT3MountedECM(FSimTowedJammersOnBoards.getObject(i));
      9 : Pi := TT3MountedECM(FSimSonobuoysOnBoards.getObject(i));
    end;

    if Assigned(Pi) then
      f := (Pi.PlatformParentInstanceIndex = pf_id) and (Pi.InstanceIndex = ass_id);

    inc(i);
  end;

  if f then
    Result := Pi
  else
    Result := nil;
end;

function TT3SimManager.FindT3PlatformByID(const id: integer)
  : TT3PlatformInstance;
var
  i, c: integer;
  Pi: TT3PlatformInstance;
  f: Boolean;
begin
  f := false;
  i := 0;
  c := FSimPlatforms.itemCount;
  Pi := nil;

  while not f and (i < c) do
  begin
    Pi := TT3PlatformInstance(FSimPlatforms.getObject(i));
    f := (Pi.InstanceIndex = id);
    inc(i);
  end;

  if f then
    Result := Pi
  else
    Result := nil;
end;


function TT3SimManager.FindT3SensorOnBoardID(const pf_id,
  ass_id, snsr_type: integer): TT3MountedSensor;
var
  i, c: integer;
  Pi: TT3MountedSensor;
  f: Boolean;
begin
  f := false;
  i := 0;
  case snsr_type of
    1 : c := FSimRadarsOnBoards.itemCount;
    2 : c := FSimSonarsOnBoards.itemCount;
    3 : c := FSimVisualsOnBoards.itemCount;
    4 : c := FSimEOsOnBoards.itemCount;
    5 : c := FSimMADsOnBoards.itemCount;
    6 : c := FSimIFFsOnBoards.itemCount;
    7 : c := FSimESMsOnBoards.itemCount;
  end;

  Pi := nil;

  while not f and (i < c) do
  begin
    case snsr_type of
      1 : Pi := TT3MountedSensor(FSimRadarsOnBoards.getObject(i));
      2 : Pi := TT3MountedSensor(FSimSonarsOnBoards.getObject(i));
      3 : Pi := TT3MountedSensor(FSimVisualsOnBoards.getObject(i));
      4 : Pi := TT3MountedSensor(FSimEOsOnBoards.getObject(i));
      5 : Pi := TT3MountedSensor(FSimMADsOnBoards.getObject(i));
      6 : Pi := TT3MountedSensor(FSimIFFsOnBoards.getObject(i));
      7 : Pi := TT3MountedSensor(FSimESMsOnBoards.getObject(i));
    end;

    if Assigned(Pi) then
      f := (Pi.PlatformParentInstanceIndex = pf_id) and (Pi.InstanceIndex = ass_id);

    inc(i);
  end;

  if f then
    Result := Pi
  else
    Result := nil;
end;

function TT3SimManager.FindT3WeaponOnBoardID(const pf_id, ass_id,
  wpn_type: integer): TT3MountedWeapon;
var
  i, c: integer;
  Pi: TT3MountedWeapon;
  f: Boolean;
begin
  f := false;
  i := 0;
  case wpn_type of
    1 : c := FSimMissilesOnBoards.itemCount;
    2 : c := FSimTorpedoesOnBoards.itemCount;
    3 : c := FSimMinesOnBoards.itemCount;
    4 : c := FSimBombsOnBoards.itemCount;
    5 : c := FSimGunsOnBoards.itemCount;
  end;

  Pi := nil;

  while not f and (i < c) do
  begin
    case wpn_type of
      1 : Pi := TT3MountedWeapon(FSimMissilesOnBoards.getObject(i));
      2 : Pi := TT3MountedWeapon(FSimTorpedoesOnBoards.getObject(i));
      3 : Pi := TT3MountedWeapon(FSimMinesOnBoards.getObject(i));
      4 : Pi := TT3MountedWeapon(FSimBombsOnBoards.getObject(i));
      5 : Pi := TT3MountedWeapon(FSimGunsOnBoards.getObject(i));
    end;

    if Assigned(Pi) then
      f := (Pi.PlatformParentInstanceIndex = pf_id) and (Pi.InstanceIndex = ass_id);

    inc(i);
  end;

  if f then
    Result := Pi
  else
    Result := nil;
end;

procedure TT3SimManager.FNetworkThread_OnPaused(const dt: double);
begin

end;

procedure TT3SimManager.FNetworkThread_OnRunning(const dt: double);
begin
  if Assigned(nethandle) then
    nethandle.GetPacket;
end;

procedure TT3SimManager.FNetworkThread_OnTerminate(sender: TObject);
begin

end;

procedure TT3SimManager.GamePause;
begin

  if FGameState = gsPlaying then
  begin

    FGameThread.OnRunning := FGameThread_OnPaused;

    // thread do nothing. :P
  end;

  inherited;
end;

procedure TT3SimManager.GameStart;
begin
  if FGameState = gsStop then
  begin
    FGameThread.OnRunning := FGameThread_OnRunning;

    if FGameThread.Suspended then
      FGameThread.Start;

  end
  else
  begin

  end;

  inherited;

end;

procedure TT3SimManager.GameTerminate;
begin
  inherited;

  FGameState := gsTerminated;
  FGameThread.Terminate;

end;

procedure TT3SimManager.InitNetwork;
begin

  { define all registered packet here, for client and server, }

  {*------------------------------------------------------------------------------
    Registering TCP and UDP command for game control
  -------------------------------------------------------------------------------}
  nethandle.RegisterUDPPacket(CPID_GAMETIME,          SizeOf(TRecUDP_GameTime),CmdGameTime);
  nethandle.RegisterUDPPacket(CPID_UDP_GAMECTRL_INFO,     SizeOf(TRecUDP_GameCtrl_Info), PacketReceive);

  nethandle.RegisterUDPPacket(CPID_CMD_GAME_CTRL, SizeOf(TRecCmd_GameCtrl),PacketReceive);
  nethandle.RegisterTCPPacket(CPID_CMD_GAME_CTRL, SizeOf(TRecCmd_GameCtrl),PacketReceive);

  nethandle.RegisterTCPPacket(CPID_TCP_REQUEST,   SizeOf(TRecTCP_Request),PacketReceive);

  nethandle.RegisterUDPPacket(CPID_UDP_Synch_Obj, SizeOf(TRecUDP_Synch_Obj),PacketReceive);
  nethandle.RegisterUDPPacket(CPID_Synch_MISSILE_TARGET, SizeOf(TRecUDP_Synch_Missile), PacketReceive);

  nethandle.RegisterUDPPacket(CPID_UDP_HYBRID,SizeOf(TrecCmd_LaunchHybrid), PacketReceive);

  {*------------------------------------------------------------------------------
    Registering TCP and UDP command for platform control
  -------------------------------------------------------------------------------}
  nethandle.RegisterUDPPacket(CPID_PLATFORM_MOVE,SizeOf(TRecUDP_PlatformMovement), PacketReceive);
  nethandle.RegisterUDPPacket(CPID_PLATFORM_LANDDATA, SizeOf(TRecUDP_PlatfomLandData), PacketReceive);

  nethandle.RegisterUDPPacket(CPID_CMD_PLATFORM_REPOS,     SizeOf(TRecCmd_Platform_MOVE), PacketReceive);
  nethandle.RegisterTCPPacket(CPID_CMD_PLATFORM_REPOS,     SizeOf(TRecCmd_Platform_MOVE), PacketReceive);

  nethandle.RegisterTCPPacket(CPID_CMD_LAUNCH_RUNTIME_PLATFORM, SizeOf(TRecCmd_LaunchRP), PacketReceive);
  nethandle.RegisterUDPPacket(CPID_CMD_LAUNCH_RUNTIME_PLATFORM, SizeOf(TRecCmd_LaunchRP), PacketReceive);

 {*------------------------------------------------------------------------------
    Registering TCP and UDP command for vehicle guidance
  -------------------------------------------------------------------------------}
  nethandle.RegisterTCPPacket(CPID_CMD_PLATFORM,  SizeOf(TRecCmd_PlatformGuidance),PacketReceive);
  nethandle.RegisterUDPPacket(CPID_CMD_PLATFORM,  SizeOf(TRecCmd_PlatformGuidance),PacketReceive);

  {*------------------------------------------------------------------------------
    Registering TCP and UDP command for countermeasure control panel
  -------------------------------------------------------------------------------}

  nethandle.RegisterTCPPacket(CPID_CMD_ACOUSTIC_DECOY_ONBOARD,SizeOf(TRecCmdAcousticDecoyOnBoard), PacketReceive);
  nethandle.RegisterUDPPacket(CPID_CMD_ACOUSTIC_DECOY_ONBOARD,SizeOf(TRecCmdAcousticDecoyOnBoard), PacketReceive );

  nethandle.RegisterTCPPacket(CPID_CMD_CHAFF_ONBOARD,SizeOf(TRecCmdChaffOnBoard), PacketReceive);
  nethandle.RegisterUDPPacket(CPID_CMD_CHAFF_ONBOARD,SizeOf(TRecCmdChaffOnBoard), PacketReceive);

  {*------------------------------------------------------------------------------
    Registering TCP and UDP command for sensor control panel
  -------------------------------------------------------------------------------}
  nethandle.RegisterTCPPacket(CPID_CMD_SENSOR,  SizeOf(TRecCmd_Sensor), PacketReceive); // radar, sonar, esm
  nethandle.RegisterUDPPacket(CPID_CMD_SENSOR,  SizeOf(TRecCmd_Sensor), PacketReceive);

end;

procedure TT3SimManager.LoadScenarioID(const vSet: TGameDataSetting);
begin
  ResetScenario;

  FScenario.LoadFromDB(vSet.ScenarioID);
  FGameEnvironment.LoadEnvironmentFromDB(FScenario.DBGameEnvironment);

  CreateSimPlatforms;

  FMainVTime.Reset(0);
  FMainVTime.DateTimeOffset := FScenario.DBResourceAloc.Game_Start_Time;

  // DataLinkManager.SetGameDefaults(GameDefaults);

  //
  //
  // for i := 0 to Scenario.Formation.Count - 1 do
  // begin
  // if FlastFormationID < TFormation(Scenario.Formation[i]).FFormation_Def.Formation_Index then
  // begin
  // FlastFormationID  := TFormation(Scenario.Formation[i]).FFormation_Def.Formation_Index;
  // end;
  // end;
  //
  // Inc(FlastFormationID);

end;

procedure TT3SimManager.LogEvent(const str: String);
var
  txt : String;
begin
  txt := TimetoStr(GameTime) + ':[EVENT]>>';
  if Assigned(OnCommonlogStr) then
    OnCommonlogStr(txt + str);
end;

procedure TT3SimManager.LogNetwork(const str: String);
var
  txt : String;
begin
  txt := TimetoStr(GameTime) + ':[NETWORK]>>';
  if Assigned(OnCommonlogStr) then
    OnCommonlogStr(txt + str);
end;

procedure TT3SimManager.OnAssignedPlatform(sender: TObject);
//var
//  Pi: TPlatform_Instance;
begin
//  if sender is TPlatform_Instance then
//  begin
//    Pi := TPlatform_Instance(sender);
//    SimAssetAssignment(Pi);
//  end;
end;

//procedure TT3SimManager.AssignMountedWeaponEvent(sensor: TT3MountedWeapon);
//begin
//
//end;

procedure TT3SimManager.AssignMountedSensorEvent(sensor: TT3MountedSensor);
begin
  if not Assigned(sensor) then
    Exit;

  with sensor do
  begin

    OnSensorRemoved           := FEventManager.OnSensorRemoved;
    OnSensorOperationalStatus := FEventManager.OnSensorOperationalStatus;

    // OnDestroy := EventManager.OnSensorRemoved;
    // OnLogEventStr := EventManager.OnLogEventStr;
    // OnDayTimeUpdate := EventManager.OnCheckDateTime;
    // OnDeploySonar := EventManager.OnDeploySonar;
    // OnUpdateFormSonar := EventManager.OnUpdateFormSonar;
    // OnUpdateActualCable := EventManager.OnUpdateActualCable;
    // OnModeSearchIFF := EventManager.OnModeSearchIFF;

    // specific event
    case SensorType of
      stRadar :
      begin
        //TT3MountedRadar(sensor).OnControlMode    := EventManager.OnRadarControlMode;
        //TT3MountedRadar(sensor).OnRemoveDetected := EventManager.OnRadarRemoveAssignedPlatform;
      end;
      stSonar :
      begin
        //OnDeploymentStatusChange := EventManager.OnSonarDeploymentStatusChange;
      end;

    end;
  end;

  // if sensor is TT3DataLink then
  // begin
  // with TT3DataLink(sensor) do
  // begin
  // OnDatalinkPoolRequest := EventManager.OnDatalinkPoolRequest;
  // OnDatalinkOffline := EventManager.OnDatalinkOffline;
  // OnDatalinkOnline := EventManager.OnDatalinkOnline;
  // OnDatalinkTrackUpdate := EventManager.OnDatalinkTracksUpdate;
  // OnNCSDatalinkEMCON := EventManager.OnNCSDatalinkEMCON;
  // OnPUDatalinkEMCON := EventManager.OnPUDatalinkEMCON;
  // end;
  // end;

end;

procedure TT3SimManager.CmdAcousticDecoyOnBoard(rec : TRecCmdAcousticDecoyOnBoard);
var
  ecm  : TT3MountedECM;
begin

  ecm := FindT3ECMOnBoardID(rec.PlatformIndex,rec.AssetIndex,1);
  if Assigned(ecm) then
  begin
    case rec.OrderID of
      1 : TT3MountedAcousticDeploy(ecm).DeploymentAction := TECMDeploymentAction(rec.Value);
      2 : TT3MountedAcousticDeploy(ecm).Control          := TECMControlActivation(rec.Value);
      3 : TT3MountedAcousticDeploy(ecm).CycleTimer       := TECMCycleTimer(rec.Value);
      4 : TT3MountedAcousticDeploy(ecm).Mode             := TECMAcousticDecoyMode(rec.Value);
      5 : TT3MountedAcousticDeploy(ecm).FilterSetting    := rec.Value;
    end;
  end;
end;

procedure TT3SimManager.CmdChaffOnBoard(rec :TRecCmdChaffOnBoard);
var
  ecm  : TT3MountedECM;
begin

  ecm := FindT3ECMOnBoardID(rec.PlatformIndex,rec.AssetIndex,3);
  if Assigned(ecm) then
  begin
    case rec.OrderID of
      CORD_CHAFF_TYPE :
      begin
        if (ecm.Category = ecAirborneChaff) then
          TT3MountedAirborneChaff(ecm).DeploymentType := TEnumAirborneType(rec.Value);
      end;
      CORD_CHAFF_DEPLOY :
      begin
      end;
      CORD_CHAFF_QUANTITY :
        TT3MountedChaff(ecm).Quantity := rec.Value;
    end;
  end;
end;

procedure TT3SimManager.CmdCircleGuide(rec: TRecCmd_PlatformGuidance);
var
  p, vCircle: TT3PlatformInstance;
begin

  p := FindT3PlatformByID(rec.PlatfomID);

  if not Assigned(p) then
    Exit;

  if p is TT3Vehicle then
    if TT3Vehicle(p).VehicleGuidance.GuidanceType = vgtCircle then
    begin

      with TT3CircleGuidance(TT3Vehicle(p).VehicleGuidance) do
      begin
        case rec.OrderType of
          CORD_TYPE_CIRCLE_MODE :
          begin
            Circlemode := TCGMode(Round(rec.OrderParam));
            Circlestate := TCGState(1);
          end;
          CORD_TYPE_CIRCLE_CENTER_X :
          begin
            CenterCirclePointX := rec.OrderParam;
            Circlestate := TCGState(1);
            Circlemode  := TCGMode(1);
          end;
          CORD_TYPE_CIRCLE_RADIUS :
          begin
            CircleRadius := rec.OrderParam;
            Circlestate := TCGState(1);
          end;
          CORD_TYPE_CIRCLE_BEARING :
          begin
            CircleBearing := rec.OrderParam;
            CircleState := TCGState(1);
          end;
          CORD_TYPE_CIRCLE_BEARING_STATE :
          begin
            CircleBearingState := TCGBearing(Round(rec.OrderParam));
            CircleState := TCGState(1);
          end;
          CORD_TYPE_CIRCLE_RANGE :
          begin
            CircleRange := rec.OrderParam;
            CircleState := TCGState(1);
          end;
          CORD_TYPE_CIRCLE_TRACK :
          begin
            TargetInstanceIndex := Round(rec.OrderParam);
            CircleState := TCGState(1);
            CircleMode  := TCGMode(2);
          end;
        end;
      end;

//      EventManager.OnPlatformOrderedControlChange(rec.OrderID, rec.OrderType,
//          rec.PlatfomID, rec.OrderParam);
    end;

end;

procedure TT3SimManager.CmdDataLink(rec: TRecCmd_DataLink);
begin

end;

procedure TT3SimManager.CmdEngagementGuide(rec: TRecCmd_PlatformGuidance);
begin

end;

procedure TT3SimManager.CmdGameControl(rec: TRecCmd_GameCtrl);
begin

  case rec.GameCtrl of
    CORD_ID_start:
      begin
        GameSPEED := 1.0;
        GameStart;
      end;
    CORD_ID_pause:
      begin
        GamePause;
        GameSPEED := 0.0;
      end;
    CORD_ID_game_speed:
      begin ;
        GameSPEED := rec.GameSPEED;
      end;
  end;

end;

procedure TT3SimManager.CmdGameControlInfo(rec: TRecUDP_GameCtrl_info);
begin
end;

procedure TT3SimManager.CmdGameTime(apRec: PAnsiChar; aSize: word);
var
  rec: ^TRecUDP_GameTime;
begin // broadcast synchronize buat Game time Viewer;
  rec := @apRec^;

  if rec^.SessionID <> FSessionID then
    Exit;

  FMainVTime.SetMilliSecond(rec^.GameMS);
end;

procedure TT3SimManager.CmdHelmGuide(rec: TRecCmd_PlatformGuidance);
begin

end;

function TT3SimManager.CmdLaunchRuntimePlatform(
  var rec: TRecCmd_LaunchRP): TT3PlatformInstance;
var
  newPIIdx : integer;
  dbPf : TDBPlatformInstance;
  pf  : TT3PlatformInstance;
begin
  newPIIdx := 0;
  result := nil;

  if machineRole = crServer then
    dbPf := Scenario.CreateNewDBPlatformInstance(newPIIdx,rec.RPPlatformID,
      rec.NewRPType,rec.ForceDesignation,rec.InstanceName,rec.TrackIdent)
  else
    dbPf := Scenario.CreateNewDBPlatformInstance(rec.NewPlatformID,rec.RPPlatformID,
      rec.NewRPType,rec.ForceDesignation,rec.InstanceName,rec.TrackIdent);

  if Assigned(dbPf) then
  begin
    pf := CreateSimPlatforms(dbPf) as TT3PlatformInstance;

    pf.PosX := rec.pX;
    pf.PosY := rec.pY;
    pf.PosZ := rec.pZ;

    if pf is TT3PlatformInstance then
    begin
      TT3PlatformInstance(pf).Speed     := rec.PSpeed;
      TT3PlatformInstance(pf).Course    := rec.PHeading;
      TT3PlatformInstance(pf).Altitude  := rec.PAltitude;
    end;

    result := pf;
  end;

  if machineRole = crServer then
  begin
    rec.NewPlatformID := newPIIdx;
    NetCmdSender.CmdLaunchRP(rec);
  end;

end;

procedure TT3SimManager.CmdMissile(var rec: TRecCmd_LaunchMissile);
var
  mountMissile : TT3MountedWeapon;
  obj: TObject;
begin

  mountMissile := FindT3WeaponOnBoardID(rec.ParentPlatformID,rec.MissileID,1);
  if not Assigned(mountMissile) then
    Exit;

  case rec.Order of
    CORD_ID_launch_hybrid     :;

    { set platform target id }
    CORD_ID_MissileTargetId   :
    begin
      mountMissile.TargetInstanceIdx := rec.TargetPlatformID;
      mountMissile.TargetTrackNumber := rec.ValInteger;

      { set target type to track, for tactic, hybrid }
      if mountMissile is TT3MountedTacticalMissiles then
        TT3MountedTacticalMissiles(mountMissile).TargetType := 1
      else
      if mountMissile is TT3MountedHybrid then
        TT3MountedHybrid(mountMissile).TargetType := 1;
    end;

    { surface to surface specific command }
    CORD_ID_MissileEngageType :
    begin
      if mountMissile is TT3MountedSurfaceToSurfaceMissile then
        TT3MountedSurfaceToSurfaceMissile(mountMissile).Engagement := TMissileEngagement(rec.EngagementMode);
    end;

    { surface to surface specific command }
    CORD_ID_MissileFiringType :
    begin
      if mountMissile is TT3MountedSurfaceToSurfaceMissile then
        TT3MountedSurfaceToSurfaceMissile(mountMissile).FiringMode := TMissileFiringMode(rec.FiringMode);
    end;

    { surface to surface specific command }
    CORD_ID_MissileDestruckRng :
    begin
      if mountMissile is TT3MountedSurfaceToSurfaceMissile then
        TT3MountedSurfaceToSurfaceMissile(mountMissile).DestruckRange := rec.ValSingle;
    end;

    { surface to surface specific command }
    CORD_ID_MissileXoverRng :
    begin
      if mountMissile is TT3MountedSurfaceToSurfaceMissile then
        TT3MountedSurfaceToSurfaceMissile(mountMissile).CrossOverRange := rec.ValSingle;
    end;

    CORD_ID_MissileLauncher,
    CORD_ID_MissilePlan,
    CORD_ID_MissileCancel      :
    begin
    end;

    CORD_ID_MissileLaunch      :;

  end;

  //obj := TT3Vehicle(Pi).getMountedSensor(rec.SensorID, TT3MountedRadar);
end;

procedure TT3SimManager.CmdOutrunGuide(rec: TRecCmd_PlatformGuidance);
begin

end;

procedure TT3SimManager.PacketReceive(apRec: PAnsiChar; aSize: word);
var
  { rec for catch pid / packet id and session id only }
  id   : integer;
  ipTo : string;

  rec                 : ^TRecSynch_Vehicle;
  rCmdGC              : ^TRecCmd_GameCtrl;
  rCmdSensor          : ^TRecCmd_Sensor;
  rCmdRepos           : ^TRecCmd_Platform_MOVE;
  rCmdNRJammer        : ^TrecRadarNoiseJammer;
  rCmdEMCON           : ^TRecCmd_ModeEmcon;
  rCmdLMissile        : ^TRecCmd_LaunchMissile;
  rCmdGunFire         : ^TRecCmd_GunFire;
  rCmdGuidance        : ^TRecCmd_PlatformGuidance;
  rCmdAcousticOnBoard : ^TRecCmdAcousticDecoyOnBoard;
  rCmdChaffOnBoard    : ^TRecCmdChaffOnBoard;
begin

  rec := @apRec^;

  if rec = nil then
    Exit;

  if rec^.SessionID <> FSessionID then
    Exit;

  id   := rec^.pid.recID;
  ipTo := LongIp_To_StrIp(rec^.pid.ipSender);

  if Assigned(OnLogStr) then
    OnLogStr('Receive : ' + IntToStr(id));

  case id of

    CPID_CMD_GAME_CTRL :
      begin
        rCmdGC := @apRec^;
        CmdGameControl(rCmdGC^);

        if machineRole = crServer then
          NetCmdSender.CmdGameControl(rCmdGC^);
      end;

    CPID_CMD_PLATFORM_REPOS :
      begin
        rCmdRepos := @apRec^;
        CmdPlatformReposition(rCmdRepos^);

        if machineRole = crServer then
          NetCmdSender.CmdRepos(rCmdRepos^);
      end;

    CPID_CMD_PLATFORM:
      begin
        rCmdGuidance := @apRec^;
        CmdPlatformGuidance(rCmdGuidance^);

        if machineRole = crServer then
          NetCmdSender.CmdPlatformGuidance(rCmdGuidance^);
      end;

    CPID_CMD_SENSOR:
      begin
        rCmdSensor := @apRec^;
        CmdSensor(rCmdSensor^);

        { broadcast if server }
        if machineRole = crServer then
          NetCmdSender.CmdSensor(rCmdSensor^);
      end;

    CPID_CMD_ACOUSTIC_DECOY_ONBOARD :
      begin
        rCmdAcousticOnBoard := @apRec^;
        CmdAcousticDecoyOnBoard(rCmdAcousticOnBoard^);

        { broadcast if server }
        if machineRole = crServer then
          NetCmdSender.CmdAcousticDecoyOnBoard(rCmdAcousticOnBoard^);
      end;

    CPID_CMD_CHAFF_ONBOARD :
      begin
        rCmdChaffOnBoard := @apRec^;
        CmdChaffOnBoard(rCmdChaffOnBoard^);

        { broadcast if server }
        if machineRole = crServer then
          NetCmdSender.CmdChaffOnBoard(rCmdChaffOnBoard^);
      end;

//    CPID_UDP_CMD_RADAR_NOISE_JAMMER:
//      begin
//        rCmdNRJammer := @apRec^;
//        //CmdNoiseJammer(rCmdSensor^);
//
//        { broadcast if server }
//        if machineRole = crServer then
//          FNetCmdSender.CmdNoiseJammer(rCmdNRJammer^);
//      end;
//
//    CPID_UDP_CMD_EMCON_MODE:
//      begin
//        rCmdEMCON := @apRec^;
//        //CmdEMCON(rCmdEMCON^);
//
//        { broadcast if server }
//        if machineRole = crServer then
//          FNetCmdSender.CmdEMCON(rCmdEMCON^);
//      end;
//
//    CPID_UDP_CMD_LAUNCH_MISSILE:
//      begin
//        rCmdLMissile := @apRec^;
//        CmdMissile(rCmdLMissile^);
//
//        { broadcast if server }
//        if machineRole = crServer then
//          FNetCmdSender.CmdMissile(rCmdLMissile^);
//      end;
//
//    CPID_UDP_CMD_LAUNCH_RUNTIME_PLATFORM:
//      begin
//        rCmdLaunchRP := @apRec^;
//        //CmdLaunchRP(rCmdLaunchRP^);
//
//        { broadcast if server }
//        if machineRole = crServer then
//          FNetCmdSender.CmdLaunchRP(rCmdLaunchRP^);
//      end;
//
//    CPID_UDP_CMD_GUN_FIRE:
//      begin
//        rCmdGunFire := @apRec^;
//        //CmdGunFire(rCmdGunFire^);
//
//        { broadcast if server }
//        if machineRole = crServer then
//          FNetCmdSender.CmdGunFire(rCmdGunFire^);
//      end;

  end;

end;

procedure TT3SimManager.ProcessPacket;
begin

end;

procedure TT3SimManager.ResetScenario;
begin
  inherited;

end;

procedure TT3SimManager.SetOnCommonLogStr(const Value: TGetStrProc);
begin
  FOnCommonLogStr := Value;
end;

//procedure TT3SimManager.MountedECMAssets(Pi: TPlatform_Instance;
//  vehicle: TT3Vehicle);
//begin
//
//  MountedECMCreator(Pi.vehicle.Acoustic_Decoys, otMeAcoustic, vehicle);
//  MountedECMCreator(Pi.vehicle.Chaffs, otMeChaff, vehicle);
//  //CounterMeasureSet(pi.Vehicle.Chaff_Launchers,TT3ChaffLauncher,vehicle);
//  MountedECMCreator(Pi.vehicle.Floating_Decoys, otMeFloating, vehicle);
//  MountedECMCreator(Pi.vehicle.Infrared_Decoys, otMeInfrared, vehicle);
//  MountedECMCreator(Pi.vehicle.Towed_Jammer_Decoys, otMeTowedJammer, vehicle);
//  MountedECMCreator(Pi.vehicle.Defensive_Jammers, otMeDefensiveJammer, vehicle);
//  MountedECMCreator(Pi.vehicle.Air_Bubble_Mount, otMeAirBubble, vehicle);
//  MountedECMCreator(Pi.vehicle.Jammers, otMeRadarNoiseJammer, vehicle);
//
//end;

//procedure TT3SimManager.MountedECMCreator(aList: TList; aType: TEnumMountedObjectType;
//  aParent: TT3Vehicle);
//begin
//
//end;
//
//procedure TT3SimManager.MountedSensorAssets(Pi: TPlatform_Instance;
//  vehicle: TT3Vehicle);
//begin
//
//  MountedSensorCreator(Pi.vehicle.Radars, otMsRadar, vehicle);
//  MountedSensorCreator(Pi.vehicle.Sonars, otMsSonar, vehicle);
//  MountedSensorCreator(Pi.vehicle.Visualsensors, otMsVisual, vehicle);
//  MountedSensorCreator(Pi.vehicle.EOSensors, otMsEO, vehicle);
//  MountedSensorCreator(Pi.vehicle.ESMSensors, otMsESM, vehicle);
//  MountedSensorCreator(Pi.vehicle.MADSensors, otMsMAD, vehicle);
//  MountedSensorCreator(Pi.vehicle.IFFSensors, otMsIFF, vehicle);
//  MountedSensorCreator(Pi.vehicle.FCRSensors, otMsFCR, vehicle);
//  MountedSensorCreator(Pi.vehicle.Sonobuoy, otMsSonobuoy, vehicle);
//
//end;
//
//procedure TT3SimManager.MountedSensorCreator(aList: TList; aType: TEnumMountedObjectType;
//  aParent: TT3Vehicle);
//var
//  J: integer;
//  sensor: TObject;
//begin
//
//  for J := 0 to aList.Count - 1 do
//  begin
//    sensor := nil;
//    { create with object factory }
//    sensor := ObjectFactory.createMountedSensor(aType, aList[J]);
//    if Assigned(sensor) then
//    begin
//      TT3MountedSensor(sensor).SetObjects(FSimPlatforms);
//      TT3MountedSensor(sensor).PlatformParent := aParent;
//
//      AssignMountedSensorEvent(TT3MountedSensor(sensor));
//      FSimMountedSensor.AddObject(sensor);
//      aParent.MountedSensors.add(TT3MountedSensor(sensor));
//    end;
//
//  end;
//
//end;

//procedure TT3SimManager.MountedWeaponAssets(Pi: TPlatform_Instance;
//  vehicle: TT3Vehicle);
//begin
//
//  MountedWeaponCreator(Pi.vehicle.Missiles, otMwMissile, vehicle);
//  MountedWeaponCreator(Pi.vehicle.Torpedos, otMwTorpedo, vehicle);
//  MountedWeaponCreator(Pi.vehicle.Mines, otMwMine, vehicle);
//  MountedWeaponCreator(Pi.vehicle.Bombs, otMwBomb, vehicle);
//  MountedWeaponCreator(Pi.vehicle.Guns, otMwgun, vehicle);
//  MountedWeaponCreator(Pi.vehicle.Hybrids, otMwHybrid, vehicle);
//
//  // if (vehicle.PlatformDomain <> 2) and (vehicle.PlatformType <> 40) then
//  // VectacSet(TT3Vectac, vehicle);
//
//end;
//
//procedure TT3SimManager.MountedWeaponCreator(aList: TList; aType: TEnumMountedObjectType;
//  aParent: TT3Vehicle);
//var
//  J: integer;
//  mweapon: TObject;
//begin
//
//  for J := 0 to aList.Count - 1 do
//  begin
//    mweapon := nil;
//    { create with object factory }
//    mweapon := ObjectFactory.createMountedWeapon(aType, aList[J], aParent.PlatformDomain);
//    if Assigned(mweapon) then
//    begin
//      TT3MountedDevice(mweapon).PlatformParent := aParent;
//
//      AssignMountedWeaponEvent(TT3MountedWeapon(mweapon));
//      aParent.MountedWeapons.add(TT3MountedWeapon(mweapon));
//      FSimMountedWeapon.AddObject(mweapon);
//    end;
//  end;
//end;

procedure TT3SimManager.setSessionID(const Value: integer);
begin
  FSessionID := Value;
  FNetCmdSender.SessionID := Value;
end;

//function TT3SimManager.SimAssetAssignment(Pi: TPlatform_Instance)
//  : TT3PlatformInstance;
//var
//  enumPlatform: TPlatformType;
//  grp : T3CubicleGroup;
//begin
//
////  Result := nil;
////  enumPlatform := ptNone;
////
////  if Pi.FData.Vehicle_Index > 0 then
////    enumPlatform := ptVehicle
////  else if Pi.FData.Mine_Index > 0 then
////    enumPlatform := ptMine
////  else if Pi.FData.Missile_Index > 0 then
////    enumPlatform := ptMissile
////  else if Pi.FData.Satellite_Index > 0 then
////    enumPlatform := ptSatellite
////  else if Pi.FData.Torpedo_Index > 0 then
////    enumPlatform := ptTorpedo
////  else if Pi.FData.Hybrid_Index > 0 then
////    enumPlatform := ptHybrid;
////
////  Result := ObjectFactory.createPlatform(enumPlatform, Pi);
////  Result.Environment  := FScenario.DBGameEnvironment;
////  Result.GameDefaults := FScenario.DBGameDefaults;
////
////  if Assigned(Result) then
////  begin
////    { set group }
////    grp := FScenario.CubiclesGroupsList.GetGroupOf_PlatformIndex(Result.InstanceIndex);
////
////    if Assigned(grp) then
////      Result.PlatformGroup := grp.FData.Group_Index
////    else
////      Result.PlatformGroup := -1;
////
////    if enumPlatform = ptVehicle then
////    begin
////      { add mounted sensor assets }
////      MountedSensorAssets(Pi, TT3Vehicle(Result));
////      { add mounted weapon assets }
////      MountedWeaponAssets(Pi, TT3Vehicle(Result));
////    end;
////
////    FSimPlatforms.AddObject(Result);
////  end;
//end;

procedure TT3SimManager.StopNetwork;
begin
  // unregister all packet

  nethandle.UnRegisterUDP;
  nethandle.UnRegisterTCP;

  FNetworkThread.Enabled     := False;
  FNetworkThread.OnRunning   := nil;
  FNetworkThread.OnTerminate := nil;
end;

end.
