unit uT3ServerManager;

interface

uses uT3SimManager, uSteppers, uThreadTimer, MapXLib_TLB, uLibSettingTTT, SysUtils,
  DateUtils, Classes, uVirtualTime, uT3TrackManager,
  uT3PlatformInstance, uT3Vehicle, uT3ServerEventManager,
  uT3MountedSensor, uGameData_TTT, uTMapTouch;

type
  TOnUpdateGameInfo = procedure (const st: byte;
    const gSpeed: double) of object;
  TOnUpdateFormTrack = procedure (track : TT3TrackManager ) of object;

  TT3ServerManager = class (TT3SimManager)
  private
    FOnUpdateGameInfo  : TOnUpdateGameInfo;

    FSendCounter       : Integer;
    FNetSender         : TDelayer;
    FUpdateForm        : TDelayer;

    { sensor track manager }
    FTrackManager      : TT3TrackManager;
    FonUpdateFormTrack : TOnUpdateFormTrack;

    procedure SetOnUpdateGameInfo(const Value: TOnUpdateGameInfo);
    function getBroadcastData: boolean;
    procedure setBroadcastData(const Value: boolean);

    procedure onTrackDetected(Sender, detected: TObject; Value: boolean);
    procedure OnIFFSensorDetect(aSensor, aTarget : TObject;
          value, value2 : boolean;
          mode1,mode2,mode3,mode3C : string);


    procedure updateForm(Sender: TObject);
    procedure SetonUpdateFormTrack(const Value: TOnUpdateFormTrack);

    // send UDP
    procedure NetSendUDP_BroadCast(Sender: TObject);
    procedure NetSendUDP_MovementData(Sender: TObject);
    procedure netSendUDP_GameTime(Sender: TObject);
    procedure netSendUDP_TrackSync(Sender: TObject);
  protected

    { all netpacket received here }
    procedure PacketReceive(apRec: PAnsiChar; aSize: word); override;

    procedure ResetScenario;   override;

    procedure FGameThread_OnRunning(const dt: double); override;
    procedure FGameThread_OnPaused(const dt: double); override;
    procedure FNetworkThread_OnRunning(const dt: double); override;

    procedure setGameSpeed(const Value: double); override;

    procedure AssignMountedSensorEvent(sensor: TT3MountedSensor); override;

    procedure CmdTCPRequest(var rec : TRecTCP_Request; ipRequestFrom : string ); override;
    procedure CmdCubicleAssignInfo(const toIp: string);
    procedure CmdDataToVBS(sIP : string);

  published

  public
    constructor Create(Map : TMapXTouch);override;
    destructor Destroy; override;

    procedure LoadScenarioID(const vSet: TGameDataSetting); override;

    procedure GameStart; override;
    procedure GamePause; override;
    procedure GameTerminate; override;

   { all network related procedure write here }

    procedure InitNetwork; override;
    procedure StopNetwork; override;

    procedure FNetServerOnClientConnect(const S: string);
    procedure FNetServerOnClientDisConnect(const S: string);
//    procedure NetSessionOnConnectedRequestCubicle(sender: TObject);

    property BroadcastData : boolean read getBroadcastData write setBroadcastData;
    { === end === }
    property TrackManager : TT3TrackManager read FTrackManager;

    property onUpdateFormTrack : TOnUpdateFormTrack read FonUpdateFormTrack write SetonUpdateFormTrack;
    property OnUpdateGameInfo  : TOnUpdateGameInfo  read FOnUpdateGameInfo  write SetOnUpdateGameInfo;

  end;

var
  serverManager : TT3ServerManager;

implementation

uses
  uNetHandle_Server, uT3Common, uTCPDatatype,
  tttData, uGlobalVar, uNetServerSender, uT3Unit; //, uT3MountedIFF, uNetSessionData, usimObjects;

{ TT3ServerManager }

procedure TT3ServerManager.AssignMountedSensorEvent(sensor: TT3MountedSensor);
var
  srvEvent : TT3ServerEventManager;
begin
  inherited;

  srvEvent := FEventManager as TT3ServerEventManager;

  with sensor do
  begin
    OnSensorDetect := onTrackDetected;

    case SensorType of
      stIFF :
      begin
//        TT3MountedIFF(sensor).OnIFFDetect := OnIFFSensorDetect;
      end;
    end;
  end;

end;


procedure TT3ServerManager.CmdCubicleAssignInfo(const toIp: string);
begin
end;

procedure TT3ServerManager.CmdDataToVBS(sIP: string);
var
  recgameTime : TRec_GameTime;
begin
    recgameTime.OrderID := 1;
    recgameTime.GameYear := StrToInt(FormatDateTime('yyyy', GameTIME));
    recgameTime.GameMonth := StrToInt(FormatDateTime('mm', GameTIME));
    recgameTime.GameDay := StrToInt(FormatDateTime('dd', GameTIME));
    recgameTime.Gamehour := StrToInt(FormatDateTime('hh', GameTIME));
    recgameTime.GameMinute := StrToInt(FormatDateTime('nn', GameTIME));
    recgameTime.GameSecond := StrToInt(FormatDateTime('ss', GameTIME));
//    TNetHandle_Server(nethandle).SendTo(CPID_GAME_TIME, @recgameTime, sIP);
end;

procedure TT3ServerManager.CmdTCPRequest(var rec: TRecTCP_Request; ipRequestFrom : string);
var
  rGC    : TRecUDP_GameCtrl_info;
  rSyn   : TRecCmdSYNCH;
  rSynch : TRecSynch_Vehicle;
  i      : integer;
  pf     : TObject;
begin
  inherited;

  case rec.reqID of
    REQ_SYNCH_GAMECTRL_INFO:
    begin
      rGC.GameState  := Byte(GameState);
      rGC.GameSpeed  := GameSPEED;
      rGC.GameTimeMS := FMainVTime.GetMillisecond;
      rGC.SessionID  := SessionID;

      TNetServerCmdSender(FNetCmdSender).CmdGameControlInfoTo(rGC, ipRequestFrom);
    end;

    REQ_LAST_PLATFORMID_INFO:
    begin
      rSyn.SynchID        := CORD_ID_Synch_NextPID;
//      rSyn.Synch_IParam   := FLastPlatformID;

//      VNetServer.SendTo(CPID_CMD_SYNCH, @rSyn, ipTo);
    end;

    REQ_CUBICLE_ASSIGNMENT :
    begin
//      netSend_CubicleAssignInfo(ipTo);
    end;

    REQ_MISSED_PACKET:
    begin
      TNetHandle_Server(nethandle).SendAllSentTo(ipRequestFrom);

      for i := 0 to SimPlatforms.itemCount - 1 do
      begin
        pf := SimPlatforms.getObject(i);

        if pf is TT3Vehicle then begin
          with TT3Vehicle(pf) do begin
            rSynch.PlatformID   := InstanceIndex;
            rSynch.SessionID    := SessionID;
            //rSynch.DormantState := Dormant;
            rSynch.OnGrounded   := OnGrounded;
            rSynch.OnLand       := OnLand;
            rSynch.DamageSensor := DamageSensor;
            rSynch.DamageWeapon := DamageWeapon;
            rSynch.DamageComm   := DamageComm;
            rSynch.DamageHelm   := DamageHelm;
            rSynch.DamagePropulsion := DamagePropulsion;
            rSynch.FuelLeakage  := FuelLeakage;
            //rSynch.FuelRemaining  := FuelRemaining;
            //rSynch.MLRemaining  := MLRemaining;
            //rSynch.ATRemaining  := ATRemaining;
            //rSynch.FoodRemaining  := FoodRemaining;
            //rSynch.WaterRemaining  := WaterRemaining;

            //TNetHandle_Server(nethandle).SendTo(CPID_SYNCH_VEHICLE, @rSynch, ipRequestFrom);
          end;
        end;
      end;
    end;

    REQ_DATA_PLATFORM :
    begin
      rGC.GameState  := Byte(GameState);
      rGC.GameSpeed  := GameSPEED;
      rGC.GameTimeMS := FMainVTime.GetMillisecond;
      rGC.SessionID  := SessionID;
      //VNetServer.SendBroadcastCommand(CPID_CMD_SYNCH_GAMECTRL, @rGC);

      CmdDataToVBS(ipRequestFrom);
    end;

  end;
end;

constructor TT3ServerManager.Create(Map: TMapXTouch);
begin
  inherited;

  FEventManager := TT3ServerEventManager.Create;
  FTrackManager := TT3TrackManager.Create;

  FNetSender := TDelayer.Create;
  FNetSender.Enabled := false;
  FNetSender.OnTime := NetSendUDP_BroadCast;
  FNetSender.DInterval := 100; //

  FUpdateForm := TDelayer.Create;
  FUpdateForm.Enabled := True;
  FUpdateForm.OnTime := updateForm;
  FUpdateForm.DInterval := 1; //

  FSendCounter := 0;

  FNetCmdSender := TNetServerCmdSender.Create;

  machineRole := crServer;

end;

destructor TT3ServerManager.Destroy;
begin

  FNetSender.Free;
  FUpdateForm.Free;

  FTrackManager.Free;
  inherited;
end;

procedure TT3ServerManager.FGameThread_OnPaused(const dt: double);
  procedure SlowDown ;
  var i, gS: integer;
  begin // d
    if GameSPEED < 1.0 then begin
       // slow down baby...

    end
    else begin
      gS := Round(GameSPEED);

      for I := 0 to gs - 1 do
      begin
      end;

      FNetSender.Run(dt);
    end;
  end;
var
  nLongWord : LongWord ;
  i, gS : integer;
  recSyncPos : TRecSyncPos;
begin
  inherited;

//  case FGamePlayType of
//  gpmScenario,
//  gpmScenAndRecord :
//    begin
//      SlowDown ;
//
//    end;
//  gpmReplay   :
//    begin
//      SlowDown;
//    end;
//  end;
//
//  //check bomb mine
//  //CheckBombMine(dt);
//
//  FConnectDelay.Run(dt);

end;

procedure TT3ServerManager.FGameThread_OnRunning(const dt: double);
  procedure SlowDown;
  var
    i, gS: Integer;
  begin // d
    if GameSPEED < 1.0 then
    begin
      // slow down baby...

    end
    else
    begin
      gS := Round(GameSPEED);

      for i := 0 to gS - 1 do
      begin
      end;

      FTrackManager.Move(dt);
      netSendUDP_TrackSync(nil);

      FNetSender.Run(dt);
      FUpdateForm.Run(dt);
    end;
  end;
begin
  inherited;

  SlowDown;

end;

procedure TT3ServerManager.FNetServerOnClientConnect(const S: string);
var rSyn: TRecCmdSYNCH;
begin
  rSyn.SynchID        := CORD_ID_Synch_NextPID;
  //rSyn.Synch_IParam   := FLastPlatformID;
  //VNetServer.SendTo(CPID_CMD_SYNCH, @rSyn, s);

  if Assigned(OnLogStr) then
    OnLogStr( s + ': _ONLINE_ ' );
end;

procedure TT3ServerManager.FNetServerOnClientDisConnect(const S: string);
begin
  if Assigned(OnLogStr) then
    OnLogStr( s + ': _OFFLINE_ ' );
end;

procedure TT3ServerManager.FNetworkThread_OnRunning(const dt: double);
begin
  inherited;
end;

procedure TT3ServerManager.GamePause;
begin
  inherited;
end;

procedure TT3ServerManager.GameStart;
begin
  inherited;

end;

procedure TT3ServerManager.GameTerminate;
begin
  inherited;

end;

function TT3ServerManager.getBroadcastData: boolean;
begin
  result := FNetSender.Enabled;
end;

procedure TT3ServerManager.InitNetwork;
begin
  inherited;

  FNetSender.DCounter := 0;
  FNetSender.DInterval := vNetServerSetting.SendInterval;

//  TNetHandle_Server(nethandle).RegisterTCPMapStubPacket(CPID_CMD_MAPS, SizeOf(TRec_MapData),
//    PacketReceive);

  TNetHandle_Server(nethandle).ConnectToSessionServer;
  TNetHandle_Server(nethandle).StartListen;

end;

procedure TT3ServerManager.LoadScenarioID(const vSet: TGameDataSetting);
begin
  inherited;

  FTrackManager.Reset;
  FTrackManager.LoadCubicleFromScenario(Scenario.DBCubicleGroupList,Scenario.DBCubicleAssignmentDict);

end;

procedure TT3ServerManager.NetSendUDP_BroadCast(sender: TObject);
begin
  FSendCounter := (FSendCounter + 1) mod 2; //edited by nando...

  case FSendCounter of
    0 :
    begin
      NetSendUDP_MovementData(nil);
    end;
    1 :
    begin
      netSendUDP_GameTime(nil);
    end;
  end;

end;

procedure TT3ServerManager.netSendUDP_GameTime(Sender: TObject);
var
  rGT: TRecUDP_GameTime;
begin
  rGT.ServerTime := Now;
  rGT.GameMS    := FMainVTime.GetMillisecond;
  rGT.GameTime  := FMainVTime.GetTime;
  rGT.GameStart := FMainVTime.DateTimeOffset;
  rGT.SessionID := SessionID;

  TNetServerCmdSender(FNetCmdSender).CmdGameTime(rGT);
end;

procedure TT3ServerManager.NetSendUDP_MovementData(Sender: TObject);
var
  i: Integer;
  rec: TRecUDP_PlatformMovement;
  pi: TT3PlatformInstance;
begin

  for i := 0 to FSimPlatforms.itemCount - 1 do
  begin
    pi := FSimPlatforms.getObject(i) as TT3PlatformInstance;
    rec := pi.GetMovementData;
    rec.SessionID := SessionID;

    // Nando Added for Flag in Voip,,
    // Flag 1 for Vehicle, 0 for other
    if pi is TT3Vehicle then
    begin
      rec.ObjectType := 1;
    end
    else
    begin
      rec.ObjectType := 0;
    end;

    TNetServerCmdSender(FNetCmdSender).CmdPlatformPropertiesUpdate(rec);
  end;
end;

procedure TT3ServerManager.netSendUDP_TrackSync(Sender: TObject);
var
  i,j : integer;
begin

  { utk menghemat trafic, seharusnya track yg dikirim hanya cubicle yg aktif saja }
  { send to controller, hanya track NRT,  }
  { send to cubicle, hanya cubicle yg aktif }
  { ini semua dibroadcast dlu }
  if Assigned(FTrackManager) then
  begin
    FTrackManager.BroadCast;
  end;
end;

//procedure TT3ServerManager.NetSessionOnConnectedRequestCubicle(sender: TObject);
//var r: TRecRequest;
//begin
//  r.RequestID := CREQID_ALL_CUB_ASSIGN;
//  TNetHandle_Server(nethandle).SendRequestToSession(CPID_REQ, @r);
//end;

procedure TT3ServerManager.OnIFFSensorDetect(aSensor, aTarget: TObject;
  value, value2: boolean; mode1, mode2, mode3, mode3C: string);
begin
//  if aTarget is TT3PlatformInstance then
//  begin
//    FTrackManager.UpdateIFFTrack(
//      TT3MountedSensor(aSensor),
//      TT3PlatformInstance(aTarget),
//      value, value2, mode1, mode2, mode3, mode3C);
//  end;
end;

procedure TT3ServerManager.onTrackDetected(Sender, detected: TObject;
  Value: boolean);
begin
  { track manager here }
  if Value then
    FTrackManager.UpdateTrack(TT3MountedSensor(Sender),TT3Unit(detected),utAdd)
  else
    FTrackManager.UpdateTrack(TT3MountedSensor(Sender),TT3Unit(detected),utRemove)
end;

procedure TT3ServerManager.PacketReceive(apRec: PAnsiChar; aSize: word);
var
  id             : integer;
  ipTo           : string;

  rec            : ^TRecSynch_Vehicle;
  rCmdTCPRequest : ^TRecTCP_Request;
  rCmdLaunchRP   : ^TRecCmd_LaunchRP;

begin
  inherited;

  rec := @apRec^;

  if rec = nil then
    Exit;

  if rec^.SessionID <> SessionID then
    Exit;

  id   := rec^.pid.recID;
  ipTo := LongIp_To_StrIp(rec^.pid.ipSender);

  case id of

    CPID_CMD_LAUNCH_RUNTIME_PLATFORM :
    begin
      rCmdLaunchRP := @apRec^;
      CmdLaunchRuntimePlatform(rCmdLaunchRP^);
    end;

    CPID_TCP_REQUEST :
    begin
      rCmdTCPRequest := @apRec^;
      CmdTCPRequest(rCmdTCPRequest^,ipTo);
    end;

  end;

end;

procedure TT3ServerManager.ResetScenario;
begin
  inherited;

  FTrackManager.Reset;
end;

procedure TT3ServerManager.setBroadcastData(const Value: boolean);
begin
  FNetSender.Enabled := Value;
end;

procedure TT3ServerManager.setGameSpeed(const Value: double);
begin
  inherited;

  if Assigned(FOnUpdateGameInfo) then
    FOnUpdateGameInfo(byte(FGameState), GameSPEED);

end;

procedure TT3ServerManager.SetonUpdateFormTrack(const Value: TOnUpdateFormTrack);
begin
  FonUpdateFormTrack := Value;
end;

procedure TT3ServerManager.SetOnUpdateGameInfo(const Value: TOnUpdateGameInfo);
begin
  FOnUpdateGameInfo := Value;
end;

procedure TT3ServerManager.StopNetwork;
begin
  inherited;

end;

procedure TT3ServerManager.updateForm(Sender: TObject);
begin

  if Assigned(FOnUpdateGameInfo) then
    FOnUpdateGameInfo(byte(FGameState), GameSPEED);

  if Assigned(FonUpdateFormTrack) then
    FonUpdateFormTrack(FTrackManager);
end;

end.
