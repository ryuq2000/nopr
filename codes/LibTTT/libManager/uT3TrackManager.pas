unit uT3TrackManager;

interface

uses Generics.Collections, uT3Track, uT3MountedSensor, uT3PlatformInstance,
  uDBClassDefinition, Classes, tttData, uGameData_TTT, uT3PointTrack, uSimObjects,
  SysUtils, uT3Unit, uT3CubicleGroup;

{ collection cubicle tracks -> tracks came from sensor or own track group }
type

  TArrTracks =  array [0..100] of TRecTrack;
  TUpdateTrackOpt = (utAdd, utRemove);

  TCubicleTrackDict = TObjectDictionary< { group index } integer,{ list of tracks } TT3CubicleGroup>;
  TCubicleTrackPair = TPair<{ group index } integer,{ list of tracks } TT3CubicleGroup>;

  TT3TrackManager = class
  private

    FCubiclesTrack         : TCubicleTrackDict;

    { rec prepare to sync with client }
    FRecTrack              : TArrTracks;

    { if client role }
    FMyGroupID             : integer;

    { fix number for track id }
    FLastTrackID           : integer;

    FLastIdx               : integer;
    FNeedUpdate            : Boolean;

    FOnTracksNeedUpdatePos : TNotifyEvent;
    FOnSensorTrackAdded    : TOnSensorTrackAdded;
    FOnSensorTrackRemoved  : TOnSensorTrackRemoved;

    function AddTrack(sensor: TT3MountedSensor; pfObject: TT3Unit): TT3Track;
    function RemoveTrack(sensor: TT3MountedSensor;pfObject: TT3Unit) : TT3Track;
    function GroupByPlatformIndex(pid : integer) : TT3CubicleGroup;

    procedure SetOnSensorTrackAdded(const Value: TOnSensorTrackAdded);
    procedure SetOnSensorTrackRemoved(const Value: TOnSensorTrackRemoved);

    procedure initRecGroupTrack;
    procedure initCubiclesTracks(cubicleasign : TDBCubicleAssignmentDict);
    procedure initCubicleGroup(cubicleGrp: TDBCubicleGroupList);
    procedure initSensorTrack(track : TT3Track; sensor : TT3MountedSensor);

    procedure updateBearingTrack(track : TT3Track; sensor : TT3MountedSensor; detectTime : TDateTime);

    procedure prepareRecord;
  public
    constructor Create;
    destructor Destroy;override;

    procedure Reset;
    procedure Move(const aDt: Double);
    procedure LoadCubicleFromScenario(cubicleGrp: TDBCubicleGroupList; cubicleTracks : TDBCubicleAssignmentDict);
//    function PlatformIndexExist(tn : integer) : TT3CubicleGroup;

    procedure BroadCast;

    { add remove track from sensor here }
    function UpdateTrack(sensor: TT3MountedSensor; pfObject: TT3Unit; Option : TUpdateTrackOpt = utAdd) : TT3Track;
    procedure UpdateIFFTrack(sensor: TT3MountedSensor; pfObject: TSimObject;
      value, value2: boolean; mode1, mode2, mode3, mode3C: string );
    procedure RemoveTracks(sensor: TT3MountedSensor);

    { ro props }
    property CubicleTracks : TObjectDictionary< integer, TT3CubicleGroup> read FCubiclesTrack;
    property RecTracks : TArrTracks read FRecTrack;

    { event }
    property OnSensorTrackAdded
      : TOnSensorTrackAdded   read FOnSensorTrackAdded   write
      SetOnSensorTrackAdded;
    property OnSensorTrackRemoved
      : TOnSensorTrackRemoved read FOnSensorTrackRemoved write
      SetOnSensorTrackRemoved;

  end;

implementation

uses uT3ObjectFactory, uGlobalVar,
  uT3MountedRadar, uT3MountedVisual,
  uT3MountedSonar, uT3MountedESM,
  uT3MountedIFF, uT3MountedEO,
  uSensorInfo, uT3BearingTrack,
  uBaseCoordSystem, uT3Vehicle;

{ TT3TrackManager }

function TT3TrackManager.AddTrack(sensor: TT3MountedSensor;
  pfObject: TT3Unit):TT3Track;
var
  trackpair            : TPair<integer, TT3Track>;
  tracks               : TT3CubicleGroup;
  track                : TT3Track;
  found, searchBearing : Boolean;
  sensorInfo           : TSensorTrackInfo;
begin
  track := nil;

  { cari sensor dari group mana }
  { get list of tracks }
  tracks :=   GroupByPlatformIndex(sensor.PlatformParentInstanceIndex);
  if not Assigned(tracks) then
    Exit;

//  { should use lock thread ? }
  MonitorEnter(FCubiclesTrack);

  try
    { TODO : sementara klo IFF ga masuk ke detectby, utk test }
    if sensor.SensorType in [stIFF, stSonobuoy] then
      Exit;

    searchBearing := False;
    if sensor is TT3MountedESM then
      searchBearing := True;

    found := False;
    { search track in list }
    for trackpair in tracks.Tracks do
    begin

      if (searchBearing and (trackpair.Value.TrackType = trBearing)) then
      begin
        { if bearing search by Platformindex and sensorindex}
        if pfObject is TT3MountedSensor then
        begin
          found := (trackpair.Value.ObjectInstanceIndex =
            TT3MountedSensor(pfObject).PlatformParentInstanceIndex);
          found := found and
            (TT3BearingTrack(trackpair.Value).ObjectSensorId = pfObject.InstanceIndex);
        end;
      end
      else
      if((not searchBearing) and (trackpair.Value.TrackType = trPoint)) then
        { if point, search by Platformindex only}
        found := trackpair.Value.ObjectInstanceIndex = pfObject.InstanceIndex
      else
        continue;

      if found then
      begin
        track := trackpair.Value;

        case track.TrackType of
          trBearing      :
          begin
            { if esm update detect time relatif to bearing radar emiter }
            if sensor is TT3MountedESM then
              updateBearingTrack(track,sensor,simManager.MainVTime.GetTime);
          end;
          trAOP          : ;
          trPoint        : track.addDetectBy(sensor, simManager.MainVTime.GetTime, sensorInfo);
          trSpecialPoint : ;
        end;

        break;
      end;
    end;

    { if not found then create one }
    if not found then
    begin
      if sensor.SensorType in [stRadar,stSonar,stEO,stVisual,stMAD] then
        track := simManager.ObjectFactory.createTrack(trPoint, pfObject)
      else
      if sensor.SensorType = stESM then
        track := simManager.ObjectFactory.createTrack(trBearing, pfObject);

      if Assigned(track) then
      begin
        track.addDetectBy(sensor, simManager.MainVTime.GetTime, sensorInfo);
        track.UniqueID := FLastTrackID;

        {customize track props before add to list}
        initSensorTrack(track,sensor);

        tracks.AddTrack(track);

        Inc(FLastTrackID);

        if Assigned(OnSensorTrackAdded) then
          OnSensorTrackAdded(track, sensor);
      end;
    end;

  finally
    result := track;
    MonitorExit(FCubiclesTrack);
  end;

end;

procedure TT3TrackManager.BroadCast;
begin

  if not FNeedUpdate  then
  begin

    if (FLastIdx >= Length(FRecTrack)) then
      FNeedUpdate := True;

    if (not FNeedUpdate) and (FRecTrack[FLastIdx].TrackGroup =  -1) then
      FNeedUpdate := True;

    if (not FNeedUpdate) and (machineRole = crServer) then
      simManager.NetCmdSender.CmdSynTrack(FRecTrack[FLastIdx]);

    Inc(FLastIdx);

  end;

  if FNeedUpdate  then
  begin
    prepareRecord;
    FLastIdx := 0;
    FNeedUpdate := False;
  end;

end;

constructor TT3TrackManager.Create;
begin
  FLastTrackID := 1;
  FLastIdx := 0;

  FCubiclesTrack := TCubicleTrackDict.Create([doOwnsValues]);

end;

destructor TT3TrackManager.Destroy;
begin
  FCubiclesTrack.Free;
  inherited;
end;

function TT3TrackManager.GroupByPlatformIndex(pid: integer): TT3CubicleGroup;
var
  item : TCubicleTrackPair;
begin
  Result := nil;
  { cari group berdasar track tipe point dan can controlled by operator }
  for item in FCubiclesTrack do
  begin
    if item.Value.isPlatformIDMemberCubicle(pid) then
    begin
      result := item.Value;
      Break;
    end;
  end;
end;

procedure TT3TrackManager.initCubicleGroup(cubicleGrp: TDBCubicleGroupList);
var
  i: integer;
  dbGrp: TDBCubicle_Group;
  grp : TT3CubicleGroup;
begin
  for dbGrp in cubicleGrp do
  begin
    { add cubicle to dict }
    if dbGrp <> nil then
    begin
      FCubiclesTrack.Add(dbGrp.Group_Index, TT3CubicleGroup.Create(dbGrp));
    end;
  end;

  // add controller
  grp := TT3CubicleGroup.Create;
  grp.GroupIndex := 0;
  FCubiclesTrack.Add(0, grp);

  // add unassigned group -> group platform that dont have a grup
  grp := TT3CubicleGroup.Create;
  grp.GroupIndex := 1;
  FCubiclesTrack.Add(1, grp);

end;


procedure TT3TrackManager.initCubiclesTracks(cubicleasign : TDBCubicleAssignmentDict);
var
  cub : TPair<Integer, TT3CubicleGroup>;
  dbAssignment : TDBPairCubicleAssignmentDict;
  dbPlatform   : TDBCubicle_Group_Assignment;
  grp : TT3CubicleGroup;
  j : integer;
  Pi: TT3PlatformInstance;
  track: TT3Track;
  trackPair : TPair<integer, TT3Track>;
  found : Boolean;
begin
  for dbAssignment in cubicleasign do
  begin
    if Assigned(dbAssignment.Value) then
    begin

      FCubiclesTrack.TryGetValue(dbAssignment.Key,grp);
      if Assigned(grp) then
      begin
        for dbPlatform in dbAssignment.Value do
        begin
          Pi := simManager.FindT3PlatformByID(dbPlatform.Platform_Instance_Index);

          if not Assigned(Pi) then
            continue;

          track := simmanager.ObjectFactory.createTrack(trPoint, Pi);
          track.UniqueID := FLastTrackID;

          grp.AddTrack(track);

          Inc(FLastTrackID);

          { track is group platform }
          if track is TT3PointTrack then
          begin
            TT3PointTrack(track).isAlwaysTracking := True;
            TT3PointTrack(track).CanControl := True;
          end;

        end;
      end;

    end;

  end;

  // check platform that doesnt have group
  for j := 0 to simmanager.SimPlatforms.itemCount - 1 do
  begin
    pi := simmanager.SimPlatforms.getObject(j) as TT3PlatformInstance;
    found := False;

    for cub in FCubiclesTrack do
    begin
      if (cub.Key = 0) or (cub.Key = -1) then
        continue;

      for trackPair in cub.Value.Tracks do
      begin
        if pi.InstanceIndex = trackPair.Value.ObjectInstanceIndex  then
        begin
          found := True;
          break;
        end;
      end;

      if found then
        break;
    end;

    if not found then // add to unasigned group
    begin
      FCubiclesTrack.TryGetValue(1,grp);
      if Assigned(grp) then
      begin

        track := simManager.ObjectFactory.createTrack(trPoint, Pi);
        track.UniqueID := FLastTrackID;

        grp.AddTrack(track);

        Inc(FLastTrackID);

        { track is group platform }
        if track is TT3PointTrack then
        begin
          TT3PointTrack(track).isAlwaysTracking := True;
          TT3PointTrack(track).CanControl := True;
        end;
      end;
    end;
  end;
end;

procedure TT3TrackManager.initRecGroupTrack;
var
  i,j,k,l,m,n : integer;
begin

  l := Length(FRecTrack);
  for i := 0 to l - 1 do
  begin
    FRecTrack[i].TrackGroup := -1; //def empty
    FRecTrack[i].TrackID := -1;
    FRecTrack[i].TrackNum := 0;
    FRecTrack[i].TrackPlatformID := 0;
    FRecTrack[i].TrackFirstDetected := 0;
    FRecTrack[i].TrackLastDetected := 0;
    FRecTrack[i].TrackSensorCount := 0;
    n := Length(FRecTrack[i].TrackSensorBy);
    for k := 0 to n - 1 do
    begin
      FRecTrack[i].TrackSensorBy[k].SensorType := 0;
      FRecTrack[i].TrackSensorBy[k].SensorID   := 0;
      FRecTrack[i].TrackSensorBy[k].SensorOwner:= 0;
      FRecTrack[i].TrackSensorBy[k].SensorFirstDetected := 0;
    end;
  end;
end;

procedure TT3TrackManager.initSensorTrack(track : TT3Track; sensor : TT3MountedSensor);
var
  EmitterRange, BlankFactor : double;
  aBearing, Range, DetectionRange, dist : double;
  pi,pi2 : TT3PlatformInstance;
  radar : TT3MountedRadar;
begin

  { init 1st time esm track }
//  if sensor.SensorType = stESM then
//  begin
//
//    pi  := queryPlatformInfo(sensor.PlatformParentInstanceIndex);
//    pi2 := queryPlatformInfo(track.ObjectInstanceIndex);
//
//    if not (Assigned(pi) and Assigned(pi2)) then
//      Exit;
//
//    aBearing := CalcBearing(pi.getPositionX, pi.getPositionY,
//      pi2.getPositionX, pi2.getPositionY);
//    dist := CalcRange(pi.getPositionX, pi.getPositionY,
//      pi2.getPositionX, pi2.getPositionY);
//
//    if pi2 is TT3Vehicle then
//    begin
//      radar := TT3Vehicle(pi2).getMountedSensor(TT3BearingTrack(track).ObjectSensorId,stRadar) as TT3MountedRadar;
//
//      if Assigned(Radar) then
//        Range := radar.DetectionRange * 1.8
//      else
//        Range := 0;
//
//      if Range = 0 then Range := INFINITE;
//
//      DetectionRange := Range * EmitterRange * BlankFactor {* ESM envi};
//      if DetectionRange <= 0 then
//        DetectionRange := 1;
//    end;
//
//    if Assigned(TT3MountedESM(sensor).ESMDefinition) then
//    begin
//      EmitterRange:= TT3MountedESM(sensor).ESMDefinition.FESM_Def.Emitter_Detect_Range_Factor;
//      BlankFactor := TT3MountedESM(sensor).ESMDefinition.FESM_Def.Sector_Blank_Detection_Factor
//    end
//    else
//    begin
//      EmitterRange:= 1;
//      BlankFactor := 1;
//    end;
//
//    TT3BearingTrack(track).RangeInterval := (1 - (dist / DetectionRange));
//    TT3BearingTrack(track).Bearing  := aBearing + TT3MountedESM(sensor).getVariance;
//    TT3BearingTrack(track).Range    := Range;
//  end;

end;

procedure TT3TrackManager.LoadCubicleFromScenario(
  cubicleGrp: TDBCubicleGroupList; cubicleTracks: TDBCubicleAssignmentDict);
begin
  { initiate groups }
  initCubicleGroup(cubicleGrp);
  { initiate group tracks }
  initCubiclesTracks(cubicleTracks);

  initRecGroupTrack;

end;

procedure TT3TrackManager.Move(const aDt: Double);
var
  pair: TPair<integer, TT3CubicleGroup>;
  track: TPair<integer, TT3Track>;
begin
  for pair in FCubiclesTrack do
    for track in pair.Value.Tracks do
      track.Value.Move(aDt);
end;

procedure TT3TrackManager.prepareRecord;
var
  i,j,k     : integer;
  cubicles  : TPair<integer, TT3CubicleGroup>;
  track     : TPair<integer, TT3Track>;
  sensor    : TPair<TT3MountedSensor,TSensorTrackInfo>;
  found: Boolean;
begin

  { should use lock thread ? }
  MonitorEnter(FCubiclesTrack);

  try

    initRecGroupTrack;

    i := 0;
    for cubicles in FCubiclesTrack do
    begin
      for track in cubicles.Value.Tracks  do
      begin
        if track.Value.UniqueID <= 0 then
          continue;

        with FRecTrack[i] do
        begin
          SessionID         := simManager.SessionID;
          TrackID           := track.Value.UniqueID;
          TrackGroup        := cubicles.Key;
          TrackNum          := track.Value.TrackNumber;
          TrackPlatformID   := track.Value.ObjectInstanceIndex;
          TrackFirstDetected:= track.Value.FirstDetected;
          TrackLastDetected := track.Value.LastDetected;
          TrackSensorCount  := track.Value.DetectedBySensors.Count;
          TrackPosX         := track.Value.PosX;
          TrackPosY         := track.Value.PosY;
          TrackPosZ         := track.Value.PosZ;
          TrackSpeed        := track.Value.DetectedSpeed;
          TrackCourse       := track.Value.DetectedCourse;
          StrLCopy(PChar(@TrackLabel[0]), PChar(track.Value.TrackLabel), High(TrackLabel));
          TrackType         := Byte(track.Value.TrackType);
          TrackCategory     := Byte(track.Value.TrackCategory);
          TrackTypeDomain   := track.Value.TrackTypeDomain;
          TrackDesignation  := track.Value.TrackDesignation;
          TrackIdentifier   := track.Value.TrackIdentifier;
          TrackDomain       := track.Value.TrackDomain;
          TrackisLostContact:= track.Value.isLostContact;

          if track.Value is TT3PointTrack then
            TrackGroupOwner := TT3PointTrack(track.Value).CanControl;

          k := 0;

          for sensor in track.Value.DetectedBySensors do
          begin
            with TrackSensorBy[k] do
            begin
              SensorType := Byte(sensor.Key.SensorType);
              SensorID   := sensor.Key.InstanceIndex;
              SensorOwner:= sensor.Key.PlatformParent.InstanceIndex;
              SensorFirstDetected := sensor.Value.FirstDetected;
              SensorLastDetected := sensor.Value.LastDetected;
              case sensor.Key.SensorType of
                {--radar info--}
                stRadar :
                begin
                  SensorDetectedSpeed  := TRadarTrackInfo(sensor.Value).DetectedSpeed;
                  SensorDetectedCourse := TRadarTrackInfo(sensor.Value).DetectedCourse;
                  SensorDetectedAltitude  := TRadarTrackInfo(sensor.Value).DetectedAltitude;
                end;
                stSonar : ;
                stESM   : ;
                stIFF   :
                begin
                  isShowIFF           := TIFFTrackInfo(sensor.Value).isShowIFF;
                  isMode4IFF          := TIFFTrackInfo(sensor.Value).isMode4IFF;
                  StrLCopy(PChar(@TransMode3CDetected[0]),
                    PChar(TIFFTrackInfo(sensor.Value).TransMode3CDetected), High(TransMode3CDetected));
                  StrLCopy(PChar(@TransMode2Detected[0]),
                    PChar(TIFFTrackInfo(sensor.Value).TransMode2Detected), High(TransMode2Detected));
                  StrLCopy(PChar(@TransMode3Detected[0]),
                    PChar(TIFFTrackInfo(sensor.Value).TransMode3Detected), High(TransMode3Detected));
                  StrLCopy(PChar(@TransMode1Detected[0]),
                    PChar(TIFFTrackInfo(sensor.Value).TransMode1Detected), High(TransMode1Detected));
                end;
                stEO    : ;
                stSonobuoy: ;
                stVisual: ;
                stMAD   : ;
              end;
            end;
            Inc(k);

          end;

        end;
        Inc(i);
      end;
    end;
  finally
    MonitorExit(FCubiclesTrack);
  end;
end;

function TT3TrackManager.RemoveTrack(sensor: TT3MountedSensor;
  pfObject: TT3Unit) : TT3Track;
var
  track: TPair<Integer, TT3Track>;
  tracks   : TT3CubicleGroup;
  found, searchBearing: Boolean;
begin
  result := nil;

  { get list of tracks }
  tracks :=   GroupByPlatformIndex(sensor.PlatformParentInstanceIndex);
  if not Assigned(tracks) then
    Exit;

//  { should use lock thread ? }
  MonitorEnter(FCubiclesTrack);

  try
    { if ESM then search bearing track }
    searchBearing := False;
    if sensor is TT3MountedESM then
      searchBearing := True;

    { search track in list }
    for track in tracks.Tracks do
    begin
      if (searchBearing and (track.Value.TrackType = trBearing)) then
      begin
        { if bearing search by Platformindex and sensorindex}
        if pfObject is TT3MountedSensor then
        begin
          found := (track.Value.ObjectInstanceIndex =
            TT3MountedSensor(pfObject).PlatformParent.InstanceIndex);
          found := found and
            (TT3BearingTrack(track.Value).ObjectSensorId = pfObject.InstanceIndex);
        end;
      end
      else
      if((not searchBearing) and (track.Value.TrackType = trPoint)) then
        { if point, search by Platformindex only}
        found := track.Value.ObjectInstanceIndex = pfObject.InstanceIndex
      else
        continue;

      if found then
      begin
        track.Value.removeDetectBy(sensor);
        result := track.Value
      end;
    end;

  finally
    MonitorExit(FCubiclesTrack);
  end;

end;

procedure TT3TrackManager.RemoveTracks(sensor: TT3MountedSensor);
var
  track: TPair<Integer, TT3Track>;
  tracks: TT3CubicleGroup;
  found: Boolean;
begin

  { get list of tracks }
  tracks :=   GroupByPlatformIndex(sensor.PlatformParentInstanceIndex);
  if not Assigned(tracks) then
    Exit;

  { should use lock thread ? }
  MonitorEnter(FCubiclesTrack);

  try
    tracks.RemoveTrack(sensor);
  finally
    MonitorExit(FCubiclesTrack);
  end;
end;

procedure TT3TrackManager.Reset;
begin

  FCubiclesTrack.Clear;
  //initCubicleGroup;

end;

procedure TT3TrackManager.SetOnSensorTrackAdded
  (const Value: TOnSensorTrackAdded);
begin
  FOnSensorTrackAdded := Value;
end;

procedure TT3TrackManager.SetOnSensorTrackRemoved
  (const Value: TOnSensorTrackRemoved);
begin
  FOnSensorTrackRemoved := Value;
end;

//function TT3TrackManager.PlatformInstanceExist(tn: integer): boolean;
//var
//  item : TPair<Integer,TT3CubicleGroup>;
//  track : TT3Track;
//  f : Boolean;
//begin
//  for item in FCubiclesTrack do
//  begin
//    f := item.Value.TrackNumberExist(tn)
//  end;
//
//
//end;

procedure TT3TrackManager.updateBearingTrack(track: TT3Track;
  sensor : TT3MountedSensor; detectTime: TDateTime);
var
  pi,pi2 : TT3PlatformInstance;
  abearing,arange, orientation : double;
  radar  : TT3MountedRadar;
  update : boolean;
  sensorInfo : TSensorTrackInfo;
begin

  if track.ObjectInstanceIndex = 0 then
    Exit;

  { update variance dari esm sensor,
    jika arah radar sejajar esm sensor }
  pi  := simManager.FindT3PlatformByID(sensor.PlatformParentInstanceIndex);
  pi2 := simManager.FindT3PlatformByID(track.ObjectInstanceIndex);

  if Assigned(pi2) then
  begin

    aBearing := CalcBearing(pi2.getPositionX, pi2.getPositionY,
      pi.getPositionX, pi.getPositionY);

    if pi2 is TT3Vehicle then
    begin
      radar := simManager.FindT3SensorOnBoardID(track.ObjectInstanceIndex,
        TT3BearingTrack(track).ObjectSensorId,1) as TT3MountedRadar;

      update := false;

      if Assigned(radar) then
      begin
        update := abs(radar.SwapDegreeRelatifToVehicle - aBearing) <= 5.0;

        if update then
        begin
          aBearing := CalcBearing(pi.getPositionX, pi.getPositionY,
            pi2.getPositionX, pi2.getPositionY);

          arange := radar.DetectionRange * 1.8;
          if arange = 0 then arange := INFINITE;

          orientation := aBearing + TT3MountedESM(sensor).getVariance;

          TT3BearingTrack(track).Bearing := orientation;
          track.addDetectBy(sensor, simManager.MainVTime.GetTime, sensorInfo);
        end;
      end;
    end;
  end;

end;

procedure TT3TrackManager.UpdateIFFTrack(sensor: TT3MountedSensor;
  pfObject: TSimObject;
  value, value2: boolean; mode1, mode2, mode3, mode3C: string );
//var
//  trackpair: TPair<integer, TT3Track>;
//  tracks: TT3CubicleGroup;
//  track: TT3Track;
//  grp: T3CubicleGroup;
//  found: Boolean;
//  sensorInfo : TSensorTrackInfo;
begin
//  // bypass dlu
//  Exit;
//
//  grp := FCubicleGrup.GetGroupOf_PlatformIndex(sensor.PlatformParent.InstanceIndex);
//  if not Assigned(grp) then
//    Exit;
//  { should use lock thread ? }
//  MonitorEnter(FCubiclesTrack);
//
//  try
//    { get list of tracks }
//    tracks := FCubiclesTrack[grp.FData.Group_Index];
//    track := nil;
//
//    if Assigned(tracks) then
//    begin
//      { search track in list }
//      for trackpair in tracks.FTracks do
//      begin
//        found := trackpair.Value.ObjectInstanceIndex = TT3PlatformInstance(pfObject).InstanceIndex;
//        if found then
//        begin
//          track := trackpair.Value;
//          track.addDetectBy(sensor, simManager.MainVTime.GetTime, sensorInfo);
//          if Assigned(sensorInfo) and (sensorinfo is TIFFTrackInfo) then
//            with TIFFTrackInfo(sensorInfo) do
//            begin
//              isShowIFF := value;
//              isMode4IFF := value2;
//              TransMode1Detected := mode1;
//              TransMode2Detected := mode2;
//              TransMode3Detected := mode3;
//              TransMode3CDetected:= mode3C;
//            end;
//
//          break;
//        end;
//      end;
//    end;
//  finally
//    MonitorExit(FCubiclesTrack);
//  end;

end;

function TT3TrackManager.UpdateTrack(sensor: TT3MountedSensor;
  pfObject: TT3Unit; Option: TUpdateTrackOpt) : TT3Track;
begin
  result := nil;
  case Option of
    utAdd   : result := AddTrack(sensor,pfObject) ;
    utRemove: RemoveTrack(sensor,pfObject);
  end;
end;

end.
