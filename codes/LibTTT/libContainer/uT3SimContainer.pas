unit uT3SimContainer;

interface

uses uSimContainers, uSimObjects;

type
  TT3SimContainer = class (TSimContainer)
  private
  protected
  public
    constructor Create;
    destructor  Destroy; override;

    procedure Move (deltaMs : double);

  end;

implementation

{ TT3SimContainer }

constructor TT3SimContainer.Create;
begin
  inherited;
end;

destructor TT3SimContainer.Destroy;
begin

  inherited;
end;

procedure TT3SimContainer.Move(deltaMs: double);
var
  i, count : integer;
  obj : TObject;
begin

  count := ItemCount;

  for I := 0 to count - 1 do
  begin
    obj := getObject(I);
    if Assigned(obj) and (obj is TSimObject) then
      TSimObject(obj).Move(deltaMs);
  end;

end;

end.
