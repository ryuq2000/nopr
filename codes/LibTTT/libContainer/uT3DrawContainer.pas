unit uT3DrawContainer;

interface

uses uSimContainers, uSimVisuals, Graphics, uCoordConvertor,
  Windows;

type
  TT3DrawContainer = class (TSimContainer)
  private
  public

    constructor Create;
    destructor  Destroy; override;

    { 1. update, }
    procedure Update;
    { 2. convertcoord, }
    procedure ConvertCoord(cvt: TCoordConverter);
    { 3. then draw }
    procedure Draw(aCnv: TCanvas);

    { all at once }
    procedure UpdateAndDraw(cvt: TCoordConverter;aCnv: TCanvas);
  end;

implementation

{ TT3DrawContainer }

procedure TT3DrawContainer.ConvertCoord(cvt: TCoordConverter);
var
  i, count : integer;
  obj : TObject;
begin
  count := ItemCount;

  for I := 0 to count - 1 do
  begin
    obj := getObject(I);
    if Assigned(obj) and (obj.ClassParent = TDrawElement) then
      TDrawElement(obj).ConvertCoord(cvt);
  end;
end;

constructor TT3DrawContainer.Create;
begin
  inherited;

end;

destructor TT3DrawContainer.Destroy;
begin

  inherited;
end;

procedure TT3DrawContainer.Draw(aCnv: TCanvas);
var
  i, count : integer;
  obj : TObject;
begin
  count := ItemCount;

  SetBkMode(aCnv.Handle, TRANSPARENT);

  for I := 0 to count - 1 do
  begin
    obj := getObject(I);
    if Assigned(obj) and (obj.ClassParent = TDrawElement) then
      TDrawElement(obj).Draw(aCnv);
  end;
end;

procedure TT3DrawContainer.Update;
var
  i, count : integer;
  obj : TObject;
begin
  count := ItemCount;

  for I := 0 to count - 1 do
  begin
    obj := getObject(I);
    if Assigned(obj) and (obj.ClassParent = TDrawElement) then
      TDrawElement(obj).Update;
  end;
end;

procedure TT3DrawContainer.UpdateAndDraw(cvt: TCoordConverter; aCnv: TCanvas);
var
  i, count : integer;
  obj : TObject;
begin
  count := ItemCount;

  SetBkMode(aCnv.Handle, TRANSPARENT);

  for I := 0 to count - 1 do
  begin
    obj := getObject(I);
    if Assigned(obj) and (obj.ClassParent = TDrawElement) then
    begin

      //if not TDrawElement(obj).Visible then Continue;

      TDrawElement(obj).Update;
      TDrawElement(obj).ConvertCoord(cvt);
      TDrawElement(obj).Draw(aCnv);

    end;
  end;

end;

end.
