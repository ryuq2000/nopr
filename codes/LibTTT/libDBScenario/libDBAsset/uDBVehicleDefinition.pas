unit uDBVehicleDefinition;

interface

{*------------------------------------------------------------------------------
  Kelas untuk database

  @author  Rizky
  @version 2015/12/18 1.0 Initial revision.
  @todo
  @comment
-------------------------------------------------------------------------------}

type

  TDBVehicle_Definition =  class
  public
    Vehicle_Index                      : integer;
    Vehicle_Identifier                 : string[60];
    Platform_Domain                    : byte;
    Platform_Category                  : byte;
    Platform_Type                      : byte;
    Motion_Characteristics             : integer;
    Length                             : single;
    Width                              : single;
    Height                             : single;
    Draft                              : single;
    Front_Radar_Cross                  : single;
    Side_Radar_Cross                   : single;
    Front_Acoustic_Cross               : single;
    Side_Acoustic_Cross                : single;
    Magnetic_Cross                     : single;
    Front_Visual_EO_Cross              : single;
    Side_Visual_EO_Cross               : single;
    Front_Infrared_Cross               : single;
    Side_Infrared_Cross                : single;
    LSpeed_Acoustic_Intens             : single;
    Below_Cav_Acoustic_Intens          : single;
    Above_Cav_Acoustic_Intens          : single;
    HSpeed_Acoustic_Intens             : single;
    Cavitation_Speed_Switch            : single;
    Time_of_Weapon_Impact              : integer;
    Chaff_Seduction_Capable            : ByteBool;
    Seduction_Mode_Prob                : single;
    Min_Delay_Between_Chaff_Rounds     : integer;
    Max_Chaff_Salvo_Size               : byte;
    SARH_POH_Modifier                  : single;
    CG_POH_Modifier                    : single;
    TARH_POH_Modifier                  : single;
    IR_POH_Modifier                    : single;
    AR_POH_Modifier                    : single;
    Active_Acoustic_Tor_POH_Mod        : single;
    Passive_Acoustic_Tor_POH_Mod       : single;
    Active_Passive_Tor_POH_Mod         : single;
    Wake_Home_POH_Modifier             : single;
    Wire_Guide_POH_Modifier            : single;
    Mag_Mine_POH_Modifier              : single;
    Press_Mine_POH_Modifier            : single;
    Impact_Mine_POH_Modifier           : single;
    Acoustic_Mine_POH_Modifier         : single;
    Sub_Comm_Antenna_Height            : single;
    Rel_Comm_Antenna_Height            : single;
    Max_Comm_Operating_Depth           : single;
    HF_Link_Capable                    : ByteBool;
    UHF_Link_Capable                   : ByteBool;
    HF_Voice_Capable                   : ByteBool;
    VHF_Voice_Capable                  : ByteBool;
    UHF_Voice_Capable                  : ByteBool;
    SATCOM_Voice_Capable               : ByteBool;
    UWT_Voice_Capable                  : ByteBool;
    HF_MHS_Capable                     : ByteBool;
    UHF_MHS_Capable                    : ByteBool;
    SATCOM_MHS_Capable                 : ByteBool;
    Damage_Capacity                    : integer;
    Plat_Basing_Capability             : ByteBool;
    Chaff_Capability                   : ByteBool;
    Readying_Time                      : integer;
    Sonobuoy_Capable                   : ByteBool;
    Nav_Light_Capable                  : ByteBool;
    Periscope_Depth                    : single;
    Periscope_Height_Above_Water       : single;
    Periscope_Front_Radar_Xsection     : single;
    Periscope_Side_Radar_Xsection      : single;
    Periscope_Front_Vis_Xsection       : single;
    Periscope_Side_Vis_Xsection        : single;
    Periscope_Front_IR_Xsection        : single;
    Periscope_Side_IR_Xsection         : single;
    Engagement_Range                   : single;
    Auto_Air_Defense_Capable           : ByteBool;
    Alert_State_Time                   : single;
    Detectability_Type                 : byte;
    Max_Sonobuoys_To_Monitor           : integer;
    Sonobuoy_Deploy_Max_Altitude       : integer;
    Sonobuoy_Deploy_Min_Altitude       : integer;
    Sonobuoy_Deploy_Max_Speed          : integer;
    Air_Drop_Torpedo_Max_Altitude      : integer;
    Air_Drop_Torpedo_Min_Altitude      : integer;
    Air_Drop_Torpedo_Max_Speed         : integer;
    TMA_Rate_Factor                    : single;
    HMS_Noise_Reduction_Factor         : single;
    TAS_Noise_Reduction_Factor         : single;
    Infrared_Decoy_Capable             : ByteBool;
    HF_Mid_Course_Update_Capable       : ByteBool;
    UHF_Mid_Course_Update_Capable      : ByteBool;
    SATCOM_Mid_Course_Update_Capable   : ByteBool;
  end;

implementation

end.
