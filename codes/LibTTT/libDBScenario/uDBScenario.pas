unit uDBScenario;

interface

uses
   Classes, uDBClassDefinition,
   tttData, uDMLite,
   Generics.Collections,
   Windows,
   uLibSettingTTT ;
type

  TT3CubicleChanel = class
  private
  public
    GroupID : integer;
    GroupName : string;
    ListExternalChannel : TList;
    constructor Create;
    destructor Destroy; override;
  end;

  {*------------------------------------------------------------------------------
    Kelas TT3DBScenario hanya berisi informasi awal scenario permainan.
    Data yang ada sifatnya (seharusnya) tetap, tidak diubah waktu permainan.
  -------------------------------------------------------------------------------}

  TT3DBScenario = class
  private
    FOnAssignedPlatform: TNotifyEvent;
    FOnGetExternalCom: TNotifyEvent;
//    FDefaultNonRealPlatform: TPlatform_Instance;

    {*------------------------------------------------------------------------------
      Resource alloc and scenario definition object
    -------------------------------------------------------------------------------}
    FDBResource_Allocation : TDBResource_Allocation;
    FDBScenario_Definition : TDBScenario_Definition;
    FDBAsset_Deployment_Definition : TDBAsset_Deployment_Definition;

    {*------------------------------------------------------------------------------
      definisi data, unik untuk setiap record,
      setiap instance akan mengacu pada data definisi disini,
      sifat data (harusnya) ga boleh diubah ubah.
    -------------------------------------------------------------------------------}
    FDBAcoustic_Decoy_Definition     : TDBAcousticDecoyDict;
    FDBAir_Bubble_Definition         : TDBAirBubbleDict;
    FDBBomb_Definition               : TDBBombDefinitionDict;
    FDBChaff_Definition              : TDBChaffDefinitionDict;
    FDBDefensive_Jammer_Definition   : TDBDefensiveJammerDefinitionDict;
    FDBEO_Detection_Definition       : TDBEODetectionDefinitionDict;
    FDBESM_Definition                : TDBESMDefinitionDict;
    FDBFCR_Definition                : TDBFCRDefinitionDict;
    FDBFloating_Decoy_Definition     : TDBFloatingDecoyDefinitionDict;
    FDBGun_Definition                : TDBGunDefinitionDict;
    FDBHybrid_Definition             : TDBHybridDefinitionDict;
    FDBInfrared_Decoy_Definition     : TDBInfraredDecoyDefinitionDict;
    FDBJammer_Definition             : TDBJammerDefinitionDict;
    FDBMAD_Definition                : TDBMADDefinitionDict;
    FDBMine_Definition               : TDBMineDefinitionDict;
    FDBMissileDefinition             : TDBMissileDefinitionDict;
    FDBRadar_Definition              : TDBRadarDefinitionDict;
    FDBSatellite_Definition          : TDBSatelliteDefinitionDict;
    FDBSonar_Definition              : TDBSonarDefinitionDict;
    FDBSonobuoy_Definition           : TDBSonobuoyDefinitionDict;
    FDBTorpedo_Definition            : TDBTorpedoDefinitionDict;
    FDBTowed_Jammer_Decoy_Definition : TDBTowedJammerDecoyDefDict;
    FDBVehicleDefinition             : TDBVehicleDefinitionDict;            {list of vehicle definition}
    FDBPlatformActivation            : TDBPlatformActivationDict;           {list of platform activation}
    FDBHelicopterLandLaunchLimits    : TDBHelicopterLandLaunchLimitsDict;   {list of heli launch limits}
    FDBMotionCharacteristics         : TDBMotionCharacteristicsDict;        {list of motion chars}

    FDBMine_POD_vs_RangeDict         : TDBMine_POD_vs_RangeDict;            {list of pod mine}
    FDBTorpedo_POH_ModifierDict      : TDBTorpedo_POH_ModifierDict;         {list of poh torpedo}
    FDBSonar_POD_CurveDict           : TDBSonar_POD_CurveDict;              {list of Sonar_POD_Curve}
    FDBRadar_Vertical_CovDict        : TDBRadar_Vertical_CovDict;

    FDBExtCommChannelGroupDict       : TDBExtCommChannelGroupDict;          {list of cubicle channel assignment}

    {*------------------------------------------------------------------------------
      Blindzone for each devices
    -------------------------------------------------------------------------------}
    FDBBlindZoneFCRDict              : TDBBlindZoneDict     ;
    FDBBlindZoneESMDict              : TDBBlindZoneDict     ;
    FDBBlindZoneEODict               : TDBBlindZoneDict      ;
    FDBBlindZoneVisualDict           : TDBBlindZoneDict  ;
    FDBBlindZonePointDict            : TDBBlindZoneDict   ;
    FDBBlindZoneFittedDict           : TDBBlindZoneDict  ;
    FDBBlindZoneSonarDict            : TDBBlindZoneDict   ;
    FDBBlindZoneRadarDict            : TDBBlindZoneDict   ;

    {*------------------------------------------------------------------------------
      Formation list and formation members
    -------------------------------------------------------------------------------}
    FDBFormationList                 : TDBFormationList;                    {list of formation instance}
    FDBFormationMembersDict          : TDBFormationAssigmentDict;           {list of members of formation}

    {*------------------------------------------------------------------------------
      Definisi data on board device,
    -------------------------------------------------------------------------------}
    FDBEOOnBoardDict                 : TDBEOOnBoardDict;                    {list of EO onboard for each vehicle}
    FDBESMOnBoardDict                : TDBESMOnBoardDict;                   {list of ESM onboard for each vehicle}
    FDBIFFOnBoardDict                : TDBIFFSensorOnBoardDict;             {list of IFF onboard for each vehicle}
    FDBMADOnBoardDict                : TDBMADSensorOnBoardDict;             {list of MAD onboard for each vehicle}
    FDBFCROnBoardDict                : TDBFCROnBoardDict;                   {list of FCR onboard for each vehicle}
    FDBVisualOnBoardDict             : TDBVisualOnBoardDict;                {list of Visual onboard for each vehicle}
    FDBRadarOnBoardDict              : TDBRadarOnBoardDict;                 {list of radar onboard for each vehicle}
    FDBSonarOnBoardDict              : TDBSonarOnBoardDict;                 {list of sonar onboard for each vehicle}
    FDBWeaponOnBoardDict             : TDBFitWeaponOnBoardDict;             {list of fitted weapon (missile,torpedo,mine) onboard for each vehicle}
    FDBFitWeaponLauncherOnBoardDict  : TDBFitWeaponLauncherOnBoardDict;     {list of fitted weapon launcher}
    FDBPointEffectOnBoardDict        : TDBPointEffectOnBoardDict;           {list of point effect (gun,bomb) onboard for each vehicle}
    FDBAcousticDecoyOnBoardDict      : TDBAcousticDecoyOnBoardDict;         {list of acoustic decoy onboard for each vehicle}
    FDBAirBubbleOnBoardDict          : TDBAirBubbleOnBoardDict;             {list of air bubble onboard for each vehicle}
    FDBChaffOnBoardDict              : TDBChaffOnBoardDict;                 {list of chaff onboard for each vehicle}
    FDBChaffLauncherOnBoardDict      : TDBChaffLauncherOnBoardDict;         {list of chaff launcher onboard for each vehicle}
    FDBDefensiveJammerOnBoardDict    : TDBDefensiveJammerOnBoardDict;       {list of def jammer onboard for each vehicle}
    FDBFloatingDecoyOnBoardDict      : TDBFloatingDecoyOnBoardDict;         {list of float decoy onboard for each vehicle}
    FDBInfraredDecoyOnBoardDict      : TDBInfraredDecoyOnBoardDict;         {list of IR decoy onboard for each vehicle}
    FDBJammerOnBoardDict             : TDBJammerOnBoardDict;                {list of jammer onboard for each vehicle}
    FDBTowedJammerOnBoardDict        : TDBTowedJammerOnBoardDict;           {list of towed jammer onboard for each vehicle}
    FDBSonobuoyOnBoardDict           : TDBSonobuoyOnBoardDict;              {list of sonobuoy onboard for each vehicle}

    {*------------------------------------------------------------------------------
      List of platform instance
    -------------------------------------------------------------------------------}
    FDBPlatformInstance              : TDBPlatformInstanceDict;     {list of platform instance loaded from DB}

    {*------------------------------------------------------------------------------
      Game Default, etc dari scenario
    -------------------------------------------------------------------------------}
    FDBGameDefaults        : TDBGame_Defaults;
    FDBGameCloud_ESM       : TDBGameCloudOnESMList;
    FDBGameCloud_Radar     : TDBGameCloudRadarList;
    FDBGameDefault_IFF     : TDBGameDefaultIFFModeList;
    FDBGameRainfall_ESM    : TDBGameRainfallOnESMList;
    FDBGameRainfall_Missile: TDBGameRainfallOnMissSeekerList;
    FDBGameRainfall_Radar  : TDBGameRainfallOnRadarList;
    FDBGameRainfall_Sonar  : TDBGameRainfallOnSonarList;
    FDBGameSea_Sonar       : TDBGameSeaOnSonarlist;
    FDBGameShip_Sonar      : TDBGameShipOnSonarList;
    FDBGameSea_Missile     : TDBGame_Sea_On_Missile_Safe_Altitude;
    FDBGameSea_Radar       : TDBGame_Sea_On_Radar;

    FDBGameEnvironment     : TDBGame_Environment_Definition;
    FDBGameAreaDefinition  : TDBGame_Area_Definition;
    FDBSubAreaDefinitions  : TDBSubAreaDefinitionList;
    FDBOverlayMappingList  : TDBOverlayMappingList;
    FDBExtCommChannelList  : TDBExtCommChannelList;

    {*------------------------------------------------------------------------------
      Scenario Links and it Participants
    -------------------------------------------------------------------------------}
    FDBLinkDefinitionList   : TDBLinkDefinitionList;
    FDBLinkParticipantsDict : TDBLinkParticipantsDict;

    {*------------------------------------------------------------------------------
      Cubicle group and its assignments
    -------------------------------------------------------------------------------}
    FDBCubicleGroupList      : TDBCubicleGroupList;
    FDBCubicleAssignmentDict : TDBCubicleAssignmentDict;

    {*------------------------------------------------------------------------------
      list of runtime platform
    -------------------------------------------------------------------------------}
    FDBRuntime_Platform_Library :  TDBRuntimePlatformLibraryDict;

    {*------------------------------------------------------------------------------
      list of reference point
    -------------------------------------------------------------------------------}
    FDBReferencePoints     : TDBReferencePointList;

    {*------------------------------------------------------------------------------
      Notify listener when DB object instance have to create on sim
    -------------------------------------------------------------------------------}
    FOnDBObjectInstanceCreate: TNotifyEvent;

    {*------------------------------------------------------------------------------
      Fungsi untuk load platform sensor, weapon dan countermeasure
    -------------------------------------------------------------------------------}
    procedure LoadDBPlatformSensor(pf_index : Integer);
    procedure LoadDBPlatformWeapon(pf_index : Integer);
    procedure LoadDBPlatformCountermeasure(pf_index : Integer);

    {*------------------------------------------------------------------------------
      Fungsi untuk generate instance index baru dari FDBPlatformInstance
    -------------------------------------------------------------------------------}
    function CreateInstanceIndex : integer;

    {*------------------------------------------------------------------------------
      Fungsi untuk load Formation, etc dari DB
    -------------------------------------------------------------------------------}
    procedure LoadFormation(deployIndex : integer);
    procedure LoadLinks(deployIndex : integer);
    procedure LoadCubicleGroups(deployIndex : integer);
    procedure LoadCommunicationAssignment;

    procedure SetOnAssignedPlatform(const Value: TNotifyEvent);
    procedure SetOnGetExternalCom(const Value: TNotifyEvent);
    function getDataModule: TDMLite;
    procedure SetOnDBObjectInstanceCreate(const Value: TNotifyEvent);
  protected
    function  getMemberInteger(const index: integer): Integer;
    function  getMemberString(const index: integer): string;
    function  getMemberfloat(const index: integer): double;
  public

//    RuntimePlatformLibrary : TList;
//    {added by me}
//    Resource_Overlay_Mapping : TResource_Overlay_Mapping;
//    Overlay_Definition : TOverlay_Definition;
//    Overlay_Mapping    : TList;
//    Overlay_Def        : TList;

    MapGeosetName     : string;
    pattern_mapping   : TList;

    overlayName       : string;
    allOverlayNames   : Array[0..20] of string;

//    CubChanelList : Tlist;

  public
    constructor Create;
    destructor Destroy; override;

    procedure ClearScenario;

    {*------------------------------------------------------------------------------
      Load all scenario resource from DB

      @param id Scenario ID
    -------------------------------------------------------------------------------}
    procedure LoadFromDB(const id: Integer);

    {*------------------------------------------------------------------------------
      Load add db platform asset from DB and add to dictionary

      @param pi Object to DBPlatforminstance
    -------------------------------------------------------------------------------}
    procedure LoadPlatformDefinition(pi: TDBPlatformInstance);

    {*------------------------------------------------------------------------------
      Create new DB Platform instance object, and then add to FDBPlatformInstance.

      @param inst_Id in : 0 then generate new instance index
                          not 0 then instance index platform set to this id
             pf_idx   : platform index reference defintion
             pf_type  : type of platform
             pf_force : force designation
    -------------------------------------------------------------------------------}
    function CreateNewDBPlatformInstance(var inst_Id : integer;pf_idx, pf_type, pf_force : integer;
      inst_name, trackid : String) : TDBPlatformInstance;

    procedure SetEventOnExternalComm ;

    function FindRuntimePlatform(const id: integer): TObject;

    property ScenarioIndex: Integer index 1 read  getMemberInteger;
    property ScenarioName: string   index 1 read  getMemberString;
    property ResourceAllocIndex: Integer index 2 read  getMemberInteger;
    property IdCubicle: integer index 3 read getMemberInteger;
    property OnAssignedPlatform : TNotifyEvent read FOnAssignedPlatform write SetOnAssignedPlatform;
    property OnGetExternalCom : TNotifyEvent read FOnGetExternalCom write SetOnGetExternalCom;

    //untuk default data nonreal dengan mengambil salah satu platform_instance
//    property DefaultNonRealPlatform : TPlatform_Instance read FDefaultNonRealPlatform
//              write FDefaultNonRealPlatform default nil;

    property DataModule : TDMLite read getDataModule;

    {*------------------------------------------------------------------------------
      Notify property
    -------------------------------------------------------------------------------}
    property OnDBObjectInstanceCreate : TNotifyEvent read FOnDBObjectInstanceCreate write SetOnDBObjectInstanceCreate;

    {*------------------------------------------------------------------------------
      Read Only Property
    -------------------------------------------------------------------------------}
    property DBGameEnvironment      : TDBGame_Environment_Definition       read FDBGameEnvironment;
    property DBGameDefaults         : TDBGame_Defaults                     read FDBGameDefaults;
    property DBResourceAloc         : TDBResource_Allocation               read FDBResource_Allocation;
    property DBGameAreaDefinition   : TDBGame_Area_Definition              read FDBGameAreaDefinition;
    property DBSubAreaDefinitions   : TDBSubAreaDefinitionList             read FDBSubAreaDefinitions;
    property DBGameCloud_ESM        : TDBGameCloudOnESMList                read FDBGameCloud_ESM;
    property DBGameCloud_Radar      : TDBGameCloudRadarList                read FDBGameCloud_Radar;
    property DBGameDefault_IFF      : TDBGameDefaultIFFModeList            read FDBGameDefault_IFF;
    property DBGameRainfall_ESM     : TDBGameRainfallOnESMList             read FDBGameRainfall_ESM;
    property DBGameRainfall_Missile : TDBGameRainfallOnMissSeekerList      read FDBGameRainfall_Missile;
    property DBGameRainfall_Radar   : TDBGameRainfallOnRadarList           read FDBGameRainfall_Radar;
    property DBGameRainfall_Sonar   : TDBGameRainfallOnSonarList           read FDBGameRainfall_Sonar;
    property DBGameSea_Sonar        : TDBGameSeaOnSonarlist                read FDBGameSea_Sonar;
    property DBGameShip_Sonar       : TDBGameShipOnSonarList               read FDBGameShip_Sonar;
    property DBGameSea_Missile      : TDBGame_Sea_On_Missile_Safe_Altitude read FDBGameSea_Missile;
    property DBGameSea_Radar        : TDBGame_Sea_On_Radar                 read FDBGameSea_Radar;

    property DBAcoustic_Decoy_Definition     : TDBAcousticDecoyDict              read FDBAcoustic_Decoy_Definition     ;
    property DBAir_Bubble_Definition         : TDBAirBubbleDict                  read FDBAir_Bubble_Definition         ;
    property DBBomb_Definition               : TDBBombDefinitionDict             read FDBBomb_Definition               ;
    property DBChaff_Definition              : TDBChaffDefinitionDict            read FDBChaff_Definition              ;
    property DBDefensive_Jammer_Definition   : TDBDefensiveJammerDefinitionDict  read FDBDefensive_Jammer_Definition   ;
    property DBEO_Detection_Definition       : TDBEODetectionDefinitionDict      read FDBEO_Detection_Definition       ;
    property DBESM_Definition                : TDBESMDefinitionDict              read FDBESM_Definition                ;
    property DBFCR_Definition                : TDBFCRDefinitionDict              read FDBFCR_Definition                ;
    property DBFloating_Decoy_Definition     : TDBFloatingDecoyDefinitionDict    read FDBFloating_Decoy_Definition     ;
    property DBGun_Definition                : TDBGunDefinitionDict              read FDBGun_Definition                ;
    property DBHybrid_Definition             : TDBHybridDefinitionDict           read FDBHybrid_Definition             ;
    property DBInfrared_Decoy_Definition     : TDBInfraredDecoyDefinitionDict    read FDBInfrared_Decoy_Definition     ;
    property DBJammer_Definition             : TDBJammerDefinitionDict           read FDBJammer_Definition             ;
    property DBMAD_Definition                : TDBMADDefinitionDict              read FDBMAD_Definition                ;
    property DBMine_Definition               : TDBMineDefinitionDict             read FDBMine_Definition               ;
    property DBMissileDefinition             : TDBMissileDefinitionDict          read FDBMissileDefinition             ;
    property DBRadar_Definition              : TDBRadarDefinitionDict            read FDBRadar_Definition              ;
    property DBSatellite_Definition          : TDBSatelliteDefinitionDict        read FDBSatellite_Definition          ;
    property DBSonar_Definition              : TDBSonarDefinitionDict            read FDBSonar_Definition              ;
    property DBSonobuoy_Definition           : TDBSonobuoyDefinitionDict         read FDBSonobuoy_Definition           ;
    property DBTorpedo_Definition            : TDBTorpedoDefinitionDict          read FDBTorpedo_Definition            ;
    property DBTowed_Jammer_Decoy_Definition : TDBTowedJammerDecoyDefDict        read FDBTowed_Jammer_Decoy_Definition ;
    property DBVehicleDefinition             : TDBVehicleDefinitionDict          read FDBVehicleDefinition             ;
    property DBRuntimePlatformLibrary        : TDBRuntimePlatformLibraryDict     read FDBRuntime_Platform_Library      ;

    property DBPlatformInstance           :  TDBPlatformInstanceDict     read FDBPlatformInstance         ;
    property DBEOOnBoardDict              :  TDBEOOnBoardDict            read FDBEOOnBoardDict            ;
    property DBESMOnBoardDict             :  TDBESMOnBoardDict           read FDBESMOnBoardDict           ;
    property DBIFFOnBoardDict             :  TDBIFFSensorOnBoardDict     read FDBIFFOnBoardDict           ;
    property DBMADOnBoardDict             :  TDBMADSensorOnBoardDict     read FDBMADOnBoardDict           ;
    property DBFCROnBoardDict             :  TDBFCROnBoardDict           read FDBFCROnBoardDict           ;
    property DBVisualOnBoardDict          :  TDBVisualOnBoardDict        read FDBVisualOnBoardDict        ;
    property DBRadarOnBoardDict           :  TDBRadarOnBoardDict         read FDBRadarOnBoardDict         ;
    property DBSonarOnBoardDict           :  TDBSonarOnBoardDict         read FDBSonarOnBoardDict         ;
    property DBWeaponOnBoardDict          :  TDBFitWeaponOnBoardDict     read FDBWeaponOnBoardDict        ;
    property DBPointEffectOnBoardDict     :  TDBPointEffectOnBoardDict   read FDBPointEffectOnBoardDict   ;
    property DBAcousticDecoyOnBoardDict   :  TDBAcousticDecoyOnBoardDict read FDBAcousticDecoyOnBoardDict ;
    property DBAirBubbleOnBoardDict       :  TDBAirBubbleOnBoardDict     read FDBAirBubbleOnBoardDict     ;
    property DBChaffOnBoardDict           :  TDBChaffOnBoardDict          read FDBChaffOnBoardDict           ;
    property DBChaffLauncherOnBoardDict   :  TDBChaffLauncherOnBoardDict  read FDBChaffLauncherOnBoardDict   ;
    property DBDefensiveJammerOnBoardDict :  TDBDefensiveJammerOnBoardDict read FDBDefensiveJammerOnBoardDict;
    property DBFloatingDecoyOnBoardDict   :  TDBFloatingDecoyOnBoardDict read FDBFloatingDecoyOnBoardDict    ;
    property DBInfraredDecoyOnBoardDict   :  TDBInfraredDecoyOnBoardDict read FDBInfraredDecoyOnBoardDict    ;
    property DBJammerOnBoardDict          :  TDBJammerOnBoardDict        read FDBJammerOnBoardDict           ;
    property DBTowedJammerOnBoardDict     :  TDBTowedJammerOnBoardDict   read FDBTowedJammerOnBoardDict      ;
    property DBSonobuoyOnBoardDict        :  TDBSonobuoyOnBoardDict      read  FDBSonobuoyOnBoardDict        ;

    property DBCubicleGroupList           :  TDBCubicleGroupList      read FDBCubicleGroupList      ;
    property DBCubicleAssignmentDict      :  TDBCubicleAssignmentDict read FDBCubicleAssignmentDict ;

    property DBBlindZoneFCRDict           : TDBBlindZoneDict read FDBBlindZoneFCRDict    ;
    property DBBlindZoneESMDict           : TDBBlindZoneDict read FDBBlindZoneESMDict    ;
    property DBBlindZoneEODict            : TDBBlindZoneDict read FDBBlindZoneEODict     ;
    property DBBlindZoneVisualDict        : TDBBlindZoneDict read FDBBlindZoneVisualDict ;
    property DBBlindZonePointDict         : TDBBlindZoneDict read FDBBlindZonePointDict  ;
    property DBBlindZoneFittedDict        : TDBBlindZoneDict read FDBBlindZoneFittedDict ;
    property DBBlindZoneSonarDict         : TDBBlindZoneDict read FDBBlindZoneSonarDict  ;
    property DBBlindZoneRadarDict         : TDBBlindZoneDict read FDBBlindZoneRadarDict  ;

    {*------------------------------------------------------------------------------
      Getter procedure from dictionary
    -------------------------------------------------------------------------------}
    function GetDBPlatformInstance(const inst_id            : Integer) : TDBPlatformInstance;
    function GetDBVehicleDefinition(const veh_id            : Integer) : TDBVehicle_Definition;
    function GetDBPlatformActivation(const pf_inst_id       : Integer) : TDBPlatform_Activation;
    function GetDBSonarDefinition(const snr_id              : Integer) : TDBSonar_Definition;
    function GetDBRadarDefinition(const rdr_id              : Integer) : TDBRadar_Definition;
    function GetDBEODefinition(const eo_id                  : Integer) : TDBEO_Detection_Definition;
    function GetDBESMDefinition(const esm_id                : Integer) : TDBESM_Definition;
    function GetDBMADDefinition(const mad_id                : Integer) : TDBMAD_Definition;
    function GetDBGunDefinition(const gun_id                : Integer) : TDBGun_Definition;
    function GetDBMissileDefintion(const mis_id             : Integer) : TDBMissile_Definition;
    function GetDBTorpedoDefintion(const torp_id            : Integer) : TDBTorpedo_Definition;
    function GetDBGunDefintion(const gun_id                 : Integer) : TDBGun_Definition;
    function GetDBBombDefintion(const bomb_id               : Integer) : TDBBomb_Definition;
    function GetDBMineDefintion(const mine_id               : Integer) : TDBMine_Definition;
    function GetDBChaffDefintion(const chaff_id             : Integer) : TDBChaff_Definition;
    function GetDBAcousticDefintion(const asst_id           : Integer) : TDBAcoustic_Decoy_Definition;
    function GetDBDefJammerDefintion(const asst_id          : Integer) : TDBDefensive_Jammer_Definition;
    function GetDBSelfJammerDefintion(const asst_id         : Integer) : TDBJammer_Definition;
    function GetDBInfraredDefintion(const asst_id           : Integer) : TDBInfrared_Decoy_Definition;
    function GetDBFloatingDecoyDefintion(const asst_id      : Integer) : TDBFloating_Decoy_Definition;
    function GetDBAirBubbleDefintion(const asst_id          : Integer) : TDBAir_Bubble_Definition;
    function GetDBTowedJammerDefintion(const asst_id        : Integer) : TDBTowed_Jammer_Decoy_Definition;
    function GetDBSatelliteDefinition(const sat_id          : Integer) : TDBSatellite_Definition;
    function GetDBSonoBuoyDefinition(const sono_id          : Integer) : TDBSonobuoy_Definition;

    function GetDBPlatformMotion(const motion_id            : integer) : TDBMotion_Characteristics;overload;
    function GetDBPlatformMotion(const platf_type, platf_id : integer) : TDBMotion_Characteristics;overload;

    function GetDBRadarOnBoardDef(const veh_id,rdr_inst     : Integer) : TDBRadar_On_Board;
    function GetDBSonarOnBoardDef(const veh_id,snr_inst     : Integer) : TDBSonar_On_Board;
    function GetDBVisualOnBoardDef(const veh_id,vis_inst    : Integer) : TDBVisual_Sensor_On_Board;
    function GetDBEOOnBoardDef(const veh_id,eo_inst         : Integer) : TDBEO_On_Board;
    function GetDBESMOnBoardDef(const veh_id,esm_inst       : Integer) : TDBESM_On_Board;
    function GetDBIFFOnBoardDef(const veh_id,iff_inst       : Integer) : TDBIFF_Sensor_On_Board;
    function GetDBMADOnBoardDef(const veh_id,mad_inst       : Integer) : TDBMAD_Sensor_On_Board;
    function GetDBPointEffectOnBoardDef(const veh_id,point_inst   : Integer) : TDBPoint_Effect_On_Board;
    function GetDBFittedWeaponOnBoardDef(const veh_id,fitted_inst   : Integer) : TDBFitted_Weapon_On_Board;
    function GetDBChaffOnBoardDef(const veh_id,chaff_inst   : Integer) : TDBChaff_On_Board;
    function GetDBAcousticOnBoardDef(const veh_id,asst_inst : Integer) : TDBAcoustic_Decoy_On_Board;
    function GetDBDefJammerOnBoardDef(const veh_id,asst_inst: Integer) : TDBDefensive_Jammer_On_Board;
    function GetDBJammerOnBoardDef(const veh_id,asst_inst   : Integer) : TDBJammer_On_Board;
    function GetDBInfraredOnBoardDef(const veh_id,asst_inst : Integer) : TDBInfrared_Decoy_On_Board;
    function GetDBFloatingDecoyOnBoardDef(const veh_id,asst_inst : Integer) : TDBFloating_Decoy_On_Board;
    function GetDBAirBubbleOnBoardDef(const veh_id,asst_inst : Integer) : TDBAir_Bubble_Mount;
    function GetDBTowedJammerOnBoardDef(const veh_id,asst_inst : Integer) : TDBTowed_Jammer_Decoy_On_Board;

    function GetDBBlindZones(const nzTypes                  : string; const asset_id : integer) : TDBBlindZoneDefinitionList;
    function GetDBSonarPODCurvePoints(const curve_idx       : Integer) : TDBSonar_POD_CurveList;
    function GetDBRadarVerticalCoverages(const radar_idx    : Integer) : TDBRadar_Vertical_CovList;
  end;

implementation

uses
  SysUtils, uSimContainers, ufProgress, Dialogs;
{ TT3DBScenario }


constructor TT3DBScenario.Create;
begin
  {*------------------------------------------------------------------------------
    Create scenario defintion, etc
  -------------------------------------------------------------------------------}
  FDBScenario_Definition          := TDBScenario_Definition.Create;
  FDBResource_Allocation          := TDBResource_Allocation.Create;
  FDBGameEnvironment              := TDBGame_Environment_Definition.Create;
  FDBAsset_Deployment_Definition  := TDBAsset_Deployment_Definition.Create;
  FDBGameAreaDefinition           := TDBGame_Area_Definition.Create;
  FDBGameSea_Missile              := TDBGame_Sea_On_Missile_Safe_Altitude.Create;
  FDBGameSea_Radar                := TDBGame_Sea_On_Radar.Create;
  FDBGameDefaults                 := TDBGame_Defaults.Create;
  FDBGameCloud_ESM                := TDBGameCloudOnESMList.Create;
  FDBGameCloud_Radar              := TDBGameCloudRadarList.Create;
  FDBGameDefault_IFF              := TDBGameDefaultIFFModeList.Create;
  FDBGameRainfall_ESM             := TDBGameRainfallOnESMList.Create;
  FDBGameRainfall_Missile         := TDBGameRainfallOnMissSeekerList.Create;
  FDBGameRainfall_Radar           := TDBGameRainfallOnRadarList.Create;
  FDBGameRainfall_Sonar           := TDBGameRainfallOnSonarList.Create;
  FDBGameSea_Sonar                := TDBGameSeaOnSonarlist.Create;
  FDBGameShip_Sonar               := TDBGameShipOnSonarList.Create;
  FDBSubAreaDefinitions           := TDBSubAreaDefinitionList.Create;
  FDBOverlayMappingList           := TDBOverlayMappingList.Create;
  FDBExtCommChannelList           := TDBExtCommChannelList.Create;
  FDBLinkDefinitionList           := TDBLinkDefinitionList.Create;

  {*------------------------------------------------------------------------------
    Create DB definition dictionary
  -------------------------------------------------------------------------------}
  FDBPlatformInstance             := TDBPlatformInstanceDict.Create([doOwnsValues]);
  FDBAcoustic_Decoy_Definition    := TDBAcousticDecoyDict.Create([doOwnsValues]);
  FDBAir_Bubble_Definition        := TDBAirBubbleDict.Create([doOwnsValues]);
  FDBBomb_Definition              := TDBBombDefinitionDict.Create([doOwnsValues]);
  FDBChaff_Definition             := TDBChaffDefinitionDict.Create([doOwnsValues]);
  FDBDefensive_Jammer_Definition  := TDBDefensiveJammerDefinitionDict.Create([doOwnsValues]);
  FDBEO_Detection_Definition      := TDBEODetectionDefinitionDict.Create([doOwnsValues]);
  FDBESM_Definition               := TDBESMDefinitionDict.Create([doOwnsValues]);
  FDBFCR_Definition               := TDBFCRDefinitionDict.Create([doOwnsValues]);
  FDBFloating_Decoy_Definition    := TDBFloatingDecoyDefinitionDict.Create([doOwnsValues]);
  FDBGun_Definition               := TDBGunDefinitionDict.Create([doOwnsValues]);
  FDBHybrid_Definition            := TDBHybridDefinitionDict.Create([doOwnsValues]);
  FDBInfrared_Decoy_Definition    := TDBInfraredDecoyDefinitionDict.Create([doOwnsValues]);
  FDBJammer_Definition            := TDBJammerDefinitionDict.Create([doOwnsValues]);
  FDBMAD_Definition               := TDBMADDefinitionDict.Create([doOwnsValues]);
  FDBMine_Definition              := TDBMineDefinitionDict.Create([doOwnsValues]);
  FDBMissileDefinition            := TDBMissileDefinitionDict.Create([doOwnsValues]);
  FDBRadar_Definition             := TDBRadarDefinitionDict.Create([doOwnsValues]);
  FDBSatellite_Definition         := TDBSatelliteDefinitionDict.Create([doOwnsValues]);
  FDBSonar_Definition             := TDBSonarDefinitionDict.Create([doOwnsValues]);
  FDBSonobuoy_Definition          := TDBSonobuoyDefinitionDict.Create([doOwnsValues]);
  FDBTorpedo_Definition           := TDBTorpedoDefinitionDict.Create([doOwnsValues]);
  FDBTowed_Jammer_Decoy_Definition:= TDBTowedJammerDecoyDefDict.Create([doOwnsValues]);
  FDBVehicleDefinition            := TDBVehicleDefinitionDict.Create([doOwnsValues]);
  FDBPlatformActivation           := TDBPlatformActivationDict.Create([doOwnsValues]);
  FDBHelicopterLandLaunchLimits   := TDBHelicopterLandLaunchLimitsDict.Create([doOwnsValues]);
  FDBMotionCharacteristics        := TDBMotionCharacteristicsDict.Create([doOwnsValues]);
  FDBRuntime_Platform_Library     := TDBRuntimePlatformLibraryDict.Create([doOwnsKeys,doOwnsValues]);

  FDBFormationList                := TDBFormationList.Create;
  FDBFormationMembersDict         := TDBFormationAssigmentDict.Create([doOwnsValues]);
  FDBReferencePoints              := TDBReferencePointList.Create;
  FDBLinkParticipantsDict         := TDBLinkParticipantsDict.Create([doOwnsValues]);

  FDBCubicleGroupList             := TDBCubicleGroupList.Create;
  FDBCubicleAssignmentDict        := TDBCubicleAssignmentDict.Create([doOwnsValues]);

  FDBMine_POD_vs_RangeDict        := TDBMine_POD_vs_RangeDict.Create([doOwnsValues]);
  FDBTorpedo_POH_ModifierDict     := TDBTorpedo_POH_ModifierDict.Create([doOwnsValues]);
  FDBSonar_POD_CurveDict          := TDBSonar_POD_CurveDict.Create([doOwnsValues]);
  FDBRadar_Vertical_CovDict       := TDBRadar_Vertical_CovDict.Create([doOwnsValues]);

  FDBExtCommChannelGroupDict      := TDBExtCommChannelGroupDict.Create([doOwnsValues]);

  {*------------------------------------------------------------------------------
    create on board device dictionary
  -------------------------------------------------------------------------------}
  FDBEOOnBoardDict                := TDBEOOnBoardDict.Create([doOwnsValues]);
  FDBESMOnBoardDict               := TDBESMOnBoardDict.Create([doOwnsValues]);
  FDBIFFOnBoardDict               := TDBIFFSensorOnBoardDict.Create([doOwnsValues]);
  FDBMADOnBoardDict               := TDBMADSensorOnBoardDict.Create([doOwnsValues]);
  FDBFCROnBoardDict               := TDBFCROnBoardDict.Create([doOwnsValues]);
  FDBVisualOnBoardDict            := TDBVisualOnBoardDict.Create([doOwnsValues]);
  FDBRadarOnBoardDict             := TDBRadarOnBoardDict.Create([doOwnsValues]);
  FDBSonarOnBoardDict             := TDBSonarOnBoardDict.Create([doOwnsValues]);
  FDBWeaponOnBoardDict            := TDBFitWeaponOnBoardDict.Create([doOwnsValues]);
  FDBPointEffectOnBoardDict       := TDBPointEffectOnBoardDict.Create([doOwnsValues]);
  FDBAcousticDecoyOnBoardDict     := TDBAcousticDecoyOnBoardDict.Create([doOwnsValues]);
  FDBAirBubbleOnBoardDict         := TDBAirBubbleOnBoardDict.Create([doOwnsValues]);
  FDBChaffOnBoardDict             := TDBChaffOnBoardDict.Create([doOwnsValues]);
  FDBChaffLauncherOnBoardDict     := TDBChaffLauncherOnBoardDict.Create([doOwnsValues]);
  FDBDefensiveJammerOnBoardDict   := TDBDefensiveJammerOnBoardDict.Create([doOwnsValues]);
  FDBFloatingDecoyOnBoardDict     := TDBFloatingDecoyOnBoardDict.Create([doOwnsValues]);
  FDBInfraredDecoyOnBoardDict     := TDBInfraredDecoyOnBoardDict.Create([doOwnsValues]);
  FDBJammerOnBoardDict            := TDBJammerOnBoardDict.Create([doOwnsValues]);
  FDBTowedJammerOnBoardDict       := TDBTowedJammerOnBoardDict.Create([doOwnsValues]);
  FDBSonobuoyOnBoardDict          := TDBSonobuoyOnBoardDict.Create([doOwnsValues]);
  FDBFitWeaponLauncherOnBoardDict := TDBFitWeaponLauncherOnBoardDict.Create([doOwnsValues]);

  {*------------------------------------------------------------------------------
    Create blind zones
  -------------------------------------------------------------------------------}
  FDBBlindZoneFCRDict             := TDBBlindZoneDict.Create([doOwnsValues]);
  FDBBlindZoneESMDict             := TDBBlindZoneDict.Create([doOwnsValues]);
  FDBBlindZoneEODict              := TDBBlindZoneDict.Create([doOwnsValues]);
  FDBBlindZoneVisualDict          := TDBBlindZoneDict.Create([doOwnsValues]);
  FDBBlindZonePointDict           := TDBBlindZoneDict.Create([doOwnsValues]);
  FDBBlindZoneFittedDict          := TDBBlindZoneDict.Create([doOwnsValues]);
  FDBBlindZoneSonarDict           := TDBBlindZoneDict.Create([doOwnsValues]);
  FDBBlindZoneRadarDict           := TDBBlindZoneDict.Create([doOwnsValues]);

end;

function TT3DBScenario.CreateInstanceIndex: integer;
var
  dbPF : TDBPairPlatformInstanceDict;
  i : integer;
begin
  i := 0;
  for dbPF in FDBPlatformInstance do
  begin
    if i < dbPF.Key then
      i := dbPF.Key;
  end;

  { in case platforms empty set to default }
  if i = 0 then
    i := 1000;

  result := i + 1;
end;

function TT3DBScenario.CreateNewDBPlatformInstance(var inst_Id: integer;
  pf_idx, pf_type, pf_force: integer; inst_name,trackid : String) : TDBPlatformInstance;
var
  dbPf : TDBPlatformInstance;
  idx : integer;
begin
  {generate new inst_Id if inst_id = 0}
  if inst_Id = 0 then
    inst_Id := CreateInstanceIndex;

  dbPf := TDBPlatformInstance.Create;
  with dbPf do begin
    Platform_Instance_Index := inst_Id;
    Platform_Index          := pf_idx;
    Platform_Type           := pf_type;
    Instance_Name           := inst_name;
    Track_ID                := trackid;
    ForceDesignation        := pf_force;
  end;

  { add to DB dictionary }
  FDBPlatformInstance.Add(inst_Id,dbPf);
  { add its db asset }
  LoadPlatformDefinition(dbPf);

  result := dbPf;

end;

destructor TT3DBScenario.Destroy;
var
  I: Integer;
  obj : TObject;

begin
  {*------------------------------------------------------------------------------
    freed objects : scenario defintion, etc
  -------------------------------------------------------------------------------}
  FreeAndNil(FDBScenario_Definition    );
  FreeAndNil(FDBResource_Allocation    );
  FreeAndNil(FDBGameEnvironment        );
  FreeAndNil(FDBAsset_Deployment_Definition);
  FreeAndNil(FDBGameAreaDefinition     );
  FreeAndNil(FDBGameSea_Missile        );
  FreeAndNil(FDBGameSea_Radar          );
  FreeAndNil(FDBGameDefaults           );
  FreeAndNil(FDBGameCloud_ESM          );
  FreeAndNil(FDBGameCloud_Radar        );
  FreeAndNil(FDBGameDefault_IFF        );
  FreeAndNil(FDBGameRainfall_ESM       );
  FreeAndNil(FDBGameRainfall_Missile   );
  FreeAndNil(FDBGameRainfall_Radar     );
  FreeAndNil(FDBGameRainfall_Sonar     );
  FreeAndNil(FDBGameSea_Sonar          );
  FreeAndNil(FDBGameShip_Sonar         );
  FreeAndNil(FDBSubAreaDefinitions     );
  FreeAndNil(FDBOverlayMappingList     );
  FreeAndNil(FDBExtCommChannelList     );
  FreeAndNil(FDBLinkDefinitionList     );

  {*------------------------------------------------------------------------------
    freed DB definition dictionary
  -------------------------------------------------------------------------------}
  FreeAndNil(FDBAcoustic_Decoy_Definition       );
  FreeAndNil(FDBAir_Bubble_Definition           );
  FreeAndNil(FDBBomb_Definition                 );
  FreeAndNil(FDBChaff_Definition                );
  FreeAndNil(FDBDefensive_Jammer_Definition     );
  FreeAndNil(FDBEO_Detection_Definition         );
  FreeAndNil(FDBESM_Definition                  );
  FreeAndNil(FDBFCR_Definition                  );
  FreeAndNil(FDBFloating_Decoy_Definition       );
  FreeAndNil(FDBGun_Definition                  );
  FreeAndNil(FDBHybrid_Definition               );
  FreeAndNil(FDBInfrared_Decoy_Definition       );
  FreeAndNil(FDBJammer_Definition               );
  FreeAndNil(FDBMAD_Definition                  );
  FreeAndNil(FDBMine_Definition                 );
  FreeAndNil(FDBMissileDefinition               );
  FreeAndNil(FDBRadar_Definition                );
  FreeAndNil(FDBSatellite_Definition            );
  FreeAndNil(FDBSonar_Definition                );
  FreeAndNil(FDBSonobuoy_Definition             );
  FreeAndNil(FDBTorpedo_Definition              );
  FreeAndNil(FDBTowed_Jammer_Decoy_Definition   );
  FreeAndNil(FDBVehicleDefinition               );
  FreeAndNil(FDBRuntime_Platform_Library        );
  FreeAndNil(FDBPlatformActivation              );
  FreeAndNil(FDBHelicopterLandLaunchLimits      );
  FreeAndNil(FDBMotionCharacteristics           );

  FreeAndNil(FDBFormationList                   );
  FreeAndNil(FDBFormationMembersDict            );
  FreeAndNil(FDBReferencePoints                 );
  FreeAndNil(FDBLinkParticipantsDict            );
  FreeAndNil(FDBCubicleGroupList                );
  FreeAndNil(FDBCubicleAssignmentDict           );
  FreeAndNil(FDBMine_POD_vs_RangeDict           );
  FreeAndNil(FDBTorpedo_POH_ModifierDict        );
  FreeAndNil(FDBSonar_POD_CurveDict             );
  FreeAndNil(FDBRadar_Vertical_CovDict          );

  FreeAndNil(FDBExtCommChannelGroupDict         );

  {*------------------------------------------------------------------------------
    freed on board device dictionary
  -------------------------------------------------------------------------------}
  FreeAndNil(FDBEOOnBoardDict                 );
  FreeAndNil(FDBESMOnBoardDict                );
  FreeAndNil(FDBIFFOnBoardDict                );
  FreeAndNil(FDBMADOnBoardDict                );
  FreeAndNil(FDBFCROnBoardDict                );
  FreeAndNil(FDBVisualOnBoardDict             );
  FreeAndNil(FDBRadarOnBoardDict              );
  FreeAndNil(FDBSonarOnBoardDict              );
  FreeAndNil(FDBWeaponOnBoardDict             );
  FreeAndNil(FDBPointEffectOnBoardDict        );
  FreeAndNil(FDBAcousticDecoyOnBoardDict      );
  FreeAndNil(FDBAirBubbleOnBoardDict          );
  FreeAndNil(FDBChaffOnBoardDict              );
  FreeAndNil(FDBChaffLauncherOnBoardDict      );
  FreeAndNil(FDBDefensiveJammerOnBoardDict    );
  FreeAndNil(FDBFloatingDecoyOnBoardDict      );
  FreeAndNil(FDBInfraredDecoyOnBoardDict      );
  FreeAndNil(FDBJammerOnBoardDict             );
  FreeAndNil(FDBTowedJammerOnBoardDict        );
  FreeAndNil(FDBSonobuoyOnBoardDict           );
  FreeAndNil(FDBPlatformInstance              );
  FreeAndNil(FDBFitWeaponLauncherOnBoardDict  );

  FreeAndNil(FDBBlindZoneFCRDict             );
  FreeAndNil(FDBBlindZoneESMDict             );
  FreeAndNil(FDBBlindZoneEODict              );
  FreeAndNil(FDBBlindZoneVisualDict          );
  FreeAndNil(FDBBlindZonePointDict           );
  FreeAndNil(FDBBlindZoneFittedDict          );
  FreeAndNil(FDBBlindZoneSonarDict           );
  FreeAndNil(FDBBlindZoneRadarDict           );

end;

function TT3DBScenario.getMemberInteger(const index: integer): Integer;
begin
  Result := -1;
  case index of
    1 : if Assigned(FDBScenario_Definition) then
      Result := FDBScenario_Definition.Scenario_Index;
    2 : if Assigned(FDBResource_Allocation) then
      Result := FDBResource_Allocation.Resource_Alloc_Index;
  end;
end;

function TT3DBScenario.getDataModule: TDMLite;
begin
  result := DMLite;
end;

function TT3DBScenario.GetDBPlatformInstance(
  const inst_id: Integer): TDBPlatformInstance;
begin
  FDBPlatformInstance.TryGetValue(inst_id,result);
end;

function TT3DBScenario.GetDBPlatformMotion(const platf_type, platf_id: integer): TDBMotion_Characteristics;
var
  dbVeh : TDBVehicle_Definition;
  dbTorp: TDBTorpedo_Definition;
  dbMiss: TDBMissile_Definition;
begin
  result := nil;
  case platf_type of
    // vehicle
    1 :
    begin
      FDBVehicleDefinition.TryGetValue(platf_id,dbVeh);
      if assigned(dbVeh) then
        Result := DMLite.GetMotion_Characteristics(dbVeh.Motion_Characteristics, FDBMotionCharacteristics);
    end;

    // missile
    2 :
    begin
      FDBMissileDefinition.TryGetValue(platf_id,dbMiss);
      if assigned(dbMiss) then
        Result := DMLite.GetMotion_Characteristics(dbMiss.Motion_Index, FDBMotionCharacteristics);
    end;
    // torpedo
    4 :
    begin
      FDBTorpedo_Definition.TryGetValue(platf_id,dbTorp);
      if assigned(dbTorp) then
        Result := DMLite.GetMotion_Characteristics(dbTorp.Motion_Index, FDBMotionCharacteristics);
    end;
  end;

end;

function TT3DBScenario.GetDBPointEffectOnBoardDef(const veh_id,
  point_inst: Integer): TDBPoint_Effect_On_Board;
var
  l : TDBPointEffectOnBoardList;
  item : TDBPoint_Effect_On_Board;
begin
  result := nil;

  FDBPointEffectOnBoardDict.TryGetValue(veh_id,l);
  if Assigned(l) then
    for item in l do
    begin
      if item.Point_Effect_Index = point_inst then
      begin
        result := item;
        Break;
      end;
    end;

end;

function TT3DBScenario.GetDBRadarDefinition(
  const rdr_id: Integer): TDBRadar_Definition;
begin
  result := DMLite.GetRadarDefinition(rdr_id,FDBRadar_Definition);
end;

function TT3DBScenario.GetDBRadarOnBoardDef(const veh_id,
  rdr_inst: Integer): TDBRadar_On_Board;
var
  l : TDBRadarOnBoardList;
  item : TDBRadar_On_Board;
begin
  result := nil;

  FDBRadarOnBoardDict.TryGetValue(veh_id,l);
  if Assigned(l) then
    for item in l do
    begin
      if item.Radar_Instance_Index = rdr_inst then
      begin
        result := item;
        Break;
      end;
    end;
end;

function TT3DBScenario.GetDBRadarVerticalCoverages(
  const radar_idx: Integer): TDBRadar_Vertical_CovList;
begin
  result := DMLite.GetVerticalCoverage(radar_idx,FDBRadar_Vertical_CovDict);
end;

function TT3DBScenario.GetDBSatelliteDefinition(
  const sat_id: Integer): TDBSatellite_Definition;
begin
  result := DMLite.GetSatellite_Definition(sat_id,FDBSatellite_Definition);
end;

function TT3DBScenario.GetDBSelfJammerDefintion(
  const asst_id: Integer): TDBJammer_Definition;
begin
  result := DMLite.GetJammerDefinition(asst_id,FDBJammer_Definition);
end;

function TT3DBScenario.GetDBSonarDefinition(
  const snr_id: Integer): TDBSonar_Definition;
begin
  result := DMLite.GetSonarDefinition(snr_id,FDBSonar_Definition);
end;

function TT3DBScenario.GetDBSonarOnBoardDef(const veh_id,
  snr_inst: Integer): TDBSonar_On_Board;
var
  l : TDBSonarOnBoardList;
  item : TDBSonar_On_Board;
begin
  result := nil;

  FDBSonarOnBoardDict.TryGetValue(veh_id,l);
  if Assigned(l) then
    for item in l do
    begin
      if item.Sonar_Instance_Index = snr_inst then
      begin
        result := item;
        Break;
      end;
    end;
end;

function TT3DBScenario.GetDBTorpedoDefintion(
  const torp_id: Integer): TDBTorpedo_Definition;
begin
  result := DMLite.GetTorpedo_Definition(torp_id,FDBTorpedo_Definition);
end;

function TT3DBScenario.GetDBTowedJammerDefintion(
  const asst_id: Integer): TDBTowed_Jammer_Decoy_Definition;
begin
  result := DMLite.GetTowedJammerDefinition(asst_id,FDBTowed_Jammer_Decoy_Definition);
end;

function TT3DBScenario.GetDBTowedJammerOnBoardDef(const veh_id,
  asst_inst: Integer): TDBTowed_Jammer_Decoy_On_Board;
var
  l : TDBTowedJammerOnBoardList;
  item : TDBTowed_Jammer_Decoy_On_Board;
begin
  result := nil;

  FDBTowedJammerOnBoardDict.TryGetValue(veh_id,l);
  if Assigned(l) then
    for item in l do
    begin
      if item.Towed_Decoy_Instance_Index = asst_inst then
      begin
        result := item;
        Break;
      end;
    end;
end;

function TT3DBScenario.GetDBPlatformMotion(
  const motion_id: integer): TDBMotion_Characteristics;
begin

end;

function TT3DBScenario.getMemberfloat(const index: integer): double;
begin
  Result := 0;
end;

function TT3DBScenario.getMemberString(const index: integer): string;
begin
//  case index of
//     1: begin
//       result := Scenario_def.FData.Scenario_Identifier;
//     end;
//  end;
end;

function TT3DBScenario.GetDBSonarPODCurvePoints(
  const curve_idx: Integer): TDBSonar_POD_CurveList;
begin
  result := DMLite.GetSonarPODCurve(curve_idx,FDBSonar_POD_CurveDict);
end;

function TT3DBScenario.GetDBSonoBuoyDefinition(
  const sono_id: Integer): TDBSonobuoy_Definition;
begin
  result := DMLite.GetSonobuoy_Definition(sono_id,FDBSonobuoy_Definition);
end;

function TT3DBScenario.GetDBAcousticDefintion(
  const asst_id: Integer): TDBAcoustic_Decoy_Definition;
begin
  result := DMLite.GetAcousticDecoyDefinition(asst_id,FDBAcoustic_Decoy_Definition);
end;

function TT3DBScenario.GetDBAcousticOnBoardDef(const veh_id,
  asst_inst: Integer): TDBAcoustic_Decoy_On_Board;
var
  l : TDBAcousticDecoyOnBoardList;
  item : TDBAcoustic_Decoy_On_Board;
begin
  result := nil;

  FDBAcousticDecoyOnBoardDict.TryGetValue(veh_id,l);
  if Assigned(l) then
    for item in l do
    begin
      if item.Acoustic_Instance_Index = asst_inst then
      begin
        result := item;
        Break;
      end;
    end;

end;

function TT3DBScenario.GetDBAirBubbleDefintion(
  const asst_id: Integer): TDBAir_Bubble_Definition;
begin
  result := DMLite.GetAirBubbleDefinition(asst_id,FDBAir_Bubble_Definition);
end;

function TT3DBScenario.GetDBAirBubbleOnBoardDef(const veh_id,
  asst_inst: Integer): TDBAir_Bubble_Mount;
var
  l : TDBAirBubbleOnBoardList;
  item : TDBAir_Bubble_Mount;
begin
  result := nil;

  FDBAirBubbleOnBoardDict.TryGetValue(veh_id,l);
  if Assigned(l) then
    for item in l do
    begin
      if item.Air_Bubble_Instance_Index = asst_inst then
      begin
        result := item;
        Break;
      end;
    end;
end;

function TT3DBScenario.GetDBBlindZones(const nzTypes: string;
  const asset_id: integer): TDBBlindZoneDefinitionList;
begin
  if nzTypes = 'EO' then
    result := DMLite.GetBlindZones(asset_id,3,FDBBlindZoneEODict)
  else
  if nzTypes = 'ESM' then
    result := DMLite.GetBlindZones(asset_id,2,FDBBlindZoneESMDict)
  else
  if nzTypes = 'Visual' then
    result := DMLite.GetBlindZones(asset_id,4,FDBBlindZoneVisualDict)
  else
  if nzTypes = 'FCR' then
    result := DMLite.GetBlindZones(asset_id,1,FDBBlindZoneFCRDict)
  else
  if nzTypes = 'Radar' then
    result := DMLite.GetBlindZones(asset_id,8,FDBBlindZoneRadarDict)
  else
  if nzTypes = 'Sonar' then
    result := DMLite.GetBlindZones(asset_id,7,FDBBlindZoneSonarDict)
  else
  if nzTypes = 'Fitted_Weapon' then
    result := DMLite.GetBlindZones(asset_id,6,FDBBlindZoneFittedDict)
  else
  if nzTypes = 'Point' then
    result := DMLite.GetBlindZones(asset_id,5,FDBBlindZonePointDict)
  else
    Result := nil;
end;

function TT3DBScenario.GetDBBombDefintion(
  const bomb_id: Integer): TDBBomb_Definition;
begin
  result := DMLite.GetBombDefinition(bomb_id,FDBBomb_Definition);
end;

function TT3DBScenario.GetDBChaffDefintion(
  const chaff_id: Integer): TDBChaff_Definition;
begin
  result := DMLite.GetChaffDefinition(chaff_id,FDBChaff_Definition);
end;

function TT3DBScenario.GetDBChaffOnBoardDef(const veh_id,
  chaff_inst: Integer): TDBChaff_On_Board;
var
  l : TDBChaffOnBoardList;
  item : TDBChaff_On_Board;
begin
  result := nil;

  FDBChaffOnBoardDict.TryGetValue(veh_id,l);
  if Assigned(l) then
    for item in l do
    begin
      if item.Chaff_Instance_Index = chaff_inst then
      begin
        result := item;
        Break;
      end;
    end;
end;

function TT3DBScenario.GetDBDefJammerDefintion(
  const asst_id: Integer): TDBDefensive_Jammer_Definition;
begin
  result := DMLite.GetDefJammerDefinition(asst_id,FDBDefensive_Jammer_Definition);
end;

function TT3DBScenario.GetDBDefJammerOnBoardDef(const veh_id,
  asst_inst: Integer): TDBDefensive_Jammer_On_Board;
var
  l : TDBDefensiveJammerOnBoardList;
  item : TDBDefensive_Jammer_On_Board;
begin
  result := nil;

  FDBDefensiveJammerOnBoardDict.TryGetValue(veh_id,l);
  if Assigned(l) then
    for item in l do
    begin
      if item.Defensive_Jammer_Instance_Index = asst_inst then
      begin
        result := item;
        Break;
      end;
    end;
end;

function TT3DBScenario.GetDBEODefinition(
  const eo_id: Integer): TDBEO_Detection_Definition;
begin
  result := DMLite.GetEO_Definition(eo_id,FDBEO_Detection_Definition);
end;

function TT3DBScenario.GetDBEOOnBoardDef(const veh_id,
  eo_inst: Integer): TDBEO_On_Board;
var
  l : TDBEOOnBoardList;
  item : TDBEO_On_Board;
begin
  result := nil;

  FDBEOOnBoardDict.TryGetValue(veh_id,l);
  if Assigned(l) then
    for item in l do
    begin
      if item.EO_Instance_Index = eo_inst then
      begin
        result := item;
        Break;
      end;
    end;
end;

function TT3DBScenario.GetDBESMDefinition(
  const esm_id: Integer): TDBESM_Definition;
begin
  result := DMLite.GetESM_Definition(esm_id,FDBESM_Definition);
end;

function TT3DBScenario.GetDBESMOnBoardDef(const veh_id,
  esm_inst: Integer): TDBESM_On_Board;
var
  l : TDBESMOnBoardList;
  item : TDBESM_On_Board;
begin
  result := nil;

  FDBESMOnBoardDict.TryGetValue(veh_id,l);
  if Assigned(l) then
    for item in l do
    begin
      if item.ESM_Instance_Index = esm_inst then
      begin
        result := item;
        Break;
      end;
    end;
end;

function TT3DBScenario.GetDBFittedWeaponOnBoardDef(const veh_id,
  fitted_inst: Integer): TDBFitted_Weapon_On_Board;
var
  l : TDBFitWeaponOnBoardList;
  item : TDBFitted_Weapon_On_Board;
begin
  result := nil;

  FDBWeaponOnBoardDict.TryGetValue(veh_id,l);
  if Assigned(l) then
    for item in l do
    begin
      if item.Fitted_Weap_Index = fitted_inst then
      begin
        result := item;
        Break;
      end;
    end;
end;

function TT3DBScenario.GetDBFloatingDecoyDefintion(
  const asst_id: Integer): TDBFloating_Decoy_Definition;
begin
  result := DMLite.GetFloatingDecoyDefinition(asst_id,FDBFloating_Decoy_Definition);
end;

function TT3DBScenario.GetDBFloatingDecoyOnBoardDef(const veh_id,
  asst_inst: Integer): TDBFloating_Decoy_On_Board;
var
  l : TDBFloatingDecoyOnBoardList;
  item : TDBFloating_Decoy_On_Board;
begin
  result := nil;

  FDBFloatingDecoyOnBoardDict.TryGetValue(veh_id,l);
  if Assigned(l) then
    for item in l do
    begin
      if item.Floating_Decoy_Instance_Index = asst_inst then
      begin
        result := item;
        Break;
      end;
    end;
end;

function TT3DBScenario.GetDBGunDefinition(
  const gun_id: Integer): TDBGun_Definition;
begin
  result := DMLite.GetGunDefinition(gun_id,FDBGun_Definition);
end;

function TT3DBScenario.GetDBGunDefintion(
  const gun_id: Integer): TDBGun_Definition;
begin
  result := DMLite.GetGunDefinition(gun_id,FDBGun_Definition);
end;

function TT3DBScenario.GetDBIFFOnBoardDef(const veh_id,
  iff_inst: Integer): TDBIFF_Sensor_On_Board;
var
  l : TDBIFFOnBoardList;
  item : TDBIFF_Sensor_On_Board;
begin
  result := nil;

  FDBIFFOnBoardDict.TryGetValue(veh_id,l);
  if Assigned(l) then
    for item in l do
    begin
      if item.IFF_Instance_Index = iff_inst then
      begin
        result := item;
        Break;
      end;
    end;
end;

function TT3DBScenario.GetDBInfraredDefintion(
  const asst_id: Integer): TDBInfrared_Decoy_Definition;
begin
  result := DMLite.GetInfraredDecoyDefinition(asst_id,FDBInfrared_Decoy_Definition);
end;

function TT3DBScenario.GetDBInfraredOnBoardDef(const veh_id,
  asst_inst: Integer): TDBInfrared_Decoy_On_Board;
var
  l : TDBInfraredDecoyOnBoardList;
  item : TDBInfrared_Decoy_On_Board;
begin
  result := nil;

  FDBInfraredDecoyOnBoardDict.TryGetValue(veh_id,l);
  if Assigned(l) then
    for item in l do
    begin
      if item.Infrared_Decoy_Instance_Index = asst_inst then
      begin
        result := item;
        Break;
      end;
    end;
end;

function TT3DBScenario.GetDBJammerOnBoardDef(const veh_id,
  asst_inst: Integer): TDBJammer_On_Board;
var
  l : TDBJammerOnBoardList;
  item : TDBJammer_On_Board;
begin
  result := nil;

  FDBJammerOnBoardDict.TryGetValue(veh_id,l);
  if Assigned(l) then
    for item in l do
    begin
      if item.Jammer_Instance_Index = asst_inst then
      begin
        result := item;
        Break;
      end;
    end;
end;

function TT3DBScenario.GetDBMADDefinition(
  const mad_id: Integer): TDBMAD_Definition;
begin
  result := DMLite.GetMAD_Definition(mad_id,FDBMAD_Definition);
end;

function TT3DBScenario.GetDBMADOnBoardDef(const veh_id,
  mad_inst: Integer): TDBMAD_Sensor_On_Board;
var
  l : TDBMADOnBoardList;
  item : TDBMAD_Sensor_On_Board;
begin
  result := nil;

  FDBMADOnBoardDict.TryGetValue(veh_id,l);
  if Assigned(l) then
    for item in l do
    begin
      if item.MAD_Instance_Index = mad_inst then
      begin
        result := item;
        Break;
      end;
    end;
end;

function TT3DBScenario.GetDBMineDefintion(
  const mine_id: Integer): TDBMine_Definition;
begin
  result := DMLite.GetMine_Definition(mine_id,FDBMine_Definition);
end;

function TT3DBScenario.GetDBMissileDefintion(
  const mis_id: Integer): TDBMissile_Definition;
begin
  result := DMLite.GetMissile_Definition(mis_id,FDBMissileDefinition);
end;

function TT3DBScenario.GetDBPlatformActivation(
  const pf_inst_id: Integer): TDBPlatform_Activation;
begin
  result := DMLite.GetPlatform_Activation(
    FDBAsset_Deployment_Definition.Deployment_Index,
    pf_inst_id,FDBPlatformActivation);
end;

function TT3DBScenario.GetDBVehicleDefinition(
  const veh_id: Integer): TDBVehicle_Definition;
begin
  result := DMLite.GetVehicle_Definition(veh_id,FDBVehicleDefinition);
end;

function TT3DBScenario.GetDBVisualOnBoardDef(const veh_id,
  vis_inst: Integer): TDBVisual_Sensor_On_Board;
var
  l : TDBVisualOnBoardList;
  item : TDBVisual_Sensor_On_Board;
begin
  result := nil;

  FDBVisualOnBoardDict.TryGetValue(veh_id,l);
  if Assigned(l) then
    for item in l do
    begin
      if item.Visual_Instance_Index = vis_inst then
      begin
        result := item;
        Break;
      end;
    end;
end;

procedure TT3DBScenario.ClearScenario;
begin
//  ClearAndFreeItems(Platform_Insts);
end;

procedure TT3DBScenario.LoadCommunicationAssignment;
var
  grp : TDBCubicle_Group;
begin
  for grp in FDBCubicleGroupList do
    DMLite.GetExternalCommunicationChannel(FDBResource_Allocation.Resource_Alloc_Index,
      grp.Group_Index,FDBExtCommChannelGroupDict);
end;

procedure TT3DBScenario.LoadCubicleGroups(deployIndex: integer);
var
  grp : TDBCubicle_Group;
begin
  DMLite.GetCubicleGroupList(FDBAsset_Deployment_Definition.Deployment_Index, FDBCubicleGroupList);
  for grp in FDBCubicleGroupList do
    DMLite.GetCubicleGroupMembers(grp.Group_Index,FDBCubicleAssignmentDict);
end;

procedure TT3DBScenario.LoadDBPlatformCountermeasure(pf_index: Integer);
var
  acdecoyOnBoardList : TDBAcousticDecoyOnBoardList;
  acdecoyOnBoard : TDBAcoustic_Decoy_On_Board;

  bubbleOnBoardList : TDBAirBubbleOnBoardList;
  bubbleOnBoard : TDBAir_Bubble_Mount;

  chaffOnBoardList : TDBChaffOnBoardList;
  chaffOnBoard : TDBChaff_On_Board;

  defJammerOnBoardList : TDBDefensiveJammerOnBoardList;
  defJammerOnBoard : TDBDefensive_Jammer_On_Board;

  floatOnBoardList : TDBFloatingDecoyOnBoardList;
  floatOnBoard : TDBFloating_Decoy_On_Board;

  irOnBoardList : TDBInfraredDecoyOnBoardList;
  irOnBoard : TDBInfrared_Decoy_On_Board;

  jammerOnBoardList : TDBJammerOnBoardList;
  jammerOnBoard : TDBJammer_On_Board;

  towedOnBoardList : TDBTowedJammerOnBoardList;
  towedOnBoard : TDBTowed_Jammer_Decoy_On_Board;
begin

  {get all accoustic decoy On Board}
  acdecoyOnBoardList := DMLite.GetAcousticDecoyOnBoard(pf_index,FDBAcousticDecoyOnBoardDict);
  {get all accoustic decoy defintion}
  if Assigned(acdecoyOnBoardList) then
  for acdecoyOnBoard in acdecoyOnBoardList do
    DMLite.GetAcousticDecoyDefinition(acdecoyOnBoard.Decoy_Index,FDBAcoustic_Decoy_Definition);

  {get all bubble On Board}
  bubbleOnBoardList := DMLite.GetAirBubbleOnBoard(pf_index,FDBAirBubbleOnBoardDict);
  {get all bubble defintion}
  if Assigned(bubbleOnBoardList) then
  for bubbleOnBoard in bubbleOnBoardList do
    DMLite.GetAirBubbleDefinition(bubbleOnBoard.Air_Bubble_Index,FDBAir_Bubble_Definition);

  {get all chaff On Board}
  chaffOnBoardList := DMLite.GetChaffOnBoard(pf_index,FDBChaffOnBoardDict);
  {get all chaff defintion}
  if Assigned(chaffOnBoardList) then
  for chaffOnBoard in chaffOnBoardList do
    DMLite.GetChaffDefinition(chaffOnBoard.Chaff_Index,FDBChaff_Definition);

  {get all chaff launcher On Board}
  DMLite.GetChaffLauncherOnBoard(pf_index,FDBChaffLauncherOnBoardDict);

  {get all def jammer On Board}
  defJammerOnBoardList := DMLite.GetDefJammerOnBoard(pf_index,FDBDefensiveJammerOnBoardDict);
  {get all def jammer defintion}
  if Assigned(defJammerOnBoardList) then
  for defJammerOnBoard in defJammerOnBoardList do
    DMLite.GetDefJammerDefinition(defJammerOnBoard.Defensive_Jammer_Index,FDBDefensive_Jammer_Definition);

  {get all floating decoy On Board}
  floatOnBoardList := DMLite.GetFloatingDecoyOnBoard(pf_index,FDBFloatingDecoyOnBoardDict);
  {get all floating decoy defintion}
  if Assigned(floatOnBoardList) then
  for floatOnBoard in floatOnBoardList do
    DMLite.GetFloatingDecoyDefinition(floatOnBoard.Floating_Decoy_Index,FDBFloating_Decoy_Definition);

  {get all ir On Board}
  irOnBoardList := DMLite.GetInfraredDecoyOnBoard(pf_index,FDBInfraredDecoyOnBoardDict);
  {get all ir defintion}
  if Assigned(irOnBoardList) then
  for irOnBoard in irOnBoardList do
    DMLite.GetInfraredDecoyDefinition(irOnBoard.Infrared_Decoy_Index,FDBInfrared_Decoy_Definition);

  {get all jammer On Board}
  jammerOnBoardList := DMLite.GetJammerOnBoard(pf_index,FDBJammerOnBoardDict);
  {get all jammer defintion}
  if Assigned(jammerOnBoardList) then
  for jammerOnBoard in jammerOnBoardList do
    DMLite.GetJammerDefinition(jammerOnBoard.Jammer_Index,FDBJammer_Definition);

  {get all towed jammer On Board}
  towedOnBoardList := DMLite.GetTowedJammerOnBoard(pf_index,FDBTowedJammerOnBoardDict);
  {get all towed jammer defintion}
  if Assigned(towedOnBoardList) then
  for towedOnBoard in towedOnBoardList do
    DMLite.GetTowedJammerDefinition(towedOnBoard.Towed_Decoy_Index,FDBTowed_Jammer_Decoy_Definition);
end;


procedure TT3DBScenario.LoadDBPlatformSensor(pf_index: Integer);
var
  eoOnBoardList : TDBEOOnBoardList;
  eoOnBoard : TDBEO_On_Board;

  esmOnBoardList : TDBESMOnBoardList;
  esmOnBoard : TDBESM_On_Board;

  madOnBoardList : TDBMADOnBoardList;
  madOnBoard : TDBMAD_Sensor_On_Board;

  fcrOnBoardList : TDBFCROnBoardList;
  fcrOnBoard : TDBFCR_On_Board;

  radarOnBoardList : TDBRadarOnBoardList;
  radarOnBoard : TDBRadar_On_Board;
  radarDef : TDBRadar_Definition;

  sonarOnBoardList : TDBSonarOnBoardList;
  sonarOnBoard : TDBSonar_On_Board;
  sonarDef : TDBSonar_Definition;

  visualOnBoardList : TDBVisualOnBoardList;
  visualOnBoard : TDBVisual_Sensor_On_Board;
begin

  {get all EO On Board}
  eoOnBoardList := DMLite.GetEOOnBoard(pf_index,FDBEOOnBoardDict);
  {get all EO defintion}
  if Assigned(eoOnBoardList) then
    for eoOnBoard in eoOnBoardList do
    begin
      DMLite.GetEO_Definition(eoOnBoard.EO_Index,FDBEO_Detection_Definition);
      DMLite.GetBlindZones(eoOnBoard.EO_Instance_Index,3,FDBBlindZoneEODict);
    end;

  {get all ESM On Board}
  esmOnBoardList := DMLite.GetESMOnBoard(pf_index,FDBESMOnBoardDict);
  {get all ESM defintion}
  if Assigned(esmOnBoardList) then
    for esmOnBoard in esmOnBoardList do
    begin
      DMLite.GetESM_Definition(esmOnBoard.ESM_Index,FDBESM_Definition);
      DMLite.GetBlindZones(esmOnBoard.ESM_Instance_Index,2,FDBBlindZoneESMDict);
    end;

  {get all IFF On Board}
  DMLite.GetIFFOnBoard(pf_index,FDBIFFOnBoardDict);

  {get all MAD On Board}
  madOnBoardList := DMLite.GetMADOnBoard(pf_index,FDBMADOnBoardDict);
  {get all MAD defintion}
  if Assigned(madOnBoardList) then
    for madOnBoard in madOnBoardList do
    begin
      DMLite.GetMAD_Definition(madOnBoard.MAD_Index,FDBMAD_Definition);
    end;

  {get all visual On Board}
  visualOnBoardList := DMLite.GetVisualOnBoard(pf_index,FDBVisualOnBoardDict);
  if Assigned(visualOnBoardList) then
    for visualOnBoard in visualOnBoardList do
    begin
      DMLite.GetBlindZones(visualOnBoard.Visual_Instance_Index,4,FDBBlindZoneVisualDict);
    end;


  {get all FCR On Board}
  fcrOnBoardList := DMLite.GetFCROnBoard(pf_index,FDBFCROnBoardDict);
  {get all FCR defintion}
  if Assigned(fcrOnBoardList) then
    for fcrOnBoard in fcrOnBoardList do
    begin
      DMLite.GetFCRDefinition(fcrOnBoard.Radar_Index,FDBFCR_Definition);
      DMLite.GetBlindZones(fcrOnBoard.FCR_Instance_Index,1,FDBBlindZoneFCRDict);
    end;

  {get all radar On Board}
  radarOnBoardList := DMLite.GetRadarOnBoard(pf_index,FDBRadarOnBoardDict);
  {get all radar defintion}
  if Assigned(radarOnBoardList) then
    for radarOnBoard in radarOnBoardList do
    begin
      radarDef := DMLite.GetRadarDefinition(radarOnBoard.Radar_Index,FDBRadar_Definition);
      DMLite.GetBlindZones(radarOnBoard.Radar_Instance_Index,8,FDBBlindZoneRadarDict);

      if Assigned(radarDef) then
        DMLite.GetVerticalCoverage(radarDef.Radar_Index,FDBRadar_Vertical_CovDict);

    end;

  {get all sonar On Board}
  sonarOnBoardList := DMLite.GetSonarOnBoard(pf_index,FDBSonarOnBoardDict);
  {get all sonar defintion}
  if Assigned(sonarOnBoardList) then
    for sonarOnBoard in sonarOnBoardList do
    begin
      sonarDef := DMLite.GetSonarDefinition(sonarOnBoard.Sonar_Index,FDBSonar_Definition);
      DMLite.GetBlindZones(sonarOnBoard.Sonar_Instance_Index,7,FDBBlindZoneSonarDict);

      if ASsigned(sonarDef) then
        DMLite.GetSonarPODCurve(sonarDef.Curve_Detection_Index,FDBSonar_POD_CurveDict);
    end;
end;

procedure TT3DBScenario.LoadDBPlatformWeapon(pf_index: Integer);
var
  wpnOnBoardList : TDBFitWeaponOnBoardList;
  wpnOnBoard : TDBFitted_Weapon_On_Board;

  pefOnBoardList : TDBPointEffectOnBoardList;
  pefOnBoard : TDBPoint_Effect_On_Board;
begin

  {get all fitted weapon On Board}
  wpnOnBoardList := DMLite.GetFittedWeaponOnBoard(pf_index,FDBWeaponOnBoardDict);
  {get all weapon defintion}
  if Assigned(wpnOnBoardList) then
  for wpnOnBoard in wpnOnBoardList do
  begin
    case wpnOnBoard.Mount_Type of
      {missile or hybrid}
      1 : DMLite.GetMissile_Definition(wpnOnBoard.Weapon_Index,FDBMissileDefinition);
      {torpedo}
      2 : DMLite.GetTorpedo_Definition(wpnOnBoard.Weapon_Index,FDBTorpedo_Definition);
      {mine}
      3 : DMLite.GetMine_Definition(wpnOnBoard.Weapon_Index,FDBMine_Definition);
    end;

    DMLite.GetBlindZones(wpnOnBoard.Fitted_Weap_Index,6,FDBBlindZoneFittedDict);
    DMLite.GetFittedWeaponLauncherOnBoard(wpnOnBoard.Fitted_Weap_Index,FDBFitWeaponLauncherOnBoardDict);
  end;

  {get all point effect On Board}
  pefOnBoardList := DMLite.GetPointEffectOnBoard(pf_index,FDBPointEffectOnBoardDict);
  {get all weapon defintion}
  if Assigned(pefOnBoardList) then
  for pefOnBoard in pefOnBoardList do
  begin
    case pefOnBoard.Mount_Type of
      {gun}
      1 : DMLite.GetGunDefinition(pefOnBoard.Weapon_Index,FDBGun_Definition);
      {bomb}
      2 : DMLite.GetBombDefinition(pefOnBoard.Weapon_Index,FDBBomb_Definition);
    end;
    DMLite.GetBlindZones(pefOnBoard.Point_Effect_Index,5,FDBBlindZonePointDict);
  end;


end;

procedure TT3DBScenario.LoadFormation(deployIndex: integer);
var
  form : TDBFormation_Definition;
begin
  DMLite.GetFormationList(FDBAsset_Deployment_Definition.Deployment_Index, FDBFormationList);
  for form in FDBFormationList do
    DMLite.GetFormationAssignments(form.Formation_Index,FDBFormationMembersDict);
end;

procedure TT3DBScenario.LoadFromDB(const id: Integer);
var
  i  : Integer;
  s: string;
  dbPI : TPair<Integer,TDBPlatformInstance>;
begin
  ClearScenario;

  frmProgress := TfrmProgress.Create(nil);

  { 1 Get Scenario }
  if not DMLite.GetScenario(id, FDBScenario_Definition ) then
    Exit;

  frmProgress.Caption := 'Loading Scenario ' +
   FDBScenario_Definition.Scenario_Identifier + '  from database';

  { 1.2 Get Resource Allocation }
  if not DMLite.GetResourceAlloc(FDBScenario_Definition.Resource_Alloc_Index, FDBResource_Allocation) then
    Exit;

  { 1.3 Get Asset Deployment }
  if not DMLite.GetAssetDeployment(id, FDBAsset_Deployment_Definition) then
    Exit;

  { 1.2.1 get Game Environment }
  DMLite.GetGame_Environment_Definition(FDBResource_Allocation.Game_Enviro_Index,
    FDBGameEnvironment);

  { 1.2.1.1 get Game Area }
  DMLite.GetGame_Area_DefByID(FDBGameEnvironment.Game_Area_Index, FDBGameAreaDefinition);

  { 1.2.1.2 get Sub Area }
  DMLite.GetSubArea_Enviro_Definition(FDBResource_Allocation.Game_Enviro_Index, FDBSubAreaDefinitions);

  { 1.2.1.3 get Geo Area }
  //dmTTT.GetGeoAreaDefinition(GameEnvironment.FData.Game_Area_Index, GeoPoint);

  { 1.2.2 get Game Default }
  if (DMLite.GetGame_Defaults(ResourceAllocIndex, FDBGameDefaults)) then
  begin
    i := FDBGameDefaults.Defaults_Index;
    DMLite.GetGame_Cloud_On_ESM(i, FDBGameCloud_ESM);
    DMLite.GetGame_Cloud_On_Radar(i, FDBGameCloud_Radar);
    DMLite.GetGame_Default_IFF_Mode_Code(i, FDBGameDefault_IFF);
    DMLite.GetGame_Rainfall_On_ESM(i, FDBGameRainfall_ESM);
    DMLite.GetGame_Rainfall_On_Missile_Seeker(i, FDBGameRainfall_Missile);
    DMLite.GetGame_Rainfall_On_Radar(i, FDBGameRainfall_Radar);
    DMLite.GetGame_Rainfall_On_Sonar(i, FDBGameRainfall_Sonar);
    DMLite.GetGame_Sea_On_Missile_Safe_Altitude (i, FDBGameSea_Missile);
    DMLite.GetGame_Sea_On_Radar(i, FDBGameSea_Radar);
    DMLite.GetGame_Sea_On_Sonar(i, FDBGameSea_Sonar);
    DMLite.GetGame_Ship_On_Sonar(i, FDBGameShip_Sonar);
  end;

  { 1.2.3 get Role List }

  { 1.2.4 get Platform Instance }
  DMLite.getPlatFormInstances(FDBResource_Allocation.Resource_Alloc_Index, FDBPlatformInstance);

  { 1.2.5 get Runtime Platform}
  DMLite.Get_Library_Instance(FDBResource_Allocation.Resource_Alloc_Index, FDBRuntime_Platform_Library);

  { 1.2.6 get Overlay_Mapping }
  dmLite.GetResource_Overlay_Mapping(FDBResource_Allocation.Resource_Alloc_Index, FDBOverlayMappingList);

  { 1.2.7 get AllReference_Point }
  DMLite.getAllReference_Point(FDBResource_Allocation.Resource_Alloc_Index, FDBReferencePoints);

  { 1.2.8 get External_Communication_Channel }
//  dmLite.GetExternalCommunicationChannel(FDBResource_Allocation.Resource_Alloc_Index,FDBExtCommChannelGroupDict);

  frmProgress.MaxJob := FDBPlatformInstance.Count;

  { 1.2.4.1 get all Platform Definition }
  for dbPI in FDBPlatformInstance do
  begin
    frmProgress.increase( dbPI.Value.Instance_Name );
    LoadPlatformDefinition(dbPI.Value);
  end;

//  for I := 0 to Platform_Insts.Count - 1 do begin
//    pi := Platform_Insts[i];
//
//    frmProgress.increase( pi.FData.Instance_Name );
//    LoadPlatformDefinition(pi);
//
//    if Assigned(FOnAssignedPlatform) then
//      FOnAssignedPlatform(pi);

//    //untuk set default data platform instance untuk NonRealTime Platform
//    if DefaultNonRealPlatform = nil then
//    begin
//      if pi.FData.Vehicle_Index > 0 then
//      begin
////        aSize := SizeOf(pi);
////        GetMem(p, aSize);
////        CopyMemory(p, @pi, aSize);
//        DefaultNonRealPlatform := TPlatform_Instance.Create;
//        DefaultNonRealPlatform.FData.Vehicle_Index := pi.FData.Vehicle_Index;
//        LoadPlatformDefinition(DefaultNonRealPlatform)
//      end;
//    end;
//  end;

//  Formation_List.Clear;
//  // --> kenapa di clear : Platform_Inst.Clear;

  { 1.3.1 get Formation definition andassignment }
  LoadFormation(FDBAsset_Deployment_Definition.Deployment_Index);

  { 1.3.2 get DataLink }
  LoadLinks(FDBAsset_Deployment_Definition.Deployment_Index);

  { 1.4 get Cubicle Group }
  LoadCubicleGroups(FDBAsset_Deployment_Definition.Deployment_Index);

  { 1.4 get Cubicle Channel Assignment }
  LoadCommunicationAssignment;

//
//  for I := 0 to CubiclesGroupsList.Count - 1 do begin
//    grp  := CubiclesGroupsList.Items[i] as T3CubicleGroup;
//    if grp <> nil then
//    begin
//      grp.InitData;
//
//      dmTTT.GetT3GroupMember(grp.FData.Group_Index, grp.FSList);
//
//      for j := 0 to grp.Count - 1 do
//      begin
//        grm := grp.Items[j] as T3CubicleGroupMember;
//        if grm = nil then
//          continue;
//
//        k := 0;
//        found := false;
//        pi := nil;
//
//        while not found and (k < Platform_Insts.Count) do
//        begin
//          pi := Platform_Insts[k];
//          found := grm.FData.Platform_Instance_Index = pi.FData.Platform_Instance_Index;
//          Inc(k);
//        end;
//
//        if found then
//        begin
//           if j = 0 then
//             pi.IsGroupLeader := true
//           else
//             pi.IsGroupLeader := false;
//
//           pi.CubicleGroupID := grp.FData.Group_Index;
//           grp.SetTrackNumber(pi.FData.Platform_Instance_Index, j);
//        end
//      end;
//    end;
//  end;
//
//  { game environment calc }
//  GameEnvironment.calculateMaxPowerScaleArea;

  { geoset name }
  s := UpperCase(Trim(FDBGameAreaDefinition.Detail_Map));
  if s  = 'ENC' then
    MapGeosetName :=  FDBGameAreaDefinition.Game_Area_Identifier + '.gst'
  else
    MapGeosetName := FDBGameAreaDefinition.Game_Area_Identifier + '\' +
    FDBGameAreaDefinition.Game_Area_Identifier + '.gst';

  { overlay name }
//  overlayName := 'empty';
//  if (Overlay_Mapping.Count > 0) then
//    for I := 0 to Overlay_Mapping.Count - 1 do
//      allOverlayNames[I] := TResource_Allocation(Overlay_Mapping[I]).FOverlay.Overlay_Filename;

  frmProgress.Free;
end;

procedure TT3DBScenario.LoadLinks(deployIndex: integer);
var
  link : TDBLink_Definition;
begin
  DMLite.GetLinkDefinition(FDBAsset_Deployment_Definition.Deployment_Index, FDBLinkDefinitionList);
  for link in FDBLinkDefinitionList do
    DMLite.GetLinkParticipant(link.Link_Index,FDBLinkParticipantsDict);
end;

function TT3DBScenario.FindRuntimePlatform(const id: integer): TObject;
var i: Integer;
    f: Boolean;
//    vDef: TVehicle_Definition;
//    mDef: TMissile_Definition;
//    bDef: TMine_Definition;
begin
  i := 0;
  f := False;
  Result := nil;

//  while not f and (i < RuntimePlatformLibrary.Count) do begin
//    Result  := RuntimePlatformLibrary[i];
//
//    if Result is TVehicle_Definition then begin
//      vDef := Result as TVehicle_Definition;
//      f := vDef.FData.Vehicle_Index = id;
//    end;
//
//    if Result is TMissile_Definition then begin
//      mDef := Result as TMissile_Definition;
//      f := mDef.FData.Missile_Index = id;
//    end;
//
//    if Result is TMine_Definition then begin
//      bDef := Result as TMine_Definition;
//      f := bDef.FData.Mine_Index = id;
//    end;
//
//    Inc(i);
//  end;

  if not f then
    Result := nil;
end;

procedure TT3DBScenario.LoadPlatformDefinition(pi: TDBPlatformInstance);
var
  vIndex : integer;
  vehDef : TDBVehicle_Definition;
  missDef: TDBMissile_Definition;
  mineDef: TDBMine_Definition;
  torpDef: TDBTorpedo_Definition;
begin
  { set activation index, save activation to dict if not exist }
  DMLite.GetPlatform_Activation(
    FDBAsset_Deployment_Definition.Deployment_Index,pi.Platform_Instance_Index,
    FDBPlatformActivation);

  case pi.Platform_Type of
    {Vehicle}
    1 :
    begin
      {vehicle definition}
      vehDef := DMLite.GetVehicle_Definition(pi.Platform_Index,FDBVehicleDefinition);
      {heli land launch limit}
      DMLite.GetHeliLaunchLimits(pi.Platform_Index,FDBHelicopterLandLaunchLimits);
      {get motion chararacteristic}
      DMLite.GetMotion_Characteristics(vehDef.Motion_Characteristics, FDBMotionCharacteristics);

      {load sensor on board}
      LoadDBPlatformSensor(pi.Platform_Index);
      {load weapon on board}
      LoadDBPlatformWeapon(pi.Platform_Index);
      {load counter on board}
      LoadDBPlatformCountermeasure(pi.Platform_Index);
    end;

    {Missile}
    2 :
    begin
      {missile definition}
      missDef := DMLite.GetMissile_Definition(pi.Platform_Index,FDBMissileDefinition);
      {get motion chararacteristic}
      DMLite.GetMotion_Characteristics(missDef.Motion_Index, FDBMotionCharacteristics);
    end;

    {Mine}
    3 :
    begin
      {mine definition}
      mineDef := DMLite.GetMine_Definition(pi.Platform_Index,FDBMine_Definition);
      {mine POD}
      DMLite.GetMinePOD(mineDef.Mine_Index,FDBMine_POD_vs_RangeDict);

    end;

    {Torpedo}
    4 :
    begin
      {torpedo definition}
      torpDef := DMLite.GetTorpedo_Definition(pi.Platform_Index,FDBTorpedo_Definition);
      {get motion chararacteristic}
      DMLite.GetMotion_Characteristics(torpDef.Motion_Index, FDBMotionCharacteristics);
      {get torpedo POH}
      DMLite.getTorpedoPOH(torpDef.Torpedo_Index,FDBTorpedo_POH_ModifierDict);
    end;

    {hybrid}
    5 :;

    {satellite}
    6 :
    begin
      {sat definition}
      DMLite.GetSatellite_Definition(pi.Platform_Index,FDBSatellite_Definition);
    end;

    {sonobuoy}
    7 :
    begin
      {sonobuoy definition}
      DMLite.GetSonobuoy_Definition(pi.Platform_Index,FDBSonobuoy_Definition);
    end;
  end;
end;

procedure TT3DBScenario.SetEventOnExternalComm;
begin
  if Assigned(FOnGetExternalCom) then
    FOnGetExternalCom(Self);
end;

procedure TT3DBScenario.SetOnAssignedPlatform(const Value: TNotifyEvent);
begin
  FOnAssignedPlatform := Value;
end;

procedure TT3DBScenario.SetOnDBObjectInstanceCreate(const Value: TNotifyEvent);
begin
  FOnDBObjectInstanceCreate := Value;
end;

procedure TT3DBScenario.SetOnGetExternalCom(const Value: TNotifyEvent);
begin
  FOnGetExternalCom := Value;
end;

{ TT3CubicleChanel }

constructor TT3CubicleChanel.Create;
begin
  ListExternalChannel := TList.Create;
end;

destructor TT3CubicleChanel.destroy;
var
  i : integer;
begin
  inherited;

  if Assigned(ListExternalChannel) then
  begin
    for i := ListExternalChannel.Count - 1 downto 0 do
    begin
      ListExternalChannel.Delete(i);
    end;
    ListExternalChannel.Clear;
    ListExternalChannel.Free;
  end;
end;

end.
