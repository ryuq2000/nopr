unit uDBClassDefinition;

interface

{*------------------------------------------------------------------------------
  Kelas untuk definisi tabel database.
  Hanya berisi field saja.

  @author  Rizky
  @version 2015/12/18 1.0 Initial revision.
  @todo
  @comment
-------------------------------------------------------------------------------}

uses
  Generics.Collections;

type

  {*------------------------------------------------------------------------------
  Kelas TDBVehicle_Definition untuk definisi tabel Vehicle_Definition
  -------------------------------------------------------------------------------}

  TDBVehicle_Definition =  class
  public
    Vehicle_Index                      : integer;
    Vehicle_Identifier                 : string[60];
    Platform_Domain                    : byte;
    Platform_Category                  : byte;
    Platform_Type                      : byte;
    Motion_Characteristics             : integer;
    Length                             : single;
    Width                              : single;
    Height                             : single;
    Draft                              : single;
    Front_Radar_Cross                  : single;
    Side_Radar_Cross                   : single;
    Front_Acoustic_Cross               : single;
    Side_Acoustic_Cross                : single;
    Magnetic_Cross                     : single;
    Front_Visual_EO_Cross              : single;
    Side_Visual_EO_Cross               : single;
    Front_Infrared_Cross               : single;
    Side_Infrared_Cross                : single;
    LSpeed_Acoustic_Intens             : single;
    Below_Cav_Acoustic_Intens          : single;
    Above_Cav_Acoustic_Intens          : single;
    HSpeed_Acoustic_Intens             : single;
    Cavitation_Speed_Switch            : single;
    Time_of_Weapon_Impact              : integer;
    Chaff_Seduction_Capable            : ByteBool;
    Seduction_Mode_Prob                : single;
    Min_Delay_Between_Chaff_Rounds     : integer;
    Max_Chaff_Salvo_Size               : byte;
    SARH_POH_Modifier                  : single;
    CG_POH_Modifier                    : single;
    TARH_POH_Modifier                  : single;
    IR_POH_Modifier                    : single;
    AR_POH_Modifier                    : single;
    Active_Acoustic_Tor_POH_Mod        : single;
    Passive_Acoustic_Tor_POH_Mod       : single;
    Active_Passive_Tor_POH_Mod         : single;
    Wake_Home_POH_Modifier             : single;
    Wire_Guide_POH_Modifier            : single;
    Mag_Mine_POH_Modifier              : single;
    Press_Mine_POH_Modifier            : single;
    Impact_Mine_POH_Modifier           : single;
    Acoustic_Mine_POH_Modifier         : single;
    Sub_Comm_Antenna_Height            : single;
    Rel_Comm_Antenna_Height            : single;
    Max_Comm_Operating_Depth           : single;
    HF_Link_Capable                    : ByteBool;
    UHF_Link_Capable                   : ByteBool;
    HF_Voice_Capable                   : ByteBool;
    VHF_Voice_Capable                  : ByteBool;
    UHF_Voice_Capable                  : ByteBool;
    SATCOM_Voice_Capable               : ByteBool;
    UWT_Voice_Capable                  : ByteBool;
    HF_MHS_Capable                     : ByteBool;
    UHF_MHS_Capable                    : ByteBool;
    SATCOM_MHS_Capable                 : ByteBool;
    Damage_Capacity                    : integer;
    Plat_Basing_Capability             : ByteBool;
    Chaff_Capability                   : ByteBool;
    Readying_Time                      : integer;
    Sonobuoy_Capable                   : ByteBool;
    Nav_Light_Capable                  : ByteBool;
    Periscope_Depth                    : single;
    Periscope_Height_Above_Water       : single;
    Periscope_Front_Radar_Xsection     : single;
    Periscope_Side_Radar_Xsection      : single;
    Periscope_Front_Vis_Xsection       : single;
    Periscope_Side_Vis_Xsection        : single;
    Periscope_Front_IR_Xsection        : single;
    Periscope_Side_IR_Xsection         : single;
    Engagement_Range                   : single;
    Auto_Air_Defense_Capable           : ByteBool;
    Alert_State_Time                   : single;
    Detectability_Type                 : byte;
    Max_Sonobuoys_To_Monitor           : integer;
    Sonobuoy_Deploy_Max_Altitude       : integer;
    Sonobuoy_Deploy_Min_Altitude       : integer;
    Sonobuoy_Deploy_Max_Speed          : integer;
    Air_Drop_Torpedo_Max_Altitude      : integer;
    Air_Drop_Torpedo_Min_Altitude      : integer;
    Air_Drop_Torpedo_Max_Speed         : integer;
    TMA_Rate_Factor                    : single;
    HMS_Noise_Reduction_Factor         : single;
    TAS_Noise_Reduction_Factor         : single;
    Infrared_Decoy_Capable             : ByteBool;
    HF_Mid_Course_Update_Capable       : ByteBool;
    UHF_Mid_Course_Update_Capable      : ByteBool;
    SATCOM_Mid_Course_Update_Capable   : ByteBool;
    Quantity_Group_Personal            : Integer;
    VBS_Class_Name                     : string[60];
    GangwayPosition                    : byte;
  end;

  {*------------------------------------------------------------------------------
  Kelas TDBMissile_Definition untuk definisi tabel Missile_Definition
  -------------------------------------------------------------------------------}
  TDBMissile_Definition =  class
    Missile_Index                    : integer;
    Class_Identifier                 : string[60];
    Platform_Domain                  : byte;
    Platform_Category                : byte;
    Platform_Type                    : byte;
    Max_Range                        : single;
    Min_Range                        : single;
    Motion_Index                     : integer;
    Seeker_TurnOn_Range              : single;
    Second_Seeker_Pattern_Capable    : byte;
    Seeker_Bias_Capable              : byte;
    Fixed_Seeker_Turn_On_Range       : byte;
    Lethality                        : integer;
    Prob_of_Hit                      : single;
    Damage_Capacity                  : integer;
    Default_Altitude                 : single;
    Length                           : single;
    Width                            : single;
    Height                           : single;
    Front_Radar_Cross                : single;
    Side_Radar_Cross                 : single;
    Front_Visual_Cross               : single;
    Side_Visual_Cross                : single;
    Front_Infrared_Cross             : single;
    Side_Infrared_Cross              : single;
    Pursuit_Guide_Type               : byte;
    Primary_Guide_Type               : byte;
    Secondary_Guide_Type             : byte;
    Anti_Air_Capable                 : byte;
    Anti_Sur_Capable                 : byte;
    Anti_SubSur_Capable              : byte;
    Anti_Land_Capable                : byte;
    Anti_Amphibious_Capable          : byte;
    Primary_Target_Domain            : byte;
    SARH_POH_Modifier                : single;
    CG_POH_Modifier                  : single;
    TARH_POH_Modifier                : single;
    IR_POH_Modifier                  : single;
    AR_POH_Modifier                  : single;
    Transmitted_Frequency            : double;
    Scan_Rate                        : single;
    Pulse_Rep_Freq                   : single;
    Pulse_Width                      : single;
    Xmit_Power                       : single;
    TARH_Jamming_A_Probability       : single;
    TARH_Jamming_B_Probability       : single;
    TARH_Jamming_C_Probability       : single;
    Wpt_Capable                      : byte;
    Max_Num_Wpts                     : byte;
    Min_Final_Leg_Length             : single;
    Engagement_Range                 : single;
    Max_Firing_Depth                 : single;
    Upper_Received_Freq              : double;
    Lower_Received_Freq              : double;
    Fly_Out_Required                 : byte;
    Fly_Out_Range                    : single;
    Fly_Out_Altitude                 : single;
    Booster_Separation_Required      : byte;
    Booster_Separation_Range         : single;
    Booster_Separation_Box_Width     : single;
    Booster_Separation_Box_Length    : single;
    Term_Guide_Azimuth               : single;
    Term_Guide_Elevation             : single;
    Term_Guide_Range                 : single;
    Terminal_Guidance_Capability     : byte;
    Terminal_Altitude_Required       : byte;
    Terminal_Acquisition_Altitude    : single;
    Terminal_Sinuation_Start_Range   : single;
    Terminal_Sinuation_Period        : single;
    Terminal_Sinuation_Amplitude     : single;
    Terminal_Pop_Up_Range            : single;
    Terminal_Pop_Up_Altitude         : single;
    Mid_Course_Update_Mode           : byte;
    Home_On_Jam_Type_A_Capable       : byte;
    Home_On_Jam_Type_B_Capable       : byte;
    Home_On_Jam_Type_C_Capable       : byte;
    Launch_Method                    : byte;
    Data_Entry_Method                : byte;
    Launch_Speed                     : byte;
    Max_Target_Altitude_Delta        : integer;
    Term_Guide_Azimuth_Narrow        : single;
    Term_Guide_Elevation_Narrow      : single;
    Term_Guide_Range_Narrow          : single;
    Spot_Number                      : integer;
    ECCM_Type                        : byte;
    ECM_Detonation                   : byte;
    ECM_Detection                    : byte;
    Detectability_Type               : byte;
    IRCM_Detonation                  : byte;
    IRCM_Detection                   : byte;
    Sea_State_Modelling_Capable      : byte;
    Torpedo_Index                    : integer;
  end;

  {*------------------------------------------------------------------------------
  Kelas TDBTorpedo_Definition untuk definisi tabel Torpedo_Definition
  -------------------------------------------------------------------------------}
  TDBTorpedo_Definition =  class
    Torpedo_Index                : integer;
    Class_Identifier             : string[60];
    Platform_Domain              : byte;
    Platform_Category            : byte;
    Platform_Type                : byte;
    Max_Range                    : single;
    Min_Range                    : single;
    Motion_Index                 : integer;
    Seeker_TurnOn_Range          : single;
    Lethality                    : integer;
    Damage_Capacity              : integer;
    Default_Depth                : single;
    Length                       : single;
    Width                        : single;
    Height                       : single;
    Front_Acoustic_Cross         : single;
    Side_Acoustic_Cross          : single;
    LSpeed_Acoustic_Intens       : single;
    Below_Cav_Acoustic_Intens    : single;
    Above_Cav_Acoustic_Intens    : single;
    HSpeed_Acoustic_Intens       : single;
    Cavitation_Switch_Point      : single;
    Term_Guide_Azimuth           : single;
    Term_Guide_Elevation         : single;
    Term_Guide_Range             : single;
    Pursuit_Guidance_Type        : byte;
    Air_Drop_Capable             : byte;
    Use_Terminal_Circle          : byte;
    Terminal_Circle_Radius       : single;
    Fixed_Circle_Radius          : byte;
    Lateral_Deceleration         : single;
    Airborne_Descent_Rate        : double;
    Wire_Angle_Offset            : single;
    Guidance_Type                : byte;
    Anti_Sur_Capable             : byte;
    Anti_SubSur_Capable          : byte;
    Primary_Target_Domain        : byte;
    Active_Acoustic_POH_Mod      : single;
    Passive_Acoustic_POH_Mod     : single;
    Active_Passive_POH_Mod       : single;
    WireGuide_POH_Modifier       : single;
    WakeHome_POH_Modifier        : single;
    Active_Seeker_Power          : single;
    Active_Seeker_Freq           : single;
    Engagement_Range             : single;
    First_Relative_Gyro_Angle    : integer;
    Second_Relative_Gyro_Angle   : integer;
    Max_Torpedo_Gyro_Angle       : single;
    Max_Torpedo_Search_Depth     : single;
    Acoustic_Torp_Ceiling_Depth  : single;
    Fixed_Ceiling_Depth          : byte;
    Fixed_Seeker_TurnOn_Range    : byte;
    Sinuation_Runout             : byte;
    Runout_Sinuation_Period      : single;
    Runout_Sinuation_Amplitude   : single;
    Min_Runout_Range             : single;
    Launch_Method                : byte;
    Data_Entry_Method            : byte;
    Launch_Speed                 : byte;
    Opt_Launch_Range_Nuc_Sub     : single;
    Opt_Launch_Range_Conv_Sub    : single;
    Opt_Launch_Range_Other       : single;
    Detectability_Type           : byte;
  end;

  {*------------------------------------------------------------------------------
  Kelas TDBMine_Definition untuk definisi tabel Mine_Definition
  -------------------------------------------------------------------------------}
  TDBMine_Definition =  class
    Mine_Index             : integer;
    Mine_Identifier        : string[60];
    Platform_Domain        : byte;
    Platform_Category      : byte;
    Platform_Type          : byte;
    Mine_Classification    : byte;        // acoustic, impact, magnetic, pressure
    Length                 : single;
    Width                  : single;
    Height                 : single;
    Mooring_Type           : byte;
    Max_Laying_Depth       : single;
    Front_Acoustic_Cross   : single;
    Side_Acoustic_Cross    : single;
    Mine_Lethality         : integer;
    Engagement_Range       : single;
    Anti_Sur_Capable       : byte;
    Anti_SubSur_Capable    : byte;
    Detectability_Type     : byte;
  end;

  {*------------------------------------------------------------------------------
  Kelas TDBHybrid_Definition untuk definisi tabel Hybrid_Definition
  -------------------------------------------------------------------------------}
  TDBHybrid_Definition =  class
    Hybrid_Index   : integer;
    Missile_Index  : integer;
    Torpedo_Index  : integer;
  end;

  {*------------------------------------------------------------------------------
  Kelas TDBChaff_Definition untuk definisi tabel Chaff_Definition
  -------------------------------------------------------------------------------}
  TDBChaff_Definition =  class
    Chaff_Index              : integer;
    Chaff_Identifier         : string[60];
    Platform_Domain          : byte;
    Platform_Category        : byte;
    Platform_Type            : byte;
    Max_Radar_Cross          : single;
    Bloom_Time               : integer;
    Max_Dissipation_Time     : integer;
    Min_Dissipation_Time     : integer;
    Descent_Rate             : single;
    Max_Radius               : single;
    Max_Radar_Attenuation    : single;
    Radar_Affect_Lower_Freq  : single;
    Radar_Affect_Upper_Freq  : single;
  end;

  {*------------------------------------------------------------------------------
  Kelas TDBAir_Bubble_Definition untuk definisi tabel Air_Bubble_Definition
  -------------------------------------------------------------------------------}
  TDBAir_Bubble_Definition =  class
    Air_Bubble_Index       : integer;
    Air_Bubble_Identifier  : string[60];
    Platform_Domain        : byte;
    Platform_Category      : byte;
    Platform_Type          : byte;
    Max_Acoustic_Cross     : single;
    Dissipation_Time       : single;
    Ascent_Rate            : single;
    Descent_Rate           : single;
  end;

  {*------------------------------------------------------------------------------
  Kelas TDBBomb_Definition untuk definisi tabel Bomb_Definition
  -------------------------------------------------------------------------------}
  TDBBomb_Definition =  class
    Bomb_Index           : integer;
    Bomb_Identifier      : string[60];
    Bomb_Type            : byte;
    Lethality            : integer;
    Min_Range            : single;
    Max_Range            : single;
    Anti_Sur_Capable     : byte;
    Anti_SubSur_Capable  : byte;
    Anti_Land_Capable    : byte;
    Anti_Amphibious_Capable: Byte;
  end;

  {*------------------------------------------------------------------------------
  Kelas TDBDefensive_Jammer_Definition untuk definisi tabel Defensive_Jammer_Definition
  -------------------------------------------------------------------------------}
  TDBDefensive_Jammer_Definition =  class
    Defensive_Jammer_Index       : integer;
    Defensive_Jammer_Identifier  : string[60];
    Jammer_TARH_Capable          : byte;
    Jammer_SARH_Capable          : byte;
    Type_A_Seducing_Prob         : single;
    Type_B_Seducing_Prob         : single;
    Type_C_Seducing_Prob         : single;
    ECM_Type                     : byte;
  end;

  {*------------------------------------------------------------------------------
  Kelas TDBAcoustic_Decoy_Definition untuk definisi tabel Acoustic_Decoy_Definition
  -------------------------------------------------------------------------------}
  TDBAcoustic_Decoy_Definition =  class
    Decoy_Index                  : integer;
    Decoy_Identifier             : string[60];
    Acoustic_Intensity_Increase  : single;
  end;

  {*------------------------------------------------------------------------------
  Kelas TDBEO_Detection_Definition untuk definisi tabel EO_Detection_Definition
  -------------------------------------------------------------------------------}
  TDBEO_Detection_Definition =  class
    EO_Index             : integer;
    Class_Identifier     : string[60];
    Sensor_Type          : byte;
    Detection_Range      : single;
    Known_Cross_Section  : single;
    Max_Range            : single;
    Scan_Rate            : single;
    Num_FC_Channels      : byte;
  end;

  {*------------------------------------------------------------------------------
  Kelas TDBESM_Definition untuk definisi tabel ESM_Definition
  -------------------------------------------------------------------------------}
  TDBESM_Definition =  class
    ESM_Index                        : integer;
    Class_Identifier                 : string[60];
    Low_Detect_Frequency1            : double;
    High_Detect_Frequency1           : double;
    Low_Detect_Frequency2            : double;
    High_Detect_Frequency2           : double;
    ESM_Classification               : byte;
    Emitter_Detect_Range_Factor      : single;
    Comm_Intercept_Capable           : byte;
    Frequency_Identify_Range         : double;
    PRF_Identify_Range               : single;
    Pulsewidth_Identify_Range        : single;
    Scan_Period_Identify_Range       : single;
    Sector_Blank_Detection_Factor    : single;
    Identification_Period            : single;
    Classification_Period            : single;
    Minimum_Bearing_Error_Variance   : single;
    Initial_Bearing_Error_Variance   : single;
  end;

  {*------------------------------------------------------------------------------
  Kelas TDBFloating_Decoy_Definition untuk definisi tabel Floating_Decoy_Definition
  -------------------------------------------------------------------------------}
  TDBFloating_Decoy_Definition =  class
    Floating_Decoy_Index       : integer;
    Floating_Decoy_Identifier  : string[60];
    Platform_Domain            : byte;
    Platform_Category          : byte;
    Platform_Type              : byte;
    Length                     : single;
    Width                      : single;
    Height                     : single;
    Front_Radar_Cross          : single;
    Side_Radar_Cross           : single;
    Front_Visual_Cross         : single;
    Side_Visual_Cross          : single;
    Front_Acoustic_Cross       : single;
    Side_Acoustic_Cross        : single;
    Lifetime_Duration          : single;
  end;

  {*------------------------------------------------------------------------------
  Kelas TRecFCR_Definition untuk definisi tabel FCR_Definition
  -------------------------------------------------------------------------------}
  TDBFCR_Definition =  class
    FCR_Index                    : integer;
    Radar_Identifier             : string[60];
    Radar_Emitter                : string[60];
    Frequency                    : single;
    Scan_Rate                    : single;
    Radar_Power                  : single;
    Pulse_Rep_Freq               : single;
    Pulse_Width                  : single;
    Max_Unambig_Detection_Range  : single;
  end;

  {*------------------------------------------------------------------------------
  Kelas TDBGun_Definition untuk definisi tabel Gun_Definition
  -------------------------------------------------------------------------------}
  TDBGun_Definition =  class
    Gun_Index                    : integer;
    Gun_Identifier               : string[60];
    Gun_Category                 : byte;
    Rate_of_Fire                 : integer;
    Lethality_per_Round          : integer;
    Min_Range                    : single;
    Max_Range                    : single;
    Air_Min_Range                : single;
    Air_Max_Range                : single;
    Fire_Cntl_Director_Req       : byte;
    Chaff_Capable_Gun            : byte;
    Anti_Sur_Capable             : byte;
    Anti_Land_Capable            : byte;
    Anti_Air_Capable             : byte;
    Anti_Amphibious_Capable      : byte;
    Automode_Capable             : byte;
    Max_Target_Altitude_Delta    : integer;
    Gun_Average_Shell_Velocity   : single;
    Man_Gun_Max_Elevation        : single;
    Man_Gun_Min_Elevation        : single;
    Man_Gun_Rotation_Rate        : single;
    Man_Gun_Elevation_Rate       : single;
    Man_Gun_Num_Rounds_Per_Load  : integer;
    Man_Gun_Time_to_Reload       : single;
    Man_Gun_Muzzle_Velocity      : single;
    NGS_Capable                  : byte;
    NGS_MinDeflectionError       : single;
    NGS_MaxDeflectionError       : single;
    NGS_MinRangeError            : single;
    NGS_MaxRangeError            : single;
    NGS_MaxDispersionError       : single;
    NGS_MaxDamageRadius          : single;
    NGS_EffectiveRadius          : single;
    NGS_DamageRating             : integer;
  end;

  {*------------------------------------------------------------------------------
  Kelas TDBInfrared_Decoy_Definition untuk definisi tabel Infrared_Decoy_Definition
  -------------------------------------------------------------------------------}
  TDBInfrared_Decoy_Definition =  class
    Infrared_Decoy_Index       : integer;
    Infrared_Decoy_Identifier  : string[60];
    Platform_Domain            : byte;
    Platform_Category          : byte;
    Platform_Type              : byte;
    Max_Infrared_Cross         : single;
    Bloom_Time                 : integer;
    Sustain_Time               : integer;
    Max_Dissipation_Time       : integer;
    Min_Dissipation_Time       : integer;
    Descent_Rate               : single;
  end;

  {*------------------------------------------------------------------------------
  Kelas TDBJammer_Definition untuk definisi tabel Jammer_Definition
  -------------------------------------------------------------------------------}
  TDBJammer_Definition =  class
    Jammer_Index               : integer;
    Jammer_Type                : byte;
    Jammer_Identifier          : string[60];
    Lower_Freq_Limit           : double;
    Upper_Freq_Limit           : double;
    Jammer_Power_Density       : double;
    Max_Effective_Range        : single;
    Max_Sector_Width           : single;
    Upper_Vert_Coverage_Angle  : single;
    Lower_Vert_Coverage_Angle  : single;
  end;

  {*------------------------------------------------------------------------------
  Kelas TDBMAD_Definition untuk definisi tabel MAD_Definition
  -------------------------------------------------------------------------------}
  TDBMAD_Definition =  class
    MAD_Index              : integer;
    Class_Identifier       : string[60];
    Baseline_Detect_Range  : single;
    Known_Cross_Section    : single;
  end;

  {*------------------------------------------------------------------------------
  Kelas TDBRadar_Definition untuk definisi tabel Radar_Definition
  -------------------------------------------------------------------------------}
  TDBRadar_Definition =  class
    Radar_Index                    : integer;
    Radar_Identifier               : string[60];
    Radar_Emitter                  : string[60];
    Radar_Type                     : byte;
    Frequency                      : single;
    Scan_Rate                      : single;
    Pulse_Rep_Freq                 : single;
    Pulse_Width                    : single;
    Radar_Power                    : single;
    Detection_Range                : single;
    Known_Cross_Section            : single;
    Max_Unambig_Detect_Range       : single;
    IFF_Capability                 : ByteBool;
    Altitude_Data_Capability       : ByteBool;
    Ground_Speed_Data_Capability   : ByteBool;
    Heading_Data_Capability        : ByteBool;
    Plat_Type_Recog_Capability     : ByteBool;
    Plat_Class_Recog_Capability    : ByteBool;
    Clutter_Rejection              : single;
    Anti_Jamming_Capable           : ByteBool;
    Curve_Definition_Index         : integer;
    Second_Vert_Coverage           : ByteBool;
    Jamming_A_Resistant            : ByteBool;
    Jamming_B_Resistant            : ByteBool;
    Jamming_C_Resistant            : ByteBool;
    Anti_Jamming_A_Resistant       : ByteBool;
    Anti_Jamming_B_Resistant       : ByteBool;
    Anti_Jamming_C_Resistant       : ByteBool;
    Anti_Jamming_Range_Reduction   : single;
    Beam_Width                     : single;
    Sector_Scan_Capable            : ByteBool;
    Off_Axis_Jammer_Reduction      : single;
    Num_FCR_Channels               : byte;
    Radar_Spot_Number              : integer;
    Radar_Horizon_Factor           : single;
    Main_Lobe_Gain                 : single;
    Counter_Detection_Factor       : single;
    ECCM_Type                      : byte;
    MTI_Capable                    : ByteBool;
    MTI_MinTargetSpeed             : single;
  end;

  {*------------------------------------------------------------------------------
  Kelas TDBSatellite_Definition untuk definisi tabel Satellite_Definition
  -------------------------------------------------------------------------------}
  TDBSatellite_Definition =  class
    Satellite_Index              : integer;
    Satellite_Identifier         : string[60];
    Platform_Domain              : byte;
    Platform_Category            : byte;
    Platform_Type                : byte;
    Length                       : single;
    Width                        : single;
    Height                       : single;
    Front_Radar_Cross            : single;
    Side_Radar_Cross             : single;
    Orbit_Period                 : integer;
    Detection_Range_Radius       : single;
    Altitude                     : single;
    Ground_Speed                 : single;
    Plat_Type_Recog_Capability   : byte;
    Plat_Class_Recog_Capability  : byte;
  end;

  {*------------------------------------------------------------------------------
  Kelas TDBSonar_Definition untuk definisi tabel Sonar_Definition
  -------------------------------------------------------------------------------}
  TDBSonar_Definition =  class
    Sonar_Index                      : integer;
    Sonar_Identifier                 : string[60];
    Sonar_Category_Index             : byte;
    Sonar_Classification             : byte;
    Passive_Int_Period               : integer;
    Active_Int_Period                : integer;
    TIOW_Short_Range                 : single;
    TIOW_Medium_Range                : single;
    TIOW_Long_Range                  : single;
    Passive_Detect_Range             : single;
    Active_Detect_Range              : single;
    Max_Detect_Range                 : single;
    Known_Signal_Source              : single;
    Known_Cross_Section              : single;
    Sonar_Directivity_Index          : single;
    Active_Operating_Power           : single;
    Active_Freq_of_Op                : single;
    Passive_Freq_of_Op               : single;
    Max_Operating_Depth              : single;
    Sonar_Depth_Rate_of_Change       : single;
    Depth_per_Speed                  : single;
    Kinking_Processing               : ByteBool;
    Turn_Rate_2_Kink                 : single;
    Time_2_Settle_Kinked             : integer;
    Bearing_Processing               : ByteBool;
    Time_2_Resolve_Bearing           : integer;
    Passive_Processing               : ByteBool;
    Target_Identification            : ByteBool;
    Time_2_Identify                  : integer;
    Curve_Detection_Index            : integer;
    Track_Analysis                   : byte;
    Time_2_Provide_Track             : integer;
    Ownship_Increase_due_to_Active   : single;
    Tow_Speed                        : single;
    Minimum_Depth                    : single;
    Maximum_Tow_Speed                : single;
    Maximum_Sonar_Speed              : single;
    Depth_Finding_Capable            : ByteBool;
    Tracking_Capable                 : ByteBool;
    Surface_Detection_Capable        : ByteBool;
    SubSurface_Detection_Capable     : ByteBool;
    Torpedo_Detection_Capable        : ByteBool;
    Mine_Detection_Capable           : ByteBool;
    Cable_Length                     : single;
    Maximum_Reported_Bearing_Error   : single;
    Average_Beam_Width               : single;
    Counter_Detection_Factor         : single;
  end;

  {*------------------------------------------------------------------------------
  Kelas TDBSonobuoy_Definition untuk definisi tabel Sonobuoy_Definition
  -------------------------------------------------------------------------------}
  TDBSonobuoy_Definition =  class
    Sonobuoy_Index         : integer;
    Class_Identifier       : string[60];
    Sonobuoy_Type          : byte;
    Platform_Domain        : byte;
    Platform_Category      : byte;
    Platform_Type          : byte;
    Endurance_Time         : integer;
    Max_Depth              : single;
    Length                 : single;
    Width                  : single;
    Height                 : single;
    Front_Acoustic_Cross   : single;
    Side_Acoustic_Cross    : single;
    Damage_Capacity        : integer;
    CPA_Detection_Capable  : byte;
    CPA_Range_Limit        : single;
    Sonar_Index            : integer;
  end;

  {*------------------------------------------------------------------------------
  Kelas TDBTowed_Jammer_Decoy_Definition untuk definisi tabel Towed_Jammer_Decoy_Definition
  -------------------------------------------------------------------------------}
  TDBTowed_Jammer_Decoy_Definition =  class
    Towed_Decoy_Index          : integer;
    Towed_Decoy_Identifier     : string[60];
    Decoy_TARH_Capable         : byte;
    Decoy_SARH_Capable         : byte;
    Platform_Domain            : byte;
    Platform_Category          : byte;
    Platform_Type              : byte;
    Length                     : single;
    Width                      : single;
    Height                     : single;
    Front_Radar_Cross          : single;
    Side_Radar_Cross           : single;
    Front_Visual_Cross         : single;
    Side_Visual_Cross          : single;
    Front_Acoustic_Cross       : single;
    Side_Acoustic_Cross        : single;
    Type_A_Seducing_Prob       : single;
    Type_B_Seducing_Prob       : single;
    Type_C_Seducing_Prob       : single;
    Activation_Control_Delay   : single;
    Tow_Length                 : single;
    ECM_Type                   : byte;
  end;

  {*------------------------------------------------------------------------------
  Kelas TDBScenario_Definition untuk definisi tabel Scenario_Definition
  -------------------------------------------------------------------------------}
  TDBScenario_Definition =  class
    Scenario_Index         : integer;
    Scenario_Identifier    : string[60];
    Resource_Alloc_Index   : integer;
  end;

  {*------------------------------------------------------------------------------
  Kelas TDBResource_Allocation untuk definisi tabel Resource_Allocation
  -------------------------------------------------------------------------------}
  TDBResource_Allocation =  class
    Resource_Alloc_Index   : integer;
    Allocation_Identifier  : string[60];
    Game_Enviro_Index      : integer;
    Defaults_Index         : integer;
    Role_List_Index        : integer;
    Game_Start_Time        : double;
  end;

  {*------------------------------------------------------------------------------
  Kelas TDBGame_Environment_Definition untuk definisi tabel Game_Environment_Definition
  -------------------------------------------------------------------------------}
  TDBGame_Environment_Definition =  class
    Game_Enviro_Index                : integer;
    Game_Enviro_Identifier           : string[60];
    Game_Area_Index                  : integer;
    Game_Area_Index_2                : integer;
    Game_Area_Index_3                : integer;
    Wind_Speed                       : single;
    Wind_Direction                   : single;
    Daytime_Visual_Modifier          : single;
    Nighttime_Visual_Modifier        : single;
    Daytime_Infrared_Modifier        : single;
    Nighttime_Infrared_Modifier      : single;
    Sunrise                          : integer;
    Sunset                           : integer;
    Period_of_Twilight               : integer;
    Rain_Rate                        : byte;
    Cloud_Base_Height                : single;
    Cloud_Attenuation                : byte;
    Sea_State                        : byte;
    Ocean_Current_Speed              : single;
    Ocean_Current_Direction          : single;
    Thermal_Layer_Depth              : single;
    Sound_Velocity_Type              : byte;
    Surface_Sound_Speed              : single;
    Layer_Sound_Speed                : single;
    Bottom_Sound_Speed               : single;
    Bottomloss_Coefficient           : byte;
    Ave_Ocean_Depth                  : single;
    CZ_Active                        : byte;
    Surface_Ducting_Active           : byte;
    Upper_Limit_Surface_Duct_Depth   : single;
    Lower_Limit_Surface_Duct_Depth   : single;
    Sub_Ducting_Active               : byte;
    Upper_Limit_Sub_Duct_Depth       : single;
    Lower_Limit_Sub_Duct_Depth       : single;
    Shipping_Rate                    : byte;
    Shadow_Zone_Trans_Loss           : single;
    Atmospheric_Refract_Modifier     : single;
    Barometric_Pressure              : single;
    Air_Temperature                  : single;
    Surface_Temperature              : single;
    Start_HF_Range_Gap               : single;
    End_HF_Range_Gap                 : single;

    { from global convergence zone table }
    Occurance_Range                  : Single;
    Width                            : single;
    Signal_Reduction_Term            : Single;
    Increase_per_CZ                  : Single;
    Max_Sonar_Depth                  : Single;
  end;

  {*------------------------------------------------------------------------------
  Kelas TDBGame_Area_Definition untuk definisi tabel Game_Area_Definition
  -------------------------------------------------------------------------------}
  TDBGame_Area_Definition =  class
    Game_Area_Index          : integer;
    Game_Area_Identifier     : string[60];
    Game_Centre_Lat          : double;
    Game_Centre_Long         : double;
    Game_X_Dimension         : single;
    Game_Y_Dimension         : single;
    Use_Real_World           : byte;
    Use_Artificial_Landmass  : byte;
    Detail_Map               : string[50];
  end;

  {*------------------------------------------------------------------------------
  Kelas TDBGame_Defaults untuk definisi tabel Game_Defaults
  -------------------------------------------------------------------------------}
  TDBGame_Defaults =  class
    Defaults_Index                   : integer;
    Defaults_Identifier              : string[60];
    Init_AOP                         : integer;
    AOP_Decrease_Rate                : single;
    Sono_Num_2_Initiate              : integer;
    Trans_Range_2_Air                : single;
    Trans_Range_2_Sur                : single;
    Init_AOP_Modifier                : single;
    Visual_Detect_Range              : single;
    Known_Cross_Section              : single;
    Max_Visual_Range                 : single;
    EO_Detection_Factor              : single;
    Visual_Detection_Factor          : single;
    EO_Ident_Factor                  : single;
    Visual_Ident_Factor              : single;
    Sine_Period_Distance             : single;
    Sine_Period_Amplitude            : single;
    Short_Period_Distance            : single;
    Short_Period_Amplitude           : single;
    Long_Period_Distance             : single;
    Long_Period_Amplitude            : single;
    Very_Period_Distance             : single;
    Very_Period_Amplitude            : single;
    Air_Lost_Time                    : integer;
    Sur_Lost_Time                    : integer;
    Sub_Lost_Time                    : integer;
    ESM_Bearing_Lost_Time            : integer;
    Sonar_Bearing_Lost_Time          : integer;
    Stale_Air_Time                   : integer;
    Stale_Sur_Time                   : integer;
    Stale_Sub_Time                   : integer;
    Stale_ESM_Bearing_Time           : integer;
    Stale_Sonar_Bearing_Time         : integer;
    POD_Check_Time                   : integer;
    TMA_Range_Rate                   : single;
    Frequency_Identity_Weighting     : single;
    PRF_Identity_Weighting           : single;
    Pulsewidth_Identity_Weighting    : single;
    Scan_Period_Identity_Weighting   : single;
    Crew_Eff_Heading_Error           : single;
    Crew_Eff_Speed_Error             : single;
    TMA_Relative_Bearing_Rate        : single;
    Passive_Sonar_Max_Course_Error   : single;
    Passive_Sonar_Max_Speed_Error    : single;
    ESM_Error_Corr_Rate              : single;
    Chaff_Altitude_Threshold         : single;
    MHS_Flash_Delay_Time             : single;
    MHS_Immed_Delay_Time             : single;
    MHS_Priority_Delay_Time          : single;
    MHS_Routine_Delay_Time           : single;
    Max_UWT_Range                    : single;
    Max_HF_Detect_Range              : single;
    Max_UHF_Detect_Range             : single;
    Max_IFF_Range                    : single;
    Track_History_Air_Sample_Rate    : integer;
    Track_History_Air_Max_Points     : integer;
    Track_History_Sample_Rate        : integer;
    Track_History_Max_Points         : integer;
    Auto_Gun_Interception_Range      : single;
    Auto_Gun_Threshold_Speed         : single;
    Clutter_Reduction_Scale          : single;
    Jam_Break_Lock_Time_Interval     : integer;
    Missile_Reacquisition_Time       : integer;
    Seduction_Bloom_Altitude         : integer;
    Seduction_Bloom_Range            : single;
    HF_Datalink_MHS_Trans_Freq       : single;
    UHF_Datalink_MHS_Trans_Freq      : single;
    Max_Num_Radar_Classes            : integer;
    Max_Num_Sonar_Classes            : integer;
    Max_Num_Sonobuoy_Classes         : integer;
    Max_Num_EO_Classes               : integer;
    Max_Num_ESM_Classes              : integer;
    Max_Num_MAD_Classes              : integer;
    Max_Num_Fitted_Weap_Classes      : integer;
    Max_Num_Point_Effect_Classes     : integer;
    HAFO_Min_Range                   : single;
    HAFO_Max_Range                   : single;
    Engage_Guide_Stale_Target_Time   : integer;
    Outrun_Guide_Stale_Target_Time   : integer;
    Shadow_Guide_Stale_Target_Time   : integer;
    Sonobuoy_Air_Deceleration        : single;
    Sonobuoy_Air_Descent_Rate        : single;
    Depth_Charge_Air_Deceleration    : single;
    Depth_Charge_Air_Descent_Rate    : single;
    Missile_Sea_Check_Interval       : integer;
  end;

  {*------------------------------------------------------------------------------
  Kelas TDBGame_Cloud_On_ESM untuk definisi tabel Game_Cloud_On_ESM
  -------------------------------------------------------------------------------}
  TDBGame_Cloud_On_ESM =  class
    Defaults_Index   : integer;
    Radar_Frequency  : single;
    Cloud_0_Effect   : single;
    Cloud_1_Effect   : single;
    Cloud_2_Effect   : single;
    Cloud_3_Effect   : single;
    Cloud_4_Effect   : single;
    Cloud_5_Effect   : single;
    Cloud_6_Effect   : single;
  end;

  {*------------------------------------------------------------------------------
  Kelas TDBGame_Cloud_On_Radar untuk definisi tabel Game_Cloud_On_Radar
  -------------------------------------------------------------------------------}
  TDBGame_Cloud_On_Radar =  class
    Defaults_Index   : integer;
    Radar_Frequency  : single;
    Cloud_0_Effect   : single;
    Cloud_1_Effect   : single;
    Cloud_2_Effect   : single;
    Cloud_3_Effect   : single;
    Cloud_4_Effect   : single;
    Cloud_5_Effect   : single;
    Cloud_6_Effect   : single;
  end;

  {*------------------------------------------------------------------------------
  Kelas TDBGame_Default_IFF_Mode_Code untuk definisi tabel Game_Default_IFF_Mode_Code
  -------------------------------------------------------------------------------}
  TDBGame_Default_IFF_Mode_Code =  class
    Defaults_Index     : integer;
    Force_Designation  : byte;
    IFF_Device_Type    : byte;
    IFF_Mode           : byte;
    IFF_Code           : integer;
    Mode_State         : byte;
  end;

  {*------------------------------------------------------------------------------
  Kelas TDBGame_Rainfall_On_ESM untuk definisi tabel Game_Rainfall_On_ESM
  -------------------------------------------------------------------------------}
  TDBGame_Rainfall_On_ESM =  class
    Defaults_Index   : integer;
    Radar_Frequency  : single;
    Rain_0_Effect    : single;
    Rain_1_Effect    : single;
    Rain_2_Effect    : single;
    Rain_3_Effect    : single;
    Rain_4_Effect    : single;
    Rain_5_Effect    : single;
    Rain_6_Effect    : single;
  end;

  {*------------------------------------------------------------------------------
  Kelas TDBGame_Rainfall_On_Missile_Seeker untuk definisi tabel Game_Rainfall_On_Missile_Seeker
  -------------------------------------------------------------------------------}
  TDBGame_Rainfall_On_Missile_Seeker =  class
    Defaults_Index   : integer;
    Guide_Type       : byte;
    Rain_0_Effect    : single;
    Rain_1_Effect    : single;
    Rain_2_Effect    : single;
    Rain_3_Effect    : single;
    Rain_4_Effect    : single;
    Rain_5_Effect    : single;
    Rain_6_Effect    : single;
  end;

  {*------------------------------------------------------------------------------
  Kelas TDBGame_Rainfall_On_Radar untuk definisi tabel Game_Rainfall_On_Radar
  -------------------------------------------------------------------------------}
  TDBGame_Rainfall_On_Radar =  class
    Defaults_Index   : integer;
    Radar_Frequency  : single;
    Rain_0_Effect    : single;
    Rain_1_Effect    : single;
    Rain_2_Effect    : single;
    Rain_3_Effect    : single;
    Rain_4_Effect    : single;
    Rain_5_Effect    : single;
    Rain_6_Effect    : single;
  end;

  {*------------------------------------------------------------------------------
  Kelas TDBGame_Rainfall_On_Sonar untuk definisi tabel Game_Rainfall_On_Sonar
  -------------------------------------------------------------------------------}
  TDBGame_Rainfall_On_Sonar =  class
    Defaults_Index   : integer;
    Sonar_Frequency  : single;
    Rain_0_Effect    : single;
    Rain_1_Effect    : single;
    Rain_2_Effect    : single;
    Rain_3_Effect    : single;
    Rain_4_Effect    : single;
    Rain_5_Effect    : single;
    Rain_6_Effect    : single;
  end;

  {*------------------------------------------------------------------------------
  Kelas TDBGame_Sea_On_Missile_Safe_Altitude untuk definisi tabel Game_Sea_On_Missile_Safe_Altitude
  -------------------------------------------------------------------------------}
  TDBGame_Sea_On_Missile_Safe_Altitude =  class
    Defaults_Index   : integer;
    Sea_0_Effect     : single;
    Sea_1_Effect     : single;
    Sea_2_Effect     : single;
    Sea_3_Effect     : single;
    Sea_4_Effect     : single;
    Sea_5_Effect     : single;
    Sea_6_Effect     : single;
    Sea_7_Effect     : single;
    Sea_8_Effect     : single;
    Sea_9_Effect     : single;
  end;

  {*------------------------------------------------------------------------------
  Kelas TDBGame_Sea_On_Radar untuk definisi tabel Game_Sea_On_Radar
  -------------------------------------------------------------------------------}
  TDBGame_Sea_On_Radar =  class
    Defaults_Index   : integer;
    Sea_0_Effect     : single;
    Sea_1_Effect     : single;
    Sea_2_Effect     : single;
    Sea_3_Effect     : single;
    Sea_4_Effect     : single;
    Sea_5_Effect     : single;
    Sea_6_Effect     : single;
    Sea_7_Effect     : single;
    Sea_8_Effect     : single;
    Sea_9_Effect     : single;
  end;

  {*------------------------------------------------------------------------------
  Kelas TDBGame_Sea_On_Sonar untuk definisi tabel Game_Sea_On_Sonar
  -------------------------------------------------------------------------------}
  TDBGame_Sea_On_Sonar =  class
    Defaults_Index   : integer;
    Sonar_Frequency  : single;
    Sea_0_Effect     : single;
    Sea_1_Effect     : single;
    Sea_2_Effect     : single;
    Sea_3_Effect     : single;
    Sea_4_Effect     : single;
    Sea_5_Effect     : single;
    Sea_6_Effect     : single;
    Sea_7_Effect     : single;
    Sea_8_Effect     : single;
    Sea_9_Effect     : single;
  end;

  {*------------------------------------------------------------------------------
  Kelas TDBGame_Ship_On_Sonar untuk definisi tabel Game_Ship_On_Sonar
  -------------------------------------------------------------------------------}
  TDBGame_Ship_On_Sonar =  class
    Defaults_Index       : integer;
    Sonar_Frequency      : single;
    Distant_Ship_Effect  : single;
    Light_Ship_Effect    : single;
    Medium_Ship_Effect   : single;
    Heavy_Ship_Effect    : single;
  end;

  {*------------------------------------------------------------------------------
  Kelas TDBAsset_Deployment_Definition untuk definisi tabel TDBAsset_Deployment_Definition
  -------------------------------------------------------------------------------}
  TDBAsset_Deployment_Definition =  class
    Deployment_Index       : integer;
    Deployment_Identifier  : string[60];
    Scenario_Index         : integer;
  end;

  {*------------------------------------------------------------------------------
  Kelas TDBPlatformInstance untuk definisi stored proc get_platform_instances
  -------------------------------------------------------------------------------}
  TDBPlatformInstance = class
    Platform_Instance_Index : integer;
    Platform_Index          : integer;
    Platform_Type           : Byte;
    Instance_Name           : string[100];
    Track_ID                : string[20];
    ForceDesignation        : Byte;
  end;

  {*------------------------------------------------------------------------------
  Kelas TDBSubArea_Enviro_Definition untuk definisi Tabel SubArea_Enviro_Definition
  -------------------------------------------------------------------------------}
  TDBSubArea_Enviro_Definition =  class
    Enviro_Index                   : integer; //sub area index
    Game_Enviro_Index              : integer; //enviro_index
    Enviro_Identifier              : string[60];
    X_Position_1                   : single;
    Y_Position_1                   : single;
    X_Position_2                   : single;
    Y_Position_2                   : single;
    Latitude_1                     : double;
    Longitude_1                    : double;
    Latitude_2                     : double;
    Longitude_2                    : double;
    Wind_Speed                     : single;
    Wind_Direction                 : single;
    Daytime_Visual_Modifier        : single;
    Nighttime_Visual_Modifier      : single;
    Daytime_Infrared_Modifier      : single;
    Nighttime_Infrared_Modifier    : single;
    Rain_Rate                      : byte;
    Cloud_Base_Height              : single;
    Cloud_Attenuation              : byte;
    Sea_State                      : byte;
    Ocean_Current_Speed            : single;
    Ocean_Current_Direction        : single;
    Thermal_Layer_Depth            : single;
    Sound_Velocity_Type            : byte;
    Surface_Sound_Speed            : single;
    Layer_Sound_Speed              : single;
    Bottom_Sound_Speed             : single;
    Bottomloss_Coefficient         : byte;
    Ave_Ocean_Depth                : single;
    CZ_Active                      : byte;
    Surface_Ducting_Active         : byte;
    Upper_Limit_Sur_Duct_Depth     : single;
    Lower_Limit_Sur_Duct_Depth     : single;
    Sub_Ducting_Active             : byte;
    Upper_Limit_Sub_Duct_Depth     : single;
    Lower_Limit_Sub_Duct_Depth     : single;
    Shipping_Rate                  : byte;
    Shadow_Zone_Trans_Loss         : single;
    Atmospheric_Refract_Modifier   : single;
    Barometric_Pressure            : single;
    Air_Temperature                : single;
    Surface_Temperature            : single;
    HF_Black_Hole                  : byte;
  end;

  {*------------------------------------------------------------------------------
  Kelas TDBRuntime_Platform_Library untuk definisi Tabel Runtime_Platform_Library
  -------------------------------------------------------------------------------}
  TDBRuntime_Platform_Library =  class
    Platform_Library_Index   : integer;
    Library_Name             : string[60];
  end;
  TArrDBRuntime_Platform_Library = array of TDBRuntime_Platform_Library;

  {*------------------------------------------------------------------------------
  Kelas TDBPlatform_Library_Entry untuk definisi Tabel Platform_Library_Entry
  -------------------------------------------------------------------------------}
  TDBPlatform_Library_Entry =  class
    Library_Entry_Index  : integer;
    Library_Index        : integer;
    Platform_Type        : byte;
    Platform_Index       : integer;
  end;

  {*------------------------------------------------------------------------------
  Kelas TDBReference_Point untuk definisi Tabel Reference_Point
  -------------------------------------------------------------------------------}
  TDBReference_Point =  class
    Reference_Index        : integer;
    Resource_Alloc_Index   : integer;
    Reference_Identifier   : string[60];
    Force_Designation      : byte;
    Track_Type             : byte;
    Symbol_Type            : integer;
    Course                 : single;
    Speed                  : single;
    X_Position             : single;
    Y_Position             : single;
    Latitude               : double;
    Longitude              : double;
    Track_Bearing          : single;
    AOP_Start_Time_Offset  : integer;
  end;

  {*------------------------------------------------------------------------------
  Kelas TDBPlatform_Activation untuk definisi Tabel Platform_Activation
  -------------------------------------------------------------------------------}
  TDBPlatform_Activation =  class
    Platform_Event_Index         : integer;
    Deployment_Index             : integer;
    Platform_Instance_Index      : integer;
    Platform_Activation_Time     : integer;
    Init_Guidance_Type           : byte;
    Init_Position_Latitude       : double;
    Init_Position_Longitude      : double;
    Init_Position_Cartesian_X    : single;
    Init_Position_Cartesian_Y    : single;
    Init_Altitude                : single;
    Init_Course                  : single;
    Init_Helm_Angle              : single;
    Init_Ground_Speed            : byte;
    Init_Vertical_Speed          : byte;
    Init_Command_Altitude        : single;
    Init_Command_Course          : single;
    Init_Command_Helm_Angle      : single;
    Init_Command_Ground          : byte;
    Init_Command_Vert            : byte;
    Deg_of_Rotation              : single;
    Radius_of_Travel             : single;
    Direction_of_Travel          : byte;
    Circle_Latitude              : double;
    Circle_Longitude             : double;
    Circle_X                     : single;
    Circle_Y                     : single;
    Dynamic_Circle_Range_Offset  : single;
    Dynamic_Circle_Angle_Offset  : byte;
    Dynamic_Circle_Offset_Mode   : byte;
    Period_Distance              : single;
    Amplitude_Distance           : single;
    Zig_Zag_Leg_Type             : byte;
    Target_Angle_Offset          : single;
    Target_Angle_Type            : byte;
    Target_Range                 : single;
    Guidance_Target              : integer;
    Pattern_Instance_Index       : integer;
    Angular_Offset               : single;
    Anchor_Cartesian_X           : single;
    Anchor_Cartesian_Y           : single;
    Anchor_Latitude              : single;
    Anchor_Longitude             : single;
    Current_Drift                : byte;
    Waypoint_Termination         : byte;
    Termination_Heading          : single;
    Cond_List_Instance_Index     : integer;
    Damage                       : single;
  end;

  {*------------------------------------------------------------------------------
  Kelas TDBHelicopter_Land_Launch_Limits untuk definisi Tabel Helicopter_Land_Launch_Limits
  -------------------------------------------------------------------------------}
  TDBHelicopter_Land_Launch_Limits =  class
    Vehicle_Index                : integer;
    Max_Relative_Wind_Magnitude  : single;
    Max_Turn_Rate_To_Launch      : integer;
    Max_Turn_Rate_To_Land        : integer;
    Max_Landing_Altitude         : single;
    Max_Relative_Speed           : integer;
    Approach_Range               : single;
    Approach_Center_Bearing      : integer;
    Approach_Sector_Width        : integer;
  end;

  {*------------------------------------------------------------------------------
  Kelas TDBMotion_Characteristics untuk definisi Tabel Motion_Characteristics
  -------------------------------------------------------------------------------}
  TDBMotion_Characteristics =  class
    Motion_Index               : integer;
    Motion_Identifier          : string[60];
    Motion_Type                : byte;
    Max_Altitude               : single;
    Max_Depth                  : single;
    Min_Ground_Speed           : single;
    Cruise_Ground_Speed        : single;
    High_Ground_Speed          : single;
    Max_Ground_Speed           : single;
    Acceleration               : single;
    Deceleration               : single;
    Normal_Climb_Rate          : single;
    Max_Climb_Rate             : single;
    Normal_Descent_Rate        : single;
    Max_Descent_Rate           : single;
    Vertical_Accel             : single;
    Standard_Turn_Rate         : single;
    Tight_Turn_Rate            : single;
    Max_Helm_Angle             : single;
    Helm_Angle_Rate            : single;
    Speed_Reduce_In_Turn       : single;
    Time_To_Reduce_Speed       : single;
    Min_Speed_To_Reduce        : single;
    Rate_of_Turn_Rate_Chg      : single;
    Min_Pitch_Angle            : single;
    Max_Pitch_Angle            : single;
    Max_Roll_Angle             : single;
    Endurance_Type             : byte;
    Endurance_Time             : integer;
    Max_Effective_Range        : single;
    Fuel_Unit_Type             : byte;
    Max_Fuel_Capacity          : single;
    Min_Speed_Fuel_Consume     : double;
    Cruise_Speed_Fuel_Consume  : double;
    High_Speed_Fuel_Consume    : double;
    Max_Speed_Fuel_Consume     : double;
  end;


  {*------------------------------------------------------------------------------
  Kelas TDBEO_On_Board untuk definisi Tabel EO_On_Board
  -------------------------------------------------------------------------------}
  TDBEO_On_Board =  class
    EO_Instance_Index    : integer;
    Instance_Identifier  : string[80];
    Instance_Type        : byte;
    Vehicle_Index        : integer;
    EO_Index             : integer;
    Antenna_Height       : single;
  end;

  {*------------------------------------------------------------------------------
  Kelas TDBESM_On_Board untuk definisi Tabel ESM_On_Board
  -------------------------------------------------------------------------------}
  TDBESM_On_Board =  class
    ESM_Instance_Index         : integer;
    Instance_Identifier        : string[80];
    Instance_Type              : byte;
    Vehicle_Index              : integer;
    ESM_Index                  : integer;
    Rel_Antenna_Height         : single;
    Max_Operational_Depth      : single;
    Submerged_Antenna_Height   : single;
  end;

  {*------------------------------------------------------------------------------
  Kelas TDBIFF_Sensor_On_Board untuk definisi Tabel IFF_Sensor_On_Board
  -------------------------------------------------------------------------------}
  TDBIFF_Sensor_On_Board =  class
    IFF_Instance_Index         : integer;
    Instance_Identifier        : string[80];
    Instance_Type              : byte;
    Vehicle_Index              : integer;
    IFF_Capability             : byte;
    Rel_Antenna_Height         : single;
    Submerged_Antenna_Height   : single;
    Max_Operational_Depth      : single;
  end;

  {*------------------------------------------------------------------------------
  Kelas TDBMAD_Sensor_On_Board untuk definisi Tabel MAD_Sensor_On_Board
  -------------------------------------------------------------------------------}
  TDBMAD_Sensor_On_Board =  class
    MAD_Instance_Index   : integer;
    Instance_Identifier  : string[80];
    Instance_Type        : byte;
    Vehicle_Index        : integer;
    MAD_Index            : integer;
    Antenna_Height       : single;
  end;

  {*------------------------------------------------------------------------------
  Kelas TDBFCR_On_Board untuk definisi Tabel FCR_On_Board
  -------------------------------------------------------------------------------}
  TDBFCR_On_Board =  class
    FCR_Instance_Index   : integer;
    Instance_Identifier  : string[80];
    Instance_Type        : byte;
    Vehicle_Index        : integer;
    Radar_Index          : integer;
    Rel_Antenna_Height   : single;
  end;

  {*------------------------------------------------------------------------------
  Kelas TDBVisual_Sensor_On_Board untuk definisi Tabel Visual_Sensor_On_Board
  -------------------------------------------------------------------------------}
  TDBVisual_Sensor_On_Board =  class
    Visual_Instance_Index  : integer;
    Instance_Identifier    : string[80];
    Instance_Type          : byte;
    Vehicle_Index          : integer;
    Observer_Height        : single;
  end;

  {*------------------------------------------------------------------------------
  Kelas TDBRadar_On_Board untuk definisi Tabel Radar_On_Board
  -------------------------------------------------------------------------------}
  TDBRadar_On_Board =  class
    Radar_Instance_Index       : integer;
    Instance_Identifier        : string[80];
    Instance_Type              : byte;
    Vehicle_Index              : integer;
    Radar_Index                : integer;
    Rel_Antenna_Height         : single;
    Submerged_Antenna_Height   : single;
    Max_Operational_Depth      : single;
  end;

  {*------------------------------------------------------------------------------
  Kelas TDBSonar_On_Board untuk definisi Tabel Sonar_On_Board
  -------------------------------------------------------------------------------}
  TDBSonar_On_Board =  class
    Sonar_Instance_Index   : integer;
    Instance_Identifier    : string[80];
    Instance_Type          : byte;
    Vehicle_Index          : integer;
    Sonar_Index            : integer;
    Minimum_Depth          : single;
    Time_2_Deploy          : integer;
    Time_2_Stow            : integer;
  end;

  {*------------------------------------------------------------------------------
  Kelas TDBFormation_Definition untuk definisi Tabel Formation_Definition
  -------------------------------------------------------------------------------}
  TDBFormation_Definition =  class
    Formation_Index        : integer;
    Formation_Identifier   : string[60];
    Force_Designation      : byte;
    Formation_Leader       : integer;
    Angle_Type             : byte;
    Deployment_Index       : integer;
    Instance_Name          : string[60];
  end;

  {*------------------------------------------------------------------------------
  Kelas TDBFormation_Assignment untuk definisi Tabel Formation_Assignment
  -------------------------------------------------------------------------------}
  TDBFormation_Assignment =  class
    Platform_Instance_Index  : integer;
    Formation_Index          : integer;
    Angle_Offset             : single;
    Range_from_Leader        : single;
    Altitude                 : single;
    Force_Designation        : byte;
    Instance_Name            : string[60];
  end;

  {*------------------------------------------------------------------------------
  Kelas TDBFitted_Weapon_On_Board untuk definisi Tabel Fitted_Weapon_On_Board
  Mount_type : 1 = Missile/Hybrid, 2 = Torped, 3 = Mine
  -------------------------------------------------------------------------------}
  TDBFitted_Weapon_On_Board =  class
    Fitted_Weap_Index      : integer;
    Instance_Identifier    : string[80];
    Instance_Type          : byte;
    Vehicle_Index          : integer;
    Mount_Type             : byte;
    Launch_Angle           : single;
    Launch_Angle_Required  : byte;
    Quantity               : integer;
    Firing_Delay           : Single;
    Weapon_Index           : integer;
  end;

  {*------------------------------------------------------------------------------
  Kelas TDBPoint_Effect_On_Board untuk definisi Tabel Point_Effect_On_Board
  Mount_type : 1 = Gun, 2 = Bomb
  -------------------------------------------------------------------------------}
  TDBPoint_Effect_On_Board =  class
    Point_Effect_Index   : integer;
    Instance_Identifier  : string[80];
    Instance_Type        : byte;
    Vehicle_Index        : integer;
    Mount_Type           : byte;
    Quantity             : integer;
    Weapon_Index         : integer;
  end;

  {*------------------------------------------------------------------------------
  Kelas TDBAcoustic_Decoy_On_Board untuk definisi Tabel Acoustic_Decoy_On_Board
  -------------------------------------------------------------------------------}
  TDBAcoustic_Decoy_On_Board =  class
    Acoustic_Instance_Index  : integer;
    Instance_Identifier      : string[80];
    Instance_Type            : byte;
    Quantity                 : integer;
    Decoy_Index              : integer;
  end;

  {*------------------------------------------------------------------------------
  Kelas TDBAir_Bubble_Mount untuk definisi Tabel Air_Bubble_Mount
  -------------------------------------------------------------------------------}
  TDBAir_Bubble_Mount =  class
    Air_Bubble_Instance_Index  : integer;
    Instance_Identifier        : string[80];
    Instance_Type              : byte;
    Bubble_Qty_On_Board        : integer;
    Vehicle_Index              : integer;
    Air_Bubble_Index           : integer;
  end;

  {*------------------------------------------------------------------------------
  Kelas TDBChaff_On_Board untuk definisi Tabel Chaff_On_Board
  -------------------------------------------------------------------------------}
  TDBChaff_On_Board =  class
    Chaff_Instance_Index   : integer;
    Instance_Identifier    : string[80];
    Instance_Type          : byte;
    Vehicle_Index          : integer;
    Chaff_Qty_On_Board     : integer;
    Chaff_Index            : integer;
  end;

  {*------------------------------------------------------------------------------
  Kelas TDBChaff_Launcher_On_Board untuk definisi Tabel Chaff_Launcher_On_Board
  -------------------------------------------------------------------------------}
  TDBChaff_Launcher_On_Board =  class
    Launcher_Number      : byte;
    Launcher_Angle       : single;
    Launcher_Kind        : byte;
    Def_Bloom_Range      : single;
    Def_Bloom_Altitude   : single;
    Max_Range            : single;
    Min_Range            : single;
    Max_Elevation        : single;
    Min_Elevation        : single;
    Average_Launch_Spd   : single;
  end;

  {*------------------------------------------------------------------------------
  Kelas TDBDefensive_Jammer_On_Board untuk definisi Tabel Defensive_Jammer_On_Board
  -------------------------------------------------------------------------------}
  TDBDefensive_Jammer_On_Board =  class
    Defensive_Jammer_Instance_Index  : integer;
    Instance_Identifier              : string[80];
    Instance_Type                    : byte;
    Vehicle_Index                    : integer;
    Defensive_Jammer_Index           : integer;
  end;

  {*------------------------------------------------------------------------------
  Kelas TDBFloating_Decoy_On_Board untuk definisi Tabel Floating_Decoy_On_Board
  -------------------------------------------------------------------------------}
  TDBFloating_Decoy_On_Board =  class
    Floating_Decoy_Instance_Index  : integer;
    Instance_Identifier            : string[80];
    Instance_Type                  : byte;
    Quantity                       : integer;
    Floating_Decoy_Index           : integer;
  end;

  {*------------------------------------------------------------------------------
  Kelas TDBInfrared_Decoy_On_Board untuk definisi Tabel Infrared_Decoy_On_Board
  -------------------------------------------------------------------------------}
  TDBInfrared_Decoy_On_Board =  class
    Infrared_Decoy_Instance_Index  : integer;
    Instance_Identifier            : string[80];
    Instance_Type                  : byte;
    Infrared_Decoy_Qty_On_Board    : integer;
    Infrared_Decoy_Index           : integer;
  end;

  {*------------------------------------------------------------------------------
  Kelas TDBJammer_On_Board untuk definisi Tabel Jammer_On_Board
  -------------------------------------------------------------------------------}
  TDBJammer_On_Board =  class
    Jammer_Instance_Index  : integer;
    Instance_Identifier    : string[80];
    Instance_Type          : byte;
    Jammer_Index           : integer;
    Antenna_Height         : single;
  end;

  {*------------------------------------------------------------------------------
  Kelas TDBTowed_Jammer_Decoy_On_Board untuk definisi Tabel Towed_Jammer_Decoy_On_Board
  -------------------------------------------------------------------------------}
  TDBTowed_Jammer_Decoy_On_Board =  class
    Towed_Decoy_Instance_Index   : integer;
    Instance_Identifier          : string[80];
    Instance_Type                : byte;
    Quantity                     : integer;
    Towed_Decoy_Index            : integer;
  end;

  {*------------------------------------------------------------------------------
  Kelas TDBSonobuoy_On_Board untuk definisi Tabel Sonobuoy_On_Board
  -------------------------------------------------------------------------------}
  TDBSonobuoy_On_Board =  class
    Sonobuoy_Instance_Index  : integer;
    Instance_Identifier      : string[80];
    Instance_Type            : byte;
    Sonobuoy_Index           : integer;
    Quantity                 : integer;
    Sonar_Instance_Index     : integer;
  end;

  {*------------------------------------------------------------------------------
  Kelas TDBOverlay_Mapping untuk definisi Tabel Resource_Overlay_Mapping & Overlay_Definition
  -------------------------------------------------------------------------------}
  TDBOverlay_Mapping =  class
    Overlay_Instance_Index : integer;
    Overlay_Index          : integer;
    Overlay_Identifier     : string[60];
    Overlay_Filename       : string[60];
    Static_Overlay         : byte;
    Game_Area_Index        : integer;
    Domain                 : byte;
    Force                  : byte;
  end;

  {*------------------------------------------------------------------------------
  Kelas TDBExternal_Communication_Channel untuk definisi Tabel External_Communication_Channel
  -------------------------------------------------------------------------------}
  TDBExternal_Communication_Channel =  class
    Comms_Channel_Index    : integer;
    Resource_Alloc_Index   : integer;
    Channel_Number         : integer;
    Channel_Identifier     : string[60];
    Comms_Band             : byte;
    Channel_Freq           : double;
    Channel_Security       : byte;
    Channel_Code           : string[6];
  end;

  {*------------------------------------------------------------------------------
  Kelas TDBExternal_Communication_Channel_Group untuk definisi Tabel External_Communication_Channel
  -------------------------------------------------------------------------------}
  TDBExternal_Communication_Channel_Group =  class
    Channel_Slot           : integer;
    Comms_Channel_Index    : integer;
    Resource_Alloc_Index   : integer;
    Channel_Number         : integer;
    Channel_Identifier     : string[60];
    Comms_Band             : byte;
    Channel_Freq           : double;
    Channel_Security       : byte;
    Channel_Code           : string[6];
  end;

  {*------------------------------------------------------------------------------
  Kelas TDBLink_Definition untuk definisi Tabel Link_Definition
  -------------------------------------------------------------------------------}
  TDBLink_Definition =  class
    Link_Index           : integer;
    Link_Identifier_Num  : byte;
    Link_Force           : byte;
    Link_Controller      : integer;
    Deployment_Index     : integer;
    Trans_Mode           : byte;
    Link_Identifier      : string[60];
  end;

  {*------------------------------------------------------------------------------
  Kelas TDBLink_Participant untuk definisi Tabel Link_Participant
  -------------------------------------------------------------------------------}
  TDBLink_Participant =  class
    Link_Index           : integer;
    Participating_Unit   : integer;
    Deployment_Index     : integer;
    PU_Octal_Code        : integer;
  end;

  {*------------------------------------------------------------------------------
  Kelas TDBCubicle_Group untuk definisi Tabel Cubicle_Group
  -------------------------------------------------------------------------------}
  TDBCubicle_Group =  class
    Group_Index        : integer;
    Deployment_Index   : integer;
    Group_Identifier   : string[60];
    Force_Designation  : byte;
    Tracks_Block       : byte;
    Track_Block_Start  : integer;
    Track_Block_End    : integer;
    Zulu_Zulu          : integer;
  end;

  {*------------------------------------------------------------------------------
  Kelas TDBCubicle_Group_Assignment untuk definisi Tabel Cubicle_Group_Assignment
  -------------------------------------------------------------------------------}
  TDBCubicle_Group_Assignment =  class
    Platform_Instance_Index  : integer;
    Group_Index              : integer;
    Command_Priority         : integer;
    Deployment_Index         : integer;
  end;

  {*------------------------------------------------------------------------------
  Kelas TDBMine_POD_vs_Range untuk definisi Tabel Mine_POD_vs_Range
  -------------------------------------------------------------------------------}
  TDBMine_POD_vs_Range =  class
    List_Index           : integer;
    Mine_Index           : integer;
    Prob_of_Detonation   : single;
    Range                : single;
  end;

  {*------------------------------------------------------------------------------
  Kelas TDBTorpedo_POH_Modifier untuk definisi Tabel Torpedo_POH_Modifier
  -------------------------------------------------------------------------------}
  TDBTorpedo_POH_Modifier =  class
    Torpedo_Index  : integer;
    Aspect_Angle   : byte;
    POH_Modifier   : single;
  end;

  {*------------------------------------------------------------------------------
  Kelas TDBBlind_Zone_Definition untuk definisi Tabel Blind_Zone_Definition
  -------------------------------------------------------------------------------}
  TDBBlind_Zone_Definition =  class
    Blind_Zone_Index       : integer;
    Blind_Zone_Type        : byte;
    BlindZone_Number       : byte;
    Instance_Index         : integer;
    Start_Angle            : single;
    End_Angle              : single;
  end;

  {*------------------------------------------------------------------------------
  Kelas TDBGlobal_Convergence_Zone untuk definisi Tabel Global_Convergence_Zone
  -------------------------------------------------------------------------------}
  TDBGlobal_Convergence_Zone =  record
    Converge_Index         : integer;
    Game_Enviro_Index      : integer;
    Occurance_Range        : single;
    Width                  : single;
    Signal_Reduction_Term  : single;
    Increase_per_CZ        : single;
    Max_Sonar_Depth        : single;
  end;

  {*------------------------------------------------------------------------------
  Kelas TDBFitted_Weap_Launcher_On_Board untuk definisi Tabel Fitted_Weap_Launcher_On_Board
  -------------------------------------------------------------------------------}
  TDBFitted_Weap_Launcher_On_Board =  class
    Fitted_Weap_Index        : integer;
    Launcher_Type            : byte;
    Launcher_Angle_Required  : byte;
    Launcher_Angle           : integer;
    Launcher_Max_Qty         : integer;
  end;

  {*------------------------------------------------------------------------------
  Kelas TDBPOD_vs_SNR_Point untuk definisi Tabel POD_vs_SNR_Point
  -------------------------------------------------------------------------------}
  TDBPOD_vs_SNR_Point =  class
    List_Index               : integer;
    Curve_Definition_Index   : integer;
    SNR_Ratio                : single;
    Prob_of_Detection        : single;
  end;

  {*------------------------------------------------------------------------------
  Kelas TDBRadar_Vertical_Coverage untuk definisi Tabel Radar_Vertical_Coverage
  -------------------------------------------------------------------------------}
  TDBRadar_Vertical_Coverage =  class
    Coverage_Index             : integer;
    Radar_Index                : integer;
    Coverage_Diagram           : byte;
    Vert_Coverage_Range        : single;
    Vert_Cover_Min_Elevation   : single;
    Vert_Cover_Max_Elevation   : single;
  end;

  {*------------------------------------------------------------------------------
    Definisi generics Custom List type
  -------------------------------------------------------------------------------}
  TDBGameCloudOnESMList         = TObjectList<TDBGame_Cloud_On_ESM>;
  TDBGameCloudRadarList         = TObjectList<TDBGame_Cloud_On_Radar>;
  TDBGameDefaultIFFModeList     = TObjectList<TDBGame_Default_IFF_Mode_Code>;
  TDBGameRainfallOnESMList      = TObjectList<TDBGame_Rainfall_On_ESM>;
  TDBGameRainfallOnMissSeekerList= TObjectList<TDBGame_Rainfall_On_Missile_Seeker>;
  TDBGameRainfallOnRadarList    = TObjectList<TDBGame_Rainfall_On_Radar>;
  TDBGameRainfallOnSonarList    = TObjectList<TDBGame_Rainfall_On_Sonar>;
  TDBGameSeaOnSonarlist         = TObjectList<TDBGame_Sea_On_Sonar>;
  TDBGameShipOnSonarList        = TObjectList<TDBGame_Ship_On_Sonar>;
  TDBSubAreaDefinitionList      = TObjectList<TDBSubArea_Enviro_Definition>;

  TDBPlatformLibraryEntryList   = TObjectList<TDBPlatform_Library_Entry>;
  TDBReferencePointList         = TObjectList<TDBReference_Point>;
  TDBEOOnBoardList              = TObjectList<TDBEO_On_Board>;
  TDBESMOnBoardList             = TObjectList<TDBESM_On_Board>;
  TDBIFFOnBoardList             = TObjectList<TDBIFF_Sensor_On_Board>;
  TDBMADOnBoardList             = TObjectList<TDBMAD_Sensor_On_Board>;
  TDBFCROnBoardList             = TObjectList<TDBFCR_On_Board>;
  TDBVisualOnBoardList          = TObjectList<TDBVisual_Sensor_On_Board>;
  TDBRadarOnBoardList           = TObjectList<TDBRadar_On_Board>;
  TDBSonarOnBoardList           = TObjectList<TDBSonar_On_Board>;
  TDBFitWeaponOnBoardList       = TObjectList<TDBFitted_Weapon_On_Board>;
  TDBPointEffectOnBoardList     = TObjectList<TDBPoint_Effect_On_Board>;
  TDBAcousticDecoyOnBoardList   = TObjectList<TDBAcoustic_Decoy_On_Board>;
  TDBAirBubbleOnBoardList       = TObjectList<TDBAir_Bubble_Mount>;
  TDBChaffOnBoardList           = TObjectList<TDBChaff_On_Board>;
  TDBChaffLauncherOnBoardList   = TObjectList<TDBChaff_Launcher_On_Board>;
  TDBDefensiveJammerOnBoardList = TObjectList<TDBDefensive_Jammer_On_Board>;
  TDBFloatingDecoyOnBoardList   = TObjectList<TDBFloating_Decoy_On_Board>;
  TDBInfraredDecoyOnBoardList   = TObjectList<TDBInfrared_Decoy_On_Board>;
  TDBJammerOnBoardList          = TObjectList<TDBJammer_On_Board>;
  TDBTowedJammerOnBoardList     = TObjectList<TDBTowed_Jammer_Decoy_On_Board>;
  TDBSonobuoyOnBoardList        = TObjectList<TDBSonobuoy_On_Board>;
  TDBOverlayMappingList         = TObjectList<TDBOverlay_Mapping>;
  TDBExtCommChannelList         = TObjectList<TDBExternal_Communication_Channel>;
  TDBFormationList              = TObjectList<TDBFormation_Definition>;
  TDBFormationAssignmentList    = TObjectList<TDBFormation_Assignment>;
  TDBLinkDefinitionList         = TObjectList<TDBLink_Definition>;
  TDBLinkParticipantsList       = TObjectList<TDBLink_Participant>;
  TDBCubicleGroupList           = TObjectList<TDBCubicle_Group>;
  TDBCubicleGroupMemberList     = TObjectList<TDBCubicle_Group_Assignment>;
  TDBBlindZoneDefinitionList    = TObjectList<TDBBlind_Zone_Definition>;
  TDBFittedWeaponLauncherList   = TObjectList<TDBFitted_Weap_Launcher_On_Board>;
  TDBExtCommChannelGroupList    = TObjectList<TDBExternal_Communication_Channel_Group>;

  TDBMine_POD_vs_RangeList      = TObjectList<TDBMine_POD_vs_Range>;
  TDBTorpedo_POH_ModifierList   = TObjectList<TDBTorpedo_POH_Modifier>;
  TDBSonar_POD_CurveList        = TObjectList<TDBPOD_vs_SNR_Point>;
  TDBRadar_Vertical_CovList     = TObjectList<TDBRadar_Vertical_Coverage>;

  {*------------------------------------------------------------------------------
    Definisi generic Dictionary type
  -------------------------------------------------------------------------------}
  TDBAcousticDecoyDict            = TObjectDictionary<{Decoy_Index}integer, TDBAcoustic_Decoy_Definition>;
  TDBAirBubbleDict                = TObjectDictionary<{Air_Bubble_Index}integer, TDBAir_Bubble_Definition>;
  TDBBombDefinitionDict           = TObjectDictionary<{Bomb_Index}integer, TDBBomb_Definition>;
  TDBChaffDefinitionDict          = TObjectDictionary<{Chaff_Index}integer, TDBChaff_Definition>;
  TDBDefensiveJammerDefinitionDict= TObjectDictionary<{Defensive_Jammer_Index}integer, TDBDefensive_Jammer_Definition>;
  TDBEODetectionDefinitionDict    = TObjectDictionary<{EO_Index}integer, TDBEO_Detection_Definition>;
  TDBESMDefinitionDict            = TObjectDictionary<{ESM_Index}integer, TDBESM_Definition>;
  TDBFCRDefinitionDict            = TObjectDictionary<{FCR_Index}integer, TDBFCR_Definition>;
  TDBFloatingDecoyDefinitionDict  = TObjectDictionary<{Floating_Decoy_Index}integer, TDBFloating_Decoy_Definition>;
  TDBGunDefinitionDict            = TObjectDictionary<{Gun_Index}integer, TDBGun_Definition>;
  TDBHybridDefinitionDict         = TObjectDictionary<{Hybrid_Index}integer, TDBHybrid_Definition>;
  TDBInfraredDecoyDefinitionDict  = TObjectDictionary<{Infrared_Decoy_Index}integer, TDBInfrared_Decoy_Definition>;
  TDBJammerDefinitionDict         = TObjectDictionary<{Jammer_Index}integer, TDBJammer_Definition>;
  TDBMADDefinitionDict            = TObjectDictionary<{MAD_Index}integer, TDBMAD_Definition>;
  TDBMineDefinitionDict           = TObjectDictionary<{Mine_Index}integer, TDBMine_Definition>;
  TDBMissileDefinitionDict        = TObjectDictionary<{Missile_Index}integer, TDBMissile_Definition>;
  TDBRadarDefinitionDict          = TObjectDictionary<{Radar_Index}integer, TDBRadar_Definition>;
  TDBSatelliteDefinitionDict      = TObjectDictionary<{Satellite_Index}integer, TDBSatellite_Definition>;
  TDBSonarDefinitionDict          = TObjectDictionary<{Sonar_Index}integer, TDBSonar_Definition>;
  TDBSonobuoyDefinitionDict       = TObjectDictionary<{Sonobuoy_Index}integer, TDBSonobuoy_Definition>;
  TDBTorpedoDefinitionDict        = TObjectDictionary<{Torpedo_Index}integer, TDBTorpedo_Definition>;
  TDBTowedJammerDecoyDefDict      = TObjectDictionary<{Towed_Decoy_Index}integer, TDBTowed_Jammer_Decoy_Definition>;
  TDBVehicleDefinitionDict        = TObjectDictionary<{Vehicle_Index}integer, TDBVehicle_Definition>;
  TDBPlatformActivationDict       = TObjectDictionary<{Platform_Instance_Index}integer, TDBPlatform_Activation>;
  TDBHelicopterLandLaunchLimitsDict=TObjectDictionary<{Vehicle_Index}integer, TDBHelicopter_Land_Launch_Limits>;
  TDBMotionCharacteristicsDict    = TObjectDictionary<{Motion_Index}integer, TDBMotion_Characteristics>;
  TDBFormationAssigmentDict       = TObjectDictionary<{formation_Index}integer, TDBFormationAssignmentList>;
  TDBExtCommChannelDict           = TObjectDictionary<{comm ch idx}integer, TDBExternal_Communication_Channel>;
  TDBLinkParticipantsDict         = TObjectDictionary<{link index}integer, TDBLinkParticipantsList>;
  TDBCubicleAssignmentDict        = TObjectDictionary<{group index}integer, TDBCubicleGroupMemberList>;

  TDBMine_POD_vs_RangeDict        = TObjectDictionary<{mine index}integer, TDBMine_POD_vs_RangeList>;
  TDBTorpedo_POH_ModifierDict     = TObjectDictionary<{torpedo index}integer, TDBTorpedo_POH_ModifierList>;
  TDBSonar_POD_CurveDict          = TObjectDictionary<{curve def index}integer, TDBSonar_POD_CurveList>;
  TDBRadar_Vertical_CovDict       = TObjectDictionary<{radar index}integer, TDBRadar_Vertical_CovList>;

  TDBExtCommChannelGroupDict      = TObjectDictionary<{group index}integer,TDBExtCommChannelGroupList>;


  {*------------------------------------------------------------------------------
    Definisi generic dict untuk on board device
  -------------------------------------------------------------------------------}
  TDBEOOnBoardDict                = TObjectDictionary<{Vehicle_Index}integer, TDBEOOnBoardList>;
  TDBESMOnBoardDict               = TObjectDictionary<{Vehicle_Index}integer, TDBESMOnBoardList>;
  TDBIFFSensorOnBoardDict         = TObjectDictionary<{Vehicle_Index}integer, TDBIFFOnBoardList>;
  TDBMADSensorOnBoardDict         = TObjectDictionary<{Vehicle_Index}integer, TDBMADOnBoardList>;
  TDBFCROnBoardDict               = TObjectDictionary<{Vehicle_Index}integer, TDBFCROnBoardList>;
  TDBVisualOnBoardDict            = TObjectDictionary<{Vehicle_Index}integer, TDBVisualOnBoardList>;
  TDBRadarOnBoardDict             = TObjectDictionary<{Vehicle_Index}integer, TDBRadarOnBoardList>;
  TDBSonarOnBoardDict             = TObjectDictionary<{Vehicle_Index}integer, TDBSonarOnBoardList>;
  TDBFitWeaponOnBoardDict         = TObjectDictionary<{Vehicle_Index}integer, TDBFitWeaponOnBoardList>;
  TDBFitWeaponLauncherOnBoardDict = TObjectDictionary<{fitted_weapon_Index}integer, TDBFittedWeaponLauncherList>;
  TDBPointEffectOnBoardDict       = TObjectDictionary<{Vehicle_Index}integer, TDBPointEffectOnBoardList>;
  TDBAcousticDecoyOnBoardDict     = TObjectDictionary<{Vehicle_Index}integer, TDBAcousticDecoyOnBoardList>;
  TDBAirBubbleOnBoardDict         = TObjectDictionary<{Vehicle_Index}integer, TDBAirBubbleOnBoardList>;
  TDBChaffOnBoardDict             = TObjectDictionary<{Vehicle_Index}integer, TDBChaffOnBoardList>;
  TDBChaffLauncherOnBoardDict     = TObjectDictionary<{Vehicle_Index}integer, TDBChaffLauncherOnBoardList>;
  TDBDefensiveJammerOnBoardDict   = TObjectDictionary<{Vehicle_Index}integer, TDBDefensiveJammerOnBoardList>;
  TDBFloatingDecoyOnBoardDict     = TObjectDictionary<{Vehicle_Index}integer, TDBFloatingDecoyOnBoardList>;
  TDBInfraredDecoyOnBoardDict     = TObjectDictionary<{Vehicle_Index}integer, TDBInfraredDecoyOnBoardList>;
  TDBJammerOnBoardDict            = TObjectDictionary<{Vehicle_Index}integer, TDBJammerOnBoardList>;
  TDBTowedJammerOnBoardDict       = TObjectDictionary<{Vehicle_Index}integer, TDBTowedJammerOnBoardList>;
  TDBSonobuoyOnBoardDict          = TObjectDictionary<{Vehicle_Index}integer, TDBSonobuoyOnBoardList>;
  TDBPlatformInstanceDict         = TObjectDictionary<{Platform_Instance_Index}integer, TDBPlatformInstance>;
  TDBRuntimePlatformLibraryDict   = TObjectDictionary<{TDBRuntime_Platform_Library} TDBRuntime_Platform_Library,
                                                      {list of TDBPlatform_Library_Entry} TDBPlatformLibraryEntryList>;

  {*------------------------------------------------------------------------------
    Definisi generic dict untuk blindzone device
  -------------------------------------------------------------------------------}
  TDBBlindZoneDict                = TObjectDictionary<{Asset_Index}integer, TDBBlindZoneDefinitionList>;

  {*------------------------------------------------------------------------------
    Pair of element on dictionary
  -------------------------------------------------------------------------------}
  TDBPairEOOnBoardDict                = TPair<{Vehicle_Index}integer, TDBEOOnBoardList>;
  TDBPairESMOnBoardDict               = TPair<{Vehicle_Index}integer, TDBESMOnBoardList>;
  TDBPairIFFSensorOnBoardDict         = TPair<{Vehicle_Index}integer, TDBIFFOnBoardList>;
  TDBPairMADSensorOnBoardDict         = TPair<{Vehicle_Index}integer, TDBMADOnBoardList>;
  TDBPairFCROnBoardDict               = TPair<{Vehicle_Index}integer, TDBFCROnBoardList>;
  TDBPairVisualOnBoardDict            = TPair<{Vehicle_Index}integer, TDBVisualOnBoardList>;
  TDBPairRadarOnBoardDict             = TPair<{Vehicle_Index}integer, TDBRadarOnBoardList>;
  TDBPairSonarOnBoardDict             = TPair<{Vehicle_Index}integer, TDBSonarOnBoardList>;
  TDBPairFitWeaponOnBoardDict         = TPair<{Vehicle_Index}integer, TDBFitWeaponOnBoardList>;
  TDBPairPointEffectOnBoardDict       = TPair<{Vehicle_Index}integer, TDBPointEffectOnBoardList>;
  TDBPairAcousticDecoyOnBoardDict     = TPair<{Vehicle_Index}integer, TDBAcousticDecoyOnBoardList>;
  TDBPairAirBubbleOnBoardDict         = TPair<{Vehicle_Index}integer, TDBAirBubbleOnBoardList>;
  TDBPairChaffOnBoardDict             = TPair<{Vehicle_Index}integer, TDBChaffOnBoardList>;
  TDBPairChaffLauncherOnBoardDict     = TPair<{Vehicle_Index}integer, TDBChaffLauncherOnBoardList>;
  TDBPairDefensiveJammerOnBoardDict   = TPair<{Vehicle_Index}integer, TDBDefensiveJammerOnBoardList>;
  TDBPairFloatingDecoyOnBoardDict     = TPair<{Vehicle_Index}integer, TDBFloatingDecoyOnBoardList>;
  TDBPairInfraredDecoyOnBoardDict     = TPair<{Vehicle_Index}integer, TDBInfraredDecoyOnBoardList>;
  TDBPairJammerOnBoardDict            = TPair<{Vehicle_Index}integer, TDBJammerOnBoardList>;
  TDBPairTowedJammerOnBoardDict       = TPair<{Vehicle_Index}integer, TDBTowedJammerOnBoardList>;
  TDBPairSonobuoyOnBoardDict          = TPair<{Vehicle_Index}integer, TDBSonobuoyOnBoardList>;
  TDBPairPlatformInstanceDict         = TPair<{Platform_Instance_Index}integer, TDBPlatformInstance>;
  TDBPairCubicleAssignmentDict        = TPair<{group index}integer, TDBCubicleGroupMemberList>;
  TDBPairRuntimePlatformLibraryDict   = TPair<{TDBRuntime_Platform_Library} TDBRuntime_Platform_Library,
                                                      {list of TDBPlatform_Library_Entry} TDBPlatformLibraryEntryList>;

implementation

end.
