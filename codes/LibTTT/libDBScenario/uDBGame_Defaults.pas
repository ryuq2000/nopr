unit uDBGame_Defaults;

interface

uses
  Classes, tttData;

type

  TGame_Defaults = class
  public
    FData                  : TRecGame_Defaults;
    FGameCloud_ESM         : array of TRecGame_Cloud_On_ESM;
    FGameCloud_Radar       : array of TRecGame_Cloud_On_Radar;
    FGameDefault_IFF       : array of TRecGame_Default_IFF_Mode_Code;
    FGameRainfall_ESM      : array of TRecGame_Rainfall_On_ESM;
    FGameRainfall_Missile  : array of TRecGame_Rainfall_On_Missile_Seeker;
    FGameRainfall_Radar    : array of TRecGame_Rainfall_On_Radar;
    FGameRainfall_Sonar    : array of TRecGame_Rainfall_On_Sonar;
    FGameSea_Missile       : TRecGame_Sea_On_Missile_Safe_Altitude;
    FGameSea_Radar         : TRecGame_Sea_On_Radar;
    FGameSea_Sonar         : array of TRecGame_Sea_On_Sonar;
    FGameShip_Sonar        : array of TRecGame_Ship_On_Sonar;

  public
    constructor Create;
    destructor Destroy; override;
  end;

implementation

constructor TGame_Defaults.Create;
begin
end;

destructor TGame_Defaults.Destroy;
begin

  inherited;
end;

end.
