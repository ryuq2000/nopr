unit uWaypoint;

interface

uses Windows,uDBAsset_Scripted, Classes, tttData, Graphics,uCoordConvertor,
     uSimObjects, Dialogs, uT3PlatformInstance, uGameData_TTT, ExtCtrls;

type
  TOnWaypointReached = procedure (behavoiur, event : TObject) of object;

  TOnWaypoint_OnRadar = procedure (aShipID, aRadarIndex,
    aEventMode : Integer) of object;
  TOnWaypoint_OnSonar = procedure (aShipID, aSonarIndex,
    aEventMode : Integer) of object;
  TOnWaypoint_OnIFF = procedure (aShipID, aIffIndex, aIffType,
    aEventMode : Integer) of object;
  TOnWaypoint_DeployContinousEvent = procedure (aShipID, aObjIndex : Integer;
    aObjType, aSonoMode : Byte; aObjDepth, aObjSpacing : Double;
    aObjQty : Integer; aDeployUntilNextWaypoint : Boolean) of object;
  TOnWaypoint_DeploySonobuoy = procedure (aShipID, aSonoIndex : Integer;
    aSonoMode : Byte; aDepth : Double) of object;
  TOnWaypoint_LaunchMissile = procedure (aParentID, aTargetID, aMissileID,
    aSalvo : integer) of object;
  TOnWaypoint_LaunchTorpedo = procedure (aParentID, aTargetID, aMissileID,
    aSalvo : integer) of object;
  TOnWayPoint_LaunchBomb = procedure (aParentID, aTargetID, aMissileID,
    aSalvo : integer) of object;
  TOnWaypoint_DeployMine = procedure (aShipID, aMineIndex : Integer; aDepth
    : Double) of object;
  TOnWaypoint_OnCountermeasure = procedure (aShipID, aCMIndex,
    aEventMode : Integer) of object;
  TOnWaypoint_OnDatalink = procedure (aShipID, aDatalinkIndex, aDatalinkType,
    aEventMode : Integer) of object;
  TOnWaypointEnd = procedure (aShipID : Integer; aSpeed,
    aHeading : Double) of object;
  TOnWaypointDestinantion_Change = procedure (aShipID : Integer; Long,
    Lat : Double) of object;
  TOnWaypointRemoveOwner = procedure (aShipID : Integer) of object;

  TWaypointEventType  = (wetRadar, wetSonar, wetIFFTransponder, wetIFFInterogator, wetSonobuoy,
                         wetWeapon1, wetWeapon2, wetWeapon3, wetWeapon4, wetWeapon5, wetMine,
                         wetCounterMeasure, wetComHF, wetComUHF, wetNone);

  TWaypointEventClass = class
    FType   : TWaypointEventType;
    FEnable : boolean;
    FData   : TObject;  // object of scripted event
    FObjectInstanceIndex :  integer;
  end;

  TWaypoint = class
  private
    FWPList : TList;
    FWPListCache : TList;
    FPixelTrails : array of TPoint;
    //FEvents: TList;
    FTermination : TWaypointTermination;
    FIndexPoint : Integer;
    FOwner : TSimObject;

    FOwnerPositionX : Integer;
    FOwnerPositionY : Integer;
    FOnPointReached : TOnWaypointReached;
    FOffsetX, FOffsetY : Double;
    FPNextWaypoint : ^TRecWaypoint;
    FEnabled : Boolean;
    FIsOwnerHooked : Boolean;
    FIsOpenGuidanceTab : Boolean;

    //-nando
    FisRemoveOwner  : Boolean;
    FRunWaypoint    : Boolean;
    FStartWaypointX : Double;
    FStartWaypointY : Double;
    FShipOwn        : TT3PlatformInstance;
    FTimer          : TTimer;
    FIdTimer        : Integer;
    //Focused_weapon  : TSimObject;
    MisilleRemain   : Integer;
    ArrayMisLauch   : array[0..4] of TRecCmd_LaunchMissile;
    IDArray         : Integer;
    IDLaunchMismile : Integer;
    //choco
    FNextNodeID     : Integer;
    FIsChange       : Boolean;

    FLastLongCountermeasure : Double;
    FLastLatCountermeasure : Double;
    FIsContCountermeasure : Boolean;
    FCountermeasureIndex : Integer;
    FCountermeasureSpacing : Double;
    FCountermeasureQty : Integer;

    FLastLongSonobuoy : Double;
    FLastLatSonobuoy : Double;
    FIsContSonobuoy : Boolean;
    FSonobuoyIndex : Integer;
    FSonobuoyMode : Byte;
    FSonobuoyDepth : Double;
    FSonobuoySpacing : Double;
    FSonobuoyQty : Integer;

    FLastLongMine : Double;
    FLastLatMine : Double;
    FIsContMine : Boolean;
    FMineIndex : Integer;
    FMineDepth : Double;
    FMineSpacing : Double;
    FMineQty : Integer;

    FTerminationHeading : Double;
    FOldRange : Double;

    {$IFDEF SERVER}
    procedure CheckWaypointEvent(index : integer);
    {$ENDIF}

    procedure ClearWaypoint;
    procedure FTimerOnTime(Sender : TObject);
    procedure LauncMissile(Id : Integer);

    function GetEvents(index : Integer): Tlist;
    function GetBehaviour(index: Integer): TObject;
    function GetCount: Integer;

    procedure SetTermination(const Value: TWaypointTermination);
    procedure SetOwner(const Value: TSimObject);
    procedure SetOnPointReached(const Value: TOnWaypointReached);

    //---- tdk dipanggil ------//
    {function First    : TObject;
    function Last     : TObject;
    function Next     : TObject;
    function Previous : TObject;}
    //---- tdk dipanggil ------//

    procedure SetEnabled(const Value: boolean);
    procedure TerminationAction;
    function getNextWaypoint: TScripted_Behav_Definition;
    procedure CopyList(const Source, Destination: TList);
  public
    //nando
    FonLaunchMissile  : TOnWaypoint_LaunchMissile;
    FOnLaunchTorpedo  : TOnWaypoint_LaunchTorpedo;
    FOnLaunchBomb     : TOnWayPoint_LaunchBomb;
    FonRadarEvent     : TOnWaypoint_OnRadar;
    FonSonarEvent     : TOnWaypoint_OnSonar;
    FonIffEvent       : TOnWaypoint_OnIFF;
    FonDeployContinousEvent : TOnWaypoint_DeployContinousEvent;
    FonDeploySonobuoy : TOnWaypoint_DeploySonobuoy;
    FonDeployMine     : TOnWaypoint_DeployMine;
    FonCMEvent        : TOnWaypoint_OnCountermeasure;
    FonDatalinkEvent  : TOnWaypoint_OnDatalink;
    FonWaypointEnd    : TOnWaypointEnd;
    FonWaypointDestinationChange  : TOnWaypointDestinantion_Change;
    FonWaypointRemoveOwner        :  TOnWaypointRemoveOwner;
    NetBehav         : TScripted_Behav_Definition;
    //VehiclePlatform  : TT3UnitContainer;
    FOnWaypointChange: TOnWaypointChange;

    constructor Create;

    function EventExistForObject(indexbehav : integer; valObject : TObject; var varObject : TObject) : boolean;overload;
    function EventExistForObject(indexbehav : integer; valType : TWaypointEventType; var varObject : TObject) : boolean;overload;

    procedure Add(behav : TScripted_Behav_Definition; event : TWaypointEventClass);
    procedure Delete(behav : TScripted_Behav_Definition);overload;
    procedure Delete(index : integer);overload;
    procedure Clear;
    procedure ConvertCoord(cvt: TCoordConverter);
    procedure Draw(aCnv : TCanvas);
    procedure RestartWaypoint;
    procedure Apply;

    //nando
    procedure CheckWaypointReach;
    procedure SetStartWaypoint;
    procedure SetHeadingWaypoint;
    procedure RestartWaypointWithCurrentPosition;
    property ShipOwn : TT3PlatformInstance read FShipOwn write FShipOwn;
    procedure EnableSensorEvent(index, id, State, device : integer);
    procedure EnableWeaponEvent(index, id, targetIndex: integer; track : Boolean; weapontype : byte;
                  weaponIndex : integer;aSalvo : integer = 0);

    //choco - waypoint =================== BEGIN ===================
    function GetAvailableWaypointID: Integer;

    //----- Event -----
    procedure DoContinousEvent;

    procedure SensorStateChange(aIndex, aList_Index, aDeviceType,
              aState : Integer);

    procedure SonobuoyChange(aIndex, aList_Index, aSonobuoyIndex,
              aDeviceID: Integer);
    procedure SonobuoyModeChange(aIndex, aList_Index, aSonobuoyIndex,
              aSonobuoyMode : Integer);
    procedure SonobuoyDepthChange(aIndex, aList_Index, aSonobuoyIndex : Integer;
              aSonobuoyDepth : Double);
    procedure SonobuoySpacingChange(aIndex, aList_Index,
              aSonobuoyIndex : Integer; aSonobuoySpacing : Double);
    procedure SonobuoyQuantityChange(aIndex, aList_Index,
              aSonobuoyIndex, aSonobuoyQty : Integer);
    procedure SonobuoyContinueStateChange(aIndex, aList_Index,
              aSonobuoyIndex : Integer; aSonobuoyContinueState : Boolean);

    procedure WeaponChange(aIndex, aList_Index, aWeaponIndex,
              aWeaponID: Integer);
    procedure WeaponSalvoChange(aIndex, aList_Index, aWeaponIndex,
              aSalvo: Integer);
    procedure WeaponTargetChange(aIndex, aList_Index, aWeaponIndex,
              aTarget: Integer);

    procedure MineChange(aIndex, aList_Index, aMineIndex, aWeaponID : Integer);
    procedure MineDepthChange(aIndex, aList_Index, aMineIndex : Integer;
              aMineDepth : Double);
    procedure MineSpacingChange(aIndex, aList_Index,
              aMineIndex : Integer; aMineSpacing : Double);
    procedure MineQuantityChange(aIndex, aList_Index,
              aMineIndex, aMineQty : Integer);
    procedure MineContinueStateChange(aIndex, aList_Index,
              aMineIndex : Integer; aMineContinueState : Boolean);

    procedure CountermeasureStateChange(aIndex, aList_Index, aState : Integer);

    procedure DatalinkStateChange(aIndex, aList_Index, aDeviceType,
              aState : Integer);

    procedure ContinousEventChange(aObjIndex: Integer; aObjType, aObjMode: Byte;
              aObjDepth, aObjSpacing: Double; aObjQty : Integer;
              aDeployUntilNextWaypoint: Boolean);
    //----- Event -----
    //choco - waypoint ==================== END ====================

    property StartWaypointX : Double read FstartWaypointX write FstartWaypointX;
    property StartWaypointY : Double read FstartWaypointY write FstartWaypointY;

    property Events[index : integer] : TList read GetEvents;
    property Behaviour[index : integer] : TObject read GetBehaviour;
    property Count : integer read GetCount;
    property Termination : TWaypointTermination read FTermination write SetTermination;
    property Owner : TSimObject read FOwner write SetOwner;
    property OnPointReached : TOnWaypointReached read FOnPointReached write SetOnPointReached;

    property Enabled : boolean read FEnabled write SetEnabled;
    property NextWaypoint : TScripted_Behav_Definition read getNextWaypoint;

    //Nando
    property RunWaypoint : Boolean read FRunWaypoint write FRunWaypoint;
    property NextNodeID : Integer read FNextNodeID write FNextNodeID;

    property IsChange : Boolean read FIsChange write FIsChange;
    property IsOwnerHooked : Boolean read FIsOwnerHooked write FIsOwnerHooked;
    property IsOpenGuidanceTab : Boolean read FIsOpenGuidanceTab write FIsOpenGuidanceTab;
    property TerminationHeading : Double read FTerminationHeading write FTerminationHeading;
    property OldRange : Double read FOldRange write FOldRange;
  end;

implementation

uses uLibSettingTTT, uBaseCoordSystem, SysUtils, uT3Vehicle, uT3MountedRadar,
     uT3MountedSonar, uT3MountedSonobuoy;
//  uT3Missile, uT3SimManager, uT3Radar, uT3Sonar,
//  uT3OtherSensor, uT3Weapon, uT3Torpedo, uT3Gun, uT3Bomb, uT3Sonobuoy;

{ TWaypoint }

var
  rMis: TRecCmd_LaunchMissile;


procedure TWaypoint.Add(behav: TScripted_Behav_Definition; event: TWaypointEventClass);
var
  rec : ^TRecWaypoint;
  isNew : Boolean;
  i : Integer;
  List : TList;
begin
  isNew := True;
  List := FWPList;

  for i := 0 to List.Count - 1 do
  begin
    rec := List.Items[i];
    if TScripted_Behav_Definition(rec^.Behav).FData.Scripted_Event_Index =
       behav.FData.Scripted_Event_Index then
    begin
      if Assigned(event) then
        rec^.Events.Add(event);

      isNew := False;
    end;
  end;

  if isNew then
  begin
    New(rec);
    rec^.Events := TList.Create;
    rec^.Behav  := behav;
    rec^.Name   := 'Waypoint' + IntToStr(behav.FData.Scripted_Event_Index);

    if Assigned(event) then
      rec^.Events.Add(event);

    FWPList.Add(rec);
  end;
end;

procedure TWaypoint.Apply;
var
  tmp : ^TRecWaypoint;
begin
  if FWPList.Count > 0 then
  begin
    tmp := FWPList.Items[FNextNodeID];

    FOffsetX := FOwner.getPositionX - TScripted_Behav_Definition(tmp^.Behav).FData.Waypoint_Latitude;
    FOffsetY := FOwner.getPositionY - TScripted_Behav_Definition(tmp^.Behav).FData.Waypoint_Longitude;
  end;

  SetStartWaypoint;
  //setHeadingWaypoint;
end;

procedure TWaypoint.CheckWaypointReach;
var
  {$IFDEF SERVER}
  i : integer;
  {$ENDIF}
  dx, dy : double;
  range, bearing, radius : Double;
begin
  if Assigned(FOwner) then
  begin
    if Assigned(FPNextWaypoint) then
    begin
      dx := TScripted_Behav_Definition(FPNextWaypoint^.Behav).FData.Waypoint_Longitude;
      dy := TScripted_Behav_Definition(FPNextWaypoint^.Behav).FData.Waypoint_Latitude;

      range := CalcRange(FOwner.getPositionX,FOwner.getPositionY, dx,dy);
      bearing := CalcBearing(FOwner.getPositionX,FOwner.getPositionY, dx,dy);

      radius := 0;
      if TT3Vehicle(FOwner).Mover.TurnRate > 0.00001 then
        radius := TT3Vehicle(FOwner).Speed / (TT3Vehicle(FOwner).Mover.TurnRate * 60);

      if range <= radius then
      begin
        FOldRange := 9999999999;

        FIsContCountermeasure := False;
        FIsContSonobuoy := False;
        FIsContMine := False;

        if Assigned(FOnPointReached) then
          FOnPointReached(FPNextWaypoint^.Behav, FPNextWaypoint^.Events);

        //check event
        {$IFDEF SERVER}
        i := FWPList.IndexOf(FPNextWaypoint);
        CheckWaypointEvent(i);
        {$ENDIF}

        if FNextNodeID = FWPList.Count - 1 then
          TerminationAction
        else
        begin
          Inc(FNextNodeID);
          FPNextWaypoint := FWPList.Items[FNextNodeID];
        end;
      end;
    end;
  end;
end;

procedure TWaypoint.Clear;
var
  rec : ^TRecWaypoint;
  i : integer;
  List : TList;
begin
  //list := FWPList.LockList;
  List := FWPList;
  for i := 0 to List.Count - 1 do
  begin
    rec := List.Items[i];
    rec^.Behav.Free;
    rec^.Events.Clear;
    Dispose(rec);
  end;
  List.Clear;

  FPNextWaypoint := nil;
  ClearWaypoint;
  //FWPList.UnlockList;
end;

procedure TWaypoint.ClearWaypoint;
begin
  FRunWaypoint := False;
end;

procedure TWaypoint.EnableSensorEvent(index, id, State, device: integer);
var
  rec : ^TRecWaypoint;
  List , TempList : TList;

  EventList : TWaypointEventClass;
begin
  //check event Radar, sonar, Weapon
  List := FWPList;
  rec := List.Items[index];
  TempList := Rec^.Events;

  EventList := TWaypointEventClass(TempList[id]);
  EventList.FEnable := (State <> 2);

  case Device of
    1 : begin
          //Radar
          TScripted_Radar_Event(EventList.FData).FData.Radar_Control := State;
        end;
    2 : begin
          //Sensor
          TScripted_Sonar_Event(EventList.FData).FData.Sonar_Control := State;
        end;
    3 : begin
          //IFF Transponder
          TScripted_IFF_Event(EventList.FData).FData.IFF_Transponder_Control := State;
        end;
    4 : begin
          //IFF Interogator
          TScripted_IFF_Event(EventList.FData).FData.IFF_Interrogator_Control := State;
        end;
  end;
end;

procedure TWaypoint.EnableWeaponEvent(index, id, targetIndex: integer; track: Boolean;
              weapontype : byte;weaponIndex : integer;aSalvo : integer = 0);
var
  rec : ^TRecWaypoint;
  List , TempList : TList;
  i : integer;
  EventList : TWaypointEventClass;
begin
  EventList := nil;
  List := FWPList;
  rec := List.Items[index];
  TempList := Rec^.Events;

  for i := 0 to TempList.Count - 1 do
  begin
    EventList := TWaypointEventClass(TempList[i]);
    if EventList.FType = TWaypointEventType(id) then
      Break;
  end;

  if Assigned(EventList) then
  begin
    EventList.FObjectInstanceIndex := weaponIndex;

    if track then
    begin
      TScripted_Weapon_Event(EventList.FData).FData.Target_Index := targetIndex;
    end
    else
    begin
      if aSalvo > 0 then
        TScripted_Weapon_Event(EventList.FData).FData.Salvo_Size := aSalvo
      else
        TScripted_Weapon_Event(EventList.FData).FData.Weapon_Type := weapontype;
    end;
  end;
end;

{$IFDEF SERVER}
procedure TWaypoint.CheckWaypointEvent(index : integer);
var
  rec : ^TRecWaypoint;
  List , TempList : TList;
  EventList : TWaypointEventClass;
  j : Integer;
  weapon : TObject;
  ShipIndex : Integer;
//  target : TT3PlatformInstance;
//  IsQuantityAvailable, IsInRange, IsBlindzone : Boolean;
begin
  //check event Radar, sonar, IFF
  List := FWPList;
  rec := List.Items[index];
  TempList := Rec^.Events;

  IDArray := 0;
  IDLaunchMismile := 0;
  ShipIndex := TT3PlatformInstance(FOwner).InstanceIndex;

  for j := 0 to TempList.Count - 1 do
  begin
    EventList := TWaypointEventClass(TempList[j]);

    //------------Radar
    if (EventList.FType = wetRadar) and (EventList.FEnable = True) then
    begin
      if Assigned(FonRadarEvent) then
      begin
        FonRadarEvent(ShipIndex, EventList.FObjectInstanceIndex,
            TScripted_Radar_Event(EventList.FData).FData.Radar_Control);
      end;
    end
    else
    //------------Sonar
    if (EventList.FType = wetSonar) and (EventList.FEnable = True) then
    begin
      if Assigned(FonSonarEvent) then
      begin
        FonSonarEvent(ShipIndex, EventList.FObjectInstanceIndex,
            TScripted_Sonar_Event(EventList.FData).FData.Sonar_Control);
      end
    end
    else
    //---------IFFTransponder
    if (EventList.FType = wetIFFTransponder) and (EventList.FEnable = True) then
    begin
      if Assigned(FonIffEvent) then
      begin
        FonIffEvent(ShipIndex, EventList.FObjectInstanceIndex, 1,
            TScripted_IFF_Event(EventList.FData).FData.IFF_Transponder_Control);
      end
    end
    else
    //---------IFFInterogrator
    if (EventList.FType = wetIFFInterogator) and (EventList.FEnable = true) then
    begin
      if Assigned(FonIffEvent) then
      begin
        FonIffEvent(ShipIndex, EventList.FObjectInstanceIndex, 2,
            TScripted_IFF_Event(EventList.FData).FData.IFF_Interrogator_Control);
      end
    end
    else
    //Sonobuoy
    if (EventList.FType = wetSonobuoy) and (EventList.FEnable = true) then
    begin
      if EventList.FObjectInstanceIndex <> 0 then
      begin
        with TScripted_Sonobuoy_Event(EventList.FData).FData do
        begin
          if DeploySonoUntilNextWp then
          begin
            if Assigned(FonDeployContinousEvent) then
              FonDeployContinousEvent(ShipIndex, EventList.FObjectInstanceIndex,
                Byte(EventList.FType), SonobuoyMode, SonobuoyDepth,
                SonobuoySpacing, SonobuoyQuantity, DeploySonoUntilNextWp);
          end
          else
          begin
            if Assigned(FonDeploySonobuoy) then
              FonDeploySonobuoy(ShipIndex, EventList.FObjectInstanceIndex,
                SonobuoyMode, SonobuoyDepth);
          end;
        end;
      end;
    end
    else
    //-- weapon
    if ((EventList.FType = wetWeapon1) or (EventList.FType = wetWeapon2)
        or (EventList.FType = wetWeapon3) or (EventList.FType = wetWeapon4)
        or (EventList.FType = wetWeapon5)) and (EventList.FEnable = true) then
    begin
      if EventList.FObjectInstanceIndex <> 0 then
      begin
        with TT3Vehicle(FOwner) do
        begin
          weapon := getWeapon(EventList.FObjectInstanceIndex);

          if weapon is TT3MissilesOnVehicle then
          begin
            if not (TT3MissilesOnVehicle(weapon).Quantity > 0) then
              Exit;

            if Assigned(FonLaunchMissile) then
              FonLaunchMissile(InstanceIndex, EventList.FObjectInstanceIndex,
                TScripted_Weapon_Event(EventList.FData).FData.Target_Index,
                TScripted_Weapon_Event(EventList.FData).FData.Salvo_Size);
          end
          else
          if weapon is TT3TorpedoesOnVehicle then
          begin
            if not (TT3TorpedoesOnVehicle(weapon).Quantity > 0) then
              Exit;

            if Assigned(FOnLaunchTorpedo) then
              FOnLaunchTorpedo(InstanceIndex, EventList.FObjectInstanceIndex,
                TScripted_Weapon_Event(EventList.FData).FData.Target_Index,
                TScripted_Weapon_Event(EventList.FData).FData.Salvo_Size);
          end
          else if weapon is TT3BombONVehicle then
          begin
            if not (TT3BombONVehicle(weapon).Quantity > 0) then
              Exit;

            if Assigned(FOnLaunchBomb) then
              FOnLaunchBomb(InstanceIndex, EventList.FObjectInstanceIndex,
                TScripted_Weapon_Event(EventList.FData).FData.Target_Index,
                TScripted_Weapon_Event(EventList.FData).FData.Salvo_Size);
          end;
        end;
      end;
    end
    else
    //Mine
    if (EventList.FType = wetMine) and (EventList.FEnable = true) then
    begin
      if EventList.FObjectInstanceIndex <> 0 then
      begin
        with TScripted_Mine_Event(EventList.FData).FData do
        begin
          if DeployMineUntilNextWp then
          begin
            if Assigned(FonDeployContinousEvent) then
              FonDeployContinousEvent(ShipIndex, EventList.FObjectInstanceIndex,
                Byte(EventList.FType), 0, MineDepth, MineSpacing, MineQuantity,
                DeployMineUntilNextWp);
          end
          else
          begin
            if Assigned(FonDeployMine) then
              FonDeployMine(ShipIndex, EventList.FObjectInstanceIndex, MineDepth);
          end;
        end;
      end;
    end
    else
    //Countermeasure
    if (EventList.FType = wetCounterMeasure) and (EventList.FEnable = True) then
    begin
      if Assigned(FonCMEvent) then
      begin
        FonCMEvent(ShipIndex, EventList.FObjectInstanceIndex,
            TScripted_Chaff_Event(EventList.FData).FData.Countermeasure_Control);
      end;
    end
    else
    //Datalink HF
    if (EventList.FType = wetComHF) and (FEnabled = True) then
    begin
      if Assigned(FonDatalinkEvent) then
        FonDatalinkEvent(ShipIndex, EventList.FObjectInstanceIndex, 1,
          TScripted_Datalink_Event(EventList.FData).FData.Datalink_Control);
    end
    else
    //Datalink UHF/VHF
    if (EventList.FType = wetComUHF) and (FEnabled = True) then
    begin
      if Assigned(FonDatalinkEvent) then
        FonDatalinkEvent(ShipIndex, EventList.FObjectInstanceIndex, 2,
          TScripted_Datalink_Event(EventList.FData).FData.Datalink_Control);
    end;
  end;
end;
{$ENDIF}

procedure TWaypoint.LauncMissile(Id: integer);
begin
  if Id < IDArray then
  begin
    FIdTimer := 0;
    FTimer.Enabled := True;
  end;
end;

procedure TWaypoint.MineChange(aIndex, aList_Index, aMineIndex, aWeaponID : Integer);
var
  List, TempList : TList;
  Rec : ^TRecWaypoint;
  EventList : TWaypointEventClass;
begin
  List := FWPList;
  rec := List.Items[aIndex];
  TempList := Rec^.Events;

  EventList := TWaypointEventClass(TempList[aList_Index]);
  EventList.FEnable := (aMineIndex <> 0);

  TScripted_Mine_Event(EventList.FData).FData.MineType := aMineIndex;
  EventList.FObjectInstanceIndex := aWeaponID;
end;

procedure TWaypoint.MineContinueStateChange(aIndex, aList_Index,
  aMineIndex: Integer; aMineContinueState: Boolean);
var
  List, TempList : TList;
  Rec : ^TRecWaypoint;
  EventList : TWaypointEventClass;
begin
  List := FWPList;
  rec := List.Items[aIndex];
  TempList := Rec^.Events;

  EventList := TWaypointEventClass(TempList[aList_Index]);

  TScripted_Mine_Event(EventList.FData).FData.DeployMineUntilNextWp := aMineContinueState;
end;

procedure TWaypoint.MineDepthChange(aIndex, aList_Index, aMineIndex: Integer;
  aMineDepth: Double);
var
  List, TempList : TList;
  Rec : ^TRecWaypoint;
  EventList : TWaypointEventClass;
begin
  List := FWPList;
  rec := List.Items[aIndex];
  TempList := Rec^.Events;

  EventList := TWaypointEventClass(TempList[aList_Index]);

  TScripted_Mine_Event(EventList.FData).FData.MineDepth := aMineDepth;
end;

procedure TWaypoint.MineQuantityChange(aIndex, aList_Index, aMineIndex,
  aMineQty: Integer);
var
  List, TempList : TList;
  Rec : ^TRecWaypoint;
  EventList : TWaypointEventClass;
begin
  List := FWPList;
  rec := List.Items[aIndex];
  TempList := Rec^.Events;

  EventList := TWaypointEventClass(TempList[aList_Index]);

  TScripted_Mine_Event(EventList.FData).FData.MineQuantity := aMineQty;
end;

procedure TWaypoint.MineSpacingChange(aIndex, aList_Index, aMineIndex: Integer;
  aMineSpacing: Double);
var
  List, TempList : TList;
  Rec : ^TRecWaypoint;
  EventList : TWaypointEventClass;
begin
  List := FWPList;
  rec := List.Items[aIndex];
  TempList := Rec^.Events;

  EventList := TWaypointEventClass(TempList[aList_Index]);

  TScripted_Mine_Event(EventList.FData).FData.MineSpacing := aMineSpacing;
end;

procedure TWaypoint.FTimerOnTime(Sender: TObject);
begin
  FIdTimer := FIdTimer + integer(FTimer.Interval);

  //Timer On For 5 Second
  if FIdTimer = 5000 then
  begin
    rMis.ParentPlatformID := ArrayMisLauch[IDLaunchMismile].ParentPlatformID;
    rMis.TargetPlatformID := ArrayMisLauch[IDLaunchMismile].TargetPlatformID;
    rMis.MissileID        := ArrayMisLauch[IDLaunchMismile].MissileID;

    //send Misil after 5 detik
    if MisilleRemain > 0 then
    begin
      //simMgrClient.netSend_CmdLaunch_Missile(rMis);}
      if Assigned(FonLaunchMissile) then
      begin
        FonLaunchMissile(rMis.ParentPlatformID, rMis.MissileID, rMis.TargetPlatformID, 2);
      end;
    end;

    FTimer.Enabled := False;

    IDLaunchMismile := IDLaunchMismile + 1;
    LauncMissile(IDLaunchMismile);
  end;
end;

procedure TWaypoint.ContinousEventChange(aObjIndex: Integer; aObjType,
  aObjMode: Byte; aObjDepth, aObjSpacing: Double; aObjQty: Integer;
  aDeployUntilNextWaypoint: Boolean);
begin
  case TWaypointEventType(aObjType) of
    wetSonobuoy :
    begin
      FLastLongSonobuoy := TT3Vehicle(Owner).PosX;
      FLastLatSonobuoy := TT3Vehicle(Owner).PosY;
      FSonobuoyIndex := aObjIndex;
      FSonobuoyMode := aObjMode;
      FSonobuoyDepth := aObjDepth;
      FSonobuoySpacing := aObjSpacing;
      FSonobuoyQty := aObjQty;
      FIsContSonobuoy := aDeployUntilNextWaypoint;
    end;
    wetMine :
    begin
      FLastLongMine := TT3Vehicle(Owner).PosX;
      FLastLatMine := TT3Vehicle(Owner).PosY;
      FMineIndex := aObjIndex;
      FMineDepth := aObjDepth;
      FMineSpacing := aObjSpacing;
      FMineQty := aObjQty;
      FIsContMine := aDeployUntilNextWaypoint;
    end;
    wetCounterMeasure :
    begin
      FLastLongCountermeasure := TT3Vehicle(Owner).PosX;
      FLastLatCountermeasure := TT3Vehicle(Owner).PosY;
      FCountermeasureIndex := aObjIndex;
      FCountermeasureSpacing := aObjSpacing;
      FCountermeasureQty := aObjQty;
      FIsContCountermeasure := aDeployUntilNextWaypoint;
    end;
  end;
end;

procedure TWaypoint.ConvertCoord(cvt: TCoordConverter);
var
  i : Integer;
  dx, dy : Double;
  rec : ^TRecWaypoint;
begin
  cvt.ConvertToScreen(Owner.PosX,Owner.PosY,FOwnerPositionX,FOwnerPositionY);

  SetLength(FPixelTrails, FWPList.Count);
  for i := 0 to FWPList.Count - 1 do
  begin
    rec := FWPList.Items[i];
    dx := TScripted_Behav_Definition(rec^.Behav).FData.Waypoint_Longitude;
    dy := TScripted_Behav_Definition(rec^.Behav).FData.Waypoint_Latitude;
    cvt.ConvertToScreen(dx, dy, FPixelTrails[i].X, FPixelTrails[i].Y);
  end;
end;

procedure TWaypoint.CopyList(const Source, Destination: TList);
var
  i : Integer;
begin
  Destination.Clear;

  for i := 0 to Source.Count - 1 do
    Destination.Add(Source.Items[i]);

  Source.Clear;
end;

procedure TWaypoint.CountermeasureStateChange(aIndex, aList_Index,
  aState: Integer);
var
  rec : ^TRecWaypoint;
  List , TempList : TList;

  EventList : TWaypointEventClass;
begin
  List := FWPList;
  rec := List.Items[aIndex];
  TempList := Rec^.Events;

  EventList := TWaypointEventClass(TempList[aList_Index]);
  EventList.FEnable := (aState <> 2);

  TScripted_Chaff_Event(EventList.FData).FData.Countermeasure_Control := aState;
end;

constructor TWaypoint.Create;
begin
  //FWPList := TThreadList.Create;
  FWPList := TList.Create;
  FWPListCache := TList.Create;
  FTermination := wtLH;
  FIndexPoint  := -1;
  FPNextWaypoint  := nil;
  FEnabled := False;
  FIsChange := False;

  //nando
  FRunWaypoint := False;
  FTimer := TTimer.Create(nil);
  FTimer.Enabled := False;
  FTimer.Interval := 1000;
  FTimer.OnTimer := FTimerOnTime;
  FIdTimer := 0;
  FNextNodeID := 0;

  FisRemoveOwner := false;

  FIsContCountermeasure := False;
  FIsContSonobuoy := False;
  FIsContMine := False;

  //choco - For Drawing Purpose
  FIsOwnerHooked := False;
  FIsOpenGuidanceTab := False;
end;

procedure TWaypoint.DatalinkStateChange(aIndex, aList_Index, aDeviceType,
  aState: Integer);
var
  rec : ^TRecWaypoint;
  List, TempList : TList;
  EventList : TWaypointEventClass;
begin
  List := FWPList;
  rec := List.Items[aIndex];
  TempList := rec^.Events;

  EventList := TWaypointEventClass(TempList[aList_Index]);
  EventList.FEnable := (aState <> 2);

  TScripted_Datalink_Event(EventList.FData).FData.Datalink_Control := aState;
end;

procedure TWaypoint.Delete(index: integer);
var rec : ^TRecWaypoint;
  list : TList;
begin
  //list := FWPList.LockList;
  list := FWPList;
  if (index < 0) and (index >= List.Count) then exit;

  rec := List.Items[index];
  rec^.Behav.Free;
  rec^.Events.Clear;
  rec^.Events.Free;
  Dispose(rec);

  list.Delete(index);
  //FWPList.UnlockList;
end;

procedure TWaypoint.DoContinousEvent;
{$IFDEF SERVER}
var
  range : Double;
{$ENDIF}
begin
  if not (FIsContCountermeasure or FIsContSonobuoy or FIsContMine) then
    Exit;

  if FIsContCountermeasure then
  begin
//    if Assigned(TT3Vehicle(Owner).OnLogEventStr) then
//    begin
//      TT3Vehicle(Owner).OnLogEventStr('Run Continous Countermeasure');
//    end;
  end;

  if FIsContSonobuoy then
  begin
    {$IFDEF SERVER}
    range := CalcRange(FLastLongSonobuoy, FLastLatSonobuoy,
             TT3Vehicle(Owner).PosX, TT3Vehicle(Owner).PosY);

    if range >= FSonobuoySpacing then
    begin
      FLastLongSonobuoy := TT3Vehicle(Owner).PosX;
      FLastLatSonobuoy := TT3Vehicle(Owner).PosY;

      if Assigned(FonDeploySonobuoy) then
        FonDeploySonobuoy(TT3Vehicle(Owner).InstanceIndex, FSonobuoyIndex,
          FSonobuoyMode, FSonobuoyDepth);
    end;
    {$ENDIF}
  end;

  if FIsContMine then
  begin
    {$IFDEF SERVER}
    range := CalcRange(FLastLongMine, FLastLatMine,
             TT3Vehicle(Owner).PosX, TT3Vehicle(Owner).PosY);

    if range >= FMineSpacing then
    begin
      FLastLongMine := TT3Vehicle(Owner).PosX;
      FLastLatMine := TT3Vehicle(Owner).PosY;

      if Assigned(FonDeployMine) then
        FonDeployMine(TT3Vehicle(Owner).InstanceIndex, FMineIndex, FMineDepth);
    end;
    {$ENDIF}
  end;
end;

procedure TWaypoint.Draw(aCnv: TCanvas);
var
  i, j, x, y : Integer;
  bmp : TBitmap;
begin
//  with aCnv do
//  begin
//    if (FIsOwnerHooked and FIsOpenGuidanceTab) or
//      (TT3Vehicle(FOwner).isInstructor) then
//    begin
//      for i := 0 to Length(FPixelTrails) - 1 do
//      begin
//        if i <> Length(FPixelTrails) - 1 then
//        begin
//          Pen.Style := psSolid;
//          Pen.Color := clWhite;
//          MoveTo(FPixelTrails[i].X, FPixelTrails[i].Y);
//          LineTo(FPixelTrails[i + 1].X, FPixelTrails[i + 1].Y);
//        end;
//
//        bmp := TBitmap.Create;
//        bmp.Transparent := true;
//        bmp.LoadFromFile(vSymbolSetting.ImgPath + pctWayPoint + '.bmp');
//
//        x := FPixelTrails[i].X - (bmp.Width div 2);
//        y := FPixelTrails[i].Y - (bmp.Height div 2);
//
//        Pen.Color := clBlack;
//        Pen.Style := psSolid;
//        Draw(x, y, bmp);
//        bmp.Free;
//      end;
//    end;
//  end;
end;

function TWaypoint.EventExistForObject(indexbehav: integer;
         valType: TWaypointEventType; var varObject: TObject): boolean;
var
    rec : ^TRecWaypoint;
    event : TWaypointEventClass;
    i : integer;
    list : TList;
    exist : boolean;
begin
  result := False;

  if indexbehav > Count - 1 then
    exit;

  if indexbehav < 0 then
    exit;

  //list := FWPList.LockList;
  list := FWPList;
  exist := false;
  varObject := nil;

  rec := List.Items[indexbehav];

  for i:=0 to rec^.Events.Count - 1 do
  begin
    event := rec^.Events[i];
    if event.FType = valType then
    begin
      varObject := event;
      exist := true;
      break;
    end;
  end;

  result := exist;
end;

//function TWaypoint.First: TObject;
//begin
//  result := nil;
//
//  if FWPList.Count = 0 then exit;
//
//  FIndexPoint := 0;
//  result := Behaviour[0];
//end;

function TWaypoint.EventExistForObject(indexbehav: integer;
         valObject: TObject; var varObject : TObject): boolean;
var
    rec : ^TRecWaypoint;
    event : TWaypointEventClass;
    i : integer;
    list : TList;
    exist : boolean;
begin
  result := False;

  if indexbehav > Count - 1 then
    exit;

  if indexbehav < 0 then
    exit;

  //list := FWPList.LockList;
  list := FWPList;
  exist := false;
  varObject := nil;

  rec := List.Items[indexbehav];
  for i:=0 to rec^.Events.Count - 1 do
  begin
    event := rec^.Events[i];

    if (valObject is TT3MountedRadar) and
       (event.FData.ClassName = 'TScripted_Radar_Event') and
       (TT3MountedRadar(valObject).InstanceIndex = TScripted_Radar_Event(event.FData).FData.Radar_Index )
    then
      begin
        exist     := true;
        varObject := event;

        break;
      end
    else if (valObject is TT3MountedSonar) and
       (event.FData.ClassName = 'TScripted_Sonar_Event') and
       (TT3MountedSonar(valObject).InstanceIndex = TScripted_Sonar_Event(event.FData).FData.Sonar_Index)
    then
      begin
        exist     := true;
        varObject := event;

        break;
      end
      { TODO -oRyu : IFF Sensor commented first }
//    else if (valObject is TT3IFFSensor) and
//       (event.FData.ClassName = 'TScripted_IFF_Event') and
//       (TT3IFFSensor(valObject).InstanceIndex = TScripted_IFF_Event(event.FData).FData.IFF_Instance_Index)
//    then
//    begin
//      exist     := true;
//      varObject := event;
//
//      break;
//    end
    else if (valObject is TT3MountedSonobuoy) and
       (event.FData.ClassName = 'TScripted_Sonobuoy_Event') and
       (TT3MountedSonobuoy(valObject).InstanceIndex = TScripted_Sonobuoy_Event(event.FData).FData.Scripted_Event_Index)//TScripted_Sonobuoy_Event(event.FData).FData.Scripted_Event_Index)
    then
    begin
      exist     := true;
      varObject := event;

      break;
    end;
  end;

  result := exist;
  //FWPList.UnlockList;
end;

procedure TWaypoint.Delete(behav : TScripted_Behav_Definition);
var rec : ^TRecWaypoint;
    i : integer;
    list : Tlist;
begin
  //list := FWPList.LockList;
  list := FWPList;
  for i := 0 to List.Count - 1 do begin
    rec := List.Items[i];
    if TScripted_Behav_Definition(rec^.Behav).FData.Scripted_Event_Index =
       behav.FData.Scripted_Event_Index then begin
          List.Delete(i);
          break;
       end;
  end;
  //FWPList.UnlockList;
end;

function TWaypoint.GetAvailableWaypointID: Integer;
var
  i, avaID, countNotFound : Integer;
  isFoundAvaId : Boolean;
  rec : ^TRecWaypoint;
begin
  avaID := 1;
  isFoundAvaId := False;

  while not isFoundAvaId do
  begin
    countNotFound := 0;

    for i := 0 to FWPListCache.Count - 1 do
    begin
      rec := FWPListCache.Items[i];

      if TScripted_Behav_Definition(rec^.Behav).FData.Scripted_Event_Index =
         avaID then
      begin
        Inc(avaID);
        Break;
      end;

      Inc(countNotFound);
    end;

    for i := 0 to FWPList.Count - 1 do
    begin
      rec := FWPList.Items[i];

      if TScripted_Behav_Definition(rec^.Behav).FData.Scripted_Event_Index =
         avaID then
      begin
        Inc(avaID);
        Break;
      end;

      Inc(countNotFound);
    end;

    if countNotFound = FWPListCache.Count + FWPList.Count then
      isFoundAvaId := True;
  end;

  Result := avaID;
end;

function TWaypoint.GetBehaviour(index: integer): TObject;
var rec : ^TRecWaypoint;
  list : TList;
begin
  //list := FWPList.LockList;
  result := nil;
  list := FWPList;

  if List.Count > 0 then
  begin
    if (index > -1) and (index <= List.count-1) then
    begin
      rec := List.Items[index];
      result := rec^.Behav;
    end
    else
      result := nil;
  end;

  //FWPList.UnlockList;
end;

function TWaypoint.GetCount: integer;
var
  list : TList;
begin
  //list := FWPList.LockList;
  list := FWPList;
  result := List.Count;
  //FWPList.UnlockList;
end;

function TWaypoint.GetEvents(index : integer): Tlist;
var rec : ^TRecWaypoint;
  list : TList;
begin
  //list := FWPList.LockList;
  result := nil;
  list := FWPList;

  if List.Count > 0 then
  begin
    if (index > -1) and (index <= List.count-1) then
    begin
      rec := List.Items[index];
      result := rec^.Events;
    end
    else
      result := nil;
  end;

  //FWPList.UnlockList;
end;

function TWaypoint.getNextWaypoint: TScripted_Behav_Definition;
begin
  if Assigned(FPNextWaypoint) then
    result := TScripted_Behav_Definition(FPNextWaypoint^.Behav)
  else
    result := nil;
end;

//function TWaypoint.Last: TObject;
//begin
//  result := nil;
//
//  if FWPList.Count = 0 then exit;
//
//  FIndexPoint := FWPList.Count - 1;
//  result := Behaviour[FWPList.Count - 1];
//end;

//function TWaypoint.Next: TObject;
//begin
//  result := nil;
//
//  if FWPList.Count = 0 then exit;
//
//  FIndexPoint := FIndexPoint + 1;
//  if FIndexPoint < FWPList.Count - 1 then
//    result := Behaviour[FIndexPoint]
//  else begin
//    FIndexPoint := FWPList.Count;
//    result := nil;
//  end;
//end;

//function TWaypoint.Previous: TObject;
//begin
//  result := nil;
//
//  if FWPList.Count = 0 then exit;
//
//  FIndexPoint := FIndexPoint - 1;
//  if FIndexPoint >= 0 then
//    result := Behaviour[FIndexPoint]
//  else begin
//    FIndexPoint := -1;
//    result := nil;
//  end;
//end;

//procedure TWaypoint.ReposWaypoint;
//var
//  rec : ^TRecWaypoint;
//  i : integer;
//begin
//  for i := 0 to FWPList.Count - 1 do begin
//    rec := FWPList.Items[i];
//
//    TScripted_Behav_Definition(rec^.Behav).FData.Waypoint_Latitude := FOwner.getPositionX - FOffsetX;
//    TScripted_Behav_Definition(rec^.Behav).FData.Waypoint_Longitude := FOwner.getPositionY - FOffsetY;
//  end;
//end;

procedure TWaypoint.RestartWaypoint;
begin
  if FWPList.Count > 0 then
  begin
    FNextNodeID := 0;
    FPNextWaypoint := FWPList.Items[0];
  end;
end;

//===Nando
//Repost Position Versi 2 ^^8
procedure TWaypoint.SetStartWaypoint;
begin
  FStartWaypointX := FOwner.getPositionX;
  FstartWaypointY := FOwner.getPositionY;
end;

procedure TWaypoint.SetHeadingWaypoint;
begin
  if not Assigned(NextWaypoint) then exit;

  {
  with NextWaypoint do
  begin
    bearing := CalcBearing(FOwner.getPositionX,FOwner.getPositionY, FData.Waypoint_Longitude,
     FData.Waypoint_Latitude);

    if Assigned(TT3PlatformInstance(FOwner).FOnOrderedControl) then
        TT3PlatformInstance(FOwner).FOnOrderedControl(TT3Vehicle(FOwner), pocHeading, bearing);
  end;
  }

end;

procedure TWaypoint.RestartWaypointWithCurrentPosition;
var
  i : Integer;
  recNew : ^TRecWaypoint;
  difLat, difLong : Double;
begin
  difLong := FOwner.getPositionX - FStartWaypointX;
  difLat := FOwner.getPositionY - FstartWaypointY;

  for i := 0 to FWPList.Count - 1 do
  begin
    recNew := FWPList.Items[i];

    //Change old Coordinat with new Coordinat (because Pattern is same)
    //Change Longitude
    TScripted_Behav_Definition(recNew^.Behav).FData.Waypoint_Longitude :=
      TScripted_Behav_Definition(recNew^.Behav).FData.Waypoint_Longitude + difLong;

    //Change Latitude
    TScripted_Behav_Definition(recNew^.Behav).FData.Waypoint_Latitude :=
      TScripted_Behav_Definition(recNew^.Behav).FData.Waypoint_Latitude + difLat;
  end;
end;

procedure TWaypoint.TerminationAction;
var
  v : TT3Vehicle;
begin
  // procedure ketika mencapai point terakhir --> tergantung termination-nya
  case FTermination of
    wtLH:
    begin
      Clear;
      Enabled := False;

      v := TT3Vehicle(FOwner);
      FonWaypointEnd(v.InstanceIndex, v.Speed, v.Heading);
    end;
    wtTH:
    begin
      Clear;
      Enabled := False;

      v := TT3Vehicle(FOwner);
      FonWaypointEnd(v.InstanceIndex, v.Speed, FTerminationHeading);
    end;
    wtRP:
    begin
      RestartWaypointWithCurrentPosition;
      RestartWaypoint;
      SetStartWaypoint;
    end;
    wtRP2:
    begin
      RestartWaypoint;
    end;
    wtRB: ;
    wtRM:
    begin
      if Assigned(FOwner) then
      begin
        v := TT3Vehicle(FOwner);

        if Assigned(FonWaypointRemoveOwner) then
        begin
          if not FisRemoveOwner then
          begin
            FisRemoveOwner := True;
            FonWaypointRemoveOwner(v.InstanceIndex);
          end;
        end;
      end;
    end;
  end;
end;

procedure TWaypoint.WeaponChange(aIndex, aList_Index, aWeaponIndex,
  aWeaponID: Integer);
var
  List, TempList : TList;
  Rec : ^TRecWaypoint;
  EventList : TWaypointEventClass;
begin
  List := FWPList;
  rec := List.Items[aIndex];
  TempList := Rec^.Events;

  EventList := TWaypointEventClass(TempList[aList_Index]);
  EventList.FEnable := (aWeaponIndex <> 0);

  TScripted_Weapon_Event(EventList.FData).FData.Weapon_Type := aWeaponIndex;
  EventList.FObjectInstanceIndex := aWeaponID;
end;

procedure TWaypoint.WeaponSalvoChange(aIndex, aList_Index, aWeaponIndex,
  aSalvo: Integer);
var
  List, TempList : TList;
  Rec : ^TRecWaypoint;
  EventList : TWaypointEventClass;
begin
  List := FWPList;
  rec := List.Items[aIndex];
  TempList := Rec^.Events;

  EventList := TWaypointEventClass(TempList[aList_Index]);

  TScripted_Weapon_Event(EventList.FData).FData.Salvo_Size := aSalvo;
end;

procedure TWaypoint.WeaponTargetChange(aIndex, aList_Index, aWeaponIndex,
  aTarget: Integer);
var
  List, TempList : TList;
  Rec : ^TRecWaypoint;
  EventList : TWaypointEventClass;
begin
  List := FWPList;
  rec := List.Items[aIndex];
  TempList := Rec^.Events;

  EventList := TWaypointEventClass(TempList[aList_Index]);

  TScripted_Weapon_Event(EventList.FData).FData.Target_Index := aTarget;
end;

procedure TWaypoint.SensorStateChange(aIndex, aList_Index, aDeviceType,
  aState: Integer);
var
  rec : ^TRecWaypoint;
  List , TempList : TList;

  EventList : TWaypointEventClass;
begin
  List := FWPList;
  rec := List.Items[aIndex];
  TempList := Rec^.Events;

  EventList := TWaypointEventClass(TempList[aList_Index]);
  EventList.FEnable := (aState <> 2);

  case aDeviceType of
    1 : TScripted_Radar_Event(EventList.FData).FData.Radar_Control := aState;
    2 : TScripted_Sonar_Event(EventList.FData).FData.Sonar_Control := aState;
    3 : TScripted_IFF_Event(EventList.FData).FData
          .IFF_Transponder_Control := aState;
    4 : TScripted_IFF_Event(EventList.FData).FData
          .IFF_Interrogator_Control := aState;
  end;
end;

procedure TWaypoint.SetEnabled(const Value: boolean);
begin
  FEnabled := Value;
end;

procedure TWaypoint.SetOnPointReached(const Value: TOnWaypointReached);
begin
  FOnPointReached := Value;
end;

procedure TWaypoint.SetOwner(const Value: TSimObject);
begin
  FOwner := Value;
  //VConvertor.ConvertToScreen(FOwner.getPositionX,FOwner.getPositionY,FOwnerPositionX,FOwnerPositionY);
end;

procedure TWaypoint.SetTermination(const Value: TWaypointTermination);
begin
  FTermination := Value;
end;

procedure TWaypoint.SonobuoyChange(aIndex, aList_Index, aSonobuoyIndex,
  aDeviceID: Integer);
var
  List, TempList : TList;
  Rec : ^TRecWaypoint;
  EventList : TWaypointEventClass;
begin
  List := FWPList;
  rec := List.Items[aIndex];
  TempList := Rec^.Events;

  EventList := TWaypointEventClass(TempList[aList_Index]);
  EventList.FEnable := (aSonobuoyIndex <> 0);

  TScripted_Sonobuoy_Event(EventList.FData).FData.SonobuoyType := aSonobuoyIndex;
  EventList.FObjectInstanceIndex := aDeviceID;
end;

procedure TWaypoint.SonobuoyContinueStateChange(aIndex, aList_Index,
  aSonobuoyIndex: Integer; aSonobuoyContinueState: Boolean);
var
  List, TempList : TList;
  Rec : ^TRecWaypoint;
  EventList : TWaypointEventClass;
begin
  List := FWPList;
  rec := List.Items[aIndex];
  TempList := Rec^.Events;

  EventList := TWaypointEventClass(TempList[aList_Index]);

  TScripted_Sonobuoy_Event(EventList.FData).FData.DeploySonoUntilNextWp := aSonobuoyContinueState;
end;

procedure TWaypoint.SonobuoyDepthChange(aIndex, aList_Index,
  aSonobuoyIndex: Integer; aSonobuoyDepth: Double);
var
  List, TempList : TList;
  Rec : ^TRecWaypoint;
  EventList : TWaypointEventClass;
begin
  List := FWPList;
  rec := List.Items[aIndex];
  TempList := Rec^.Events;

  EventList := TWaypointEventClass(TempList[aList_Index]);

  TScripted_Sonobuoy_Event(EventList.FData).FData.SonobuoyDepth := aSonobuoyDepth;
end;

procedure TWaypoint.SonobuoyModeChange(aIndex, aList_Index, aSonobuoyIndex,
  aSonobuoyMode: Integer);
var
  List, TempList : TList;
  Rec : ^TRecWaypoint;
  EventList : TWaypointEventClass;
begin
  List := FWPList;
  rec := List.Items[aIndex];
  TempList := Rec^.Events;

  EventList := TWaypointEventClass(TempList[aList_Index]);

  TScripted_Sonobuoy_Event(EventList.FData).FData.SonobuoyMode := aSonobuoyMode;
end;

procedure TWaypoint.SonobuoyQuantityChange(aIndex, aList_Index,
  aSonobuoyIndex, aSonobuoyQty: Integer);
var
  List, TempList : TList;
  Rec : ^TRecWaypoint;
  EventList : TWaypointEventClass;
begin
  List := FWPList;
  rec := List.Items[aIndex];
  TempList := Rec^.Events;

  EventList := TWaypointEventClass(TempList[aList_Index]);

  TScripted_Sonobuoy_Event(EventList.FData).FData.SonobuoyQuantity := aSonobuoyQty;
end;

procedure TWaypoint.SonobuoySpacingChange(aIndex, aList_Index,
  aSonobuoyIndex: Integer; aSonobuoySpacing: Double);
var
  List, TempList : TList;
  Rec : ^TRecWaypoint;
  EventList : TWaypointEventClass;
begin
  List := FWPList;
  rec := List.Items[aIndex];
  TempList := Rec^.Events;

  EventList := TWaypointEventClass(TempList[aList_Index]);

  TScripted_Sonobuoy_Event(EventList.FData).FData.SonobuoySpacing := aSonobuoySpacing;
end;

end.
