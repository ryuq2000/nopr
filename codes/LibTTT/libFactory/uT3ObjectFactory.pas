unit uT3ObjectFactory;

interface

uses
  uT3Unit, uT3PlatformInstance, tttData, uT3MountedDevice, uDBClassDefinition,
  uT3Track, uT3PointTrack, uT3BearingTrack;

type

  { object type enumeration }
  TEnumMountedObjectType = (

    { mounted weapon }
    otMwMissile, otMwTorpedo, otMwMine, otMwBomb, otMwGun, otMwHybrid,

    { mounted sensor }
    otMsRadar, otMsSonar, otMsVisual, otMsEO, otMsESM, otMsMAD, otMsIFF, otMsFCR, otMsSonobuoy,

    { mounted ecm }
    otMeAcoustic, otMeChaff, otMeFloating, otMeInfrared, otMeTowedJammer,
    otMeDefensiveJammer, otMeAirBubble, otMeRadarNoiseJammer, otMeSonobuoy,
    otMeAirborneChaff, otMeSurfaceChaff, otMeChaffLauncher

    );

  { class for handling TTT object creation }

  TT3ObjectFactory = class
  private
    FLastInstanceIndex: Integer;
  public
    constructor Create;

    { platform factory }
    function createPlatform(platformType: TPlatformType; platformInstance: integer): TT3PlatformInstance; overload;
    { platform factory }
    function createPlatform(platformType: TPlatformType): TT3PlatformInstance; overload;
    { mounted sensor factory }
    function createMountedSensor(msensorType: TEnumMountedObjectType; msensorOnboardDef, msensorDefinition, msensorParentIdx: integer): TT3MountedDevice;
    { mounted weapon factory }
    function createMountedWeapon(mweaponType: TEnumMountedObjectType; mweaponOnboardDef, mweaponDefinition, mweaponParentIdx: integer): TT3MountedDevice;
    { mounted ecm factory }
    function createMountedCM(mcmType: TEnumMountedObjectType; mcmOnboardDef, mcmDefinition, mcmParentIdx: integer): TT3MountedDevice;

    { detected track and pf track creator }
    function createTrack(trackType: TTrackType; platformIdx: integer): TT3Track; overload;  // real track
    function createTrack(trackType: TTrackType; pfInstance: TT3Unit): TT3Track; overload;  // real track
    function createTrack(trackType: TTrackType): TT3Track; overload; // non real track

  end;

implementation

uses
  uGLobalVar,

  {platforms}
  uT3Vehicle, uT3Mine, uT3Missile, uT3Satellite, uT3Sonobuoy, uT3Hybrid,
  uT3Torpedo,

  {mounted sensor}
  uT3MountedSensor, uT3MountedRadar, uT3MountedVisual, uT3MountedSonar,
  uT3MountedEO, uT3mountedESM, uT3MountedMAD, uT3MountedIFF,

  {mounted torpedo}
  uT3MountedTorpedo, uT3MountedStraigthTorpedo, uT3MountedActiveAcousticTorp,
  uT3MountedActivePassiveTorp, uT3MountedAirDroppedTorp, uT3MountedWireGuided,
  uT3MountedWakeHomeTorp,

  {mounted missile}
  uT3MountedMissile, uT3MountedTacticalMissiles,
  uT3MountedSurfaceToSurfaceMissile, uT3MountedSurfaceToAirMissile,

  {mounted gun}
  uT3MountedGun, uT3MountedGunCIWS, uT3MountedGunAutoManual, uT3MountedMine,
  uT3MountedBomb, uT3MountedHybrid,

  {mounted ecm}
  uT3MountedAcousticDeploy, uT3MountedDefensiveJammer,
  uT3MountedRadarNoiseJammer, uT3MountedAirborneChaff, uT3MountedSurfaceChaff,
  uT3mountedInfrared, uT3mountedAirBubble, uT3MountedTowedJammer,
  uT3mountedFloatingDecoy, uT3MountedChaffLauncher

  ;

{ TT3ObjectFactory }

 {
   fungsi create mounted weapon untuk mengcreate 6 jenis mounted weapon
 }
function TT3ObjectFactory.createMountedWeapon(mweaponType: TEnumMountedObjectType; mweaponOnboardDef, mweaponDefinition, mweaponParentIdx: integer): TT3MountedDevice;
var
  mweapon: TT3MountedDevice;
  missileDef: TDBMissile_Definition;
  torpDef: TDBTorpedo_Definition;
  vehDef: TDBVehicle_Definition;
  gundef: TDBGun_Definition;
begin
  mweapon := nil;

  case mweaponType of
    otMwMissile:
      begin
        { what kind of mounted missile }
        missileDef := simManager.Scenario.GetDBMissileDefintion(mweaponDefinition);
        if Assigned(missileDef) then
          case missileDef.Primary_Target_Domain of
            {wcMissileAirToSurfaceSubsurface, wcMissileAirToAir, wcMissileLandAttack}
            0, 3, 4:
              mweapon := TT3MountedTacticalMissiles.Create;
            {wcMissileSurfaceSubsurfaceToSurfaceSubsurface}
            1:
              mweapon := TT3MountedSurfaceToSurfaceMissile.Create;
            {wcMissileSurfaceSubsurfaceToAir}
            2:
              mweapon := TT3MountedSurfaceToAirMissile.Create;
          end;
      end;
    otMwTorpedo:
      { what kind of mounted torpedo }
      begin
        torpDef := simManager.Scenario.GetDBTorpedoDefintion(mweaponDefinition);
        if Assigned(torpDef) then
          case torpDef.Guidance_Type of
            {wcTorpedoStraigth}
            9:
              mweapon := TT3MountedStraigthTorpedo.Create;
            {wcTorpedoActiveAcoustic or wcTorpedoAirDropped}
            10:
              begin
                vehDef := simManager.Scenario.GetDBVehicleDefinition(mweaponParentIdx);
              {wcTorpedoAirDropped}
                if Assigned(vehDef) and (vehDef.Platform_Domain = 0) then
                  mweapon := TT3MountedAirDroppedTorp.Create
                else
                {wcTorpedoActiveAcoustic}
                  mweapon := TT3MountedActiveAcousticTorp.Create;
              end;
            {wcTorpedoPassiveAcoustic}
            11:
              mweapon := TT3MountedActivePassiveTorp.Create;
            {wcTorpedoActivePassive}
            18:
              mweapon := TT3MountedActivePassiveTorp.Create;
            {wcTorpedoWireGuided}
            12:
              mweapon := TT3MountedWireGuided.Create;
            {wcTorpedoWakeHoming}
            13:
              mweapon := TT3MountedWakeHomeTorp.Create;
          end;
      end;
    otMwMine:
      mweapon := TT3MountedMine.Create;
    otMwBomb:
      mweapon := TT3MountedBomb.Create;
    otMwGun:
      { what kind of mounted gun }
      begin
        gunDef := simManager.Scenario.GetDBGunDefintion(mweaponDefinition);
        if Assigned(gunDef) then
          case gunDef.Gun_Category of
          {wcGunCIWS}
            1:
              mweapon := TT3MountedGunCIWS.Create;
          {wcGunGun, wcGunRocket}
            2, 4:
              mweapon := TT3MountedGunAutoManual.Create;
          end;
      end;
    otMwHybrid:
      mweapon := TT3MountedHybrid.Create;
  end;

  if ASsigned(mweapon) then
  begin
    mweapon.MountedDeviceOnBoardDefIndex := mweaponOnboardDef;
    mweapon.MountedDeviceDefinitionIndex := mweaponDefinition;
    mweapon.PlatformParentInstanceIndex := mweaponParentIdx;
    mweapon.Initialize;
  end;

  Result := mweapon;
end;

function TT3ObjectFactory.createPlatform(platformType: TPlatformType): TT3PlatformInstance;
var
  obj: TT3PlatformInstance;
begin

  obj := nil;

  case platformType of
    ptVehicle:
      obj := TT3Vehicle.Create;
    ptHybrid:
      obj := TT3Hybrid.Create;
    ptSonobuoy:
      obj := TT3Sonobuoy.Create;
    ptMine:
      obj := TT3Mine.Create;
    ptMissile:
      obj := TT3Missile.Create;
    ptSatellite:
      obj := TT3Satellite.Create;
    ptTorpedo:
      obj := TT3Torpedo.Create;
  end;

  if Assigned(obj) then
  begin
//    obj.GameDefaults  := queryGameDefault;
//    obj.Environment   := queryEnvironment;

    obj.Initialize;
  end;

  result := obj;
end;

{
   fungsi create platform untuk mengcreate 7 jenis platform
 }
function TT3ObjectFactory.createPlatform(platformType: TPlatformType; platformInstance: integer): TT3PlatformInstance;
var
  obj: TT3PlatformInstance;
  dbPf: TDBPlatformInstance;
  dbVeh: TDBVehicle_Definition;
begin

  {*------------------------------------------------------------------------------
    Make sure before creating sim platform, DB object platform exist in
      FDBPlatformInstance and all assets on FDBxxxxxxxOnBoardDict.
  -------------------------------------------------------------------------------}
  dbPf := simManager.Scenario.GetDBPlatformInstance(platformInstance);

  obj := nil;

  if not Assigned(dbPf) then
    Exit;

  case platformType of
    ptVehicle:
      obj := TT3Vehicle.Create;
    ptHybrid:
      obj := TT3Hybrid.Create;
    ptSonobuoy:
      obj := TT3Sonobuoy.Create;
    ptMine:
      obj := TT3Mine.Create;
    ptMissile:
      obj := TT3Missile.Create;
    ptSatellite:
      obj := TT3Satellite.Create;
    ptTorpedo:
      obj := TT3Torpedo.Create;
  end;

  if Assigned(obj) then
  begin

    obj.InstanceName            := dbPf.Instance_Name;
    obj.InstanceIndex           := dbPf.Platform_Instance_Index;
    obj.TrackID                 := dbPf.Track_ID;
    obj.PlatformType            := dbPf.Platform_Type;
    obj.ForceDesignation        := dbPf.ForceDesignation;
    obj.PlatformDefinitionIndex := dbPf.Platform_Index;

    obj.Initialize;
  end;

  result := obj;

end;

(*
   fungsi createTrack untuk membuat real time track object
 *)
function TT3ObjectFactory.createTrack(trackType: TTrackType; pfInstance: TT3Unit): TT3Track;
var
  track: TT3Track;
begin
  track := createTrack(trackType);

  if pfInstance is TT3PlatformInstance then
  begin
    track.TrackDesignation    := TT3PlatformInstance(pfInstance).ForceDesignation;
    track.TrackDomain         := TT3PlatformInstance(pfInstance).PlatformDomain;
    track.TrackTypeDomain     := TT3PlatformInstance(pfInstance).PlatformType;
    track.ObjectInstanceIndex := TT3PlatformInstance(pfInstance).InstanceIndex;
  end;

  if pfInstance is TT3MountedSensor then
  begin
    track.ObjectInstanceIndex := TT3MountedSensor(pfInstance).PlatformParent.InstanceIndex;
    track.TrackDesignation    := TT3MountedSensor(pfInstance).PlatformParent.ForceDesignation;
    track.TrackDomain         := TT3MountedSensor(pfInstance).PlatformParent.PlatformDomain;
    track.TrackTypeDomain     := TT3MountedSensor(pfInstance).PlatformParent.PlatformType;

    if track is TT3BearingTrack then
      TT3BearingTrack(track).ObjectSensorId := TT3MountedSensor(pfInstance).InstanceIndex;
  end;

  if pfInstance is TT3Vehicle then
    track.TrackLabel := TT3Vehicle(pfInstance).TrackID;

  track.setPositionX(pfInstance.getPositionX);
  track.setPositionY(pfInstance.getPositionY);
  track.setPositionZ(pfInstance.getPositionZ);

  track.TrackCategory := tcRealTrack;

  result := track;
end;

(*
   fungsi createTrack untuk membuat non real time track object
 *)
function TT3ObjectFactory.createTrack(trackType: TTrackType): TT3Track;
var
  track: TT3Track;
begin
  track := nil;
  case trackType of
    trBearing : track := TT3BearingTrack.Create;
    trAOP     : ;
    trPoint   : track := TT3PointTrack.Create;
  end;

  if Assigned(track) then
  begin
    track.TrackType     := trackType;
    track.TrackCategory := tcNonRealTrack;
  end;

  result := track;
end;

 (*
   fungsi createTrack untuk membuat real time track object dari platform dengan index
 *)
function TT3ObjectFactory.createTrack(trackType: TTrackType; platformIdx: integer): TT3Track;
var
  track: TT3Track;
begin
  track                     := createTrack(trackType);
  track.ObjectInstanceIndex := platformIdx;
  track.TrackCategory       := tcRealTrack;

  result := track;
end;

constructor TT3ObjectFactory.Create;
begin
  FLastInstanceIndex := 0;
end;

 {
   fungsi create mounted ecm untuk mengcreate 8 jenis mounted weapon
 }
function TT3ObjectFactory.createMountedCM(mcmType: TEnumMountedObjectType; mcmOnboardDef, mcmDefinition, mcmParentIdx: integer): TT3MountedDevice;
var
  ecm: TT3MountedDevice;
begin
  ecm := nil;

  case mcmType of
    otMeAcoustic         : ecm := TT3MountedAcousticDeploy.Create;
    otMeAirborneChaff    : ecm := TT3MountedAirborneChaff.Create;
    otMeSurfaceChaff     : ecm := TT3MountedSurfaceChaff.Create;
    otMeChaffLauncher    : ecm := TT3MountedChaffLauncher.Create;
    otMeFloating         : ecm := TT3MountedFloatingDecoy.Create;
    otMeInfrared         : ecm := TT3MountedInfrared.Create;
    otMeTowedJammer      : ecm := TT3MountedTowedJammer.Create;
    otMeDefensiveJammer  : ecm := TT3MountedDefensiveJammer.Create;
    otMeAirBubble        : ecm := TT3MountedAirbubble.Create;
    otMeRadarNoiseJammer : ecm := TT3MountedRadarNoiseJammer.Create;
  end;

  if ASsigned(ecm) then
  begin
    ecm.MountedDeviceOnBoardDefIndex := mcmOnboardDef;
    ecm.MountedDeviceDefinitionIndex := mcmDefinition;
    ecm.PlatformParentInstanceIndex := mcmParentIdx;
    ecm.Initialize;
  end;

  Result := ecm;
end;

 {
   fungsi create mounted sensor untuk mengcreate 9 jenis sensor
 }
function TT3ObjectFactory.createMountedSensor(msensorType: TEnumMountedObjectType; msensorOnboardDef, msensorDefinition, msensorParentIdx: integer): TT3MountedDevice;
var
  sensor: TT3MountedDevice;
begin
  sensor := nil;

  case msensorType of
    otMsRadar:
      sensor := TT3MountedRadar.Create;
    otMsSonar:
      sensor := TT3MountedSonar.Create;
    otMsVisual:
      sensor := TT3MountedVisual.Create;
    otMsEO:
      sensor := TT3MountedEO.Create;
    otMsESM:
      sensor := TT3MountedESM.Create;
    otMsMAD:
      sensor := TT3MountedMAD.Create;
    otMsIFF:
      sensor := TT3MountedIFF.Create;
    otMsFCR:
      sensor := TT3MountedRadar.Create;
    otMsSonobuoy:
      ;
  end;

  if ASsigned(sensor) then
  begin
    sensor.MountedDeviceOnBoardDefIndex := msensorOnboardDef;
    sensor.MountedDeviceDefinitionIndex := msensorDefinition;
    sensor.PlatformParentInstanceIndex := msensorParentIdx;
    sensor.Initialize;
  end;

  Result := sensor;

end;

end.

