unit uT3ObjectVisualFactory;

interface

uses
  uSimVisuals, uT3Unit, MapXLib_TLB, uSimObjects, uT3Track;

{ factory class for creating object visual }

type
  TT3ObjectVisualFactory = class
  private
    FMap : TMap;

  public
    constructor Create(Map : TMap);

    { visual factory }
    function createVisual (aName : String) : TDrawElement;overload;
    { visual factory with TT3Unit parent option }
    function createVisual (aName : String;aParent : TT3Unit) : TDrawElement;overload;
    { visual factory with TSimobject parent option }
    function createVisual (aName : String;aParent : TT3Track) : TDrawElement;overload;

    {specific visual creation}
    function createBlindZoneVisual (aParent : TSimObject) : TDrawElement;
    function createRangeVisual(aParent : TSimObject) : TDrawElement;
  end;

implementation

uses
  uObjectVisuals, uCompassVisual, uT3BlindZone,
  uT3MountedRadar;

{ TT3ObjectVisualFactory }

constructor TT3ObjectVisualFactory.Create;
begin
  FMap := Map;
end;

function TT3ObjectVisualFactory.createVisual(aName: String): TDrawElement;
var
  visual : TDrawElement;
begin

  visual := nil;

  if aName = 'RangeRing' then
  begin
    visual := TRangeRingsVisual.Create;
    TRangeRingsVisual(visual).Interval := 10;
  end
  else
  if aName = 'Line' then
  begin
    visual := TLineVisual.Create;
  end
  else
  if aName = 'Compass' then
  begin
    visual := TCompassVisual.Create;
    TCompassVisual(visual).Width := FMap.Width;
    TCompassVisual(visual).Height := FMap.Height;
  end
  else
  if aName = 'Taktis' then
  begin
    visual := TTacticalSymbol.Create;
  end
  else
  if aName = 'RangeVisual' then
  begin
    visual := TRangeVisual.Create;
  end
  else
  if aName = 'BlindZoneVisual' then
  begin
    visual := TBlindZoneVisual.Create;
  end
  else
  if aName = 'TextVisual' then
  begin
    visual := TTextVisual.Create;
  end;

  result := visual;

end;

function TT3ObjectVisualFactory.createVisual(aName: String;
  aParent: TT3Unit): TDrawElement;
var
  visual : TDrawElement;
begin
  visual := createVisual(aName);

  if Assigned(visual) then
    visual.SimObject := aParent;

  result := visual;
end;

function TT3ObjectVisualFactory.createBlindZoneVisual(aParent: TSimObject): TDrawElement;
var
  visual : TDrawElement;
begin
  visual := createVisual('BlindZoneVisual');
  if Assigned(visual) then
  begin
    TBlindZoneVisual(visual).Ranges     := TT3BlindZone(aParent).Range;
    TBlindZoneVisual(visual).StartAngle := TT3BlindZone(aParent).Start_Angle;
    TBlindZoneVisual(visual).EndAngle   := TT3BlindZone(aParent).End_Angle;
    TBlindZoneVisual(visual).Heading    := TT3BlindZone(aParent).Heading;
  end;

  result := visual;

end;

function TT3ObjectVisualFactory.createRangeVisual(
  aParent: TSimObject): TDrawElement;
var
  visual : TDrawElement;
begin
  visual := createVisual('RangeVisual');

  if Assigned(visual) then
  begin
    TRangeVisual(visual).Range := TT3MountedRadar(aParent).DetectionRange;
  end;

  result := visual;
end;

function TT3ObjectVisualFactory.createVisual(aName: String;
  aParent: TT3Track): TDrawElement;
var
  visual : TDrawElement;
begin
  visual := createVisual(aName);

  if Assigned(visual) then begin
    visual.SimObject := aParent;
  end;

  result := visual;

end;

end.
