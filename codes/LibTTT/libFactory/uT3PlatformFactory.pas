unit uT3PlatformFactory;

interface

uses
  uT3PlatformInstance, uT3MountedDevice, tttData ;

type

  { class for handling TTT object creation }

  TT3ObjectFactory = class

  public

    { platform factory }
    function platformFactory (platformType : TPlatformType; platformDefinition : TObject) : TT3PlatformInstance;
    { mounted sensor factory }
    function mountsensorFactory  (msensorType : String; msensorDefinition : TObject) : TT3MountedDevice;
    { mounted weapon factory }
    function mountweaponFactory  (mweaponType : String; mweaponDefinition : TObject) : TT3MountedDevice;
    { mounted cm factory }
    function mountcmFactory      (mcmType : String; mcmDefinition : TObject) : TT3MountedDevice;

  end;

implementation

uses
  uT3Vehicle, uT3Mine, uT3Missile, uT3Satellite, uT3Sonobuoy, uT3Hybrid, uT3Torpedo;

{ TT3ObjectFactory }

function TT3ObjectFactory.mountweaponFactory(mweaponType: String;
  mweaponDefinition: TObject): TT3MountedDevice;
begin

end;

function TT3ObjectFactory.platformFactory(platformType: TPlatformType;
  platformDefinition: TObject): TT3PlatformInstance;
var
  obj : TT3PlatformInstance;
begin

  obj := nil;

  case platformType of
    ptVehicle   : obj := TT3Vehicle.Create;
    ptHybrid    : obj := TT3Hybrid.Create;
    ptSonobuoy  : obj := TT3Sonobuoy.Create;
    ptMine      : obj := TT3Mine.Create;
    ptMissile   : obj := TT3Missile.Create;
    ptSatellite : obj := TT3Satellite.Create;
    ptTorpedo   : obj := TT3Torpedo.Create;
  end;

  result := obj;


end;

function TT3ObjectFactory.mountcmFactory(mcmType: String;
  mcmDefinition: TObject): TT3MountedDevice;
begin

end;

function TT3ObjectFactory.mountsensorFactory(msensorType: String;
  msensorDefinition: TObject): TT3MountedDevice;
begin

end;

end.
