unit uRPLibrary;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls, ExtCtrls, uT3ClientManager, Buttons, tttData,
  uGameData_TTT, ImgList, uDBClassDefinition;

type
  TfrmRPLibrary = class(TForm)
    Bevel1: TBevel;
    Notebook1: TNotebook;
    Label1: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    lbClass: TLabel;
    edtName: TEdit;
    edtTrackId: TEdit;
    Label6: TLabel;
    Label7: TLabel;
    lbClassLaunch: TLabel;
    lbNameLaunch: TLabel;
    Label8: TLabel;
    btnNext: TButton;
    btnLaunch: TButton;
    btnCancel: TButton;
    lbPosition: TLabel;
    edtPosition: TEdit;
    btnPosition: TSpeedButton;
    pnlTrackID: TPanel;
    pnlSelectPos: TPanel;
    pnlClassSelect: TPanel;
    lvObject: TListView;
    pnlSelectGroup: TPanel;
    Label2: TLabel;
    rgForceDesignation: TRadioGroup;
    lbxGroup: TListBox;
    edtHeading: TEdit;
    edtSpeed: TEdit;
    edtAltitude: TEdit;
    lbHeading: TLabel;
    lbSpeed: TLabel;
    lbAltitude: TLabel;
    tvRuntimePlatform: TTreeView;
    ImageList1: TImageList;

    procedure FormCreate(Sender: TObject);
    procedure btnCancelClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnNextClick(Sender: TObject);
    procedure btnLaunchClick(Sender: TObject);
    procedure btnPositionClick(Sender: TObject);
    procedure rgForceDesignationClick(Sender: TObject);
    procedure lbxGroupClick(Sender: TObject);
    procedure edtTrackIdKeyPress(Sender: TObject; var Key: Char);
    procedure edtTrackIdChange(Sender: TObject);
    procedure edtHeadingKeyPress(Sender: TObject; var Key: Char);
    procedure edtSpeedKeyPress(Sender: TObject; var Key: Char);
    procedure edtAltitudeKeyPress(Sender: TObject; var Key: Char);

    procedure lvObjectCompare(Sender: TObject; Item1, Item2: TListItem;
      Data: Integer; var Compare: Integer);
    procedure FormDestroy(Sender: TObject);
  private
    FDeployedObject: TObject;
    FMapPositionX: double;
    FMapPositionY: double;
    FGroupID : integer;
    rSend: TRecCmd_LaunchRP;

    FForceList: array [1..5] of TStrings;

    //procedure SetDeployedObject(const Value: TObject);
    procedure SetMapPositionX(const Value: double);
    procedure SetMapPositionY(const Value: double);
    { Private declarations }
  public
    procedure SetUpGroupAndForce;

  public
    { Public declarations }
    property MapPositionX : double read FMapPositionX write SetMapPositionX;
    property MapPositionY : double read FMapPositionY write SetMapPositionY;
  end;

var
  frmRPLibrary: TfrmRPLibrary;

implementation

uses uT3Unit,
  uBaseCoordSystem, uMapXhandler,
  uDBScenario, uT3Vehicle, uT3Common;

{$R *.dfm}

procedure TfrmRPLibrary.btnCancelClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmRPLibrary.btnLaunchClick(Sender: TObject);
var
  vIndex : integer;
  rSend: TRecCmd_LaunchRP;
begin
  if trim(edtPosition.Text) = '' then exit;
  if FDeployedObject = nil then exit;

  vIndex := 0;

  if FDeployedObject is TDBVehicle_Definition then
  begin
    rSend.NewRPType := 1;
    vIndex := TDBVehicle_Definition(FDeployedObject).Vehicle_Index
  end
  else
  if FDeployedObject is TDBMine_Definition then
  begin
    rSend.NewRPType := 2;
    vIndex := TDBMine_Definition(FDeployedObject).Mine_Index
  end
  else
  if FDeployedObject is TDBMissile_Definition then
  begin
    rSend.NewRPType := 3; // or 5 if hybrid
    vIndex := TDBMissile_Definition(FDeployedObject).Missile_Index
  end
  else
  if FDeployedObject is TDBTorpedo_Definition then
  begin
    rSend.NewRPType := 4;
    vIndex := TDBTorpedo_Definition(FDeployedObject).Torpedo_Index
  end
  else
  if FDeployedObject is TDBSatellite_Definition then
  begin
    rSend.NewRPType := 6;
    vIndex := TDBSatellite_Definition(FDeployedObject).Satellite_Index
  end
  else
  if FDeployedObject is TDBSonobuoy_Definition then
  begin
    rSend.NewRPType := 7;
    vIndex := TDBSonobuoy_Definition(FDeployedObject).Sonobuoy_Index;
  end;

  rSend.RPPlatformID   := vIndex; // ID nya launcher / vehicle def
  rSend.NewPlatformID  := 0;  // digenerate server. client kirim 0
  rSend.RPGroupID      := FGroupID;
  StrToChar(edtName.Text, rSend.InstanceName);
  StrToChar(edtTrackId.Text, rSend.TrackIdent);
  rSend.IDPlatformName := 0;
  rSend.IDPlatformClass := 0;

  StrToChar('', rSend.namePlatform);
  StrToChar('', rSend.nameClass);
  StrToChar('', rSend.VBSClassName);

  rSend.pX := MapPositionX;
  rSend.pY := MapPositionY;

  rSend.PHeading  := StrToFloat(edtHeading.Text);
  rSend.PSpeed    := StrToFloat(edtSpeed.Text);

  if FDeployedObject <> nil then begin

    if FDeployedObject is TDBMine_Definition then
    begin
      rSend.PAltitude := StrToFloat(edtAltitude.Text);
    end;

    if FDeployedObject is TDBMissile_Definition then begin
      rSend.PAltitude := StrToFloat(edtAltitude.Text) * C_Feet_To_Meter;
    end;

    if FDeployedObject is TDBVehicle_Definition then begin
      if TDBVehicle_Definition(FDeployedObject).Platform_Domain = 0 then //air
      begin
        rSend.PAltitude := StrToFloat(edtAltitude.Text) * C_Feet_To_Meter;
      end
      else
      if  TDBVehicle_Definition(FDeployedObject).Platform_Domain = 2 then //subsurface
      begin
        rSend.PAltitude := StrToFloat(edtAltitude.Text);
      end;
    end;
  end;

  case rgForceDesignation.ItemIndex of
    0 : rSend.ForceDesignation  := 1;   //mk Force Designation 27042012
    1 : rSend.ForceDesignation  := 2;   //mk Force Designation 27042012
    2 : rSend.ForceDesignation  := 3;   //mk Force Designation 27042012
    3 : rSend.ForceDesignation  := 4;   //mk Force Designation 27042012
    4 : rSend.ForceDesignation  := 5;   //mk Force Designation 27042012
  end;

  rSend.OrderID := CORD_ID_LAUNCH_SINGLE_RUNTIME_PLATFORM;
  rSend.MaxRowIndex := 0;
  rSend.MaxColumnIndex := 0;

  clientManager.NetCmdSender.CmdLaunchRP(rSend);

  Close;
end;

procedure TfrmRPLibrary.btnNextClick(Sender: TObject);
var
  s: string;
begin
  if not Assigned(tvRuntimePlatform.Selected) then
  Exit;

  FDeployedObject := tvRuntimePlatform.Selected.Data;

  if FDeployedObject = nil then
    exit;

  case Notebook1.PageIndex of
    0 : begin
      Notebook1.PageIndex := 1;

      if FDeployedObject is TDBVehicle_Definition then begin
        s := TDBVehicle_Definition(FDeployedObject).Vehicle_Identifier;
        lbClass.Caption := s;
        edtName.Text    := s;
      end else
      if FDeployedObject is TDBMissile_Definition then begin
        s := TDBMissile_Definition(FDeployedObject).Class_Identifier;
        lbClass.Caption := s;
        edtName.Text    := s;
      end else
      if FDeployedObject is TDBTorpedo_Definition then begin
        s := TDBTorpedo_Definition(FDeployedObject).Class_Identifier;
        lbClass.Caption := s;
        edtName.Text    := s;
      end else
      if FDeployedObject is TDBMine_Definition then begin
        s := TDBMine_Definition(FDeployedObject).Mine_Identifier;
        lbClass.Caption := s;
        edtName.Text    := s;
      end else
      {if FDeployedObject.ClassType = TSonobuoy_On_Board then begin
        lbClass.Caption := TSonobuoy_On_Board(FDeployedObject).FDef.Class_Identifier;
        edtName.Text := TSonobuoy_On_Board(FDeployedObject).FDef.Class_Identifier;
      end; }

      btnLaunch.Enabled := false;
      btnNext.Enabled := false;
    end;
    1 :  begin
      if (Length(Trim(edtName.Text)) > 0) and (Length(Trim(edtTrackId.Text)) > 0) then
      begin
        Notebook1.PageIndex := 2;
        if FDeployedObject <> nil then begin

          if FDeployedObject is TDBMine_Definition then
          begin
           lbAltitude.Enabled := True;
           edtAltitude.Enabled := True;
           edtAltitude.Text := '0';
           lbAltitude.Caption := 'Depth :'
          end;

          if FDeployedObject is TDBMissile_Definition then begin
            lbAltitude.Enabled := True;
            edtAltitude.Enabled := True;
            lbAltitude.Caption := 'Altitude :'
          end;

          if FDeployedObject is TDBVehicle_Definition then begin
            if TDBVehicle_Definition(FDeployedObject).Platform_Domain = 0 then
            begin
              lbAltitude.Enabled := True;
              edtAltitude.Enabled := True;
              lbAltitude.Caption := 'Altitude :'
            end
            else
            if  TDBVehicle_Definition(FDeployedObject).Platform_Domain = 2 then
            begin
               lbAltitude.Enabled := True;
               edtAltitude.Enabled := True;
               edtAltitude.Text := '0';
               lbAltitude.Caption := 'Depth :'
            end
            else
            begin
               lbAltitude.Enabled := False;
               edtAltitude.Enabled := False;
            end;

            lbClassLaunch.Caption := lbClass.Caption;
            lbNameLaunch.Caption  := edtName.Text;
          end;
        end;

        btnLaunch.Enabled := false;
        btnNext.Enabled   := true;
      end
      else
      begin
        MessageDlg('Name or ID Track is empty!', mtWarning, [mbOK], 0);
      end;
    end;
    2: begin
      if FDeployedObject is TDBVehicle_Definition then begin
        if TDBVehicle_Definition(FDeployedObject).Platform_Domain = 0 then
        begin
          if edtAltitude.Text = '0' then
          begin
            MessageDlg('Air Platform Must Have Altitude More Than 0 Meter', mtWarning, [mbOK], 0);
            Exit;
          end;
        end;
      end;

      if (Length(Trim(edtPosition.Text)) > 0) then
      begin
        Notebook1.PageIndex := 3;
        btnLaunch.Enabled := false;
        btnNext.Enabled   := false;
      end
      else
      begin
        MessageDlg('Value of Deploy Positions is empty!', mtWarning, [mbOK], 0);
      end;
    end;
  end;
end;

procedure TfrmRPLibrary.btnPositionClick(Sender: TObject);
begin
  clientManager.SimMap.FMap.CurrentTool := mtDeployPosition;
end;

procedure TfrmRPLibrary.edtAltitudeKeyPress(Sender: TObject; var Key: Char);
var
  ValKey : set of AnsiChar;
begin
  ValKey := [#48 .. #57, #8, #13, #46];
  if not(CharInSet(Key, ValKey)) then
    Key := #0;
end;

procedure TfrmRPLibrary.edtHeadingKeyPress(Sender: TObject; var Key: Char);
var
  ValKey : set of AnsiChar;
begin
  ValKey := [#48 .. #57, #8, #13, #46];
  if not(CharInSet(Key, ValKey)) then
    Key := #0;
end;

procedure TfrmRPLibrary.edtSpeedKeyPress(Sender: TObject; var Key: Char);
var
  ValKey : set of AnsiChar;
begin
  ValKey := [#48 .. #57, #8, #13, #46];
  if not(CharInSet(Key, ValKey)) then
    Key := #0;
end;

procedure TfrmRPLibrary.edtTrackIdChange(Sender: TObject);
begin
  if (Length(edtTrackId.Text) > 0) then
    btnNext.Enabled := True
  else
    btnNext.Enabled := False;
end;

procedure TfrmRPLibrary.edtTrackIdKeyPress(Sender: TObject; var Key: Char);
begin
  if Key = #13 then
  begin
    if (Length(edtTrackId.Text) > 0) then
      btnNext.Enabled := True
    else
      btnNext.Enabled := False;
  end
  else
    btnNext.Enabled := False;
end;

procedure TfrmRPLibrary.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  frmRPLibrary := nil;
  Action := caFree;
end;

procedure TfrmRPLibrary.FormCreate(Sender: TObject);
var
  i,j : integer;
  aObject : TObject;

  tn, tnItem : TTreeNode;
  itemRP     : TDBPairRuntimePlatformLibraryDict;
  itemEntry  : TDBPlatform_Library_Entry;

  vehDefinition : TDBVehicle_Definition;
  missDef    : TDBMissile_Definition;
  mineDef    : TDBMine_Definition;
  torpDef    : TDBTorpedo_Definition;
  satDef     : TDBSatellite_Definition;
  sonoDef    : TDBSonobuoy_Definition;
begin

  for i := 1 to 5 do begin
    FForceList[i] := TStringList.Create;
  end;

  tvRuntimePlatform.Items.Clear;

  for itemRP in clientManager.Scenario.DBRuntimePlatformLibrary do
  begin
    tn := TTreeNode.Create(tvRuntimePlatform.Items);
    tn := tvRuntimePlatform.Items.Add(tn, itemRP.Key.Library_Name);

    for itemEntry in itemRP.Value do begin

      with itemEntry do
      begin
        aObject := nil;

        case Platform_Type of
          // vehicle
          1 :
          begin
            vehDefinition := clientManager.Scenario.GetDBVehicleDefinition(Platform_Index);
            if Assigned(vehDefinition) then
            tnItem := tvRuntimePlatform.Items.AddChildObject(tn,
              vehDefinition.Vehicle_Identifier, vehDefinition);
          end;
          // missile
          2 :
          begin
            missDef := clientManager.Scenario.GetDBMissileDefintion(Platform_Index);
            if Assigned(missDef) then
            tnItem := tvRuntimePlatform.Items.AddChildObject(tn,
              missDef.Class_Identifier, missDef);
          end;
          // mine
          3 :
          begin
            mineDef := clientManager.Scenario.GetDBMineDefintion(Platform_Index);
            if Assigned(mineDef) then
            tnItem := tvRuntimePlatform.Items.AddChildObject(tn,
              mineDef.Mine_Identifier, mineDef);
          end;
          // torpedo
          4 :
          begin
            torpDef := clientManager.Scenario.GetDBTorpedoDefintion(Platform_Index);
            if Assigned(torpDef) then
            tnItem := tvRuntimePlatform.Items.AddChildObject(tn,
              torpDef.Class_Identifier, torpDef);
          end;
          // hybrid
          5 :
          begin
            missDef := clientManager.Scenario.GetDBMissileDefintion(Platform_Index);
            if Assigned(missDef) then
            tnItem := tvRuntimePlatform.Items.AddChildObject(tn,
              missDef.Class_Identifier, missDef);
          end;
          // satellite
          6 :
          begin
            satDef := clientManager.Scenario.GetDBSatelliteDefinition(Platform_Index);
            if Assigned(satDef) then
            tnItem := tvRuntimePlatform.Items.AddChildObject(tn,
              satDef.Satellite_Identifier, satDef);
          end;
          // sonobuoy
          7 :
          begin
            sonoDef := clientManager.Scenario.GetDBSonoBuoyDefinition(Platform_Index);
            if Assigned(sonoDef) then
            tnItem := tvRuntimePlatform.Items.AddChildObject(tn,
              sonoDef.Class_Identifier, sonoDef);
          end;
        end;
      end;
    end;
  end;

  FGroupID := 0;
  Notebook1.PageIndex := 0;
  btnLaunch.Enabled   := false;
  btnNext.Enabled     := true;
end;

procedure TfrmRPLibrary.FormDestroy(Sender: TObject);
var
  item : TTreeNode;
  i : integer;
begin
  while tvRuntimePlatform.Items.Count > 0 do
  begin
    item := tvRuntimePlatform.Items[0];
    item.Free;
  end;

  for i := 1 to 5 do begin
    FForceList[i].Free;
  end;

end;

procedure TfrmRPLibrary.lbxGroupClick(Sender: TObject);
var g: TDBCubicle_Group;
begin
  btnLaunch.Enabled := lbxGroup.ItemIndex >= 0;

  if lbxGroup.ItemIndex < 0 then
    exit;

  g := lbxGroup.Items.Objects[lbxGroup.ItemIndex] as TDBCubicle_Group;
  if Assigned(g) then
    FGroupID := g.Group_Index
  else
    FGroupID := 0;
end;

procedure TfrmRPLibrary.lvObjectCompare(Sender: TObject; Item1,
  Item2: TListItem; Data: Integer; var Compare: Integer);
begin
//  Compare := CompareText(Item1.Caption, Item2.Caption);
end;

procedure TfrmRPLibrary.rgForceDesignationClick(Sender: TObject);
var i, j: integer;
begin
  lbxGroup.Items.Clear;

  i := rgForceDesignation.ItemIndex + 1;
  if (i > 0) and (i < 6) then begin
    for j := 0 to FForceList[i].Count - 1 do
      lbxGroup.Items.AddObject(FForceList[i][j],FForceList[i].Objects[j]);

    //lbxGroup.Items.Add('Controler');
  end;

  if lbxGroup.Count > 0 then begin
    btnLaunch.Enabled := true;
    lbxGroup.ItemIndex := 0;
    lbxGroupClick(lbxGroup);
  end
  else begin
    btnLaunch.Enabled := false;
  end;

end;

procedure TfrmRPLibrary.SetMapPositionX(const Value: double);
begin
  FMapPositionX := Value;
  edtPosition.Text := formatDM_longitude(FMapPositionX) + '  '
  + formatDM_latitude(FMapPositionY);
end;

procedure TfrmRPLibrary.SetMapPositionY(const Value: double);
begin
  FMapPositionY := Value;
  edtPosition.Text := formatDM_longitude(FMapPositionX) + '  '
  + formatDM_latitude(FMapPositionY);
end;

procedure TfrmRPLibrary.SetUpGroupAndForce;
var i, f: integer;
    grp: TDBCubicle_Group;
begin
  for i := 1 to 5 do begin
    FForceList[i].Clear;
  end;

  for grp in clientManager.Scenario.DBCubicleGroupList do
  begin

    f := grp.Force_Designation;
    if (f < 1) or (f > 5 ) then
      f := 5;

    FForceList[f].AddObject(grp.Group_Identifier, grp);
  end;

  rgForceDesignation.ItemIndex := 0;
end;

end.

