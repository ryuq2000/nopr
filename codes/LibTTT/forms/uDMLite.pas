unit uDMLite;

interface

uses
  SysUtils, Classes, ZAbstractConnection, ZConnection, Dialogs,
  uDBClassDefinition, ZDataset, ZStoredProcedure, Generics.Collections,
  DB, ZAbstractRODataset, ZAbstractDataset;

type

  TDMLite = class(TDataModule)
    ZConn: TZConnection;
    zstrdprc1: TZStoredProc;
  private
    { Private declarations }

    {*------------------------------------------------------------------------------
      Create and return storedprocedure object;

      @param aSpName Stored procedure name
      @return
    -------------------------------------------------------------------------------}
    function createStoredProcedure(aSpName : String) : TZStoredProc;

  public
    { Public declarations }

    {*------------------------------------------------------------------------------
    Initialize Database

    @param zDbServer Server address
    @param zDBProto  Protocol
    @param zDBname   Database NAme
    @param zDBuser   User name
    @param zDBPass   Password
    @return
    -------------------------------------------------------------------------------}
    function InitZDB(const zDbServer, zDBProto, zDBname, zDBuser,
      zDBPass: string): boolean;

    {*------------------------------------------------------------------------------
      Get Scenario info

      @param id   sceario id
      @param rec  TScenario_Definition varable
      @return
    -------------------------------------------------------------------------------}
    function GetScenario(const id: Integer; var rec: TDBScenario_Definition)
      : boolean;

    {*------------------------------------------------------------------------------
      Get Resource allocation

      @param id alloc id
      @param rec
      @return
    -------------------------------------------------------------------------------}
    function GetResourceAlloc(const id: Integer; var rec: TDBResource_Allocation)
      : boolean;

    {*------------------------------------------------------------------------------
      Get Asset Deployment data

      @param id
      @param asset
      @return
    -------------------------------------------------------------------------------}
    function GetAssetDeployment(const id: Integer; var asset: TDBAsset_Deployment_Definition)
      : boolean;

    {*------------------------------------------------------------------------------
      Get Game Environment

      @param id game environment index
      @param rec
      @return
    -------------------------------------------------------------------------------}
    function GetGame_Environment_Definition(const id: Integer;var rec: TDBGame_Environment_Definition): boolean;

    {*------------------------------------------------------------------------------
      Get Game Area Definition

      @param id Game Area Index
      @param gameArea
      @return
    -------------------------------------------------------------------------------}
    function GetGame_Area_DefByID(const id: Integer;var gameArea: TDBGame_Area_Definition): Integer;

    {*------------------------------------------------------------------------------
      Get Sub area defintion

      @param id  Enviro_Index
      @param rectList list of TDBSubAreaDef
      @return
    -------------------------------------------------------------------------------}
    function GetSubArea_Enviro_Definition(const id: Integer; var rectList: TDBSubAreaDefinitionList): boolean;

    {*------------------------------------------------------------------------------
      Get Game default from sp get_g_defaults_alloc_info

      @param id resource alloc id
      @param rec
      @return
    -------------------------------------------------------------------------------}
    function GetGame_Defaults(const id: Integer; var rec: TDBGame_Defaults) : boolean;

    {*------------------------------------------------------------------------------
      Get default cloud on ESM from sp get_g_cloud_esm_info

      @param id game def index
      @param rec
      @return
    -------------------------------------------------------------------------------}
    function GetGame_Cloud_On_ESM(const id: Integer; var rec: TDBGameCloudOnESMList): boolean;

    {*------------------------------------------------------------------------------
      Get default CLoud on Radar from sp get_g_cloud_radar_info

      @param id Game def Index
      @param rec
      @return
    -------------------------------------------------------------------------------}
    function GetGame_Cloud_On_Radar(const id: Integer; var rec: TDBGameCloudRadarList): boolean;

    {*------------------------------------------------------------------------------
      Get default IFF Mode from sp get_g_def_iff_info

      @param id Game def Index
      @param rec
      @return
    -------------------------------------------------------------------------------}
    function GetGame_Default_IFF_Mode_Code(const id: Integer; var rec: TDBGameDefaultIFFModeList): boolean;

    {*------------------------------------------------------------------------------
      Get default Rainfall on ESM from sp get_g_rain_esm_info

      @param id Game def Index
      @param rec
      @return
    -------------------------------------------------------------------------------}
    function GetGame_Rainfall_On_ESM(const id: Integer; var rec: TDBGameRainfallOnESMList): boolean;

    {*------------------------------------------------------------------------------
      Get default Rainfall on Missile seek from sp get_g_rain_missile_seeker_info

      @param id game def index
      @param rec
      @return
    -------------------------------------------------------------------------------}
    function GetGame_Rainfall_On_Missile_Seeker(const id: Integer; var rec: TDBGameRainfallOnMissSeekerList): boolean;

    {*------------------------------------------------------------------------------
      Get Rainfall on Radar from sp get_g_rain_radar_info

      @param id game def index
      @param rec
      @return
    -------------------------------------------------------------------------------}
    function GetGame_Rainfall_On_Radar(const id: Integer; var rec: TDBGameRainfallOnRadarList): boolean;

    {*------------------------------------------------------------------------------
      Get default ranifall on sonar from sp get_g_rain_sonar_info

      @param id game def index
      @param rec
      @return
    -------------------------------------------------------------------------------}
    function GetGame_Rainfall_On_Sonar(const id: Integer;var rec: TDBGameRainfallOnSonarList): boolean;

    {*------------------------------------------------------------------------------
      Get default sea on missile safe alt from sp get_g_sea_missile_safe_altitude_info

      @param id game def index
      @param rec
      @return
    -------------------------------------------------------------------------------}
    function GetGame_Sea_On_Missile_Safe_Altitude(const id: Integer;
      var rec: TDBGame_Sea_On_Missile_Safe_Altitude): boolean;

    {*------------------------------------------------------------------------------
      Get default sea on radar from sp get_g_sea_radar_info

      @param id game def index
      @param rec
      @return
    -------------------------------------------------------------------------------}
    function GetGame_Sea_On_Radar(const id: Integer; var rec: TDBGame_Sea_On_Radar): boolean;

    {*------------------------------------------------------------------------------
      Get default sea on sonar from sp get_g_sea_sonar_info

      @param id game def index
      @param rec
      @return
    -------------------------------------------------------------------------------}
    function GetGame_Sea_On_Sonar(const id: Integer; var rec: TDBGameSeaOnSonarlist): boolean;

    {*------------------------------------------------------------------------------
      Get default ship on sonar from sp get_g_ship_sonar_info

      @param id game def index
      @param rec
      @return
    -------------------------------------------------------------------------------}
    function GetGame_Ship_On_Sonar(const id: Integer; var rec: TDBGameShipOnSonarList): boolean;

    {*------------------------------------------------------------------------------
      Get all DB platform instance

      @param alloc_ID
      @param aRec
      @return
    -------------------------------------------------------------------------------}
    function getPlatFormInstances(const alloc_ID: Integer;
      var aRec: TDBPlatformInstanceDict): Integer;

    {*------------------------------------------------------------------------------
      get library instance from sp get_gl_library_instances and get_gl_library_entry_info

      @param id Resource_Alloc_Index
      @param pList
      @return
    -------------------------------------------------------------------------------}
    function Get_Library_Instance(const id: Integer; var pList: TDBRuntimePlatformLibraryDict): boolean;


    {*------------------------------------------------------------------------------
      get library instance from sp get_gl_library_instances and get_gl_library_entry_info

      @param id Resource_Alloc_Index
      @param pList
      @return
    -------------------------------------------------------------------------------}
    function getAllReference_Point(const ra_id: integer; var aRec: TDBReferencePointList): Integer;

    {*------------------------------------------------------------------------------
      Get Platform Activation from get_activation_info

      @param deploy_id dployment index
      @param pf_inst_id pf intance index
      @param actDict
      @return platform event index
    -------------------------------------------------------------------------------}
    function GetPlatform_Activation(const deploy_id, pf_inst_id: integer; dict : TDBPlatformActivationDict): TDBPlatform_Activation;

    {*------------------------------------------------------------------------------
      Get Vehicle definition and add to Dictionary if not exist in it

      @param Vehicle_Index
      @param defDict
      @return
    -------------------------------------------------------------------------------}
    function GetVehicle_Definition(const v_idx: Integer;
          dict: TDBVehicleDefinitionDict): TDBVehicle_Definition;

    {*------------------------------------------------------------------------------
      get heli land launch limit for vehicle, add to dictionary if not exist

      @param vehicle_id
      @param dict
      @return
    -------------------------------------------------------------------------------}
    function GetHeliLaunchLimits(const vehicle_id: Integer; dict: TDBHelicopterLandLaunchLimitsDict): TDBHelicopter_Land_Launch_Limits;

    {*------------------------------------------------------------------------------
      Get Motion characteristic for platform, add to dictionary if not exist

      @param motion_id
      @param dict
      @return
    -------------------------------------------------------------------------------}
    function GetMotion_Characteristics(const motion_id: Integer; dict: TDBMotionCharacteristicsDict): TDBMotion_Characteristics;

    {*------------------------------------------------------------------------------
      Get missile defintion from sp get_missile_info

      @param missile_id
      @param dict
      @return
    -------------------------------------------------------------------------------}
    function GetMissile_Definition(const missile_id : integer; dict: TDBMissileDefinitionDict): TDBMissile_Definition;

    {*------------------------------------------------------------------------------
      Get mine definition from sp get_mine_info

      @param mine_id
      @param dict
      @return
    -------------------------------------------------------------------------------}
    function GetMine_Definition(const mine_id : integer; dict: TDBMineDefinitionDict): TDBMine_Definition;

    {*------------------------------------------------------------------------------
      Get torpedo definition from sp get_torpedo_info

      @param torp_id
      @param dict
      @return
    -------------------------------------------------------------------------------}
    function GetTorpedo_Definition(const torp_id : integer; dict: TDBTorpedoDefinitionDict): TDBTorpedo_Definition;

    {*------------------------------------------------------------------------------
      Get Satellite defintion from sp get_satellite_info

      @param sat_id
      @param dict
      @return
    -------------------------------------------------------------------------------}
    function GetSatellite_Definition(const sat_id : integer; dict: TDBSatelliteDefinitionDict): TDBSatellite_Definition;

    {*------------------------------------------------------------------------------
      Get Sonobuoy definition from sp  get_sonobuoy_info

      @param sono_id
      @param dict
      @return
    -------------------------------------------------------------------------------}
    function GetSonobuoy_Definition(const sono_id : integer; dict: TDBSonobuoyDefinitionDict): TDBSonobuoy_Definition;

    {*------------------------------------------------------------------------------
      Get EO on Board for vehicle from [get_eo_instances]

      @param veh_id
      @param dict
      @return
    -------------------------------------------------------------------------------}
    function GetEOOnBoard(const veh_id : integer; dict: TDBEOOnBoardDict): TDBEOOnBoardList;

    {*------------------------------------------------------------------------------
      Get EO definition from sp [get_eo_info]

      @param eo_id
      @param dict
      @return
    -------------------------------------------------------------------------------}
    function GetEO_Definition(const eo_id : integer; dict: TDBEODetectionDefinitionDict): TDBEO_Detection_Definition;

    {*------------------------------------------------------------------------------
      Get ESM on Board for vehicle from [get_esm_instances]

      @param veh_id
      @param dict
      @return
    -------------------------------------------------------------------------------}
    function GetESMOnBoard(const veh_id : integer; dict: TDBESMOnBoardDict): TDBESMOnBoardList;

    {*------------------------------------------------------------------------------
      Get ESM definition from sp [get_esm_info]

      @param esm_id
      @param dict
      @return
    -------------------------------------------------------------------------------}
    function GetESM_Definition(const esm_id : integer; dict: TDBESMDefinitionDict): TDBESM_Definition;

    {*------------------------------------------------------------------------------
      Get IFF on Board for vehicle from [get_iff_instances]

      @param veh_id
      @param dict
      @return
    -------------------------------------------------------------------------------}
    function GetIFFOnBoard(const veh_id : integer; dict: TDBIFFSensorOnBoardDict): TDBIFFOnBoardList;

    {*------------------------------------------------------------------------------
      Get MAD on Board for vehicle from [get_mad_instances]

      @param veh_id
      @param dict
      @return
    -------------------------------------------------------------------------------}
    function GetMADOnBoard(const veh_id : integer; dict: TDBMADSensorOnBoardDict): TDBMADOnBoardList;

    {*------------------------------------------------------------------------------
      Get MAD Definition from sp  get_mad_info

      @param mad_id
      @param dict
      @return
    -------------------------------------------------------------------------------}
    function GetMAD_Definition(const mad_id : integer; dict: TDBMADDefinitionDict): TDBMAD_Definition;

    {*------------------------------------------------------------------------------
      Get Visual On Board

      @param veh_id
      @param dict
      @return
    -------------------------------------------------------------------------------}
    function GetVisualOnBoard(const veh_id : integer; dict: TDBVisualOnBoardDict): TDBVisualOnBoardList;

    {*------------------------------------------------------------------------------
      Get FCR On Board

      @param veh_id
      @param dict
      @return
    -------------------------------------------------------------------------------}
    function GetFCROnBoard(const veh_id : integer; dict: TDBFCROnBoardDict): TDBFCROnBoardList;

    {*------------------------------------------------------------------------------
      Get FCR Definition

      @param fcr_id
      @param dict
      @return
    -------------------------------------------------------------------------------}
    function GetFCRDefinition(const fcr_id : integer; dict: TDBFCRDefinitionDict): TDBFCR_Definition;

    {*------------------------------------------------------------------------------
      Get Radar On Board

      @param veh_id
      @param dict
      @return
    -------------------------------------------------------------------------------}
    function GetRadarOnBoard(const veh_id : integer; dict: TDBRadarOnBoardDict): TDBRadarOnBoardList;

    {*------------------------------------------------------------------------------
      Get Radar Definition

      @param rdr_id
      @param dict
      @return
    -------------------------------------------------------------------------------}
    function GetRadarDefinition(const rdr_id : integer; dict: TDBRadarDefinitionDict): TDBRadar_Definition;

    {*------------------------------------------------------------------------------
      Get Sonar On Board

      @param veh_id
      @param dict
      @return
    -------------------------------------------------------------------------------}
    function GetSonarOnBoard(const veh_id : integer; dict: TDBSonarOnBoardDict): TDBSonarOnBoardList;

    {*------------------------------------------------------------------------------
      Get sonar definition

      @param snr_id
      @param dict
      @return
    -------------------------------------------------------------------------------}
    function GetSonarDefinition(const snr_id : integer; dict: TDBSonarDefinitionDict): TDBSonar_Definition;

    {*------------------------------------------------------------------------------
      Get mounted weapon on board

      @param veh_id
      @param dict
      @return
    -------------------------------------------------------------------------------}
    function GetFittedWeaponOnBoard(const veh_id : integer; dict: TDBFitWeaponOnBoardDict): TDBFitWeaponOnBoardList;
    function GetFittedWeaponLauncherOnBoard(const fitted_wpn_id : integer; dict: TDBFitWeaponLauncherOnBoardDict): TDBFittedWeaponLauncherList;

    {*------------------------------------------------------------------------------
      Get point effect on board

      @param veh_id
      @param dict
      @return
    -------------------------------------------------------------------------------}
    function GetPointEffectOnBoard(const veh_id : integer; dict: TDBPointEffectOnBoardDict): TDBPointEffectOnBoardList;

    {*------------------------------------------------------------------------------
      Get Gun Defintion

      @param gun_id
      @param dict
      @return
    -------------------------------------------------------------------------------}
    function GetGunDefinition(const gun_id : integer; dict: TDBGunDefinitionDict): TDBGun_Definition;

    {*------------------------------------------------------------------------------
      Get bomb defintion

      @param bomb_id
      @param dict
      @return
    -------------------------------------------------------------------------------}
    function GetBombDefinition(const bomb_id : integer; dict: TDBBombDefinitionDict): TDBBomb_Definition;

    function GetAcousticDecoyOnBoard(const veh_id : integer; dict: TDBAcousticDecoyOnBoardDict): TDBAcousticDecoyOnBoardList;
    function GetAcousticDecoyDefinition(const decoy_id : integer; dict: TDBAcousticDecoyDict): TDBAcoustic_Decoy_Definition;

    function GetAirBubbleOnBoard(const veh_id : integer; dict: TDBAirBubbleOnBoardDict): TDBAirBubbleOnBoardList;
    function GetAirBubbleDefinition(const bubble_id : integer; dict: TDBAirBubbleDict): TDBAir_Bubble_Definition;

    function GetChaffOnBoard(const veh_id : integer; dict: TDBChaffOnBoardDict): TDBChaffOnBoardList;
    function GetChaffDefinition(const chaff_id : integer; dict: TDBChaffDefinitionDict): TDBChaff_Definition;

    function GetChaffLauncherOnBoard(const veh_id : integer; dict: TDBChaffLauncherOnBoardDict): TDBChaffLauncherOnBoardList;

    function GetDefJammerOnBoard(const veh_id : integer; dict: TDBDefensiveJammerOnBoardDict): TDBDefensiveJammerOnBoardList;
    function GetDefJammerDefinition(const jammer_id : integer; dict: TDBDefensiveJammerDefinitionDict): TDBDefensive_Jammer_Definition;

    function GetFloatingDecoyOnBoard(const veh_id : integer; dict: TDBFloatingDecoyOnBoardDict): TDBFloatingDecoyOnBoardList;
    function GetFloatingDecoyDefinition(const decoy_id : integer; dict: TDBFloatingDecoyDefinitionDict): TDBFloating_Decoy_Definition;

    function GetInfraredDecoyOnBoard(const veh_id : integer; dict: TDBInfraredDecoyOnBoardDict): TDBInfraredDecoyOnBoardList;
    function GetInfraredDecoyDefinition(const decoy_id : integer; dict: TDBInfraredDecoyDefinitionDict): TDBInfrared_Decoy_Definition;

    function GetJammerOnBoard(const veh_id : integer; dict: TDBJammerOnBoardDict): TDBJammerOnBoardList;
    function GetJammerDefinition(const jammer_id : integer; dict: TDBJammerDefinitionDict): TDBJammer_Definition;

    function GetTowedJammerOnBoard(const veh_id : integer; dict: TDBTowedJammerOnBoardDict): TDBTowedJammerOnBoardList;
    function GetTowedJammerDefinition(const jammer_id : integer; dict: TDBTowedJammerDecoyDefDict): TDBTowed_Jammer_Decoy_Definition;

    function GetSonobuoyOnBoard(const veh_id : integer; dict: TDBSonobuoyOnBoardDict): TDBSonobuoyOnBoardList;

    function GetFormationList(const deploy_id : Integer; list: TDBFormationList):boolean;
    function GetFormationAssignments(const frm_id : Integer; dict: TDBFormationAssigmentDict): TDBFormationAssignmentList;

    function GetResource_Overlay_Mapping(const id: Integer; recList: TDBOverlayMappingList): boolean;
    //function GetExternalCommunicationChannel(const alloc_id : Integer; recList : TDBExtCommChannelList) : boolean;overload;
    function GetExternalCommunicationChannel(const alloc_id, group_id : Integer; dict : TDBExtCommChannelGroupDict) : TDBExtCommChannelGroupList;overload;

    function GetLinkDefinition(const deploy_id: Integer; var list: TDBLinkDefinitionList): Boolean;
    function GetLinkParticipant(const link_id : integer; dict : TDBLinkParticipantsDict): TDBLinkParticipantsList;

    function GetCubicleGroupList(const deploy_id: Integer; list : TDBCubicleGroupList): Boolean;
    function GetCubicleGroupMembers(const grpID: Integer; dict : TDBCubicleAssignmentDict ): TDBCubicleGroupMemberList;

    function GetMinePOD(const mineIndex : integer; dict : TDBMine_POD_vs_RangeDict) : TDBMine_POD_vs_RangeList;
    function GetTorpedoPOH(torpIndex : integer; dict : TDBTorpedo_POH_ModifierDict) : TDBTorpedo_POH_ModifierList;
    function GetSonarPODCurve(curveIndex : integer; dict : TDBSonar_POD_CurveDict) : TDBSonar_POD_CurveList;

    function GetBlindZones(const asset_id,asset_type : integer;dict : TDBBlindZoneDict) : TDBBlindZoneDefinitionList;
    function GetVerticalCoverage(const rdr_id : integer; dict : TDBRadar_Vertical_CovDict) : TDBRadar_Vertical_CovList;

  end;

var
  DMLite: TDMLite;

implementation


{$R *.dfm}

{ TDM }

function TDMLite.createStoredProcedure(aSpName : String): TZStoredProc;
var
  sp : TZStoredProc;
begin
  result := nil;

  if ZConn.Connected then
  begin

    try
      sp := TZStoredProc.Create(Self);
      sp.Connection := ZConn;
      sp.ParamCheck := True;
      sp.StoredProcName := aSpName;
    except
      sp.Free;
      sp := nil;
    end;

    result := sp;

  end;

end;


function TDMLite.GetAcousticDecoyDefinition(const decoy_id: integer;
  dict: TDBAcousticDecoyDict): TDBAcoustic_Decoy_Definition;
var
  sp : TZStoredProc;
  decoyDefinition : TDBAcoustic_Decoy_Definition;
  newData : Boolean;
begin
  result := nil;
  newData := False;

  { search in dict first }
  if not dict.ContainsKey(decoy_id) then
    newData := True
  else
    result := dict.Items[decoy_id];

  { new data add to dict }
  if newData then
  begin
    sp := createStoredProcedure('get_decoy_info');
    if Assigned(sp) then
      with sp do
      begin
        Params.ParamByName('@index').AsInteger := decoy_id;

        try
          Open;

          if not IsEmpty then
          begin
            First;

            decoyDefinition := TDBAcoustic_Decoy_Definition.Create;
            dict.Add(decoy_id,decoyDefinition);

            with decoyDefinition do
            begin
              Decoy_Index                 := decoy_id;
              Decoy_Identifier            := FieldByName('Decoy_Identifier').AsString;
              Acoustic_Intensity_Increase := FieldByName('Acoustic_Intensity_Increase').AsSingle;
            end;

            result := decoyDefinition;
          end;
        finally
          sp.Close;
          FreeAndNil(sp);
        end;
      end;
  end;
end;

function TDMLite.GetAcousticDecoyOnBoard(const veh_id: integer;
  dict: TDBAcousticDecoyOnBoardDict): TDBAcousticDecoyOnBoardList;
var
  sp               : TZStoredProc;
  decoyOnBoard     : TDBAcoustic_Decoy_On_Board;
  decoyOnBoardList : TDBAcousticDecoyOnBoardList;
  newData          : Boolean;
begin
  result := nil;
  newData := False;

  { search in dict first }
  if not dict.ContainsKey(veh_id) then
    newData := True
  else
    result := dict.Items[veh_id];

  { new data add to dict }
  if newData then
  begin
    sp := createStoredProcedure('get_decoy_instances');
    if Assigned(sp) then
      with sp do
      begin
        Params.ParamByName('@vehicle').AsInteger := veh_id;

        try
          Open;

          if not IsEmpty then
          begin
            First;

            decoyOnBoardList := TDBAcousticDecoyOnBoardList.Create;
            dict.Add(veh_id,decoyOnBoardList);

            while not EOF do
            begin
              decoyOnBoard := TDBAcoustic_Decoy_On_Board.Create;

              with decoyOnBoard do
              begin
                Acoustic_Instance_Index := FieldByName('Acoustic_Instance_Index').AsInteger;
                Instance_Identifier     := FieldByName('Instance_Identifier').AsString;
                Quantity                := FieldByName('Quantity').AsInteger;
                Decoy_Index             := FieldByName('Decoy_Index').AsInteger;
              end;

              decoyOnBoardList.Add(decoyOnBoard);

              Next;
            end;

            result := decoyOnBoardList;
          end;
        finally
          sp.Close;
          FreeAndNil(sp);
        end;
      end;
  end;
end;

function TDMLite.GetAirBubbleDefinition(const bubble_id: integer;
  dict: TDBAirBubbleDict): TDBAir_Bubble_Definition;
var
  sp : TZStoredProc;
  bubbleDefinition : TDBAir_Bubble_Definition;
  newData : Boolean;
begin
  result := nil;
  newData := False;

  { search in dict first }
  if not dict.ContainsKey(bubble_id) then
    newData := True
  else
    result := dict.Items[bubble_id];

  { new data add to dict }
  if newData then
  begin
    sp := createStoredProcedure('get_bubble_info');
    if Assigned(sp) then
      with sp do
      begin
        Params.ParamByName('@index').AsInteger := bubble_id;

        try
          Open;

          if not IsEmpty then
          begin
            First;

            bubbleDefinition := TDBAir_Bubble_Definition.Create;
            dict.Add(bubble_id,bubbleDefinition);

            with bubbleDefinition do
            begin
              Air_Bubble_Index      := bubble_id;
              Air_Bubble_Identifier := FieldByName('Air_Bubble_Identifier').AsString;
              Platform_Domain       := FieldByName('Platform_Domain').AsInteger;
              Platform_Category     := FieldByName('Platform_Category').AsInteger;
              Platform_Type         := FieldByName('Platform_Type').AsInteger;
              Max_Acoustic_Cross    := FieldByName('Max_Acoustic_Cross').AsSingle;
              Dissipation_Time      := FieldByName('Dissipation_Time').AsSingle;
              Ascent_Rate           := FieldByName('Ascent_Rate').AsSingle;
              Descent_Rate          := FieldByName('Descent_Rate').AsSingle;
            end;

            result := bubbleDefinition;
          end;
        finally
          sp.Close;
          FreeAndNil(sp);
        end;
      end;
  end;
end;

function TDMLite.GetAirBubbleOnBoard(const veh_id: integer;
  dict: TDBAirBubbleOnBoardDict): TDBAirBubbleOnBoardList;
var
  sp               : TZStoredProc;
  decoyOnBoard     : TDBAir_Bubble_Mount;
  decoyOnBoardList : TDBAirBubbleOnBoardList;
  newData          : Boolean;
begin
  result := nil;
  newData := False;

  { search in dict first }
  if not dict.ContainsKey(veh_id) then
    newData := True
  else
    result := dict.Items[veh_id];

  { new data add to dict }
  if newData then
  begin
    sp := createStoredProcedure('get_bubble_instances');
    if Assigned(sp) then
      with sp do
      begin
        Params.ParamByName('@vehicle').AsInteger := veh_id;

        try
          Open;

          if not IsEmpty then
          begin
            First;

            decoyOnBoardList := TDBAirBubbleOnBoardList.Create;
            dict.Add(veh_id,decoyOnBoardList);

            while not EOF do
            begin
              decoyOnBoard := TDBAir_Bubble_Mount.Create;

              with decoyOnBoard do
              begin
                Air_Bubble_Instance_Index := FieldByName('Air_Bubble_Instance_Index').AsInteger;
                Instance_Identifier       := FieldByName('Instance_Identifier').AsString;
                Instance_Type             := FieldByName('Instance_Type').AsInteger;
                Bubble_Qty_On_Board       := FieldByName('Bubble_Qty_On_Board').AsInteger;
                Vehicle_Index             := veh_id;
                Air_Bubble_Index          := FieldByName('Air_Bubble_Index').AsInteger;
              end;

              decoyOnBoardList.Add(decoyOnBoard);

              Next;
            end;

            result := decoyOnBoardList;
          end;
        finally
          sp.Close;
          FreeAndNil(sp);
        end;
      end;
  end;
end;

function TDMLite.getAllReference_Point(const ra_id: integer;
  var aRec: TDBReferencePointList): Integer;
var
  sp : TZStoredProc;
  pt : TDBReference_Point;
begin
  if not ZConn.Connected then
    exit;

  sp := createStoredProcedure('get_ref_point_info');

  if Assigned(sp) then
    with sp do
    begin
      Params.ParamByName('@alloc').AsInteger := ra_id;

      try
        Open;

        result := RecordCount;
        while not Eof do
        begin
          pt := TDBReference_Point.Create;

          with pt do begin
            Reference_Index         := FieldByName('Reference_Index').AsInteger;
            Resource_Alloc_Index    := ra_id;
            Reference_Identifier    := FieldByName('Reference_Identifier').AsString;
            Force_Designation       := FieldByName('Force_Designation').AsInteger;
            Track_Type              := FieldByName('Track_Type').AsInteger;
            Symbol_Type             := FieldByName('Symbol_Type').AsInteger;
            Course                  := FieldByName('Course').AsSingle;
            Speed                   := FieldByName('Speed').AsSingle;
            X_Position              := FieldByName('X_Position').AsSingle;
            Y_Position              := FieldByName('Y_Position').AsSingle;
            Latitude                := FieldByName('Latitude').AsFloat;
            Longitude               := FieldByName('Longitude').AsFloat;
            Track_Bearing           := FieldByName('Track_Bearing').AsSingle;
            AOP_Start_Time_Offset   := FieldByName('AOP_Start_Time_Offset').AsInteger;
          end;

          aRec.Add(pt);
          Next;
        end;
      finally
        sp.Close;
        sp.Free;
      end;
    end;
end;

function TDMLite.GetAssetDeployment(const id: Integer;
  var asset: TDBAsset_Deployment_Definition): boolean;
var
  sp : TZStoredProc;
begin
  result := false;
  if not ZConn.Connected then
    exit;

  sp := createStoredProcedure('get_asset_info');

  if Assigned(sp) then
    with sp do
    begin
      Params.ParamByName('@scenario').AsInteger := id;

      try
        Open;

        result := RecordCount > 0;
        if not IsEmpty then
        begin
          First;

          with asset do
          begin
            Deployment_Index := FieldByName('Deployment_Index').AsInteger;
            Deployment_Identifier := FieldByName('Deployment_Identifier').AsString;
            Scenario_Index := id;
          end;
        end;
      finally
        sp.Close;
        sp.Free;
      end;
      result := True;
    end;
end;

function TDMLite.GetBlindZones(const asset_id,asset_type: integer;
  dict: TDBBlindZoneDict): TDBBlindZoneDefinitionList;
var
  sp         : TZStoredProc;
  pod        : TDBBlind_Zone_Definition;
  pods       : TDBBlindZoneDefinitionList;
  newData    : Boolean;
begin
  result := nil;
  newData := False;

  { search in dict first }
  if not dict.ContainsKey(asset_id) then
    newData := True
  else
    result := dict.Items[asset_id];

  { new data add to dict }
  if newData then
  begin
    sp := createStoredProcedure('get_blind_zone_info');
    if Assigned(sp) then
      with sp do
      begin
        Params.ParamByName('@plat_asset').AsInteger := asset_id;
        Params.ParamByName('@type').AsInteger       := asset_type;

        try
          Open;

          if not IsEmpty then
          begin
            First;

            pods := TDBBlindZoneDefinitionList.Create;
            dict.Add(asset_id,pods);

            while not EOF do
            begin
              pod := TDBBlind_Zone_Definition.Create;

              with pod do
              begin
                Blind_Zone_Index    := FieldByName('Blind_Zone_Index').AsInteger;
                Blind_Zone_Type     := asset_type;
                BlindZone_Number    := FieldByName('BlindZone_Number').AsInteger;
                Instance_Index      := asset_id;
                Start_Angle         := FieldByName('Start_Angle').AsSingle;
                End_Angle           := FieldByName('End_Angle').AsSingle;
              end;

              pods.Add(pod);

              Next;
            end;

            result := pods;
          end;
        finally
          sp.Close;
          FreeAndNil(sp);
        end;
      end;
  end;
end;

function TDMLite.GetBombDefinition(const bomb_id: integer;
  dict: TDBBombDefinitionDict): TDBBomb_Definition;
var
  sp : TZStoredProc;
  bombDefinition : TDBBomb_Definition;
  newData : Boolean;
begin
  result := nil;
  newData := False;

  { search in dict first }
  if not dict.ContainsKey(bomb_id) then
    newData := True
  else
    result := dict.Items[bomb_id];

  { new data add to dict }
  if newData then
  begin
    sp := createStoredProcedure('get_bomb_info');
    if Assigned(sp) then
      with sp do
      begin
        Params.ParamByName('@index').AsInteger := bomb_id;

        try
          Open;

          if not IsEmpty then
          begin
            First;

            bombDefinition := TDBBomb_Definition.Create;
            dict.Add(bomb_id,bombDefinition);

            with bombDefinition do
            begin
              Bomb_Index              := bomb_id;
              Bomb_Identifier         := FieldByName('Bomb_Identifier').AsString;
              Bomb_Type               := FieldByName('Bomb_Type').AsInteger;
              Lethality               := FieldByName('Lethality').AsInteger;
              Min_Range               := FieldByName('Min_Range').AsSingle;
              Max_Range               := FieldByName('Max_Range').AsSingle;
              Anti_Sur_Capable        := FieldByName('Anti_Sur_Capable').AsInteger;
              Anti_SubSur_Capable     := FieldByName('Anti_SubSur_Capable').AsInteger;
              Anti_Land_Capable       := FieldByName('Anti_Land_Capable').AsInteger;
              Anti_Amphibious_Capable := FieldByName('Anti_Amphibious_Capable').AsInteger;
            end;

            result := bombDefinition;
          end;
        finally
          sp.Close;
          FreeAndNil(sp);
        end;
      end;
  end;
end;

function TDMLite.GetChaffDefinition(const chaff_id: integer;
  dict: TDBChaffDefinitionDict): TDBChaff_Definition;
var
  sp : TZStoredProc;
  chaffDefinition : TDBChaff_Definition;
  newData : Boolean;
begin
  result := nil;
  newData := False;

  { search in dict first }
  if not dict.ContainsKey(chaff_id) then
    newData := True
  else
    result := dict.Items[chaff_id];

  { new data add to dict }
  if newData then
  begin
    sp := createStoredProcedure('get_chaff_info');
    if Assigned(sp) then
      with sp do
      begin
        Params.ParamByName('@index').AsInteger := chaff_id;

        try
          Open;

          if not IsEmpty then
          begin
            First;

            chaffDefinition := TDBChaff_Definition.Create;
            dict.Add(chaff_id,chaffDefinition);

            with chaffDefinition do
            begin
              Chaff_Index               := chaff_id;
              Chaff_Identifier          := FieldByName('Chaff_Identifier').AsString;
              Platform_Domain           := FieldByName('Platform_Domain').AsInteger;
              Platform_Category         := FieldByName('Platform_Category').AsInteger;
              Platform_Type             := FieldByName('Platform_Type').AsInteger;
              Max_Radar_Cross           := FieldByName('Max_Radar_Cross').AsSingle;
              Bloom_Time                := FieldByName('Bloom_Time').AsInteger;
              Max_Dissipation_Time      := FieldByName('Max_Dissipation_Time').AsInteger;
              Min_Dissipation_Time      := FieldByName('Min_Dissipation_Time').AsInteger;
              Descent_Rate              := FieldByName('Descent_Rate').AsSingle;
              Max_Radius                := FieldByName('Max_Radius').AsSingle;
              Max_Radar_Attenuation     := FieldByName('Max_Radar_Attenuation').AsSingle;
              Radar_Affect_Lower_Freq   := FieldByName('Radar_Affect_Lower_Freq').AsSingle;
              Radar_Affect_Upper_Freq   := FieldByName('Radar_Affect_Upper_Freq').AsSingle;
            end;

            result := chaffDefinition;
          end;
        finally
          sp.Close;
          FreeAndNil(sp);
        end;
      end;
  end;
end;

function TDMLite.GetChaffLauncherOnBoard(const veh_id: integer;
  dict: TDBChaffLauncherOnBoardDict): TDBChaffLauncherOnBoardList;
var
  sp               : TZStoredProc;
  decoyOnBoard     : TDBChaff_Launcher_On_Board;
  decoyOnBoardList : TDBChaffLauncherOnBoardList;
  newData          : Boolean;
begin
  result := nil;
  newData := False;

  { search in dict first }
  if not dict.ContainsKey(veh_id) then
    newData := True
  else
    result := dict.Items[veh_id];

  { new data add to dict }
  if newData then
  begin
    sp := createStoredProcedure('get_chaff_launcher_instances');
    if Assigned(sp) then
      with sp do
      begin
        Params.ParamByName('@veh_index').AsInteger := veh_id;

        try
          Open;

          if not IsEmpty then
          begin
            First;

            decoyOnBoardList := TDBChaffLauncherOnBoardList.Create;
            dict.Add(veh_id,decoyOnBoardList);

            while not EOF do
            begin
              decoyOnBoard := TDBChaff_Launcher_On_Board.Create;

              with decoyOnBoard do
              begin
                Launcher_Number    := FieldByName('Launcher_Number').AsInteger;
                Launcher_Angle     := FieldByName('Launcher_Angle').AsSingle;
                Launcher_Kind      := FieldByName('Launcher_Kind').AsInteger;
                Def_Bloom_Range    := FieldByName('Def_Bloom_Range').AsSingle;
                Def_Bloom_Altitude := FieldByName('Def_Bloom_Altitude').AsSingle;
                Max_Range          := FieldByName('Max_Range').AsSingle;
                Min_Range          := FieldByName('Min_Range').AsSingle;
                Max_Elevation      := FieldByName('Max_Elevation').AsSingle;
                Min_Elevation      := FieldByName('Min_Elevation').AsSingle;
                Average_Launch_Spd := FieldByName('Average_Launch_Spd').AsSingle;
              end;

              decoyOnBoardList.Add(decoyOnBoard);

              Next;
            end;

            result := decoyOnBoardList;
          end;
        finally
          sp.Close;
          FreeAndNil(sp);
        end;
      end;
  end;
end;

function TDMLite.GetChaffOnBoard(const veh_id: integer;
  dict: TDBChaffOnBoardDict): TDBChaffOnBoardList;
var
  sp               : TZStoredProc;
  decoyOnBoard     : TDBChaff_On_Board;
  decoyOnBoardList : TDBChaffOnBoardList;
  newData          : Boolean;
begin
  result := nil;
  newData := False;

  { search in dict first }
  if not dict.ContainsKey(veh_id) then
    newData := True
  else
    result := dict.Items[veh_id];

  { new data add to dict }
  if newData then
  begin
    sp := createStoredProcedure('get_chaff_instances');
    if Assigned(sp) then
      with sp do
      begin
        Params.ParamByName('@vehicle').AsInteger := veh_id;

        try
          Open;

          if not IsEmpty then
          begin
            First;

            decoyOnBoardList := TDBChaffOnBoardList.Create;
            dict.Add(veh_id,decoyOnBoardList);

            while not EOF do
            begin
              decoyOnBoard := TDBChaff_On_Board.Create;

              with decoyOnBoard do
              begin
                Chaff_Instance_Index:= FieldByName('Chaff_Instance_Index').AsInteger;
                Instance_Identifier := FieldByName('Instance_Identifier').AsString;
                Instance_Type       := FieldByName('Instance_Type').AsInteger;
                Chaff_Qty_On_Board  := FieldByName('Chaff_Qty_On_Board').AsInteger;
                Chaff_Index         := FieldByName('Chaff_Index').AsInteger;
                Vehicle_Index       := veh_id;
              end;

              decoyOnBoardList.Add(decoyOnBoard);

              Next;
            end;

            result := decoyOnBoardList;
          end;
        finally
          sp.Close;
          FreeAndNil(sp);
        end;
      end;
  end;
end;

function TDMLite.GetCubicleGroupList(const deploy_id: Integer;
  list: TDBCubicleGroupList): Boolean;
var
  sp      : TZStoredProc;
  grp     : TDBCubicle_Group;
begin
  result := false;

  if not ZConn.Connected then
    exit;

  sp := createStoredProcedure('get_gl_group_info');
  if Assigned(sp) then
    with sp do
    begin
      Params.ParamByName('@deploy').AsInteger := deploy_id;

      try
        Open;

        result := RecordCount > 0;
        if not IsEmpty then
        begin

          First;

          while not EOF do
          begin
            grp := TDBCubicle_Group.Create;

            with grp do
            begin
              Group_Index       := FieldByName('Group_Index').AsInteger;
              Deployment_Index  := deploy_id;
              Group_Identifier  := FieldByName('Group_Identifier').AsString;
              Force_Designation := FieldByName('Force_Designation').AsInteger;
              Tracks_Block      := FieldByName('Tracks_Block').AsInteger;
              Track_Block_Start := FieldByName('Track_Block_Start').AsInteger;
              Track_Block_End   := FieldByName('Track_Block_End').AsInteger;
              Zulu_Zulu         := FieldByName('Zulu_Zulu').AsInteger;
            end;

            list.Add(grp);
            Next;
          end;
        end;
      finally
        sp.Close;
        FreeAndNil(sp);
      end;
      Result := True;
    end;
end;

function TDMLite.GetCubicleGroupMembers(const grpID: Integer;
  dict: TDBCubicleAssignmentDict): TDBCubicleGroupMemberList;
var
  sp            : TZStoredProc;
  member        : TDBCubicle_Group_Assignment;
  members       : TDBCubicleGroupMemberList;
  newData       : Boolean;
begin
  result := nil;
  newData := False;

  { search in dict first }
  if not dict.ContainsKey(grpID) then
    newData := True
  else
    result := dict.Items[grpID];

  { new data add to dict }
  if newData then
  begin
    sp := createStoredProcedure('get_gl_member_instance_command');
    if Assigned(sp) then
      with sp do
      begin
        Params.ParamByName('@group').AsInteger := grpID;

        try
          Open;

          if not IsEmpty then
          begin
            First;

            members := TDBCubicleGroupMemberList.Create;
            dict.Add(grpID,members);

            while not EOF do
            begin
              member := TDBCubicle_Group_Assignment.Create;

              with member do
              begin
                Platform_Instance_Index  := FieldByName('Platform_Instance_Index').AsInteger;
                Group_Index              := grpID;
                Command_Priority         := FieldByName('Command_Priority').AsInteger;
                Deployment_Index         := FieldByName('Deployment_Index').AsInteger;
              end;

              members.Add(member);

              Next;
            end;

            result := members;
          end;
        finally
          sp.Close;
          FreeAndNil(sp);
        end;
      end;
  end;
end;

function TDMLite.GetDefJammerDefinition(const jammer_id: integer;
  dict: TDBDefensiveJammerDefinitionDict): TDBDefensive_Jammer_Definition;
var
  sp : TZStoredProc;
  jammerDefinition : TDBDefensive_Jammer_Definition;
  newData : Boolean;
begin
  result := nil;
  newData := False;

  { search in dict first }
  if not dict.ContainsKey(jammer_id) then
    newData := True
  else
    result := dict.Items[jammer_id];

  { new data add to dict }
  if newData then
  begin
    sp := createStoredProcedure('get_def_jam_info');
    if Assigned(sp) then
      with sp do
      begin
        Params.ParamByName('@index').AsInteger := jammer_id;

        try
          Open;

          if not IsEmpty then
          begin
            First;

            jammerDefinition := TDBDefensive_Jammer_Definition.Create;
            dict.Add(jammer_id,jammerDefinition);

            with jammerDefinition do
            begin
              Defensive_Jammer_Index        := jammer_id;
              Defensive_Jammer_Identifier   := FieldByName('Defensive_Jammer_Identifier').AsString;
              Jammer_TARH_Capable           := FieldByName('Jammer_TARH_Capable').AsInteger;
              Jammer_SARH_Capable           := FieldByName('Jammer_SARH_Capable').AsInteger;
              Type_A_Seducing_Prob          := FieldByName('Type_A_Seducing_Prob').AsSingle;
              Type_B_Seducing_Prob          := FieldByName('Type_B_Seducing_Prob').AsSingle;
              Type_C_Seducing_Prob          := FieldByName('Type_C_Seducing_Prob').AsSingle;
              ECM_Type                      := FieldByName('ECM_Type').AsInteger;
            end;

            result := jammerDefinition;
          end;
        finally
          sp.Close;
          FreeAndNil(sp);
        end;
      end;
  end;
end;

function TDMLite.GetDefJammerOnBoard(const veh_id: integer;
  dict: TDBDefensiveJammerOnBoardDict): TDBDefensiveJammerOnBoardList;
var
  sp               : TZStoredProc;
  devOnBoard       : TDBDefensive_Jammer_On_Board;
  devOnBoardList   : TDBDefensiveJammerOnBoardList;
  newData          : Boolean;
begin
  result := nil;
  newData := False;

  { search in dict first }
  if not dict.ContainsKey(veh_id) then
    newData := True
  else
    result := dict.Items[veh_id];

  { new data add to dict }
  if newData then
  begin
    sp := createStoredProcedure('get_def_jam_instances');
    if Assigned(sp) then
      with sp do
      begin
        Params.ParamByName('@vehicle').AsInteger := veh_id;

        try
          Open;

          if not IsEmpty then
          begin
            First;

            devOnBoardList := TDBDefensiveJammerOnBoardList.Create;
            dict.Add(veh_id,devOnBoardList);

            while not EOF do
            begin
              devOnBoard := TDBDefensive_Jammer_On_Board.Create;

              with devOnBoard do
              begin
                Defensive_Jammer_Instance_Index := FieldByName('Defensive_Jammer_Instance_Id').AsInteger;
                Instance_Identifier             := FieldByName('Instance_Identifier').AsString;
                Instance_Type                   := FieldByName('Instance_Type').AsInteger;
                Vehicle_Index                   := veh_id;
                Defensive_Jammer_Index          := FieldByName('Defensive_Jammer_Index').AsInteger;
              end;

              devOnBoardList.Add(devOnBoard);

              Next;
            end;

            result := devOnBoardList;
          end;
        finally
          sp.Close;
          FreeAndNil(sp);
        end;
      end;
  end;
end;

function TDMLite.GetEOOnBoard(const veh_id: integer;
  dict: TDBEOOnBoardDict): TDBEOOnBoardList;
var
  sp : TZStoredProc;
  eoOnBoard : TDBEO_On_Board;
  eoOnBoardList : TDBEOOnBoardList;
  newData : Boolean;
begin
  result := nil;
  newData := False;

  { search in dict first }
  if not dict.ContainsKey(veh_id) then
    newData := True
  else
    result := dict.Items[veh_id];

  { new data add to dict }
  if newData then
  begin
    sp := createStoredProcedure('get_eo_instances');
    if Assigned(sp) then
      with sp do
      begin
        Params.ParamByName('@vehicle').AsInteger := veh_id;

        try
          Open;

          if not IsEmpty then
          begin
            First;

            eoOnBoardList := TDBEOOnBoardList.Create;
            dict.Add(veh_id,eoOnBoardList);

            while not EOF do
            begin
              eoOnBoard := TDBEO_On_Board.Create;

              with eoOnBoard do
              begin
                EO_Instance_Index   := FieldByName('EO_Instance_Index').AsInteger;
                Instance_Identifier := FieldByName('Instance_Identifier').AsString;
                Instance_Type       := FieldByName('Instance_Type').AsInteger;
                Vehicle_Index       := veh_id;
                EO_Index            := FieldByName('EO_Index').AsInteger;
                Antenna_Height      := FieldByName('Antenna_Height').AsSingle;
              end;

              eoOnBoardList.Add(eoOnBoard);

              Next;
            end;

            result := eoOnBoardList;
          end;
        finally
          sp.Close;
          FreeAndNil(sp);
        end;
      end;
  end;
end;

function TDMLite.GetEO_Definition(const eo_id: integer;
  dict: TDBEODetectionDefinitionDict): TDBEO_Detection_Definition;
var
  sp : TZStoredProc;
  eoDefinition : TDBEO_Detection_Definition;
  newData : Boolean;
begin
  result := nil;
  newData := False;

  { search in dict first }
  if not dict.ContainsKey(eo_id) then
    newData := True
  else
    result := dict.Items[eo_id];

  { new data add to dict }
  if newData then
  begin
    sp := createStoredProcedure('get_eo_info');
    if Assigned(sp) then
      with sp do
      begin
        Params.ParamByName('@index').AsInteger := eo_id;

        try
          Open;

          if not IsEmpty then
          begin
            First;

            eoDefinition := TDBEO_Detection_Definition.Create;
            dict.Add(eo_id,eoDefinition);

            with eoDefinition do
            begin
              EO_Index := eo_id;
              Class_Identifier    := FieldByName('Class_Identifier').AsString;
              Sensor_Type         := FieldByName('Sensor_Type').AsInteger;
              Detection_Range     := FieldByName('Detection_Range').AsSingle;
              Known_Cross_Section := FieldByName('Known_Cross_Section').AsSingle;
              Max_Range           := FieldByName('Max_Range').AsSingle;
              Scan_Rate           := FieldByName('Scan_Rate').AsSingle;
              Num_FC_Channels     := FieldByName('Num_FC_Channels').AsInteger;
            end;

            result := eoDefinition;
          end;
        finally
          sp.Close;
          FreeAndNil(sp);
        end;
      end;
  end;
end;

function TDMLite.GetESMOnBoard(const veh_id: integer;
  dict: TDBESMOnBoardDict): TDBESMOnBoardList;
var
  sp : TZStoredProc;
  esmOnBoard : TDBESM_On_Board;
  esmOnBoardList : TDBESMOnBoardList;
  newData : Boolean;
begin
  result := nil;
  newData := False;

  { search in dict first }
  if not dict.ContainsKey(veh_id) then
    newData := True
  else
    result := dict.Items[veh_id];

  { new data add to dict }
  if newData then
  begin
    sp := createStoredProcedure('get_esm_instances');
    if Assigned(sp) then
      with sp do
      begin
        Params.ParamByName('@vehicle').AsInteger := veh_id;

        try
          Open;

          if not IsEmpty then
          begin
            First;

            esmOnBoardList := TDBESMOnBoardList.Create;
            dict.Add(veh_id,esmOnBoardList);

            while not EOF do
            begin
              esmOnBoard := TDBESM_On_Board.Create;

              with esmOnBoard do
              begin
                ESM_Instance_Index       := FieldByName('ESM_Instance_Index').AsInteger;
                Instance_Identifier      := FieldByName('Instance_Identifier').AsString;
                Instance_Type            := FieldByName('Instance_Type').AsInteger;
                Vehicle_Index            := veh_id;
                ESM_Index                := FieldByName('ESM_Index').AsInteger;
                Rel_Antenna_Height       := FieldByName('Rel_Antenna_Height').AsSingle;
                Max_Operational_Depth    := FieldByName('Max_Operational_Depth').AsSingle;
                Submerged_Antenna_Height := FieldByName('Submerged_Antenna_Height').AsSingle;
              end;

              esmOnBoardList.Add(esmOnBoard);

              Next;
            end;

            result := esmOnBoardList;
          end;
        finally
          sp.Close;
          FreeAndNil(sp);
        end;
      end;
  end;
end;

function TDMLite.GetESM_Definition(const esm_id: integer;
  dict: TDBESMDefinitionDict): TDBESM_Definition;
var
  sp : TZStoredProc;
  esmDefinition : TDBESM_Definition;
  newData : Boolean;
begin
  result := nil;
  newData := False;

  { search in dict first }
  if not dict.ContainsKey(esm_id) then
    newData := True
  else
    result := dict.Items[esm_id];

  { new data add to dict }
  if newData then
  begin
    sp := createStoredProcedure('get_esm_info');
    if Assigned(sp) then
      with sp do
      begin
        Params.ParamByName('@index').AsInteger := esm_id;

        try
          Open;

          if not IsEmpty then
          begin
            First;

            esmDefinition := TDBESM_Definition.Create;
            dict.Add(esm_id,esmDefinition);

            with esmDefinition do
            begin
              ESM_Index                      := esm_id;
              Class_Identifier               := FieldByName('Class_Identifier').AsString;
              Low_Detect_Frequency1          := FieldByName('Low_Detect_Frequency1').AsFloat;
              High_Detect_Frequency1         := FieldByName('High_Detect_Frequency1').AsFloat;
              Low_Detect_Frequency2          := FieldByName('Low_Detect_Frequency2').AsFloat;
              High_Detect_Frequency2         := FieldByName('High_Detect_Frequency2').AsFloat;
              ESM_Classification             := FieldByName('ESM_Classification').AsInteger;
              Emitter_Detect_Range_Factor    := FieldByName('Emitter_Detect_Range_Factor').AsSingle;
              Comm_Intercept_Capable         := FieldByName('Comm_Intercept_Capable').AsInteger;
              Frequency_Identify_Range       := FieldByName('Frequency_Identify_Range').AsFloat;
              PRF_Identify_Range             := FieldByName('PRF_Identify_Range').AsSingle;
              Pulsewidth_Identify_Range      := FieldByName('Pulsewidth_Identify_Range').AsSingle;
              Scan_Period_Identify_Range     := FieldByName('Scan_Period_Identify_Range').AsSingle;
              Sector_Blank_Detection_Factor  := FieldByName('Sector_Blank_Detection_Factor').AsSingle;
              Identification_Period          := FieldByName('Identification_Period').AsSingle;
              Classification_Period          := FieldByName('Classification_Period').AsSingle;
              Minimum_Bearing_Error_Variance := FieldByName('Minimum_Bearing_Error_Variance').AsSingle;
              Initial_Bearing_Error_Variance := FieldByName('Initial_Bearing_Error_Variance').AsSingle;
            end;

            result := esmDefinition;
          end;
        finally
          sp.Close;
          FreeAndNil(sp);
        end;
      end;
  end;
end;

function TDMLite.GetExternalCommunicationChannel(const alloc_id,
  group_id: Integer; dict: TDBExtCommChannelGroupDict): TDBExtCommChannelGroupList;
var
  sp       : TZStoredProc;
  comm     : TDBExternal_Communication_Channel_Group;
  commList : TDBExtCommChannelGroupList;
  newData  : Boolean;
begin
  result := nil;
  newData := False;

  { search in dict first }
  if not dict.ContainsKey(group_id) then
    newData := True
  else
    result := dict.Items[group_id];

  { new data add to dict }
  if newData then
  begin
    sp := createStoredProcedure('get_gl_comm_channel_instances');
    if Assigned(sp) then
      with sp do
      begin
        Params.ParamByName('@alloc').AsInteger := alloc_id;
        Params.ParamByName('@group').AsInteger := group_id;

        try
          Open;

          if not IsEmpty then
          begin

            First;

            commList := TDBExtCommChannelGroupList.Create;
            dict.Add(group_id,commList);

            while not EOF do
            begin
              comm := TDBExternal_Communication_Channel_Group.Create;

              with comm do
              begin
                Channel_Slot         := FieldByName('Channel_Slot').AsInteger;
                Comms_Channel_Index  := FieldByName('Comms_Channel_Index').AsInteger;
                Resource_Alloc_Index := alloc_id;
                Channel_Number       := FieldByName('Channel_Number').AsInteger;
                Channel_Identifier   := FieldByName('Channel_Identifier').AsString;
                Comms_Band           := FieldByName('Comms_Band').AsInteger;
                Channel_Freq         := FieldByName('Channel_Freq').AsFloat;
                Channel_Security     := FieldByName('Channel_Security').AsInteger;
                Channel_Code         := FieldByName('Channel_Code').AsString;
              end;

              commList.Add(comm);

              Next;
            end;
          end;
        finally
          sp.Close;
          FreeAndNil(sp);
        end;
        Result := commList;
      end;
  end;
end;

//function TDMLite.GetExternalCommunicationChannel(
//  const alloc_id: Integer; recList: TDBExtCommChannelList): boolean;
//var
//  sp    : TZStoredProc;
//  comm  : TDBExternal_Communication_Channel;
//begin
//  result := false;
//
//  if not ZConn.Connected then
//    exit;
//
//  sp := createStoredProcedure('get_gl_comm_channel_info');
//  if Assigned(sp) then
//    with sp do
//    begin
//      Params.ParamByName('@alloc').AsInteger := alloc_id;
//
//      try
//        Open;
//
//        result := RecordCount > 0;
//        if not IsEmpty then
//        begin
//
//          First;
//
//          while not EOF do
//          begin
//            comm := TDBExternal_Communication_Channel.Create;
//
//            with comm do
//            begin
//              Comms_Channel_Index  := FieldByName('Comms_Channel_Index').AsInteger;
//              Resource_Alloc_Index := alloc_id;
//              Channel_Number       := FieldByName('Channel_Number').AsInteger;
//              Channel_Identifier   := FieldByName('Channel_Identifier').AsString;
//              Comms_Band           := FieldByName('Comms_Band').AsInteger;
//              Channel_Freq         := FieldByName('Channel_Freq').AsFloat;
//              Channel_Security     := FieldByName('Channel_Security').AsInteger;
//              Channel_Code         := FieldByName('Channel_Code').AsString;
//            end;
//
//            recList.Add(comm);
//            Next;
//          end;
//        end;
//      finally
//        sp.Close;
//        FreeAndNil(sp);
//      end;
//      Result := True;
//    end;
//
//end;

function TDMLite.GetFCRDefinition(const fcr_id: integer;
  dict: TDBFCRDefinitionDict): TDBFCR_Definition;
var
  sp : TZStoredProc;
  fcrDefinition : TDBFCR_Definition;
  newData : Boolean;
begin
  result := nil;
  newData := False;

  { search in dict first }
  if not dict.ContainsKey(fcr_id) then
    newData := True
  else
    result := dict.Items[fcr_id];

  { new data add to dict }
  if newData then
  begin
    sp := createStoredProcedure('get_fcr_info');
    if Assigned(sp) then
      with sp do
      begin
        Params.ParamByName('@index').AsInteger := fcr_id;

        try
          Open;

          if not IsEmpty then
          begin
            First;

            fcrDefinition := TDBFCR_Definition.Create;
            dict.Add(fcr_id,fcrDefinition);

            with fcrDefinition do
            begin
              FCR_Index                   := fcr_id;
              Radar_Identifier            := FieldByName('Radar_Identifier').AsString;
              Radar_Emitter               := FieldByName('Radar_Emitter').AsString;
              Frequency                   := FieldByName('Frequency').AsSingle;
              Scan_Rate                   := FieldByName('Scan_Rate').AsSingle;
              Radar_Power                 := FieldByName('Radar_Power').AsSingle;
              Pulse_Rep_Freq              := FieldByName('Pulse_Rep_Freq').AsSingle;
              Pulse_Width                 := FieldByName('Pulse_Width').AsSingle;
              Max_Unambig_Detection_Range := FieldByName('Max_Unambig_Detection_Range').AsSingle;
            end;

            result := fcrDefinition;
          end;
        finally
          sp.Close;
          FreeAndNil(sp);
        end;
      end;
  end;
end;

function TDMLite.GetFCROnBoard(const veh_id: integer;
  dict: TDBFCROnBoardDict): TDBFCROnBoardList;
var
  sp : TZStoredProc;
  fcrOnBoard : TDBFCR_On_Board;
  fcrOnBoardList : TDBFCROnBoardList;
  newData : Boolean;
begin
  result := nil;
  newData := False;

  { search in dict first }
  if not dict.ContainsKey(veh_id) then
    newData := True
  else
    result := dict.Items[veh_id];

  { new data add to dict }
  if newData then
  begin
    sp := createStoredProcedure('get_fcr_instances');
    if Assigned(sp) then
      with sp do
      begin
        Params.ParamByName('@vehicle').AsInteger := veh_id;

        try
          Open;

          if not IsEmpty then
          begin
            First;

            fcrOnBoardList := TDBFCROnBoardList.Create;
            dict.Add(veh_id,fcrOnBoardList);

            while not EOF do
            begin
              fcrOnBoard := TDBFCR_On_Board.Create;

              with fcrOnBoard do
              begin
                FCR_Instance_Index  := FieldByName('FCR_Instance_Index').AsInteger;
                Instance_Identifier := FieldByName('Instance_Identifier').AsString;
                Instance_Type       := FieldByName('Instance_Type').AsInteger;
                Vehicle_Index       := veh_id;
                Radar_Index         := FieldByName('Radar_Index').AsInteger;
                Rel_Antenna_Height  := FieldByName('Rel_Antenna_Height').AsSingle;
              end;

              fcrOnBoardList.Add(fcrOnBoard);

              Next;
            end;

            result := fcrOnBoardList;
          end;
        finally
          sp.Close;
          FreeAndNil(sp);
        end;
      end;
  end;
end;

function TDMLite.GetFittedWeaponLauncherOnBoard(const fitted_wpn_id: integer;
  dict: TDBFitWeaponLauncherOnBoardDict): TDBFittedWeaponLauncherList;
var
  sp : TZStoredProc;
  fwpOnBoard : TDBFitted_Weap_Launcher_On_Board;
  fwpOnBoardList : TDBFittedWeaponLauncherList;
  newData : Boolean;
begin
  result := nil;
  newData := False;

  { search in dict first }
  if not dict.ContainsKey(fitted_wpn_id) then
    newData := True
  else
    result := dict.Items[fitted_wpn_id];

  { new data add to dict }
  if newData then
  begin
    sp := createStoredProcedure('get_weap_launcher_instances');
    if Assigned(sp) then
      with sp do
      begin
        Params.ParamByName('@weapon_instance').AsInteger := fitted_wpn_id;

        try
          Open;

          if not IsEmpty then
          begin
            First;

            fwpOnBoardList := TDBFittedWeaponLauncherList.Create;
            dict.Add(fitted_wpn_id,fwpOnBoardList);

            while not EOF do
            begin
              fwpOnBoard := TDBFitted_Weap_Launcher_On_Board.Create;

              with fwpOnBoard do
              begin
                Fitted_Weap_Index       := fitted_wpn_id;
                Launcher_Type           := FieldByName('Launcher_Type').AsInteger;
                Launcher_Angle_Required := FieldByName('Launcher_Angle_Required').AsInteger;
                Launcher_Angle          := FieldByName('Launcher_Angle').AsInteger;
                Launcher_Max_Qty        := FieldByName('Launcher_Max_Qty').AsInteger;
              end;

              fwpOnBoardList.Add(fwpOnBoard);

              Next;
            end;

            result := fwpOnBoardList;
          end;
        finally
          sp.Close;
          FreeAndNil(sp);
        end;
      end;
  end;
end;

function TDMLite.GetFittedWeaponOnBoard(const veh_id: integer;
  dict: TDBFitWeaponOnBoardDict): TDBFitWeaponOnBoardList;
var
  sp : TZStoredProc;
  fwpOnBoard : TDBFitted_Weapon_On_Board;
  fwpOnBoardList : TDBFitWeaponOnBoardList;
  newData : Boolean;
begin
  result := nil;
  newData := False;

  { search in dict first }
  if not dict.ContainsKey(veh_id) then
    newData := True
  else
    result := dict.Items[veh_id];

  { new data add to dict }
  if newData then
  begin
    sp := createStoredProcedure('get_fitted_mount_instances');
    if Assigned(sp) then
      with sp do
      begin
        Params.ParamByName('@veh_index').AsInteger := veh_id;

        try
          Open;

          if not IsEmpty then
          begin
            First;

            fwpOnBoardList := TDBFitWeaponOnBoardList.Create;
            dict.Add(veh_id,fwpOnBoardList);

            while not EOF do
            begin
              fwpOnBoard := TDBFitted_Weapon_On_Board.Create;

              with fwpOnBoard do
              begin
                Fitted_Weap_Index     := FieldByName('Fitted_Weap_Index').AsInteger;
                Instance_Identifier   := FieldByName('Instance_Identifier').AsString;
                Instance_Type         := FieldByName('Instance_Type').AsInteger;
                Vehicle_Index         := veh_id;
                Mount_Type            := FieldByName('Mount_Type').AsInteger;
                Launch_Angle          := FieldByName('Launch_Angle').AsSingle;
                Launch_Angle_Required := FieldByName('Launch_Angle_Required').AsInteger;
                Quantity              := FieldByName('Quantity').AsInteger;
                Firing_Delay          := FieldByName('Firing_Delay').AsSingle;
                Weapon_Index          := FieldByName('Weapon_Index').AsInteger;
              end;

              fwpOnBoardList.Add(fwpOnBoard);

              Next;
            end;

            result := fwpOnBoardList;
          end;
        finally
          sp.Close;
          FreeAndNil(sp);
        end;
      end;
  end;
end;

function TDMLite.GetFloatingDecoyDefinition(const decoy_id: integer;
  dict: TDBFloatingDecoyDefinitionDict): TDBFloating_Decoy_Definition;
var
  sp : TZStoredProc;
  decoyDefinition : TDBFloating_Decoy_Definition;
  newData : Boolean;
begin
  result := nil;
  newData := False;

  { search in dict first }
  if not dict.ContainsKey(decoy_id) then
    newData := True
  else
    result := dict.Items[decoy_id];

  { new data add to dict }
  if newData then
  begin
    sp := createStoredProcedure('get_float_decoy_info');
    if Assigned(sp) then
      with sp do
      begin
        Params.ParamByName('@index').AsInteger := decoy_id;

        try
          Open;

          if not IsEmpty then
          begin
            First;

            decoyDefinition := TDBFloating_Decoy_Definition.Create;
            dict.Add(decoy_id,decoyDefinition);

            with decoyDefinition do
            begin
              Floating_Decoy_Index      := decoy_id;
              Floating_Decoy_Identifier := FieldByName('Floating_Decoy_Identifier').AsString;
              Platform_Domain           := FieldByName('Platform_Domain').AsInteger;
              Platform_Category         := FieldByName('Platform_Category').AsInteger;
              Platform_Type             := FieldByName('Platform_Type').AsInteger;
              Length                    := FieldByName('Length').AsSingle;
              Width                     := FieldByName('Width').AsSingle;
              Height                    := FieldByName('Height').AsSingle;
              Front_Radar_Cross         := FieldByName('Front_Radar_Cross').AsSingle;
              Side_Radar_Cross          := FieldByName('Side_Radar_Cross').AsSingle;
              Front_Visual_Cross        := FieldByName('Front_Visual_Cross').AsSingle;
              Side_Visual_Cross         := FieldByName('Side_Visual_Cross').AsSingle;
              Front_Acoustic_Cross      := FieldByName('Front_Acoustic_Cross').AsSingle;
              Side_Acoustic_Cross       := FieldByName('Side_Acoustic_Cross').AsSingle;
              Lifetime_Duration         := FieldByName('Lifetime_Duration').AsSingle;
            end;

            result := decoyDefinition;
          end;
        finally
          sp.Close;
          FreeAndNil(sp);
        end;
      end;
  end;
end;

function TDMLite.GetFloatingDecoyOnBoard(const veh_id: integer;
  dict: TDBFloatingDecoyOnBoardDict): TDBFloatingDecoyOnBoardList;
var
  sp               : TZStoredProc;
  devOnBoard       : TDBFloating_Decoy_On_Board;
  devOnBoardList   : TDBFloatingDecoyOnBoardList;
  newData          : Boolean;
begin
  result := nil;
  newData := False;

  { search in dict first }
  if not dict.ContainsKey(veh_id) then
    newData := True
  else
    result := dict.Items[veh_id];

  { new data add to dict }
  if newData then
  begin
    sp := createStoredProcedure('get_float_decoy_instances');
    if Assigned(sp) then
      with sp do
      begin
        Params.ParamByName('@vehicle').AsInteger := veh_id;

        try
          Open;

          if not IsEmpty then
          begin
            First;

            devOnBoardList := TDBFloatingDecoyOnBoardList.Create;
            dict.Add(veh_id,devOnBoardList);

            while not EOF do
            begin
              devOnBoard := TDBFloating_Decoy_On_Board.Create;

              with devOnBoard do
              begin
                Floating_Decoy_Instance_Index := FieldByName('Floating_Decoy_Instance_Index').AsInteger;
                Instance_Identifier           := FieldByName('Instance_Identifier').AsString;
                Instance_Type                 := FieldByName('Instance_Type').AsInteger;
                Quantity                      := FieldByName('Quantity').AsInteger;
                Floating_Decoy_Index          := FieldByName('Floating_Decoy_Index').AsInteger;
              end;

              devOnBoardList.Add(devOnBoard);

              Next;
            end;

            result := devOnBoardList;
          end;
        finally
          sp.Close;
          FreeAndNil(sp);
        end;
      end;
  end;
end;

function TDMLite.GetFormationAssignments(const frm_id: Integer;
  dict: TDBFormationAssigmentDict): TDBFormationAssignmentList;
var
  sp : TZStoredProc;
  membrForm : TDBFormation_Assignment;
  membrFormList : TDBFormationAssignmentList;
  newData : Boolean;
begin
  result := nil;
  newData := False;

  { search in dict first }
  if not dict.ContainsKey(frm_id) then
    newData := True
  else
    result := dict.Items[frm_id];

  { new data add to dict }
  if newData then
  begin
    sp := createStoredProcedure('get_formation_members');
    if Assigned(sp) then
      with sp do
      begin
        Params.ParamByName('@formation').AsInteger := frm_id;

        try
          Open;

          if not IsEmpty then
          begin
            First;

            membrFormList := TDBFormationAssignmentList.Create;
            dict.Add(frm_id,membrFormList);

            while not EOF do
            begin
              membrForm := TDBFormation_Assignment.Create;

              with membrForm do
              begin
                Formation_Index         := frm_id;
                Platform_Instance_Index := FieldByName('Platform_Instance_Index').AsInteger;
                Angle_Offset            := FieldByName('Angle_Offset').AsInteger;
                Range_from_Leader       := FieldByName('Range_from_Leader').AsInteger;
                Altitude                := FieldByName('Altitude').AsInteger;
                Force_Designation       := FieldByName('Force_Designation').AsInteger;
                Instance_Name           := FieldByName('Instance_Name').AsString;
              end;

              membrFormList.Add(membrForm);

              Next;
            end;

            result := membrFormList;
          end;
        finally
          sp.Close;
          FreeAndNil(sp);
        end;
      end;
  end;
end;

function TDMLite.GetFormationList(const deploy_id: Integer;
  list: TDBFormationList):boolean;
var
  sp     : TZStoredProc;
  form   : TDBFormation_Definition;
begin
  result := false;

  if not ZConn.Connected then
    exit;

  sp := createStoredProcedure('get_gl_formation_info');
  if Assigned(sp) then
    with sp do
    begin
      Params.ParamByName('@deploy').AsInteger := deploy_id;

      try
        Open;

        result := RecordCount > 0;
        if not IsEmpty then
        begin

          First;

          while not EOF do
          begin
            form := TDBFormation_Definition.Create;

            with form do
            begin
              Formation_Index      := FieldByName('Formation_Index').AsInteger;
              Formation_Identifier := FieldByName('Formation_Identifier').AsString;
              Force_Designation    := FieldByName('Force_Designation').AsInteger;
              Formation_Leader     := FieldByName('Formation_Leader').AsInteger;
              Angle_Type           := FieldByName('Angle_Type').AsInteger;
              Deployment_Index     := deploy_id;
            end;

            list.Add(form);
            Next;
          end;
        end;
      finally
        sp.Close;
        FreeAndNil(sp);
      end;
      Result := True;
    end;
end;

function TDMLite.GetGame_Area_DefByID(const id: Integer;
  var gameArea: TDBGame_Area_Definition): Integer;
var
  sp : TZStoredProc;
begin
  result := -1;
  if not ZConn.Connected then
    exit;

  sp := createStoredProcedure('get_game_area_info');
  if Assigned(sp) then
    with sp do
    begin
      Params.ParamByName('@index').AsInteger := id;

      try
        Open;

        if not IsEmpty then
        begin
          if not Assigned(gameArea) then
            gameArea := TDBGame_Area_Definition.Create;

          with gameArea do
          begin
            Game_Area_Index         := id;
            Game_Area_Identifier    := FieldByName('Game_Area_Identifier').AsString;
            Game_Centre_Lat         := FieldByName('Game_Centre_Lat').AsFloat;
            Game_Centre_Long        := FieldByName('Game_Centre_Long').AsFloat;
            Game_X_Dimension        := FieldByName('Game_X_Dimension').AsSingle;
            Game_Y_Dimension        := FieldByName('Game_Y_Dimension').AsSingle;
            Use_Real_World          := FieldByName('Use_Real_World').AsInteger;
            Use_Artificial_Landmass := FieldByName('Use_Artificial_Landmass').AsInteger;
            Detail_Map              := FieldByName('Detail_Map').AsString;
          end;
        end;
      finally
        sp.Close;
        sp.Free;
      end;
      result := 0;
    end;
end;

function TDMLite.GetGame_Cloud_On_ESM(const id: Integer;
  var rec: TDBGameCloudOnESMList): boolean;
var
  sp : TZStoredProc;
  esm : TDBGame_Cloud_On_ESM;
begin
  result := false;

  if not ZConn.Connected then
    exit;

  sp := createStoredProcedure('get_g_cloud_esm_info');
  if Assigned(sp) then
    with sp do
    begin
      Params.ParamByName('@default').AsInteger := id;

      try
        Open;

        result := RecordCount > 0;
        if not IsEmpty then
        begin

          First;

          while not EOF do
          begin
            esm := TDBGame_Cloud_On_ESM.Create;

            with esm do
            begin
              Defaults_Index := id;
              Radar_Frequency := FieldByName('Radar_Frequency').AsSingle;
              Cloud_0_Effect := FieldByName('Cloud_0_Effect').AsSingle;
              Cloud_1_Effect := FieldByName('Cloud_1_Effect').AsSingle;
              Cloud_2_Effect := FieldByName('Cloud_2_Effect').AsSingle;
              Cloud_3_Effect := FieldByName('Cloud_3_Effect').AsSingle;
              Cloud_4_Effect := FieldByName('Cloud_4_Effect').AsSingle;
              Cloud_5_Effect := FieldByName('Cloud_5_Effect').AsSingle;
              Cloud_6_Effect := FieldByName('Cloud_6_Effect').AsSingle;
            end;

            rec.Add(esm);
            Next;
          end;
        end;
      finally
        sp.Close;
        FreeAndNil(sp);
      end;
      Result := True;
    end;
end;

function TDMLite.GetGame_Cloud_On_Radar(const id: Integer;
  var rec: TDBGameCloudRadarList): boolean;
var
  sp : TZStoredProc;
  rdr : TDBGame_Cloud_On_Radar;
begin
  result := false;

  if not ZConn.Connected then
    exit;

  sp := createStoredProcedure('get_g_cloud_radar_info');
  if Assigned(sp) then
    with sp do
    begin
      Params.ParamByName('@default').AsInteger := id;

      try
        Open;

        result := RecordCount > 0;
        if not IsEmpty then
        begin
          First;

          while not EOF do
          begin
            rdr := TDBGame_Cloud_On_Radar.Create;
            with rdr do
            begin
              Defaults_Index := id;
              Radar_Frequency := FieldByName('Radar_Frequency').AsSingle;
              Cloud_0_Effect := FieldByName('Cloud_0_Effect').AsSingle;
              Cloud_1_Effect := FieldByName('Cloud_1_Effect').AsSingle;
              Cloud_2_Effect := FieldByName('Cloud_2_Effect').AsSingle;
              Cloud_3_Effect := FieldByName('Cloud_3_Effect').AsSingle;
              Cloud_4_Effect := FieldByName('Cloud_4_Effect').AsSingle;
              Cloud_5_Effect := FieldByName('Cloud_5_Effect').AsSingle;
              Cloud_6_Effect := FieldByName('Cloud_6_Effect').AsSingle;
            end;

            rec.Add(rdr);
            Next;
          end;
        end;
      finally
        sp.Close;
        FreeAndNil(sp);
      end;
      Result := True;
    end;
end;

function TDMLite.GetGame_Defaults(const id: Integer;
  var rec: TDBGame_Defaults): boolean;
var
  sp : TZStoredProc;
begin
  result := false;

  if not ZConn.Connected then
    exit;

  sp := createStoredProcedure('get_g_defaults_alloc_info');
  if Assigned(sp) then
    with sp do
    begin
      Params.ParamByName('@alloc_index').AsInteger := id;

      try
        Open;

        result := RecordCount > 0;

        if not IsEmpty then
        begin
          First;

          if not(Assigned(rec)) then
            rec := TDBGame_Defaults.Create;

          with rec do
          begin
            Defaults_Index := FieldByName('Defaults_Index').AsInteger;
            Defaults_Identifier := FieldByName('Defaults_Identifier').AsString;
            Init_AOP := FieldByName('Init_AOP').AsInteger;
            AOP_Decrease_Rate := FieldByName('AOP_Decrease_Rate').AsSingle;
            Sono_Num_2_Initiate := FieldByName('Sono_Num_2_Initiate').AsInteger;
            Trans_Range_2_Air := FieldByName('Trans_Range_2_Air').AsSingle;
            Trans_Range_2_Sur := FieldByName('Trans_Range_2_Sur').AsSingle;
            Init_AOP_Modifier := FieldByName('Init_AOP_Modifier').AsSingle;
            Visual_Detect_Range := FieldByName('Visual_Detect_Range').AsSingle;
            Known_Cross_Section := FieldByName('Known_Cross_Section').AsSingle;
            Max_Visual_Range := FieldByName('Max_Visual_Range').AsSingle;
            EO_Detection_Factor := FieldByName('EO_Detection_Factor').AsSingle;
            Visual_Detection_Factor := FieldByName('Visual_Detection_Factor')
              .AsSingle;
            EO_Ident_Factor := FieldByName('EO_Ident_Factor').AsSingle;
            Visual_Ident_Factor := FieldByName('Visual_Ident_Factor').AsSingle;
            Sine_Period_Distance := FieldByName('Sine_Period_Distance').AsSingle;
            Sine_Period_Amplitude := FieldByName('Sine_Period_Amplitude').AsSingle;
            Short_Period_Distance := FieldByName('Short_Period_Distance').AsSingle;
            Short_Period_Amplitude := FieldByName('Short_Period_Amplitude')
              .AsSingle;
            Long_Period_Distance := FieldByName('Long_Period_Distance').AsSingle;
            Long_Period_Amplitude := FieldByName('Long_Period_Amplitude').AsSingle;
            Very_Period_Distance := FieldByName('Very_Period_Distance').AsSingle;
            Very_Period_Amplitude := FieldByName('Very_Period_Amplitude').AsSingle;
            Air_Lost_Time := FieldByName('Air_Lost_Time').AsInteger;
            Sur_Lost_Time := FieldByName('Sur_Lost_Time').AsInteger;
            Sub_Lost_Time := FieldByName('Sub_Lost_Time').AsInteger;
            ESM_Bearing_Lost_Time := FieldByName('ESM_Bearing_Lost_Time').AsInteger;
            Sonar_Bearing_Lost_Time := FieldByName('Sonar_Bearing_Lost_Time')
              .AsInteger;
            Stale_Air_Time := FieldByName('Stale_Air_Time').AsInteger;
            Stale_Sur_Time := FieldByName('Stale_Sur_Time').AsInteger;
            Stale_Sub_Time := FieldByName('Stale_Sub_Time').AsInteger;
            Stale_ESM_Bearing_Time := FieldByName('Stale_ESM_Bearing_Time')
              .AsInteger;
            Stale_Sonar_Bearing_Time := FieldByName('Stale_Sonar_Bearing_Time')
              .AsInteger;
            POD_Check_Time := FieldByName('POD_Check_Time').AsInteger;
            TMA_Range_Rate := FieldByName('TMA_Range_Rate').AsSingle;
            Frequency_Identity_Weighting := FieldByName
              ('Frequency_Identity_Weighting').AsSingle;
            PRF_Identity_Weighting := FieldByName('PRF_Identity_Weighting')
              .AsSingle;
            Pulsewidth_Identity_Weighting := FieldByName
              ('Pulsewidth_Identity_Weighting').AsSingle;
            Scan_Period_Identity_Weighting := FieldByName
              ('Scan_Period_Identity_Weighting').AsSingle;
            Crew_Eff_Heading_Error := FieldByName('Crew_Eff_Heading_Error')
              .AsSingle;
            Crew_Eff_Speed_Error := FieldByName('Crew_Eff_Speed_Error').AsSingle;
            TMA_Relative_Bearing_Rate := FieldByName('TMA_Relative_Bearing_Rate')
              .AsSingle;
            Passive_Sonar_Max_Course_Error := FieldByName
              ('Passive_Sonar_Max_Course_Error').AsSingle;
            Passive_Sonar_Max_Speed_Error := FieldByName
              ('Passive_Sonar_Max_Speed_Error').AsSingle;
            ESM_Error_Corr_Rate := FieldByName('ESM_Error_Corr_Rate').AsSingle;
            Chaff_Altitude_Threshold := FieldByName('Chaff_Altitude_Threshold')
              .AsSingle;
            MHS_Flash_Delay_Time := FieldByName('MHS_Flash_Delay_Time').AsSingle;
            MHS_Immed_Delay_Time := FieldByName('MHS_Immed_Delay_Time').AsSingle;
            MHS_Priority_Delay_Time := FieldByName('MHS_Priority_Delay_Time')
              .AsSingle;
            MHS_Routine_Delay_Time := FieldByName('MHS_Routine_Delay_Time')
              .AsSingle;
            Max_UWT_Range := FieldByName('Max_UWT_Range').AsSingle;
            Max_HF_Detect_Range := FieldByName('Max_HF_Detect_Range').AsSingle;
            Max_UHF_Detect_Range := FieldByName('Max_UHF_Detect_Range').AsSingle;
            Max_IFF_Range := FieldByName('Max_IFF_Range').AsSingle;
            Track_History_Air_Sample_Rate := FieldByName
              ('Track_History_Air_Sample_Rate').AsInteger;
            Track_History_Air_Max_Points := FieldByName
              ('Track_History_Air_Max_Points').AsInteger;
            Track_History_Sample_Rate := FieldByName('Track_History_Sample_Rate')
              .AsInteger;
            Track_History_Max_Points := FieldByName('Track_History_Max_Points')
              .AsInteger;
            Auto_Gun_Interception_Range := FieldByName
              ('Auto_Gun_Interception_Range').AsSingle;
            Auto_Gun_Threshold_Speed := FieldByName('Auto_Gun_Threshold_Speed')
              .AsSingle;
            Clutter_Reduction_Scale := FieldByName('Clutter_Reduction_Scale')
              .AsSingle;
            Jam_Break_Lock_Time_Interval := FieldByName
              ('Jam_Break_Lock_Time_Interval').AsInteger;
            Missile_Reacquisition_Time := FieldByName('Missile_Reacquisition_Time')
              .AsInteger;
            Seduction_Bloom_Altitude := FieldByName('Seduction_Bloom_Altitude')
              .AsInteger;
            Seduction_Bloom_Range := FieldByName('Seduction_Bloom_Range').AsSingle;
            HF_Datalink_MHS_Trans_Freq := FieldByName('HF_Datalink_MHS_Trans_Freq')
              .AsSingle;
            UHF_Datalink_MHS_Trans_Freq := FieldByName
              ('UHF_Datalink_MHS_Trans_Freq').AsSingle;
            Max_Num_Radar_Classes := FieldByName('Max_Num_Radar_Classes').AsInteger;
            Max_Num_Sonar_Classes := FieldByName('Max_Num_Sonar_Classes').AsInteger;
            Max_Num_Sonobuoy_Classes := FieldByName('Max_Num_Sonobuoy_Classes')
              .AsInteger;
            Max_Num_EO_Classes := FieldByName('Max_Num_EO_Classes').AsInteger;
            Max_Num_ESM_Classes := FieldByName('Max_Num_ESM_Classes').AsInteger;
            Max_Num_MAD_Classes := FieldByName('Max_Num_MAD_Classes').AsInteger;
            Max_Num_Fitted_Weap_Classes := FieldByName
              ('Max_Num_Fitted_Weap_Classes')
              .AsInteger;
            Max_Num_Point_Effect_Classes := FieldByName
              ('Max_Num_Point_Effect_Classes').AsInteger;
            HAFO_Min_Range := FieldByName('HAFO_Min_Range').AsSingle;
            HAFO_Max_Range := FieldByName('HAFO_Max_Range').AsSingle;
            Engage_Guide_Stale_Target_Time := FieldByName
              ('Engage_Guide_Stale_Target_Time').AsInteger;
            Outrun_Guide_Stale_Target_Time := FieldByName
              ('Outrun_Guide_Stale_Target_Time').AsInteger;
            Shadow_Guide_Stale_Target_Time := FieldByName
              ('Shadow_Guide_Stale_Target_Time').AsInteger;
            Sonobuoy_Air_Deceleration := FieldByName('Sonobuoy_Air_Deceleration')
              .AsSingle;
            Sonobuoy_Air_Descent_Rate := FieldByName('Sonobuoy_Air_Descent_Rate')
              .AsSingle;
            Depth_Charge_Air_Deceleration := FieldByName
              ('Depth_Charge_Air_Deceleration').AsSingle;
            Depth_Charge_Air_Descent_Rate := FieldByName
              ('Depth_Charge_Air_Descent_Rate').AsSingle;
            Missile_Sea_Check_Interval := FieldByName('Missile_Sea_Check_Interval')
              .AsInteger;
          end;
        end;
      finally
        sp.Close;
        FreeAndNil(sp);
      end;
    end;end;

function TDMLite.GetGame_Default_IFF_Mode_Code(const id: Integer;
  var rec: TDBGameDefaultIFFModeList): boolean;
var
  sp : TZStoredProc;
  iff : TDBGame_Default_IFF_Mode_Code;
begin
  result := false;

  if not ZConn.Connected then
    exit;

  sp := createStoredProcedure('get_g_def_iff_info');
  if Assigned(sp) then
    with sp do
    begin
      Params.ParamByName('@default').AsInteger := id;

      try
        Open;

        result := RecordCount > 0;
        if not IsEmpty then
        begin
          First;

          while not EOF do
          begin
            iff := TDBGame_Default_IFF_Mode_Code.Create;
            with iff do
            begin
              Defaults_Index := id;
              Force_Designation := FieldByName('Force_Designation').AsInteger;
              IFF_Device_Type := FieldByName('IFF_Device_Type').AsInteger;
              IFF_Mode := FieldByName('IFF_Mode').AsInteger;
              IFF_Code := FieldByName('IFF_Code').AsInteger;
              Mode_State := FieldByName('Mode_State').AsInteger;
            end;

            rec.Add(iff);
            Next;
          end;
        end;
      finally
        sp.Close;
        FreeAndNil(sp);
      end;
      Result := True;
    end;
end;

function TDMLite.GetGame_Environment_Definition(const id: Integer;
  var rec: TDBGame_Environment_Definition): boolean;
var
  sp : TZStoredProc;
begin
  result := false;
  if not ZConn.Connected then
    exit;

  sp := createStoredProcedure('get_game_enviro_info');
  if Assigned(sp) then
    with sp do
    begin
      Params.ParamByName('@index').AsInteger := id;

      try
        Open;

        result := RecordCount > 0;
        if not IsEmpty then
        begin
          First;

          if not Assigned(rec) then
            rec := TDBGame_Environment_Definition.Create;

          with rec do
          begin
            Game_Enviro_Index              := id;
            Game_Enviro_Identifier         := FieldByName('Game_Enviro_Identifier')
              .AsString;
            Game_Area_Index                := FieldByName('Game_Area_Index').AsInteger;
            Wind_Speed                     := FieldByName('Wind_Speed').AsSingle;
            Wind_Direction                 := FieldByName('Wind_Direction').AsSingle;
            Daytime_Visual_Modifier        := FieldByName('Daytime_Visual_Modifier')
              .AsSingle;
            Nighttime_Visual_Modifier      := FieldByName('Nighttime_Visual_Modifier')
              .AsSingle;
            Daytime_Infrared_Modifier      := FieldByName('Daytime_Infrared_Modifier')
              .AsSingle;
            Nighttime_Infrared_Modifier    := FieldByName
              ('Nighttime_Infrared_Modifier').AsSingle;
            Sunrise                        := FieldByName('Sunrise').AsInteger;
            Sunset                         := FieldByName('Sunset').AsInteger;
            Period_of_Twilight             := FieldByName('Period_of_Twilight').AsInteger;
            Rain_Rate                      := FieldByName('Rain_Rate').AsInteger;
            Cloud_Base_Height              := FieldByName('Cloud_Base_Height').AsSingle;
            Cloud_Attenuation              := FieldByName('Cloud_Attenuation').AsInteger;
            Sea_State                      := FieldByName('Sea_State').AsInteger;
            Ocean_Current_Speed            := FieldByName('Ocean_Current_Speed').AsSingle;
            Ocean_Current_Direction        := FieldByName('Ocean_Current_Direction')
              .AsSingle;
            Thermal_Layer_Depth            := FieldByName('Thermal_Layer_Depth').AsSingle;
            Sound_Velocity_Type            := FieldByName('Sound_Velocity_Type').AsInteger;
            Surface_Sound_Speed            := FieldByName('Surface_Sound_Speed').AsSingle;
            Layer_Sound_Speed              := FieldByName('Layer_Sound_Speed').AsSingle;
            Bottom_Sound_Speed             := FieldByName('Bottom_Sound_Speed').AsSingle;
            Bottomloss_Coefficient         := FieldByName('Bottomloss_Coefficient')
              .AsInteger;
            Ave_Ocean_Depth                := FieldByName('Ave_Ocean_Depth').AsSingle;
            CZ_Active                      := FieldByName('CZ_Active').AsInteger;
            Surface_Ducting_Active         := FieldByName('Surface_Ducting_Active')
              .AsInteger;
            Upper_Limit_Surface_Duct_Depth := FieldByName
              ('Upper_Limit_Surface_Duct_Depth').AsSingle;
            Lower_Limit_Surface_Duct_Depth := FieldByName
              ('Lower_Limit_Surface_Duct_Depth').AsSingle;
            Sub_Ducting_Active             := FieldByName('Sub_Ducting_Active').AsInteger;
            Upper_Limit_Sub_Duct_Depth     := FieldByName('Upper_Limit_Sub_Duct_Depth')
              .AsSingle;
            Lower_Limit_Sub_Duct_Depth     := FieldByName('Lower_Limit_Sub_Duct_Depth')
              .AsSingle;
            Shipping_Rate                  := FieldByName('Shipping_Rate').AsInteger;
            Shadow_Zone_Trans_Loss         := FieldByName('Shadow_Zone_Trans_Loss')
              .AsSingle;
            Atmospheric_Refract_Modifier   := FieldByName
              ('Atmospheric_Refract_Modifier').AsSingle;
            Barometric_Pressure            := FieldByName('Barometric_Pressure').AsSingle;
            Air_Temperature                := FieldByName('Air_Temperature').AsSingle;
            Surface_Temperature            := FieldByName('Surface_Temperature').AsSingle;
            Start_HF_Range_Gap             := FieldByName('Start_HF_Range_Gap').AsSingle;
            End_HF_Range_Gap               := FieldByName('End_HF_Range_Gap').AsSingle;

            Occurance_Range                := FieldByName('Occurance_Range').AsSingle;
            Width                          := FieldByName('Width').AsSingle;
            Signal_Reduction_Term          := FieldByName('Signal_Reduction_Term').AsSingle;
            Increase_per_CZ                := FieldByName('Increase_per_CZ').AsSingle;
            Max_Sonar_Depth                := FieldByName('Max_Sonar_Depth').AsSingle;
          end;
        end;
      finally

      end;
    end;

end;

function TDMLite.GetGame_Rainfall_On_ESM(const id: Integer;
  var rec: TDBGameRainfallOnESMList): boolean;
var
  sp : TZStoredProc;
  esm : TDBGame_Rainfall_On_ESM;
begin
  result := false;

  if not ZConn.Connected then
    exit;

  sp := createStoredProcedure('get_g_rain_esm_info');
  if Assigned(sp) then
    with sp do
    begin
      Params.ParamByName('@default').AsInteger := id;

      try
        Open;

        result := RecordCount > 0;
        if not IsEmpty then
        begin
          First;


          while not EOF do
          begin
            esm := TDBGame_Rainfall_On_ESM.Create;

            with esm do
            begin
              Defaults_Index := id;
              Radar_Frequency := FieldByName('Radar_Frequency').AsSingle;
              Rain_0_Effect := FieldByName('Rain_0_Effect').AsSingle;
              Rain_1_Effect := FieldByName('Rain_1_Effect').AsSingle;
              Rain_2_Effect := FieldByName('Rain_2_Effect').AsSingle;
              Rain_3_Effect := FieldByName('Rain_3_Effect').AsSingle;
              Rain_4_Effect := FieldByName('Rain_4_Effect').AsSingle;
              Rain_5_Effect := FieldByName('Rain_5_Effect').AsSingle;
              Rain_6_Effect := FieldByName('Rain_6_Effect').AsSingle;
            end;

            rec.Add(esm);
            Next;
          end;
        end;
      finally
        sp.Close;
        FreeAndNil(sp);
      end;
      Result := True;
    end;
end;

function TDMLite.GetGame_Rainfall_On_Missile_Seeker(const id: Integer;
  var rec: TDBGameRainfallOnMissSeekerList): boolean;
var
  sp : TZStoredProc;
  rain : TDBGame_Rainfall_On_Missile_Seeker;
begin
  result := false;

  if not ZConn.Connected then
    exit;

  sp := createStoredProcedure('get_g_rain_missile_seeker_info');
  if Assigned(sp) then
    with sp do
    begin
      Params.ParamByName('@default').AsInteger := id;

      try
        Open;

        result := RecordCount > 0;
        if not IsEmpty then
        begin
          First;

          while not Eof do
          begin
            rain := TDBGame_Rainfall_On_Missile_Seeker.Create;
            with rain do
            begin
              Guide_Type := FieldByName('Guide_Type').AsInteger;
              Rain_0_Effect := FieldByName('Rain_0_Effect').AsSingle;
              Rain_1_Effect := FieldByName('Rain_1_Effect').AsSingle;
              Rain_2_Effect := FieldByName('Rain_2_Effect').AsSingle;
              Rain_3_Effect := FieldByName('Rain_3_Effect').AsSingle;
              Rain_4_Effect := FieldByName('Rain_4_Effect').AsSingle;
              Rain_5_Effect := FieldByName('Rain_5_Effect').AsSingle;
              Rain_6_Effect := FieldByName('Rain_6_Effect').AsSingle;
            end;

            rec.Add(rain);
            Next;
          end;
        end;
      finally
        sp.Close;
        FreeAndNil(sp);
      end;
      Result := True;
    end;
end;

function TDMLite.GetGame_Rainfall_On_Radar(const id: Integer;
  var rec: TDBGameRainfallOnRadarList): boolean;
var
  sp : TZStoredProc;
  rain: TDBGame_Rainfall_On_Radar;
begin
  result := false;

  if not ZConn.Connected then
    exit;

  sp := createStoredProcedure('get_g_rain_radar_info');
  if Assigned(sp) then
    with sp do
    begin
      Params.ParamByName('@default').AsInteger := id;

      try
        Open;

        result := RecordCount > 0;
        if not IsEmpty then
        begin
          First;

          while not Eof do
          begin
            rain := TDBGame_Rainfall_On_Radar.Create;
            with rain do
            begin
              Defaults_Index := id;
              Radar_Frequency := FieldByName('Radar_Frequency').AsSingle;
              Rain_0_Effect := FieldByName('Rain_0_Effect').AsSingle;
              Rain_1_Effect := FieldByName('Rain_1_Effect').AsSingle;
              Rain_2_Effect := FieldByName('Rain_2_Effect').AsSingle;
              Rain_3_Effect := FieldByName('Rain_3_Effect').AsSingle;
              Rain_4_Effect := FieldByName('Rain_4_Effect').AsSingle;
              Rain_5_Effect := FieldByName('Rain_5_Effect').AsSingle;
              Rain_6_Effect := FieldByName('Rain_6_Effect').AsSingle;
            end;

            rec.Add(rain);
            Next;
          end;
        end;
        Result := True;
      finally
        sp.Close;
        FreeAndNil(sp);
      end;
    end;
end;

function TDMLite.GetGame_Rainfall_On_Sonar(const id: Integer;
  var rec: TDBGameRainfallOnSonarList): boolean;
var
  sp : TZStoredProc;
  rain: TDBGame_Rainfall_On_Sonar;
begin
  result := false;

  if not ZConn.Connected then
    exit;

  sp := createStoredProcedure('get_g_rain_sonar_info');
  if Assigned(sp) then
    with sp do
    begin
      Params.ParamByName('@default').AsInteger := id;

      try
        Open;

        result := RecordCount > 0;
        if not IsEmpty then
        begin
          First;

          while not Eof do
          begin
            rain := TDBGame_Rainfall_On_Sonar.Create;
            with rain do
            begin
              Defaults_Index := id;
              Sonar_Frequency := FieldByName('Sonar_Frequency').AsSingle;
              Rain_0_Effect := FieldByName('Rain_0_Effect').AsSingle;
              Rain_1_Effect := FieldByName('Rain_1_Effect').AsSingle;
              Rain_2_Effect := FieldByName('Rain_2_Effect').AsSingle;
              Rain_3_Effect := FieldByName('Rain_3_Effect').AsSingle;
              Rain_4_Effect := FieldByName('Rain_4_Effect').AsSingle;
              Rain_5_Effect := FieldByName('Rain_5_Effect').AsSingle;
              Rain_6_Effect := FieldByName('Rain_6_Effect').AsSingle;
            end;

            rec.Add(rain);
            Next;
          end;
        end;
        Result := True;

      finally
        sp.Close;
        FreeAndNil(sp);
      end;
    end;
end;

function TDMLite.GetGame_Sea_On_Missile_Safe_Altitude(const id: Integer;
  var rec: TDBGame_Sea_On_Missile_Safe_Altitude): boolean;
var
  sp : TZStoredProc;
begin
  result := false;

  if not ZConn.Connected then
    exit;

  sp := createStoredProcedure('get_g_sea_missile_safe_altitude_info');
  if Assigned(sp) then
    with sp do
    begin
      Params.ParamByName('@default').AsInteger := id;

      try
        Open;

        result := RecordCount > 0;

        if not IsEmpty then
        begin
          First;

          rec := TDBGame_Sea_On_Missile_Safe_Altitude.Create;
          with rec do
          begin
            Defaults_Index := id;
            Sea_0_Effect := FieldByName('Sea_0_Effect').AsSingle;
            Sea_1_Effect := FieldByName('Sea_1_Effect').AsSingle;
            Sea_2_Effect := FieldByName('Sea_2_Effect').AsSingle;
            Sea_3_Effect := FieldByName('Sea_3_Effect').AsSingle;
            Sea_4_Effect := FieldByName('Sea_4_Effect').AsSingle;
            Sea_5_Effect := FieldByName('Sea_5_Effect').AsSingle;
            Sea_6_Effect := FieldByName('Sea_6_Effect').AsSingle;
            Sea_7_Effect := FieldByName('Sea_7_Effect').AsSingle;
            Sea_8_Effect := FieldByName('Sea_8_Effect').AsSingle;
            Sea_9_Effect := FieldByName('Sea_9_Effect').AsSingle;
          end;
        end;
        Result := True;
      finally
        sp.Close;
        FreeAndNil(sp);
      end;
    end;
end;

function TDMLite.GetGame_Sea_On_Radar(const id: Integer;
  var rec: TDBGame_Sea_On_Radar): boolean;
var
  sp : TZStoredProc;
begin
  result := false;

  if not ZConn.Connected then
    exit;

  sp := createStoredProcedure('get_g_sea_radar_info');
  if Assigned(sp) then
    with sp do
    begin
      Params.ParamByName('@default').AsInteger := id;

      try
        Open;

        result := RecordCount > 0;

        if not IsEmpty then
        begin
          First;

          rec := TDBGame_Sea_On_Radar.Create;
          with rec do
          begin
            Defaults_Index := id;
            Sea_0_Effect := FieldByName('Sea_0_Effect').AsSingle;
            Sea_1_Effect := FieldByName('Sea_1_Effect').AsSingle;
            Sea_2_Effect := FieldByName('Sea_2_Effect').AsSingle;
            Sea_3_Effect := FieldByName('Sea_3_Effect').AsSingle;
            Sea_4_Effect := FieldByName('Sea_4_Effect').AsSingle;
            Sea_5_Effect := FieldByName('Sea_5_Effect').AsSingle;
            Sea_6_Effect := FieldByName('Sea_6_Effect').AsSingle;
            Sea_7_Effect := FieldByName('Sea_7_Effect').AsSingle;
            Sea_8_Effect := FieldByName('Sea_8_Effect').AsSingle;
            Sea_9_Effect := FieldByName('Sea_9_Effect').AsSingle;
          end;
        end;
      finally
        sp.Close;
        FreeAndNil(sp);
      end;
    end;
end;

function TDMLite.GetGame_Sea_On_Sonar(const id: Integer;
  var rec: TDBGameSeaOnSonarlist): boolean;
var
  sp : TZStoredProc;
  sea: TDBGame_Sea_On_Sonar;
begin
  result := false;

  if not ZConn.Connected then
    exit;

  sp := createStoredProcedure('get_g_sea_sonar_info');
  if Assigned(sp) then
    with sp do
    begin
      Params.ParamByName('@default').AsInteger := id;

      try
        Open;

        result := RecordCount > 0;

        if not IsEmpty then
        begin
          First;

          while not EOF do
          begin
            sea := TDBGame_Sea_On_Sonar.Create;
            with sea do
            begin
              Defaults_Index := id;
              Sonar_Frequency := FieldByName('Sonar_Frequency').AsSingle;
              Sea_0_Effect := FieldByName('Sea_0_Effect').AsSingle;
              Sea_1_Effect := FieldByName('Sea_1_Effect').AsSingle;
              Sea_2_Effect := FieldByName('Sea_2_Effect').AsSingle;
              Sea_3_Effect := FieldByName('Sea_3_Effect').AsSingle;
              Sea_4_Effect := FieldByName('Sea_4_Effect').AsSingle;
              Sea_5_Effect := FieldByName('Sea_5_Effect').AsSingle;
              Sea_6_Effect := FieldByName('Sea_6_Effect').AsSingle;
              Sea_7_Effect := FieldByName('Sea_7_Effect').AsSingle;
              Sea_8_Effect := FieldByName('Sea_8_Effect').AsSingle;
              Sea_9_Effect := FieldByName('Sea_9_Effect').AsSingle;
            end;

            rec.Add(sea);
            Next;
          end;
        end;
      finally
        sp.Close;
        FreeAndNil(sp);
      end;
    end;
end;

function TDMLite.GetGame_Ship_On_Sonar(const id: Integer;
  var rec: TDBGameShipOnSonarList): boolean;
var
  sp : TZStoredProc;
  ship : TDBGame_Ship_On_Sonar;
begin
  result := false;

  sp := createStoredProcedure('get_g_ship_sonar_info');
  if Assigned(sp) then
    with sp do
    begin
      Params.ParamByName('@default').AsInteger := id;

      try
        Open;

        result := RecordCount > 0;

        if not IsEmpty then
        begin
          First;

          while not EOF do
          begin
            ship := TDBGame_Ship_On_Sonar.Create;
            with ship do
            begin
              Defaults_Index := id;
              Sonar_Frequency := FieldByName('Sonar_Frequency').AsSingle;
              Distant_Ship_Effect := FieldByName('Distant_Ship_Effect').AsSingle;
              Light_Ship_Effect := FieldByName('Light_Ship_Effect').AsSingle;
              Medium_Ship_Effect := FieldByName('Medium_Ship_Effect').AsSingle;
              Heavy_Ship_Effect := FieldByName('Heavy_Ship_Effect').AsSingle;
            end;

            rec.Add(ship);
            Next;
          end;
        end;
      finally
        sp.Close;
        FreeAndNil(sp);
      end;
    end;
end;


function TDMLite.GetGunDefinition(const gun_id: integer;
  dict: TDBGunDefinitionDict): TDBGun_Definition;
var
  sp : TZStoredProc;
  gunDefinition : TDBGun_Definition;
  newData : Boolean;
begin
  result := nil;
  newData := False;

  { search in dict first }
  if not dict.ContainsKey(gun_id) then
    newData := True
  else
    result := dict.Items[gun_id];

  { new data add to dict }
  if newData then
  begin
    sp := createStoredProcedure('get_gun_info');
    if Assigned(sp) then
      with sp do
      begin
        Params.ParamByName('@index').AsInteger := gun_id;

        try
          Open;

          if not IsEmpty then
          begin
            First;

            gunDefinition := TDBGun_Definition.Create;
            dict.Add(gun_id,gunDefinition);

            with gunDefinition do
            begin
              Gun_Index                   := gun_id;
              Gun_Identifier              := FieldByName('Gun_Identifier').AsString;
              Gun_Category                := FieldByName('Gun_Category').AsInteger;
              Rate_of_Fire                := FieldByName('Rate_of_Fire').AsInteger;
              Lethality_per_Round         := FieldByName('Lethality_per_Round').AsInteger;
              Min_Range                   := FieldByName('Min_Range').AsSingle;
              Max_Range                   := FieldByName('Max_Range').AsSingle;
              Air_Min_Range               := FieldByName('Air_Min_Range').AsSingle;
              Air_Max_Range               := FieldByName('Air_Max_Range').AsSingle;
              Fire_Cntl_Director_Req      := FieldByName('Fire_Cntl_Director_Req').AsInteger;
              Chaff_Capable_Gun           := FieldByName('Chaff_Capable_Gun').AsInteger;
              Anti_Sur_Capable            := FieldByName('Anti_Sur_Capable').AsInteger;
              Anti_Land_Capable           := FieldByName('Anti_Land_Capable').AsInteger;
              Anti_Air_Capable            := FieldByName('Anti_Air_Capable').AsInteger;
              Anti_Amphibious_Capable     := FieldByName('Anti_Amphibious_Capable').AsInteger;
              Automode_Capable            := FieldByName('Automode_Capable').AsInteger;
              Max_Target_Altitude_Delta   := FieldByName('Max_Target_Altitude_Delta').AsInteger;
              Gun_Average_Shell_Velocity  := FieldByName('Gun_Average_Shell_Velocity').AsSingle;
              Man_Gun_Max_Elevation       := FieldByName('Man_Gun_Max_Elevation').AsSingle;
              Man_Gun_Min_Elevation       := FieldByName('Man_Gun_Min_Elevation').AsSingle;
              Man_Gun_Rotation_Rate       := FieldByName('Man_Gun_Rotation_Rate').AsSingle;
              Man_Gun_Elevation_Rate      := FieldByName('Man_Gun_Elevation_Rate').AsSingle;
              Man_Gun_Num_Rounds_Per_Load := FieldByName('Man_Gun_Num_Rounds_Per_Load').AsInteger;
              Man_Gun_Time_to_Reload      := FieldByName('Man_Gun_Time_to_Reload').AsSingle;
              Man_Gun_Muzzle_Velocity     := FieldByName('Man_Gun_Muzzle_Velocity').AsSingle;
              NGS_Capable                 := FieldByName('NGS_Capable').AsInteger;
              NGS_MinDeflectionError      := FieldByName('NGS_MinDeflectionError').AsSingle;
              NGS_MaxDeflectionError      := FieldByName('NGS_MaxDeflectionError').AsSingle;
              NGS_MinRangeError           := FieldByName('NGS_MinRangeError').AsSingle;
              NGS_MaxRangeError           := FieldByName('NGS_MaxRangeError').AsSingle;
              NGS_MaxDispersionError      := FieldByName('NGS_MaxDispersionError').AsSingle;
              NGS_MaxDamageRadius         := FieldByName('NGS_MaxDamageRadius').AsSingle;
              NGS_EffectiveRadius         := FieldByName('NGS_EffectiveRadius').AsSingle;
              NGS_DamageRating            := FieldByName('NGS_DamageRating').AsInteger;
            end;

            result := gunDefinition;
          end;
        finally
          sp.Close;
          FreeAndNil(sp);
        end;
      end;
  end;
end;

function TDMLite.GetHeliLaunchLimits(const vehicle_id: Integer;
  dict: TDBHelicopterLandLaunchLimitsDict): TDBHelicopter_Land_Launch_Limits;
var
  sp : TZStoredProc;
  heloDefinition : TDBHelicopter_Land_Launch_Limits;
  newData : Boolean;
begin
  result := nil;
  newData := False;

  { search in dict first }
  if not dict.ContainsKey(vehicle_id) then
    newData := True
  else
    result := dict.Items[vehicle_id];

  { new data add to dict }
  if newData then
  begin
    sp := createStoredProcedure('get_helo_limits_info');
    if Assigned(sp) then
      with sp do
      begin
        Params.ParamByName('@vehicle').AsInteger := vehicle_id;

        try
          Open;

          if not IsEmpty then
          begin
            First;

            if not dict.ContainsKey(vehicle_id) then
            begin
              heloDefinition := TDBHelicopter_Land_Launch_Limits.Create;
              dict.Add(vehicle_id,heloDefinition);

              with heloDefinition do
              begin
                Vehicle_Index                := vehicle_id;
                Max_Relative_Wind_Magnitude  := FieldByName('Max_Relative_Wind_Magnitude').Assingle;
                Max_Turn_Rate_To_Launch      := FieldByName('Max_Turn_Rate_To_Launch').Asinteger;
                Max_Turn_Rate_To_Land        := FieldByName('Max_Turn_Rate_To_Land').Asinteger;
                Max_Landing_Altitude         := FieldByName('Max_Landing_Altitude').Assingle;
                Max_Relative_Speed           := FieldByName('Max_Relative_Speed').Asinteger;
                Approach_Range               := FieldByName('Approach_Range').Assingle;
                Approach_Center_Bearing      := FieldByName('Approach_Center_Bearing').Asinteger;
                Approach_Sector_Width        := FieldByName('Approach_Sector_Width').Asinteger;
              end;
            end;
            result := heloDefinition;
          end;
        finally
          sp.Close;
          FreeAndNil(sp);
        end;
      end;
  end;
end;

function TDMLite.GetIFFOnBoard(const veh_id: integer;
  dict: TDBIFFSensorOnBoardDict): TDBIFFOnBoardList;
var
  sp : TZStoredProc;
  iffOnBoard : TDBIFF_Sensor_On_Board;
  iffOnBoardList : TDBIFFOnBoardList;
  newData : Boolean;
begin
  result := nil;
  newData := False;

  { search in dict first }
  if not dict.ContainsKey(veh_id) then
    newData := True
  else
    result := dict.Items[veh_id];

  { new data add to dict }
  if newData then
  begin
    sp := createStoredProcedure('get_iff_instances');
    if Assigned(sp) then
      with sp do
      begin
        Params.ParamByName('@vehicle').AsInteger := veh_id;

        try
          Open;

          if not IsEmpty then
          begin
            First;

            iffOnBoardList := TDBIFFOnBoardList.Create;
            dict.Add(veh_id,iffOnBoardList);

            while not EOF do
            begin
              iffOnBoard := TDBIFF_Sensor_On_Board.Create;

              with iffOnBoard do
              begin
                IFF_Instance_Index       := FieldByName('IFF_Instance_Index').AsInteger;
                Instance_Identifier      := FieldByName('Instance_Identifier').AsString;
                Instance_Type            := FieldByName('Instance_Type').AsInteger;
                Vehicle_Index            := veh_id;
                IFF_Capability           := FieldByName('IFF_Capability').AsInteger;
                Rel_Antenna_Height       := FieldByName('Rel_Antenna_Height').AsSingle;
                Submerged_Antenna_Height := FieldByName('Submerged_Antenna_Height').AsSingle;
                Max_Operational_Depth    := FieldByName('Max_Operational_Depth').AsSingle;
              end;

              iffOnBoardList.Add(iffOnBoard);

              Next;
            end;

            result := iffOnBoardList;
          end;
        finally
          sp.Close;
          FreeAndNil(sp);
        end;
      end;
  end;
end;

function TDMLite.GetInfraredDecoyDefinition(const decoy_id: integer;
  dict: TDBInfraredDecoyDefinitionDict): TDBInfrared_Decoy_Definition;
var
  sp : TZStoredProc;
  decoyDefinition : TDBInfrared_Decoy_Definition;
  newData : Boolean;
begin
  result := nil;
  newData := False;

  { search in dict first }
  if not dict.ContainsKey(decoy_id) then
    newData := True
  else
    result := dict.Items[decoy_id];

  { new data add to dict }
  if newData then
  begin
    sp := createStoredProcedure('get_infrared_decoy_info');
    if Assigned(sp) then
      with sp do
      begin
        Params.ParamByName('@index').AsInteger := decoy_id;

        try
          Open;

          if not IsEmpty then
          begin
            First;

            decoyDefinition := TDBInfrared_Decoy_Definition.Create;
            dict.Add(decoy_id,decoyDefinition);

            with decoyDefinition do
            begin
              Infrared_Decoy_Index      := decoy_id;
              Infrared_Decoy_Identifier := FieldByName('Infrared_Decoy_Identifier').AsString;
              Platform_Domain           := FieldByName('Platform_Domain').AsInteger;
              Platform_Category         := FieldByName('Platform_Category').AsInteger;
              Platform_Type             := FieldByName('Platform_Type').AsInteger;
              Max_Infrared_Cross        := FieldByName('Max_Infrared_Cross').AsSingle;
              Bloom_Time                := FieldByName('Bloom_Time').AsInteger;
              Sustain_Time              := FieldByName('Sustain_Time').AsInteger;
              Max_Dissipation_Time      := FieldByName('Max_Dissipation_Time').AsInteger;
              Min_Dissipation_Time      := FieldByName('Min_Dissipation_Time').AsInteger;
              Descent_Rate              := FieldByName('Descent_Rate').AsSingle;
            end;

            result := decoyDefinition;
          end;
        finally
          sp.Close;
          FreeAndNil(sp);
        end;
      end;
  end;
end;

function TDMLite.GetInfraredDecoyOnBoard(const veh_id: integer;
  dict: TDBInfraredDecoyOnBoardDict): TDBInfraredDecoyOnBoardList;
var
  sp               : TZStoredProc;
  devOnBoard       : TDBInfrared_Decoy_On_Board;
  devOnBoardList   : TDBInfraredDecoyOnBoardList;
  newData          : Boolean;
begin
  result := nil;
  newData := False;

  { search in dict first }
  if not dict.ContainsKey(veh_id) then
    newData := True
  else
    result := dict.Items[veh_id];

  { new data add to dict }
  if newData then
  begin
    sp := createStoredProcedure('get_infrared_decoy_instances');
    if Assigned(sp) then
      with sp do
      begin
        Params.ParamByName('@vehicle').AsInteger := veh_id;

        try
          Open;

          if not IsEmpty then
          begin
            First;

            devOnBoardList := TDBInfraredDecoyOnBoardList.Create;
            dict.Add(veh_id,devOnBoardList);

            while not EOF do
            begin
              devOnBoard := TDBInfrared_Decoy_On_Board.Create;

              with devOnBoard do
              begin
                Infrared_Decoy_Instance_Index := FieldByName('Infrared_Decoy_Instance_Index').AsInteger;
                Instance_Identifier           := FieldByName('Instance_Identifier').AsString;
                Instance_Type                 := FieldByName('Instance_Type').AsInteger;
                Infrared_Decoy_Qty_On_Board   := FieldByName('Infrared_Decoy_Qty_On_Board').AsInteger;
                Infrared_Decoy_Index          := FieldByName('Infrared_Decoy_Index').AsInteger;
              end;

              devOnBoardList.Add(devOnBoard);

              Next;
            end;

            result := devOnBoardList;
          end;
        finally
          sp.Close;
          FreeAndNil(sp);
        end;
      end;
  end;
end;

function TDMLite.GetJammerDefinition(const jammer_id: integer;
  dict: TDBJammerDefinitionDict): TDBJammer_Definition;
var
  sp : TZStoredProc;
  jammerDefinition : TDBJammer_Definition;
  newData : Boolean;
begin
  result := nil;
  newData := False;

  { search in dict first }
  if not dict.ContainsKey(jammer_id) then
    newData := True
  else
    result := dict.Items[jammer_id];

  { new data add to dict }
  if newData then
  begin
    sp := createStoredProcedure('get_jammer_info');
    if Assigned(sp) then
      with sp do
      begin
        Params.ParamByName('@index').AsInteger := jammer_id;

        try
          Open;

          if not IsEmpty then
          begin
            First;

            jammerDefinition := TDBJammer_Definition.Create;
            dict.Add(jammer_id,jammerDefinition);

            with jammerDefinition do
            begin
              Jammer_Index              := jammer_id;
              Jammer_Type               := FieldByName('Jammer_Type').AsInteger;
              Jammer_Identifier         := FieldByName('Jammer_Identifier').AsString;
              Lower_Freq_Limit          := FieldByName('Lower_Freq_Limit').AsFloat;
              Upper_Freq_Limit          := FieldByName('Upper_Freq_Limit').AsFloat;
              Jammer_Power_Density      := FieldByName('Jammer_Power_Density').AsFloat;
              Max_Effective_Range       := FieldByName('Max_Effective_Range').AsSingle;
              Max_Sector_Width          := FieldByName('Max_Sector_Width').AsSingle;
              Upper_Vert_Coverage_Angle := FieldByName('Upper_Vert_Coverage_Angle').AsSingle;
              Lower_Vert_Coverage_Angle := FieldByName('Lower_Vert_Coverage_Angle').AsSingle;
            end;

            result := jammerDefinition;
          end;
        finally
          sp.Close;
          FreeAndNil(sp);
        end;
      end;
  end;
end;

function TDMLite.GetJammerOnBoard(const veh_id: integer;
  dict: TDBJammerOnBoardDict): TDBJammerOnBoardList;
var
  sp               : TZStoredProc;
  devOnBoard       : TDBJammer_On_Board;
  devOnBoardList   : TDBJammerOnBoardList;
  newData          : Boolean;
begin
  result := nil;
  newData := False;

  { search in dict first }
  if not dict.ContainsKey(veh_id) then
    newData := True
  else
    result := dict.Items[veh_id];

  { new data add to dict }
  if newData then
  begin
    sp := createStoredProcedure('get_jammer_instances');
    if Assigned(sp) then
      with sp do
      begin
        Params.ParamByName('@vehicle').AsInteger := veh_id;

        try
          Open;

          if not IsEmpty then
          begin
            First;

            devOnBoardList := TDBJammerOnBoardList.Create;
            dict.Add(veh_id,devOnBoardList);

            while not EOF do
            begin
              devOnBoard := TDBJammer_On_Board.Create;

              with devOnBoard do
              begin
                Jammer_Instance_Index := FieldByName('Jammer_Instance_Index').AsInteger;
                Instance_Identifier   := FieldByName('Instance_Identifier').AsString;
                Instance_Type         := FieldByName('Instance_Type').AsInteger;
                Jammer_Index          := FieldByName('Jammer_Index').AsInteger;
                Antenna_Height        := FieldByName('Antenna_Height').AsSingle;
              end;

              devOnBoardList.Add(devOnBoard);

              Next;
            end;

            result := devOnBoardList;
          end;
        finally
          sp.Close;
          FreeAndNil(sp);
        end;
      end;
  end;
end;

function TDMLite.GetLinkDefinition(const deploy_id: Integer;
  var list: TDBLinkDefinitionList): Boolean;
var
  sp      : TZStoredProc;
  link    : TDBLink_Definition;
begin
  result := false;

  if not ZConn.Connected then
    exit;

  sp := createStoredProcedure('get_gl_link_info');
  if Assigned(sp) then
    with sp do
    begin
      Params.ParamByName('@deploy').AsInteger := deploy_id;

      try
        Open;

        result := RecordCount > 0;
        if not IsEmpty then
        begin

          First;

          while not EOF do
          begin
            link := TDBLink_Definition.Create;

            with link do
            begin
              Link_Index          := FieldByName('Link_Index').AsInteger;
              Link_Identifier_Num := FieldByName('Link_Identifier_Num').AsInteger;
              Link_Force          := FieldByName('Link_Force').AsInteger;
              Link_Controller     := FieldByName('Link_Controller').AsInteger;
              Deployment_Index    := deploy_id;
              Trans_Mode          := FieldByName('Trans_Mode').AsInteger;
              Link_Identifier     := FieldByName('Link_Identifier').AsString;
            end;

            list.Add(link);
            Next;
          end;
        end;
      finally
        sp.Close;
        FreeAndNil(sp);
      end;
      Result := True;
    end;
end;

function TDMLite.GetLinkParticipant(const link_id : integer;
  dict: TDBLinkParticipantsDict): TDBLinkParticipantsList;
var
  sp            : TZStoredProc;
  part          : TDBLink_Participant;
  participants  : TDBLinkParticipantsList;
  newData       : Boolean;
begin
  result := nil;
  newData := False;

  { search in dict first }
  if not dict.ContainsKey(link_id) then
    newData := True
  else
    result := dict.Items[link_id];

  { new data add to dict }
  if newData then
  begin
    sp := createStoredProcedure('get_gl_participant_instances');
    if Assigned(sp) then
      with sp do
      begin
        Params.ParamByName('@link').AsInteger := link_id;

        try
          Open;

          if not IsEmpty then
          begin
            First;

            participants := TDBLinkParticipantsList.Create;
            dict.Add(link_id,participants);

            while not EOF do
            begin
              part := TDBLink_Participant.Create;

              with part do
              begin
                Link_Index         := link_id;
                Participating_Unit := FieldByName('Participating_Unit').AsInteger;
                PU_Octal_Code      := FieldByName('PU_Octal_Code').AsInteger;
                Deployment_Index   := FieldByName('Deployment_Index').AsInteger;
              end;

              participants.Add(part);

              Next;
            end;

            result := participants;
          end;
        finally
          sp.Close;
          FreeAndNil(sp);
        end;
      end;
  end;
end;

function TDMLite.GetMADOnBoard(const veh_id: integer;
  dict: TDBMADSensorOnBoardDict): TDBMADOnBoardList;
var
  sp : TZStoredProc;
  madOnBoard : TDBMAD_Sensor_On_Board;
  madOnBoardList : TDBMADOnBoardList;
  newData : Boolean;
begin
  result := nil;
  newData := False;

  { search in dict first }
  if not dict.ContainsKey(veh_id) then
    newData := True
  else
    result := dict.Items[veh_id];

  { new data add to dict }
  if newData then
  begin
    sp := createStoredProcedure('get_mad_instances');
    if Assigned(sp) then
      with sp do
      begin
        Params.ParamByName('@vehicle').AsInteger := veh_id;

        try
          Open;

          if not IsEmpty then
          begin
            First;

            madOnBoardList := TDBMADOnBoardList.Create;
            dict.Add(veh_id,madOnBoardList);

            while not EOF do
            begin
              madOnBoard := TDBMAD_Sensor_On_Board.Create;

              with madOnBoard do
              begin
                MAD_Instance_Index  := FieldByName('MAD_Instance_Index').AsInteger;
                Instance_Identifier := FieldByName('Instance_Identifier').AsString;
                Instance_Type       := FieldByName('Instance_Type').AsInteger;
                Vehicle_Index       := veh_id;
                MAD_Index           := FieldByName('MAD_Index').AsInteger;
                Antenna_Height      := FieldByName('Antenna_Height').AsSingle;
              end;

              madOnBoardList.Add(madOnBoard);

              Next;
            end;

            result := madOnBoardList;
          end;
        finally
          sp.Close;
          FreeAndNil(sp);
        end;
      end;
  end;
end;

function TDMLite.GetMAD_Definition(const mad_id: integer;
  dict: TDBMADDefinitionDict): TDBMAD_Definition;
var
  sp : TZStoredProc;
  madDefinition : TDBMAD_Definition;
  newData : Boolean;
begin
  result := nil;
  newData := False;

  { search in dict first }
  if not dict.ContainsKey(mad_id) then
    newData := True
  else
    result := dict.Items[mad_id];

  { new data add to dict }
  if newData then
  begin
    sp := createStoredProcedure('get_mad_info');
    if Assigned(sp) then
      with sp do
      begin
        Params.ParamByName('@index').AsInteger := mad_id;

        try
          Open;

          if not IsEmpty then
          begin
            First;

            madDefinition := TDBMAD_Definition.Create;
            dict.Add(mad_id,madDefinition);

            with madDefinition do
            begin
              MAD_Index             := mad_id;
              Class_Identifier      := FieldByName('Class_Identifier').AsString;
              Baseline_Detect_Range := FieldByName('Baseline_Detect_Range').AsSingle;
              Known_Cross_Section   := FieldByName('Known_Cross_Section').AsSingle;
            end;

            result := madDefinition;
          end;
        finally
          sp.Close;
          FreeAndNil(sp);
        end;
      end;
  end;
end;

function TDMLite.GetMinePOD(const mineIndex: integer;
  dict: TDBMine_POD_vs_RangeDict): TDBMine_POD_vs_RangeList;
var
  sp         : TZStoredProc;
  pod        : TDBMine_POD_vs_Range;
  pods       : TDBMine_POD_vs_RangeList;
  newData    : Boolean;
begin
  result := nil;
  newData := False;

  { search in dict first }
  if not dict.ContainsKey(mineIndex) then
    newData := True
  else
    result := dict.Items[mineIndex];

  { new data add to dict }
  if newData then
  begin
    sp := createStoredProcedure('get_gl_mine_curve_instances');
    if Assigned(sp) then
      with sp do
      begin
        Params.ParamByName('@mine').AsInteger := mineIndex;

        try
          Open;

          if not IsEmpty then
          begin
            First;

            pods := TDBMine_POD_vs_RangeList.Create;
            dict.Add(mineIndex,pods);

            while not EOF do
            begin
              pod := TDBMine_POD_vs_Range.Create;

              with pod do
              begin
                List_Index        := FieldByName('List_Index').AsInteger;
                Mine_Index        := mineIndex;
                Prob_of_Detonation:= FieldByName('Prob_of_Detonation').AsSingle;
                Range             := FieldByName('Range').AsSingle;
              end;

              pods.Add(pod);

              Next;
            end;

            result := pods;
          end;
        finally
          sp.Close;
          FreeAndNil(sp);
        end;
      end;
  end;
end;

function TDMLite.GetMine_Definition(const mine_id: integer;
  dict: TDBMineDefinitionDict): TDBMine_Definition;
var
  sp : TZStoredProc;
  mineDefinition : TDBMine_Definition;
  newData : Boolean;
begin
  result := nil;
  newData := False;

  { search in dict first }
  if not dict.ContainsKey(mine_id) then
    newData := True
  else
    result := dict.Items[mine_id];

  { new data add to dict }
  if newData then
  begin
    sp := createStoredProcedure('get_mine_info');
    if Assigned(sp) then
      with sp do
      begin
        Params.ParamByName('@index').AsInteger := mine_id;

        try
          Open;

          if not IsEmpty then
          begin
            First;

            mineDefinition := TDBMine_Definition.Create;
            dict.Add(mine_id,mineDefinition);

            with mineDefinition do
            begin
              Mine_Index           := mine_id;
              Mine_Identifier      := FieldByName('Mine_Identifier').AsString;
              Platform_Domain      := FieldByName('Platform_Domain').AsInteger;
              Platform_Category    := FieldByName('Platform_Category').AsInteger;
              Platform_Type        := FieldByName('Platform_Type').AsInteger;
              Mine_Classification  := FieldByName('Mine_Classification').AsInteger;
              Length               := FieldByName('Length').AsSingle;
              Width                := FieldByName('Width').AsSingle;
              Height               := FieldByName('Height').AsSingle;
              Mooring_Type         := FieldByName('Mooring_Type').AsInteger;
              Max_Laying_Depth     := FieldByName('Max_Laying_Depth').AsSingle;
              Front_Acoustic_Cross := FieldByName('Front_Acoustic_Cross').AsSingle;
              Side_Acoustic_Cross  := FieldByName('Side_Acoustic_Cross').AsSingle;
              Mine_Lethality       := FieldByName('Mine_Lethality').AsInteger;
              Engagement_Range     := FieldByName('Engagement_Range').AsSingle;
              Anti_Sur_Capable     := FieldByName('Anti_Sur_Capable').AsInteger;
              Anti_SubSur_Capable  := FieldByName('Anti_SubSur_Capable').AsInteger;
              Detectability_Type   := FieldByName('Detectability_Type').AsInteger;
            end;

            result := mineDefinition;
          end;
        finally
          sp.Close;
          FreeAndNil(sp);
        end;
      end;
  end;

end;


function TDMLite.GetMissile_Definition(const missile_id: integer;
  dict: TDBMissileDefinitionDict): TDBMissile_Definition;
var
  sp : TZStoredProc;
  missDefinition : TDBMissile_Definition;
  newData : Boolean;
begin
  result := nil;
  newData := False;

  if not dict.ContainsKey(missile_id) then
    newData := True
  else
    result := dict.Items[missile_id];

  { new data add to dict }
  if newData then
  begin
    sp := createStoredProcedure('get_missile_info');
    if Assigned(sp) then
      with sp do
      begin
        Params.ParamByName('@index').AsInteger := missile_id;

        try
          Open;

          if not IsEmpty then
          begin
            First;

            missDefinition := TDBMissile_Definition.Create;
            dict.Add(missile_id,missDefinition);

            with missDefinition do
            begin
              Missile_Index                  := missile_id;
              Class_Identifier               := FieldByName('Class_Identifier').AsString;
              Platform_Domain                := FieldByName('Platform_Domain').AsInteger;
              Platform_Category              := FieldByName('Platform_Category').AsInteger;
              Platform_Type                  := FieldByName('Platform_Type').AsInteger;
              Max_Range                      := FieldByName('Max_Range').AsSingle;
              Min_Range                      := FieldByName('Min_Range').AsSingle;
              Motion_Index                   := FieldByName('Motion_Index').AsInteger;
              Seeker_TurnOn_Range            := FieldByName('Seeker_TurnOn_Range').AsSingle;
              Second_Seeker_Pattern_Capable  := FieldByName('Second_Seeker_Pattern_Capable').AsInteger;
              Seeker_Bias_Capable            := FieldByName('Seeker_Bias_Capable').AsInteger;
              Fixed_Seeker_Turn_On_Range     := FieldByName('Fixed_Seeker_Turn_On_Range').AsInteger;
              Lethality                      := FieldByName('Lethality').AsInteger;
              Prob_of_Hit                    := FieldByName('Prob_of_Hit').AsSingle;
              Damage_Capacity                := FieldByName('Damage_Capacity').AsInteger;
              Default_Altitude               := FieldByName('Default_Altitude').AsSingle;
              Length                         := FieldByName('Length').AsSingle;
              Width                          := FieldByName('Width').AsSingle;
              Height                         := FieldByName('Height').AsSingle;
              Front_Radar_Cross              := FieldByName('Front_Radar_Cross').AsSingle;
              Side_Radar_Cross               := FieldByName('Side_Radar_Cross').AsSingle;
              Front_Visual_Cross             := FieldByName('Front_Visual_Cross').AsSingle;
              Side_Visual_Cross              := FieldByName('Side_Visual_Cross').AsSingle;
              Front_Infrared_Cross           := FieldByName('Front_Infrared_Cross').AsSingle;
              Side_Infrared_Cross            := FieldByName('Side_Infrared_Cross').AsSingle;
              Pursuit_Guide_Type             := FieldByName('Pursuit_Guide_Type').AsInteger;
              Primary_Guide_Type             := FieldByName('Primary_Guide_Type').AsInteger;
              Secondary_Guide_Type           := FieldByName('Secondary_Guide_Type').AsInteger;
              Anti_Air_Capable               := FieldByName('Anti_Air_Capable').AsInteger;
              Anti_Sur_Capable               := FieldByName('Anti_Sur_Capable').AsInteger;
              Anti_SubSur_Capable            := FieldByName('Anti_SubSur_Capable').AsInteger;
              Anti_Land_Capable              := FieldByName('Anti_Land_Capable').AsInteger;
              Anti_Amphibious_Capable        := FieldByName('Anti_Amphibious_Capable').AsInteger;
              Primary_Target_Domain          := FieldByName('Primary_Target_Domain').AsInteger;
              SARH_POH_Modifier              := FieldByName('SARH_POH_Modifier').AsSingle;
              CG_POH_Modifier                := FieldByName('CG_POH_Modifier').AsSingle;
              TARH_POH_Modifier              := FieldByName('TARH_POH_Modifier').AsSingle;
              IR_POH_Modifier                := FieldByName('IR_POH_Modifier').AsSingle;
              AR_POH_Modifier                := FieldByName('AR_POH_Modifier').AsSingle;
              Transmitted_Frequency          := FieldByName('Transmitted_Frequency').AsFloat;
              Scan_Rate                      := FieldByName('Scan_Rate').AsSingle;
              Pulse_Rep_Freq                 := FieldByName('Pulse_Rep_Freq').AsSingle;
              Pulse_Width                    := FieldByName('Pulse_Width').AsSingle;
              Xmit_Power                     := FieldByName('Xmit_Power').AsSingle;
              TARH_Jamming_A_Probability     := FieldByName('TARH_Jamming_A_Probability').AsSingle;
              TARH_Jamming_B_Probability     := FieldByName('TARH_Jamming_B_Probability').AsSingle;
              TARH_Jamming_C_Probability     := FieldByName('TARH_Jamming_C_Probability').AsSingle;
              Wpt_Capable                    := FieldByName('Wpt_Capable').AsInteger;
              Max_Num_Wpts                   := FieldByName('Max_Num_Wpts').AsInteger;
              Min_Final_Leg_Length           := FieldByName('Min_Final_Leg_Length').AsSingle;
              Engagement_Range               := FieldByName('Engagement_Range').AsSingle;
              Max_Firing_Depth               := FieldByName('Max_Firing_Depth').AsSingle;
              Upper_Received_Freq            := FieldByName('Upper_Received_Freq').AsFloat;
              Lower_Received_Freq            := FieldByName('Lower_Received_Freq').AsFloat;
              Fly_Out_Required               := FieldByName('Fly_Out_Required').AsInteger;
              Fly_Out_Range                  := FieldByName('Fly_Out_Range').AsSingle;
              Fly_Out_Altitude               := FieldByName('Fly_Out_Altitude').AsSingle;
              Booster_Separation_Required    := FieldByName('Booster_Separation_Required').AsInteger;
              Booster_Separation_Range       := FieldByName('Booster_Separation_Range').AsSingle;
              Booster_Separation_Box_Width   := FieldByName('Booster_Separation_Box_Width').AsSingle;
              Booster_Separation_Box_Length  := FieldByName('Booster_Separation_Box_Length').AsSingle;
              Term_Guide_Azimuth             := FieldByName('Term_Guide_Azimuth').AsSingle;
              Term_Guide_Elevation           := FieldByName('Term_Guide_Elevation').AsSingle;
              Term_Guide_Range               := FieldByName('Term_Guide_Range').AsSingle;
              Terminal_Guidance_Capability   := FieldByName('Terminal_Guidance_Capability').AsInteger;
              Terminal_Altitude_Required     := FieldByName('Terminal_Altitude_Required').AsInteger;
              Terminal_Acquisition_Altitude  := FieldByName('Terminal_Acquisition_Altitude').AsSingle;
              Terminal_Sinuation_Start_Range := FieldByName('Terminal_Sinuation_Start_Range').AsSingle;
              Terminal_Sinuation_Period      := FieldByName('Terminal_Sinuation_Period').AsSingle;
              Terminal_Sinuation_Amplitude   := FieldByName('Terminal_Sinuation_Amplitude').AsSingle;
              Terminal_Pop_Up_Range          := FieldByName('Terminal_Pop_Up_Range').AsSingle;
              Terminal_Pop_Up_Altitude       := FieldByName('Terminal_Pop_Up_Altitude').AsSingle;
              Mid_Course_Update_Mode         := FieldByName('Mid_Course_Update_Mode').AsInteger;
              Home_On_Jam_Type_A_Capable     := FieldByName('Home_On_Jam_Type_A_Capable').AsInteger;
              Home_On_Jam_Type_B_Capable     := FieldByName('Home_On_Jam_Type_B_Capable').AsInteger;
              Home_On_Jam_Type_C_Capable     := FieldByName('Home_On_Jam_Type_C_Capable').AsInteger;
              Launch_Method                  := FieldByName('Launch_Method').AsInteger;
              Data_Entry_Method              := FieldByName('Data_Entry_Method').AsInteger;
              Launch_Speed                   := FieldByName('Launch_Speed').AsInteger;
              Max_Target_Altitude_Delta      := FieldByName('Max_Target_Altitude_Delta').AsInteger;
              Term_Guide_Azimuth_Narrow      := FieldByName('Term_Guide_Azimuth_Narrow').AsSingle;
              Term_Guide_Elevation_Narrow    := FieldByName('Term_Guide_Elevation_Narrow').AsSingle;
              Term_Guide_Range_Narrow        := FieldByName('Term_Guide_Range_Narrow').AsSingle;
              Spot_Number                    := FieldByName('Spot_Number').AsInteger;
              ECCM_Type                      := FieldByName('ECCM_Type').AsInteger;
              ECM_Detonation                 := FieldByName('ECM_Detonation').AsInteger;
              ECM_Detection                  := FieldByName('ECM_Detection').AsInteger;
              Detectability_Type             := FieldByName('Detectability_Type').AsInteger;
              IRCM_Detonation                := FieldByName('IRCM_Detonation').AsInteger;
              IRCM_Detection                 := FieldByName('IRCM_Detection').AsInteger;
              Sea_State_Modelling_Capable    := FieldByName('Sea_State_Modelling_Capable').AsInteger;
              Torpedo_Index                  := FieldByName('Torpedo_Index').AsInteger;
            end;

            result := missDefinition;

          end;
        finally
          sp.Close;
          FreeAndNil(sp);
        end;
      end;
  end;
end;

function TDMLite.GetMotion_Characteristics(const motion_id: Integer;
  dict: TDBMotionCharacteristicsDict): TDBMotion_Characteristics;
var
  sp : TZStoredProc;
  motDefinition : TDBMotion_Characteristics;
  newData : Boolean;
begin
  result := nil;
  newData := False;

  { search in dict first }
  if not dict.ContainsKey(motion_id) then
    newData := True
  else
    result := dict.Items[motion_id];

  { new data add to dict }
  if newData then
  begin
    sp := createStoredProcedure('get_motion_info');
    if Assigned(sp) then
      with sp do
      begin
        Params.ParamByName('@index').AsInteger := motion_id;

        try
          Open;

          if not IsEmpty then
          begin
            First;

            if not dict.ContainsKey(motion_id) then
            begin
              motDefinition := TDBMotion_Characteristics.Create;
              dict.Add(motion_id,motDefinition);

              with motDefinition do
              begin
                Motion_Index              := motion_id;
                Motion_Identifier         := FieldByName('Motion_Identifier').AsString;
                Motion_Type               := FieldByName('Motion_Type').AsInteger;
                Max_Altitude              := FieldByName('Max_Altitude').AsSingle;
                Max_Depth                 := FieldByName('Max_Depth').AsSingle;
                Min_Ground_Speed          := FieldByName('Min_Ground_Speed').AsSingle;
                Cruise_Ground_Speed       := FieldByName('Cruise_Ground_Speed').AsSingle;
                High_Ground_Speed         := FieldByName('High_Ground_Speed').AsSingle;
                Max_Ground_Speed          := FieldByName('Max_Ground_Speed').AsSingle;
                Acceleration              := FieldByName('Acceleration').AsSingle;
                Deceleration              := FieldByName('Deceleration').AsSingle;
                Normal_Climb_Rate         := FieldByName('Normal_Climb_Rate').AsSingle;
                Max_Climb_Rate            := FieldByName('Max_Climb_Rate').AsSingle;
                Normal_Descent_Rate       := FieldByName('Normal_Descent_Rate').AsSingle;
                Max_Descent_Rate          := FieldByName('Max_Descent_Rate').AsSingle;
                Vertical_Accel            := FieldByName('Vertical_Accel').AsSingle;
                Standard_Turn_Rate        := FieldByName('Standard_Turn_Rate').AsSingle;
                Tight_Turn_Rate           := FieldByName('Tight_Turn_Rate').AsSingle;
                Max_Helm_Angle            := FieldByName('Max_Helm_Angle').AsSingle;
                Helm_Angle_Rate           := FieldByName('Helm_Angle_Rate').AsSingle;
                Speed_Reduce_In_Turn      := FieldByName('Speed_Reduce_In_Turn').AsSingle;
                Time_To_Reduce_Speed      := FieldByName('Time_To_Reduce_Speed').AsSingle;
                Min_Speed_To_Reduce       := FieldByName('Min_Speed_To_Reduce').AsSingle;
                Rate_of_Turn_Rate_Chg     := FieldByName('Rate_of_Turn_Rate_Chg').AsSingle;
                Min_Pitch_Angle           := FieldByName('Min_Pitch_Angle').AsSingle;
                Max_Pitch_Angle           := FieldByName('Max_Pitch_Angle').AsSingle;
                Max_Roll_Angle            := FieldByName('Max_Roll_Angle').AsSingle;
                Endurance_Type            := FieldByName('Endurance_Type').AsInteger;
                Endurance_Time            := FieldByName('Endurance_Time').AsInteger;
                Max_Effective_Range       := FieldByName('Max_Effective_Range').AsSingle;
                Fuel_Unit_Type            := FieldByName('Fuel_Unit_Type').AsInteger;
                Max_Fuel_Capacity         := FieldByName('Max_Fuel_Capacity').AsSingle;
                Min_Speed_Fuel_Consume    := FieldByName('Min_Speed_Fuel_Consume').AsFloat;
                Cruise_Speed_Fuel_Consume := FieldByName('Cruise_Speed_Fuel_Consume').AsFloat;
                High_Speed_Fuel_Consume   := FieldByName('High_Speed_Fuel_Consume').AsFloat;
                Max_Speed_Fuel_Consume    := FieldByName('Max_Speed_Fuel_Consume').AsFloat;
              end;
            end;
          end;
          result := motDefinition;
        finally
          sp.Close;
          FreeAndNil(sp);
        end;
      end;
  end;
end;

function TDMLite.getPlatFormInstances(const alloc_ID: Integer;
  var aRec: TDBPlatformInstanceDict): Integer;
var
  sp, spdef : TZStoredProc;
  pf : TDBPlatformInstance;
begin

  result := 0;
  if not ZConn.Connected then
    exit;

  pf := nil;

  sp := createStoredProcedure('get_platform_instances');
  if Assigned(sp) then
    with sp do
    begin

      Params.ParamByName('@alloc').AsInteger := alloc_ID;
      try
        Open;

        result := RecordCount ;
        if not IsEmpty then
        begin
          First;

          while not EOF do
          begin
            pf := TDBPlatformInstance.Create;

            with pf do begin
              Platform_Instance_Index := FieldByName('Platform_Instance_Index').AsInteger;
              Platform_Index          := FieldByName('Platform_Index').AsInteger;
              Platform_Type           := FieldByName('Platform_Type').AsInteger;
              Instance_Name           := FieldByName('Instance_Name').AsString;
              Track_ID                := FieldByName('Track_ID').AsString;
              ForceDesignation        := FieldByName('Force_Designation').AsInteger;

              aRec.Add(Platform_Instance_Index,pf);
            end;

            Next;
          end;

        end;
      finally
        sp.Close;
        FreeAndNil(sp);
      end;
    end;
end;

function TDMLite.GetPlatform_Activation(const deploy_id,
  pf_inst_id: integer; dict : TDBPlatformActivationDict): TDBPlatform_Activation;
var
  sp : TZStoredProc;
  pfAct : TDBPlatform_Activation;
  eventId : integer;
  newData : Boolean;
begin
  result  := nil;
  newData := False;

  { search in dict first }
  if not dict.ContainsKey(pf_inst_id) then
    newData := True
  else
    result := dict.Items[pf_inst_id];

  if not ZConn.Connected then
    exit;

  { new data add to dict }
  if newData then
  begin
    sp := createStoredProcedure('get_activation_info');
    if Assigned(sp) then
      with sp do
      begin
        Params.ParamByName('@deploy').AsInteger := deploy_id;
        Params.ParamByName('@instance').AsInteger := pf_inst_id;

        try
          Open;

          if not IsEmpty then
          begin
            First;

            if not dict.ContainsKey(pf_inst_id) then
            begin
              pfAct := TDBPlatform_Activation.Create;
              dict.Add(pf_inst_id,pfAct);

              with pfAct do
              begin
                Platform_Event_Index        := FieldByName('Platform_Event_Index').AsInteger;
                Deployment_Index            := deploy_id ;
                Platform_Instance_Index     := pf_inst_id;
                Platform_Activation_Time    := FieldByName('Platform_Activation_Time').AsInteger;
                Init_Guidance_Type          := FieldByName('Init_Guidance_Type').AsInteger;
                Init_Position_Latitude      := FieldByName('Init_Position_Latitude').AsFloat;
                Init_Position_Longitude     := FieldByName('Init_Position_Longitude').AsFloat;
                Init_Position_Cartesian_X   := FieldByName('Init_Position_Cartesian_X').AsSingle;
                Init_Position_Cartesian_Y   := FieldByName('Init_Position_Cartesian_Y').AsSingle;
                Init_Altitude               := FieldByName('Init_Altitude').AsSingle;
                Init_Course                 := FieldByName('Init_Course').AsSingle;
                Init_Helm_Angle             := FieldByName('Init_Helm_Angle').AsSingle;
                Init_Ground_Speed           := FieldByName('Init_Ground_Speed').AsInteger;
                Init_Vertical_Speed         := FieldByName('Init_Vertical_Speed').AsInteger;
                Init_Command_Altitude       := FieldByName('Init_Command_Altitude').AsSingle;
                Init_Command_Course         := FieldByName('Init_Command_Course').AsSingle;
                Init_Command_Helm_Angle     := FieldByName('Init_Command_Helm_Angle').AsSingle;
                Init_Command_Ground         := FieldByName('Init_Command_Ground').AsInteger;
                Init_Command_Vert           := FieldByName('Init_Command_Vert').AsInteger;
                Deg_of_Rotation             := FieldByName('Deg_of_Rotation').AsSingle;
                Radius_of_Travel            := FieldByName('Radius_of_Travel').AsSingle;
                Direction_of_Travel         := FieldByName('Direction_of_Travel').AsInteger;
                Circle_Latitude             := FieldByName('Circle_Latitude').AsFloat;
                Circle_Longitude            := FieldByName('Circle_Longitude').AsFloat;
                Circle_X                    := FieldByName('Circle_X').AsSingle;
                Circle_Y                    := FieldByName('Circle_Y').AsSingle;
                Dynamic_Circle_Range_Offset := FieldByName('Dynamic_Circle_Range_Offset').AsSingle;
                Dynamic_Circle_Angle_Offset := FieldByName('Dynamic_Circle_Angle_Offset').AsInteger;
                Dynamic_Circle_Offset_Mode  := FieldByName('Dynamic_Circle_Offset_Mode').AsInteger;
                Period_Distance             := FieldByName('Period_Distance').AsSingle;
                Amplitude_Distance          := FieldByName('Amplitude_Distance').AsSingle;
                Zig_Zag_Leg_Type            := FieldByName('Zig_Zag_Leg_Type').AsInteger;
                Target_Angle_Offset         := FieldByName('Target_Angle_Offset').AsSingle;
                Target_Angle_Type           := FieldByName('Target_Angle_Type').AsInteger;
                Target_Range                := FieldByName('Target_Range').AsSingle;
                Guidance_Target             := FieldByName('Guidance_Target').AsInteger;
                Pattern_Instance_Index      := FieldByName('Pattern_Instance_Index').AsInteger;
                Angular_Offset              := FieldByName('Angular_Offset').AsSingle;
                Anchor_Cartesian_X          := FieldByName('Anchor_Cartesian_X').AsSingle;
                Anchor_Cartesian_Y          := FieldByName('Anchor_Cartesian_Y').AsSingle;
                Anchor_Latitude             := FieldByName('Anchor_Latitude').AsSingle;
                Anchor_Longitude            := FieldByName('Anchor_Longitude').AsSingle;
                Current_Drift               := FieldByName('Current_Drift').AsInteger;
                Waypoint_Termination        := FieldByName('Waypoint_Termination').AsInteger;
                Termination_Heading         := FieldByName('Termination_Heading').AsSingle;
                Cond_List_Instance_Index    := FieldByName('Cond_List_Instance_Index').AsInteger;
                Damage                      := FieldByName('Damage').AsSingle;
              end;

              result := pfAct;
            end;
          end;
        finally
          sp.Close;
          FreeAndNil(sp);
        end;
      end;
  end;
end;

function TDMLite.GetPointEffectOnBoard(const veh_id: integer;
  dict: TDBPointEffectOnBoardDict): TDBPointEffectOnBoardList;
var
  sp : TZStoredProc;
  pefOnBoard : TDBPoint_Effect_On_Board;
  pefOnBoardList : TDBPointEffectOnBoardList;
  newData : Boolean;
begin
  result := nil;
  newData := False;

  { search in dict first }
  if not dict.ContainsKey(veh_id) then
    newData := True
  else
    result := dict.Items[veh_id];

  { new data add to dict }
  if newData then
  begin
    sp := createStoredProcedure('get_point_effect_instances');
    if Assigned(sp) then
      with sp do
      begin
        Params.ParamByName('@vehicle').AsInteger := veh_id;

        try
          Open;

          if not IsEmpty then
          begin
            First;

            pefOnBoardList := TDBPointEffectOnBoardList.Create;
            dict.Add(veh_id,pefOnBoardList);

            while not EOF do
            begin
              pefOnBoard := TDBPoint_Effect_On_Board.Create;

              with pefOnBoard do
              begin
                Point_Effect_Index  := FieldByName('Point_Effect_Index').AsInteger;
                Instance_Identifier := FieldByName('Instance_Identifier').AsString;
                Mount_Type          := FieldByName('Mount_Type').AsInteger;
                Instance_Type       := FieldByName('Instance_Type').AsInteger;
                Quantity            := FieldByName('Quantity').AsInteger;
                Weapon_Index        := FieldByName('Weapon_Index').AsInteger;
              end;

              pefOnBoardList.Add(pefOnBoard);

              Next;
            end;

            result := pefOnBoardList;
          end;
        finally
          sp.Close;
          FreeAndNil(sp);
        end;
      end;
  end;
end;

function TDMLite.GetRadarDefinition(const rdr_id: integer;
  dict: TDBRadarDefinitionDict): TDBRadar_Definition;
var
  sp : TZStoredProc;
  radarDefinition : TDBRadar_Definition;
  newData : Boolean;
begin
  result := nil;
  newData := False;

  { search in dict first }
  if not dict.ContainsKey(rdr_id) then
    newData := True
  else
    result := dict.Items[rdr_id];

  { new data add to dict }
  if newData then
  begin
    sp := createStoredProcedure('get_radar_info');
    if Assigned(sp) then
      with sp do
      begin
        Params.ParamByName('@index').AsInteger := rdr_id;

        try
          Open;

          if not IsEmpty then
          begin
            First;

            radarDefinition := TDBRadar_Definition.Create;
            dict.Add(rdr_id,radarDefinition);

            with radarDefinition do
            begin
              Radar_Index                  := rdr_id;
              Radar_Identifier             := FieldByName('Radar_Identifier').AsString;
              Radar_Emitter                := FieldByName('Radar_Emitter').AsString;
              Radar_Type                   := FieldByName('Radar_Type').AsInteger;
              Frequency                    := FieldByName('Frequency').AsSingle;
              Scan_Rate                    := FieldByName('Scan_Rate').AsSingle;
              Pulse_Rep_Freq               := FieldByName('Pulse_Rep_Freq').AsSingle;
              Pulse_Width                  := FieldByName('Pulse_Width').AsSingle;
              Radar_Power                  := FieldByName('Radar_Power').AsSingle;
              Detection_Range              := FieldByName('Detection_Range').AsSingle;
              Known_Cross_Section          := FieldByName('Known_Cross_Section').AsSingle;
              Max_Unambig_Detect_Range     := FieldByName('Max_Unambig_Detect_Range').AsSingle;
              IFF_Capability               := FieldByName('IFF_Capability').AsBoolean;
              Altitude_Data_Capability     := FieldByName('Altitude_Data_Capability').AsBoolean;
              Ground_Speed_Data_Capability := FieldByName('Ground_Speed_Data_Capability').AsBoolean;
              Heading_Data_Capability      := FieldByName('Heading_Data_Capability').AsBoolean;
              Plat_Type_Recog_Capability   := FieldByName('Plat_Type_Recog_Capability').AsBoolean;
              Plat_Class_Recog_Capability  := FieldByName('Plat_Class_Recog_Capability').AsBoolean;
              Clutter_Rejection            := FieldByName('Clutter_Rejection').AsSingle;
              Anti_Jamming_Capable         := FieldByName('Anti_Jamming_Capable').AsBoolean;
              Curve_Definition_Index       := FieldByName('Curve_Definition_Index').AsInteger;
              Second_Vert_Coverage         := FieldByName('Second_Vert_Coverage').AsBoolean;
              Jamming_A_Resistant          := FieldByName('Jamming_A_Resistant').AsBoolean;
              Jamming_B_Resistant          := FieldByName('Jamming_B_Resistant').AsBoolean;
              Jamming_C_Resistant          := FieldByName('Jamming_C_Resistant').AsBoolean;
              Anti_Jamming_A_Resistant     := FieldByName('Anti_Jamming_A_Resistant').AsBoolean;
              Anti_Jamming_B_Resistant     := FieldByName('Anti_Jamming_B_Resistant').AsBoolean;
              Anti_Jamming_C_Resistant     := FieldByName('Anti_Jamming_C_Resistant').AsBoolean;
              Anti_Jamming_Range_Reduction := FieldByName('Anti_Jamming_Range_Reduction').AsSingle;
              Beam_Width                   := FieldByName('Beam_Width').AsSingle;
              Sector_Scan_Capable          := FieldByName('Sector_Scan_Capable').AsBoolean;
              Off_Axis_Jammer_Reduction    := FieldByName('Off_Axis_Jammer_Reduction').AsSingle;
              Num_FCR_Channels             := FieldByName('Num_FCR_Channels').AsInteger;
              Radar_Spot_Number            := FieldByName('Radar_Spot_Number').AsInteger;
              Radar_Horizon_Factor         := FieldByName('Radar_Horizon_Factor').AsSingle;
              Main_Lobe_Gain               := FieldByName('Main_Lobe_Gain').AsSingle;
              Counter_Detection_Factor     := FieldByName('Counter_Detection_Factor').AsSingle;
              ECCM_Type                    := FieldByName('ECCM_Type').AsInteger;
              MTI_Capable                  := FieldByName('MTI_Capable').AsBoolean;
              MTI_MinTargetSpeed           := FieldByName('MTI_MinTargetSpeed').AsSingle;
            end;

            result := radarDefinition;
          end;
        finally
          sp.Close;
          FreeAndNil(sp);
        end;
      end;
  end;
end;

function TDMLite.GetRadarOnBoard(const veh_id: integer;
  dict: TDBRadarOnBoardDict): TDBRadarOnBoardList;
var
  sp : TZStoredProc;
  radarOnBoard : TDBRadar_On_Board;
  radarOnBoardList : TDBRadarOnBoardList;
  newData : Boolean;
begin
  result := nil;
  newData := False;

  { search in dict first }
  if not dict.ContainsKey(veh_id) then
    newData := True
  else
    result := dict.Items[veh_id];

  { new data add to dict }
  if newData then
  begin
    sp := createStoredProcedure('get_radar_instances');
    if Assigned(sp) then
      with sp do
      begin
        Params.ParamByName('@vehicle').AsInteger := veh_id;

        try
          Open;

          if not IsEmpty then
          begin
            First;

            radarOnBoardList := TDBRadarOnBoardList.Create;
            dict.Add(veh_id,radarOnBoardList);

            while not EOF do
            begin
              radarOnBoard := TDBRadar_On_Board.Create;

              with radarOnBoard do
              begin
                Radar_Instance_Index     := FieldByName('Radar_Instance_Index').AsInteger;
                Instance_Identifier      := FieldByName('Instance_Identifier').AsString;
                Instance_Type            := FieldByName('Instance_Type').AsInteger;
                Vehicle_Index            := veh_id;
                Radar_Index              := FieldByName('Radar_Index').AsInteger;
                Rel_Antenna_Height       := FieldByName('Rel_Antenna_Height').AsSingle;
                Submerged_Antenna_Height := FieldByName('Submerged_Antenna_Height').AsSingle;
                Max_Operational_Depth    := FieldByName('Max_Operational_Depth').AsSingle;
              end;

              radarOnBoardList.Add(radarOnBoard);

              Next;
            end;

            result := radarOnBoardList;
          end;
        finally
          sp.Close;
          FreeAndNil(sp);
        end;
      end;
  end;
end;

function TDMLite.GetResourceAlloc(const id: Integer;
  var rec: TDBResource_Allocation): boolean;
var
  sp : TZStoredProc;
begin
  result := false;
  if not ZConn.Connected then
    exit;

  sp := createStoredProcedure('get_allocation_info');
  if Assigned(sp) then
    with sp do
    begin
      Params.ParamByName('@index').AsInteger := id;

      try
        Open;

        result := RecordCount > 0;
        if not IsEmpty then
        begin
          First;

          if not Assigned(rec) then
            rec := TDBResource_Allocation.Create;

          with rec do
          begin
            Resource_Alloc_Index := id;
            Allocation_Identifier := FieldByName('Allocation_Identifier').AsString;
            Game_Enviro_Index := FieldByName('Game_Enviro_Index').AsInteger;
            Defaults_Index := FieldByName('Defaults_Index').AsInteger;
            Role_List_Index := FieldByName('Role_List_Index').AsInteger;
            Game_Start_Time := FieldByName('Game_Start_Time').AsFloat;
          end;
        end;
      finally
        sp.Close;
        FreeAndNil(sp);
      end;
      result := True;

    end;end;

function TDMLite.GetResource_Overlay_Mapping(const id: Integer;
  recList: TDBOverlayMappingList):boolean;
var
  sp      : TZStoredProc;
  overlay : TDBOverlay_Mapping;
begin
  result := false;

  if not ZConn.Connected then
    exit;

  sp := createStoredProcedure('get_gl_overlay_instances');
  if Assigned(sp) then
    with sp do
    begin
      Params.ParamByName('@resource').AsInteger := id;

      try
        Open;

        result := RecordCount > 0;
        if not IsEmpty then
        begin

          First;

          while not EOF do
          begin
            overlay := TDBOverlay_Mapping.Create;

            with overlay do
            begin
              Overlay_Instance_Index           := FieldByName('Overlay_Instance_Index').AsInteger;
              Overlay_Index                    := FieldByName('Overlay_Index').AsInteger;
              Overlay_Identifier               := FieldByName('Overlay_Identifier').AsString;
              Overlay_Filename                 := FieldByName('Overlay_Filename').AsString;
              Static_Overlay                   := FieldByName('Static_Overlay').AsInteger;
              Game_Area_Index                  := FieldByName('Game_Area_Index').AsInteger;
              domain                           := FieldByName('Domain').AsInteger;
              Force                            := FieldByName('Force_Designation').AsInteger;
            end;

            recList.Add(overlay);
            Next;
          end;
        end;
      finally
        sp.Close;
        FreeAndNil(sp);
      end;
      Result := True;
    end;
end;

function TDMLite.GetSatellite_Definition(const sat_id: integer;
  dict: TDBSatelliteDefinitionDict): TDBSatellite_Definition;
var
  sp : TZStoredProc;
  satDefinition : TDBSatellite_Definition;
  newData : Boolean;
begin
  result := nil;
  newData := False;

  { search in dict first }
  if not dict.ContainsKey(sat_id) then
    newData := True
  else
    result := dict.Items[sat_id];

  { new data add to dict }
  if newData then
  begin
    sp := createStoredProcedure('get_satellite_info');
    if Assigned(sp) then
      with sp do
      begin
        Params.ParamByName('@index').AsInteger := sat_id;

        try
          Open;

          if not IsEmpty then
          begin
            First;

            satDefinition := TDBSatellite_Definition.Create;
            dict.Add(sat_id,satDefinition);

            with satDefinition do
            begin
              Satellite_Index             := sat_id;
              Satellite_Identifier        := FieldByName('Satellite_Identifier').AsString;
              Platform_Domain             := FieldByName('Platform_Domain').AsInteger;
              Platform_Category           := FieldByName('Platform_Category').AsInteger;
              Platform_Type               := FieldByName('Platform_Type').AsInteger;
              Length                      := FieldByName('Length').AsSingle;
              Width                       := FieldByName('Width').AsSingle;
              Height                      := FieldByName('Height').AsSingle;
              Front_Radar_Cross           := FieldByName('Front_Radar_Cross').AsSingle;
              Side_Radar_Cross            := FieldByName('Side_Radar_Cross').AsSingle;
              Orbit_Period                := FieldByName('Orbit_Period').AsInteger;
              Detection_Range_Radius      := FieldByName('Detection_Range_Radius').AsSingle;
              Altitude                    := FieldByName('Altitude').AsSingle;
              Ground_Speed                := FieldByName('Ground_Speed').AsSingle;
              Plat_Type_Recog_Capability  := FieldByName('Plat_Type_Recog_Capability').AsInteger;
              Plat_Class_Recog_Capability := FieldByName('Plat_Class_Recog_Capability').AsInteger;
            end;
            result := satDefinition;
          end;
        finally
          sp.Close;
          FreeAndNil(sp);
        end;
      end;
  end;
end;


function TDMLite.GetScenario(const id: Integer;
  var rec: TDBScenario_Definition): boolean;
var
  sp : TZStoredProc;
begin
  sp := createStoredProcedure('get_scenario_info');
  if Assigned(sp) then
    with sp do
    begin

      Params.ParamByName('@index').AsInteger := id;

      try
        Open;

        result := RecordCount > 0;
        if not IsEmpty then
        begin
          First;

          if not Assigned(rec) then
            rec := TDBScenario_Definition.Create;

          with rec do
          begin
            Scenario_Index := id;
            Scenario_Identifier := FieldByName('Scenario_Identifier').AsString;
            Resource_Alloc_Index := FieldByName('Resource_Alloc_Index').AsInteger;
          end;
        end;
      finally
        sp.Close;
        sp.Free;
      end;
    end;
end;


function TDMLite.GetSonarDefinition(const snr_id: integer;
  dict: TDBSonarDefinitionDict): TDBSonar_Definition;
var
  sp : TZStoredProc;
  sonarDefinition : TDBSonar_Definition;
  newData : Boolean;
begin
  result := nil;
  newData := False;

  { search in dict first }
  if not dict.ContainsKey(snr_id) then
    newData := True
  else
    result := dict.Items[snr_id];

  { new data add to dict }
  if newData then
  begin
    sp := createStoredProcedure('get_gl_sonar_info');
    if Assigned(sp) then
      with sp do
      begin
        Params.ParamByName('@sonar').AsInteger := snr_id;

        try
          Open;

          if not IsEmpty then
          begin
            First;

            sonarDefinition := TDBSonar_Definition.Create;
            dict.Add(snr_id,sonarDefinition);

            with sonarDefinition do
            begin
              Sonar_Index                    := snr_id;
              Sonar_Identifier               := FieldByName('Sonar_Identifier').AsString;
              Sonar_Category_Index           := FieldByName('Sonar_Category_Index').AsInteger;
              Sonar_Classification           := FieldByName('Sonar_Classification').AsInteger;
              Passive_Int_Period             := FieldByName('Passive_Int_Period').AsInteger;
              Active_Int_Period              := FieldByName('Active_Int_Period').AsInteger;
              TIOW_Short_Range               := FieldByName('TIOW_Short_Range').AsSingle;
              TIOW_Medium_Range              := FieldByName('TIOW_Medium_Range').AsSingle;
              TIOW_Long_Range                := FieldByName('TIOW_Long_Range').AsSingle;
              Passive_Detect_Range           := FieldByName('Passive_Detect_Range').AsSingle;
              Active_Detect_Range            := FieldByName('Active_Detect_Range').AsSingle;
              Max_Detect_Range               := FieldByName('Max_Detect_Range').AsSingle;
              Known_Signal_Source            := FieldByName('Known_Signal_Source').AsSingle;
              Known_Cross_Section            := FieldByName('Known_Cross_Section').AsSingle;
              Sonar_Directivity_Index        := FieldByName('Sonar_Directivity_Index').AsSingle;
              Active_Operating_Power         := FieldByName('Active_Operating_Power').AsSingle;
              Active_Freq_of_Op              := FieldByName('Active_Freq_of_Op').AsSingle;
              Passive_Freq_of_Op             := FieldByName('Passive_Freq_of_Op').AsSingle;
              Max_Operating_Depth            := FieldByName('Max_Operating_Depth').AsSingle;
              Sonar_Depth_Rate_of_Change     := FieldByName('Sonar_Depth_Rate_of_Change').AsSingle;
              Depth_per_Speed                := FieldByName('Depth_per_Speed').AsSingle;
              Kinking_Processing             := FieldByName('Kinking_Processing').AsBoolean;
              Turn_Rate_2_Kink               := FieldByName('Turn_Rate_2_Kink').AsSingle;
              Time_2_Settle_Kinked           := FieldByName('Time_2_Settle_Kinked').AsInteger;
              Bearing_Processing             := FieldByName('Bearing_Processing').AsBoolean;
              Time_2_Resolve_Bearing         := FieldByName('Time_2_Resolve_Bearing').AsInteger;
              Passive_Processing             := FieldByName('Passive_Processing').AsBoolean;
              Target_Identification          := FieldByName('Target_Identification').AsBoolean;
              Time_2_Identify                := FieldByName('Time_2_Identify').AsInteger;
              Curve_Detection_Index          := FieldByName('Curve_Detection_Index').AsInteger;
              Track_Analysis                 := FieldByName('Track_Analysis').AsInteger;
              Time_2_Provide_Track           := FieldByName('Time_2_Provide_Track').AsInteger;
              Ownship_Increase_due_to_Active := FieldByName('Ownship_Increase_due_to_Active').AsSingle;
              Tow_Speed                      := FieldByName('Tow_Speed').AsSingle;
              Minimum_Depth                  := FieldByName('Minimum_Depth').AsSingle;
              Maximum_Tow_Speed              := FieldByName('Maximum_Tow_Speed').AsSingle;
              Maximum_Sonar_Speed            := FieldByName('Maximum_Sonar_Speed').AsSingle;
              Depth_Finding_Capable          := FieldByName('Depth_Finding_Capable').AsBoolean;
              Tracking_Capable               := FieldByName('Tracking_Capable').AsBoolean;
              Surface_Detection_Capable      := FieldByName('Surface_Detection_Capable').AsBoolean;
              SubSurface_Detection_Capable   := FieldByName('SubSurface_Detection_Capable').AsBoolean;
              Torpedo_Detection_Capable      := FieldByName('Torpedo_Detection_Capable').AsBoolean;
              Mine_Detection_Capable         := FieldByName('Mine_Detection_Capable').AsBoolean;
              Cable_Length                   := FieldByName('Cable_Length').AsSingle;
              Maximum_Reported_Bearing_Error := FieldByName('Maximum_Reported_Bearing_Error').AsSingle;
              Average_Beam_Width             := FieldByName('Average_Beam_Width').AsSingle;
              Counter_Detection_Factor       := FieldByName('Counter_Detection_Factor').AsSingle;
            end;

            result := sonarDefinition;
          end;
        finally
          sp.Close;
          FreeAndNil(sp);
        end;
      end;
  end;
end;

function TDMLite.GetSonarOnBoard(const veh_id: integer;
  dict: TDBSonarOnBoardDict): TDBSonarOnBoardList;
var
  sp : TZStoredProc;
  sonarOnBoard : TDBSonar_On_Board;
  sonarOnBoardList : TDBSonarOnBoardList;
  newData : Boolean;
begin
  result := nil;
  newData := False;

  { search in dict first }
  if not dict.ContainsKey(veh_id) then
    newData := True
  else
    result := dict.Items[veh_id];

  { new data add to dict }
  if newData then
  begin
    sp := createStoredProcedure('get_sonar_instances');
    if Assigned(sp) then
      with sp do
      begin
        Params.ParamByName('@vehicle').AsInteger := veh_id;

        try
          Open;

          if not IsEmpty then
          begin
            First;

            sonarOnBoardList := TDBSonarOnBoardList.Create;
            dict.Add(veh_id,sonarOnBoardList);

            while not EOF do
            begin
              sonarOnBoard := TDBSonar_On_Board.Create;

              with sonarOnBoard do
              begin
                Sonar_Instance_Index := FieldByName('Sonar_Instance_Index').AsInteger;
                Instance_Identifier  := FieldByName('Instance_Identifier').AsString;
                Instance_Type        := FieldByName('Instance_Type').AsInteger;
                Vehicle_Index        := veh_id;
                Sonar_Index          := FieldByName('Sonar_Index').AsInteger;
                Minimum_Depth        := FieldByName('Minimum_Depth').AsSingle;
                Time_2_Deploy        := FieldByName('Time_2_Deploy').AsInteger;
                Time_2_Stow          := FieldByName('Time_2_Stow').AsInteger;
              end;

              sonarOnBoardList.Add(sonarOnBoard);

              Next;
            end;

            result := sonarOnBoardList;
          end;
        finally
          sp.Close;
          FreeAndNil(sp);
        end;
      end;
  end;
end;

function TDMLite.GetSonarPODCurve(curveIndex: integer;
  dict: TDBSonar_POD_CurveDict): TDBSonar_POD_CurveList;
var
  sp         : TZStoredProc;
  pod        : TDBPOD_vs_SNR_Point;
  pods       : TDBSonar_POD_CurveList;
  newData    : Boolean;
begin
  result := nil;
  newData := False;

  { search in dict first }
  if not dict.ContainsKey(curveIndex) then
    newData := True
  else
    result := dict.Items[curveIndex];

  { new data add to dict }
  if newData then
  begin
    sp := createStoredProcedure('get_gl_pod_snr_point_instances');
    if Assigned(sp) then
      with sp do
      begin
        Params.ParamByName('@curve').AsInteger := curveIndex;

        try
          Open;

          if not IsEmpty then
          begin
            First;

            pods := TDBSonar_POD_CurveList.Create;
            dict.Add(curveIndex,pods);

            while not EOF do
            begin
              pod := TDBPOD_vs_SNR_Point.Create;

              with pod do
              begin
                Curve_Definition_Index := curveIndex;
                SNR_Ratio              := FieldByName('SNR_Ratio').AsFloat;
                Prob_of_Detection      := FieldByName('Prob_of_Detection').AsFloat;
              end;

              pods.Add(pod);

              Next;
            end;

            result := pods;
          end;
        finally
          sp.Close;
          FreeAndNil(sp);
        end;
      end;
  end;
end;

function TDMLite.GetSonobuoyOnBoard(const veh_id: integer;
  dict: TDBSonobuoyOnBoardDict): TDBSonobuoyOnBoardList;
var
  sp               : TZStoredProc;
  devOnBoard       : TDBSonobuoy_On_Board;
  devOnBoardList   : TDBSonobuoyOnBoardList;
  newData          : Boolean;
begin
  result := nil;
  newData := False;

  { search in dict first }
  if not dict.ContainsKey(veh_id) then
    newData := True
  else
    result := dict.Items[veh_id];

  { new data add to dict }
  if newData then
  begin
    sp := createStoredProcedure('get_tow_jam_instances');
    if Assigned(sp) then
      with sp do
      begin
        Params.ParamByName('@vehicle').AsInteger := veh_id;

        try
          Open;

          if not IsEmpty then
          begin
            First;

            devOnBoardList := TDBSonobuoyOnBoardList.Create;
            dict.Add(veh_id,devOnBoardList);

            while not EOF do
            begin
              devOnBoard := TDBSonobuoy_On_Board.Create;

              with devOnBoard do
              begin
                Sonobuoy_Instance_Index := FieldByName('Sonobuoy_Instance_Index').AsInteger;
                Instance_Identifier     := FieldByName('Instance_Identifier').AsString;
                Instance_Type           := FieldByName('Instance_Type').AsInteger;
                Sonobuoy_Index          := FieldByName('Sonobuoy_Index').AsInteger;
                Quantity                := FieldByName('Quantity').AsInteger;
                Sonar_Instance_Index    := FieldByName('Sonar_Instance_Index').AsInteger;
              end;

              devOnBoardList.Add(devOnBoard);

              Next;
            end;

            result := devOnBoardList;
          end;
        finally
          sp.Close;
          FreeAndNil(sp);
        end;
      end;
  end;
end;

function TDMLite.GetSonobuoy_Definition(const sono_id: integer;
  dict: TDBSonobuoyDefinitionDict): TDBSonobuoy_Definition;
var
  sp : TZStoredProc;
  sonoDefinition : TDBSonobuoy_Definition;
  newData : Boolean;
begin
  result := nil;
  newData := False;

  { search in dict first }
  if not dict.ContainsKey(sono_id) then
    newData := True
  else
    result := dict.Items[sono_id];

  { new data add to dict }
  if newData then
  begin
    sp := createStoredProcedure('get_sonobuoy_info');
    if Assigned(sp) then
      with sp do
      begin
        Params.ParamByName('@index').AsInteger := sono_id;

        try
          Open;

          if not IsEmpty then
          begin
            First;

            sonoDefinition := TDBSonobuoy_Definition.Create;
            dict.Add(sono_id,sonoDefinition);

            with sonoDefinition do
            begin
              Sonobuoy_Index        := sono_id;
              Class_Identifier      := FieldByName('Class_Identifier').AsString;
              Sonobuoy_Type         := FieldByName('Sonobuoy_Type').AsInteger;
              Platform_Domain       := FieldByName('Platform_Domain').AsInteger;
              Platform_Category     := FieldByName('Platform_Category').AsInteger;
              Platform_Type         := FieldByName('Platform_Type').AsInteger;
              Endurance_Time        := FieldByName('Endurance_Time').AsInteger;
              Max_Depth             := FieldByName('Max_Depth').AsSingle;
              Length                := FieldByName('Length').AsSingle;
              Width                 := FieldByName('Width').AsSingle;
              Height                := FieldByName('Height').AsSingle;
              Front_Acoustic_Cross  := FieldByName('Front_Acoustic_Cross').AsSingle;
              Side_Acoustic_Cross   := FieldByName('Side_Acoustic_Cross').AsSingle;
              Damage_Capacity       := FieldByName('Damage_Capacity').AsInteger;
              CPA_Detection_Capable := FieldByName('CPA_Detection_Capable').AsInteger;
              CPA_Range_Limit       := FieldByName('CPA_Range_Limit').AsSingle;
              Sonar_Index           := FieldByName('Sonar_Index').AsInteger;
            end;

            result := sonoDefinition;
          end;
        finally
          sp.Close;
          FreeAndNil(sp);
        end;
      end;
  end;
end;

function TDMLite.GetSubArea_Enviro_Definition(const id: Integer;
  var rectList: TDBSubAreaDefinitionList): boolean;
var
  rec: TDBSubArea_Enviro_Definition;
  sp : TZStoredProc;
begin
  result := false;

  if not ZConn.Connected then
    exit;

  sp := createStoredProcedure('get_sub_area_info');
  if Assigned(sp) then
    with sp do
    begin
      Params.ParamByName('@enviro').AsInteger := id;

      try
        Open;

        result := RecordCount > 0;
        if not Assigned(rectList) then
          rectList.Create
        else
          rectList.Clear;

        if not IsEmpty then
        begin
          First;

          while not Eof do
          begin
            rec := TDBSubArea_Enviro_Definition.Create;

            with rec do
            begin
              Enviro_Index                 := id;
              Game_Enviro_Index            := FieldByName('Game_Enviro_Index').AsInteger;
              Enviro_Identifier            := FieldByName('Enviro_Identifier').AsString;
              X_Position_1                 := FieldByName('X_Position_1').AsSingle;
              Y_Position_1                 := FieldByName('Y_Position_1').AsSingle;
              X_Position_2                 := FieldByName('X_Position_2').AsSingle;
              Y_Position_2                 := FieldByName('Y_Position_2').AsSingle;
              Latitude_1                   := FieldByName('Latitude_1').AsFloat;
              Longitude_1                  := FieldByName('Longitude_1').AsFloat;
              Latitude_2                   := FieldByName('Latitude_2').AsFloat;
              Longitude_2                  := FieldByName('Longitude_2').AsFloat;
              Wind_Speed                   := FieldByName('Wind_Speed').AsSingle;
              Wind_Direction               := FieldByName('Wind_Direction').AsSingle;
              Daytime_Visual_Modifier      := FieldByName('Daytime_Visual_Modifier').AsSingle;
              Nighttime_Visual_Modifier    := FieldByName('Nighttime_Visual_Modifier').AsSingle;
              Daytime_Infrared_Modifier    := FieldByName('Daytime_Infrared_Modifier').AsSingle;
              Nighttime_Infrared_Modifier  := FieldByName('Nighttime_Infrared_Modifier').AsSingle;
              Rain_Rate                    := FieldByName('Rain_Rate').AsInteger;
              Cloud_Base_Height            := FieldByName('Cloud_Base_Height').AsSingle;
              Cloud_Attenuation            := FieldByName('Cloud_Attenuation').AsInteger;
              Sea_State                    := FieldByName('Sea_State').AsInteger;
              Ocean_Current_Speed          := FieldByName('Ocean_Current_Speed').AsSingle;
              Ocean_Current_Direction      := FieldByName('Ocean_Current_Direction').AsSingle;
              Thermal_Layer_Depth          := FieldByName('Thermal_Layer_Depth').AsSingle;
              Sound_Velocity_Type          := FieldByName('Sound_Velocity_Type').AsInteger;
              Surface_Sound_Speed          := FieldByName('Surface_Sound_Speed').AsSingle;
              Layer_Sound_Speed            := FieldByName('Layer_Sound_Speed').AsSingle;
              Bottom_Sound_Speed           := FieldByName('Bottom_Sound_Speed').AsSingle;
              Bottomloss_Coefficient       := FieldByName('Bottomloss_Coefficient').AsInteger;
              Ave_Ocean_Depth              := FieldByName('Ave_Ocean_Depth').AsSingle;
              CZ_Active                    := FieldByName('CZ_Active').AsInteger;
              Surface_Ducting_Active       := FieldByName('Surface_Ducting_Active').AsInteger;
              Upper_Limit_Sur_Duct_Depth   := FieldByName('Upper_Limit_Sur_Duct_Depth').AsSingle;
              Lower_Limit_Sur_Duct_Depth   := FieldByName('Lower_Limit_Sur_Duct_Depth').AsSingle;
              Sub_Ducting_Active           := FieldByName('Sub_Ducting_Active').AsInteger;
              Upper_Limit_Sub_Duct_Depth   := FieldByName('Upper_Limit_Sub_Duct_Depth').AsSingle;
              Lower_Limit_Sub_Duct_Depth   := FieldByName('Lower_Limit_Sub_Duct_Depth').AsSingle;
              Shipping_Rate                := FieldByName('Shipping_Rate').AsInteger;
              Shadow_Zone_Trans_Loss       := FieldByName('Shadow_Zone_Trans_Loss').AsSingle;
              Atmospheric_Refract_Modifier := FieldByName('Atmospheric_Refract_Modifier').AsSingle;
              Barometric_Pressure          := FieldByName('Barometric_Pressure').AsSingle;
              Air_Temperature              := FieldByName('Air_Temperature').AsSingle;
              Surface_Temperature          := FieldByName('Surface_Temperature').AsSingle;
              HF_Black_Hole                := FieldByName('HF_Black_Hole').AsInteger;
            end;

            rectList.Add(rec);
            Next;
          end;
        end;
      finally
        sp.Close;
        sp.Free;
      end;
      result := False;
    end;
end;

function TDMLite.GetTorpedoPOH(torpIndex: integer;
  dict: TDBTorpedo_POH_ModifierDict): TDBTorpedo_POH_ModifierList;
var
  sp         : TZStoredProc;
  pod        : TDBTorpedo_POH_Modifier;
  pods       : TDBTorpedo_POH_ModifierList;
  newData    : Boolean;
begin
  result := nil;
  newData := False;

  { search in dict first }
  if not dict.ContainsKey(torpIndex) then
    newData := True
  else
    result := dict.Items[torpIndex];

  { new data add to dict }
  if newData then
  begin
    sp := createStoredProcedure('get_gl_mine_curve_instances');
    if Assigned(sp) then
      with sp do
      begin
        Params.ParamByName('@torpedo').AsInteger := torpIndex;

        try
          Open;

          if not IsEmpty then
          begin
            First;

            pods := TDBTorpedo_POH_ModifierList.Create;
            dict.Add(torpIndex,pods);

            while not EOF do
            begin
              pod := TDBTorpedo_POH_Modifier.Create;

              with pod do
              begin
                Torpedo_Index := FieldByName('Torpedo_Index').AsInteger;
                Aspect_Angle  := FieldByName('Aspect_Angle').AsInteger;
                POH_Modifier  := FieldByName('POH_Modifier').AsFloat;
              end;

              pods.Add(pod);

              Next;
            end;

            result := pods;
          end;
        finally
          sp.Close;
          FreeAndNil(sp);
        end;
      end;
  end;
end;

function TDMLite.GetTorpedo_Definition(const torp_id: integer;
  dict: TDBTorpedoDefinitionDict): TDBTorpedo_Definition;
var
  sp : TZStoredProc;
  torpDefinition : TDBTorpedo_Definition;
  newData : Boolean;
begin
  result  := nil;
  newData := False;

  { search in dict first }
  if not dict.ContainsKey(torp_id) then
    newData := True
  else
    result := dict.Items[torp_id];

  { new data add to dict }
  if newData then
  begin
    sp := createStoredProcedure('get_torpedo_info');
    if Assigned(sp) then
      with sp do
      begin
        Params.ParamByName('@index').AsInteger := torp_id;

        try
          Open;

          if not IsEmpty then
          begin
            First;

            torpDefinition := TDBTorpedo_Definition.Create;
            dict.Add(torp_id,torpDefinition);

            with torpDefinition do
            begin
              Torpedo_Index               := torp_id;
              Class_Identifier            := FieldByName('Class_Identifier').AsString;
              Platform_Domain             := FieldByName('Platform_Domain').AsInteger;
              Platform_Category           := FieldByName('Platform_Category').AsInteger;
              Platform_Type               := FieldByName('Platform_Type').AsInteger;
              Max_Range                   := FieldByName('Max_Range').AsSingle;
              Min_Range                   := FieldByName('Min_Range').AsSingle;
              Motion_Index                := FieldByName('Motion_Index').AsInteger;
              Seeker_TurnOn_Range         := FieldByName('Seeker_TurnOn_Range').AsSingle;
              Lethality                   := FieldByName('Lethality').AsInteger;
              Damage_Capacity             := FieldByName('Damage_Capacity').AsInteger;
              Default_Depth               := FieldByName('Default_Depth').AsSingle;
              Length                      := FieldByName('Length').AsSingle;
              Width                       := FieldByName('Width').AsSingle;
              Height                      := FieldByName('Height').AsSingle;
              Front_Acoustic_Cross        := FieldByName('Front_Acoustic_Cross').AsSingle;
              Side_Acoustic_Cross         := FieldByName('Side_Acoustic_Cross').AsSingle;
              LSpeed_Acoustic_Intens      := FieldByName('LSpeed_Acoustic_Intens').AsSingle;
              Below_Cav_Acoustic_Intens   := FieldByName('Below_Cav_Acoustic_Intens').AsSingle;
              Above_Cav_Acoustic_Intens   := FieldByName('Above_Cav_Acoustic_Intens').AsSingle;
              HSpeed_Acoustic_Intens      := FieldByName('HSpeed_Acoustic_Intens').AsSingle;
              Cavitation_Switch_Point     := FieldByName('Cavitation_Switch_Point').AsSingle;
              Term_Guide_Azimuth          := FieldByName('Term_Guide_Azimuth').AsSingle;
              Term_Guide_Elevation        := FieldByName('Term_Guide_Elevation').AsSingle;
              Term_Guide_Range            := FieldByName('Term_Guide_Range').AsSingle;
              Pursuit_Guidance_Type       := FieldByName('Pursuit_Guidance_Type').AsInteger;
              Air_Drop_Capable            := FieldByName('Air_Drop_Capable').AsInteger;
              Use_Terminal_Circle         := FieldByName('Use_Terminal_Circle').AsInteger;
              Terminal_Circle_Radius      := FieldByName('Terminal_Circle_Radius').AsSingle;
              Fixed_Circle_Radius         := FieldByName('Fixed_Circle_Radius').AsInteger;
              Lateral_Deceleration        := FieldByName('Lateral_Deceleration').AsSingle;
              Airborne_Descent_Rate       := FieldByName('Airborne_Descent_Rate').AsFloat;
              Wire_Angle_Offset           := FieldByName('Wire_Angle_Offset').AsSingle;
              Guidance_Type               := FieldByName('Guidance_Type').AsInteger;
              Anti_Sur_Capable            := FieldByName('Anti_Sur_Capable').AsInteger;
              Anti_SubSur_Capable         := FieldByName('Anti_SubSur_Capable').AsInteger;
              Primary_Target_Domain       := FieldByName('Primary_Target_Domain').AsInteger;
              Active_Acoustic_POH_Mod     := FieldByName('Active_Acoustic_POH_Mod').AsSingle;
              Passive_Acoustic_POH_Mod    := FieldByName('Passive_Acoustic_POH_Mod').AsSingle;
              Active_Passive_POH_Mod      := FieldByName('Active_Passive_POH_Mod').AsSingle;
              WireGuide_POH_Modifier      := FieldByName('WireGuide_POH_Modifier').AsSingle;
              WakeHome_POH_Modifier       := FieldByName('WakeHome_POH_Modifier').AsSingle;
              Active_Seeker_Power         := FieldByName('Active_Seeker_Power').AsSingle;
              Active_Seeker_Freq          := FieldByName('Active_Seeker_Freq').AsSingle;
              Engagement_Range            := FieldByName('Engagement_Range').AsSingle;
              First_Relative_Gyro_Angle   := FieldByName('First_Relative_Gyro_Angle').AsInteger;
              Second_Relative_Gyro_Angle  := FieldByName('Second_Relative_Gyro_Angle').AsInteger;
              Max_Torpedo_Gyro_Angle      := FieldByName('Max_Torpedo_Gyro_Angle').AsSingle;
              Max_Torpedo_Search_Depth    := FieldByName('Max_Torpedo_Search_Depth').AsSingle;
              Acoustic_Torp_Ceiling_Depth := FieldByName('Acoustic_Torp_Ceiling_Depth').AsSingle;
              Fixed_Ceiling_Depth         := FieldByName('Fixed_Ceiling_Depth').AsInteger;
              Fixed_Seeker_TurnOn_Range   := FieldByName('Fixed_Seeker_TurnOn_Range').AsInteger;
              Sinuation_Runout            := FieldByName('Sinuation_Runout').AsInteger;
              Runout_Sinuation_Period     := FieldByName('Runout_Sinuation_Period').AsSingle;
              Runout_Sinuation_Amplitude  := FieldByName('Runout_Sinuation_Amplitude').AsSingle;
              Min_Runout_Range            := FieldByName('Min_Runout_Range').AsSingle;
              Launch_Method               := FieldByName('Launch_Method').AsInteger;
              Data_Entry_Method           := FieldByName('Data_Entry_Method').AsInteger;
              Launch_Speed                := FieldByName('Launch_Speed').AsInteger;
              Opt_Launch_Range_Nuc_Sub    := FieldByName('Opt_Launch_Range_Nuc_Sub').AsSingle;
              Opt_Launch_Range_Conv_Sub   := FieldByName('Opt_Launch_Range_Conv_Sub').AsSingle;
              Opt_Launch_Range_Other      := FieldByName('Opt_Launch_Range_Other').AsSingle;
              Detectability_Type          := FieldByName('Detectability_Type').AsInteger;
            end;

            result := torpDefinition;
          end;
        finally
          sp.Close;
          FreeAndNil(sp);
        end;
      end;
  end;
end;

function TDMLite.GetTowedJammerDefinition(const jammer_id: integer;
  dict: TDBTowedJammerDecoyDefDict): TDBTowed_Jammer_Decoy_Definition;
var
  sp : TZStoredProc;
  jammerDefinition : TDBTowed_Jammer_Decoy_Definition;
  newData : Boolean;
begin
  result := nil;
  newData := False;

  { search in dict first }
  if not dict.ContainsKey(jammer_id) then
    newData := True
  else
    result := dict.Items[jammer_id];

  { new data add to dict }
  if newData then
  begin
    sp := createStoredProcedure('get_tow_jam_info');
    if Assigned(sp) then
      with sp do
      begin
        Params.ParamByName('@index').AsInteger := jammer_id;

        try
          Open;

          if not IsEmpty then
          begin
            First;

            jammerDefinition := TDBTowed_Jammer_Decoy_Definition.Create;
            dict.Add(jammer_id,jammerDefinition);

            with jammerDefinition do
            begin
              Towed_Decoy_Index        := jammer_id;
              Towed_Decoy_Identifier   := FieldByName('Towed_Decoy_Identifier').AsString;
              Decoy_TARH_Capable       := FieldByName('Decoy_TARH_Capable').AsInteger;
              Decoy_SARH_Capable       := FieldByName('Decoy_SARH_Capable').AsInteger;
              Platform_Domain          := FieldByName('Platform_Domain').AsInteger;
              Platform_Category        := FieldByName('Platform_Category').AsInteger;
              Platform_Type            := FieldByName('Platform_Type').AsInteger;
              Length                   := FieldByName('Length').AsSingle;
              Width                    := FieldByName('Width').AsSingle;
              Height                   := FieldByName('Height').AsSingle;
              Front_Radar_Cross        := FieldByName('Front_Radar_Cross').AsSingle;
              Side_Radar_Cross         := FieldByName('Side_Radar_Cross').AsSingle;
              Front_Visual_Cross       := FieldByName('Front_Visual_Cross').AsSingle;
              Side_Visual_Cross        := FieldByName('Side_Visual_Cross').AsSingle;
              Front_Acoustic_Cross     := FieldByName('Front_Acoustic_Cross').AsSingle;
              Side_Acoustic_Cross      := FieldByName('Side_Acoustic_Cross').AsSingle;
              Type_A_Seducing_Prob     := FieldByName('Type_A_Seducing_Prob').AsSingle;
              Type_B_Seducing_Prob     := FieldByName('Type_B_Seducing_Prob').AsSingle;
              Type_C_Seducing_Prob     := FieldByName('Type_C_Seducing_Prob').AsSingle;
              Activation_Control_Delay := FieldByName('Activation_Control_Delay').AsSingle;
              Tow_Length               := FieldByName('Tow_Length').AsSingle;
              ECM_Type                 := FieldByName('ECM_Type').AsInteger;
            end;

            result := jammerDefinition;
          end;
        finally
          sp.Close;
          FreeAndNil(sp);
        end;
      end;
  end;
end;

function TDMLite.GetTowedJammerOnBoard(const veh_id: integer;
  dict: TDBTowedJammerOnBoardDict): TDBTowedJammerOnBoardList;
var
  sp               : TZStoredProc;
  devOnBoard       : TDBTowed_Jammer_Decoy_On_Board;
  devOnBoardList   : TDBTowedJammerOnBoardList;
  newData          : Boolean;
begin
  result := nil;
  newData := False;

  { search in dict first }
  if not dict.ContainsKey(veh_id) then
    newData := True
  else
    result := dict.Items[veh_id];

  { new data add to dict }
  if newData then
  begin
    sp := createStoredProcedure('get_tow_jam_instances');
    if Assigned(sp) then
      with sp do
      begin
        Params.ParamByName('@vehicle').AsInteger := veh_id;

        try
          Open;

          if not IsEmpty then
          begin
            First;

            devOnBoardList := TDBTowedJammerOnBoardList.Create;
            dict.Add(veh_id,devOnBoardList);

            while not EOF do
            begin
              devOnBoard := TDBTowed_Jammer_Decoy_On_Board.Create;

              with devOnBoard do
              begin
                Towed_Decoy_Instance_Index := FieldByName('Towed_Decoy_Instance_Index').AsInteger;
                Instance_Identifier        := FieldByName('Instance_Identifier').AsString;
                Instance_Type              := FieldByName('Instance_Type').AsInteger;
                Quantity                   := FieldByName('Quantity').AsInteger;
                Towed_Decoy_Index          := FieldByName('Towed_Decoy_Index').AsInteger;
              end;

              devOnBoardList.Add(devOnBoard);

              Next;
            end;

            result := devOnBoardList;
          end;
        finally
          sp.Close;
          FreeAndNil(sp);
        end;
      end;
  end;
end;

function TDMLite.GetVehicle_Definition(const v_idx: Integer;
  dict: TDBVehicleDefinitionDict): TDBVehicle_Definition;
var
  sp : TZStoredProc;
  vehDefinition : TDBVehicle_Definition;
  newData : Boolean;
begin
  result := nil;
  newData := False;

  { search in dict first }
  if not dict.ContainsKey(v_idx) then
    newData := True
  else
    result := dict.Items[v_idx];

  { new data add to dict }
  if newData then
  begin
  sp := createStoredProcedure('get_gl_vehicle_info');

  if Assigned(sp) then
    with sp do
    begin
      Params.ParamByName('@vehicle').AsInteger := v_idx;

      try
        Open;

        if not IsEmpty then
        begin
          First;

          if not dict.ContainsKey(v_idx) then
          begin
            vehDefinition := TDBVehicle_Definition.Create;
            dict.Add(v_idx,vehDefinition);

            with vehDefinition do
            begin
              Vehicle_Index                  := v_idx;
              Vehicle_Identifier             := FieldByName('Vehicle_Identifier').AsString;
              Platform_Domain                := FieldByName('Platform_Domain').AsInteger;
              Platform_Category              := FieldByName('Platform_Category').AsInteger;
              Platform_Type                  := FieldByName('Platform_Type').AsInteger;
              Motion_Characteristics         := FieldByName('Motion_Characteristics').AsInteger;
              Length                         := FieldByName('Length').AsSingle;
              Width                          := FieldByName('Width').AsSingle;
              Height                         := FieldByName('Height').AsSingle;
              Draft                          := FieldByName('Draft').AsSingle;
              Front_Radar_Cross              := FieldByName('Front_Radar_Cross').AsSingle;
              Side_Radar_Cross               := FieldByName('Side_Radar_Cross').AsSingle;
              Front_Acoustic_Cross           := FieldByName('Front_Acoustic_Cross').AsSingle;
              Side_Acoustic_Cross            := FieldByName('Side_Acoustic_Cross').AsSingle;
              Magnetic_Cross                 := FieldByName('Magnetic_Cross').AsSingle;
              Front_Visual_EO_Cross          := FieldByName('Front_Visual_EO_Cross').AsSingle;
              Side_Visual_EO_Cross           := FieldByName('Side_Visual_EO_Cross').AsSingle;
              Front_Infrared_Cross           := FieldByName('Front_Infrared_Cross').AsSingle;
              Side_Infrared_Cross            := FieldByName('Side_Infrared_Cross').AsSingle;
              LSpeed_Acoustic_Intens         := FieldByName('LSpeed_Acoustic_Intens').AsSingle;
              Below_Cav_Acoustic_Intens      := FieldByName('Below_Cav_Acoustic_Intens').AsSingle;
              Above_Cav_Acoustic_Intens      := FieldByName('Above_Cav_Acoustic_Intens').AsSingle;
              HSpeed_Acoustic_Intens         := FieldByName('HSpeed_Acoustic_Intens').AsSingle;
              Cavitation_Speed_Switch        := FieldByName('Cavitation_Speed_Switch').AsSingle;
              Time_of_Weapon_Impact          := FieldByName('Time_of_Weapon_Impact').AsInteger;
              Chaff_Seduction_Capable        := FieldByName('Chaff_Seduction_Capable').AsBoolean;
              Seduction_Mode_Prob            := FieldByName('Seduction_Mode_Prob').AsSingle;
              Min_Delay_Between_Chaff_Rounds := FieldByName('Min_Delay_Between_Chaff_Rounds').AsInteger;
              Max_Chaff_Salvo_Size           := FieldByName('Max_Chaff_Salvo_Size').AsInteger;
              SARH_POH_Modifier              := FieldByName('SARH_POH_Modifier').AsSingle;
              CG_POH_Modifier                := FieldByName('CG_POH_Modifier').AsSingle;
              TARH_POH_Modifier              := FieldByName('TARH_POH_Modifier').AsSingle;
              IR_POH_Modifier                := FieldByName('IR_POH_Modifier').AsSingle;
              AR_POH_Modifier                := FieldByName('AR_POH_Modifier').AsSingle;
              Active_Acoustic_Tor_POH_Mod    := FieldByName('Active_Acoustic_Tor_POH_Mod').AsSingle;
              Passive_Acoustic_Tor_POH_Mod   := FieldByName('Passive_Acoustic_Tor_POH_Mod').AsSingle;
              Active_Passive_Tor_POH_Mod     := FieldByName('Active_Passive_Tor_POH_Mod').AsSingle;
              Wake_Home_POH_Modifier         := FieldByName('Wake_Home_POH_Modifier').AsSingle;
              Wire_Guide_POH_Modifier        := FieldByName('Wire_Guide_POH_Modifier').AsSingle;
              Mag_Mine_POH_Modifier          := FieldByName('Mag_Mine_POH_Modifier').AsSingle;
              Press_Mine_POH_Modifier        := FieldByName('Press_Mine_POH_Modifier').AsSingle;
              Impact_Mine_POH_Modifier       := FieldByName('Impact_Mine_POH_Modifier').AsSingle;
              Acoustic_Mine_POH_Modifier     := FieldByName('Acoustic_Mine_POH_Modifier').AsSingle;
              Sub_Comm_Antenna_Height        := FieldByName('Sub_Comm_Antenna_Height').AsSingle;
              Rel_Comm_Antenna_Height        := FieldByName('Rel_Comm_Antenna_Height').AsSingle;
              Max_Comm_Operating_Depth       := FieldByName('Max_Comm_Operating_Depth').AsSingle;
              HF_Link_Capable                := FieldByName('HF_Link_Capable').AsBoolean;
              UHF_Link_Capable               := FieldByName('UHF_Link_Capable').AsBoolean;
              HF_Voice_Capable               := FieldByName('HF_Voice_Capable').AsBoolean;
              VHF_Voice_Capable              := FieldByName('VHF_Voice_Capable').AsBoolean;
              UHF_Voice_Capable              := FieldByName('UHF_Voice_Capable').AsBoolean;
              SATCOM_Voice_Capable           := FieldByName('SATCOM_Voice_Capable').AsBoolean;
              UWT_Voice_Capable              := FieldByName('UWT_Voice_Capable').AsBoolean;
              HF_MHS_Capable                 := FieldByName('HF_MHS_Capable').AsBoolean;
              UHF_MHS_Capable                := FieldByName('UHF_MHS_Capable').AsBoolean;
              SATCOM_MHS_Capable             := FieldByName('SATCOM_MHS_Capable').AsBoolean;
              Damage_Capacity                := FieldByName('Damage_Capacity').AsInteger;
              Plat_Basing_Capability         := FieldByName('Plat_Basing_Capability').AsBoolean;
              Chaff_Capability               := FieldByName('Chaff_Capability').AsBoolean;
              Readying_Time                  := FieldByName('Readying_Time').AsInteger;
              Sonobuoy_Capable               := FieldByName('Sonobuoy_Capable').AsBoolean;
              Nav_Light_Capable              := FieldByName('Nav_Light_Capable').AsBoolean;
              Periscope_Depth                := FieldByName('Periscope_Depth').AsSingle;
              Periscope_Height_Above_Water   := FieldByName('Periscope_Height_Above_Water').AsSingle;
              Periscope_Front_Radar_Xsection := FieldByName('Periscope_Front_Radar_Xsection').AsSingle;
              Periscope_Side_Radar_Xsection  := FieldByName('Periscope_Side_Radar_Xsection').AsSingle;
              Periscope_Front_Vis_Xsection   := FieldByName('Periscope_Front_Vis_Xsection').AsSingle;
              Periscope_Side_Vis_Xsection    := FieldByName('Periscope_Side_Vis_Xsection').AsSingle;
              Periscope_Front_IR_Xsection    := FieldByName('Periscope_Front_IR_Xsection').AsSingle;
              Periscope_Side_IR_Xsection     := FieldByName('Periscope_Side_IR_Xsection').AsSingle;
              Engagement_Range               := FieldByName('Engagement_Range').AsSingle;
              Auto_Air_Defense_Capable       := FieldByName('Auto_Air_Defense_Capable').AsBoolean;
              Alert_State_Time               := FieldByName('Alert_State_Time').AsSingle;
              Detectability_Type             := FieldByName('Detectability_Type').AsInteger;
              Max_Sonobuoys_To_Monitor       := FieldByName('Max_Sonobuoys_To_Monitor').AsInteger;
              Sonobuoy_Deploy_Max_Altitude   := FieldByName('Sonobuoy_Deploy_Max_Altitude').AsInteger;
              Sonobuoy_Deploy_Min_Altitude   := FieldByName('Sonobuoy_Deploy_Min_Altitude').AsInteger;
              Sonobuoy_Deploy_Max_Speed      := FieldByName('Sonobuoy_Deploy_Max_Speed').AsInteger;
              Air_Drop_Torpedo_Max_Altitude  := FieldByName('Air_Drop_Torpedo_Max_Altitude').AsInteger;
              Air_Drop_Torpedo_Min_Altitude  := FieldByName('Air_Drop_Torpedo_Min_Altitude').AsInteger;
              Air_Drop_Torpedo_Max_Speed     := FieldByName('Air_Drop_Torpedo_Max_Speed').AsInteger;
              TMA_Rate_Factor                := FieldByName('TMA_Rate_Factor').AsSingle;
              HMS_Noise_Reduction_Factor     := FieldByName('HMS_Noise_Reduction_Factor').AsSingle;
              TAS_Noise_Reduction_Factor     := FieldByName('TAS_Noise_Reduction_Factor').AsSingle;
              Infrared_Decoy_Capable         := FieldByName('Infrared_Decoy_Capable').AsBoolean;
              HF_Mid_Course_Update_Capable   := FieldByName('HF_Mid_Course_Update_Capable').AsBoolean;
              UHF_Mid_Course_Update_Capable  := FieldByName('UHF_Mid_Course_Update_Capable').AsBoolean;
              //VBS_Class_Name := FieldByName('vbs_class_name').AsString;
              //Quantity_Group_Personal := FieldByName('Quantity_Group_Personal').AsInteger;
              //GangwayPosition := FieldByName('GangwayPosition').AsInteger;
            end;
          end;

          result := vehDefinition;
        end;
      finally
        sp.Close;
        sp.Free;
      end;
    end;
  end;
end;

function TDMLite.GetVerticalCoverage(const rdr_id: integer;
  dict: TDBRadar_Vertical_CovDict): TDBRadar_Vertical_CovList;
var
  sp         : TZStoredProc;
  pod        : TDBRadar_Vertical_Coverage;
  pods       : TDBRadar_Vertical_CovList;
  newData    : Boolean;
begin
  result := nil;
  newData := False;

  { search in dict first }
  if not dict.ContainsKey(rdr_id) then
    newData := True
  else
    result := dict.Items[rdr_id];

  { new data add to dict }
  if newData then
  begin
    { for diagram 1 }
    sp := createStoredProcedure('get_gl_vert_coverage_info');
    if Assigned(sp) then
      with sp do
      begin
        Params.ParamByName('@radar').AsInteger := rdr_id;
        Params.ParamByName('@diagram').AsInteger := 1;

        try
          Open;

          if not IsEmpty then
          begin
            First;

            pods := TDBRadar_Vertical_CovList.Create;
            dict.Add(rdr_id,pods);

            while not EOF do
            begin
              pod := TDBRadar_Vertical_Coverage.Create;

              with pod do
              begin
                Coverage_Index              := FieldByName('Coverage_Index').AsInteger;
                Radar_Index                 := rdr_id;
                Coverage_Diagram            := FieldByName('Coverage_Diagram').AsInteger;
                Vert_Coverage_Range         := FieldByName('Vert_Coverage_Range').AsFloat;
                Vert_Cover_Min_Elevation    := FieldByName('Vert_Cover_Min_Elevation').AsFloat;
                Vert_Cover_Max_Elevation    := FieldByName('Vert_Cover_Max_Elevation').AsFloat;
              end;

              pods.Add(pod);

              Next;
            end;

            result := pods;
          end;
        finally
          sp.Close;
          FreeAndNil(sp);
        end;
      end;

    { for diagram 2 }
    sp := createStoredProcedure('get_gl_vert_coverage_info');
    if Assigned(sp) then
      with sp do
      begin
        Params.ParamByName('@radar').AsInteger := rdr_id;
        Params.ParamByName('@diagram').AsInteger := 2;

        try
          Open;

          if not IsEmpty then
          begin
            First;

            // add to existing list
            if not Assigned(pods) then
            begin
              pods := TDBRadar_Vertical_CovList.Create;
              dict.Add(rdr_id,pods);
            end;

            while not EOF do
            begin
              pod := TDBRadar_Vertical_Coverage.Create;

              with pod do
              begin
                Coverage_Index              := FieldByName('Coverage_Index').AsInteger;
                Radar_Index                 := rdr_id;
                Coverage_Diagram            := FieldByName('Coverage_Diagram').AsInteger;
                Vert_Coverage_Range         := FieldByName('Vert_Coverage_Range').AsFloat;
                Vert_Cover_Min_Elevation    := FieldByName('Vert_Cover_Min_Elevation').AsFloat;
                Vert_Cover_Max_Elevation    := FieldByName('Vert_Cover_Max_Elevation').AsFloat;
              end;

              pods.Add(pod);

              Next;
            end;

            result := pods;
          end;
        finally
          sp.Close;
          FreeAndNil(sp);
        end;
      end;
  end;
end;

function TDMLite.GetVisualOnBoard(const veh_id: integer;
  dict: TDBVisualOnBoardDict): TDBVisualOnBoardList;
var
  sp : TZStoredProc;
  visOnBoard : TDBVisual_Sensor_On_Board;
  visOnBoardList : TDBVisualOnBoardList;
  newData : Boolean;
begin
  result := nil;
  newData := False;

  { search in dict first }
  if not dict.ContainsKey(veh_id) then
    newData := True
  else
    result := dict.Items[veh_id];

  { new data add to dict }
  if newData then
  begin
    sp := createStoredProcedure('get_visual_instances');
    if Assigned(sp) then
      with sp do
      begin
        Params.ParamByName('@vehicle').AsInteger := veh_id;

        try
          Open;

          if not IsEmpty then
          begin
            First;

            visOnBoardList := TDBVisualOnBoardList.Create;
            dict.Add(veh_id,visOnBoardList);

            while not EOF do
            begin
              visOnBoard := TDBVisual_Sensor_On_Board.Create;

              with visOnBoard do
              begin
                Visual_Instance_Index := FieldByName('Visual_Instance_Index').AsInteger;
                Instance_Identifier   := FieldByName('Instance_Identifier').AsString;
                Instance_Type         := FieldByName('Instance_Type').AsInteger;
                Vehicle_Index         := veh_id;
                Observer_Height       := FieldByName('Observer_Height').AsSingle;
              end;

              visOnBoardList.Add(visOnBoard);

              Next;
            end;

            result := visOnBoardList;
          end;
        finally
          sp.Close;
          FreeAndNil(sp);
        end;
      end;
  end;
end;

function TDMLite.Get_Library_Instance(const id: Integer;
  var pList: TDBRuntimePlatformLibraryDict): boolean;
var
  sp    : TZStoredProc;
  lib   : TDBRuntime_Platform_Library;
  entryList : TDBPlatformLibraryEntryList;
  entry : TDBPlatform_Library_Entry;
  item  : TDBPairRuntimePlatformLibraryDict;
begin
  result := false;

  sp := createStoredProcedure('get_gl_library_instances');
  if Assigned(sp) then
    with sp do
    begin
      Params.ParamByName('@resource').AsInteger := id;

      try
        Open;

        result := RecordCount > 0;

        if not IsEmpty then
        begin
          First;

          while not EOF do
          begin
            lib := TDBRuntime_Platform_Library.Create;

            with lib do
            begin
              Platform_Library_Index := FieldByName('Platform_Library_Index').AsInteger;
              Library_Name           := FieldByName('Library_Name').AsString;
            end;

            pList.Add(lib,nil);

            Next;
          end;
        end;
      finally
        sp.Close;
        FreeAndNil(sp);
      end;
    end;

  for item in pList do
  begin
    sp := createStoredProcedure('get_library_entry_info');
    if Assigned(sp) then
      with sp do
      begin
        Params.ParamByName('@library').AsInteger := item.Key.Platform_Library_Index;

        try
          Open;

          result := RecordCount > 0;

          if not IsEmpty then
          begin
            First;

            entryList :=  TDBPlatformLibraryEntryList.Create;
            pList.AddOrSetValue(item.Key,entryList);

            while not EOF do
            begin
              entry := TDBPlatform_Library_Entry.Create;

              with entry do
              begin
                Library_Entry_Index := FieldByName('Library_Entry_Index').AsInteger;
                Library_Index       := item.Key.Platform_Library_Index;
                Platform_Type       := FieldByName('Platform_Type').AsInteger;
                Platform_Index      := FieldByName('Platform_Index').AsInteger;
              end;

              entryList.Add(entry);

              Next;
            end;
          end;
        finally
          sp.Close;
          FreeAndNil(sp);
        end;
      end;
  end;


end;

function TDMLite.InitZDB(const zDbServer, zDBProto, zDBname, zDBuser,
  zDBPass: string): boolean;
var
  connStr : String;
begin
  with ZConn do
  begin
    HostName := zDbServer;
    Protocol := zDBProto;
    Database := zDBname;
    User := zDBuser;
    Password := zDBPass;
  end;

  try
    ZConn.Connect;
    result := ZConn.Connected;
  except
    on Exception do
    begin
      MessageDlg('Error Database Connection on ' + zDbServer, mtError, [mbOK],
        0);
      result := false;
      exit;
    end
  end;
end;

end.
