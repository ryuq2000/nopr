object frmFormationEditor: TfrmFormationEditor
  Left = 0
  Top = 0
  BorderIcons = [biMinimize, biMaximize]
  BorderStyle = bsDialog
  Caption = 'Formation Editor'
  ClientHeight = 442
  ClientWidth = 474
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object spl1: TSplitter
    Left = 8
    Top = 228
    Width = 457
    Height = 9
    Align = alNone
  end
  object btnNew: TSpeedButton
    Left = 421
    Top = 199
    Width = 23
    Height = 23
    Hint = 'New/Edit Formation'
    Enabled = False
    Glyph.Data = {
      66060000424D6606000000000000360000002800000017000000160000000100
      1800000000003006000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF6060604F4F4F6060604F4F4F6060604F4F4F6060604F4F4F606060
      4F4F4F6060604F4F4F6060604F4F4F6060604F4F4F6060604F4F4F6060604F4F
      4F606060000000000000FFFFFFC1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1
      C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1
      C1C1C1C1C1C1C1C1C14F4F4F000000000000FFFFFFC1C1C1C1C1C1C1C1C1C1C1
      C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1
      C1C1C1C1C1C1C1C1C1C1C1C1C1C1D1D1D1606060000000000000FFFFFFC1C1C1
      C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1
      C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C14F4F4F00000000
      0000FFFFFFC1C1C1C1C1C1C1C1C1C1C1C1C1C1C1D1D1D1C1C1C1C1C1C1C1C1C1
      C1C1C1C1C1C1C1C1C1D1D1D1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1D1D1
      D1606060000000000000FFFFFFC1C1C1C1C1C1C1C1C1C1C1C1C1C1C100000000
      0000C1C1C1000000000000C1C1C1000000000000C1C1C1000000000000C1C1C1
      C1C1C1C1C1C1C1C1C14F4F4F000000000000FFFFFFC1C1C1C1C1C1C1C1C1C1C1
      C1C1C1C1000000C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1
      C1C1000000C1C1C1C1C1C1C1C1C1D1D1D1606060000000000000FFFFFFC1C1C1
      C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1
      C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C14F4F4F00000000
      0000FFFFFFC1C1C1C1C1C1C1C1C1C1C1C1C1C1C1000000C1C1C1C1C1C1C1C1C1
      C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1000000C1C1C1C1C1C1C1C1C1D1D1
      D1606060000000000000FFFFFFC1C1C1C1C1C1C1C1C1C1C1C1C1C1C1000000C1
      C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1000000C1C1C1
      C1C1C1C1C1C1C1C1C14F4F4F000000000000FFFFFFC1C1C1C1C1C1C1C1C1C1C1
      C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1
      C1C1C1C1C1C1C1C1C1C1C1C1C1C1D1D1D1606060000000000000FFFFFFC1C1C1
      C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1
      C1C1C1C1C1C1C1C1C1C1000000C1C1C1C1C1C1C1C1C1C1C1C14F4F4F00000000
      0000FFFFFFC1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1
      C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1000000C1C1C1C1C1C1C1C1C1D1D1
      D1606060000000000000FFFFFFC1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1
      C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1
      C1C1C1C1C1C1C1C1C14F4F4F000000000000FFFFFFC1C1C1C1C1C1C1C1C1C1C1
      C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1D1
      D1D1000000C1C1C1C1C1C1C1C1C1C1C1C1606060000000000000FFFFFFC1C1C1
      C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1000000000000C1C1C10000
      00000000C1C1C1000000000000C1C1C1C1C1C1C1C1C1C1C1C14F4F4F00000000
      0000FFFFFFC1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1
      C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1D1D1
      D1606060000000000000FFFFFFC1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1
      C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1
      C1C1C1C1C1C1C1C1C14F4F4F000000000000FFFFFFC1C1C1C1C1C1C1C1C1C1C1
      C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1
      C1C1C1C1C1C1C1C1C1C1C1C1C1C1D1D1D1606060000000000000FFFFFFC1C1C1
      C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1
      C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C14F4F4F00000000
      0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFF000000000000}
    OnClick = btnNewClick
  end
  object btnDelete: TSpeedButton
    Left = 442
    Top = 199
    Width = 23
    Height = 22
    Hint = 'Remove Formation'
    Enabled = False
    Glyph.Data = {
      0E060000424D0E06000000000000360000002800000016000000160000000100
      180000000000D805000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      FF6060604F4F4F6060604F4F4F6060604F4F4F6060606060604F4F4F6060604F
      4F4F6060606060604F4F4F6060604F4F4F6060604F4F4F6060604F4F4F606060
      0000FFFFFFC1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1
      C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1
      C14F4F4F0000FFFFFFC1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1
      C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1
      C1C1D1D1D16060600000FFFFFFD1D1D1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1
      C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1
      C1C1C1C1C1C1C1C1C14F4F4F0000FFFFFFC1C1C1C1C1C1C1C1C1C1C1C1C1C1C1
      C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1
      C1C1C1C1C1C1C1C1C1C1D1D1D16060600000FFFFFFC1C1C1C1C1C1C1C1C1C1C1
      C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1
      C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C14F4F4F0000FFFFFFC1C1C1C1C1C1C1
      C1C1C1C1C1C1C1C1C1C1C1000000C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1
      000000C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1D1D1D16060600000FFFFFFC1C1C1
      C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1000000D1D1D1C1C1C1C1C1C1C1C1
      C1000000C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C14F4F4F0000FFFF
      FFC1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1000000C1C1C1C1
      C1C1000000C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1606060
      0000FFFFFFC1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1
      000000000000D1D1D1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1
      C14F4F4F0000FFFFFFC1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1
      C1C1C1C1000000000000C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1
      C1C1D1D1D16060600000FFFFFFC1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1
      C1C1C1C1C1000000C1C1C1C1C1C1000000C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1
      C1C1C1C1C1C1C1C1C14F4F4F0000FFFFFFC1C1C1C1C1C1C1C1C1C1C1C1C1C1C1
      C1C1C1C1C1C1000000C1C1C1C1C1C1C1C1C1C1C1C1000000C1C1C1C1C1C1C1C1
      C1C1C1C1C1C1C1C1C1C1D1D1D16060600000FFFFFFC1C1C1C1C1C1C1C1C1C1C1
      C1C1C1C1C1C1C1000000C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1000000C1
      C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C14F4F4F0000FFFFFFC1C1C1C1C1C1C1
      C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1
      C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C16060600000FFFFFFC1C1C1
      C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1
      C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C14F4F4F0000FFFF
      FFC1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1
      C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1D1D1D1606060
      0000FFFFFFC1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1
      C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1
      C14F4F4F0000FFFFFFC1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1
      C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1
      C1C1D1D1D16060600000FFFFFFC1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1
      C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1
      C1C1C1C1C1C1C1C1C14F4F4F0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000}
    OnClick = btnDeleteClick
  end
  object lb1: TLabel
    Left = 8
    Top = 204
    Width = 37
    Height = 13
    Caption = 'Leader:'
  end
  object lbLeaderFormation: TLabel
    Left = 51
    Top = 204
    Width = 12
    Height = 13
    Caption = '---'
  end
  object Bevel32: TBevel
    Left = 8
    Top = 226
    Width = 456
    Height = 3
  end
  object btnClose: TButton
    Left = 390
    Top = 243
    Width = 75
    Height = 25
    Caption = 'Close'
    TabOrder = 0
    OnClick = btnCloseClick
  end
  object btnShow: TButton
    Left = 8
    Top = 243
    Width = 146
    Height = 25
    Caption = 'Show Members >>'
    Enabled = False
    TabOrder = 1
    OnClick = btnShowClick
  end
  object btnAddRemove: TButton
    Left = 160
    Top = 243
    Width = 146
    Height = 25
    Caption = 'Add/Remove Members...'
    Enabled = False
    TabOrder = 2
    OnClick = btnAddRemoveClick
  end
  object lvMemberFormation: TListView
    Left = 8
    Top = 274
    Width = 457
    Height = 157
    Columns = <
      item
        Caption = 'Name'
        Width = 160
      end
      item
        Caption = 'Bearing (degrees)'
        Width = 110
      end
      item
        Caption = 'Range (nm)'
        Width = 80
      end
      item
        Caption = 'Altitude/Depth'
        Width = 90
      end>
    TabOrder = 3
    ViewStyle = vsReport
  end
  object lvFormationAvailable: TListView
    Left = 8
    Top = 8
    Width = 457
    Height = 185
    Columns = <
      item
        Caption = 'Name'
        Width = 300
      end>
    TabOrder = 4
    ViewStyle = vsReport
    OnSelectItem = lvFormationAvailableSelectItem
  end
end
