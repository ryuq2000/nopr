unit ufTrackTargetDetail;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, uSimObjects;

type
  TfrmDetailTrackTarget = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    lblTrack: TLabel;
    lblClass: TLabel;
    lblForce: TLabel;
    lblCourse: TLabel;
    lblSpeed: TLabel;
    lblBearing: TLabel;
    lblRange: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Bevel1: TBevel;
    btnClose: TButton;
    procedure btnCloseClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
    FPlatform : TSimObject;
  public
    { Public declarations }
    procedure SetDetailedTarget(controlled, target : TSimObject);
  end;

var
  frmDetailTrackTarget: TfrmDetailTrackTarget;

implementation

uses uBaseCoordSystem, uT3PlatformInstance, uT3Vehicle,
  uT3Common;

{$R *.dfm}

procedure TfrmDetailTrackTarget.btnCloseClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmDetailTrackTarget.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := caFree;
  frmDetailTrackTarget := nil;
end;

procedure TfrmDetailTrackTarget.SetDetailedTarget(controlled,
  target: TSimObject);
var                                 
  bearing, range : double;
begin
  FPlatform := controlled;

  bearing := CalcBearing(FPlatform.getPositionX,FPlatform.getPositionY,
                  target.getPositionX, target.getPositionY);
  range   := CalcRange(FPlatform.getPositionX,FPlatform.getPositionY,
                  target.getPositionX, target.getPositionY);

  lblRange.Caption := FormatFloat('#.##',range);
  lblBearing.Caption := FormatCourse(bearing);

  lblForce.Caption := '---';
  lblCourse.Caption := '---';
  lblSpeed.Caption := '---';

  if target is TT3PlatformInstance then
  begin
    lblTrack.Caption  := TT3PlatformInstance(target).TrackID;
    if target is TT3Vehicle then
      lblClass.Caption  := TT3Vehicle(target).VehicleDefinition.Vehicle_Identifier
    else
      lblClass.Caption  := '---';

    lblForce.Caption := getForceDesignation(TT3PlatformInstance(target).ForceDesignation);
    lblCourse.Caption := FormatCourse(TT3PlatformInstance(target).Course);
    lblSpeed.Caption := FormatSpeed(TT3PlatformInstance(target).Speed);
  end;
end;

end.
