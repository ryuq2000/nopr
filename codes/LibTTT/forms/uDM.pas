unit uDM;

interface

uses
  SysUtils, Classes, ZAbstractConnection, ZConnection, Dialogs,
  newClassASTT, ZDataset, ZStoredProcedure;

type
  TDM = class(TDataModule)
    ZConn: TZConnection;
  private
    { Private declarations }

    {*------------------------------------------------------------------------------
      Create and return storedprocedure object;

      @param aSpName Stored procedure name
      @return
    -------------------------------------------------------------------------------}
    function createStoredProcedure(aSpName : String) : TZStoredProc;

  public
    { Public declarations }

    {*------------------------------------------------------------------------------
    Initialize Database

    @param zDbServer Server address
    @param zDBProto  Protocol
    @param zDBname   Database NAme
    @param zDBuser   User name
    @param zDBPass   Password
    @return
    -------------------------------------------------------------------------------}
    function InitZDB(const zDbServer, zDBProto, zDBname, zDBuser,
      zDBPass: string): boolean;

    {*------------------------------------------------------------------------------
      Get Scenario info

      @param id   sceario id
      @param rec  TScenario_Definition varable
      @return
    -------------------------------------------------------------------------------}
    function GetScenario(const id: Integer; var rec: TScenario_Definition)
      : boolean;

  end;

var
  DM: TDM;

implementation


{$R *.dfm}

{ TDM }

function TDM.createStoredProcedure(aSpName : String): TZStoredProc;
var
  sp : TZStoredProc;
begin
  result := nil;

  if ZConn.Connected then
  begin

    try
      sp := TZStoredProc.Create(nil);
      sp.Connection := ZConn;
      sp.StoredProcName := aSpName;
      sp.ParamCheck := True;

    except
      sp.Free;
      sp := nil;
    end;

    result := sp;

  end;

end;


function TDM.GetScenario(const id: Integer;
  var rec: TScenario_Definition): boolean;
var
  sp : TZStoredProc;
begin
  sp := createStoredProcedure('get_scenario_info');
  if Assigned(sp) then
    with sp do
    begin
      ParamByName('index').AsInteger := id;

      try
        ExecProc;

        result := RecordCount > 0;
        if not IsEmpty then
        begin
          First;

          if not Assigned(rec) then
            rec := TScenario_Definition.Create;

          with rec.FData do
          begin
            Scenario_Index := FieldByName('Scenario_Index').AsInteger;
            Scenario_Identifier := FieldByName('Scenario_Identifier').AsString;
            Resource_Alloc_Index := FieldByName('Resource_Alloc_Index').AsInteger;
          end;
        end;
      finally
        sp.Close;
        sp.Free;
      end;
    end;
end;


function TDM.InitZDB(const zDbServer, zDBProto, zDBname, zDBuser,
  zDBPass: string): boolean;
var
  connStr : String;
begin
  with ZConn do
  begin
    HostName := zDbServer;
    Protocol := zDBProto;
    Database := zDBname;
    User := zDBuser;
    Password := zDBPass;
  end;

  try
    ZConn.Connect;
    result := ZConn.Connected;
  except
    on Exception do
    begin
      MessageDlg('Error Database Connection on ' + zDbServer, mtError, [mbOK],
        0);
      result := false;
      exit;
    end
  end;
end;

end.
