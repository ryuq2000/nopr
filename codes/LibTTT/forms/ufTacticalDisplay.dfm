object frmTacticalDisplay: TfrmTacticalDisplay
  Left = 44
  Top = -238
  BorderStyle = bsToolWindow
  Caption = 
    'Tactical Display - Ahmad Yani - Cubicle 3, Station 6 [Command Pl' +
    'atform: Arctic Passage 3] '
  ClientHeight = 1656
  ClientWidth = 1353
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  Menu = MainMenu1
  OldCreateOrder = False
  Position = poDesigned
  ShowHint = True
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Image1: TImage
    Left = 616
    Top = 312
    Width = 105
    Height = 105
  end
  object pnlLeft: TPanel
    Left = 0
    Top = 33
    Width = 341
    Height = 1550
    Align = alLeft
    BevelKind = bkFlat
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitHeight = 1530
    object TacticalDisplayControlPanel: TPageControl
      Left = 0
      Top = 305
      Width = 337
      Height = 1241
      ActivePage = tsPlatformGuidance
      Align = alClient
      Images = ImageList2
      TabOrder = 1
      OnChange = TacticalDisplayControlPanelChange
      ExplicitHeight = 1221
      object tsOwnShip: TTabSheet
        ExplicitHeight = 1192
        inline fmOwnShip1: TfmOwnShip
          Left = 0
          Top = 0
          Width = 329
          Height = 1212
          Align = alClient
          TabOrder = 0
          ExplicitWidth = 329
          ExplicitHeight = 1192
          inherited ScrollBox4: TScrollBox
            Width = 329
            Height = 1212
            ExplicitWidth = 329
            ExplicitHeight = 1192
            inherited Label34: TLabel
              Width = 20
              Caption = 'feet'
              ExplicitWidth = 20
            end
            inherited Label37: TLabel
              Width = 20
              Caption = 'feet'
              ExplicitWidth = 20
            end
            inherited StaticText23: TStaticText [27]
            end
            inherited StaticText16: TStaticText [28]
            end
            inherited StaticText21: TStaticText [29]
            end
            inherited StaticText22: TStaticText [30]
            end
            inherited btnLaunch: TButton
              OnClick = fmOwnShip1btnLaunchClick
            end
          end
        end
        object pnlInfoDepth: TPanel
          Left = 7
          Top = 185
          Width = 130
          Height = 40
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 1
          Visible = False
          object StaticText23: TStaticText
            Left = 10
            Top = 24
            Width = 66
            Height = 17
            Caption = 'Actual Depth'
            TabOrder = 0
          end
          object StaticText22: TStaticText
            Left = 10
            Top = 1
            Width = 76
            Height = 17
            Caption = 'Ordered Depth'
            TabOrder = 1
          end
        end
      end
      object tsPlatformGuidance: TTabSheet
        ImageIndex = 1
        ExplicitHeight = 1192
        inline fmPlatformGuidance1: TfmPlatformGuidance
          Left = 0
          Top = 0
          Width = 329
          Height = 1212
          Align = alClient
          TabOrder = 0
          ExplicitWidth = 329
          ExplicitHeight = 1192
          inherited PanelGuidanceControlChoices: TPanel
            Width = 329
            ExplicitWidth = 329
            inherited SpeedButton2: TSpeedButton
              OnClick = fmPlatformGuidance1SpeedButton2Click
            end
          end
          inherited PanelPlatformGuidance: TPanel
            Width = 329
            Height = 1171
            ExplicitWidth = 329
            ExplicitHeight = 1151
            inherited grbSinuation: TGroupBox [0]
              Width = 329
              Height = 1171
              ExplicitWidth = 329
              ExplicitHeight = 1151
            end
            inherited grbOutrun: TGroupBox [1]
              Width = 329
              Height = 1171
              ExplicitWidth = 329
              ExplicitHeight = 1151
              inherited sbTrackToOutrun: TSpeedButton
                OnClick = fmPlatformGuidance1sbTrackToOutrunClick
              end
            end
            inherited grbEvasion: TGroupBox [2]
              Width = 329
              Height = 1171
              ExplicitWidth = 329
              ExplicitHeight = 1151
              inherited sbTrackToEvade: TSpeedButton
                OnClick = fmPlatformGuidance1sbTrackToEvadeClick
              end
            end
            inherited grbShadow: TGroupBox [3]
              Width = 329
              Height = 1171
              ExplicitWidth = 329
              ExplicitHeight = 1151
            end
            inherited grbZigZag: TGroupBox [4]
              Width = 329
              Height = 1171
              ExplicitWidth = 329
              ExplicitHeight = 1151
            end
            inherited grbStraightLine: TGroupBox [5]
              Width = 329
              Height = 1171
              ExplicitWidth = 329
              ExplicitHeight = 1151
              inherited ScrollBox5: TScrollBox
                Width = 325
                Height = 1154
                ExplicitWidth = 325
                ExplicitHeight = 1134
                inherited whHeading: TVrWheel
                  OnChange = fmPlatformGuidance1whHeadingChange
                end
                inherited Label128: TLabel
                  Top = 152
                  ExplicitTop = 152
                end
                inherited Label126: TLabel
                  Top = 130
                  ExplicitTop = 130
                end
                inherited Label125: TLabel
                  Top = 193
                  ExplicitTop = 193
                end
                inherited Label124: TLabel
                  Top = 172
                  ExplicitTop = 172
                end
                inherited Label122: TLabel
                  Top = 108
                  ExplicitTop = 108
                end
                inherited panAltitude: TPanel
                  inherited edOrderAltitude: TEdit
                    OnKeyPress = fmPlatformGuidance1edOrderAltitudeKeyPress
                  end
                end
                inherited panDepth: TPanel
                  inherited lbl4: TLabel
                    Top = 26
                    ExplicitTop = 26
                  end
                end
                inherited edtStraightLineOrderedGroundSpeed: TEdit
                  OnKeyPress = fmPlatformGuidance1edtStraightLineOrderedGroundSpeedKeyPress
                end
                inherited edtStraightLineOrderedHeading: TEdit
                  OnKeyPress = fmPlatformGuidance1edtStraightLineOrderedHeadingKeyPress
                end
              end
            end
            inherited grbEngagement: TGroupBox [6]
              Width = 329
              Height = 1171
              ExplicitWidth = 329
              ExplicitHeight = 1151
              inherited sbEngageTrackToEngage: TSpeedButton
                OnClick = fmPlatformGuidance1sbEngageTrackToEngageClick
              end
            end
            inherited grbHelm: TGroupBox [7]
              Width = 329
              Height = 1171
              ExplicitWidth = 329
              ExplicitHeight = 1151
              inherited Label129: TLabel
                Width = 39
                Caption = 'degrees'
                ExplicitWidth = 39
              end
              inherited StaticText88: TStaticText [20]
              end
              inherited StaticText89: TStaticText [21]
              end
              inherited StaticText90: TStaticText [22]
              end
              inherited StaticText91: TStaticText [23]
              end
              inherited StaticText92: TStaticText [24]
              end
              inherited StaticText93: TStaticText [25]
              end
              inherited StaticText94: TStaticText [26]
              end
              inherited edtOrderedHelmAngle: TEdit [27]
              end
              inherited StaticText95: TStaticText [28]
              end
              inherited StaticText96: TStaticText [29]
              end
              inherited StaticText97: TStaticText [30]
              end
              inherited StaticText98: TStaticText [31]
              end
              inherited Panel2: TPanel [32]
              end
              inherited Panel1: TPanel [33]
              end
              inherited edtHelmOrderedGroundSpeed: TEdit [34]
              end
            end
            inherited gbWaypoint: TGroupBox [8]
              Width = 329
              Height = 1171
              ExplicitWidth = 329
              ExplicitHeight = 1151
              inherited btnWaypoint: TButton
                Top = 187
                OnClick = fmPlatformGuidance1btnWaypointClick
                ExplicitTop = 187
              end
            end
            inherited grbReturnToBase: TGroupBox [9]
              Width = 329
              Height = 1171
              ExplicitWidth = 329
              ExplicitHeight = 1151
            end
            inherited grpStation: TGroupBox [10]
              Width = 329
              Height = 1171
              ExplicitWidth = 329
              ExplicitHeight = 1151
              inherited sbOnTrackAnchorMode: TSpeedButton
                OnClick = fmPlatformGuidance1sbOnTrackAnchorModeClick
              end
              inherited pnlStationPosition: TPanel
                inherited btnStationAnchorPosition: TSpeedButton
                  OnClick = fmPlatformGuidance1btnStationAnchorPositionClick
                end
              end
              inherited pnlStationTrack: TPanel
                inherited sbOnTrackAnchorTrack: TSpeedButton
                  OnClick = fmPlatformGuidance1sbOnTrackAnchorTrackClick
                end
              end
            end
            inherited grpCircle: TGroupBox [11]
              Width = 329
              Height = 1171
              ExplicitWidth = 329
              ExplicitHeight = 1151
              inherited ScrollBox1: TScrollBox
                Width = 325
                Height = 1154
                ExplicitWidth = 325
                ExplicitHeight = 1134
                inherited btnCircleMode: TSpeedButton
                  Left = 218
                  Top = 18
                  OnClick = fmPlatformGuidance1btnCircleModeClick
                  ExplicitLeft = 218
                  ExplicitTop = 18
                end
                inherited pnlTrack: TPanel [12]
                  inherited sbCircleOnTrackTrack: TSpeedButton
                    OnClick = fmPlatformGuidance1sbCircleOnTrackTrackClick
                  end
                end
                inherited pnlPosition: TPanel [13]
                  inherited sbCircleOnPositionPosition: TSpeedButton
                    OnClick = fmPlatformGuidance1sbCircleOnPositionPositionClick
                  end
                end
                inherited edtCircleRadius: TEdit
                  Top = 145
                  ExplicitTop = 145
                end
              end
            end
            inherited grbFormation: TGroupBox
              Width = 329
              Height = 1171
              ExplicitWidth = 329
              ExplicitHeight = 1151
            end
          end
          inherited pmGuidance: TPopupMenu
            inherited mnStraightLine1: TMenuItem
              OnClick = fmPlatformGuidance1mnStraightLine1Click
            end
            inherited mnZigzag1: TMenuItem
              inherited Short1: TMenuItem
                OnClick = fmPlatformGuidance1Short1Click
              end
              inherited Long1: TMenuItem
                OnClick = fmPlatformGuidance1Long1Click
              end
              inherited VeryLong1: TMenuItem
                OnClick = fmPlatformGuidance1VeryLong1Click
              end
            end
            inherited mnFormation2: TMenuItem
              OnClick = nil
            end
            inherited mnOutrun1: TMenuItem
              OnClick = fmPlatformGuidance1mnOutrun1Click
            end
          end
        end
      end
      object tsSensor: TTabSheet
        ImageIndex = 2
        ExplicitHeight = 1192
        inline fmSensor1: TfmSensor
          Left = 0
          Top = 0
          Width = 329
          Height = 1212
          Align = alClient
          TabOrder = 0
          ExplicitWidth = 329
          ExplicitHeight = 1192
          inherited PanelSpace: TPanel
            Top = 111
            Width = 329
            Height = 0
            ExplicitTop = 111
            ExplicitWidth = 329
            ExplicitHeight = 0
          end
          inherited PanelSensorChoices: TPanel
            Width = 329
            Height = 111
            ExplicitWidth = 329
            ExplicitHeight = 111
            inherited lstSensor: TListView
              Width = 327
              Height = 109
              Font.Color = clActiveCaption
              ParentFont = False
              ExplicitWidth = 327
              ExplicitHeight = 109
            end
          end
          inherited PanelALL: TPanel
            Top = 111
            Width = 329
            Height = 1101
            ExplicitTop = 111
            ExplicitWidth = 329
            ExplicitHeight = 1081
            inherited PanelSensorControl: TPanel
              Width = 329
              Height = 1101
              ExplicitWidth = 329
              ExplicitHeight = 1081
              inherited grbVisualDetectorSensor: TGroupBox [0]
                Width = 323
                Height = 1095
                ExplicitWidth = 323
                ExplicitHeight = 1075
                inherited Label258: TLabel
                  Left = 160
                  ExplicitLeft = 160
                end
                inherited Label259: TLabel
                  Left = 62
                  ExplicitLeft = 62
                end
                inherited sbVisualDetectorDisplayRangeShow: TSpeedButton
                  Left = 35
                  Width = 89
                  Height = 23
                  OnClick = OnVisualShowClick
                  ExplicitLeft = 35
                  ExplicitWidth = 89
                  ExplicitHeight = 23
                end
                inherited sbVisualDetectorDisplayRangeHide: TSpeedButton
                  Left = 35
                  Top = 65
                  Width = 89
                  Height = 23
                  OnClick = OnVisualShowClick
                  ExplicitLeft = 35
                  ExplicitTop = 65
                  ExplicitWidth = 89
                  ExplicitHeight = 23
                end
                inherited sbVisualDetectorDisplayBlindZonesShow: TSpeedButton
                  Left = 141
                  Width = 89
                  Height = 23
                  OnClick = OnVisualShowClick
                  ExplicitLeft = 141
                  ExplicitWidth = 89
                  ExplicitHeight = 23
                end
                inherited sbVisualDetectorDisplayBlindZonesHide: TSpeedButton
                  Left = 141
                  Top = 65
                  Width = 89
                  Height = 23
                  OnClick = OnVisualShowClick
                  ExplicitLeft = 141
                  ExplicitTop = 65
                  ExplicitWidth = 89
                  ExplicitHeight = 23
                end
              end
              inherited grbSonarControl: TGroupBox [1]
                Width = 323
                Height = 1095
                ExplicitWidth = 323
                ExplicitHeight = 1075
                inherited ScrollBox2: TScrollBox
                  Width = 319
                  Height = 1078
                  ExplicitWidth = 319
                  ExplicitHeight = 1058
                  inherited Bevel43: TBevel
                    Left = 51
                    Top = 207
                    ExplicitLeft = 51
                    ExplicitTop = 207
                  end
                  inherited Label213: TLabel
                    Left = 6
                    Top = -3
                    ExplicitLeft = 6
                    ExplicitTop = -3
                  end
                  inherited Label214: TLabel
                    Left = 49
                    Top = 8
                    ExplicitLeft = 49
                    ExplicitTop = 8
                  end
                  inherited Label237: TLabel
                    Left = 9
                    Top = 201
                    ExplicitLeft = 9
                    ExplicitTop = 201
                  end
                  inherited Label238: TLabel
                    Left = 84
                    Top = 211
                    ExplicitLeft = 84
                    ExplicitTop = 211
                  end
                  inherited Label239: TLabel
                    Left = 182
                    Top = 211
                    ExplicitLeft = 182
                    ExplicitTop = 211
                  end
                  inherited sbDisplayBlindHide: TSpeedButton
                    Left = 165
                    Top = 245
                    Width = 86
                    Height = 23
                    OnClick = OnSonarBtnClick
                    ExplicitLeft = 165
                    ExplicitTop = 245
                    ExplicitWidth = 86
                    ExplicitHeight = 23
                  end
                  inherited sbDisplayBlindShow: TSpeedButton
                    Left = 165
                    Top = 224
                    Width = 86
                    Height = 23
                    OnClick = OnSonarBtnClick
                    ExplicitLeft = 165
                    ExplicitTop = 224
                    ExplicitWidth = 86
                    ExplicitHeight = 23
                  end
                  inherited sbDisplayRangeHide: TSpeedButton
                    Left = 58
                    Top = 245
                    Width = 86
                    Height = 23
                    OnClick = OnSonarBtnClick
                    ExplicitLeft = 58
                    ExplicitTop = 245
                    ExplicitWidth = 86
                    ExplicitHeight = 23
                  end
                  inherited sbDisplayRangeShow: TSpeedButton
                    Left = 58
                    Top = 224
                    Width = 86
                    Height = 23
                    OnClick = OnSonarBtnClick
                    ExplicitLeft = 58
                    ExplicitTop = 224
                    ExplicitWidth = 86
                    ExplicitHeight = 23
                  end
                  inherited sbSonarControlModeActive: TSpeedButton
                    Left = 21
                    Top = 22
                    Width = 86
                    Height = 23
                    OnClick = OnSonarBtnClick
                    ExplicitLeft = 21
                    ExplicitTop = 22
                    ExplicitWidth = 86
                    ExplicitHeight = 23
                  end
                  inherited sbSonarControlModeOff: TSpeedButton
                    Left = 21
                    Top = 64
                    Width = 86
                    Height = 23
                    OnClick = OnSonarBtnClick
                    ExplicitLeft = 21
                    ExplicitTop = 64
                    ExplicitWidth = 86
                    ExplicitHeight = 23
                  end
                  inherited sbSonarControlModePassive: TSpeedButton
                    Left = 21
                    Top = 43
                    Width = 86
                    Height = 23
                    OnClick = OnSonarBtnClick
                    ExplicitLeft = 21
                    ExplicitTop = 43
                    ExplicitWidth = 86
                    ExplicitHeight = 23
                  end
                  inherited pnlDeployment: TPanel
                    Left = 3
                    Top = 92
                    Height = 114
                    ExplicitLeft = 3
                    ExplicitTop = 92
                    ExplicitHeight = 114
                    inherited Bevel39: TBevel
                      Left = 66
                      Top = 6
                      ExplicitLeft = 66
                      ExplicitTop = 6
                    end
                    inherited Bevel41: TBevel
                      Left = 195
                      Top = 15
                      Width = 113
                      ExplicitLeft = 195
                      ExplicitTop = 15
                      ExplicitWidth = 113
                    end
                    inherited Bevel42: TBevel
                      Left = 169
                      Top = 62
                      ExplicitLeft = 169
                      ExplicitTop = 62
                    end
                    inherited Label215: TLabel
                      Left = 42
                      Top = 12
                      ExplicitLeft = 42
                      ExplicitTop = 12
                    end
                    inherited Label216: TLabel
                      Top = -2
                      ExplicitTop = -2
                    end
                    inherited Label221: TLabel
                      Left = 133
                      Top = 24
                      ExplicitLeft = 133
                      ExplicitTop = 24
                    end
                    inherited Label222: TLabel
                      Left = 133
                      Top = 40
                      ExplicitLeft = 133
                      ExplicitTop = 40
                    end
                    inherited Label223: TLabel
                      Left = 256
                      Top = 24
                      ExplicitLeft = 256
                      ExplicitTop = 24
                    end
                    inherited Label224: TLabel
                      Left = 256
                      Top = 40
                      ExplicitLeft = 256
                      ExplicitTop = 40
                    end
                    inherited Label226: TLabel
                      Left = 17
                      Top = 72
                      ExplicitLeft = 17
                      ExplicitTop = 72
                    end
                    inherited Label228: TLabel
                      Left = 133
                      Top = 69
                      ExplicitLeft = 133
                      ExplicitTop = 69
                    end
                    inherited Label229: TLabel
                      Left = 133
                      Top = 83
                      ExplicitLeft = 133
                      ExplicitTop = 83
                    end
                    inherited Label232: TLabel
                      Left = 256
                      Top = 69
                      ExplicitLeft = 256
                      ExplicitTop = 69
                    end
                    inherited Label233: TLabel
                      Left = 256
                      Top = 84
                      ExplicitLeft = 256
                      ExplicitTop = 84
                    end
                    inherited Label234: TLabel
                      Left = 133
                      Top = 99
                      ExplicitLeft = 133
                      ExplicitTop = 99
                    end
                    inherited Label236: TLabel
                      Left = 256
                      Top = 99
                      ExplicitLeft = 256
                      ExplicitTop = 99
                    end
                    inherited LabelCablePayout: TLabel
                      Left = 125
                      Top = 9
                      ExplicitLeft = 125
                      ExplicitTop = 9
                    end
                    inherited LabelDepth: TLabel
                      Left = 126
                      Top = 55
                      ExplicitLeft = 126
                      ExplicitTop = 55
                    end
                    inherited lbCableActual: TLabel
                      Left = 212
                      Top = 40
                      ExplicitLeft = 212
                      ExplicitTop = 40
                    end
                    inherited lbDepthActual: TLabel
                      Top = 84
                      ExplicitTop = 84
                    end
                    inherited lbDepthSettled: TLabel
                      Top = 69
                      ExplicitTop = 69
                    end
                    inherited lbDepthTow: TLabel
                      Top = 99
                      ExplicitTop = 99
                    end
                    inherited lblStatusDeployment: TLabel
                      Left = 58
                      Top = 72
                      ExplicitLeft = 58
                      ExplicitTop = 72
                    end
                    inherited sbDeploymentActiondeploy: TSpeedButton
                      Left = 21
                      Top = 26
                      Width = 78
                      OnClick = OnSonarBtnClick
                      ExplicitLeft = 21
                      ExplicitTop = 26
                      ExplicitWidth = 78
                    end
                    inherited sbDeploymentActionShow: TSpeedButton
                      Left = 21
                      Top = 46
                      Width = 78
                      OnClick = OnSonarBtnClick
                      ExplicitLeft = 21
                      ExplicitTop = 46
                      ExplicitWidth = 78
                    end
                    inherited editCableOrdered: TEdit
                      Left = 212
                      Top = 20
                      Width = 40
                      ExplicitLeft = 212
                      ExplicitTop = 20
                      ExplicitWidth = 40
                    end
                  end
                end
              end
              inherited grbIFFTransponderControl: TGroupBox [2]
                Width = 323
                Height = 1095
                ExplicitWidth = 323
                ExplicitHeight = 1075
                inherited ScrollBox6: TScrollBox
                  Width = 319
                  Height = 1078
                  ExplicitWidth = 319
                  ExplicitHeight = 1058
                  inherited Bevel8: TBevel
                    Top = 9
                    ExplicitTop = 9
                  end
                  inherited Bevel9: TBevel
                    Top = 92
                    ExplicitTop = 92
                  end
                  inherited Label14: TLabel
                    Top = 2
                    ExplicitTop = 2
                  end
                  inherited Label15: TLabel
                    Left = 43
                    Top = 18
                    ExplicitLeft = 43
                    ExplicitTop = 18
                  end
                  inherited Label16: TLabel
                    Top = 84
                    ExplicitTop = 84
                  end
                  inherited sbIFFTransponderControlModeOff: TSpeedButton
                    Left = 10
                    Top = 53
                    Width = 89
                    Height = 23
                    OnClick = fmSensor1sbIFFTransponderControlModeOffClick
                    ExplicitLeft = 10
                    ExplicitTop = 53
                    ExplicitWidth = 89
                    ExplicitHeight = 23
                  end
                  inherited sbIFFTransponderControlModeOn: TSpeedButton
                    Left = 10
                    Top = 32
                    Width = 89
                    Height = 23
                    OnClick = fmSensor1sbIFFTransponderControlModeOnClick
                    ExplicitLeft = 10
                    ExplicitTop = 32
                    ExplicitWidth = 89
                    ExplicitHeight = 23
                  end
                  inherited cbIFFTransponderControlMode1: TCheckBox
                    Top = 105
                    ExplicitTop = 105
                  end
                  inherited cbIFFTransponderControlMode2: TCheckBox
                    Top = 127
                    ExplicitTop = 127
                  end
                  inherited cbIFFTransponderControlMode3: TCheckBox
                    Top = 149
                    ExplicitTop = 149
                  end
                  inherited cbIFFTransponderControlMode3C: TCheckBox
                    Top = 170
                    ExplicitTop = 170
                  end
                  inherited cbIFFTransponderControlMode4: TCheckBox
                    Top = 191
                    ExplicitTop = 191
                  end
                  inherited edtIFFTransponderControlMode1: TEdit
                    Top = 103
                    Text = '0000'
                    ExplicitTop = 103
                  end
                  inherited edtIFFTransponderControlMode2: TEdit
                    Top = 125
                    ExplicitTop = 125
                  end
                  inherited edtIFFTransponderControlMode3: TEdit
                    Top = 147
                    ExplicitTop = 147
                  end
                end
              end
              inherited grbESMSensorControl: TGroupBox [3]
                Width = 323
                Height = 1095
                ExplicitWidth = 323
                ExplicitHeight = 1075
                inherited ScrollBox4: TScrollBox
                  Width = 319
                  Height = 1078
                  ExplicitWidth = 319
                  ExplicitHeight = 1058
                  inherited Label7: TLabel
                    Left = 48
                    Top = 19
                    ExplicitLeft = 48
                    ExplicitTop = 19
                  end
                  inherited Label8: TLabel
                    Top = 87
                    ExplicitTop = 87
                  end
                  inherited Bevel4: TBevel
                    Top = 94
                    ExplicitTop = 94
                  end
                  inherited Label9: TLabel
                    Left = 32
                    Top = 103
                    ExplicitLeft = 32
                    ExplicitTop = 103
                  end
                  inherited sbESMSensorControlModeOn: TSpeedButton
                    Left = 15
                    Top = 34
                    Width = 89
                    Height = 23
                    OnClick = btnESMOnClick
                    ExplicitLeft = 15
                    ExplicitTop = 34
                    ExplicitWidth = 89
                    ExplicitHeight = 23
                  end
                  inherited sbESMSensorControlModeOff: TSpeedButton
                    Left = 15
                    Top = 55
                    Width = 89
                    Height = 23
                    OnClick = btnESMOnClick
                    ExplicitLeft = 15
                    ExplicitTop = 55
                    ExplicitWidth = 89
                    ExplicitHeight = 23
                  end
                  inherited sbESMSensorDisplayBlindZoneShow: TSpeedButton
                    Top = 118
                    Width = 89
                    Height = 23
                    OnClick = btnESMOnClick
                    ExplicitTop = 118
                    ExplicitWidth = 89
                    ExplicitHeight = 23
                  end
                  inherited sbESMSensorDisplayBlindZoneHide: TSpeedButton
                    Top = 139
                    Width = 89
                    Height = 23
                    OnClick = btnESMOnClick
                    ExplicitTop = 139
                    ExplicitWidth = 89
                    ExplicitHeight = 23
                  end
                end
              end
              inherited grbElectroOpticalSensor: TGroupBox [4]
                Width = 323
                Height = 1095
                ExplicitWidth = 323
                ExplicitHeight = 1075
                inherited ScrollBox3: TScrollBox
                  Width = 319
                  Height = 1078
                  ExplicitWidth = 319
                  ExplicitHeight = 1058
                  inherited Label2: TLabel
                    Left = 54
                    Top = 20
                    ExplicitLeft = 54
                    ExplicitTop = 20
                  end
                  inherited Label4: TLabel
                    Left = 161
                    Top = 102
                    ExplicitLeft = 161
                    ExplicitTop = 102
                  end
                  inherited Label5: TLabel
                    Left = 64
                    Top = 102
                    ExplicitLeft = 64
                    ExplicitTop = 102
                  end
                  inherited sbElectroOpticalSensorDisplayRangeShow: TSpeedButton
                    Top = 118
                    Width = 89
                    Height = 23
                    OnClick = btnEOOnClick
                    ExplicitTop = 118
                    ExplicitWidth = 89
                    ExplicitHeight = 23
                  end
                  inherited sbElectroOpticalSensorDisplayRangeHide: TSpeedButton
                    Top = 139
                    Width = 89
                    Height = 23
                    OnClick = btnEOOnClick
                    ExplicitTop = 139
                    ExplicitWidth = 89
                    ExplicitHeight = 23
                  end
                  inherited sbElectroOpticalSensorBlindZoneShow: TSpeedButton
                    Top = 118
                    Width = 89
                    Height = 23
                    OnClick = btnEOOnClick
                    ExplicitTop = 118
                    ExplicitWidth = 89
                    ExplicitHeight = 23
                  end
                  inherited sbElectroOpticalSensorBlindZoneHide: TSpeedButton
                    Top = 139
                    Width = 89
                    Height = 23
                    OnClick = btnEOOnClick
                    ExplicitTop = 139
                    ExplicitWidth = 89
                    ExplicitHeight = 23
                  end
                  inherited sbElectroOpticalSensorControlModeOn: TSpeedButton
                    Left = 33
                    Top = 36
                    Width = 89
                    Height = 23
                    OnClick = btnEOOnClick
                    ExplicitLeft = 33
                    ExplicitTop = 36
                    ExplicitWidth = 89
                    ExplicitHeight = 23
                  end
                  inherited sbElectroOpticalSensorControlModeOff: TSpeedButton
                    Left = 33
                    Top = 57
                    Width = 89
                    Height = 23
                    OnClick = btnEOOnClick
                    ExplicitLeft = 33
                    ExplicitTop = 57
                    ExplicitWidth = 89
                    ExplicitHeight = 23
                  end
                end
              end
              inherited grbAnomalyDetectorSensor: TGroupBox [5]
                Width = 323
                Height = 1095
                ExplicitWidth = 323
                ExplicitHeight = 1075
                inherited Label230: TLabel
                  Left = 48
                  ExplicitLeft = 48
                end
                inherited sbAnomalyDetectorControlModeOn: TSpeedButton
                  Left = 16
                  Width = 89
                  Height = 23
                  OnClick = btnMADOnClick
                  ExplicitLeft = 16
                  ExplicitWidth = 89
                  ExplicitHeight = 23
                end
                inherited sbAnomalyDetectorControlModeOff: TSpeedButton
                  Left = 16
                  Width = 89
                  Height = 23
                  OnClick = btnMADOnClick
                  ExplicitLeft = 16
                  ExplicitWidth = 89
                  ExplicitHeight = 23
                end
              end
              inherited grbSonobuoyControl: TGroupBox [6]
                Width = 323
                Height = 1095
                ExplicitWidth = 323
                ExplicitHeight = 1075
                inherited ScrollBox7: TScrollBox
                  Width = 319
                  Height = 1078
                  ExplicitWidth = 319
                  ExplicitHeight = 1058
                  inherited Bevel12: TBevel
                    Top = 155
                    ExplicitTop = 155
                  end
                  inherited btnSonobuoyControlCombo: TSpeedButton
                    Left = 278
                    Width = 25
                    ExplicitLeft = 278
                    ExplicitWidth = 25
                  end
                  inherited Label21: TLabel
                    Top = 98
                    ExplicitTop = 98
                  end
                  inherited Label22: TLabel
                    Top = 98
                    ExplicitTop = 98
                  end
                  inherited Label23: TLabel
                    Top = 147
                    ExplicitTop = 147
                  end
                  inherited Label24: TLabel
                    Top = 166
                    ExplicitTop = 166
                  end
                  inherited Label25: TLabel
                    Top = 188
                    ExplicitTop = 188
                  end
                  inherited Label26: TLabel
                    Top = 166
                    ExplicitTop = 166
                  end
                  inherited Label27: TLabel
                    Top = 188
                    ExplicitTop = 188
                  end
                  inherited lblSonobuoyMonitorCurrently: TLabel
                    Top = 188
                    ExplicitTop = 188
                  end
                  inherited lblSonobuoyMonitorPlatform: TLabel
                    Top = 166
                    ExplicitTop = 166
                  end
                  inherited btnSonobuoyControlDeploy: TButton
                    Left = 220
                    Top = 122
                    Width = 84
                    Height = 24
                    OnClick = OnSoonobuoyBtnClick
                    ExplicitLeft = 220
                    ExplicitTop = 122
                    ExplicitWidth = 84
                    ExplicitHeight = 24
                  end
                  inherited editControlDepth: TEdit
                    Top = 94
                    ExplicitTop = 94
                  end
                end
              end
              inherited grbSearchRadarControl: TGroupBox [7]
                Width = 323
                Height = 1095
                ExplicitWidth = 323
                ExplicitHeight = 1075
                inherited ScrollBox1: TScrollBox
                  Width = 319
                  Height = 1078
                  ExplicitWidth = 319
                  ExplicitHeight = 1058
                  inherited Bevel28: TBevel
                    Left = 45
                    Width = 251
                    ExplicitLeft = 45
                    ExplicitWidth = 251
                  end
                  inherited Label193: TLabel
                    Left = 41
                    Top = 12
                    ExplicitLeft = 41
                    ExplicitTop = 12
                  end
                  inherited Label194: TLabel
                    Left = 236
                    Top = 14
                    ExplicitLeft = 236
                    ExplicitTop = 14
                  end
                  inherited Label261: TLabel
                    Top = 184
                    ExplicitTop = 184
                  end
                  inherited Bevel31: TBevel
                    Left = 46
                    Top = 189
                    Width = 248
                    ExplicitLeft = 46
                    ExplicitTop = 189
                    ExplicitWidth = 248
                  end
                  inherited Label262: TLabel
                    Left = 47
                    Top = 194
                    ExplicitLeft = 47
                    ExplicitTop = 194
                  end
                  inherited Label263: TLabel
                    Left = 136
                    Top = 194
                    ExplicitLeft = 136
                    ExplicitTop = 194
                  end
                  inherited Label264: TLabel
                    Left = 231
                    Top = 194
                    ExplicitLeft = 231
                    ExplicitTop = 194
                  end
                  inherited btnControlComboInterval: TSpeedButton
                    Left = 266
                    Top = 91
                    Width = 24
                    Height = 21
                    ExplicitLeft = 266
                    ExplicitTop = 91
                    ExplicitWidth = 24
                    ExplicitHeight = 21
                  end
                  inherited sbControlEccmOn: TSpeedButton
                    Left = 207
                    Top = 29
                    Width = 83
                    Height = 23
                    OnClick = OnRadarBtnClick
                    ExplicitLeft = 207
                    ExplicitTop = 29
                    ExplicitWidth = 83
                    ExplicitHeight = 23
                  end
                  inherited sbControlEccmOff: TSpeedButton
                    Left = 207
                    Top = 50
                    Width = 83
                    Height = 23
                    OnClick = OnRadarBtnClick
                    ExplicitLeft = 207
                    ExplicitTop = 50
                    ExplicitWidth = 83
                    ExplicitHeight = 23
                  end
                  inherited sbRangeShow: TSpeedButton
                    Left = 21
                    Top = 208
                    Width = 86
                    Height = 23
                    OnClick = OnRadarBtnClick
                    ExplicitLeft = 21
                    ExplicitTop = 208
                    ExplicitWidth = 86
                    ExplicitHeight = 23
                  end
                  inherited sbRangeHide: TSpeedButton
                    Left = 21
                    Top = 229
                    Width = 86
                    Height = 23
                    OnClick = OnRadarBtnClick
                    ExplicitLeft = 21
                    ExplicitTop = 229
                    ExplicitWidth = 86
                    ExplicitHeight = 23
                  end
                  inherited sbBlindShow: TSpeedButton
                    Left = 119
                    Top = 208
                    Width = 86
                    Height = 23
                    OnClick = OnRadarBtnClick
                    ExplicitLeft = 119
                    ExplicitTop = 208
                    ExplicitWidth = 86
                    ExplicitHeight = 23
                  end
                  inherited sbBlindHide: TSpeedButton
                    Left = 119
                    Top = 229
                    Width = 86
                    Height = 23
                    OnClick = OnRadarBtnClick
                    ExplicitLeft = 119
                    ExplicitTop = 229
                    ExplicitWidth = 86
                    ExplicitHeight = 23
                  end
                  inherited sbScanShow: TSpeedButton
                    Left = 214
                    Top = 208
                    Width = 86
                    Height = 23
                    OnClick = OnRadarBtnClick
                    ExplicitLeft = 214
                    ExplicitTop = 208
                    ExplicitWidth = 86
                    ExplicitHeight = 23
                  end
                  inherited sbScanHide: TSpeedButton
                    Left = 214
                    Top = 229
                    Width = 86
                    Height = 23
                    OnClick = OnRadarBtnClick
                    ExplicitLeft = 214
                    ExplicitTop = 229
                    ExplicitWidth = 86
                    ExplicitHeight = 23
                  end
                  inherited pnlControlRadar: TPanel [18]
                    Left = 12
                    Top = 27
                    Width = 88
                    Height = 67
                    ExplicitLeft = 12
                    ExplicitTop = 27
                    ExplicitWidth = 88
                    ExplicitHeight = 67
                    inherited btnControlModeOff2: TSpeedButton
                      Left = 0
                      Top = 21
                      Width = 85
                      ExplicitLeft = 0
                      ExplicitTop = 21
                      ExplicitWidth = 85
                    end
                    inherited btnControlModeOn: TSpeedButton
                      Left = 0
                      Top = 0
                      Width = 85
                      Height = 23
                      ExplicitLeft = 0
                      ExplicitTop = 0
                      ExplicitWidth = 85
                      ExplicitHeight = 23
                    end
                  end
                  inherited pnlControlModeRadar2: TPanel [19]
                    Left = 11
                    Top = 27
                    Height = 68
                    ExplicitLeft = 11
                    ExplicitTop = 27
                    ExplicitHeight = 68
                    inherited sbControlModeSearch: TSpeedButton
                      Left = 1
                      Top = 2
                      Width = 86
                      Height = 23
                      OnClick = OnRadarBtnClick
                      ExplicitLeft = 1
                      ExplicitTop = 2
                      ExplicitWidth = 86
                      ExplicitHeight = 23
                    end
                    inherited sbControlModeTrack: TSpeedButton
                      Left = 1
                      Top = 23
                      Width = 86
                      Height = 23
                      OnClick = OnRadarBtnClick
                      ExplicitLeft = 1
                      ExplicitTop = 23
                      ExplicitWidth = 86
                      ExplicitHeight = 23
                    end
                    inherited sbControlModeOff: TSpeedButton
                      Left = 1
                      Top = 44
                      Width = 86
                      Height = 23
                      OnClick = OnRadarBtnClick
                      ExplicitLeft = 1
                      ExplicitTop = 44
                      ExplicitWidth = 86
                      ExplicitHeight = 23
                    end
                  end
                  inherited pnlScanSector: TPanel [20]
                    Left = 6
                    Top = 115
                    Height = 65
                    ExplicitLeft = 6
                    ExplicitTop = 115
                    ExplicitHeight = 65
                    inherited Bevel30: TBevel
                      Top = 5
                      Width = 194
                      ExplicitTop = 5
                      ExplicitWidth = 194
                    end
                    inherited Label200: TLabel
                      Top = -3
                      ExplicitTop = -3
                    end
                    inherited Label201: TLabel
                      Left = 40
                      Top = 11
                      ExplicitLeft = 40
                      ExplicitTop = 11
                    end
                    inherited sbScanModePartial: TSpeedButton
                      Left = 12
                      Top = 43
                      Width = 79
                      OnClick = OnRadarBtnClick
                      ExplicitLeft = 12
                      ExplicitTop = 43
                      ExplicitWidth = 79
                    end
                    inherited sbScanModeFull: TSpeedButton
                      Left = 12
                      Top = 23
                      Width = 79
                      OnClick = OnRadarBtnClick
                      ExplicitLeft = 12
                      ExplicitTop = 23
                      ExplicitWidth = 79
                    end
                    inherited Label202: TLabel
                      Left = 129
                      Top = 21
                      ExplicitLeft = 129
                      ExplicitTop = 21
                    end
                    inherited Label203: TLabel
                      Left = 129
                      Top = 42
                      ExplicitLeft = 129
                      ExplicitTop = 42
                    end
                    inherited Label205: TLabel
                      Left = 203
                      Top = 43
                      ExplicitLeft = 203
                      ExplicitTop = 43
                    end
                    inherited Label204: TLabel
                      Left = 202
                      Top = 21
                      ExplicitLeft = 202
                      ExplicitTop = 21
                    end
                    inherited btnComboScanStrart: TSpeedButton
                      Left = 260
                      Top = 19
                      Width = 24
                      Height = 20
                      ExplicitLeft = 260
                      ExplicitTop = 19
                      ExplicitWidth = 24
                      ExplicitHeight = 20
                    end
                    inherited editScanStart: TEdit
                      Left = 166
                      Top = 17
                      ParentFont = False
                      ExplicitLeft = 166
                      ExplicitTop = 17
                    end
                    inherited editScanEnd: TEdit
                      Left = 166
                      Top = 38
                      ParentFont = False
                      ExplicitLeft = 166
                      ExplicitTop = 38
                    end
                  end
                  inherited cbActivationInterval: TCheckBox
                    Left = 23
                    Top = 96
                    Height = 14
                    ExplicitLeft = 23
                    ExplicitTop = 96
                    ExplicitHeight = 14
                  end
                  inherited editComboInterval: TEdit
                    Left = 135
                    Top = 92
                    ExplicitLeft = 135
                    ExplicitTop = 92
                  end
                  inherited btShowRangeAltitude: TButton
                    Left = 23
                    Top = 253
                    Height = 21
                    OnClick = OnRadarBtnClick
                    ExplicitLeft = 23
                    ExplicitTop = 253
                    ExplicitHeight = 21
                  end
                  inherited btExecuteSingleScan: TButton
                    Left = 188
                    Top = 253
                    Height = 21
                    OnClick = OnRadarBtnClick
                    ExplicitLeft = 188
                    ExplicitTop = 253
                    ExplicitHeight = 21
                  end
                end
              end
              inherited grbIFFInterrogatorControl: TGroupBox [8]
                Width = 323
                Height = 1095
                ExplicitWidth = 323
                ExplicitHeight = 1075
                inherited ScrollBox5: TScrollBox
                  Width = 319
                  Height = 1078
                  ExplicitWidth = 319
                  ExplicitHeight = 1058
                  inherited Bevel5: TBevel
                    Top = 6
                    ExplicitTop = 6
                  end
                  inherited Bevel6: TBevel
                    Top = 119
                    ExplicitTop = 119
                  end
                  inherited Bevel7: TBevel
                    Top = 78
                    ExplicitTop = 78
                  end
                  inherited btnIFFInterrogatorTrackSearch: TSpeedButton
                    Left = 93
                    Top = 84
                    Width = 32
                    Height = 24
                    OnClick = fmSensor1btnIFFInterrogatorTrackSearchClick
                    ExplicitLeft = 93
                    ExplicitTop = 84
                    ExplicitWidth = 32
                    ExplicitHeight = 24
                  end
                  inherited Label10: TLabel
                    Top = -1
                    ExplicitTop = -1
                  end
                  inherited Label11: TLabel
                    Left = 47
                    Top = 11
                    ExplicitLeft = 47
                    ExplicitTop = 11
                  end
                  inherited Label12: TLabel
                    Top = 111
                    ExplicitTop = 111
                  end
                  inherited Label13: TLabel
                    Top = 70
                    ExplicitTop = 70
                  end
                  inherited sbIFFInterrogatorControlModeOff: TSpeedButton
                    Left = 16
                    Top = 46
                    Width = 89
                    Height = 23
                    OnClick = fmSensor1sbIFFInterrogatorControlModeOffClick
                    ExplicitLeft = 16
                    ExplicitTop = 46
                    ExplicitWidth = 89
                    ExplicitHeight = 23
                  end
                  inherited sbIFFInterrogatorControlModeOn: TSpeedButton
                    Left = 16
                    Top = 25
                    Width = 89
                    Height = 23
                    OnClick = fmSensor1sbIFFInterrogatorControlModeOnClick
                    ExplicitLeft = 16
                    ExplicitTop = 25
                    ExplicitWidth = 89
                    ExplicitHeight = 23
                  end
                  inherited btnManual: TSpeedButton
                    Left = 154
                    Top = 48
                    Width = 89
                    Height = 23
                    ExplicitLeft = 154
                    ExplicitTop = 48
                    ExplicitWidth = 89
                    ExplicitHeight = 23
                  end
                  inherited btnAutomatic: TSpeedButton
                    Left = 154
                    Top = 27
                    Width = 89
                    Height = 23
                    ExplicitLeft = 154
                    ExplicitTop = 27
                    ExplicitWidth = 89
                    ExplicitHeight = 23
                  end
                  inherited lblInterrogation: TLabel
                    Left = 167
                    Top = 13
                    ExplicitLeft = 167
                    ExplicitTop = 13
                  end
                  inherited cbbtnIFFInterrogatorMode1: TCheckBox
                    Left = 13
                    Top = 130
                    OnClick = fmSensor1cbbtnIFFInterrogatorMode1Click
                    ExplicitLeft = 13
                    ExplicitTop = 130
                  end
                  inherited cbbtnIFFInterrogatorMode2: TCheckBox
                    Left = 13
                    Top = 151
                    OnClick = fmSensor1cbbtnIFFInterrogatorMode1Click
                    ExplicitLeft = 13
                    ExplicitTop = 151
                  end
                  inherited cbbtnIFFInterrogatorMode3: TCheckBox
                    Left = 13
                    Top = 172
                    OnClick = fmSensor1cbbtnIFFInterrogatorMode1Click
                    ExplicitLeft = 13
                    ExplicitTop = 172
                  end
                  inherited cbbtnIFFInterrogatorMode3C: TCheckBox
                    Left = 13
                    Top = 192
                    OnClick = fmSensor1cbbtnIFFInterrogatorMode1Click
                    ExplicitLeft = 13
                    ExplicitTop = 192
                  end
                  inherited cbbtnIFFInterrogatorMode4: TCheckBox
                    Left = 13
                    Top = 212
                    OnClick = fmSensor1cbbtnIFFInterrogatorMode1Click
                    ExplicitLeft = 13
                    ExplicitTop = 212
                  end
                  inherited editbtnIFFInterrogatorTrack: TEdit
                    Left = 12
                    Top = 86
                    ExplicitLeft = 12
                    ExplicitTop = 86
                  end
                  inherited edtIFFInterrogatorMode1: TEdit
                    Top = 127
                    OnKeyPress = fmSensor1edtIFFInterrogatorMode1KeyPress
                    ExplicitTop = 127
                  end
                  inherited edtIFFInterrogatorMode2: TEdit
                    Left = 87
                    Top = 149
                    OnKeyPress = fmSensor1edtIFFInterrogatorMode1KeyPress
                    ExplicitLeft = 87
                    ExplicitTop = 149
                  end
                  inherited edtIFFInterrogatorMode3: TEdit
                    Left = 87
                    Top = 171
                    OnKeyPress = fmSensor1edtIFFInterrogatorMode1KeyPress
                    ExplicitLeft = 87
                    ExplicitTop = 171
                  end
                end
              end
            end
          end
          inherited pmModeSonobuoy: TPopupMenu
            Left = 232
            Top = 440
          end
        end
      end
      object tsWeapon: TTabSheet
        ImageIndex = 3
        ExplicitHeight = 1192
        object VrAnalogClock1: TVrAnalogClock
          Left = 136
          Top = 176
          Width = 90
          Height = 90
          Threaded = True
          Visible = False
        end
        inline fmWeapon1: TfmWeapon
          Left = 0
          Top = 0
          Width = 329
          Height = 1212
          Align = alClient
          TabOrder = 0
          ExplicitWidth = 329
          ExplicitHeight = 1192
          inherited PanelWeaponChoices: TPanel
            Width = 329
            ExplicitWidth = 329
            inherited btnWeapon: TSpeedButton
              OnClick = fmWeapon1btnWeaponClick
            end
          end
          inherited PanelALL: TPanel
            Width = 329
            Height = 1190
            ExplicitWidth = 329
            ExplicitHeight = 1170
            inherited PanelTacticalWeapons: TPanel
              Width = 329
              Height = 1190
              ExplicitWidth = 329
              ExplicitHeight = 1170
              inherited grbTacticalAcousticTorpedos: TGroupBox [0]
                Width = 329
                Height = 1190
                ExplicitWidth = 329
                ExplicitHeight = 1170
                inherited Label439: TLabel
                  Left = 5
                  Top = 8
                  ExplicitLeft = 5
                  ExplicitTop = 8
                end
                inherited lblTacticalAcousticTorpedosStatus: TLabel
                  Left = 23
                  Top = 25
                  ExplicitLeft = 23
                  ExplicitTop = 25
                end
                inherited Label455: TLabel
                  Left = 125
                  Top = 25
                  ExplicitLeft = 125
                  ExplicitTop = 25
                end
                inherited lbTacticalAcousticTorpedosQuantity: TLabel
                  Left = 181
                  Top = 25
                  ExplicitLeft = 181
                  ExplicitTop = 25
                end
                inherited Label460: TLabel
                  Left = 5
                  Top = 100
                  ExplicitLeft = 5
                  ExplicitTop = 100
                end
                inherited Bevel91: TBevel
                  Left = 45
                  Top = 108
                  Width = 253
                  ExplicitLeft = 45
                  ExplicitTop = 108
                  ExplicitWidth = 253
                end
                inherited Label461: TLabel
                  Left = 23
                  Top = 122
                  ExplicitLeft = 23
                  ExplicitTop = 122
                end
                inherited Label462: TLabel
                  Left = 5
                  Top = 171
                  ExplicitLeft = 5
                  ExplicitTop = 171
                end
                inherited Bevel92: TBevel
                  Left = 47
                  Top = 180
                  Width = 253
                  ExplicitLeft = 47
                  ExplicitTop = 180
                  ExplicitWidth = 253
                end
                inherited Label463: TLabel
                  Left = 50
                  Top = 186
                  ExplicitLeft = 50
                  ExplicitTop = 186
                end
                inherited Label464: TLabel
                  Left = 151
                  Top = 186
                  ExplicitLeft = 151
                  ExplicitTop = 186
                end
                inherited Label465: TLabel
                  Left = 23
                  Top = 145
                  ExplicitLeft = 23
                  ExplicitTop = 145
                end
                inherited lbTacticalAcousticTorpedosTargetIdentity: TLabel
                  Left = 128
                  Top = 145
                  ExplicitLeft = 128
                  ExplicitTop = 145
                end
                inherited Bevel93: TBevel
                  Left = 43
                  Top = 16
                  Width = 253
                  ExplicitLeft = 43
                  ExplicitTop = 16
                  ExplicitWidth = 253
                end
                inherited Label459: TLabel
                  Left = 5
                  Top = 48
                  ExplicitLeft = 5
                  ExplicitTop = 48
                end
                inherited Bevel94: TBevel
                  Left = 42
                  Top = 55
                  Width = 253
                  ExplicitLeft = 42
                  ExplicitTop = 55
                  ExplicitWidth = 253
                end
                inherited Label476: TLabel
                  Left = 23
                  Top = 74
                  ExplicitLeft = 23
                  ExplicitTop = 74
                end
                inherited btnTacticalAcousticTorpedosTargetTrack: TSpeedButton
                  Left = 189
                  Top = 117
                  ExplicitLeft = 189
                  ExplicitTop = 117
                end
                inherited sbTacticalAcousticTorpedosDisplayRangeShow: TSpeedButton
                  Left = 27
                  Top = 202
                  Height = 22
                  ExplicitLeft = 27
                  ExplicitTop = 202
                  ExplicitHeight = 22
                end
                inherited sbTacticalAcousticTorpedosDisplayRangeHide: TSpeedButton
                  Left = 27
                  Top = 221
                  Height = 22
                  ExplicitLeft = 27
                  ExplicitTop = 221
                  ExplicitHeight = 22
                end
                inherited sbTacticalAcousticTorpedosDisplayBlindShow: TSpeedButton
                  Left = 139
                  Top = 202
                  Height = 22
                  ExplicitLeft = 139
                  ExplicitTop = 202
                  ExplicitHeight = 22
                end
                inherited sbTacticalAcousticTorpedosDisplayBlindHide: TSpeedButton
                  Left = 139
                  Top = 221
                  Height = 22
                  ExplicitLeft = 139
                  ExplicitTop = 221
                  ExplicitHeight = 22
                end
                inherited editTacticalAcousticTorpedosTargetTrack: TEdit
                  Left = 128
                  Top = 118
                  ExplicitLeft = 128
                  ExplicitTop = 118
                end
                inherited btnTacticalAcousticTorpedosLaunch: TButton
                  Left = 215
                  Top = 247
                  Height = 22
                  OnClick = fmWeapon1btnAcousticTorpedoLaunchClick
                  ExplicitLeft = 215
                  ExplicitTop = 247
                  ExplicitHeight = 22
                end
                inherited editTacticalAcousticTorpedosSalvo: TEdit
                  Left = 128
                  Top = 70
                  ExplicitLeft = 128
                  ExplicitTop = 70
                end
              end
              inherited grbWireGuidedTorpedo: TGroupBox [1]
                Width = 329
                Height = 1190
                ExplicitWidth = 329
                ExplicitHeight = 1170
                inherited ScrollBox10: TScrollBox
                  Width = 325
                  Height = 1173
                  ExplicitWidth = 325
                  ExplicitHeight = 1153
                  inherited Bevel19: TBevel
                    Left = 48
                    Top = 57
                    ExplicitLeft = 48
                    ExplicitTop = 57
                  end
                  inherited Bevel20: TBevel
                    Left = 45
                    Top = 180
                    Width = 263
                    ExplicitLeft = 45
                    ExplicitTop = 180
                    ExplicitWidth = 263
                  end
                  inherited Bevel21: TBevel
                    Left = 43
                    Top = 16
                    Width = 265
                    ExplicitLeft = 43
                    ExplicitTop = 16
                    ExplicitWidth = 265
                  end
                  inherited btnlWireGuidedTorpedoTargetTrack: TSpeedButton
                    Left = 180
                    Top = 66
                    ExplicitLeft = 180
                    ExplicitTop = 66
                  end
                  inherited Label84: TLabel
                    Left = 8
                    Top = 9
                    ExplicitLeft = 8
                    ExplicitTop = 9
                  end
                  inherited Label85: TLabel
                    Top = 28
                    ExplicitTop = 28
                  end
                  inherited Label86: TLabel
                    Left = 8
                    Top = 50
                    ExplicitLeft = 8
                    ExplicitTop = 50
                  end
                  inherited Label87: TLabel
                    Top = 69
                    ExplicitTop = 69
                  end
                  inherited Label88: TLabel
                    Left = 8
                    Top = 173
                    ExplicitLeft = 8
                    ExplicitTop = 173
                  end
                  inherited Label89: TLabel
                    Left = 92
                    Top = 191
                    ExplicitLeft = 92
                    ExplicitTop = 191
                  end
                  inherited Label90: TLabel
                    Left = 171
                    Top = 191
                    ExplicitLeft = 171
                    ExplicitTop = 191
                  end
                  inherited Label91: TLabel
                    Top = 100
                    ExplicitTop = 100
                  end
                  inherited Label92: TLabel
                    Top = 117
                    ExplicitTop = 117
                  end
                  inherited Label93: TLabel
                    Top = 134
                    ExplicitTop = 134
                  end
                  inherited Label94: TLabel
                    Top = 151
                    ExplicitTop = 151
                  end
                  inherited Label95: TLabel
                    Left = 180
                    Top = 117
                    ExplicitLeft = 180
                    ExplicitTop = 117
                  end
                  inherited Label96: TLabel
                    Left = 180
                    Top = 134
                    ExplicitLeft = 180
                    ExplicitTop = 134
                  end
                  inherited Label97: TLabel
                    Left = 180
                    Top = 151
                    ExplicitLeft = 180
                    ExplicitTop = 151
                  end
                  inherited lblWGQuantity: TLabel
                    Top = 29
                    ExplicitTop = 29
                  end
                  inherited lblWireGuidedTorpedoStatus: TLabel
                    Left = 28
                    Top = 28
                    ExplicitLeft = 28
                    ExplicitTop = 28
                  end
                  inherited lblWireGuidedTorpedoTargetCourse: TLabel
                    Top = 117
                    ExplicitTop = 117
                  end
                  inherited lblWireGuidedTorpedoTargetDepth: TLabel
                    Top = 151
                    ExplicitTop = 151
                  end
                  inherited lblWireGuidedTorpedoTargetGround: TLabel
                    Top = 134
                    ExplicitTop = 134
                  end
                  inherited lblWireGuidedTorpedoTargetIdentity: TLabel
                    Top = 100
                    ExplicitTop = 100
                  end
                  inherited sblWireGuidedTorpedodDisplayRangeHide: TSpeedButton
                    Left = 69
                    Top = 231
                    Width = 77
                    Height = 23
                    ExplicitLeft = 69
                    ExplicitTop = 231
                    ExplicitWidth = 77
                    ExplicitHeight = 23
                  end
                  inherited sblWireGuidedTorpedoDisplayBlindHide: TSpeedButton
                    Left = 159
                    Top = 231
                    Width = 77
                    Height = 23
                    ExplicitLeft = 159
                    ExplicitTop = 231
                    ExplicitWidth = 77
                    ExplicitHeight = 23
                  end
                  inherited sblWireGuidedTorpedoDisplayBlindShow: TSpeedButton
                    Left = 159
                    Top = 207
                    Width = 77
                    Height = 23
                    ExplicitLeft = 159
                    ExplicitTop = 207
                    ExplicitWidth = 77
                    ExplicitHeight = 23
                  end
                  inherited sblWireGuidedTorpedoDisplayRangeShow: TSpeedButton
                    Left = 69
                    Top = 207
                    Width = 77
                    Height = 23
                    ExplicitLeft = 69
                    ExplicitTop = 207
                    ExplicitWidth = 77
                    ExplicitHeight = 23
                  end
                  inherited EdtWGTargetTrack: TEdit [28]
                    Left = 114
                    Top = 66
                    Width = 60
                    Text = '---'
                    ExplicitLeft = 114
                    ExplicitTop = 66
                    ExplicitWidth = 60
                  end
                  inherited btnLaunchWG: TButton [29]
                    Left = 232
                    Top = 266
                    Width = 75
                    Height = 22
                    ExplicitLeft = 232
                    ExplicitTop = 266
                    ExplicitWidth = 75
                    ExplicitHeight = 22
                  end
                end
              end
              inherited grbGunEngagementChaffMode: TGroupBox [2]
                Width = 329
                Height = 1190
                ExplicitWidth = 329
                ExplicitHeight = 1170
                inherited ScrollBox9: TScrollBox
                  Width = 325
                  Height = 1173
                  ExplicitWidth = 325
                  ExplicitHeight = 1153
                  inherited Label365: TLabel
                    Left = 23
                    Top = 112
                    ExplicitLeft = 23
                    ExplicitTop = 112
                  end
                  inherited Label366: TLabel
                    Left = 23
                    Top = 131
                    ExplicitLeft = 23
                    ExplicitTop = 131
                  end
                  inherited Label466: TLabel
                    Left = 23
                    Top = 150
                    ExplicitLeft = 23
                    ExplicitTop = 150
                  end
                  inherited Label467: TLabel
                    Left = 23
                    Top = 174
                    ExplicitLeft = 23
                    ExplicitTop = 174
                  end
                  inherited lbChaffControlQuantity: TLabel
                    Left = 147
                    Top = 131
                    ExplicitLeft = 147
                    ExplicitTop = 131
                  end
                  inherited Label469: TLabel
                    Left = 198
                    Top = 174
                    ExplicitLeft = 198
                    ExplicitTop = 174
                  end
                  inherited Label470: TLabel
                    Left = 7
                    Top = 193
                    ExplicitLeft = 7
                    ExplicitTop = 193
                  end
                  inherited Bevel98: TBevel
                    Left = 44
                    Top = 201
                    ExplicitLeft = 44
                    ExplicitTop = 201
                  end
                  inherited Label471: TLabel
                    Left = 53
                    Top = 208
                    ExplicitLeft = 53
                    ExplicitTop = 208
                  end
                  inherited Label472: TLabel
                    Left = 168
                    Top = 207
                    ExplicitLeft = 168
                    ExplicitTop = 207
                  end
                  inherited btnChaffType: TSpeedButton
                    Left = 225
                    Top = 107
                    ExplicitLeft = 225
                    ExplicitTop = 107
                  end
                  inherited btnChaffBloomPosition: TSpeedButton
                    Left = 276
                    Top = 145
                    ExplicitLeft = 276
                    ExplicitTop = 145
                  end
                  inherited sbGunEngagementChaffContolAuto: TSpeedButton
                    Height = 22
                    ExplicitHeight = 22
                  end
                  inherited sbGunEngagementChaffContolManual: TSpeedButton
                    Top = 60
                    Height = 22
                    ExplicitTop = 60
                    ExplicitHeight = 22
                  end
                  inherited sbChaffDisplayShow: TSpeedButton
                    Left = 29
                    Top = 221
                    Height = 22
                    ExplicitLeft = 29
                    ExplicitTop = 221
                    ExplicitHeight = 22
                  end
                  inherited sbChaffDisplayHide: TSpeedButton
                    Left = 29
                    Top = 240
                    Height = 22
                    ExplicitLeft = 29
                    ExplicitTop = 240
                    ExplicitHeight = 22
                  end
                  inherited sbChaffBlindZoneShow: TSpeedButton
                    Left = 150
                    Top = 221
                    Height = 22
                    ExplicitLeft = 150
                    ExplicitTop = 221
                    ExplicitHeight = 22
                  end
                  inherited sbChaffBlindZoneHide: TSpeedButton
                    Left = 150
                    Top = 240
                    Height = 22
                    ExplicitLeft = 150
                    ExplicitTop = 240
                    ExplicitHeight = 22
                  end
                  inherited sbGunEngagementChaffContolChaff: TSpeedButton
                    Top = 81
                    Height = 22
                    ExplicitTop = 81
                    ExplicitHeight = 22
                  end
                  inherited editChaffControlChaff: TEdit
                    Left = 144
                    Top = 108
                    ExplicitLeft = 144
                    ExplicitTop = 108
                  end
                  inherited editChaffControlBloomPosition: TEdit
                    Left = 144
                    Top = 146
                    ExplicitLeft = 144
                    ExplicitTop = 146
                  end
                  inherited editChaffControlBloomAltitude: TEdit
                    Left = 144
                    Top = 170
                    ExplicitLeft = 144
                    ExplicitTop = 170
                  end
                  inherited btnChaffFire: TButton
                    Left = 113
                    Top = 268
                    Height = 23
                    ExplicitLeft = 113
                    ExplicitTop = 268
                    ExplicitHeight = 23
                  end
                  inherited btnChaffCeaseFire: TButton
                    Left = 210
                    Top = 268
                    Height = 23
                    ExplicitLeft = 210
                    ExplicitTop = 268
                    ExplicitHeight = 23
                  end
                end
              end
              inherited grbSurfaceToAirMissile: TGroupBox [3]
                Width = 329
                Height = 1190
                ExplicitWidth = 329
                ExplicitHeight = 1170
                inherited ScrollBox1: TScrollBox
                  Width = 325
                  Height = 1173
                  ExplicitWidth = 325
                  ExplicitHeight = 1153
                  inherited lblSurfaceToAirStatus: TLabel
                    Left = 21
                    Top = 14
                    ExplicitLeft = 21
                    ExplicitTop = 14
                  end
                  inherited Label2: TLabel
                    Left = 124
                    Top = 14
                    ExplicitLeft = 124
                    ExplicitTop = 14
                  end
                  inherited lbSurfaceToAirStatusQuantity: TLabel
                    Left = 180
                    Top = 14
                    ExplicitLeft = 180
                    ExplicitTop = 14
                  end
                  inherited Label3: TLabel
                    Left = 2
                    Top = 28
                    ExplicitLeft = 2
                    ExplicitTop = 28
                  end
                  inherited Bevel2: TBevel
                    Left = 42
                    Top = 35
                    Width = 260
                    Height = 3
                    ExplicitLeft = 42
                    ExplicitTop = 35
                    ExplicitWidth = 260
                    ExplicitHeight = 3
                  end
                  inherited Label4: TLabel
                    Left = 21
                    Top = 43
                    ExplicitLeft = 21
                    ExplicitTop = 43
                  end
                  inherited Label5: TLabel
                    Left = 3
                    Top = 57
                    ExplicitLeft = 3
                    ExplicitTop = 57
                  end
                  inherited Bevel3: TBevel
                    Left = 40
                    Top = 64
                    ExplicitLeft = 40
                    ExplicitTop = 64
                  end
                  inherited Label6: TLabel
                    Left = 21
                    Top = 76
                    ExplicitLeft = 21
                    ExplicitTop = 76
                  end
                  inherited Label7: TLabel
                    Left = 21
                    Top = 93
                    ExplicitLeft = 21
                    ExplicitTop = 93
                  end
                  inherited Label8: TLabel
                    Left = 21
                    Top = 109
                    ExplicitLeft = 21
                    ExplicitTop = 109
                  end
                  inherited Label9: TLabel
                    Left = 21
                    Top = 126
                    ExplicitLeft = 21
                    ExplicitTop = 126
                  end
                  inherited Label10: TLabel
                    Left = 21
                    Top = 146
                    ExplicitLeft = 21
                    ExplicitTop = 146
                  end
                  inherited Label11: TLabel
                    Left = 21
                    Top = 162
                    ExplicitLeft = 21
                    ExplicitTop = 162
                  end
                  inherited Label12: TLabel
                    Left = 21
                    Top = 178
                    ExplicitLeft = 21
                    ExplicitTop = 178
                  end
                  inherited Label13: TLabel
                    Left = 21
                    Top = 193
                    ExplicitLeft = 21
                    ExplicitTop = 193
                  end
                  inherited Label14: TLabel
                    Left = 6
                    Top = 207
                    ExplicitLeft = 6
                    ExplicitTop = 207
                  end
                  inherited Bevel4: TBevel
                    Left = 43
                    Top = 215
                    ExplicitLeft = 43
                    ExplicitTop = 215
                  end
                  inherited Label15: TLabel
                    Left = 46
                    Top = 219
                    ExplicitLeft = 46
                    ExplicitTop = 219
                  end
                  inherited Label16: TLabel
                    Left = 153
                    Top = 219
                    ExplicitLeft = 153
                    ExplicitTop = 219
                  end
                  inherited lbSurfaceToAirCourse: TLabel
                    Left = 120
                    Top = 94
                    ExplicitLeft = 120
                    ExplicitTop = 94
                  end
                  inherited lbSurfaceToAirGround: TLabel
                    Left = 120
                    Top = 109
                    ExplicitLeft = 120
                    ExplicitTop = 109
                  end
                  inherited lbSurfaceToAirAltitude: TLabel
                    Left = 120
                    Top = 125
                    ExplicitLeft = 120
                    ExplicitTop = 125
                  end
                  inherited lbSurfaceToAirStatus: TLabel
                    Left = 120
                    Top = 164
                    ExplicitLeft = 120
                    ExplicitTop = 164
                  end
                  inherited lbSurfaceToAirTimeToWait: TLabel
                    Left = 120
                    Top = 180
                    ExplicitLeft = 120
                    ExplicitTop = 180
                  end
                  inherited lbSurfaceToAirTimeToIntercept: TLabel
                    Left = 120
                    Top = 195
                    ExplicitLeft = 120
                    ExplicitTop = 195
                  end
                  inherited Label17: TLabel
                    Left = 167
                    Top = 91
                    ExplicitLeft = 167
                    ExplicitTop = 91
                  end
                  inherited Label18: TLabel
                    Left = 167
                    Top = 108
                    ExplicitLeft = 167
                    ExplicitTop = 108
                  end
                  inherited Label19: TLabel
                    Left = 167
                    Top = 125
                    ExplicitLeft = 167
                    ExplicitTop = 125
                  end
                  inherited Label20: TLabel
                    Left = 190
                    Top = 179
                    ExplicitLeft = 190
                    ExplicitTop = 179
                  end
                  inherited Label22: TLabel
                    Left = 190
                    Top = 193
                    ExplicitLeft = 190
                    ExplicitTop = 193
                  end
                  inherited btnSurfaceToAirTargetTrack: TSpeedButton
                    Left = 168
                    Top = 72
                    Height = 18
                    OnClick = fmWeapon1btnSurfaceToAirTargetTrackClick
                    ExplicitLeft = 168
                    ExplicitTop = 72
                    ExplicitHeight = 18
                  end
                  inherited sbSurfaceToAirDisplayRangeShow: TSpeedButton
                    Left = 22
                    Top = 232
                    Width = 78
                    Height = 22
                    OnClick = fmWeapon1btnSurfaceToAirTargetTrackClick
                    ExplicitLeft = 22
                    ExplicitTop = 232
                    ExplicitWidth = 78
                    ExplicitHeight = 22
                  end
                  inherited sbSurfaceToAirDisplayRangeHide: TSpeedButton
                    Left = 22
                    Top = 251
                    Width = 78
                    Height = 22
                    OnClick = fmWeapon1btnSurfaceToAirTargetTrackClick
                    ExplicitLeft = 22
                    ExplicitTop = 251
                    ExplicitWidth = 78
                    ExplicitHeight = 22
                  end
                  inherited sbSurfaceToAirDisplayBlindShow: TSpeedButton
                    Top = 232
                    Width = 78
                    Height = 22
                    OnClick = fmWeapon1btnSurfaceToAirTargetTrackClick
                    ExplicitTop = 232
                    ExplicitWidth = 78
                    ExplicitHeight = 22
                  end
                  inherited sbSurfaceToAirDisplayBlindHide: TSpeedButton
                    Top = 251
                    Width = 78
                    Height = 22
                    OnClick = fmWeapon1btnSurfaceToAirTargetTrackClick
                    ExplicitTop = 251
                    ExplicitWidth = 78
                    ExplicitHeight = 22
                  end
                  inherited ediSurfaceToAirSalvo: TEdit
                    Left = 120
                    Top = 40
                    Width = 67
                    ExplicitLeft = 120
                    ExplicitTop = 40
                    ExplicitWidth = 67
                  end
                  inherited editSurfaceToAirTrack: TEdit
                    Left = 120
                    Top = 72
                    ExplicitLeft = 120
                    ExplicitTop = 72
                  end
                  inherited btSurfaceToAirPlan: TButton
                    Left = 5
                    Top = 278
                    Height = 22
                    OnClick = fmWeapon1btnSurfaceToAirTargetTrackClick
                    ExplicitLeft = 5
                    ExplicitTop = 278
                    ExplicitHeight = 22
                  end
                  inherited btSurfaceToAirLaunch: TButton
                    Left = 236
                    Top = 278
                    Height = 22
                    OnClick = fmWeapon1btnSurfaceToAirTargetTrackClick
                    ExplicitLeft = 236
                    ExplicitTop = 278
                    ExplicitHeight = 22
                  end
                  inherited btSurfaceToAirCancel: TButton
                    Left = 95
                    Top = 278
                    Height = 22
                    OnClick = fmWeapon1btnSurfaceToAirTargetTrackClick
                    ExplicitLeft = 95
                    ExplicitTop = 278
                    ExplicitHeight = 22
                  end
                end
              end
              inherited grbMines: TGroupBox [4]
                Width = 329
                Height = 1190
                ExplicitWidth = 329
                ExplicitHeight = 1170
                inherited Label272: TLabel
                  Left = 8
                  Top = 9
                  ExplicitLeft = 8
                  ExplicitTop = 9
                end
                inherited lblStatusMines: TLabel
                  Left = 25
                  Top = 28
                  ExplicitLeft = 25
                  ExplicitTop = 28
                end
                inherited Label288: TLabel
                  Left = 118
                  Top = 28
                  ExplicitLeft = 118
                  ExplicitTop = 28
                end
                inherited lbMinesQuantity: TLabel
                  Left = 173
                  Top = 28
                  ExplicitLeft = 173
                  ExplicitTop = 28
                end
                inherited Label291: TLabel
                  Left = 8
                  Top = 50
                  ExplicitLeft = 8
                  ExplicitTop = 50
                end
                inherited Bevel58: TBevel
                  Left = 48
                  Top = 57
                  Width = 270
                  ExplicitLeft = 48
                  ExplicitTop = 57
                  ExplicitWidth = 270
                end
                inherited Label292: TLabel
                  Left = 24
                  Top = 69
                  ExplicitLeft = 24
                  ExplicitTop = 69
                end
                inherited Bevel61: TBevel
                  Left = 43
                  Top = 16
                  Width = 275
                  ExplicitLeft = 43
                  ExplicitTop = 16
                  ExplicitWidth = 275
                end
                inherited lbl84: TLabel
                  Left = 183
                  Top = 68
                  ExplicitLeft = 183
                  ExplicitTop = 68
                end
                inherited EdtMinesDepth: TEdit
                  Left = 117
                  Top = 66
                  Width = 60
                  ExplicitLeft = 117
                  ExplicitTop = 66
                  ExplicitWidth = 60
                end
                inherited btnMinesDeploy: TButton
                  Left = 232
                  Top = 101
                  Width = 75
                  Height = 22
                  OnClick = fmWeapon1btnMinesDeployClick
                  ExplicitLeft = 232
                  ExplicitTop = 101
                  ExplicitWidth = 75
                  ExplicitHeight = 22
                end
              end
              inherited grbActivePasiveTorpedo: TGroupBox [5]
                Width = 329
                Height = 1190
                ExplicitWidth = 329
                ExplicitHeight = 1170
                inherited ScrollBox3: TScrollBox
                  Width = 325
                  Height = 1173
                  ExplicitWidth = 325
                  ExplicitHeight = 1153
                  inherited btnHideBlindZoneAPG: TSpeedButton
                    Left = 159
                    Top = 283
                    Width = 77
                    ExplicitLeft = 159
                    ExplicitTop = 283
                    ExplicitWidth = 77
                  end
                  inherited btnShowBlindZoneAPG: TSpeedButton [1]
                    Left = 159
                    Top = 259
                    Width = 77
                    ExplicitLeft = 159
                    ExplicitTop = 259
                    ExplicitWidth = 77
                  end
                  inherited btnHideRangeAPG: TSpeedButton [2]
                    Left = 69
                    Top = 283
                    Width = 77
                    ExplicitLeft = 69
                    ExplicitTop = 283
                    ExplicitWidth = 77
                  end
                  inherited btnShowRangeAPG: TSpeedButton
                    Left = 69
                    Top = 259
                    Width = 77
                    ExplicitLeft = 69
                    ExplicitTop = 259
                    ExplicitWidth = 77
                  end
                  inherited btnTargetTrackAPG: TSpeedButton
                    Left = 180
                    Top = 66
                    ExplicitLeft = 180
                    ExplicitTop = 66
                  end
                  inherited bvl1: TBevel
                    Left = 43
                    Width = 265
                    ExplicitLeft = 43
                    ExplicitWidth = 265
                  end
                  inherited bvl2: TBevel
                    Left = 48
                    Width = 260
                    ExplicitLeft = 48
                    ExplicitWidth = 260
                  end
                  inherited bvl3: TBevel
                    Left = 49
                    Top = 98
                    Width = 259
                    ExplicitLeft = 49
                    ExplicitTop = 98
                    ExplicitWidth = 259
                  end
                  inherited bvl4: TBevel
                    Top = 232
                    Width = 263
                    Height = 3
                    ExplicitTop = 232
                    ExplicitWidth = 263
                    ExplicitHeight = 3
                  end
                  inherited lbl1: TLabel
                    Left = 8
                    ExplicitLeft = 8
                  end
                  inherited lbl12: TLabel
                    Left = 25
                    Top = 132
                    ExplicitLeft = 25
                    ExplicitTop = 132
                  end
                  inherited lbl13: TLabel
                    Left = 25
                    Top = 155
                    ExplicitLeft = 25
                    ExplicitTop = 155
                  end
                  inherited lbl14: TLabel
                    Left = 25
                    Top = 178
                    ExplicitLeft = 25
                    ExplicitTop = 178
                  end
                  inherited lbl15: TLabel
                    Left = 25
                    Top = 200
                    ExplicitLeft = 25
                    ExplicitTop = 200
                  end
                  inherited lbl17: TLabel
                    Left = 180
                    Top = 132
                    ExplicitLeft = 180
                    ExplicitTop = 132
                  end
                  inherited lbl18: TLabel
                    Left = 180
                    Top = 155
                    ExplicitLeft = 180
                    ExplicitTop = 155
                  end
                  inherited lbl19: TLabel
                    Left = 180
                    Top = 178
                    ExplicitLeft = 180
                    ExplicitTop = 178
                  end
                  inherited lbl20: TLabel
                    Left = 180
                    Top = 200
                    ExplicitLeft = 180
                    ExplicitTop = 200
                  end
                  inherited lbl21: TLabel
                    Left = 92
                    Top = 243
                    ExplicitLeft = 92
                    ExplicitTop = 243
                  end
                  inherited lbl22: TLabel
                    Left = 171
                    Top = 243
                    ExplicitLeft = 171
                    ExplicitTop = 243
                  end
                  inherited lbl3: TLabel
                    Left = 117
                    ExplicitLeft = 117
                  end
                  inherited lbl5: TLabel
                    Left = 8
                    ExplicitLeft = 8
                  end
                  inherited lbl6: TLabel
                    Left = 25
                    Top = 69
                    ExplicitLeft = 25
                    ExplicitTop = 69
                  end
                  inherited lbl7: TLabel
                    Left = 8
                    Top = 91
                    ExplicitLeft = 8
                    ExplicitTop = 91
                  end
                  inherited lbl8: TLabel
                    Left = 8
                    Top = 225
                    ExplicitLeft = 8
                    ExplicitTop = 225
                  end
                  inherited lbl9: TLabel
                    Top = 110
                    ExplicitTop = 110
                  end
                  inherited lblQuantityAPG: TLabel
                    Left = 173
                    ExplicitLeft = 173
                  end
                  inherited btn4: TButton
                    Left = 232
                    Top = 129
                    Width = 75
                    Height = 22
                    ExplicitLeft = 232
                    ExplicitTop = 129
                    ExplicitWidth = 75
                    ExplicitHeight = 22
                  end
                  inherited btn5: TButton
                    Left = 232
                    Top = 151
                    Width = 75
                    Height = 22
                    ExplicitLeft = 232
                    ExplicitTop = 151
                    ExplicitWidth = 75
                    ExplicitHeight = 22
                  end
                  inherited btn6: TButton
                    Left = 232
                    Top = 173
                    Width = 75
                    Height = 22
                    ExplicitLeft = 232
                    ExplicitTop = 173
                    ExplicitWidth = 75
                    ExplicitHeight = 22
                  end
                  inherited btn7: TButton
                    Left = 232
                    Top = 195
                    Width = 75
                    Height = 22
                    ExplicitLeft = 232
                    ExplicitTop = 195
                    ExplicitWidth = 75
                    ExplicitHeight = 22
                  end
                  inherited btnLaunchAP: TButton
                    Left = 232
                    Top = 318
                    Width = 75
                    Height = 22
                    ExplicitLeft = 232
                    ExplicitTop = 318
                    ExplicitWidth = 75
                    ExplicitHeight = 22
                  end
                  inherited EdtAPTargetTrack: TEdit
                    Left = 114
                    Top = 66
                    Width = 60
                    ExplicitLeft = 114
                    ExplicitTop = 66
                    ExplicitWidth = 60
                  end
                  inherited EdtSafetyCeilingAP: TEdit
                    Left = 114
                    Top = 173
                    Width = 60
                    ExplicitLeft = 114
                    ExplicitTop = 173
                    ExplicitWidth = 60
                  end
                  inherited EdtSalvoAP: TEdit
                    Left = 114
                    Top = 107
                    Width = 60
                    ExplicitLeft = 114
                    ExplicitTop = 107
                    ExplicitWidth = 60
                  end
                  inherited EdtSearchDepthAP: TEdit
                    Left = 114
                    Top = 151
                    Width = 60
                    ExplicitLeft = 114
                    ExplicitTop = 151
                    ExplicitWidth = 60
                  end
                  inherited EdtSearchRadiusAP: TEdit
                    Top = 129
                    Width = 60
                    ExplicitTop = 129
                    ExplicitWidth = 60
                  end
                  inherited EdtSeekerRangeAP: TEdit
                    Left = 114
                    Top = 195
                    Width = 60
                    ExplicitLeft = 114
                    ExplicitTop = 195
                    ExplicitWidth = 60
                  end
                end
              end
              inherited grbAirDroppedVECTAC: TGroupBox [6]
                Width = 329
                Height = 1190
                ExplicitWidth = 329
                ExplicitHeight = 1170
                inherited ScrollBox13: TScrollBox
                  Width = 325
                  Height = 1173
                  ExplicitWidth = 325
                  ExplicitHeight = 1153
                  inherited Bevel75: TBevel
                    Left = 85
                    Top = 167
                    ExplicitLeft = 85
                    ExplicitTop = 167
                  end
                  inherited Bevel76: TBevel
                    Left = 48
                    ExplicitLeft = 48
                  end
                  inherited btnVectacTargetTrack: TSpeedButton
                    Left = 190
                    Top = 17
                    Height = 21
                    ExplicitLeft = 190
                    ExplicitTop = 17
                    ExplicitHeight = 21
                  end
                  inherited btnVectacWeaponCarrierName: TSpeedButton
                    Left = 295
                    Top = 177
                    Height = 21
                    ExplicitLeft = 295
                    ExplicitTop = 177
                    ExplicitHeight = 21
                  end
                  inherited btnVectacWeaponName: TSpeedButton
                    Left = 295
                    Height = 21
                    ExplicitLeft = 295
                    ExplicitHeight = 21
                  end
                  inherited Label368: TLabel
                    Left = 15
                    Top = 19
                    ExplicitLeft = 15
                    ExplicitTop = 19
                  end
                  inherited Label369: TLabel
                    Top = 160
                    ExplicitTop = 160
                  end
                  inherited Label371: TLabel
                    Left = 15
                    Top = 179
                    ExplicitLeft = 15
                    ExplicitTop = 179
                  end
                  inherited Label375: TLabel
                    Left = 15
                    Top = 205
                    ExplicitLeft = 15
                    ExplicitTop = 205
                  end
                  inherited Label376: TLabel
                    Left = 15
                    Top = 231
                    ExplicitLeft = 15
                    ExplicitTop = 231
                  end
                  inherited Label377: TLabel
                    Left = 15
                    Top = 257
                    ExplicitLeft = 15
                    ExplicitTop = 257
                  end
                  inherited Label378: TLabel
                    Left = 15
                    Top = 283
                    ExplicitLeft = 15
                    ExplicitTop = 283
                  end
                  inherited Label379: TLabel
                    Top = 306
                    ExplicitTop = 306
                  end
                  inherited Label380: TLabel
                    Left = 15
                    Top = 325
                    ExplicitLeft = 15
                    ExplicitTop = 325
                  end
                  inherited Label383: TLabel
                    Left = 160
                    Top = 104
                    ExplicitLeft = 160
                    ExplicitTop = 104
                  end
                  inherited Label384: TLabel
                    Top = 205
                    ExplicitTop = 205
                  end
                  inherited Label385: TLabel
                    Top = 257
                    ExplicitTop = 257
                  end
                  inherited Label386: TLabel
                    Left = 15
                    Top = 50
                    ExplicitLeft = 15
                    ExplicitTop = 50
                  end
                  inherited Label387: TLabel
                    Left = 15
                    Top = 68
                    ExplicitLeft = 15
                    ExplicitTop = 68
                  end
                  inherited Label388: TLabel
                    Left = 15
                    Top = 86
                    ExplicitLeft = 15
                    ExplicitTop = 86
                  end
                  inherited Label389: TLabel
                    Left = 15
                    Top = 104
                    ExplicitLeft = 15
                    ExplicitTop = 104
                  end
                  inherited Label390: TLabel
                    Left = 15
                    Top = 122
                    ExplicitLeft = 15
                    ExplicitTop = 122
                  end
                  inherited Label391: TLabel
                    Left = 15
                    Top = 140
                    ExplicitLeft = 15
                    ExplicitTop = 140
                  end
                  inherited Label392: TLabel
                    Left = 160
                    Top = 122
                    ExplicitLeft = 160
                    ExplicitTop = 122
                  end
                  inherited Label393: TLabel
                    Left = 160
                    Top = 140
                    ExplicitLeft = 160
                    ExplicitTop = 140
                  end
                  inherited Label394: TLabel
                    Top = 231
                    ExplicitTop = 231
                  end
                  inherited Label395: TLabel
                    Top = 283
                    ExplicitTop = 283
                  end
                  inherited Label396: TLabel
                    Left = 15
                    Top = 351
                    ExplicitLeft = 15
                    ExplicitTop = 351
                  end
                  inherited Label398: TLabel
                    Left = 185
                    Top = 351
                    ExplicitLeft = 185
                    ExplicitTop = 351
                  end
                  inherited lbVectacTargetCourse: TLabel
                    Top = 104
                    ExplicitTop = 104
                  end
                  inherited lbVectacTargetDepth: TLabel
                    Top = 140
                    ExplicitTop = 140
                  end
                  inherited lbVectacTargetDoppler: TLabel
                    Top = 86
                    ExplicitTop = 86
                  end
                  inherited lbVectacTargetGround: TLabel
                    Top = 122
                    ExplicitTop = 122
                  end
                  inherited lbVectacTargetIdentity: TLabel
                    Top = 50
                    ExplicitTop = 50
                  end
                  inherited lbVectacTargetPropulsion: TLabel
                    Top = 68
                    ExplicitTop = 68
                  end
                  inherited lbVectacWeaponCarrierAdviced: TLabel
                    Top = 231
                    ExplicitTop = 231
                  end
                  inherited lbVectacWeaponCarrierTime: TLabel
                    Top = 283
                    ExplicitTop = 283
                  end
                  inherited lbVectacWeaponExpiry: TLabel
                    Left = 125
                    Top = 351
                    ExplicitLeft = 125
                    ExplicitTop = 351
                  end
                  inherited btnVectacCancel: TButton
                    Left = 106
                    ExplicitLeft = 106
                  end
                  inherited btnVectacConfirm: TButton
                    Width = 114
                    ExplicitWidth = 114
                  end
                  inherited btnVectacPlan: TButton
                    Left = 10
                    ExplicitLeft = 10
                  end
                  inherited btnVectacWeaponCarrierDrop: TButton
                    Left = 253
                    Top = 255
                    Width = 65
                    Height = 22
                    ExplicitLeft = 253
                    ExplicitTop = 255
                    ExplicitWidth = 65
                    ExplicitHeight = 22
                  end
                  inherited btnVectacWeaponCarrierGround: TButton
                    Left = 253
                    Top = 203
                    Width = 65
                    Height = 22
                    ExplicitLeft = 253
                    ExplicitTop = 203
                    ExplicitWidth = 65
                    ExplicitHeight = 22
                  end
                  inherited editVectacTargetTrack: TEdit
                    Width = 60
                    Enabled = True
                    ReadOnly = True
                    ExplicitWidth = 60
                  end
                  inherited editVectacWeaponCarrierDrop: TEdit
                    Top = 255
                    Enabled = True
                    ReadOnly = True
                    ExplicitTop = 255
                  end
                  inherited editVectacWeaponCarrierGround: TEdit
                    Top = 203
                    Enabled = True
                    ReadOnly = True
                    ExplicitTop = 203
                  end
                  inherited editVectacWeaponCarrierName: TEdit
                    Left = 125
                    Top = 177
                    Width = 163
                    Enabled = True
                    ReadOnly = True
                    ExplicitLeft = 125
                    ExplicitTop = 177
                    ExplicitWidth = 163
                  end
                  inherited editVectacWeaponName: TEdit
                    Top = 321
                    Width = 165
                    Enabled = True
                    ReadOnly = True
                    Text = ''
                    ExplicitTop = 321
                    ExplicitWidth = 165
                  end
                end
              end
              inherited grbStraightRunningTorpedos: TGroupBox [7]
                Width = 329
                Height = 1190
                ExplicitWidth = 329
                ExplicitHeight = 1170
                inherited ScrollBox4: TScrollBox
                  Width = 325
                  Height = 1173
                  ExplicitWidth = 325
                  ExplicitHeight = 1153
                  inherited btnStraightRunningTorpedosDisplayBlindHide: TSpeedButton
                    Left = 159
                    Top = 228
                    Width = 77
                    Height = 23
                    ExplicitLeft = 159
                    ExplicitTop = 228
                    ExplicitWidth = 77
                    ExplicitHeight = 23
                  end
                  inherited btnStraightRunningTorpedosDisplayBlindShow: TSpeedButton
                    Left = 159
                    Top = 204
                    Width = 77
                    Height = 23
                    ExplicitLeft = 159
                    ExplicitTop = 204
                    ExplicitWidth = 77
                    ExplicitHeight = 23
                  end
                  inherited btnStraightRunningTorpedosDisplayRangeHide: TSpeedButton
                    Left = 69
                    Top = 228
                    Width = 77
                    Height = 23
                    ExplicitLeft = 69
                    ExplicitTop = 228
                    ExplicitWidth = 77
                    ExplicitHeight = 23
                  end
                  inherited btnStraightRunningTorpedosDisplayRangeShow: TSpeedButton
                    Left = 69
                    Top = 204
                    Width = 77
                    Height = 23
                    ExplicitLeft = 69
                    ExplicitTop = 204
                    ExplicitWidth = 77
                    ExplicitHeight = 23
                  end
                  inherited btnStraightRunningTorpedosTargetTrack: TSpeedButton
                    Left = 180
                    Top = 66
                    ExplicitLeft = 180
                    ExplicitTop = 66
                  end
                  inherited bvl14: TBevel
                    Left = 48
                    Top = 57
                    Width = 250
                    ExplicitLeft = 48
                    ExplicitTop = 57
                    ExplicitWidth = 250
                  end
                  inherited bvl15: TBevel
                    Left = 48
                    Top = 177
                    Width = 253
                    ExplicitLeft = 48
                    ExplicitTop = 177
                    ExplicitWidth = 253
                  end
                  inherited bvl16: TBevel
                    Left = 43
                    Top = 16
                    Width = 255
                    ExplicitLeft = 43
                    ExplicitTop = 16
                    ExplicitWidth = 255
                  end
                  inherited lbl62: TLabel
                    Left = 8
                    Top = 9
                    ExplicitLeft = 8
                    ExplicitTop = 9
                  end
                  inherited lbl63: TLabel
                    Left = 117
                    Top = 28
                    ExplicitLeft = 117
                    ExplicitTop = 28
                  end
                  inherited lbl64: TLabel
                    Left = 8
                    Top = 50
                    ExplicitLeft = 8
                    ExplicitTop = 50
                  end
                  inherited lbl65: TLabel
                    Top = 69
                    ExplicitTop = 69
                  end
                  inherited lbl66: TLabel
                    Left = 8
                    Top = 170
                    ExplicitLeft = 8
                    ExplicitTop = 170
                  end
                  inherited lbl67: TLabel
                    Left = 92
                    Top = 188
                    ExplicitLeft = 92
                    ExplicitTop = 188
                  end
                  inherited lbl68: TLabel
                    Left = 171
                    Top = 188
                    ExplicitLeft = 171
                    ExplicitTop = 188
                  end
                  inherited lbl69: TLabel
                    Top = 96
                    ExplicitTop = 96
                  end
                  inherited lbl70: TLabel
                    Top = 113
                    ExplicitTop = 113
                  end
                  inherited lbl71: TLabel
                    Top = 130
                    ExplicitTop = 130
                  end
                  inherited lbl72: TLabel
                    Top = 147
                    ExplicitTop = 147
                  end
                  inherited lbl73: TLabel
                    Left = 180
                    Top = 113
                    ExplicitLeft = 180
                    ExplicitTop = 113
                  end
                  inherited lbl74: TLabel
                    Left = 180
                    Top = 130
                    ExplicitLeft = 180
                    ExplicitTop = 130
                  end
                  inherited lbl75: TLabel
                    Left = 180
                    Top = 147
                    ExplicitLeft = 180
                    ExplicitTop = 147
                  end
                  inherited lblSRQuantity: TLabel
                    Left = 173
                    Top = 28
                    ExplicitLeft = 173
                    ExplicitTop = 28
                  end
                  inherited lblStatusStraightRunningTorpedos: TLabel
                    Left = 25
                    Top = 28
                    ExplicitLeft = 25
                    ExplicitTop = 28
                  end
                  inherited lblStraightRunningTorpedosTargetCourse: TLabel
                    Left = 117
                    Top = 113
                    ExplicitLeft = 117
                    ExplicitTop = 113
                  end
                  inherited lblStraightRunningTorpedosTargetDepth: TLabel
                    Left = 117
                    Top = 147
                    ExplicitLeft = 117
                    ExplicitTop = 147
                  end
                  inherited lblStraightRunningTorpedosTargetGround: TLabel
                    Left = 117
                    Top = 130
                    ExplicitLeft = 117
                    ExplicitTop = 130
                  end
                  inherited lblStraightRunningTorpedosTargetIdentity: TLabel
                    Left = 117
                    Top = 96
                    ExplicitLeft = 117
                    ExplicitTop = 96
                  end
                  inherited btnLaunchSR: TButton
                    Left = 226
                    Top = 263
                    Width = 75
                    Height = 22
                    ExplicitLeft = 226
                    ExplicitTop = 263
                    ExplicitWidth = 75
                    ExplicitHeight = 22
                  end
                  inherited EdtSRTargetTrack: TEdit
                    Left = 114
                    Top = 66
                    Width = 60
                    ExplicitLeft = 114
                    ExplicitTop = 66
                    ExplicitWidth = 60
                  end
                end
              end
              inherited grbAirDroppedTorpedo: TGroupBox [8]
                Width = 329
                Height = 1190
                ExplicitWidth = 329
                ExplicitHeight = 1170
                inherited ScrollBox8: TScrollBox
                  Width = 325
                  Height = 1173
                  ExplicitWidth = 325
                  ExplicitHeight = 1153
                  inherited btnAirDropeedBilndZonesHide: TSpeedButton
                    Left = 160
                    Top = 384
                    Width = 77
                    Height = 23
                    ExplicitLeft = 160
                    ExplicitTop = 384
                    ExplicitWidth = 77
                    ExplicitHeight = 23
                  end
                  inherited btnAirDroppesDisplayBilndZonesShow: TSpeedButton
                    Left = 160
                    Top = 360
                    Width = 77
                    Height = 23
                    ExplicitLeft = 160
                    ExplicitTop = 360
                    ExplicitWidth = 77
                    ExplicitHeight = 23
                  end
                  inherited btnAirDroppesDisplayRangeHide: TSpeedButton
                    Left = 69
                    Top = 384
                    Width = 77
                    Height = 23
                    ExplicitLeft = 69
                    ExplicitTop = 384
                    ExplicitWidth = 77
                    ExplicitHeight = 23
                  end
                  inherited btnAirDroppesDisplayRangeShow: TSpeedButton
                    Left = 69
                    Top = 360
                    Width = 77
                    Height = 23
                    ExplicitLeft = 69
                    ExplicitTop = 360
                    ExplicitWidth = 77
                    ExplicitHeight = 23
                  end
                  inherited btnAirDroppesTargetTrack: TSpeedButton
                    Left = 180
                    Top = 66
                    ExplicitLeft = 180
                    ExplicitTop = 66
                  end
                  inherited bvl10: TBevel
                    Left = 45
                    Top = 333
                    Width = 262
                    ExplicitLeft = 45
                    ExplicitTop = 333
                    ExplicitWidth = 262
                  end
                  inherited bvl11: TBevel
                    Left = 43
                    Top = 16
                    Width = 264
                    ExplicitLeft = 43
                    ExplicitTop = 16
                    ExplicitWidth = 264
                  end
                  inherited bvl12: TBevel
                    Left = 48
                    Top = 57
                    Width = 259
                    ExplicitLeft = 48
                    ExplicitTop = 57
                    ExplicitWidth = 259
                  end
                  inherited bvl13: TBevel
                    Left = 88
                    Top = 272
                    Width = 219
                    ExplicitLeft = 88
                    ExplicitTop = 272
                    ExplicitWidth = 219
                  end
                  inherited bvl9: TBevel
                    Left = 49
                    Top = 180
                    Width = 258
                    ExplicitLeft = 49
                    ExplicitTop = 180
                    ExplicitWidth = 258
                  end
                  inherited lbl38: TLabel
                    Left = 9
                    Top = 9
                    ExplicitLeft = 9
                    ExplicitTop = 9
                  end
                  inherited lbl39: TLabel
                    Left = 117
                    Top = 28
                    ExplicitLeft = 117
                    ExplicitTop = 28
                  end
                  inherited lbl40: TLabel
                    Left = 9
                    Top = 50
                    ExplicitLeft = 9
                    ExplicitTop = 50
                  end
                  inherited lbl41: TLabel
                    Left = 28
                    Top = 69
                    ExplicitLeft = 28
                    ExplicitTop = 69
                  end
                  inherited lbl42: TLabel
                    Left = 8
                    Top = 325
                    ExplicitLeft = 8
                    ExplicitTop = 325
                  end
                  inherited lbl43: TLabel
                    Left = 92
                    Top = 344
                    ExplicitLeft = 92
                    ExplicitTop = 344
                  end
                  inherited lbl44: TLabel
                    Left = 172
                    Top = 344
                    ExplicitLeft = 172
                    ExplicitTop = 344
                  end
                  inherited lbl45: TLabel
                    Left = 28
                    Top = 100
                    ExplicitLeft = 28
                    ExplicitTop = 100
                  end
                  inherited lbl46: TLabel
                    Left = 8
                    Top = 172
                    ExplicitLeft = 8
                    ExplicitTop = 172
                  end
                  inherited lbl47: TLabel
                    Left = 28
                    Top = 192
                    ExplicitLeft = 28
                    ExplicitTop = 192
                  end
                  inherited lbl48: TLabel
                    Left = 28
                    Top = 216
                    ExplicitLeft = 28
                    ExplicitTop = 216
                  end
                  inherited lbl49: TLabel
                    Left = 28
                    Top = 239
                    ExplicitLeft = 28
                    ExplicitTop = 239
                  end
                  inherited lbl50: TLabel
                    Left = 180
                    Top = 192
                    ExplicitLeft = 180
                    ExplicitTop = 192
                  end
                  inherited lbl51: TLabel
                    Left = 180
                    Top = 216
                    ExplicitLeft = 180
                    ExplicitTop = 216
                  end
                  inherited lbl52: TLabel
                    Left = 180
                    Top = 239
                    ExplicitLeft = 180
                    ExplicitTop = 239
                  end
                  inherited lbl53: TLabel
                    Left = 28
                    Top = 117
                    ExplicitLeft = 28
                    ExplicitTop = 117
                  end
                  inherited lbl54: TLabel
                    Left = 28
                    Top = 134
                    ExplicitLeft = 28
                    ExplicitTop = 134
                  end
                  inherited lbl55: TLabel
                    Left = 28
                    Top = 151
                    ExplicitLeft = 28
                    ExplicitTop = 151
                  end
                  inherited lbl56: TLabel
                    Left = 180
                    Top = 117
                    ExplicitLeft = 180
                    ExplicitTop = 117
                  end
                  inherited lbl57: TLabel
                    Left = 180
                    Top = 134
                    ExplicitLeft = 180
                    ExplicitTop = 134
                  end
                  inherited lbl58: TLabel
                    Left = 180
                    Top = 151
                    ExplicitLeft = 180
                    ExplicitTop = 151
                  end
                  inherited lbl59: TLabel
                    Left = 8
                    Top = 265
                    ExplicitLeft = 8
                    ExplicitTop = 265
                  end
                  inherited lbl60: TLabel
                    Left = 50
                    Top = 308
                    ExplicitLeft = 50
                    ExplicitTop = 308
                  end
                  inherited lbl61: TLabel
                    Left = 180
                    Top = 308
                    ExplicitLeft = 180
                    ExplicitTop = 308
                  end
                  inherited lbl85: TLabel
                    Left = 229
                    Top = 80
                    ExplicitLeft = 229
                    ExplicitTop = 80
                  end
                  inherited lblADQuantity: TLabel
                    Left = 173
                    Top = 28
                    ExplicitLeft = 173
                    ExplicitTop = 28
                  end
                  inherited lblAirDproppedTargetCourse: TLabel
                    Left = 117
                    Top = 117
                    ExplicitLeft = 117
                    ExplicitTop = 117
                  end
                  inherited lblAirDproppedTargetDepth: TLabel
                    Left = 117
                    Top = 151
                    ExplicitLeft = 117
                    ExplicitTop = 151
                  end
                  inherited lblAirDproppedTargetForce: TLabel
                    Left = 117
                    Top = 100
                    ExplicitLeft = 117
                    ExplicitTop = 100
                  end
                  inherited lblAirDproppedTargetGroundSpeed: TLabel
                    Left = 117
                    Top = 134
                    ExplicitLeft = 117
                    ExplicitTop = 134
                  end
                  inherited lblAirDroppedStatus: TLabel
                    Left = 25
                    Top = 28
                    ExplicitLeft = 25
                    ExplicitTop = 28
                  end
                  inherited btnDefaultAirDroppedControlSearchCeiling: TButton
                    Left = 232
                    Top = 235
                    Width = 75
                    Height = 22
                    ExplicitLeft = 232
                    ExplicitTop = 235
                    ExplicitWidth = 75
                    ExplicitHeight = 22
                  end
                  inherited btnDefaultAirDroppedSearchDepth: TButton
                    Left = 232
                    Top = 212
                    Width = 75
                    Height = 22
                    ExplicitLeft = 232
                    ExplicitTop = 212
                    ExplicitWidth = 75
                    ExplicitHeight = 22
                  end
                  inherited btnLauchAD: TButton
                    Left = 232
                    Top = 419
                    Width = 75
                    Height = 22
                    ExplicitLeft = 232
                    ExplicitTop = 419
                    ExplicitWidth = 75
                    ExplicitHeight = 22
                  end
                  inherited chkAirDroppedLaunchWhithoutTarget: TCheckBox
                    Left = 210
                    Top = 66
                    ExplicitLeft = 210
                    ExplicitTop = 66
                  end
                  inherited chkAirDroppedUseLaunchPlatformHeading: TCheckBox
                    Left = 28
                    Top = 284
                    ExplicitLeft = 28
                    ExplicitTop = 284
                  end
                  inherited EdtADTargetTrack: TEdit
                    Left = 114
                    Top = 66
                    Width = 60
                    Text = '---'
                    ExplicitLeft = 114
                    ExplicitTop = 66
                    ExplicitWidth = 60
                  end
                  inherited EdtLaunchBearingAD: TEdit
                    Top = 303
                    ExplicitTop = 303
                  end
                  inherited EdtSearchCeilingAD: TEdit
                    Top = 235
                    ExplicitTop = 235
                  end
                  inherited EdtSearchDepthAD: TEdit
                    Top = 212
                    ExplicitTop = 212
                  end
                  inherited EdtSearchRadiusAD: TEdit
                    Top = 189
                    ExplicitTop = 189
                  end
                end
              end
              inherited grbHybridMissile: TGroupBox [9]
                Width = 329
                Height = 1190
                ExplicitWidth = 329
                ExplicitHeight = 1170
                inherited scrlbx1: TScrollBox
                  Width = 325
                  Height = 1173
                  VertScrollBar.Position = 0
                  ExplicitWidth = 325
                  ExplicitHeight = 1153
                  inherited btnAddHybridMissileTargetAimpoint: TSpeedButton
                    OnClick = fmWeapon1btnAddHybridMissileTargetAimpointClick
                  end
                  inherited btnHybridMissileLaunch: TButton
                    Left = 235
                    Top = 382
                    ExplicitLeft = 235
                    ExplicitTop = 382
                  end
                end
              end
              inherited grbAcousticTorpedo: TGroupBox [10]
                Width = 329
                Height = 1190
                ExplicitWidth = 329
                ExplicitHeight = 1170
                inherited ScrollBox2: TScrollBox
                  Width = 325
                  Height = 1173
                  ExplicitWidth = 325
                  ExplicitHeight = 1153
                  inherited lblAcousticTorpedoStatus: TLabel
                    Top = 28
                    ExplicitTop = 28
                  end
                  inherited lblAcousticTorpedoQuantity: TLabel
                    Left = 173
                    Top = 28
                    ExplicitLeft = 173
                    ExplicitTop = 28
                  end
                  inherited lbl4: TLabel
                    Left = 8
                    Top = 9
                    ExplicitLeft = 8
                    ExplicitTop = 9
                  end
                  inherited lbl37: TLabel
                    Left = 171
                    Top = 312
                    ExplicitLeft = 171
                    ExplicitTop = 312
                  end
                  inherited lbl36: TLabel
                    Left = 180
                    Top = 248
                    ExplicitLeft = 180
                    ExplicitTop = 248
                  end
                  inherited lbl35: TLabel
                    Left = 180
                    Top = 226
                    ExplicitLeft = 180
                    ExplicitTop = 226
                  end
                  inherited lbl34: TLabel
                    Left = 180
                    Top = 204
                    ExplicitLeft = 180
                    ExplicitTop = 204
                  end
                  inherited lbl33: TLabel
                    Left = 180
                    Top = 180
                    ExplicitLeft = 180
                    ExplicitTop = 180
                  end
                  inherited lbl32: TLabel
                    Left = 24
                    Top = 270
                    ExplicitLeft = 24
                    ExplicitTop = 270
                  end
                  inherited lbl31: TLabel
                    Left = 24
                    Top = 248
                    ExplicitLeft = 24
                    ExplicitTop = 248
                  end
                  inherited lbl30: TLabel
                    Left = 24
                    Top = 226
                    ExplicitLeft = 24
                    ExplicitTop = 226
                  end
                  inherited lbl29: TLabel
                    Left = 24
                    Top = 204
                    ExplicitLeft = 24
                    ExplicitTop = 204
                  end
                  inherited lbl28: TLabel
                    Left = 26
                    Top = 180
                    ExplicitLeft = 26
                    ExplicitTop = 180
                  end
                  inherited lbl27: TLabel
                    Left = 24
                    Top = 160
                    ExplicitLeft = 24
                    ExplicitTop = 160
                  end
                  inherited lbl26: TLabel
                    Left = 24
                    Top = 138
                    ExplicitLeft = 24
                    ExplicitTop = 138
                  end
                  inherited lbl25: TLabel
                    Left = 24
                    Top = 110
                    ExplicitLeft = 24
                    ExplicitTop = 110
                  end
                  inherited lbl24: TLabel
                    Left = 8
                    Top = 293
                    ExplicitLeft = 8
                    ExplicitTop = 293
                  end
                  inherited lbl23: TLabel
                    Left = 8
                    Top = 91
                    ExplicitLeft = 8
                    ExplicitTop = 91
                  end
                  inherited lbl2: TLabel
                    Left = 92
                    Top = 312
                    ExplicitLeft = 92
                    ExplicitTop = 312
                  end
                  inherited lbl16: TLabel
                    Left = 24
                    Top = 69
                    ExplicitLeft = 24
                    ExplicitTop = 69
                  end
                  inherited lbl11: TLabel
                    Left = 8
                    Top = 50
                    ExplicitLeft = 8
                    ExplicitTop = 50
                  end
                  inherited lbl10: TLabel
                    Left = 117
                    Top = 28
                    ExplicitLeft = 117
                    ExplicitTop = 28
                  end
                  inherited bvl8: TBevel
                    Left = 34
                    Top = 301
                    Width = 273
                    ExplicitLeft = 34
                    ExplicitTop = 301
                    ExplicitWidth = 273
                  end
                  inherited bvl7: TBevel
                    Left = 38
                    Top = 98
                    Width = 269
                    ExplicitLeft = 38
                    ExplicitTop = 98
                    ExplicitWidth = 269
                  end
                  inherited bvl6: TBevel
                    Left = 37
                    Top = 57
                    Width = 270
                    ExplicitLeft = 37
                    ExplicitTop = 57
                    ExplicitWidth = 270
                  end
                  inherited bvl5: TBevel
                    Left = 32
                    Top = 16
                    Width = 275
                    ExplicitLeft = 32
                    ExplicitTop = 16
                    ExplicitWidth = 275
                  end
                  inherited btnSearchTarget: TSpeedButton
                    Left = 180
                    Top = 66
                    ExplicitLeft = 180
                    ExplicitTop = 66
                  end
                  inherited btnRunOutAT: TSpeedButton
                    Left = 180
                    Top = 156
                    Height = 20
                    ExplicitLeft = 180
                    ExplicitTop = 156
                    ExplicitHeight = 20
                  end
                  inherited btnGyroAngleAT: TSpeedButton
                    Left = 180
                    Top = 266
                    Height = 20
                    ExplicitLeft = 180
                    ExplicitTop = 266
                    ExplicitHeight = 20
                  end
                  inherited btnFiringModeAT: TSpeedButton
                    Left = 180
                    Top = 134
                    Height = 20
                    ExplicitLeft = 180
                    ExplicitTop = 134
                    ExplicitHeight = 20
                  end
                  inherited btnDisplayBlindZonesShow: TSpeedButton
                    Left = 159
                    Top = 328
                    Width = 77
                    Height = 23
                    ExplicitLeft = 159
                    ExplicitTop = 328
                    ExplicitWidth = 77
                    ExplicitHeight = 23
                  end
                  inherited btnDisplayBlindZonesHide: TSpeedButton
                    Left = 159
                    Top = 352
                    Width = 77
                    Height = 23
                    ExplicitLeft = 159
                    ExplicitTop = 352
                    ExplicitWidth = 77
                    ExplicitHeight = 23
                  end
                  inherited btnAccousticDisplayRangeShow: TSpeedButton
                    Left = 69
                    Top = 328
                    Width = 77
                    Height = 23
                    ExplicitLeft = 69
                    ExplicitTop = 328
                    ExplicitWidth = 77
                    ExplicitHeight = 23
                  end
                  inherited btnAccousticDisplayRangeHide: TSpeedButton
                    Left = 69
                    Top = 352
                    Width = 77
                    Height = 23
                    Transparent = False
                    ExplicitLeft = 69
                    ExplicitTop = 352
                    ExplicitWidth = 77
                    ExplicitHeight = 23
                  end
                  inherited btnTargetDetails: TButton
                    Left = 232
                    Top = 66
                    Width = 75
                    Height = 22
                    ExplicitLeft = 232
                    ExplicitTop = 66
                    ExplicitWidth = 75
                    ExplicitHeight = 22
                  end
                  inherited EdtSeekerRangeAT: TEdit
                    Left = 117
                    Top = 244
                    Width = 60
                    ExplicitLeft = 117
                    ExplicitTop = 244
                    ExplicitWidth = 60
                  end
                  inherited EdtSearchRadiusAT: TEdit
                    Left = 117
                    Top = 178
                    Width = 60
                    ExplicitLeft = 117
                    ExplicitTop = 178
                    ExplicitWidth = 60
                  end
                  inherited EdtSearchDepthAT: TEdit
                    Left = 117
                    Top = 200
                    Width = 60
                    ExplicitLeft = 117
                    ExplicitTop = 200
                    ExplicitWidth = 60
                  end
                  inherited EdtSafetyCeilingAT: TEdit
                    Left = 117
                    Top = 221
                    Width = 60
                    ExplicitLeft = 117
                    ExplicitTop = 221
                    ExplicitWidth = 60
                  end
                  inherited EdtRunOutAT: TEdit
                    Left = 117
                    Top = 156
                    Width = 60
                    ExplicitLeft = 117
                    ExplicitTop = 156
                    ExplicitWidth = 60
                  end
                  inherited EdtGyroAngleAT: TEdit
                    Left = 117
                    Top = 266
                    Width = 60
                    ExplicitLeft = 117
                    ExplicitTop = 266
                    ExplicitWidth = 60
                  end
                  inherited EdtFiringModeAT: TEdit
                    Left = 117
                    Top = 134
                    Width = 60
                    ExplicitLeft = 117
                    ExplicitTop = 134
                    ExplicitWidth = 60
                  end
                  inherited EdtATTargetTrack: TEdit
                    Left = 117
                    Top = 66
                    Width = 60
                    ExplicitLeft = 117
                    ExplicitTop = 66
                    ExplicitWidth = 60
                  end
                  inherited btnTube4AT: TButton
                    Left = 180
                    Top = 106
                    Width = 20
                    Height = 20
                    ExplicitLeft = 180
                    ExplicitTop = 106
                    ExplicitWidth = 20
                    ExplicitHeight = 20
                  end
                  inherited btnTube3AT: TButton
                    Left = 159
                    Top = 106
                    Width = 20
                    Height = 20
                    ExplicitLeft = 159
                    ExplicitTop = 106
                    ExplicitWidth = 20
                    ExplicitHeight = 20
                  end
                  inherited btnTube2AT: TButton
                    Left = 138
                    Top = 106
                    Width = 20
                    Height = 20
                    ExplicitLeft = 138
                    ExplicitTop = 106
                    ExplicitWidth = 20
                    ExplicitHeight = 20
                  end
                  inherited btnTube1AT: TButton
                    Left = 117
                    Top = 106
                    Width = 20
                    Height = 20
                    ExplicitLeft = 117
                    ExplicitTop = 106
                    ExplicitWidth = 20
                    ExplicitHeight = 20
                  end
                  inherited btntControlGyroAdvised: TButton
                    Left = 232
                    Top = 266
                    Width = 75
                    Height = 22
                    OnClick = fmWeapon1btntControlGyroAdvisedClick
                    ExplicitLeft = 232
                    ExplicitTop = 266
                    ExplicitWidth = 75
                    ExplicitHeight = 22
                  end
                  inherited btnPlanAT: TButton
                    Left = 24
                    Top = 387
                    Width = 75
                    Height = 22
                    ExplicitLeft = 24
                    ExplicitTop = 387
                    ExplicitWidth = 75
                    ExplicitHeight = 22
                  end
                  inherited btnLaunchAT: TButton
                    Left = 232
                    Top = 387
                    Width = 75
                    Height = 22
                    OnClick = fmWeapon1btnAcousticTorpedoLaunchClick
                    ExplicitLeft = 232
                    ExplicitTop = 387
                    ExplicitWidth = 75
                    ExplicitHeight = 22
                  end
                  inherited btnControlSeeker: TButton
                    Left = 232
                    Top = 244
                    Width = 75
                    Height = 22
                    ExplicitLeft = 232
                    ExplicitTop = 244
                    ExplicitWidth = 75
                    ExplicitHeight = 22
                  end
                  inherited btnControlSearchRadius: TButton
                    Left = 232
                    Top = 178
                    Width = 75
                    Height = 22
                    ExplicitLeft = 232
                    ExplicitTop = 178
                    ExplicitWidth = 75
                    ExplicitHeight = 22
                  end
                  inherited btnControlSearchDepth: TButton
                    Left = 232
                    Top = 200
                    Width = 75
                    Height = 22
                    ExplicitLeft = 232
                    ExplicitTop = 200
                    ExplicitWidth = 75
                    ExplicitHeight = 22
                  end
                  inherited btnControlSafety: TButton
                    Left = 232
                    Top = 221
                    Width = 75
                    Height = 22
                    ExplicitLeft = 232
                    ExplicitTop = 221
                    ExplicitWidth = 75
                    ExplicitHeight = 22
                  end
                  inherited btnControlControlRunAdvised: TButton
                    Left = 232
                    Top = 156
                    Width = 75
                    Height = 22
                    ExplicitLeft = 232
                    ExplicitTop = 156
                    ExplicitWidth = 75
                    ExplicitHeight = 22
                  end
                  inherited btnCancelAT: TButton
                    Left = 118
                    Top = 387
                    Width = 75
                    Height = 22
                    ExplicitLeft = 118
                    ExplicitTop = 387
                    ExplicitWidth = 75
                    ExplicitHeight = 22
                  end
                end
              end
              inherited grbWakeHomingTorpedos: TGroupBox [11]
                Width = 329
                Height = 1190
                ExplicitWidth = 329
                ExplicitHeight = 1170
                inherited ScrollBox5: TScrollBox
                  Width = 325
                  Height = 1173
                  ExplicitWidth = 325
                  ExplicitHeight = 1153
                  inherited Bevel17: TBevel
                    Left = 43
                    Top = 16
                    Width = 264
                    ExplicitLeft = 43
                    ExplicitTop = 16
                    ExplicitWidth = 264
                  end
                  inherited Bevel18: TBevel
                    Left = 49
                    Top = 57
                    Width = 258
                    ExplicitLeft = 49
                    ExplicitTop = 57
                    ExplicitWidth = 258
                  end
                  inherited Bevel5: TBevel
                    Left = 48
                    Top = 98
                    Width = 259
                    ExplicitLeft = 48
                    ExplicitTop = 98
                    ExplicitWidth = 259
                  end
                  inherited Bevel6: TBevel
                    Left = 45
                    Top = 230
                    Width = 262
                    ExplicitLeft = 45
                    ExplicitTop = 230
                    ExplicitWidth = 262
                  end
                  inherited btnWakeHomingTargetTrack: TSpeedButton
                    Left = 180
                    Top = 107
                    ExplicitLeft = 180
                    ExplicitTop = 107
                  end
                  inherited Label23: TLabel
                    Left = 8
                    Top = 9
                    ExplicitLeft = 8
                    ExplicitTop = 9
                  end
                  inherited Label25: TLabel
                    Left = 117
                    ExplicitLeft = 117
                  end
                  inherited Label26: TLabel
                    Left = 8
                    Top = 91
                    ExplicitLeft = 8
                    ExplicitTop = 91
                  end
                  inherited Label27: TLabel
                    Top = 110
                    ExplicitTop = 110
                  end
                  inherited Label28: TLabel
                    Left = 8
                    Top = 223
                    ExplicitLeft = 8
                    ExplicitTop = 223
                  end
                  inherited Label29: TLabel
                    Left = 92
                    Top = 241
                    ExplicitLeft = 92
                    ExplicitTop = 241
                  end
                  inherited Label30: TLabel
                    Left = 171
                    Top = 241
                    ExplicitLeft = 171
                    ExplicitTop = 241
                  end
                  inherited Label31: TLabel
                    Top = 140
                    ExplicitTop = 140
                  end
                  inherited Label32: TLabel
                    Top = 157
                    ExplicitTop = 157
                  end
                  inherited Label33: TLabel
                    Top = 178
                    ExplicitTop = 178
                  end
                  inherited Label34: TLabel
                    Top = 199
                    ExplicitTop = 199
                  end
                  inherited Label35: TLabel
                    Top = 157
                    ExplicitTop = 157
                  end
                  inherited Label37: TLabel
                    Top = 178
                    ExplicitTop = 178
                  end
                  inherited Label46: TLabel
                    Top = 199
                    ExplicitTop = 199
                  end
                  inherited Label47: TLabel
                    Left = 8
                    Top = 50
                    ExplicitLeft = 8
                    ExplicitTop = 50
                  end
                  inherited Label55: TLabel
                    Top = 69
                    ExplicitTop = 69
                  end
                  inherited lblWakeHomingTargetIdentity: TLabel
                    Left = 114
                    Top = 140
                    ExplicitLeft = 114
                    ExplicitTop = 140
                  end
                  inherited lblWHQuantity: TLabel
                    Left = 173
                    ExplicitLeft = 173
                  end
                  inherited lbWakeHomingTargetTarget: TLabel
                    Left = 114
                    Top = 157
                    ExplicitLeft = 114
                    ExplicitTop = 157
                  end
                  inherited sbWakeHomingDisplayBlindHide: TSpeedButton
                    Left = 159
                    Top = 281
                    Width = 77
                    Height = 23
                    ExplicitLeft = 159
                    ExplicitTop = 281
                    ExplicitWidth = 77
                    ExplicitHeight = 23
                  end
                  inherited sbWakeHomingDisplayBlindShow: TSpeedButton
                    Left = 159
                    Top = 257
                    Width = 77
                    Height = 23
                    ExplicitLeft = 159
                    ExplicitTop = 257
                    ExplicitWidth = 77
                    ExplicitHeight = 23
                  end
                  inherited sbWakeHomingDisplayRangeHide: TSpeedButton
                    Left = 69
                    Top = 281
                    Width = 77
                    Height = 23
                    ExplicitLeft = 69
                    ExplicitTop = 281
                    ExplicitWidth = 77
                    ExplicitHeight = 23
                  end
                  inherited sbWakeHomingDisplayRangeShow: TSpeedButton
                    Left = 69
                    Top = 257
                    Width = 77
                    Height = 23
                    ExplicitLeft = 69
                    ExplicitTop = 257
                    ExplicitWidth = 77
                    ExplicitHeight = 23
                  end
                  inherited btnLaunchWH: TButton
                    Left = 232
                    Top = 316
                    Width = 75
                    Height = 22
                    ExplicitLeft = 232
                    ExplicitTop = 316
                    ExplicitWidth = 75
                    ExplicitHeight = 22
                  end
                  inherited btnTargetSeekerWH: TButton
                    Left = 232
                    Top = 193
                    Width = 75
                    Height = 22
                    ExplicitLeft = 232
                    ExplicitTop = 193
                    ExplicitWidth = 75
                    ExplicitHeight = 22
                  end
                  inherited EdtLaunchBearingWH: TEdit
                    Left = 114
                    Top = 171
                    Width = 60
                    ExplicitLeft = 114
                    ExplicitTop = 171
                    ExplicitWidth = 60
                  end
                  inherited EdtSalvoWH: TEdit
                    Left = 114
                    Top = 66
                    Width = 60
                    ExplicitLeft = 114
                    ExplicitTop = 66
                    ExplicitWidth = 60
                  end
                  inherited EdtSeekerRangeWH: TEdit
                    Left = 114
                    Top = 193
                    Width = 60
                    ExplicitLeft = 114
                    ExplicitTop = 193
                    ExplicitWidth = 60
                  end
                  inherited EdtWHTargetTrack: TEdit
                    Left = 114
                    Top = 107
                    Width = 60
                    ExplicitLeft = 114
                    ExplicitTop = 107
                    ExplicitWidth = 60
                  end
                end
              end
              inherited grbGunEngagementCIWS: TGroupBox [12]
                Width = 329
                Height = 1190
                ExplicitWidth = 329
                ExplicitHeight = 1170
              end
              inherited grbSurfaceToSurfaceMissile: TGroupBox [13]
                Width = 329
                Height = 1190
                ExplicitWidth = 329
                ExplicitHeight = 1170
                inherited ScrollBox6: TScrollBox
                  Width = 325
                  Height = 1173
                  ExplicitWidth = 325
                  ExplicitHeight = 1153
                  inherited Label67: TLabel
                    Top = -1
                    ExplicitTop = -1
                  end
                  inherited Bevel29: TBevel
                    Top = 6
                    ExplicitTop = 6
                  end
                  inherited lblSurfaceToSurfaceMissileStatus: TLabel
                    Left = 14
                    Top = 17
                    ExplicitLeft = 14
                    ExplicitTop = 17
                  end
                  inherited Label69: TLabel
                    Left = 109
                    Top = 17
                    ExplicitLeft = 109
                    ExplicitTop = 17
                  end
                  inherited lbSurfaceToSurfaceMissileQuantity: TLabel
                    Left = 165
                    Top = 17
                    ExplicitLeft = 165
                    ExplicitTop = 17
                  end
                  inherited Label75: TLabel
                    Top = 31
                    ExplicitTop = 31
                  end
                  inherited Bevel33: TBevel
                    Left = 42
                    Top = 38
                    ExplicitLeft = 42
                    ExplicitTop = 38
                  end
                  inherited Label76: TLabel
                    Left = 14
                    Top = 49
                    ExplicitLeft = 14
                    ExplicitTop = 49
                  end
                  inherited Label77: TLabel
                    Top = 65
                    ExplicitTop = 65
                  end
                  inherited Bevel74: TBevel
                    Left = 42
                    Top = 89
                    Width = 261
                    Height = 0
                    ExplicitLeft = 42
                    ExplicitTop = 89
                    ExplicitWidth = 261
                    ExplicitHeight = 0
                  end
                  inherited Label78: TLabel
                    Top = 258
                    ExplicitTop = 258
                  end
                  inherited Bevel77: TBevel
                    Left = 76
                    Top = 266
                    Width = 227
                    ExplicitLeft = 76
                    ExplicitTop = 266
                    ExplicitWidth = 227
                  end
                  inherited Label414: TLabel
                    Left = 14
                    Top = 124
                    ExplicitLeft = 14
                    ExplicitTop = 124
                  end
                  inherited Label415: TLabel
                    Left = 14
                    Top = 82
                    ExplicitLeft = 14
                    ExplicitTop = 82
                  end
                  inherited Label416: TLabel
                    Left = 14
                    Top = 104
                    ExplicitLeft = 14
                    ExplicitTop = 104
                  end
                  inherited btnSurfaceToSurfaceMissileFiring: TSpeedButton
                    Left = 164
                    Top = 77
                    Height = 21
                    ExplicitLeft = 164
                    ExplicitTop = 77
                    ExplicitHeight = 21
                  end
                  inherited btnSurfaceToSurfaceMissileEngagement: TSpeedButton
                    Left = 164
                    Top = 99
                    Height = 21
                    ExplicitLeft = 164
                    ExplicitTop = 99
                    ExplicitHeight = 21
                  end
                  inherited btnSurfaceToSurfaceMissileTargetTrack: TSpeedButton
                    Left = 164
                    Top = 47
                    Height = 20
                    OnClick = fmWeapon1btnSurfaceToSurfaceMissileTargetTrackClick
                    ExplicitLeft = 164
                    ExplicitTop = 47
                    ExplicitHeight = 20
                  end
                  inherited sbSurfaceToSurfaceMissileDisplayRangeShow: TSpeedButton
                    Left = 29
                    Top = 274
                    Width = 77
                    Height = 22
                    OnClick = fmWeapon1btnSurfaceToSurfaceMissileTargetTrackClick
                    ExplicitLeft = 29
                    ExplicitTop = 274
                    ExplicitWidth = 77
                    ExplicitHeight = 22
                  end
                  inherited sbSurfaceToSurfaceMissileDisplayRangeHide: TSpeedButton
                    Left = 29
                    Top = 293
                    Width = 77
                    Height = 22
                    OnClick = fmWeapon1btnSurfaceToSurfaceMissileTargetTrackClick
                    ExplicitLeft = 29
                    ExplicitTop = 293
                    ExplicitWidth = 77
                    ExplicitHeight = 22
                  end
                  inherited lblDestruckRange: TLabel
                    Left = 14
                    Top = 198
                    ExplicitLeft = 14
                    ExplicitTop = 198
                  end
                  inherited lblCrossOverRange: TLabel
                    Left = 14
                    Top = 180
                    ExplicitLeft = 14
                    ExplicitTop = 180
                  end
                  inherited lblNmCrossOverRange: TLabel
                    Left = 169
                    Top = 180
                    ExplicitLeft = 169
                    ExplicitTop = 180
                  end
                  inherited lblNmDestruckRange: TLabel
                    Left = 169
                    Top = 198
                    ExplicitLeft = 169
                    ExplicitTop = 198
                  end
                  inherited editSurfaceToSurfaceMissileTargetTrack: TEdit
                    Left = 107
                    Top = 47
                    ExplicitLeft = 107
                    ExplicitTop = 47
                  end
                  inherited btnSurfaceToSurfacePlan: TButton
                    Left = 12
                    Top = 317
                    Height = 22
                    OnClick = fmWeapon1btnSurfaceToSurfaceMissileTargetTrackClick
                    ExplicitLeft = 12
                    ExplicitTop = 317
                    ExplicitHeight = 22
                  end
                  inherited btnSurfaceToSurfaceLaunch: TButton
                    Left = 220
                    Top = 317
                    Height = 22
                    OnClick = fmWeapon1btnSurfaceToSurfaceMissileTargetTrackClick
                    ExplicitLeft = 220
                    ExplicitTop = 317
                    ExplicitHeight = 22
                  end
                  inherited btnSurfaceToSurfaceCancel: TButton
                    Left = 98
                    Top = 317
                    Height = 22
                    OnClick = fmWeapon1btnSurfaceToSurfaceMissileTargetTrackClick
                    ExplicitLeft = 98
                    ExplicitTop = 317
                    ExplicitHeight = 22
                  end
                  inherited editSurfaceToSurfaceMissileFiring: TEdit
                    Left = 106
                    Top = 78
                    ExplicitLeft = 106
                    ExplicitTop = 78
                  end
                  inherited editSurfaceToSurfaceMissileEngangement: TEdit
                    Left = 106
                    Top = 100
                    ExplicitLeft = 106
                    ExplicitTop = 100
                  end
                  inherited btnSurfaceToSurfaceMissileTargetTrackDetails: TButton
                    Left = 218
                    Top = 46
                    Height = 22
                    OnClick = fmWeapon1btnSurfaceToSurfaceMissileTargetTrackDetailsClick
                    ExplicitLeft = 218
                    ExplicitTop = 46
                    ExplicitHeight = 22
                  end
                  inherited btnSurfaceToSurfaceMissileLauncherMore: TButton [31]
                    Left = 218
                    Top = 193
                    Height = 22
                    OnClick = fmWeapon1btnSurfaceToSurfaceMissileLauncherMoreClick
                    ExplicitLeft = 218
                    ExplicitTop = 193
                    ExplicitHeight = 22
                  end
                  inherited pnlLaunch1: TPanel [32]
                    Left = 105
                    Top = 122
                    ExplicitLeft = 105
                    ExplicitTop = 122
                  end
                  inherited pnlLaunch2: TPanel [33]
                    Left = 129
                    Top = 122
                    ExplicitLeft = 129
                    ExplicitTop = 122
                    inherited vbl2: TVrBlinkLed
                      ExplicitLeft = 1
                      ExplicitTop = 1
                    end
                  end
                  inherited pnlLaunch3: TPanel [34]
                    Left = 153
                    Top = 122
                    ExplicitLeft = 153
                    ExplicitTop = 122
                  end
                  inherited pnlLaunch4: TPanel [35]
                    Left = 177
                    Top = 122
                    ExplicitLeft = 177
                    ExplicitTop = 122
                    inherited vbl4: TVrBlinkLed
                      ExplicitLeft = 1
                      ExplicitTop = 1
                      ExplicitWidth = 21
                    end
                  end
                  inherited pnlLaunch5: TPanel [36]
                    Left = 201
                    Top = 122
                    ExplicitLeft = 201
                    ExplicitTop = 122
                    inherited vbl5: TVrBlinkLed
                      ExplicitLeft = 1
                      ExplicitTop = 1
                      ExplicitWidth = 21
                    end
                  end
                  inherited pnlLaunch6: TPanel [37]
                    Left = 225
                    Top = 122
                    ExplicitLeft = 225
                    ExplicitTop = 122
                    inherited vbl6: TVrBlinkLed
                      ExplicitLeft = 1
                      ExplicitTop = 1
                      ExplicitWidth = 21
                    end
                  end
                  inherited pnlLaunch7: TPanel [38]
                    Left = 249
                    Top = 122
                    ExplicitLeft = 249
                    ExplicitTop = 122
                    inherited vbl7: TVrBlinkLed
                      ExplicitLeft = 1
                      ExplicitTop = 1
                      ExplicitWidth = 21
                    end
                  end
                  inherited pnlLaunch8: TPanel [39]
                    Left = 273
                    Top = 122
                    ExplicitLeft = 273
                    ExplicitTop = 122
                    inherited vbl8: TVrBlinkLed
                      ExplicitLeft = 1
                      ExplicitTop = 1
                      ExplicitWidth = 21
                    end
                  end
                  inherited edtDestructRange: TEdit [40]
                    Left = 106
                    Top = 198
                    ExplicitLeft = 106
                    ExplicitTop = 198
                  end
                  inherited edtCrossOverRange: TEdit [41]
                    Left = 106
                    Top = 177
                    ExplicitLeft = 106
                    ExplicitTop = 177
                  end
                  inherited panSurfaceToSurfaceWp: TPanel [42]
                    Left = 10
                    Top = 218
                    Width = 295
                    Height = 43
                    ExplicitLeft = 10
                    ExplicitTop = 218
                    ExplicitWidth = 295
                    ExplicitHeight = 43
                    inherited Bevel16: TBevel
                      Top = 6
                      Width = 233
                      ExplicitTop = 6
                      ExplicitWidth = 233
                    end
                    inherited Label54: TLabel
                      Left = 6
                      Top = -1
                      ExplicitLeft = 6
                      ExplicitTop = -1
                    end
                    inherited btnSurfaceToSurfaceMissileWaypointsEdit: TButton
                      Top = 16
                      Width = 56
                      Height = 22
                      OnClick = fmWeapon1btnSurfaceToSurfaceMissileTargetTrackClick
                      ExplicitTop = 16
                      ExplicitWidth = 56
                      ExplicitHeight = 22
                    end
                    inherited btnSurfaceToSurfaceMissileWaypointsAdd: TButton
                      Left = 60
                      Top = 16
                      Width = 56
                      Height = 22
                      OnClick = fmWeapon1btnSurfaceToSurfaceMissileTargetTrackClick
                      ExplicitLeft = 60
                      ExplicitTop = 16
                      ExplicitWidth = 56
                      ExplicitHeight = 22
                    end
                    inherited btnSurfaceToSurfaceMissileWaypointsDelete: TButton
                      Left = 116
                      Top = 16
                      Width = 56
                      Height = 22
                      OnClick = fmWeapon1btnSurfaceToSurfaceMissileTargetTrackClick
                      ExplicitLeft = 116
                      ExplicitTop = 16
                      ExplicitWidth = 56
                      ExplicitHeight = 22
                    end
                    inherited btnSurfaceToSurfaceMissileWaypointsApply: TButton
                      Left = 172
                      Top = 16
                      Width = 56
                      Height = 22
                      OnClick = fmWeapon1btnSurfaceToSurfaceMissileTargetTrackClick
                      ExplicitLeft = 172
                      ExplicitTop = 16
                      ExplicitWidth = 56
                      ExplicitHeight = 22
                    end
                    inherited btnSurfaceToSurfaceMissileWaypointsCancel: TButton
                      Left = 228
                      Top = 16
                      Width = 56
                      Height = 22
                      OnClick = fmWeapon1btnSurfaceToSurfaceMissileTargetTrackClick
                      ExplicitLeft = 228
                      ExplicitTop = 16
                      ExplicitWidth = 56
                      ExplicitHeight = 22
                    end
                  end
                end
              end
              inherited grbBombDepthCharge: TGroupBox [14]
                Width = 329
                Height = 1190
                ExplicitWidth = 329
                ExplicitHeight = 1170
                inherited bvl17: TBevel
                  Left = 49
                  Top = 98
                  Width = 269
                  ExplicitLeft = 49
                  ExplicitTop = 98
                  ExplicitWidth = 269
                end
                inherited bvl18: TBevel
                  Left = 48
                  Top = 57
                  Width = 270
                  ExplicitLeft = 48
                  ExplicitTop = 57
                  ExplicitWidth = 270
                end
                inherited bvl19: TBevel
                  Left = 45
                  Top = 138
                  Width = 273
                  ExplicitLeft = 45
                  ExplicitTop = 138
                  ExplicitWidth = 273
                end
                inherited bvl20: TBevel
                  Left = 43
                  Top = 16
                  Width = 275
                  ExplicitLeft = 43
                  ExplicitTop = 16
                  ExplicitWidth = 275
                end
                inherited btnBombTarget: TSpeedButton
                  Left = 180
                  Top = 66
                  ExplicitLeft = 180
                  ExplicitTop = 66
                end
                inherited lbl76: TLabel
                  Left = 8
                  Top = 91
                  ExplicitLeft = 8
                  ExplicitTop = 91
                end
                inherited lbl77: TLabel
                  Left = 24
                  Top = 110
                  ExplicitLeft = 24
                  ExplicitTop = 110
                end
                inherited lbl78: TLabel
                  Left = 8
                  Top = 50
                  ExplicitLeft = 8
                  ExplicitTop = 50
                end
                inherited lbl79: TLabel
                  Left = 8
                  Top = 131
                  ExplicitLeft = 8
                  ExplicitTop = 131
                end
                inherited lbl80: TLabel
                  Left = 92
                  Top = 149
                  ExplicitLeft = 92
                  ExplicitTop = 149
                end
                inherited lbl81: TLabel
                  Left = 24
                  Top = 69
                  ExplicitLeft = 24
                  ExplicitTop = 69
                end
                inherited lbl82: TLabel
                  Left = 8
                  Top = 9
                  ExplicitLeft = 8
                  ExplicitTop = 9
                end
                inherited lbl83: TLabel
                  Left = 118
                  Top = 28
                  ExplicitLeft = 118
                  ExplicitTop = 28
                end
                inherited lblBombQuantity: TLabel
                  Left = 173
                  Top = 28
                  ExplicitLeft = 173
                  ExplicitTop = 28
                end
                inherited lblBombStatus: TLabel
                  Left = 25
                  Top = 28
                  ExplicitLeft = 25
                  ExplicitTop = 28
                end
                inherited btnBombDisplayRangeHide: TSpeedButton
                  Left = 69
                  Top = 189
                  Width = 77
                  Height = 23
                  ExplicitLeft = 69
                  ExplicitTop = 189
                  ExplicitWidth = 77
                  ExplicitHeight = 23
                end
                inherited btnBombDisplayRangeShow: TSpeedButton
                  Left = 69
                  Top = 165
                  Width = 77
                  Height = 23
                  ExplicitLeft = 69
                  ExplicitTop = 165
                  ExplicitWidth = 77
                  ExplicitHeight = 23
                end
                inherited lbl86: TLabel
                  Top = 83
                  ExplicitTop = 83
                end
                inherited btnBombDrop: TButton
                  Left = 232
                  Top = 224
                  Width = 75
                  Height = 22
                  ExplicitLeft = 232
                  ExplicitTop = 224
                  ExplicitWidth = 75
                  ExplicitHeight = 22
                end
                inherited EdtBombControlSalvo: TEdit
                  Left = 117
                  Top = 106
                  Width = 60
                  ExplicitLeft = 117
                  ExplicitTop = 106
                  ExplicitWidth = 60
                end
                inherited EdtBombTargetTrack: TEdit
                  Left = 117
                  Top = 66
                  Width = 60
                  ExplicitLeft = 117
                  ExplicitTop = 66
                  ExplicitWidth = 60
                end
                inherited chkBombDropWhitoutTarget: TCheckBox
                  Top = 69
                  ExplicitTop = 69
                end
              end
              inherited grbGunEngagementAutomaticManualMode: TGroupBox [15]
                Width = 329
                Height = 1190
                ExplicitWidth = 329
                ExplicitHeight = 1170
              end
              inherited grbTacticalMissiles: TGroupBox [16]
                Width = 329
                Height = 1190
                ExplicitWidth = 329
                ExplicitHeight = 1170
                inherited ScrollBox7: TScrollBox
                  Width = 325
                  Height = 1173
                  ExplicitWidth = 325
                  ExplicitHeight = 1153
                  inherited Bevel107: TBevel
                    Top = 5
                    ExplicitTop = 5
                  end
                  inherited Bevel108: TBevel
                    Top = 199
                    ExplicitTop = 199
                  end
                  inherited Bevel59: TBevel
                    Left = 41
                    Top = 121
                    ExplicitLeft = 41
                    ExplicitTop = 121
                  end
                  inherited Bevel60: TBevel
                    Left = 38
                    Top = 36
                    ExplicitLeft = 38
                    ExplicitTop = 36
                  end
                  inherited Bevel62: TBevel
                    Left = 42
                    Top = 236
                    ExplicitLeft = 42
                    ExplicitTop = 236
                  end
                  inherited btnTacticalMissileTargetAimpoint: TSpeedButton
                    Left = 247
                    Top = 66
                    ExplicitLeft = 247
                    ExplicitTop = 66
                  end
                  inherited btnTacticalMissileTargetTrack: TSpeedButton
                    Height = 20
                    OnClick = fmWeapon1btnTacticalMissileTargetTrackClick
                    ExplicitHeight = 20
                  end
                  inherited Label289: TLabel
                    Top = -2
                    ExplicitTop = -2
                  end
                  inherited Label294: TLabel
                    Left = 117
                    Top = 14
                    ExplicitLeft = 117
                    ExplicitTop = 14
                  end
                  inherited Label295: TLabel
                    Left = 1
                    Top = 28
                    ExplicitLeft = 1
                    ExplicitTop = 28
                  end
                  inherited Label296: TLabel
                    Left = 4
                    Top = 114
                    ExplicitLeft = 4
                    ExplicitTop = 114
                  end
                  inherited Label302: TLabel
                    Left = 24
                    Top = 130
                    ExplicitLeft = 24
                    ExplicitTop = 130
                  end
                  inherited Label303: TLabel
                    Left = 175
                    Top = 93
                    ExplicitLeft = 175
                    ExplicitTop = 93
                  end
                  inherited Label304: TLabel
                    Left = 2
                    Top = 228
                    ExplicitLeft = 2
                    ExplicitTop = 228
                  end
                  inherited Label486: TLabel
                    Left = 51
                    Top = 239
                    ExplicitLeft = 51
                    ExplicitTop = 239
                  end
                  inherited Label494: TLabel
                    Left = 204
                    Top = 239
                    ExplicitLeft = 204
                    ExplicitTop = 239
                  end
                  inherited Label496: TLabel
                    Left = 24
                    Top = 152
                    ExplicitLeft = 24
                    ExplicitTop = 152
                  end
                  inherited Label497: TLabel
                    Left = 23
                    Top = 174
                    ExplicitLeft = 23
                    ExplicitTop = 174
                  end
                  inherited Label498: TLabel
                    Left = 175
                    Top = 152
                    ExplicitLeft = 175
                    ExplicitTop = 152
                  end
                  inherited Label499: TLabel
                    Left = 174
                    Top = 174
                    ExplicitLeft = 174
                    ExplicitTop = 174
                  end
                  inherited Label500: TLabel
                    Left = 1
                    Top = 191
                    ExplicitLeft = 1
                    ExplicitTop = 191
                  end
                  inherited lblTacticalMissileStatus: TLabel
                    Left = 26
                    Top = 14
                    ExplicitLeft = 26
                    ExplicitTop = 14
                  end
                  inherited lblTacticalMissileStatusQuantity: TLabel
                    Left = 173
                    Top = 14
                    ExplicitLeft = 173
                    ExplicitTop = 14
                  end
                  inherited sbTacticalMissileDisplayBlindHide: TSpeedButton
                    Left = 194
                    Top = 272
                    Height = 22
                    ExplicitLeft = 194
                    ExplicitTop = 272
                    ExplicitHeight = 22
                  end
                  inherited sbTacticalMissileDisplayBlindShow: TSpeedButton
                    Left = 194
                    Top = 253
                    Height = 22
                    ExplicitLeft = 194
                    ExplicitTop = 253
                    ExplicitHeight = 22
                  end
                  inherited sbTacticalMissileDisplayRangeHide: TSpeedButton
                    Left = 31
                    Top = 272
                    Height = 22
                    ExplicitLeft = 31
                    ExplicitTop = 272
                    ExplicitHeight = 22
                  end
                  inherited sbTacticalMissileDisplayRangeShow: TSpeedButton
                    Left = 31
                    Top = 253
                    Height = 22
                    ExplicitLeft = 31
                    ExplicitTop = 253
                    ExplicitHeight = 22
                  end
                  inherited btnTacticalMissileControlCruise: TButton
                    Left = 227
                    Top = 146
                    Height = 22
                    ExplicitLeft = 227
                    ExplicitTop = 146
                    ExplicitHeight = 22
                  end
                  inherited btnTacticalMissileControlSeeker: TButton
                    Left = 227
                    Top = 169
                    Height = 22
                    ExplicitLeft = 227
                    ExplicitTop = 169
                    ExplicitHeight = 22
                  end
                  inherited btnTacticalMissileLaunch: TButton
                    Left = 229
                    Top = 297
                    Height = 22
                    ExplicitLeft = 229
                    ExplicitTop = 297
                    ExplicitHeight = 22
                  end
                  inherited btnTacticalMissileTargetBearing: TButton
                    Left = 226
                    Top = 90
                    Height = 22
                    ExplicitLeft = 226
                    ExplicitTop = 90
                    ExplicitHeight = 22
                  end
                  inherited btnTacticalMissileWaypointAdd: TButton
                    Left = 125
                    Top = 207
                    Height = 22
                    ExplicitLeft = 125
                    ExplicitTop = 207
                    ExplicitHeight = 22
                  end
                  inherited btnTacticalMissileWaypointCancel: TButton
                    Left = 221
                    Top = 207
                    Height = 22
                    ExplicitLeft = 221
                    ExplicitTop = 207
                    ExplicitHeight = 22
                  end
                  inherited btnTacticalMissileWaypointEdit: TButton
                    Left = 29
                    Top = 207
                    Height = 22
                    ExplicitLeft = 29
                    ExplicitTop = 207
                    ExplicitHeight = 22
                  end
                  inherited editTacticalMissileControlCruise: TEdit
                    Left = 115
                    Top = 148
                    ExplicitLeft = 115
                    ExplicitTop = 148
                  end
                  inherited editTacticalMissileControlSalvo: TEdit
                    Left = 115
                    Top = 126
                    ExplicitLeft = 115
                    ExplicitTop = 126
                  end
                  inherited editTacticalMissileControlSeeker: TEdit
                    Left = 115
                    Top = 170
                    ExplicitLeft = 115
                    ExplicitTop = 170
                  end
                  inherited rdoTacticalMissileTargetAimpoint: TRadioButton
                    Top = 68
                    ExplicitTop = 68
                  end
                  inherited rdoTacticalMissileTargetBearing: TRadioButton
                    Top = 90
                    ExplicitTop = 90
                  end
                  inherited rdoTacticalMissileTargetTrack: TRadioButton
                    Top = 46
                    ExplicitTop = 46
                  end
                  inherited editTacticalMissileTargetAimpoint: TEdit
                    Top = 66
                    ExplicitTop = 66
                  end
                  inherited editTacticalMissileTargetTrack: TEdit
                    Top = 44
                    ExplicitTop = 44
                  end
                  inherited editTacticalMissileTargetBearing: TEdit
                    Top = 88
                    ExplicitTop = 88
                  end
                end
              end
            end
          end
          inherited pmenuWeapon: TPopupMenu
            Left = 234
            Top = 65529
          end
          inherited pmenuFiring: TPopupMenu
            Left = 156
            Top = 65527
          end
          inherited pmenuEngagement: TPopupMenu
            Left = 204
            Top = 65528
          end
          inherited TimerLaunch: TTimer
            Left = 128
            Top = 520
          end
          inherited TimerRipple: TTimer
            Left = 96
            Top = 520
          end
          inherited tmrSTOT: TTimer
            OnTimer = nil
            Left = 64
            Top = 520
          end
          inherited tmrLaunchSTOT: TTimer
            Left = 32
            Top = 520
          end
          inherited pmenuWpnCarrier: TPopupMenu
            Left = 176
            Top = 65528
          end
          inherited pmenuWpn: TPopupMenu
            Top = 0
          end
          inherited pmTorpedoFiring: TPopupMenu
            Left = 168
            Top = 520
          end
          inherited pmTorpedoRunOut: TPopupMenu
            Left = 200
            Top = 520
          end
          inherited pmTorpedoGyroAngle: TPopupMenu
            Left = 232
            Top = 520
          end
        end
      end
      object tsCounterMeasure: TTabSheet
        ImageIndex = 4
        ExplicitHeight = 1192
        inline fmCounterMeasure1: TfmCounterMeasure
          Left = 0
          Top = 0
          Width = 329
          Height = 1212
          Align = alClient
          TabOrder = 0
          ExplicitWidth = 329
          ExplicitHeight = 1192
          inherited PanelALL: TPanel
            Width = 329
            Height = 1098
            ExplicitWidth = 329
            ExplicitHeight = 1078
            inherited PanelCounterMeasure: TPanel
              Width = 329
              Height = 1098
              ExplicitWidth = 329
              ExplicitHeight = 1078
              inherited grbTowedJammerDecoy: TGroupBox [0]
                Width = 329
                Height = 1098
                ExplicitWidth = 329
                ExplicitHeight = 1078
                inherited ScrollBox4: TScrollBox
                  Width = 325
                  Height = 1081
                  ExplicitWidth = 325
                  ExplicitHeight = 1061
                  inherited Bevel109: TBevel
                    Top = 189
                    ExplicitTop = 189
                  end
                  inherited Bevel118: TBevel
                    Top = 156
                    ExplicitTop = 156
                  end
                  inherited Label503: TLabel
                    Left = 50
                    ExplicitLeft = 50
                  end
                  inherited Label504: TLabel
                    Top = 126
                    ExplicitTop = 126
                  end
                  inherited Label506: TLabel
                    Top = 105
                    ExplicitTop = 105
                  end
                  inherited Label507: TLabel
                    Top = 107
                    ExplicitTop = 107
                  end
                  inherited Label508: TLabel
                    Top = 171
                    ExplicitTop = 171
                  end
                  inherited Label509: TLabel
                    Top = 166
                    ExplicitTop = 166
                  end
                  inherited Label512: TLabel
                    Top = 183
                    ExplicitTop = 183
                  end
                  inherited Label559: TLabel
                    Top = 149
                    ExplicitTop = 149
                  end
                  inherited Label560: TLabel
                    Top = 201
                    ExplicitTop = 201
                  end
                  inherited Label561: TLabel
                    Top = 219
                    ExplicitTop = 219
                  end
                  inherited Label563: TLabel
                    Top = 219
                    ExplicitTop = 219
                  end
                  inherited Label564: TLabel
                    Top = 201
                    ExplicitTop = 201
                  end
                  inherited lblTowedJammerDecoyActual: TLabel
                    Top = 219
                    ExplicitTop = 219
                  end
                  inherited lblTowedJammerDecoyQuantity: TLabel
                    Top = 166
                    ExplicitTop = 166
                  end
                  inherited sbTowedJammerDecoyActionDeploy: TSpeedButton
                    Left = 20
                    Top = 187
                    Width = 89
                    Height = 24
                    ExplicitLeft = 20
                    ExplicitTop = 187
                    ExplicitWidth = 89
                    ExplicitHeight = 24
                  end
                  inherited sbTowedJammerDecoyActionStow: TSpeedButton
                    Left = 20
                    Top = 209
                    Width = 89
                    Height = 24
                    ExplicitLeft = 20
                    ExplicitTop = 209
                    ExplicitWidth = 89
                    ExplicitHeight = 24
                  end
                  inherited sbTowedJammerDecoyModeAuto: TSpeedButton
                    Width = 89
                    Height = 24
                    ExplicitWidth = 89
                    ExplicitHeight = 24
                  end
                  inherited sbTowedJammerDecoyModeManual: TSpeedButton
                    Top = 54
                    Width = 89
                    Height = 24
                    ExplicitTop = 54
                    ExplicitWidth = 89
                    ExplicitHeight = 24
                  end
                  inherited sbTowedJammerDecoyModeOff: TSpeedButton
                    Top = 76
                    Width = 89
                    Height = 24
                    ExplicitTop = 76
                    ExplicitWidth = 89
                    ExplicitHeight = 24
                  end
                  inherited sbTowedJammerDecoyTargetingSpot: TSpeedButton
                    Width = 89
                    Height = 24
                    ExplicitWidth = 89
                    ExplicitHeight = 24
                  end
                  inherited sbTowedJammerDecoyTargetingTrack: TSpeedButton
                    Top = 54
                    Width = 89
                    Height = 24
                    ExplicitTop = 54
                    ExplicitWidth = 89
                    ExplicitHeight = 24
                  end
                  inherited editTowedJammerDecoyOrdered: TEdit
                    Top = 197
                    ExplicitTop = 197
                  end
                  inherited edtTowedJammerDecoyBearing: TEdit
                    Top = 105
                    ExplicitTop = 105
                  end
                  inherited edtTowedJammerDecoySpotNumb: TEdit
                    Top = 127
                    ExplicitTop = 127
                  end
                end
              end
              inherited grbSurfaceChaffDeployment: TGroupBox [1]
                Width = 329
                Height = 1098
                ExplicitWidth = 329
                ExplicitHeight = 1078
                inherited ScrollBox2: TScrollBox
                  Width = 325
                  Height = 1081
                  ExplicitWidth = 325
                  ExplicitHeight = 1061
                  inherited Bevel3: TBevel
                    Top = 4
                    ExplicitTop = 4
                  end
                  inherited Label12: TLabel
                    Top = 43
                    ExplicitTop = 43
                  end
                  inherited btnSurfaceChaffLauncher: TSpeedButton
                    OnClick = fmCounterMeasure1btnSurfaceChaffLauncherClick
                  end
                  inherited btnSurfaceChaffType: TSpeedButton
                    OnClick = fmCounterMeasure1btnSurfaceChaffTypeClick
                  end
                  inherited btnSurfaceChaffCopy: TSpeedButton
                    OnClick = fmCounterMeasure1btnSurfaceChaffCopyClick
                  end
                  inherited btnSurfaceChaffLaunch: TSpeedButton
                    OnClick = fmCounterMeasure1btnSurfaceChaffLaunchClick
                  end
                  inherited btnSurfaceChaffAbort: TSpeedButton
                    OnClick = fmCounterMeasure1btnSurfaceChaffAbortClick
                  end
                  inherited edtSurfaceChaffBloomRange: TEdit
                    OnKeyPress = fmCounterMeasure1edtSurfaceChaffBloomRangeKeyPress
                  end
                  inherited edtSurfaceChaffBloomAltitude: TEdit
                    OnKeyPress = fmCounterMeasure1edtSurfaceChaffBloomAltitudeKeyPress
                  end
                  inherited edtSurfaceChaffSalvoSize: TEdit
                    OnKeyPress = fmCounterMeasure1edtSurfaceChaffSalvoSizeKeyPress
                  end
                  inherited edtSurfaceChaffDelay: TEdit
                    OnKeyPress = fmCounterMeasure1edtSurfaceChaffDelayKeyPress
                  end
                  inherited ckSurfaceChaffEnabled: TCheckBox
                    OnClick = fmCounterMeasure1ckSurfaceChaffEnabledClick
                  end
                  inherited ckSurfaceChaffSeductionEnabled: TCheckBox
                    OnClick = fmCounterMeasure1ckSurfaceChaffSeductionEnabledClick
                  end
                end
              end
              inherited grpAirBubble: TGroupBox [2]
                Width = 329
                Height = 1098
                ExplicitWidth = 329
                ExplicitHeight = 1078
                inherited btnType: TSpeedButton
                  Left = 278
                  Width = 29
                  Height = 23
                  ExplicitLeft = 278
                  ExplicitWidth = 29
                  ExplicitHeight = 23
                end
                inherited btnAirBubbleDeploy: TButton
                  Width = 89
                  Height = 24
                  OnClick = fmCounterMeasure1btnAirBubbleDeployClick
                  ExplicitWidth = 89
                  ExplicitHeight = 24
                end
              end
              inherited grbAcousticDecoy: TGroupBox [3]
                Width = 329
                Height = 1098
                ExplicitWidth = 329
                ExplicitHeight = 1078
                inherited ScrollBox1: TScrollBox
                  Width = 325
                  Height = 1081
                  ExplicitWidth = 325
                  ExplicitHeight = 1061
                  inherited Label4: TLabel
                    Left = 50
                    ExplicitLeft = 50
                  end
                  inherited Label6: TLabel
                    Left = 43
                    ExplicitLeft = 43
                  end
                  inherited Label7: TLabel
                    Left = 152
                    ExplicitLeft = 152
                  end
                  inherited btnComboAcousticDecoyMode: TSpeedButton
                    Left = 275
                    Width = 26
                    Height = 23
                    ExplicitLeft = 275
                    ExplicitWidth = 26
                    ExplicitHeight = 23
                  end
                  inherited btnComboAcousticDecoyFilter: TSpeedButton
                    Left = 275
                    Width = 26
                    Height = 23
                    ExplicitLeft = 275
                    ExplicitWidth = 26
                    ExplicitHeight = 23
                  end
                  inherited sbAcousticDecoyActionDeploy: TSpeedButton
                    Width = 89
                    ExplicitWidth = 89
                  end
                  inherited sbAcousticDecoyActionStow: TSpeedButton
                    Width = 89
                    ExplicitWidth = 89
                  end
                  inherited sbAcousticDecoyActivationOn: TSpeedButton
                    Width = 89
                    Height = 23
                    ExplicitWidth = 89
                    ExplicitHeight = 23
                  end
                  inherited sbAcousticDecoyActivationOff: TSpeedButton
                    Width = 89
                    Height = 23
                    ExplicitWidth = 89
                    ExplicitHeight = 23
                  end
                  inherited sbAcousticDecoyCycleTimerOn: TSpeedButton
                    Width = 89
                    Height = 23
                    ExplicitWidth = 89
                    ExplicitHeight = 23
                  end
                  inherited sbAcousticDecoyCycleTimerOff: TSpeedButton
                    Width = 89
                    Height = 23
                    ExplicitWidth = 89
                    ExplicitHeight = 23
                  end
                end
              end
              inherited grbOnBoardSelfDefenseJammer: TGroupBox [4]
                Width = 329
                Height = 1098
                ExplicitWidth = 329
                ExplicitHeight = 1078
                inherited ScrollBox5: TScrollBox
                  Width = 325
                  Height = 1081
                  ExplicitWidth = 325
                  ExplicitHeight = 1061
                  inherited Label543: TLabel
                    Left = 161
                    ExplicitLeft = 161
                  end
                  inherited Label544: TLabel
                    Left = 60
                    ExplicitLeft = 60
                  end
                  inherited sbOnBoardSelfDefenseJammerControlModeAuto: TSpeedButton
                    Width = 89
                    Height = 24
                    ExplicitWidth = 89
                    ExplicitHeight = 24
                  end
                  inherited sbOnBoardSelfDefenseJammerControlModeManual: TSpeedButton
                    Top = 57
                    Width = 89
                    Height = 24
                    ExplicitTop = 57
                    ExplicitWidth = 89
                    ExplicitHeight = 24
                  end
                  inherited sbOnBoardSelfDefenseJammerControlModeOff: TSpeedButton
                    Top = 79
                    Width = 89
                    Height = 24
                    ExplicitTop = 79
                    ExplicitWidth = 89
                    ExplicitHeight = 24
                  end
                  inherited sbOnBoardSelfDefenseJammerControlTargetingSpot: TSpeedButton
                    Width = 89
                    Height = 24
                    ExplicitWidth = 89
                    ExplicitHeight = 24
                  end
                  inherited sbOnBoardSelfDefenseJammerControlTargetingTrack: TSpeedButton
                    Top = 57
                    Width = 89
                    Height = 24
                    ExplicitTop = 57
                    ExplicitWidth = 89
                    ExplicitHeight = 24
                  end
                end
              end
              inherited grbAirborneChaff: TGroupBox [5]
                Width = 329
                Height = 1098
                ExplicitWidth = 329
                ExplicitHeight = 1078
                inherited btnAirboneChaffType: TSpeedButton
                  Left = 279
                  Width = 28
                  Height = 23
                  ExplicitLeft = 279
                  ExplicitWidth = 28
                  ExplicitHeight = 23
                end
                inherited btnChaffAirboneDeploy: TButton
                  Width = 89
                  Height = 24
                  OnClick = fmCounterMeasure1btnChaffAirboneDeployClick
                  ExplicitWidth = 89
                  ExplicitHeight = 24
                end
              end
              inherited grbFloatingDecoy: TGroupBox [6]
                Width = 329
                Height = 1098
                ExplicitWidth = 329
                ExplicitHeight = 1078
                inherited btnFloatingDecoyDeploy: TButton
                  Left = 216
                  Width = 89
                  Height = 24
                  OnClick = fmCounterMeasure1btnFloatingDecoyDeployClick
                  ExplicitLeft = 216
                  ExplicitWidth = 89
                  ExplicitHeight = 24
                end
              end
              inherited grbRadarNoiseJammer: TGroupBox [7]
                Width = 329
                Height = 1098
                ExplicitWidth = 329
                ExplicitHeight = 1078
                inherited ScrollBox3: TScrollBox
                  Width = 325
                  Height = 1081
                  ExplicitWidth = 325
                  ExplicitHeight = 1061
                  inherited btnComboRadarJammingControlMode: TSpeedButton
                    Left = 276
                    Top = 78
                    Width = 27
                    Height = 23
                    ExplicitLeft = 276
                    ExplicitTop = 78
                    ExplicitWidth = 27
                    ExplicitHeight = 23
                  end
                  inherited Label532: TLabel
                    Top = 84
                    ExplicitTop = 84
                  end
                  inherited Label558: TLabel
                    Left = 34
                    Top = 13
                    ExplicitLeft = 34
                    ExplicitTop = 13
                  end
                  inherited sbRadarJammingControlActivationOn: TSpeedButton [5]
                    Width = 89
                    Height = 24
                    OnClick = fmCounterMeasure1sbRadarJammingControlActivationOnClick
                    ExplicitWidth = 89
                    ExplicitHeight = 24
                  end
                  inherited sbRadarJammingControlActivationOff: TSpeedButton [6]
                    Top = 50
                    Width = 89
                    Height = 24
                    OnClick = fmCounterMeasure1sbRadarJammingControlActivationOffClick
                    ExplicitTop = 50
                    ExplicitWidth = 89
                    ExplicitHeight = 24
                  end
                  inherited cbRadarJammingControlMode: TComboBox
                    Top = 79
                    ExplicitTop = 79
                  end
                  inherited PanelRadarJammingMode: TPanel
                    Top = 104
                    ExplicitTop = 104
                    inherited gbRadarJammingBarrageMode: TGroupBox
                      inherited Label533: TLabel
                        Left = 185
                        ExplicitLeft = 185
                      end
                      inherited Label534: TLabel
                        Left = 185
                        ExplicitLeft = 185
                      end
                      inherited Label538: TLabel
                        Left = 185
                        ExplicitLeft = 185
                      end
                    end
                    inherited gbRadarJammingSpotNumberMode: TGroupBox
                      inherited Label521: TLabel
                        Left = 185
                        ExplicitLeft = 185
                      end
                      inherited Label522: TLabel
                        Left = 185
                        ExplicitLeft = 185
                      end
                      inherited Label525: TLabel
                        Left = 185
                        ExplicitLeft = 185
                      end
                    end
                    inherited gbRadarJammingSelectedTrackMode: TGroupBox
                      inherited Label528: TLabel
                        Left = 185
                        ExplicitLeft = 185
                      end
                      inherited Label530: TLabel
                        Left = 185
                        ExplicitLeft = 185
                      end
                      inherited Label540: TLabel
                        Left = 185
                        ExplicitLeft = 185
                      end
                      inherited btnRadarJammingModeSelectedTrackTrack: TSpeedButton
                        Top = 5
                        Width = 31
                        OnClick = fmCounterMeasure1btnRadarJammingModeSelectedTrackTrackClick
                        ExplicitTop = 5
                        ExplicitWidth = 31
                      end
                    end
                  end
                end
              end
            end
          end
          inherited PanelCounterMeasureSpace: TPanel
            Width = 329
            ExplicitWidth = 329
          end
          inherited PanelCounterMeasureChoice: TPanel
            Width = 329
            ExplicitWidth = 329
            inherited lvECM: TListView
              Width = 329
              ExplicitWidth = 329
            end
          end
        end
      end
      object tsFireControl: TTabSheet
        ImageIndex = 5
        ExplicitHeight = 1192
        inline fmFireControl1: TfmFireControl
          Left = 0
          Top = 0
          Width = 329
          Height = 1212
          Align = alClient
          TabOrder = 0
          ExplicitWidth = 329
          ExplicitHeight = 1192
          inherited PanelFCChoices: TPanel
            Width = 329
            Height = 82
            ExplicitWidth = 329
            ExplicitHeight = 82
            inherited lstAssetsChoices: TListView
              Width = 329
              Height = 87
              ExplicitWidth = 329
              ExplicitHeight = 87
            end
          end
          inherited PaneFCSpace: TPanel
            Top = 82
            Width = 329
            ExplicitTop = 82
            ExplicitWidth = 329
          end
          inherited PanelALL: TPanel
            Top = 94
            Width = 329
            Height = 1118
            ExplicitLeft = 0
            ExplicitTop = 94
            ExplicitWidth = 329
            ExplicitHeight = 1098
            inherited PanelFC: TPanel
              Width = 329
              Height = 1118
              ExplicitTop = 0
              ExplicitWidth = 329
              ExplicitHeight = 1098
              inherited ScrollBox3: TScrollBox
                Width = 329
                Height = 1118
                ExplicitWidth = 329
                ExplicitHeight = 1098
                inherited grbFireControl: TGroupBox
                  Width = 329
                  Height = 1118
                  ExplicitWidth = 329
                  ExplicitHeight = 1098
                  inherited Bevel27: TBevel
                    Top = 10
                    Width = 265
                    ExplicitTop = 10
                    ExplicitWidth = 265
                  end
                  inherited Bevel52: TBevel
                    Top = 101
                    Width = 265
                    ExplicitTop = 101
                    ExplicitWidth = 265
                  end
                  inherited Bevel53: TBevel
                    Top = 250
                    Width = 265
                    ExplicitTop = 250
                    ExplicitWidth = 265
                  end
                  inherited btnSearchFireControlAssetsTarget: TSpeedButton
                    Tag = 4
                    Left = 96
                    Top = 111
                    Height = 21
                    Spacing = 0
                    OnClick = fmFireControl1btnSearchFireControlAssetsTargetClick
                    ExplicitLeft = 96
                    ExplicitTop = 111
                    ExplicitHeight = 21
                  end
                  inherited Label265: TLabel
                    Left = 51
                    Top = 14
                    ExplicitLeft = 51
                    ExplicitTop = 14
                  end
                  inherited Label266: TLabel
                    Top = 17
                    ExplicitTop = 17
                  end
                  inherited Label267: TLabel
                    Top = 4
                    ExplicitTop = 4
                  end
                  inherited Label268: TLabel
                    Top = 95
                    ExplicitTop = 95
                  end
                  inherited Label514: TLabel
                    Top = 244
                    ExplicitTop = 244
                  end
                  inherited Label515: TLabel
                    Left = 74
                    Top = 253
                    ExplicitLeft = 74
                    ExplicitTop = 253
                  end
                  inherited Label527: TLabel
                    Left = 184
                    Top = 253
                    ExplicitLeft = 184
                    ExplicitTop = 253
                  end
                  inherited lbControlChannel: TLabel
                    Top = 17
                    ExplicitTop = 17
                  end
                  inherited sbFireControlAssetsBlindZonesHide: TSpeedButton
                    Left = 166
                    Top = 287
                    Width = 89
                    Height = 23
                    OnClick = btnFireControlOnClick
                    ExplicitLeft = 166
                    ExplicitTop = 287
                    ExplicitWidth = 89
                    ExplicitHeight = 23
                  end
                  inherited sbFireControlAssetsBlindZonesShow: TSpeedButton
                    Left = 166
                    Top = 266
                    Width = 89
                    Height = 23
                    OnClick = btnFireControlOnClick
                    ExplicitLeft = 166
                    ExplicitTop = 266
                    ExplicitWidth = 89
                    ExplicitHeight = 23
                  end
                  inherited sbFireControlAssetsDisplayHide: TSpeedButton
                    Left = 44
                    Top = 287
                    Width = 89
                    Height = 23
                    OnClick = btnFireControlOnClick
                    ExplicitLeft = 44
                    ExplicitTop = 287
                    ExplicitWidth = 89
                    ExplicitHeight = 23
                  end
                  inherited sbFireControlAssetsDisplayShow: TSpeedButton
                    Left = 44
                    Top = 266
                    Width = 89
                    Height = 23
                    OnClick = btnFireControlOnClick
                    ExplicitLeft = 44
                    ExplicitTop = 266
                    ExplicitWidth = 89
                    ExplicitHeight = 23
                  end
                  inherited sbFireControlAssetsModeOff: TSpeedButton
                    Left = 20
                    Top = 69
                    Width = 89
                    Height = 23
                    OnClick = btnFireControlOnClick
                    ExplicitLeft = 20
                    ExplicitTop = 69
                    ExplicitWidth = 89
                    ExplicitHeight = 23
                  end
                  inherited sbFireControlAssetsModeSearch: TSpeedButton
                    Left = 20
                    Top = 27
                    Width = 89
                    Height = 23
                    OnClick = btnFireControlOnClick
                    ExplicitLeft = 20
                    ExplicitTop = 27
                    ExplicitWidth = 89
                    ExplicitHeight = 23
                  end
                  inherited sbFireControlAssetsModeTrackOnly: TSpeedButton
                    Left = 20
                    Top = 48
                    Width = 89
                    Height = 23
                    OnClick = btnFireControlOnClick
                    ExplicitLeft = 20
                    ExplicitTop = 48
                    ExplicitWidth = 89
                    ExplicitHeight = 23
                  end
                  inherited btnFireControlAssetsTargetAssign: TButton
                    Left = 162
                    Top = 114
                    Width = 89
                    Height = 23
                    OnClick = btnFireControlOnClick
                    ExplicitLeft = 162
                    ExplicitTop = 114
                    ExplicitWidth = 89
                    ExplicitHeight = 23
                  end
                  inherited btnFireControlAssetsTargetBreak: TButton
                    Left = 162
                    Top = 135
                    Width = 89
                    Height = 24
                    OnClick = btnFireControlOnClick
                    ExplicitLeft = 162
                    ExplicitTop = 135
                    ExplicitWidth = 89
                    ExplicitHeight = 24
                  end
                  inherited btnFireControlAssetsTargetBreakAll: TButton
                    Left = 162
                    Top = 157
                    Width = 89
                    Height = 23
                    OnClick = btnFireControlOnClick
                    ExplicitLeft = 162
                    ExplicitTop = 157
                    ExplicitWidth = 89
                    ExplicitHeight = 23
                  end
                  inherited edtFireControlAssetsTarget: TEdit
                    Top = 111
                    ExplicitTop = 111
                  end
                  inherited lstFireControlAssetsAssignedTracks: TListView
                    Top = 134
                    Width = 108
                    Height = 108
                    ParentFont = False
                    ExplicitTop = 134
                    ExplicitWidth = 108
                    ExplicitHeight = 108
                  end
                end
              end
            end
          end
        end
      end
      object tsEMCON: TTabSheet
        ImageIndex = 6
        ExplicitHeight = 1192
        inline fmEMCON1: TfmEMCON
          Left = 0
          Top = 0
          Width = 329
          Height = 1212
          Align = alClient
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          ExplicitWidth = 329
          ExplicitHeight = 1192
          inherited PanelEmconChoices: TPanel
            Width = 329
            ExplicitWidth = 329
            inherited LvEmcon: TListView
              Width = 327
              Font.Color = clActiveCaption
              ParentFont = False
              ExplicitWidth = 327
            end
          end
          inherited PaneALL: TPanel
            Width = 329
            Height = 1115
            ExplicitWidth = 329
            ExplicitHeight = 1095
            inherited PanelEmcon: TPanel
              Width = 329
              Height = 1115
              ExplicitWidth = 329
              ExplicitHeight = 1095
              inherited ScrollBox2: TScrollBox
                Width = 327
                Height = 1113
                ExplicitWidth = 327
                ExplicitHeight = 1093
                inherited Bevel18: TBevel
                  Top = 226
                  ExplicitTop = 226
                end
                inherited Label562: TLabel
                  Top = 220
                  ExplicitTop = 220
                end
                inherited sbEmconAllSystemsAllSilent: TSpeedButton
                  Top = 237
                  Height = 22
                  OnClick = fmEMCON1sbEmconAllSystemsAllSilentClick
                  ExplicitTop = 237
                  ExplicitHeight = 22
                end
                inherited sbEmconAllSystemsClearAll: TSpeedButton
                  Top = 257
                  Height = 22
                  OnClick = fmEMCON1sbEmconAllSystemsClearAllClick
                  ExplicitTop = 257
                  ExplicitHeight = 22
                end
                inherited btnEmconDistributeToGroup: TButton
                  Height = 24
                  ExplicitHeight = 24
                end
                inherited cbEmconAcousticDecoys: TCheckBox
                  Top = 202
                  ExplicitTop = 202
                end
                inherited cbEmconActiveSonar: TCheckBox
                  Top = 187
                  OnClick = fmEMCON1cbEmconSearchRadarClick
                  ExplicitTop = 187
                end
                inherited cbEmconFireControl: TCheckBox
                  Top = 46
                  OnClick = fmEMCON1cbEmconSearchRadarClick
                  ExplicitTop = 46
                end
                inherited cbEmconHFComm: TCheckBox
                  Top = 108
                  ExplicitTop = 108
                end
                inherited cbEmconHFDatalink: TCheckBox
                  Top = 138
                  ExplicitTop = 138
                end
                inherited cbEmconIFF: TCheckBox
                  Top = 63
                  OnClick = fmEMCON1cbEmconSearchRadarClick
                  ExplicitTop = 63
                end
                inherited cbEmconJammingSystems: TCheckBox
                  Top = 78
                  ExplicitTop = 78
                end
                inherited cbEmconLasers: TCheckBox
                  Top = 93
                  ExplicitTop = 93
                end
                inherited cbEmconSearchRadar: TCheckBox
                  OnClick = fmEMCON1cbEmconSearchRadarClick
                end
                inherited cbEmconUWT: TCheckBox
                  Top = 170
                  ExplicitTop = 170
                end
                inherited cbEmconVHFUHFComm: TCheckBox
                  Top = 123
                  ExplicitTop = 123
                end
                inherited cbEmconVHFUHFDatalink: TCheckBox
                  Top = 154
                  OnClick = fmEMCON1cbEmconSearchRadarClick
                  ExplicitTop = 154
                end
                inherited pnlGroupAirbone: TPanel
                  Top = 204
                  Height = 78
                  ExplicitTop = 204
                  ExplicitHeight = 78
                  inherited sbEmconGroupAirboneEMCON: TSpeedButton
                    Top = 53
                    Height = 22
                    ExplicitTop = 53
                    ExplicitHeight = 22
                  end
                  inherited sbEmconGroupAirboneClear: TSpeedButton
                    Top = 33
                    Height = 22
                    ExplicitTop = 33
                    ExplicitHeight = 22
                  end
                end
              end
            end
          end
        end
      end
    end
    object HookContactInfoTraineeDisplay: TPageControl
      Left = 0
      Top = 83
      Width = 337
      Height = 222
      ActivePage = tsHook
      Align = alTop
      TabOrder = 0
      object tsHook: TTabSheet
        Caption = 'Hook'
        object lbTrackHook: TLabel
          Left = 100
          Top = 5
          Width = 12
          Height = 13
          Caption = '---'
        end
        object Label1: TLabel
          Left = 3
          Top = 20
          Width = 27
          Height = 13
          Caption = 'Name'
        end
        object lbNameHook: TLabel
          Left = 100
          Top = 21
          Width = 12
          Height = 13
          Caption = '---'
        end
        object lbClassHook: TLabel
          Left = 100
          Top = 38
          Width = 12
          Height = 13
          Caption = '---'
        end
        object Label2: TLabel
          Left = 3
          Top = 38
          Width = 25
          Height = 13
          Caption = 'Class'
        end
        object lbBearingHook: TLabel
          Left = 100
          Top = 56
          Width = 15
          Height = 13
          Caption = '---'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label3: TLabel
          Left = 3
          Top = 56
          Width = 36
          Height = 13
          Caption = 'Bearing'
        end
        object Label4: TLabel
          Left = 190
          Top = 56
          Width = 43
          Height = 13
          Caption = 'degree T'
        end
        object Label5: TLabel
          Left = 3
          Top = 73
          Width = 31
          Height = 13
          Caption = 'Range'
        end
        object lbRangeHook: TLabel
          Left = 100
          Top = 73
          Width = 15
          Height = 13
          Caption = '---'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label6: TLabel
          Left = 190
          Top = 73
          Width = 14
          Height = 13
          Caption = 'nm'
        end
        object Label7: TLabel
          Left = 140
          Top = 109
          Width = 3
          Height = 13
        end
        object lbPositionHook1: TLabel
          Left = 100
          Top = 90
          Width = 12
          Height = 13
          Caption = '---'
        end
        object lbCourseHook: TLabel
          Left = 100
          Top = 108
          Width = 12
          Height = 13
          Caption = '---'
        end
        object lbPositionHook2: TLabel
          Left = 190
          Top = 90
          Width = 12
          Height = 13
          Caption = '---'
        end
        object lbGround: TLabel
          Left = 100
          Top = 125
          Width = 12
          Height = 13
          Caption = '---'
        end
        object lbFormation: TLabel
          Left = 100
          Top = 160
          Width = 12
          Height = 13
          Caption = '---'
        end
        object Label8: TLabel
          Left = 190
          Top = 108
          Width = 43
          Height = 13
          Caption = 'degree T'
        end
        object Label9: TLabel
          Left = 190
          Top = 125
          Width = 21
          Height = 13
          Caption = 'knot'
        end
        object lbDamage: TLabel
          Left = 100
          Top = 144
          Width = 12
          Height = 13
          Caption = '---'
        end
        object lb8: TLabel
          Left = 3
          Top = 142
          Width = 39
          Height = 13
          Caption = 'Damage'
        end
        object pnlDepth: TPanel
          Left = 3
          Top = 172
          Width = 329
          Height = 22
          BevelOuter = bvNone
          TabOrder = 12
          Visible = False
          object lbDepth: TLabel
            Left = 97
            Top = 5
            Width = 12
            Height = 13
            Caption = '---'
          end
          object lb2: TLabel
            Left = 187
            Top = 5
            Width = 28
            Height = 13
            Caption = 'meter'
          end
          object lbtext3: TStaticText
            Left = 0
            Top = 5
            Width = 33
            Height = 17
            Caption = 'Depth'
            TabOrder = 0
          end
          object lb1: TStaticText
            Left = 86
            Top = 5
            Width = 8
            Height = 17
            Caption = ':'
            TabOrder = 1
          end
        end
        object pnlAltitude: TPanel
          Left = 0
          Top = 171
          Width = 329
          Height = 22
          BevelOuter = bvNone
          TabOrder = 13
          Visible = False
          object lb4: TLabel
            Left = 187
            Top = 5
            Width = 20
            Height = 13
            Caption = 'feet'
          end
          object lbAltitude: TLabel
            Left = 100
            Top = 3
            Width = 12
            Height = 13
            Caption = '---'
          end
          object lb6: TStaticText
            Left = 89
            Top = 5
            Width = 8
            Height = 17
            Caption = ':'
            TabOrder = 0
          end
          object lb5: TStaticText
            Left = 3
            Top = 4
            Width = 41
            Height = 17
            Caption = 'Altitude'
            TabOrder = 1
          end
        end
        object StaticText1: TStaticText
          Left = 3
          Top = 3
          Width = 30
          Height = 17
          Caption = 'Track'
          TabOrder = 0
        end
        object StaticText6: TStaticText
          Left = 3
          Top = 90
          Width = 41
          Height = 17
          Caption = 'Position'
          TabOrder = 1
        end
        object StaticText7: TStaticText
          Left = 3
          Top = 108
          Width = 38
          Height = 17
          Caption = 'Course'
          TabOrder = 2
        end
        object StaticText8: TStaticText
          Left = 3
          Top = 125
          Width = 72
          Height = 17
          Caption = 'Ground Speed'
          TabOrder = 3
        end
        object StaticText10: TStaticText
          Left = 3
          Top = 158
          Width = 74
          Height = 17
          Caption = 'Formation Size'
          TabOrder = 4
        end
        object StaticText25: TStaticText
          Left = 89
          Top = 3
          Width = 8
          Height = 17
          Caption = ':'
          TabOrder = 5
        end
        object StaticText28: TStaticText
          Left = 89
          Top = 19
          Width = 8
          Height = 17
          Caption = ':'
          TabOrder = 6
        end
        object StaticText29: TStaticText
          Left = 89
          Top = 36
          Width = 8
          Height = 17
          Caption = ':'
          TabOrder = 15
        end
        object StaticText30: TStaticText
          Left = 89
          Top = 56
          Width = 8
          Height = 17
          Caption = ':'
          TabOrder = 7
        end
        object StaticText31: TStaticText
          Left = 89
          Top = 73
          Width = 8
          Height = 17
          Caption = ':'
          TabOrder = 8
        end
        object StaticText32: TStaticText
          Left = 89
          Top = 90
          Width = 8
          Height = 17
          Caption = ':'
          TabOrder = 9
        end
        object StaticText33: TStaticText
          Left = 89
          Top = 125
          Width = 8
          Height = 17
          Caption = ':'
          TabOrder = 10
        end
        object StaticText36: TStaticText
          Left = 89
          Top = 108
          Width = 8
          Height = 17
          Caption = ':'
          TabOrder = 11
        end
        object lb7: TStaticText
          Left = 89
          Top = 160
          Width = 8
          Height = 17
          Caption = ':'
          TabOrder = 14
        end
        object lb3: TStaticText
          Left = 89
          Top = 142
          Width = 8
          Height = 17
          Caption = ':'
          TabOrder = 16
        end
      end
      object tsDetails: TTabSheet
        Caption = 'Details'
        ImageIndex = 1
        object lbTrackDetails: TLabel
          Left = 110
          Top = 5
          Width = 12
          Height = 13
          Caption = '---'
        end
        object Label11: TLabel
          Left = 3
          Top = 20
          Width = 27
          Height = 13
          Caption = 'Name'
        end
        object lbNameDetails: TLabel
          Left = 110
          Top = 21
          Width = 12
          Height = 13
          Caption = '---'
        end
        object lbClassdetails: TLabel
          Left = 110
          Top = 37
          Width = 12
          Height = 13
          Caption = '---'
        end
        object Label12: TLabel
          Left = 3
          Top = 37
          Width = 25
          Height = 13
          Caption = 'Class'
        end
        object Label13: TLabel
          Left = 3
          Top = 54
          Width = 24
          Height = 13
          Caption = 'Type'
        end
        object Label14: TLabel
          Left = 3
          Top = 72
          Width = 35
          Height = 13
          Caption = 'Domain'
        end
        object lbDomain: TLabel
          Left = 110
          Top = 72
          Width = 12
          Height = 13
          Caption = '---'
        end
        object Label15: TLabel
          Left = 85
          Top = 105
          Width = 3
          Height = 13
        end
        object lbPropulsion: TLabel
          Left = 110
          Top = 108
          Width = 12
          Height = 13
          Caption = '---'
        end
        object lbIdentifier: TLabel
          Left = 110
          Top = 90
          Width = 12
          Height = 13
          Caption = '---'
        end
        object lbDoppler: TLabel
          Left = 110
          Top = 144
          Width = 12
          Height = 13
          Caption = '---'
        end
        object lbSonarClass: TLabel
          Left = 110
          Top = 126
          Width = 12
          Height = 13
          Caption = '---'
        end
        object lbTrackType: TLabel
          Left = 110
          Top = 161
          Width = 12
          Height = 13
          Caption = '---'
        end
        object lbTypeDetails: TLabel
          Left = 110
          Top = 54
          Width = 12
          Height = 13
          Caption = '---'
        end
        object lbMergeStatus: TLabel
          Left = 110
          Top = 180
          Width = 12
          Height = 13
          Caption = '---'
        end
        object StaticText2: TStaticText
          Left = 3
          Top = 3
          Width = 30
          Height = 17
          Caption = 'Track'
          TabOrder = 0
        end
        object StaticText3: TStaticText
          Left = 3
          Top = 90
          Width = 48
          Height = 17
          Caption = 'Identifier'
          TabOrder = 1
        end
        object StaticText4: TStaticText
          Left = 3
          Top = 108
          Width = 80
          Height = 17
          Caption = 'Propulsion Type'
          TabOrder = 2
        end
        object StaticText5: TStaticText
          Left = 3
          Top = 126
          Width = 41
          Height = 17
          Caption = 'Doppler'
          TabOrder = 3
        end
        object StaticText11: TStaticText
          Left = 3
          Top = 144
          Width = 60
          Height = 17
          Caption = 'Sonar Class'
          TabOrder = 4
        end
        object StaticText12: TStaticText
          Left = 3
          Top = 162
          Width = 57
          Height = 17
          Caption = 'Track Type'
          TabOrder = 5
        end
        object StaticText13: TStaticText
          Left = 3
          Top = 180
          Width = 68
          Height = 17
          Caption = 'Merge Status'
          TabOrder = 6
        end
        object StaticText37: TStaticText
          Left = 96
          Top = 3
          Width = 8
          Height = 17
          Caption = ':'
          TabOrder = 7
        end
        object StaticText38: TStaticText
          Left = 96
          Top = 19
          Width = 8
          Height = 17
          Caption = ':'
          TabOrder = 8
        end
        object StaticText39: TStaticText
          Left = 96
          Top = 35
          Width = 8
          Height = 17
          Caption = ':'
          TabOrder = 9
        end
        object StaticText40: TStaticText
          Left = 96
          Top = 52
          Width = 8
          Height = 17
          Caption = ':'
          TabOrder = 10
        end
        object StaticText41: TStaticText
          Left = 96
          Top = 72
          Width = 8
          Height = 17
          Caption = ':'
          TabOrder = 11
        end
        object StaticText42: TStaticText
          Left = 96
          Top = 90
          Width = 8
          Height = 17
          Caption = ':'
          TabOrder = 12
        end
        object StaticText43: TStaticText
          Left = 96
          Top = 126
          Width = 8
          Height = 17
          Caption = ':'
          TabOrder = 13
        end
        object StaticText44: TStaticText
          Left = 96
          Top = 144
          Width = 8
          Height = 17
          Caption = ':'
          TabOrder = 14
        end
        object StaticText45: TStaticText
          Left = 96
          Top = 160
          Width = 8
          Height = 17
          Caption = ':'
          TabOrder = 15
        end
        object StaticText46: TStaticText
          Left = 96
          Top = 108
          Width = 8
          Height = 17
          Caption = ':'
          TabOrder = 16
        end
        object StaticText47: TStaticText
          Left = 96
          Top = 180
          Width = 8
          Height = 17
          Caption = ':'
          TabOrder = 17
        end
      end
      object tsDetection: TTabSheet
        Caption = 'Detection'
        ImageIndex = 2
        object lbTrackDetection: TLabel
          Left = 137
          Top = 5
          Width = 12
          Height = 13
          Caption = '---'
        end
        object Label16: TLabel
          Left = 3
          Top = 19
          Width = 27
          Height = 13
          Caption = 'Name'
        end
        object lbNameDetection: TLabel
          Left = 137
          Top = 20
          Width = 12
          Height = 13
          Caption = '---'
        end
        object lbClassDetection: TLabel
          Left = 137
          Top = 36
          Width = 12
          Height = 13
          Caption = '---'
        end
        object Label17: TLabel
          Left = 3
          Top = 36
          Width = 25
          Height = 13
          Caption = 'Class'
        end
        object Label18: TLabel
          Left = 3
          Top = 62
          Width = 88
          Height = 13
          Caption = 'Owner PU Number'
        end
        object Label19: TLabel
          Left = 3
          Top = 93
          Width = 68
          Height = 13
          Caption = 'First Detected'
        end
        object lbFirstDetected: TLabel
          Left = 137
          Top = 93
          Width = 12
          Height = 13
          Caption = '---'
        end
        object Label20: TLabel
          Left = 85
          Top = 114
          Width = 3
          Height = 13
        end
        object lbLastDetected: TLabel
          Left = 137
          Top = 114
          Width = 12
          Height = 13
          Caption = '---'
        end
        object lbOwner: TLabel
          Left = 137
          Top = 62
          Width = 12
          Height = 13
          Caption = '---'
        end
        object lbDetectionDetectionType: TLabel
          Left = 137
          Top = 135
          Width = 8
          Height = 13
          Caption = '--'
        end
        object StaticText14: TStaticText
          Left = 3
          Top = 3
          Width = 30
          Height = 17
          Caption = 'Track'
          TabOrder = 0
        end
        object StaticText15: TStaticText
          Left = 3
          Top = 113
          Width = 71
          Height = 17
          Caption = 'Last Detected'
          TabOrder = 1
        end
        object lbDetectionType: TStaticText
          Left = 3
          Top = 133
          Width = 77
          Height = 17
          Caption = 'Detection Type'
          TabOrder = 2
        end
        object StaticText48: TStaticText
          Left = 120
          Top = 3
          Width = 8
          Height = 17
          Caption = ':'
          TabOrder = 3
        end
        object StaticText49: TStaticText
          Left = 120
          Top = 18
          Width = 8
          Height = 17
          Caption = ':'
          TabOrder = 4
        end
        object StaticText50: TStaticText
          Left = 120
          Top = 34
          Width = 8
          Height = 17
          Caption = ':'
          TabOrder = 5
        end
        object StaticText51: TStaticText
          Left = 120
          Top = 60
          Width = 8
          Height = 17
          Caption = ':'
          TabOrder = 6
        end
        object StaticText53: TStaticText
          Left = 120
          Top = 91
          Width = 8
          Height = 17
          Caption = ':'
          TabOrder = 7
        end
        object StaticText54: TStaticText
          Left = 120
          Top = 133
          Width = 8
          Height = 17
          Caption = ':'
          TabOrder = 8
        end
        object StaticText55: TStaticText
          Left = 120
          Top = 112
          Width = 8
          Height = 17
          Caption = ':'
          TabOrder = 9
        end
      end
      object tsIFF: TTabSheet
        Caption = 'IFF'
        ImageIndex = 3
        object lbTrackIff: TLabel
          Left = 91
          Top = 3
          Width = 12
          Height = 13
          Caption = '---'
        end
        object Label88: TLabel
          Left = 3
          Top = 19
          Width = 27
          Height = 13
          Caption = 'Name'
        end
        object lbNameIff: TLabel
          Left = 91
          Top = 19
          Width = 12
          Height = 13
          Caption = '---'
        end
        object lbClassIff: TLabel
          Left = 91
          Top = 36
          Width = 12
          Height = 13
          Caption = '---'
        end
        object Label91: TLabel
          Left = 3
          Top = 36
          Width = 25
          Height = 13
          Caption = 'Class'
        end
        object Label92: TLabel
          Left = 3
          Top = 66
          Width = 35
          Height = 13
          Caption = 'Mode 1'
        end
        object Label93: TLabel
          Left = 3
          Top = 85
          Width = 35
          Height = 13
          Caption = 'Mode 2'
        end
        object lbMode2Iff: TLabel
          Left = 91
          Top = 86
          Width = 12
          Height = 13
          Caption = '---'
        end
        object Label95: TLabel
          Left = 95
          Top = 106
          Width = 3
          Height = 13
        end
        object lbMode1Iff: TLabel
          Left = 91
          Top = 66
          Width = 12
          Height = 13
          Caption = '---'
        end
        object lbMode3CIff: TLabel
          Left = 91
          Top = 126
          Width = 12
          Height = 13
          Caption = '---'
        end
        object lbMode3Iff: TLabel
          Left = 91
          Top = 106
          Width = 12
          Height = 13
          Caption = '---'
        end
        object lbMode4Iff: TLabel
          Left = 91
          Top = 147
          Width = 12
          Height = 13
          Caption = '---'
        end
        object StaticText17: TStaticText
          Left = 3
          Top = 3
          Width = 30
          Height = 17
          Caption = 'Track'
          TabOrder = 0
        end
        object StaticText18: TStaticText
          Left = 3
          Top = 105
          Width = 39
          Height = 17
          Caption = 'Mode 3'
          TabOrder = 1
        end
        object StaticText19: TStaticText
          Left = 3
          Top = 125
          Width = 46
          Height = 17
          Caption = 'Mode 3C'
          TabOrder = 2
        end
        object StaticText20: TStaticText
          Left = 3
          Top = 145
          Width = 39
          Height = 17
          Caption = 'Mode 4'
          TabOrder = 3
        end
        object StaticText52: TStaticText
          Left = 80
          Top = 3
          Width = 8
          Height = 17
          Caption = ':'
          TabOrder = 4
        end
        object StaticText56: TStaticText
          Left = 80
          Top = 19
          Width = 8
          Height = 17
          Caption = ':'
          TabOrder = 5
        end
        object StaticText57: TStaticText
          Left = 80
          Top = 35
          Width = 8
          Height = 17
          Caption = ':'
          TabOrder = 6
        end
        object StaticText59: TStaticText
          Left = 80
          Top = 64
          Width = 8
          Height = 17
          Caption = ':'
          TabOrder = 7
        end
        object StaticText60: TStaticText
          Left = 80
          Top = 84
          Width = 8
          Height = 17
          Caption = ':'
          TabOrder = 8
        end
        object StaticText61: TStaticText
          Left = 80
          Top = 124
          Width = 8
          Height = 17
          Caption = ':'
          TabOrder = 9
        end
        object StaticText62: TStaticText
          Left = 80
          Top = 145
          Width = 8
          Height = 17
          Caption = ':'
          TabOrder = 10
        end
        object StaticText63: TStaticText
          Left = 80
          Top = 104
          Width = 8
          Height = 17
          Caption = ':'
          TabOrder = 11
        end
      end
    end
    object lvTrackTable: TListView
      Left = 0
      Top = 0
      Width = 337
      Height = 83
      Align = alTop
      Columns = <
        item
          Caption = 'Domain'
        end
        item
          Caption = 'TrackNumber'
        end
        item
          Caption = 'Identity'
        end
        item
          Caption = 'Course'
        end
        item
          Caption = 'Speed'
        end
        item
          Caption = 'Altitude'
        end
        item
          Caption = 'Depth'
        end>
      MultiSelect = True
      ReadOnly = True
      RowSelect = True
      TabOrder = 2
      ViewStyle = vsReport
      OnSelectItem = lvTrackTableSelectItem
    end
  end
  object pnlTop: TPanel
    Left = 0
    Top = 0
    Width = 1353
    Height = 33
    Align = alTop
    BevelKind = bkFlat
    BevelOuter = bvNone
    TabOrder = 0
    object ToolBar1: TToolBar
      Left = 0
      Top = 0
      Width = 1349
      Height = 29
      ButtonHeight = 23
      Caption = 'ToolBar1'
      Images = ImageList1
      TabOrder = 0
      object ToolButton2: TToolButton
        Left = 0
        Top = 0
        Width = 8
        Caption = 'ToolButton2'
        ImageIndex = 1
        Style = tbsSeparator
      end
      object tbtnFullScreen: TToolButton
        Left = 8
        Top = 0
        Hint = 'Full Screen'
        AutoSize = True
        Caption = 'Full Screen'
        ImageIndex = 0
        ParentShowHint = False
        ShowHint = True
        OnClick = tbtnFullScreenClick
      end
      object ToolButton43: TToolButton
        Left = 31
        Top = 0
        Width = 8
        Caption = 'ToolButton43'
        ImageIndex = 31
        Style = tbsSeparator
      end
      object cbSetScale: TComboBox
        Left = 39
        Top = 0
        Width = 76
        Height = 21
        Hint = 'Select Scale'
        TabOrder = 1
        OnChange = cbSetScaleChange
      end
      object toolbtnDecreaseScale: TToolButton
        Left = 115
        Top = 0
        Hint = 'Decrease Scale'
        ImageIndex = 1
        OnClick = tbtnScaleDecreaseClick
      end
      object toolbtnIncreaseScale: TToolButton
        Left = 138
        Top = 0
        Hint = 'Increase Scale'
        Caption = 'toolbtnIncreaseScale'
        ImageIndex = 2
        OnClick = tbtnScaleIncreaseClick
      end
      object ToolButton8: TToolButton
        Left = 161
        Top = 0
        Width = 8
        Caption = 'ToolButton8'
        ImageIndex = 6
        Style = tbsSeparator
      end
      object toolbtnZoom: TToolButton
        Left = 169
        Top = 0
        Hint = 'Zoom'
        Caption = 'toolbtnZoom'
        ImageIndex = 3
        OnClick = toolbtnZoomClick
      end
      object ToolBtnCentreOnHook: TToolButton
        Left = 192
        Top = 0
        Hint = 'Center On Hook'
        Caption = 'ToolBtnCentreOnHook'
        ImageIndex = 4
        OnClick = ToolBtnCentreOnHookClick
      end
      object ToolBtnCentreOnGameCentre: TToolButton
        Left = 215
        Top = 0
        Hint = 'Center On Game Center'
        Caption = 'ToolBtnCentreOnGameCentre'
        ImageIndex = 5
        OnClick = ToolBtnCentreOnGameCentreClick
      end
      object ToolBtnPan: TToolButton
        Left = 238
        Top = 0
        Hint = 'Pan'
        Caption = 'ToolBtnPan'
        ImageIndex = 6
        OnClick = ToolBtnPanClick
      end
      object toolBtnFilterRangeRings: TToolButton
        Left = 261
        Top = 0
        Hint = 'Filter Range Rings'
        AllowAllUp = True
        Caption = 'toolBtnFilterRangeRings'
        ImageIndex = 7
        Style = tbsCheck
        OnClick = toolBtnFilterRangeRingsClick
      end
      object ToolBtnRangeRingsOnHook: TToolButton
        Left = 284
        Top = 0
        Hint = 'Range Rings On Hook'
        AllowAllUp = True
        Caption = 'ToolBtnRangeRingsOnHook'
        ImageIndex = 8
        Style = tbsCheck
        OnClick = ToolBtnRangeRingsOnHookClick
      end
      object ToolButton15: TToolButton
        Left = 307
        Top = 0
        Width = 8
        Caption = 'ToolButton15'
        ImageIndex = 12
        Style = tbsSeparator
      end
      object ToolBtnHookPrevious: TToolButton
        Left = 315
        Top = 0
        Hint = 'Hook Previous Track'
        Caption = 'ToolBtnHookPrevious'
        ImageIndex = 9
        OnClick = ToolBtnHookPreviousClick
      end
      object ToolBtnHookNext: TToolButton
        Left = 338
        Top = 0
        Hint = 'Hooks Next Track'
        Caption = 'ToolBtnHookNext'
        ImageIndex = 10
        OnClick = ToolBtnHookNextClick
      end
      object ToolButton18: TToolButton
        Left = 361
        Top = 0
        Width = 8
        Caption = 'ToolButton18'
        ImageIndex = 14
        Style = tbsSeparator
      end
      object ToolBtnAddToTrackTable: TToolButton
        Left = 369
        Top = 0
        Hint = 'Add To Track Table'
        Caption = 'ToolBtnAddToTrackTable'
        ImageIndex = 11
        OnClick = ToolBtnAddToTrackTableClick
      end
      object ToolBtnRemoveFromTrackTable: TToolButton
        Left = 392
        Top = 0
        Hint = 'Remove From Track Table'
        Caption = 'ToolBtnRemoveFromTrackTable'
        ImageIndex = 12
        OnClick = ToolBtnRemoveFromTrackTableClick
      end
      object ToolButton44: TToolButton
        Left = 415
        Top = 0
        Width = 23
        AutoSize = True
        Caption = 'ToolButton44'
        ImageIndex = 31
        Style = tbsSeparator
      end
      object cbAssumeControl: TComboBox
        Left = 438
        Top = 0
        Width = 195
        Height = 21
        Hint = 'Platform Select (for control)'
        Style = csDropDownList
        DropDownCount = 10
        TabOrder = 0
        OnChange = cbAssumeControlChange
      end
      object ToolBtnAssumeControlOfHook: TToolButton
        Left = 633
        Top = 0
        Hint = 'Assume Control Of Hook'
        Caption = 'ToolBtnAssumeControlOfHook'
        ImageIndex = 13
        OnClick = ToolBtnAssumeControlOfHookClick
      end
      object ToolButton21: TToolButton
        Left = 656
        Top = 0
        Width = 8
        Caption = 'ToolButton21'
        ImageIndex = 16
        Style = tbsSeparator
      end
      object btn1: TToolButton
        Left = 664
        Top = 0
        Hint = 'Help'
        Caption = 'btn1'
        Enabled = False
        ImageIndex = 35
      end
      object btn2: TToolButton
        Left = 687
        Top = 0
        Width = 8
        Caption = 'btn2'
        ImageIndex = 31
        Style = tbsSeparator
      end
      object ToolBtnEdit: TToolButton
        Left = 695
        Top = 0
        Hint = 'Edit Text'
        Caption = 'ToolBtnEdit'
        ImageIndex = 14
        OnClick = ToolBtnEditClick
      end
      object ToolButton23: TToolButton
        Left = 718
        Top = 0
        Width = 8
        Caption = 'ToolButton23'
        ImageIndex = 17
        Style = tbsSeparator
      end
      object ToolBtnMerge: TToolButton
        Left = 726
        Top = 0
        Hint = 'Merge'
        Caption = 'ToolBtnMerge'
        ImageIndex = 34
        OnClick = ToolBtnMergeClick
      end
      object ToolBtnSplit: TToolButton
        Left = 749
        Top = 0
        Hint = 'Split'
        Caption = 'ToolBtnSplit'
        Enabled = False
        ImageIndex = 33
        OnClick = ToolBtnSplitClick
      end
      object btn5: TToolButton
        Left = 772
        Top = 0
        Width = 8
        Caption = 'btn5'
        ImageIndex = 31
        Style = tbsSeparator
      end
      object btn6: TToolButton
        Left = 780
        Top = 0
        Hint = 'Add Data Link'
        Caption = 'btn6'
        Enabled = False
        ImageIndex = 32
      end
      object btn7: TToolButton
        Left = 803
        Top = 0
        Hint = 'Remove Data Link'
        Caption = 'btn7'
        Enabled = False
        ImageIndex = 31
      end
      object ToolBtnTrackHistory: TToolButton
        Left = 826
        Top = 0
        Hint = 'History'
        Caption = 'ToolBtnTrackHistory'
        ImageIndex = 15
        OnClick = ToolBtnTrackHistoryClick
      end
      object btn9: TToolButton
        Left = 849
        Top = 0
        Hint = 'Remove'
        Caption = 'btn9'
        ImageIndex = 26
        OnClick = btn9Click
      end
      object ToolBtnTransferSonobuoy: TToolButton
        Left = 872
        Top = 0
        Hint = 'Transfer Sonobuoy'
        Caption = 'ToolBtnTransferSonobuoy'
        Enabled = False
        ImageIndex = 18
      end
      object ToolBtnRemoveSonobuoy: TToolButton
        Left = 895
        Top = 0
        Hint = 'Remove Sonobuoy'
        Caption = 'ToolBtnRemoveSonobuoy'
        Enabled = False
        ImageIndex = 17
      end
      object ToolBtnAddSonobuoy: TToolButton
        Left = 918
        Top = 0
        Hint = 'Add Sonobuoy'
        Caption = 'ToolBtnAddSonobuoy'
        Enabled = False
        ImageIndex = 16
      end
      object ToolButton25: TToolButton
        Left = 941
        Top = 0
        Width = 8
        Caption = 'ToolButton25'
        ImageIndex = 18
        Style = tbsSeparator
      end
      object ToolBtnFilterCursor: TToolButton
        Left = 949
        Top = 0
        Hint = 'Filter Cursor'
        Caption = 'ToolBtnFilterCursor'
        ImageIndex = 27
        OnClick = ToolBtnFilterCursorClick
      end
      object ToolBtnAnchorCursor: TToolButton
        Left = 972
        Top = 0
        Hint = 'Anchor Cursor'
        Caption = 'ToolBtnAnchorCursor'
        ImageIndex = 28
        OnClick = ToolBtnAnchorCursorClick
      end
      object ToolBtnOptions: TToolButton
        Left = 995
        Top = 0
        Hint = 'Options'
        Caption = 'ToolBtnOptions'
        ImageIndex = 29
        OnClick = ToolBtnOptionsClick
      end
      object ToolButton27: TToolButton
        Left = 1018
        Top = 0
        Width = 8
        Caption = 'ToolButton27'
        ImageIndex = 19
        Style = tbsSeparator
      end
      object ToolBtnContents: TToolButton
        Left = 1026
        Top = 0
        Hint = 'Contents'
        Caption = 'ToolBtnContents'
        Enabled = False
        ImageIndex = 30
      end
      object btn8: TToolButton
        Left = 1049
        Top = 0
        Width = 8
        Caption = 'btn8'
        ImageIndex = 32
        Style = tbsSeparator
      end
      object tBtnGameFreeze: TToolButton
        Left = 1057
        Top = 0
        Hint = 'Freeze'
        Caption = 'tBtnGameFreeze'
        ImageIndex = 19
        OnClick = tBtnGameFreezeClick
      end
      object tbtnStartGame: TToolButton
        Left = 1080
        Top = 0
        Hint = 'Standard Speed'
        Caption = 'tbtnStartGame'
        ImageIndex = 20
        OnClick = tbtnStartGameClick
      end
      object tBtnDoubleSpeed: TToolButton
        Left = 1103
        Top = 0
        Hint = 'Double Current Speed'
        Caption = 'tBtnDoubleSpeed'
        ImageIndex = 21
        OnClick = tBtnDoubleSpeedClick
      end
      object ToolButton31: TToolButton
        Left = 1126
        Top = 0
        Width = 8
        Caption = 'ToolButton31'
        ImageIndex = 22
        Style = tbsSeparator
      end
      object ToolBtnFind: TToolButton
        Left = 1134
        Top = 0
        Hint = 'Monitor Student'
        Caption = 'ToolBtnFind'
        ImageIndex = 22
        OnClick = ToolBtnFindClick
      end
      object ToolBtnAnnotate: TToolButton
        Left = 1157
        Top = 0
        Hint = 'Annotate'
        Caption = 'ToolBtnAnnotate'
        Enabled = False
        ImageIndex = 23
      end
      object ToolBtnSnapshot: TToolButton
        Left = 1180
        Top = 0
        Hint = 'Snapshot'
        Caption = 'ToolBtnSnapshot'
        ImageIndex = 24
        OnClick = ToolBtnSnapshotClick
      end
      object ToolButton35: TToolButton
        Left = 1203
        Top = 0
        Width = 8
        Caption = 'ToolButton35'
        ImageIndex = 25
        Style = tbsSeparator
      end
      object ToolBtnAddPlatform: TToolButton
        Left = 1211
        Top = 0
        Hint = 'Runtime Platform'
        Caption = 'ToolBtnAddPlatform'
        ImageIndex = 25
        OnClick = ToolBtnAddPlatformClick
      end
      object ToolBtnRemovePlatformOrTrack: TToolButton
        Left = 1234
        Top = 0
        Hint = 'Remove Platform / Track'
        Caption = 'ToolBtnRemovePlatformOrTrack'
        ImageIndex = 26
        OnClick = ToolBtnRemovePlatformOrTrackClick
      end
      object ToolButton42: TToolButton
        Left = 1257
        Top = 0
        Width = 8
        Caption = 'ToolButton42'
        ImageIndex = 31
        Style = tbsSeparator
      end
      object ToolButton45: TToolButton
        Left = 1265
        Top = 0
        Width = 8
        Caption = 'ToolButton45'
        ImageIndex = 31
        Style = tbsSeparator
      end
    end
  end
  object pnlMap: TPanel
    Left = 341
    Top = 33
    Width = 1012
    Height = 1550
    Align = alClient
    BevelKind = bkFlat
    BevelOuter = bvNone
    TabOrder = 2
    ExplicitHeight = 1530
    inline fmMapWindow1: TfmMapWindow
      Left = 0
      Top = 0
      Width = 1008
      Height = 1546
      Align = alClient
      TabOrder = 0
      ExplicitWidth = 1008
      ExplicitHeight = 1526
      inherited pnlTop: TPanel
        Width = 1008
        Height = 20
        Visible = False
        ExplicitWidth = 1008
        ExplicitHeight = 20
      end
      inherited pnlMap: TPanel
        Top = 20
        Width = 1008
        Height = 1526
        ExplicitTop = 20
        ExplicitWidth = 1008
        ExplicitHeight = 1506
        inherited Map: TMap
          Width = 1006
          Height = 1524
          OnExit = Map2Exit
          OnDblClick = Map2DblClick
          OnClick = Map2Click
          ExplicitLeft = -2
          ExplicitTop = 8
          ExplicitWidth = 1006
          ExplicitHeight = 1444
          ControlData = {
            8B1A0600F9670000839D0000010000000F0000FF0D47656F44696374696F6E61
            727905456D70747900E8030000000000000000000002000E001E000000000000
            0000000000000000000000000000000000000000000600010000000000500001
            010000640000000001F4010000050000800C000000000000000000000000FFFF
            FF000100000000000000000000000000000000000000000000000352E30B918F
            CE119DE300AA004BB85101000000900154F1140005417269616C000352E30B91
            8FCE119DE300AA004BB8510100000090015C790C0005417269616C0000000000
            00000000000000000000000000000000000000000000000000000000000000FF
            FFFF000000000000000001370000000000FFFFFF000000000000000352E30B91
            8FCE119DE300AA004BB851010000009001DC7C010005417269616C000352E30B
            918FCE119DE300AA004BB851010200009001A42C02000B4D61702053796D626F
            6C730000000000000001000100FFFFFF000200FFFFFF00000000000001000000
            0100011801000000C1DA1801000000F0E822001C000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000002
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            8076C000000000008056C0000000000080764000000000008056400100000018
            01000000C1DA1801000000C8E722001C00000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000200000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000E067657430EB220024
            00000001000000000000000000000070000000FFFFFFFF000000000000000000
            000088B3400000000000408F400001000001}
        end
      end
    end
  end
  object pnlBottom: TPanel
    Left = 0
    Top = 1583
    Width = 1353
    Height = 73
    Align = alBottom
    BevelKind = bkFlat
    BevelOuter = bvNone
    TabOrder = 3
    ExplicitTop = 1563
    object Panel1: TPanel
      Left = 0
      Top = -2
      Width = 1326
      Height = 69
      TabOrder = 0
      object Label55: TLabel
        Left = 358
        Top = 8
        Width = 34
        Height = 13
        Caption = 'Course'
      end
      object Label56: TLabel
        Left = 358
        Top = 26
        Width = 30
        Height = 13
        Caption = 'Speed'
      end
      object lbCourseHooked: TLabel
        Left = 407
        Top = 8
        Width = 12
        Height = 13
        Caption = '---'
      end
      object lbSpeedHooked: TLabel
        Left = 407
        Top = 26
        Width = 12
        Height = 13
        Caption = '---'
      end
      object Label60: TLabel
        Left = 480
        Top = 26
        Width = 30
        Height = 13
        Caption = 'Speed'
      end
      object Label62: TLabel
        Left = 480
        Top = 8
        Width = 24
        Height = 13
        Caption = 'Wind'
      end
      object lbRangeRings: TLabel
        Left = 719
        Top = 26
        Width = 12
        Height = 13
        Caption = '---'
      end
      object Label64: TLabel
        Left = 630
        Top = 26
        Width = 60
        Height = 13
        Caption = 'Range Rings'
      end
      object lblRangeScale: TLabel
        Left = 719
        Top = 8
        Width = 12
        Height = 13
        Caption = '---'
      end
      object Label66: TLabel
        Left = 630
        Top = 8
        Width = 59
        Height = 13
        Caption = 'Range Scale'
      end
      object lbRangeAnchor: TLabel
        Left = 830
        Top = 26
        Width = 12
        Height = 13
        Caption = '---'
      end
      object Label68: TLabel
        Left = 781
        Top = 26
        Width = 31
        Height = 13
        Caption = 'Range'
      end
      object lbBearingAnchor: TLabel
        Left = 830
        Top = 8
        Width = 12
        Height = 13
        Caption = '---'
      end
      object Label70: TLabel
        Left = 781
        Top = 8
        Width = 36
        Height = 13
        Caption = 'Bearing'
      end
      object Label71: TLabel
        Left = 974
        Top = 27
        Width = 12
        Height = 13
        Caption = '---'
      end
      object Label73: TLabel
        Left = 903
        Top = 8
        Width = 54
        Height = 13
        Caption = 'Origin OCM'
      end
      object Label74: TLabel
        Left = 974
        Top = 8
        Width = 12
        Height = 13
        Caption = '---'
      end
      object Label72: TLabel
        Left = 1032
        Top = 8
        Width = 22
        Height = 13
        Caption = 'OBM'
      end
      object lbLongitude: TLabel
        Left = 1064
        Top = 26
        Width = 12
        Height = 13
        Caption = '---'
      end
      object lbLatitude: TLabel
        Left = 1064
        Top = 8
        Width = 12
        Height = 13
        Caption = '---'
      end
      object lbY: TLabel
        Left = 1276
        Top = 22
        Width = 12
        Height = 13
        Alignment = taRightJustify
        Caption = '---'
      end
      object lbX: TLabel
        Left = 1276
        Top = 8
        Width = 12
        Height = 13
        Alignment = taRightJustify
        Caption = '---'
      end
      object Label79: TLabel
        Left = 1167
        Top = 8
        Width = 22
        Height = 13
        Caption = 'OBM'
      end
      object Label80: TLabel
        Left = 1167
        Top = 26
        Width = 21
        Height = 13
        Caption = 'CCG'
      end
      object Bevel1: TBevel
        Left = 772
        Top = 11
        Width = 3
        Height = 27
      end
      object Bevel2: TBevel
        Left = 614
        Top = 13
        Width = 3
        Height = 25
      end
      object Bevel3: TBevel
        Left = 462
        Top = 13
        Width = 3
        Height = 25
      end
      object Bevel4: TBevel
        Left = 894
        Top = 10
        Width = 3
        Height = 25
      end
      object Label35: TLabel
        Left = 903
        Top = 24
        Width = 33
        Height = 13
        Caption = 'SDarwi'
      end
      object Bevel5: TBevel
        Left = 1026
        Top = 11
        Width = 3
        Height = 25
      end
      object Bevel6: TBevel
        Left = 1155
        Top = 13
        Width = 3
        Height = 25
      end
      object lbColor: TLabel
        Left = 1199
        Top = 8
        Width = 12
        Height = 13
        Caption = '---'
      end
      object Label10: TLabel
        Left = 520
        Top = 8
        Width = 12
        Height = 13
        Caption = '---'
      end
      object Label21: TLabel
        Left = 520
        Top = 27
        Width = 12
        Height = 13
        Caption = '---'
      end
      object Label22: TLabel
        Left = 8
        Top = 4
        Width = 43
        Height = 16
        Caption = 'Status'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object pnlStatusRed: TPanel
        Left = 1
        Top = 3
        Width = 335
        Height = 17
        Alignment = taLeftJustify
        BevelOuter = bvNone
        Color = clRed
        Font.Charset = ANSI_CHARSET
        Font.Color = clWhite
        Font.Height = -17
        Font.Name = 'System'
        Font.Style = [fsBold]
        ParentBackground = False
        ParentFont = False
        TabOrder = 1
        Visible = False
        OnClick = pnlStatusRedClick
      end
      object StatusBar1: TStatusBar
        Left = 1
        Top = 41
        Width = 1324
        Height = 27
        BiDiMode = bdLeftToRight
        Panels = <
          item
            Alignment = taCenter
            Text = 'Fly-by Help'
            Width = 150
          end
          item
            Alignment = taCenter
            Text = 'Entities'
            Width = 70
          end
          item
            Alignment = taCenter
            Text = 'Filter'
            Width = 70
          end
          item
            Alignment = taCenter
            Text = 'Declutter'
            Width = 70
          end
          item
            Alignment = taCenter
            Style = psOwnerDraw
            Text = 'EMCON'
            Width = 70
          end
          item
            Alignment = taCenter
            Style = psOwnerDraw
            Text = 'Jamming'
            Width = 70
          end
          item
            Alignment = taCenter
            Style = psOwnerDraw
            Text = 'Gunfire'
            Width = 70
          end
          item
            Alignment = taCenter
            Style = psOwnerDraw
            Text = 'FCR LOCK'
            Width = 70
          end
          item
            Alignment = taCenter
            Text = 'COMMS'
            Width = 70
          end
          item
            Alignment = taCenter
            Style = psOwnerDraw
            Text = 'FROZEN'
            Width = 70
          end
          item
            Text = ' DateTime'
            Width = 70
          end>
        ParentBiDiMode = False
        OnDrawPanel = StatusBar1DrawPanel
      end
      object pnlStatusYellow: TPanel
        Left = 1
        Top = 20
        Width = 335
        Height = 17
        Alignment = taLeftJustify
        BevelOuter = bvNone
        Color = clYellow
        Font.Charset = ANSI_CHARSET
        Font.Color = clWhite
        Font.Height = -17
        Font.Name = 'System'
        Font.Style = [fsBold]
        ParentBackground = False
        ParentFont = False
        TabOrder = 2
        Visible = False
        OnClick = pnlStatusYellowClick
      end
    end
  end
  object MainMenu1: TMainMenu
    Images = ImageList1
    Left = 584
    Top = 64
    object View1: TMenuItem
      Caption = 'View'
      object Display1: TMenuItem
        Caption = 'Display'
        object Tactical1: TMenuItem
          Caption = 'Tactical'
          OnClick = Tactical1Click
        end
        object Tote1: TMenuItem
          Caption = 'Tote'
          OnClick = Tote1Click
        end
      end
      object mnFullScreen1: TMenuItem
        Caption = 'Full Screen'
        ImageIndex = 0
        OnClick = tbtnFullScreenClick
      end
      object Scale1: TMenuItem
        Caption = 'Scale'
        object Increase1: TMenuItem
          Caption = 'Increase'
          ImageIndex = 2
          ShortCut = 16457
          OnClick = tbtnScaleIncreaseClick
        end
        object Decrease1: TMenuItem
          Caption = 'Decrease'
          ImageIndex = 1
          ShortCut = 16452
          OnClick = tbtnScaleDecreaseClick
        end
        object Zoom1: TMenuItem
          Caption = 'Zoom...'
          ImageIndex = 3
          ShortCut = 16474
          OnClick = toolbtnZoomClick
        end
      end
      object Centre1: TMenuItem
        Caption = 'Centre'
        object Settings2: TMenuItem
          Caption = 'Settings...'
          ShortCut = 32881
          Visible = False
          OnClick = Settings2Click
        end
        object OnHookedTrack2: TMenuItem
          Caption = 'On Hooked Track'
          ImageIndex = 4
          ShortCut = 113
          OnClick = OnHookedTrack2Click
        end
        object OnGameCentre1: TMenuItem
          Caption = 'On Game Centre'
          ImageIndex = 5
          OnClick = OnGameCentre1Click
        end
        object Pan1: TMenuItem
          Caption = 'Pan'
          ImageIndex = 6
          OnClick = Pan1Click
        end
      end
      object RangeRings1: TMenuItem
        Caption = 'Range Rings'
        object Settings1: TMenuItem
          Caption = 'Settings'
          ShortCut = 32882
          OnClick = Settings1Click
        end
        object OnHookedTrack1: TMenuItem
          Caption = 'On Hooked Track'
          ImageIndex = 4
          ShortCut = 114
          OnClick = ToolBtnRangeRingsOnHookClick
        end
      end
      object Filters1: TMenuItem
        Caption = 'Filters...'
        OnClick = Filters1Click
      end
      object Overrides1: TMenuItem
        Caption = 'Overrides...'
        Visible = False
      end
      object History1: TMenuItem
        Caption = 'History...'
        OnClick = History1Click
      end
    end
    object Hook1: TMenuItem
      Caption = 'Hook'
      object Next1: TMenuItem
        Caption = 'Next'
        OnClick = Next1Click
      end
      object Previous1: TMenuItem
        Caption = 'Previous'
        OnClick = Previous1Click
      end
      object rackTable1: TMenuItem
        Caption = 'Track Table'
        object Add1: TMenuItem
          Caption = 'Add'
          OnClick = Add1Click
        end
        object Remove1: TMenuItem
          Caption = 'Remove'
          OnClick = Remove1Click
        end
      end
      object AssumeControl1: TMenuItem
        Caption = 'Assume Control'
        object HookedTrack1: TMenuItem
          Caption = 'Hooked Track'
          OnClick = ToolBtnAssumeControlOfHookClick
        end
        object CommandPlatform1: TMenuItem
          Caption = 'Command Platform'
          Visible = False
        end
      end
    end
    object Track1: TMenuItem
      Caption = 'Track'
      OnClick = Track1Click
      object Characteristics1: TMenuItem
        Caption = 'Characteristics'
        object Domain1: TMenuItem
          Caption = 'Domain'
          object A1: TMenuItem
            Caption = 'Air'
            OnClick = mniAir4Click
          end
          object Surface1: TMenuItem
            Caption = 'Surface'
            OnClick = mniSurface4Click
          end
          object Subsurface1: TMenuItem
            Caption = 'Subsurface'
            OnClick = mniSubsurface4Click
          end
          object Land1: TMenuItem
            Caption = 'Land'
            OnClick = mniLand4Click
          end
          object General1: TMenuItem
            Caption = 'General'
            OnClick = mniGeneral4Click
          end
        end
        object IDentity1: TMenuItem
          Caption = 'Identity'
          object Pending1: TMenuItem
            Caption = 'Pending'
            OnClick = mniPending1Click
          end
          object Unknown1: TMenuItem
            Caption = 'Unknown'
            OnClick = mniUnknowns1Click
          end
          object AssumedFriend1: TMenuItem
            Caption = 'Assumed Friend'
            OnClick = mniAssumedFriend1Click
          end
          object Friend1: TMenuItem
            Caption = 'Friend'
            OnClick = mniFriend1Click
          end
          object Neutral1: TMenuItem
            Caption = 'Neutral'
            OnClick = mniNeutral1Click
          end
          object Suspect1: TMenuItem
            Caption = 'Suspect'
            OnClick = mniSuspect1Click
          end
          object Hostile1: TMenuItem
            Caption = 'Hostile'
            OnClick = mniHostile1Click
          end
        end
        object PlatformType1: TMenuItem
          Caption = 'Platform Type'
          object AircraftCarrierCVCVN1: TMenuItem
            Caption = 'Aircraft Carrier (CV/CVN)'
            OnClick = mniAircraftCarrier1Click
          end
          object AmphibiousWarfare1: TMenuItem
            Caption = 'Amphibious Warfare'
            OnClick = mniAmphibius1Click
          end
          object Auxiliary1: TMenuItem
            Caption = 'Auxiliary'
            OnClick = mniAuxiliary1Click
          end
          object Chaff1: TMenuItem
            Caption = 'Chaff'
            OnClick = mniChaff1Click
          end
          object CruiserGuidedMissileCGCGN1: TMenuItem
            Caption = 'Cruiser, Guided Missile (CG/CGN)'
            OnClick = mniCruiserGuidedMissileCGCGN1Click
          end
          object Destroyer1: TMenuItem
            Caption = 'Destroyer'
            OnClick = mniDestroyer1Click
          end
          object DestroyerGuidedMissileDOG1: TMenuItem
            Caption = 'Destroyer, Guided Missile (DOG)'
            OnClick = mniDestroyerGuidedMissle1Click
          end
          object FrigateFF1: TMenuItem
            Caption = 'Frigate (FF)'
            OnClick = mniFrigateFF1Click
          end
          object FrigateGuidedMissileFFG1: TMenuItem
            Caption = 'Frigate, Guided Missile (FFG)'
            OnClick = mniFrigateGuidedMissleFFG1Click
          end
          object InfraredDecoy1: TMenuItem
            Caption = 'Infrared Decoy'
            OnClick = mniInfra1Click
          end
          object JammerDecoy1: TMenuItem
            Caption = 'Jammer Decoy'
            OnClick = mniJammerDecoy1Click
          end
          object Merchant1: TMenuItem
            Caption = 'Merchant'
            OnClick = mniMerchant1Click
          end
          object MainWarfare1: TMenuItem
            Caption = 'Main Warfare'
            OnClick = mniMainwarfare1Click
          end
          object PatrolCraftPTPTG1: TMenuItem
            Caption = 'Patrol Craft (PT/PTG)'
            OnClick = mniPatrolCraftPTPTG1Click
          end
          object UtilityVessel1: TMenuItem
            Caption = 'Utility Vessel'
            OnClick = mniUtilityVessel1Click
          end
          object Other1: TMenuItem
            Caption = 'Other'
            OnClick = mniOther1Click
          end
        end
        object Propulsion1: TMenuItem
          Caption = 'Propulsion Type '
          Visible = False
        end
        object Edit1: TMenuItem
          Caption = 'Edit'
          OnClick = mniEdit1Click
        end
      end
      object MErge1: TMenuItem
        Caption = 'Merge'
        OnClick = MErge1Click
      end
      object Split1: TMenuItem
        Caption = 'Split'
        OnClick = Split1Click
      end
      object Datalink1: TMenuItem
        Caption = 'Datalink'
        Visible = False
        object o1: TMenuItem
          Caption = 'To'
        end
        object From1: TMenuItem
          Caption = 'From'
        end
      end
      object Number1: TMenuItem
        Caption = 'Number'
        object Automatic1: TMenuItem
          Caption = 'Automatic'
          OnClick = Automatic1Click
        end
        object Manual1: TMenuItem
          Caption = 'Manual...'
          OnClick = Manual1Click
        end
      end
      object History2: TMenuItem
        Caption = 'History'
        OnClick = History2Click
      end
      object InitiateTMA1: TMenuItem
        Caption = 'Initiate TMA'
        Visible = False
      end
      object Sonobuoys1: TMenuItem
        Caption = 'Sonobuoys'
        Visible = False
        object OperatingMode1: TMenuItem
          Caption = 'Operating Mode'
        end
        object Depth1: TMenuItem
          Caption = 'Depth'
        end
        object Monitor1: TMenuItem
          Caption = 'Monitor'
        end
        object Destroy1: TMenuItem
          Caption = 'Destroy'
        end
      end
      object Break1: TMenuItem
        Caption = 'Break All Fire Control Asset Assignments'
        Visible = False
      end
      object RangeControlandBlindZone1: TMenuItem
        Caption = 'Range Circle and Blind Zones'
        Visible = False
        object ClearforHookedTracks1: TMenuItem
          Caption = 'Clear for Hooked Tracks'
          OnClick = ClearforHookedTracks1Click
        end
        object ClearforAllTracks1: TMenuItem
          Caption = 'Clear for All Tracks'
          OnClick = ClearforAllTracks1Click
        end
      end
      object Remove2: TMenuItem
        Caption = 'Remove'
        OnClick = Remove2Click
      end
    end
    object ools1: TMenuItem
      Caption = 'Tools'
      object Cursor1: TMenuItem
        Caption = 'Cursor'
        object Anchor1: TMenuItem
          Caption = 'Anchor'
          OnClick = Anchor1Click
        end
        object Origin1: TMenuItem
          Caption = 'Origin...'
          OnClick = Origin1Click
        end
        object Select1: TMenuItem
          Caption = 'Select...'
          OnClick = Select1Click
        end
        object SendEndPointExactly1: TMenuItem
          Caption = 'Send End Point Exactly...'
          Visible = False
        end
      end
      object Overlays1: TMenuItem
        Caption = 'Overlays...'
        OnClick = Overlays1Click
      end
      object Formation1: TMenuItem
        Caption = 'Formation...'
        OnClick = Formation1Click
      end
      object argetIntercept1: TMenuItem
        Caption = 'Target Intercept...'
        Visible = False
      end
      object argetPriorityA1: TMenuItem
        Caption = 'Target Priority Assessment...'
        Visible = False
      end
      object Opotions1: TMenuItem
        Caption = 'Options...'
        OnClick = Opotions1Click
      end
    end
    object Help1: TMenuItem
      Caption = 'Help'
      object Contents1: TMenuItem
        Caption = 'Contents'
        Enabled = False
      end
      object About1: TMenuItem
        Caption = 'About'
        Enabled = False
      end
    end
  end
  object ImageList1: TImageList
    BkColor = 14215660
    Left = 848
    Top = 72
    Bitmap = {
      494C010124008801900110001000ECE9D800FF10FFFFFFFFFFFFFFFF424D3600
      000000000000360000002800000040000000A0000000010020000000000000A0
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000B5B2B500B5B2B500B5B2B500B5B2
      B500B5B2B500B5B2B500B5B2B500B5B2B500B5B2B500B5B2B500B5B2B500B5B2
      B500B5B2B500B5B6B500CECBCE00FFFFFF00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC006361630073717300FFFFFF00FFFF
      FF00D8E9EC00D8E9EC00D8E9EC00D8E9EC00B5B2B500B5B2B500B5B2B500B5B2
      B500B5B2B500B5B2B500B5B2B500C6C7C600FFFFFF00EFEFEF00B5B2B500B5B2
      B500B5B2B500A5A6A500636163007B7D7B00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00C0C0
      C000C0C0C000C0C0C000C0C0C000D8E9EC00D8E9EC00181C180094929400D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00C6C3C60073717300737173006361630073717300FFFF
      FF00FFFFFF00D8E9EC00D8E9EC00D8E9EC00B5B2B500B5B2B500B5B2B500B5B2
      B500B5B2B500B5B6B500ADAEAD007371730094929400EFEFEF00B5B2B500B5B2
      B500B5B6B500B5B6B500737173007B7D7B00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00C0C0C000000000000000
      00000000000000000000C0C0C000D8E9EC00D8E9EC00D8E9EC00212021009C9A
      9C00B5B6B500D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC007371730063616300C6C3C600D8E9EC00636163007371
      7300FFFFFF00FFFFFF00C6C3C600D8E9EC00B5B2B500B5B2B500B5B2B500B5B2
      B500B5B2B5009C9E9C00737173007371730094929400EFEFEF00B5B2B500B5B2
      B500ADAAAD0073717300737173007B7D7B00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC000000000000000000C0C0C000D8E9EC00D8E9EC00D8E9EC00D8E9EC002124
      21008C8E8C00D8E9EC002928290084868400D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00C6C3C6007371730063616300D8E9EC00D8E9EC00D8E9EC00C6C3C6007371
      730073717300FFFFFF00FFFFFF00D8E9EC00B5B2B500B5B2B500B5B2B500B5B6
      B500ADAAAD0073717300737173007371730094929400EFEFEF00B5B6B500B5B2
      B5007371730073717300737173007B7D7B00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00C0C0C000D8E9EC00D8E9EC00D8E9EC00D8E9EC00C0C0C0000000
      0000D8E9EC0000000000C0C0C000D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00212421008C8A8C002928290084868400D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC007371730063616300C6C3C600D8E9EC00D8E9EC00FFFFFF00FFFFFF00D8E9
      EC006361630073717300FFFFFF00FFFFFF00B5B2B500CECFCE00FFFFFF00D6D3
      D6006B696B00737573009C9E9C006365630094929400F7F3F700ADAEAD006361
      63007B797B00A5A6A500636163007B7D7B00D8E9EC00D8E9EC00D8E9EC000000
      000000000000C0C0C000C0C0C000D8E9EC00C0C0C000C0C0C00000000000C0C0
      C000D8E9EC0000000000D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00BDBEBD00292C29000000000084868400D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00BDBEBD004241420073757300D8E9EC00D8E9EC00D8E9EC00C6C3C6007371
      730063616300D8E9EC00D8E9EC00C6C3C6007371730063616300FFFFFF00D8E9
      EC00D8E9EC007371730073717300FFFFFF00A5A2A50073717300737173006B69
      6B007B797B00B5B6B500ADAEAD007371730094929400E7E3E7006B6D6B006B6D
      6B00B5B6B500B5B6B500737173007B7D7B00D8E9EC00D8E9EC00D8E9EC00C0C0
      C000C0C0C00000000000C0C0C000C0C0C000C0C0C00000000000D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC002124
      210000000000000000000000000084868400D8E9EC00BDBEBD00B5B6B500D8E9
      EC00D8E9EC0039383900000000007B7D7B00D8E9EC00D8E9EC00636163007371
      7300D8E9EC00D8E9EC00D8E9EC00D8E9EC006361630073717300D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00636163007371730094929400636563006B696B007B7D
      7B00BDBEBD00B5B2B5009C9E9C006365630073717300737173007B7D7B00B5B2
      B500B5B2B500A5A6A500636163007B7D7B00C0C0C000C0C0C000C0C0C000C0C0
      C000C0C0C000C0C0C00000000000000000000000000000000000D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00BDBEBD00313031000000000000000000000000000000
      000000000000000000000000000000000000C6C3C6007371730063616300D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00C6C3C600FFFFFF00FFFFFF00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC0073717300B5B2B500B5B2B500B5B2B500B5B2
      B500B5B2B500B5B6B500ADAEAD00737173006B6D6B0073717300B5B2B500B5B2
      B500B5B6B500B5B6B500737173007B7D7B000000000000000000000000000000
      00000000000000000000000000000000000000000000C0C0C000D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC002124
      210000000000000000000000000084868400D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00393839000000000073717300D8E9EC0063616300C6C3C600D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC006361630073717300FFFFFF00C6C3
      C600D8E9EC00D8E9EC00D8E9EC00D8E9EC00B5B2B500B5B2B500B5B2B500CECB
      CE00EFEBEF00B5B2B5009C9E9C00636563007B7D7B00B5B2B500B5B2B500B5B2
      B500B5B2B500A5A6A5006361630073717300D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC0000000000000000000000000000000000C0C0C000D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00BDBEBD00292C29000000000084868400D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC003938390073757300D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00C6C3C6007371730073717300FFFFFF00FFFF
      FF00D8E9EC00D8E9EC00D8E9EC00D8E9EC00B5B2B500B5B6B500A5A6A5009C9E
      9C00EFEBEF00B5B2B500B5B2B500B5B2B500B5B2B500B5B2B500B5B2B500B5B2
      B500B5B2B500ADAAAD006B6D6B006B696B00D8E9EC00D8E9EC00D8E9EC00C0C0
      C000C0C0C00000000000D8E9EC00D8E9EC00C0C0C00000000000C0C0C000C0C0
      C000D8E9EC00C0C0C000C0C0C000D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00212421008C8A8C002928290084868400D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00FFFFFF00FFFFFF006361630073717300FFFF
      FF00FFFFFF00D8E9EC00D8E9EC00D8E9EC00B5B2B500CECFCE00CECFCE009C9E
      9C00FFFFFF00FFFFFF00EFEBEF00B5B2B500B5B2B500B5B2B500B5B2B500B5B2
      B500B5B2B500B5B2B500B5B2B500B5B2B500D8E9EC00D8E9EC00D8E9EC000000
      000000000000C0C0C000D8E9EC00D8E9EC00D8E9EC00D8E9EC0000000000C0C0
      C000C0C0C00000000000C0C0C000D8E9EC00D8E9EC00D8E9EC00D8E9EC002124
      21008C8E8C00D8E9EC002928290084868400D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00C6C3C6007371730073717300FFFFFF00C6C3C600737173006361
      6300FFFFFF00D8E9EC00D8E9EC00D8E9EC00A5A2A50073717300737173007371
      73006B696B008C8E8C00EFEBEF00B5B2B500B5B2B500B5B2B500B5B2B500B5B2
      B500B5B2B500B5B2B500B5B2B500B5B2B500D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00C0C0C0000000
      0000C0C0C00000000000C0C0C000D8E9EC00D8E9EC00D8E9EC00212021008C8E
      8C00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC006361630073717300FFFFFF00FFFFFF00737173007371
      7300D8E9EC00D8E9EC00D8E9EC00D8E9EC009492940063616300636563007371
      73006B696B007B797B00BDBEBD00B5B2B500B5B2B500B5B2B500B5B2B500B5B2
      B500B5B2B500B5B2B500B5B2B500B5B2B500D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00EDED
      ED000000000000000000C0C0C000D8E9EC00D8E9EC00181C180094929400D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC0073717300636163007371730063616300D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00B5B2B500B5B6B500A5A2A5009492
      9400EFEBEF00B5B2B500B5B2B500B5B2B500B5B2B500B5B2B500B5B2B500B5B2
      B500B5B2B500B5B2B500B5B2B500B5B2B500D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00000000000000
      00000000000000000000D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00B5B2B500B5B2B500949694007B7D
      7B00BDBEBD00B5B2B500B5B2B500B5B2B500B5B2B500B5B2B500B5B2B500B5B2
      B500B5B2B500B5B2B500B5B2B500B5B2B500D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00C6C3C600FFFFFF00737173007371730073717300FFFFFF00FFFFFF00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00B5B2B500B5B2B500B5B2B500B5B2
      B500B5B2B500B5B2B500B5B2B500B5B2B500B5B2B500B5B2B500B5B2B500B5B2
      B500B5B2B500C6C3C600FFFFFF00FFFFFF00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC0000000000000000000000000000000000C6C3C600D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00C6C3C600D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00C6C3C600FFFF
      FF00737173007371730063616300737173007371730063616300FFFFFF00FFFF
      FF00D8E9EC00D8E9EC00D8E9EC00D8E9EC00B5B2B500B5B2B500B5B2B500B5B2
      B500B5B2B500B5B2B500B5B2B500FFFFFF00FFFFFF00B5B2B500B5B2B500B5B2
      B500B5B2B50063616300D8E9EC00FFFFFF00D8E9EC00D8E9EC00D8E9EC00C6C3
      C600000000000000000000000000000000000000000000000000000000000000
      0000D8E9EC00D8E9EC00D8E9EC00D8E9EC00C6C3C6000000000000000000D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00000000000000
      0000D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00FFFFFF00737173007371
      73007371730063616300C6C3C600FFFFFF00FFFFFF007371730073717300FFFF
      FF00FFFFFF00C6C3C600D8E9EC00D8E9EC00B5B2B500B5B2B500B5B2B500B5B2
      B500B5B2B500C6C3C600D8E9EC0063616300FFFFFF00B5B2B500B5B2B500B5B2
      B500C6C3C600D8E9EC00D8E9EC00FFFFFF00D8E9EC00D8E9EC00D8E9EC000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000C6C3C600D8E9EC00D8E9EC00D8E9EC0000000000000400000000
      0000C6C3C600D8E9EC00D8E9EC00D8E9EC00D8E9EC0000000000C6C3C6000000
      0000D8E9EC00D8E9EC00D8E9EC00D8E9EC007371730073717300636163006361
      6300C6C3C600FFFFFF007371730073717300FFFFFF00FFFFFF00737173007371
      7300FFFFFF00FFFFFF00D8E9EC00D8E9EC00B5B2B500B5B2B500B5B2B500B5B2
      B500C6C3C600D8E9EC00D8E9EC00D8E9EC00FFFFFF00B5B2B500B5B2B500B5B2
      B500D8E9EC00D8E9EC00D8E9EC00FFFFFF00D8E9EC00D8E9EC00000000000000
      000000000000D8E9EC00C6C3C6000000000000000000D8E9EC00D8E9EC000000
      00000000000000000000D8E9EC00D8E9EC00D8E9EC00D8E9EC00000000001818
      180000000000C6C3C600D8E9EC00D8E9EC0000000000D8E9EC0000000000D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC006361630073717300C6C3C600FFFF
      FF007371730073717300737173007371730073717300FFFFFF00FFFFFF007371
      730073717300FFFFFF00FFFFFF00D8E9EC00B5B2B500B5B2B500B5B2B500C6C3
      C600D8E9EC0063616300D8E9EC00D8E9EC00FFFFFF00B5B2B500C6C3C600D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00FFFFFF00D8E9EC00D8E9EC00000000000000
      0000D8E9EC00D8E9EC00D8E9EC000000000000000000D8E9EC00D8E9EC00D8E9
      EC000000000000000000C6C3C600D8E9EC00D8E9EC00D8E9EC00D8E9EC000000
      00000004000000000000C6C3C60000000000D8E9EC0000000000D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC0073717300FFFFFF00737173007371
      7300737173007371730073717300737173007371730073717300FFFFFF00FFFF
      FF007371730073717300FFFFFF00FFFFFF00B5B2B500FFFFFF00FFFFFF00D8E9
      EC0063616300C6C3C60063616300D8E9EC00FFFFFF00C6C3C60063616300D8E9
      EC00B5B2B50063616300D8E9EC00FFFFFF00D8E9EC000000000000000000D8E9
      EC00D8E9EC00D8E9EC00D8E9EC000000000000000000D8E9EC00D8E9EC00D8E9
      EC00C6C3C6000000000000000000D8E9EC00D8E9EC00D8E9EC00D8E9EC00C6C3
      C600000000000004000000000000D8E9EC0000000000D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC006361630073717300737173007371
      730073717300737173007371730073717300737173007371730073717300FFFF
      FF00FFFFFF007371730073717300D8E9EC00D8E9EC00D8E9EC00D8E9EC006361
      6300B5B2B500C6C3C600D8E9EC00D8E9EC00FFFFFF00D8E9EC0063616300B5B2
      B500C6C3C600D8E9EC00D8E9EC00FFFFFF00D8E9EC0000000000000000000000
      0000D8E9EC00D8E9EC00D8E9EC000000000000000000D8E9EC00D8E9EC00D8E9
      EC00000000000000000000000000D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC0000000000C6C3C60000000000C6C3C600D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC007371730073717300737173007371
      7300737173007371730073717300737173007371730073717300737173007371
      7300FFFFFF0073717300FFFFFF00D8E9EC006361630063616300D8E9EC00B5B2
      B500B5B2B500B5B2B50063616300D8E9EC00D8E9EC00D8E9EC00B5B2B500B5B2
      B500B5B2B50063616300D8E9EC00FFFFFF00D8E9EC0000000000D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC000000000000000000D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC0000000000D8E9EC00D8E9EC00C6C3C600D8E9EC00D8E9
      EC0000000000D8E9EC00000000001818180000000000D8E9EC00D8E9EC00D8E9
      EC00C6C3C60000000000D8E9EC00D8E9EC0063616300C6C3C600636163007371
      7300737173007371730073717300737173007371730073717300737173007371
      73007371730073717300FFFFFF00D8E9EC00B5B2B500B5B2B500B5B2B500B5B2
      B500B5B2B500C6C3C600D8E9EC00D8E9EC0063616300B5B2B500B5B2B500B5B2
      B500C6C3C600D8E9EC00D8E9EC00FFFFFF00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC000000000000000000D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC000000000000000000000000000000
      0000C6C3C60000000000D8E9EC00000000001818180000000000C6C3C600D8E9
      EC00000000007371730000000000D8E9EC00D8E9EC0063616300C6C3C6007371
      7300737173007371730073717300737173007371730073717300737173007371
      73007371730073717300FFFFFF00FFFFFF00B5B2B500B5B2B500B5B2B500B5B2
      B500B5B2B500B5B2B50063616300D8E9EC00B5B2B500B5B2B500B5B2B500B5B2
      B500B5B2B50063616300D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC000000000000000000D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC000000000000000000000000000000
      000000000000D8E9EC00D8E9EC00D8E9EC000000000000040000000000000000
      0000000000007371730063616300D8E9EC00D8E9EC00D8E9EC0063616300C6C3
      C600636163007371730073717300737173007371730073717300737173007371
      7300737173007371730073717300FFFFFF00B5B2B500B5B2B500C6C3C600B5B2
      B500B5B2B500B5B2B500B5B2B500B5B2B500B5B2B500B5B2B500B5B2B500B5B2
      B500B5B2B500D8E9EC0063616300B5B2B500D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC000000000000000000D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC0000000000D8E9EC00D8E9EC000000
      000000000000D8E9EC00D8E9EC00D8E9EC00D8E9EC0000000000000000007371
      7300737173000000000000000000D8E9EC00D8E9EC00D8E9EC00C6C3C6006361
      6300D8E9EC007371730073717300737173007371730073717300737173007371
      7300737173006361630063616300D8E9EC00B5B2B500FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00B5B2B500B5B2B500B5B2B500B5B2B500B5B2B500B5B2
      B500B5B2B500B5B2B500B5B2B500B5B2B500D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC000000000000000000D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00C6C3C6000000
      000000000000D8E9EC00D8E9EC00D8E9EC00C6C3C60000000000737173007371
      73007371730000000000D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC0063616300C6C3C60063616300737173007371730073717300737173007371
      730063616300C6C3C600D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC0063616300FFFFFF00B5B2B500B5B2B500B5B2B500B5B2B500B5B2B500B5B2
      B500B5B2B500B5B2B500B5B2B500B5B2B500D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC0000000000C6C3C600D8E9EC0000000000D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00000000000000
      000000000000D8E9EC00D8E9EC00D8E9EC000000000000000000737173007371
      73007371730000000000D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00C6C3C60063616300C6C3C60073717300737173006361630063616300D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC0063616300D8E9EC0063616300D8E9
      EC0063616300C6C3C600B5B2B500B5B2B500B5B2B500B5B2B500B5B2B500B5B2
      B500B5B2B500B5B2B500B5B2B500B5B2B500D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC0000000000D8E9EC00D8E9EC0000000000D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC0000000000000000007371730073717300000000000000
      000000000000D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00636163007371730063616300C6C3C600D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00B5B2B500B5B2B500B5B2B500B5B2
      B500B5B2B500B5B2B500B5B2B500B5B2B500B5B2B500B5B2B500B5B2B500B5B2
      B500B5B2B500B5B2B500B5B2B500B5B2B500D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC000000000000000000D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC0000000000000000000000000000000000D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00B5B2B500B5B2B500B5B2B500B5B2
      B500B5B2B500B5B2B500B5B2B500B5B2B500B5B2B500B5B2B500B5B2B500B5B2
      B500B5B2B500B5B2B500B5B2B500B5B2B500D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00C6C3C600D8E9
      EC00C6C3C600D8E9EC00D8E9EC00D8E9EC00C6C3C600D8E9EC00D8E9EC00D8E9
      EC00C6C3C600D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00C6C3C600D8E9
      EC00C6C3C600D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00C6C3C600D8E9EC00BDBABD00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00636163000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000073717300C6C3C600D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00636163007371
      73007371730073717300C6C3C600D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00C6C3C600D8E9EC00D8E9
      EC00C6C3C600636163000000000039383900C6C3C600D8E9EC00C6C3C600D8E9
      EC00C6C3C600D8E9EC00D8E9EC00D8E9EC00D8E9EC007371730000000000D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC000000000063616300D8E9EC00C6C3C60000000000D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC005A595A00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00E7E3E700E7E3
      E700E7E3E70063616300D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC007371730000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000063616300D8E9EC00D8E9EC00D8E9EC00D8E9EC0000000000D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC0000000000D8E9EC0000000000000000000000
      0000D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00E7E3E700E7E3
      E700E7E3E70073717300D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC000000000073717300737173000000
      0000000000007371730073717300737173000000000000000000737173007371
      730000000000D8E9EC00D8E9EC00D8E9EC00D8E9EC0000000000D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC0000000000D8E9EC0000000000000000000000
      000000000000D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC005A595A00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00E7E3E700E7E3
      E700E7E3E70063616300C6C3C600D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC000000000073717300737173000000
      0000737173007371730073717300737173007371730000000000737173007371
      730000000000D8E9EC00D8E9EC00D8E9EC00D8E9EC0000000000D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC0000000000D8E9EC00D8E9EC00000000000000
      000000000000C6C3C600D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC000000
      0000D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00E7E3E700E7E3E70063616300C6C3C600D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC000000000073717300737173000000
      000073717300C6C3C600636163006B696B007371730000000000737173007371
      730000000000D8E9EC00D8E9EC00D8E9EC00D8E9EC0000000000D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00C6C3C60000000000D8E9EC00D8E9EC00D8E9EC000000
      00000000000000000000D8E9EC00D8E9EC00D8E9EC00C6C3C600000000000000
      0000D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00E7E3E700E7E3E70063616300D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC000000000073717300737173000000
      000063616300D8E9EC00C6C3C600737173007371730000000000737173007371
      730000000000D8E9EC00D8E9EC00D8E9EC00D8E9EC006361630000000000C6C3
      C600D8E9EC00D8E9EC00D8E9EC00C6C3C600D8E9EC00D8E9EC00D8E9EC00C6C3
      C600D8E9EC00D8E9EC000000000073717300D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00000000000000000000000000C6C3C600D8E9EC000000000000000000D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00E7E3E700E7E3E70073717300C6C3C600D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC0000000000FFFFFF00FFFFFF000000
      0000000000007371730073717300737173000000000000000000FFFFFF00FFFF
      FF0000000000D8E9EC00D8E9EC00D8E9EC00D8E9EC00C6C3C600636163000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000063616300D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC000000000000000000000000000000000000000000D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00E7E3E700E7E3E70063616300D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC0000000000FFFFFF00FFFFFF00FFFF
      FF000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF0000000000D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC0000000000D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00000000000000000000000000C6C3C600D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00E7E3E700E7E3E70063616300C6C3
      C600D8E9EC00D8E9EC00D8E9EC00D8E9EC000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC0000000000D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00C6C3C6000000000000000000000000000000000000000000D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00E7E3E700E7E3E7007371
      7300D8E9EC00C6C3C600D8E9EC00D8E9EC00D8E9EC000000000000000000D8E9
      EC007371730000000000FFFFFF007B7D7B0063616300D8E9EC00000000000000
      0000D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00C6C3C60000000000C6C3C600D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00000000000000000000000000D8E9EC00D8E9EC000000000000000000C6C3
      C600D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00E7E3E700E7E3
      E700636163007371730073717300C6C3C600D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00636163000000000031303100C6C3C600D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC000000000000000000000000000000000000000000000000000000
      0000D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00C6C3C600000000000000
      00000000000000000000D8E9EC00D8E9EC00D8E9EC00D8E9EC00000000000000
      0000D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00E7E3
      E700E7E3E700E7E3E70063616300D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC000000000000000000000000000000000000000000D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC0000000000000000000000
      000000000000D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC000000
      00005A595A00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00E7E3
      E700E7E3E700E7E3E70073717300C6C3C600D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00000000000000000000000000D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC0000000000000000000000
      0000D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC005A595A00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00E7E3
      E700E7E3E700E7E3E70063616300D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC0000000000D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00B5B6
      B500BDBEBD00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00BDBABD00DEDBDE00FFFFFF00FFFF
      FF00FFFFFF00E7E7E700D8E9EC00B5B6B500D6D7D600FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00EFEFEF00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00C6C3C600D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00C6C3C600D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00C6C3C600D8E9EC00D8E9EC00D8E9EC009492940063616300636563007371
      730084868400D8E9EC00D8E9EC009C9A9C006365630073717300737173007371
      73006B6D6B00636563007B7D7B00D8E9EC00D8E9EC00D8E9EC00D8E9EC000000
      000000000000D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC000000
      0000000000000000000000000000D8E9EC00D8E9EC00D8E9EC00000000000000
      00000000000000000000D8E9EC00D8E9EC00D8E9EC00BDBABD009C9E9C009C9A
      9C00E7E7E700B5B6B500BDBEBD00D8E9EC00B5B6B500ADAAAD00737173007371
      730094969400EFEFEF00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC000000
      0000000000000000000000000000C6C3C600D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC000000
      0000D8E9EC00D8E9EC00D8E9EC00D8E9EC0000000000D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC000000
      0000D8E9EC000000000000000000D8E9EC00D8E9EC00D8E9EC0000000000D8E9
      EC000000000000000000D8E9EC00D8E9EC00D8E9EC00D8E9EC00949694008482
      8400D6D7D600FFFFFF00FFFFFF00FFFFFF00D6D7D60073717300737173007371
      730084828400D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC000000
      00000000000000000000000000000000000000000000D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC000000
      000000000000D8E9EC00D8E9EC00D8E9EC00000000000000000000000000D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC000000
      0000C6C3C6000000000000000000D8E9EC00D8E9EC00C6C3C60000000000C6C3
      C6000000000000000000D8E9EC00D8E9EC00D8E9EC00D8E9EC00B5B6B500A5A6
      A500737173006B696B00636563007371730073717300737173006B6D6B008C8A
      8C00EFEBEF00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC000000
      000000000000000000000000000000000000000000000000000000000000C6C3
      C600D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC000000
      0000000000000000000000000000D8E9EC00000000000000000000000000D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC009496
      940094969400E7E7E700D8E9EC009C9A9C006365630073717300737173008482
      8400D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC000000
      0000D8E9EC000000000000000000000000007371730000000000D8E9EC000000
      00000000000000000000D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC009496
      940084828400D6D7D600EFEBEF00ADAAAD00737173006B696B008C8E8C00EFEB
      EF00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC000000
      000000000000000000000000000000000000000000000000000000000000D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC000000
      0000000000000000000000000000D8E9EC000000000000000000000000000000
      0000D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC000000
      0000C6C3C6000000000000000000000000000000000000000000C6C3C6000000
      00000000000000000000D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC009496940094969400D6D3D60073717300737173007371730084868400BDBE
      BD00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC000000
      00000000000000000000000000000000000000000000D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC000000
      00000000000000000000D8E9EC00D8E9EC00000000000000000000000000D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC0000000000000000000000000063616300C6C3C60063616300000000000000
      000000000000D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00B5B6
      B500A5A2A50084828400A5A6A500737173006B696B008C8E8C00EFEBEF00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC000000
      0000000000000000000000000000D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC000000
      000000000000D8E9EC00D8E9EC00D8E9EC0000000000D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC000000000000000000D8E9EC00D8E9EC00D8E9EC00000000000000
      0000D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC009C9E9C0073717300737173007371730084828400D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC000000
      000000000000D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC0000000000D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC000000000000000000D8E9EC00D8E9EC00D8E9EC00000000000000
      0000D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC009C9E9C00737173006B696B0094929400EFEBEF00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC0094969400636563007371730084868400D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00B5B6B500A5A2A5007B797B00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00B5B2B500BDBABD00DEDBDE00F7F3
      F700F7F3F700F7F7F700F7F7F700D6D7D600B5B2B500B5B2B500B5B2B500B5B2
      B500B5B2B500B5B2B500B5B2B500B5B2B500B5B2B5008C8A8C00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00FFFFFF00E7E7E700B5B2B500B5B2B500B5B2B500B5B2
      B500B5B2B500B5B2B500B5B2B500B5B2B500D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00000000000000
      0000000000000000000000000000D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00B5B2B50094969400848284008486
      84008486840084868400C6C3C600FFFBFF00D6D7D600B5B2B500B5B2B500B5B2
      B500B5B2B500B5B2B500B5B2B500B5B2B5008486840073717300737173007371
      730073717300737173009C9E9C00FFFFFF00E7E7E700B5B2B500B5B2B500B5B2
      B500B5B2B500B5B2B500B5B2B500B5B2B500D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC0000000000D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC009C9A9C0073757300737173007371
      730073717300737173007B7D7B00C6C3C600FFFBFF00D6D7D600B5B2B500B5B2
      B500B5B2B500B5B2B500B5B2B500B5B2B5007371730073717300737173007371
      7300737173007371730073717300A5A2A500FFFFFF00B5B2B500B5B2B500B5B2
      B500B5B2B500B5B2B500B5B2B500B5B2B500D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC0000000000D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC0000000000D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC007371730073717300737173007371
      73007371730073717300737173007B7D7B00C6C3C600DEDBDE00B5B2B500B5B2
      B500B5B2B500B5B2B500B5B2B500B5B2B5007371730073717300737173007371
      730073717300737173007371730073717300FFFFFF00B5B2B500B5B2B500B5B2
      B500BDBEBD00E7E7E700B5B2B500B5B2B500D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC0000000000D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC0000000000D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC006B6D6B0073717300737173007371
      730073717300737173007371730073717300B5B6B500DEDBDE00B5B2B500B5B2
      B500B5B2B500B5B2B500B5B2B500B5B2B5007371730073717300737173007371
      730073717300737173007371730073717300FFFFFF00B5B2B500B5B2B500B5B2
      B5008C8E8C00FFFFFF00E7E3E700B5B2B500D8E9EC00D8E9EC00D8E9EC000000
      00000000000000000000D8E9EC00D8E9EC00000000000000000000000000D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC0000000000D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC0000000000D8E9EC00D8E9EC00D8E9
      EC00D8E9EC0000000000D8E9EC00D8E9EC006B696B0073717300737173007371
      730073717300737173007371730073717300B5B6B500DEDBDE00B5B2B500B5B2
      B500B5B2B500B5B2B500B5B2B500B5B2B5007371730073717300737173007371
      730073717300737173007371730063656300FFFFFF00BDBABD00E7E3E700E7E3
      E70073717300A5A6A500FFFFFF00E7E3E700D8E9EC00D8E9EC00D8E9EC000000
      00000000000000000000D8E9EC00D8E9EC00000000000000000000000000D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC0000000000D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC0000000000D8E9EC00D8E9EC00D8E9
      EC00D8E9EC0000000000D8E9EC00D8E9EC006B6D6B0073717300737173007371
      73007371730073717300737173006B696B00ADAEAD00DEDBDE00B5B6B500D6D3
      D600EFEFEF00EFEFEF00EFEFEF00EFEFEF007371730073717300737173007371
      73007371730073717300737173006B696B00CECFCE0084868400A5A6A500A5A6
      A5007371730073717300A5A6A500FFFFFF00D8E9EC00D8E9EC00D8E9EC000000
      00000000000000000000D8E9EC00D8E9EC00000000000000000000000000D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC0000000000D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC0000000000D8E9EC00D8E9EC000000
      0000000000000000000000000000D8E9EC006B696B0073717300737173007371
      73007371730073717300737173006B6D6B0094969400BDBEBD009C9E9C008C8A
      8C00949294009492940094929400BDBEBD007371730073717300737173007371
      73007371730073717300636563009C9A9C00BDBABD006B696B00636563007371
      7300737173007371730063656300D6D3D600D8E9EC00D8E9EC00D8E9EC000000
      00000000000000000000D8E9EC00D8E9EC00000000000000000000000000D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC0000000000D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC0000000000D8E9EC00D8E9EC00D8E9EC000000
      0000000000000000000000000000000000008486840073717300737173007371
      730073717300737173006B696B0084828400ADAAAD00B5B6B500949694006B69
      6B006B696B006B696B006B696B008C8A8C009496940063656300636563007371
      730073717300636563009C9A9C00B5B2B500E7E7E70094969400949294009496
      9400636563007371730094929400B5B2B500D8E9EC00D8E9EC00D8E9EC000000
      00000000000000000000D8E9EC00D8E9EC00000000000000000000000000D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00000000000000
      0000000000000000000000000000D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC0000000000D8E9EC00D8E9EC00ADAAAD007B7D7B00636563006B69
      6B00737173006B696B0084868400ADAEAD00CECFCE00D6D3D600ADAAAD009C9E
      9C009C9E9C009C9E9C009C9E9C00A5A6A500B5B2B500949294009C9A9C007371
      7300C6C3C60094929400C6C3C60084828400FFFFFF00B5B2B500B5B2B500BDBA
      BD006361630094969400B5B2B500B5B2B500D8E9EC00D8E9EC00D8E9EC000000
      00000000000000000000D8E9EC00D8E9EC00000000000000000000000000D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC0000000000D8E9EC00D8E9EC00D8E9EC0000000000D8E9EC00D8E9EC00D8E9
      EC00D8E9EC0000000000D8E9EC00D8E9EC00B5B2B500A5A6A500A5A2A5008C8A
      8C00A5A6A500BDBABD00ADAEAD009C9E9C00B5B6B500DEDBDE00B5B2B500B5B2
      B500B5B2B500B5B2B500B5B2B500B5B2B500B5B2B500B5B2B500BDBABD006365
      6300FFFFFF00BDBABD008C8A8C006B696B00FFFFFF00B5B2B500B5B2B500BDBA
      BD008C8E8C00B5B2B500B5B2B500B5B2B500D8E9EC00D8E9EC00D8E9EC000000
      00000000000000000000D8E9EC00D8E9EC00000000000000000000000000D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC0000000000D8E9EC00D8E9EC000000000000000000D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00B5B2B500B5B2B500B5B6B5008C8A
      8C00B5B6B500DEDFDE009C9E9C0073757300B5B2B500DEDBDE00B5B2B500B5B2
      B500B5B2B500B5B2B500B5B2B500B5B2B500B5B2B500B5B2B500B5B2B5006B69
      6B00FFFFFF008C8A8C009496940073717300FFFFFF00B5B2B500B5B2B500B5B2
      B500B5B2B500B5B2B500B5B2B500B5B2B500D8E9EC00D8E9EC00D8E9EC000000
      00000000000000000000D8E9EC00D8E9EC00000000000000000000000000D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC0000000000D8E9EC0000000000D8E9EC0000000000D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00B5B2B500B5B2B500B5B2B5008C8E
      8C00BDBABD00BDBEBD00949294008C8A8C00B5B6B500DEDBDE00B5B2B500B5B2
      B500B5B2B500B5B2B500B5B2B500B5B2B500B5B2B500B5B2B500B5B2B5006365
      6300B5B2B5008C8E8C00BDBABD0063656300FFFFFF00B5B2B500B5B2B500B5B2
      B500B5B2B500B5B2B500B5B2B500B5B2B500D8E9EC00D8E9EC00D8E9EC000000
      00000000000000000000D8E9EC00D8E9EC00000000000000000000000000D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC000000000000000000D8E9EC00D8E9EC0000000000D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00B5B2B500B5B2B500B5B2B5008C8A
      8C00848284009C9A9C00ADAAAD008C8E8C00ADAEAD00DEDBDE00B5B2B500B5B2
      B500B5B2B500B5B2B500B5B2B500B5B2B500B5B2B500B5B2B500BDBABD006361
      630094929400B5B2B500BDBABD0063616300FFFFFF00B5B2B500B5B2B500B5B2
      B500B5B2B500B5B2B500B5B2B500B5B2B500D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC0000000000D8E9EC00D8E9EC00D8E9EC0000000000D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00B5B2B500B5B2B500B5B6B5008C8A
      8C0084828400ADAAAD00B5B6B5008C8E8C00ADAEAD00DEDBDE00B5B2B500B5B2
      B500B5B2B500B5B2B500B5B2B500B5B2B500B5B2B500B5B2B500BDBABD008C8A
      8C00B5B2B500B5B2B500BDBABD0063616300DEDFDE00B5B2B500B5B2B500B5B2
      B500B5B2B500B5B2B500B5B2B500B5B2B500D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC0000000000D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00B5B2B500B5B2B500B5B6B500A5A6
      A500A5A6A500B5B2B500B5B6B5008C8E8C009C9A9C00C6C7C600B5B2B500B5B2
      B500B5B2B500B5B2B500B5B2B500B5B2B500B5B2B500B5B2B500B5B2B500B5B2
      B500B5B2B500B5B2B500B5B2B5008C8A8C00BDBABD00B5B2B500B5B2B500B5B2
      B500B5B2B500B5B2B500B5B2B500B5B2B500D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00B5B6B500D8E9EC00D8E9EC00D8E9EC00B5B6B500D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00C6C3C600D8E9EC00D8E9EC00C6C3C600D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00C6C3C600D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00C6C3C600C6C3C600C6C3
      C600CECFCE00C6C3C600C6C3C600C6C3C600CECFCE00C6C3C600D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC0000000000000000000000000000000000000000000000000000000000D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00000000000000000000000000000000000000000000000000C6C3
      C600D8E9EC00D8E9EC00D8E9EC00D8E9EC00A5A6A500E7E3E700E7E3E700E7E3
      E700E7E3E700E7E3E700E7E3E700E7E3E700E7E3E700FFFFFF00C6C3C600D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000C6C3
      C600D8E9EC00D8E9EC00D8E9EC00D8E9EC000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000D8E9EC000000
      0000D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00C6C3C6000000
      000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF0073717300737173000000
      000000000000D8E9EC00D8E9EC00D8E9EC000000000000000000000000000000
      00000000000000000000000000000000000000000000E7E7E700FFFFFF00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00C6C3C6000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      0000D8E9EC00D8E9EC00D8E9EC00D8E9EC0000000000D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC000000
      0000D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC0000000000FFFF
      FF00FFFFFF000000000000000000000000000000000000000000000000006361
      63007371730000000000C6C3C600D8E9EC000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC0000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      0000D8E9EC00D8E9EC00D8E9EC00D8E9EC0000000000D8E9EC00000000000000
      0000D8E9EC000000000000000000000000000000000000000000D8E9EC000000
      0000D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC0000000000FFFFFF000000
      000000000000FFFFFF00B5B2B50000000000B5B2B500FFFFFF00B5B2B5000000
      0000000000007371730000000000D8E9EC000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00D8E9
      EC00D8E9EC00BDBEBD00B5B6B500D8E9EC00D8E9EC00C6C3C60000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      0000D8E9EC00D8E9EC00D8E9EC00D8E9EC0000000000D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC000000
      0000D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC0000000000000000000000
      0000FFFFFF00B5B2B500FFFFFF0000000000FFFFFF00B5B2B500FFFFFF00FFFF
      FF00000000000000000000000000C6C3C6000000000000000000000000000000
      000000000000000000000000000000000000000000006B6D6B00FFFFFF00B5B6
      B500ADAAAD00FFFFFF00CECBCE00D8E9EC00D8E9EC0000000000FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      0000D8E9EC00D8E9EC00D8E9EC00D8E9EC0000000000D8E9EC00D8E9EC000000
      0000D8E9EC000000000000000000000000000000000000000000D8E9EC000000
      0000D8E9EC00D8E9EC00D8E9EC00D8E9EC000000000063616300C6C3C600FFFF
      FF0000000000FFFFFF00B5B2B500FFFFFF00B5B2B500FFFFFF00C6C3C6000000
      0000737173007371730073717300000000000000000000000000000000000000
      0000000000000000000000000000000000000000000063616300FFFFFF00BDBE
      BD006B6D6B00EFEBEF00FFFFFF00BDBEBD0000000000FFFFFF00FFFFFF000000
      000000000000FFFFFF00000000007B7D7B00FFFFFF0000000000FFFFFF000000
      0000D8E9EC00D8E9EC00D8E9EC00D8E9EC0000000000D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC000000
      0000D8E9EC00D8E9EC00D8E9EC00D8E9EC0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00B5B2B500FFFFFF00C6C3C600FFFFFF00C6C3C600636163007371
      7300737173007371730073717300000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00B5B6
      B5006361630073717300EFEBEF00F7F7F700000000000000000000000000D8E9
      EC0000000000FFFFFF00000000007B7D7B00FFFFFF0000000000FFFFFF000000
      0000D8E9EC00D8E9EC00D8E9EC00D8E9EC0000000000D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC0000000000D8E9EC00D8E9EC000000
      0000D8E9EC00D8E9EC00D8E9EC00D8E9EC00000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00B5B2B500FFFFFF006361630073717300737173007371
      7300737173000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00BDBE
      BD0000000000737173006B696B00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC0000000000FFFFFF00000000007B7D7B00FFFFFF0000000000FFFFFF000000
      0000D8E9EC00D8E9EC00D8E9EC00D8E9EC0000000000D8E9EC00D8E9EC000000
      0000D8E9EC00D8E9EC00D8E9EC0000000000D8E9EC0000000000D8E9EC000000
      0000D8E9EC00D8E9EC00D8E9EC000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00C6C3C6000000000000000000737173007371
      7300737173007371730073717300000000000000000000000000000000000000
      000000000000000000000000000000000000000000006B6D6B00FFFFFF00B5B6
      B5000000000000000000D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC0000000000FFFFFF00000000007B7D7B00FFFFFF0000000000FFFFFF000000
      0000D8E9EC00D8E9EC00D8E9EC00D8E9EC0000000000D8E9EC0000000000D8E9
      EC0000000000D8E9EC0000000000D8E9EC0000000000D8E9EC00D8E9EC000000
      000000000000D8E9EC00000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF0000000000000000000000
      00007371730073717300737173000000000000000000000000007B7D7B007B7D
      7B007B7D7B007B7D7B007B7D7B00000000000000000063616300FFFFFF00D8E9
      EC006B696B00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC0000000000FFFFFF00000000007B7D7B00FFFFFF000000000000000000D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC0000000000D8E9EC00D8E9EC000000
      0000D8E9EC0000000000D8E9EC0000000000D8E9EC0000000000D8E9EC00D8E9
      EC00D8E9EC0000000000D8E9EC0000000000D8E9EC0000000000FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00000000000000
      0000000000000000000000000000D8E9EC000000000000000000F7F3F700F7F3
      F700F7F3F700F7F3F700F7F3F70000000000000000006B6D6B00FFFBFF00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC0000000000FFFFFF00000000007B7D7B00FFFFFF0000000000D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC000000000000000000D8E9EC000000
      000000000000D8E9EC0000000000D8E9EC0000000000D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC000000000000000000D8E9EC0000000000FFFFFF000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF000000
      0000000000000000000000000000D8E9EC006B6D6B006B6D6B00737173007371
      73007371730073717300737173006B6D6B000000000063656300D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC0000000000000000007B7D7B0000000000D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC0000000000D8E9EC0000000000D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC000000000000000000D8E9EC00D8E9EC0000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFF
      FF00000000000000000000000000D8E9EC000000000000000000737573000000
      00007371730000000000000000006365630073757300D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00000000005A595A00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC0000000000D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC0000000000D8E9EC0000000000D8E9EC00D8E9EC00D8E9EC000000
      000000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF000000
      0000FFFFFF000000000000000000D8E9EC00D8E9EC00D8E9EC00D8E9EC000000
      00007371730063656300D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00000000000000000000000000000000000000
      000000000000D8E9EC000000000000000000D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00000000000000000000000000000000000000000000000000D8E9
      EC0000000000FFFFFF0000000000D8E9EC00D8E9EC0000000000D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC0000000000D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC0000000000D8E9EC00D8E9
      EC0000000000D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC000000
      0000D8E9EC00D8E9EC0000000000D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC0000000000D8E9
      EC0000000000D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC000000
      0000D8E9EC0000000000D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC0000000000000000000000
      0000000000000000000000000000000000000000000000000000D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC000000
      000000000000D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC000000
      000000000000D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC0000000000000000000000
      000000000000D8E9EC0000000000000000000000000000000000D8E9EC000000
      0000000000000000000000000000D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00BDBA
      BD00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00B5B6B500BDBEBD00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC0000000000D8E9EC00D8E9EC00D8E9EC00D8E9EC0000000000D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00C6C3
      C600D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC003130
      3100C6C3C600D8E9EC00D8E9EC00D8E9EC00D8E9EC009C9A9C00080C08006B69
      6B00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00B5B2B500C6C3C600D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC0000000000D8E9EC00D8E9EC000000000000000000D8E9EC00D8E9EC000000
      0000D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00000000000000
      0000000000000000000000000000D8E9EC00D8E9EC00D8E9EC00D8E9EC006361
      630063616300D8E9EC00D8E9EC00D8E9EC00B5B2B5005251520010141000D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00A5A6A5000000000000000000000000000000
      000000000000D8E9EC00D8E9EC00D8E9EC000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000D8E9
      EC00D8E9EC00D8E9EC00D8E9EC0000000000D8E9EC00D8E9EC00D8E9EC00D8E9
      EC0000000000D8E9EC0000000000D8E9EC00D8E9EC0000000000D8E9EC000000
      0000D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00000000000000
      00000000000000000000D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC0000000000D8E9EC00D8E9EC00D8E9EC009C9A9C0010141000D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00ADAAAD0000000000000000000000
      000000000000D8E9EC00D8E9EC00D8E9EC000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000D8E9
      EC00D8E9EC00D8E9EC000000000000000000D8E9EC00D8E9EC00D8E9EC00D8E9
      EC0000000000D8E9EC0000000000D8E9EC00D8E9EC0000000000D8E9EC000000
      0000D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00000000000000
      000000000000D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC0000000000D8E9EC00D8E9EC00D8E9EC009C9A9C0010141000D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00ADAAAD00000000000000
      000000000000D8E9EC00D8E9EC00D8E9EC000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000D8E9
      EC00D8E9EC00000000000000000000000000D8E9EC00D8E9EC00D8E9EC00D8E9
      EC0000000000D8E9EC00D8E9EC000000000000000000D8E9EC00D8E9EC000000
      0000D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00000000000000
      0000D8E9EC0000000000D8E9EC00C6C3C600D8E9EC00D8E9EC00D8E9EC00D8E9
      EC0000000000D8E9EC00D8E9EC00D8E9EC009C9A9C0018181800BDBEBD00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00ADAAAD0000040000ADAEAD000000
      000000000000D8E9EC00D8E9EC00D8E9EC000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000D8E9
      EC00D8E9EC00000000000000000000000000D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC0000000000D8E9EC00D8E9EC00D8E9EC00D8E9EC0000000000D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC0000000000D8E9
      EC00D8E9EC00D8E9EC000000000000000000D8E9EC00D8E9EC00D8E9EC006361
      630063616300D8E9EC00D8E9EC00D8E9EC00B5B2B5005251520010141000D8E9
      EC00B5B2B500D8E9EC00A5A6A5000000000008080800D8E9EC00D8E9EC00ADAE
      AD0000000000D8E9EC00D8E9EC00D8E9EC000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000D8E9
      EC00D8E9EC00D8E9EC000000000000000000D8E9EC0000000000000000000000
      000000000000D8E9EC0000000000000000000000000000000000D8E9EC000000
      0000000000000000000000000000D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC000000000000000000000000003130
      3100C6C3C600D8E9EC00D8E9EC00D8E9EC00D8E9EC00A5A6A500525552000000
      00000000000000000000080C0800D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000D8E9
      EC00D8E9EC00D8E9EC00D8E9EC0000000000D8E9EC00D8E9EC00D8E9EC000000
      000000000000D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC000000
      000000000000D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00000000000000000000000000D8E9
      EC00D8E9EC0000000000D8E9EC00D8E9EC00000000000000000000000000D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC0000000000D8E9
      EC0000000000D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC000000
      0000D8E9EC0000000000D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC000000000000000000000000000000
      0000D8E9EC0000000000D8E9EC0000000000000000000000000000000000D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC0000000000D8E9EC00D8E9
      EC0000000000D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC000000
      0000D8E9EC00D8E9EC0000000000D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC0000000000000000000000
      000000000000D8E9EC0000000000000000000000000000000000D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC0000000000D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC0000000000D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00000000000000000000000000D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC0084828400D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC0084828400D8E9EC0084828400D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC0084828400D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC000000000084828400D8E9EC00D8E9
      EC0084828400D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC0084828400D8E9
      EC00D8E9EC008482840000000000D8E9EC00848284008482840084828400D8E9
      EC0084828400D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC0084828400D8E9
      EC00D8E9EC008482840000000000D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00C6C3C60000000000D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00000000000000000000000000000000000000000000000000D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC000000000084828400D8E9
      EC0000000000D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC0000000000D8E9
      EC008482840000000000D8E9EC00D8E9EC00D8E9EC008482840084828400D8E9
      EC0000000000D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC0000000000D8E9
      EC008482840000000000D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00000000000000000000000000C6C3C600D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC000000000000000000D8E9EC00D8E9EC00D8E9EC00D8E9EC00000000000000
      0000D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00000000008482
      840000000000D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00000000008482
      840000000000D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00000000008482
      840000000000D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00000000008482
      840000000000D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC000000000000000000000000000000000000000000D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC000000
      0000D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC0000000000D8E9EC00D8E9EC00D8E9EC00D8E9EC0084828400848284000000
      000000000000D8E9EC00848284008482840084828400D8E9EC00000000000000
      0000D8E9EC0084828400D8E9EC00D8E9EC00D8E9EC0084828400D8E9EC000000
      000000000000D8E9EC00D8E9EC0084828400D8E9EC00D8E9EC00000000000000
      0000D8E9EC0084828400D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC0000000000D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC0000000000D8E9
      EC00D8E9EC00D8E9EC0000000000000000000000000000000000D8E9EC00D8E9
      EC00D8E9EC0000000000D8E9EC00D8E9EC00D8E9EC0000000000848284000000
      000000000000D8E9EC00000000000000000000000000D8E9EC00000000000000
      00000000000084828400D8E9EC00D8E9EC00D8E9EC0000000000000000000000
      000084828400D8E9EC00D8E9EC0000000000D8E9EC00D8E9EC00000000008482
      8400000000008482840084828400D8E9EC00D8E9EC00D8E9EC00C6C3C6000000
      0000D8E9EC00D8E9EC00D8E9EC00C6C3C60000000000D8E9EC00D8E9EC00D8E9
      EC00C6C3C60000000000D8E9EC00D8E9EC00D8E9EC000000000000000000D8E9
      EC00D8E9EC0000000000D8E9EC00D8E9EC00D8E9EC00D8E9EC0000000000D8E9
      EC00D8E9EC000000000000000000D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC008482840000000000D8E9EC0084828400D8E9EC0000000000D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC0000000000D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC0084828400D8E9EC00D8E9EC00D8E9EC00D8E9EC00000000000000
      0000C6C3C600D8E9EC00D8E9EC00D8E9EC0000000000C6C3C600D8E9EC00D8E9
      EC00D8E9EC000000000000000000C6C3C600D8E9EC00D8E9EC00D8E9EC00D8E9
      EC0000000000D8E9EC00D8E9EC000000000000000000D8E9EC00D8E9EC000000
      0000D8E9EC00D8E9EC0000000000D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC0000000000D8E9EC008482840000000000D8E9EC00D8E9EC0000000000D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00848284008482840084828400000000008482840084828400D8E9EC008482
      8400D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000D8E9EC0000000000D8E9EC00D8E9
      EC0000000000D8E9EC0000000000D8E9EC00D8E9EC0000000000D8E9EC000000
      0000D8E9EC00D8E9EC0000000000D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC0000000000D8E9EC00000000000000000000000000D8E9EC0000000000D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC0000000000000000000000000000000000000000000000000000000000D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00000000000000
      0000D8E9EC00D8E9EC00D8E9EC00D8E9EC0000000000D8E9EC00D8E9EC00D8E9
      EC00D8E9EC000000000000000000D8E9EC00D8E9EC0000000000D8E9EC00D8E9
      EC0000000000D8E9EC0000000000D8E9EC00D8E9EC0000000000D8E9EC000000
      0000D8E9EC00D8E9EC0000000000D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC000000000084828400D8E9EC0000000000D8E9EC00D8E9EC0000000000D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC0000000000D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC000000
      0000D8E9EC00D8E9EC00D8E9EC00C6C3C60000000000D8E9EC00D8E9EC00D8E9
      EC00D8E9EC0000000000D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC0000000000D8E9EC00D8E9EC000000000000000000D8E9EC00D8E9EC000000
      0000D8E9EC00D8E9EC0000000000D8E9EC00D8E9EC00D8E9EC00D8E9EC008482
      8400D8E9EC000000000084828400D8E9EC00848284000000000084828400D8E9
      EC00D8E9EC0084828400D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC0084828400D8E9EC00D8E9EC0000000000D8E9EC00D8E9EC00D8E9EC008482
      8400D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC0000000000C6C3C600D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC0000000000D8E9EC00D8E9
      EC00D8E9EC0000000000D8E9EC00D8E9EC00D8E9EC00D8E9EC0000000000D8E9
      EC00D8E9EC000000000000000000D8E9EC00D8E9EC0000000000000000000000
      000000000000D8E9EC00000000000000000000000000D8E9EC00000000000000
      00000000000000000000D8E9EC00D8E9EC00D8E9EC0000000000000000000000
      000000000000D8E9EC00D8E9EC0000000000D8E9EC00D8E9EC00000000000000
      00000000000000000000D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC000000000000000000000000000000000000000000D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC0000000000D8E9
      EC00D8E9EC00D8E9EC0000000000000000000000000000000000D8E9EC00D8E9
      EC00D8E9EC0000000000D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC000000
      000000000000D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00000000000000
      0000D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC000000
      000000000000D8E9EC00D8E9EC008482840084828400D8E9EC00000000000000
      0000D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00000000000000000000000000D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC000000
      0000D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC0000000000D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC0000000000D8E9
      EC0000000000D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC0000000000D8E9
      EC0000000000D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC0000000000D8E9
      EC0000000000D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC0000000000D8E9
      EC0000000000D8E9EC0084828400D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC0000000000D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC000000000000000000D8E9EC00D8E9EC00D8E9EC00D8E9EC00000000000000
      0000D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC0000000000D8E9EC00D8E9
      EC0000000000D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC0000000000D8E9
      EC00D8E9EC0000000000D8E9EC0084828400D8E9EC0000000000D8E9EC00D8E9
      EC0000000000D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC0000000000D8E9
      EC00D8E9EC0000000000D8E9EC0084828400D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00000000000000000000000000000000000000000000000000D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC0000000000D8E9EC00FFFFFF00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC0000000000D8E9EC0000000000D8E9EC00D8E9EC008482
      8400D8E9EC00D8E9EC0084828400D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC0000000000D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D6C7C600D6C3C600D6BABD00DEBE
      C600DEBABD00E7BAC600D6AEB500E7C3C600FFD7DE00D6B2BD00E7C3C600D6B2
      BD00D6BABD00D6B6BD00D6B6BD00CEBABD00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00CEB6BD0029080800391418002100
      0000310810004214180029000000390C10002100000031080800391418002900
      080039101800210000004224290021080800D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00B5B6B5007B7D7B00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC000000000000000000D8E9EC00D8E9EC00DEC3C60021000000FFFBFF00FFE7
      EF00FFF7FF00FFD7E700FFF7FF00FFD7F70039143900FFDFFF00FFF7FF00F7D3
      E700FFFBFF00FFF3FF00FFDFE700310C1000D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC008C8E8C000000000084828400B5B6B500D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00000000000000000000000000D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC000000
      0000D8E9EC000000000000000000D8E9EC00CEBEC600310C0800FFEBEF00FFF3
      FF00EFE7FF00FFF3FF00F7E3FF00210C5A00180C6300100C4A00EFE7FF00F7E7
      FF00F7E7FF00FFFBFF00FFE7F70031041000D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00B5B6B5008486840000000000000000000000000094929400D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00000000000000000000000000D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC0000000000D8E9
      EC00000000000000000000000000D8E9EC00C6BEBD0021000000FFFBFF00FFF3
      FF00F7F7FF00E7E3FF0018184A0000005A00000463000810520000043900F7F7
      FF00F7F7FF00E7E7F700FFFBFF0021000800D8E9EC00D8E9EC00D8E9EC00D8E9
      EC0094929400000000000000000000000000000000000000000084868400B5B6
      B500D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00000000000000000000000000D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC000000000000000000000000000000000000000000D8E9EC000000
      00000000000000000000D8E9EC00D8E9EC00BDBAB50039181000FFEBEF00DED7
      EF00E7EBFF00F7F7FF00D6DFFF00E7F3FF0008205200E7F7FF00D6EBFF00EFEF
      FF00DEDBF700F7F3FF00EFDFF70039041800D8E9EC00D8E9EC00D8E9EC008C8A
      8C00000000000000000000000000000000000000000000000000000000009496
      9400D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00000000000000000000000000D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC000000
      000000000000D8E9EC00D8E9EC00D8E9EC00D8E9EC0000000000000000000000
      000000000000D8E9EC00D8E9EC00D8E9EC00C6C3C600421C1800FFF7FF00DECF
      F70018245200DEE3FF00EFF7FF00BDCFF70008244200BDDFF700D6F3FF00D6E3
      FF0021244200EFEBFF00FFEBFF0039041800D8E9EC00D8E9EC00D8E9EC002124
      2100212421002124210000000000000000000000000021242100212421002124
      2100D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00000000000000000000000000D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00000000000000
      0000D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00000000000000
      000000000000D8E9EC00D8E9EC00D8E9EC00BDBACE0018000800F7CFF7002918
      5A0000045200EFEFFF00D6E3FF00DEF7FF00B5D7F700D6FFFF00BDDBF700E7EF
      FF00100C4A00181C3900FFEBFF0039041800D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00000000000000000000000000D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00000000000000000000000000D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC0000000000D8E9
      EC00D8E9EC00D8E9EC000000000000000000D8E9EC00D8E9EC00D8E9EC000000
      0000D8E9EC00D8E9EC00D8E9EC00D8E9EC00CEC3D60031082100421442002110
      5A00181463001008420018204A00ADBEE700DEFBFF00C6E3F700102C4A001010
      520018145200101031003928390021000800D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00000000000000000000000000D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00000000000000000000000000D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC0000000000D8E9EC00D8E9
      EC00D8E9EC00D8E9EC000000000000000000D8E9EC00D8E9EC00D8E9EC00D8E9
      EC0000000000D8E9EC00D8E9EC00D8E9EC00D6BECE0031000800FFD7F7003918
      5200210C4A00FFF3FF00EFE7FF00F7F7FF00E7EFFF00EFFBFF00CED7EF00EFDF
      FF00180C390018143100FFFBFF0039041800D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00000000000000000000000000D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC000000000000000000000000000000000000000000000000000000
      00000000000000000000D8E9EC00D8E9EC00D8E9EC0000000000D8E9EC00D8E9
      EC00000000000000000000000000000000000000000000000000D8E9EC00D8E9
      EC0000000000D8E9EC00D8E9EC00D8E9EC00D6BEC60042080800FFF7FF00F7D3
      FF0029184200EFDBFF00FFF3FF00E7DBFF00080C3100DEE7FF00F7FBFF00DECB
      F70031244A00EFE7FF00F7E7F70039081800D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00000000000000000000000000D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC0000000000000000000000000000000000000000000000
      000000000000D8E9EC00D8E9EC00D8E9EC00D8E9EC0000000000D8E9EC00D8E9
      EC00000000000000000000000000000000000000000000000000D8E9EC00D8E9
      EC0000000000D8E9EC00D8E9EC00D8E9EC00BDB6B50039100800FFE7E700FFFB
      FF00E7DFF700FFFBFF00DED7F700F7EFFF0008104A00EFF3FF00CEDBFF00F7F7
      FF00EFEBFF00FFFBFF00EFDFEF0031041000D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00000000000000000000000000D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00000000000000000000000000000000000000
      0000D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC0000000000D8E9EC00D8E9
      EC00D8E9EC00D8E9EC000000000000000000D8E9EC00D8E9EC00D8E9EC00D8E9
      EC0000000000D8E9EC00D8E9EC00D8E9EC00BDC3BD0029100800FFFFF700F7EF
      F700F7FFFF00EFEBFF00211C420000044A0010186300000C420008143900F7FB
      FF00DEE3EF00DEE3EF00FFFBFF0031041000D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00000000000000000000000000D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00000000000000000000000000D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC0000000000D8E9
      EC00D8E9EC00D8E9EC000000000000000000D8E9EC00D8E9EC00D8E9EC000000
      0000D8E9EC00D8E9EC00D8E9EC00D8E9EC00C6CBD60018040000FFE7E700EFEF
      F700DEE7F700FFFBFF00D6D3FF0018186B000804630010185A00CED7FF00E7EB
      FF00F7F7FF00FFFFFF00FFF7FF0021000000D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00080C0800080C0800080C0800D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC0000000000D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC0000000000D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC000000
      0000D8E9EC00D8E9EC00D8E9EC00D8E9EC00B5AEB50039181000FFFFF700FFF3
      F700FFFFFF00EFE3EF00FFF3FF00EFDBFF0018106300D6CBFF00FFF7FF00F7E7
      F700FFF7F700E7EBE700FFF3F700390C1000D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC0000000000D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC000000
      000000000000D8E9EC00D8E9EC00D8E9EC00D8E9EC000000000000000000D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00DEC7C60042080000390000003910
      080018000000290000002904080021002100180018001800100021080800310C
      0800311410000808000029201800390C1000D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00D8E9
      EC00D8E9EC0000000000000000000000000000000000D8E9EC00D8E9EC00D8E9
      EC00D8E9EC00D8E9EC00D8E9EC00D8E9EC00424D3E000000000000003E000000
      2800000040000000A00000000100010000000000000500000000000000000000
      000000000000000000000000FFFFFF0000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFFFFFF0F0000FFE19FFFFC07
      0000FF81C7FFFC410000FFF1E4FFF1C10000FBC9F0FFF1900000E10BF0F1C618
      0000E03FE098CF3C0000003FFC001F1E0000003FE0F89F0F0000FC1FF0F9FE0F
      0000E309F0FFFE070000E3C1E4FFF8070000FFC1CFFFFC0F0000FFE19FFFFE1F
      0000FFC3FFFFFFFF0000FFFFFFFFFFFFFFFFFFFFF01F0000FC1FDFFFC00F0002
      E00F1FCF80030206E003878F0003070EC463C35F00010B1ECE71E0BF00001112
      9E71E17F0001E3468E71F87F000121C2BE7DB47300010306FE7F021180000103
      FE7F0701C0000004FE7F6781C8010000FE7FC703F003F000FCBFC703F01F5000
      FDBFFC07FC3F0000FE7FFE1FFFFF0000FFFFD777FFFFD7FFFAFFC000FFFFC1FF
      B0579FFC9FF7C3FF0007BFFE8FFFC3FF0007BFFE87F7C1FF0007BFFEC3EFF0FF
      0007BFFCE38FF8FF04078EECF09FFC3F00078001F83FFE3F0007FF7FFC3FFF0F
      0007FF7FF03FFF8B904FFE3FF18FFFC0F87FF80F83CFFFE1FFFFFC1F87E7FFE0
      FFFFFE3F8FF7FFE1FFFFFF7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE7
      FFFFFFFFFFFF0200F7FFFFFFF7F70601E7FFFFFFE1C38103E0FFEF7FE9D3C007
      E07FE71FE183C007E00FE11FE003E20FE007E007E823E00FE01FE10FE003F00F
      E07FE31FF007E01FE1FFE77FF9CFF83FE7FFFF7FF9CFF83FFFFFFFFFFFFFF87F
      FFFFFFFFFFFFF8FFFFFFFFFFFFFFFFFFFFFF00003C00FFFFC1FF00000000FFFF
      FEFF00000000FFFF7F7F00000000FFFF7F7F00000000E31F7F7B00000000E31F
      7F7B00000000E31F7F6100000000E31FBEE000000000E31FC1FB00000000E31F
      F77B00000000E31FF67F00000000E31FF57F00000000E31FF37F00000000FFFF
      F77F00000000FFFFFF7F00000000FFFFF77FFDBFFFFFFF7F803FF01FFFFFF80F
      001FF00F002FC007001FC00F7FEFC001001FC00F482F80010019800F7FEF8000
      0001800F682F00000000000F7FEF00000000100F7F6F00000001F00F6EAE0000
      0003F00F556400000017F01F6ABA8001001FF03F257C8001003FF87FFAFCC001
      007FFCFFFDFAE001E3FFFFFFFE04F811BFFDFFFFFFFFFFFFB7EDFFFFFFFFFFFF
      D7EBFFFFFFFF803FE7E7FFFFFFFF001F8421FFEF9FFF001FFBDFEFE78F9F001F
      F66FC1E71E07001EF5AFC3F73F07001CF5AFC7F73F870018F66FCAF71F070018
      FBDFDCE71467001C8421FF0781FF001EE7E7FFFFFFFF1B1FD7EBFFFFFFFF0A1F
      B7EDFFFFFFFF843F7FFEFFFFFFFFF1FF7FFD7FFDFFFFFFFF37D917D9FE7FF81F
      97D397D3FE1FF3CFC7C7C7C7FC1FEFF7844BA6CBFF7FDC3B844386C1CE739BD9
      F2BFFEFBC738F66DF4DFF02F8000B5ADF45FF01FCF79B5ADF2DFFEFFEE7BF66D
      E91BF6EFFF3FBBD9844386C3FC1FDC3BE7CFE64FFE3FEFF7D7D7D7D5FF7FF3CF
      B7DAB7DAFFFFF81F5FFD6DFDFFFFFFFF0000FFFFFFFFFFFF0000FCFFFFFFFFF3
      0000FC3FFF1FFFE90000F03FFF1FFFD10000F00FFF1FF8230000E00FFF1FE787
      0000E00FFF1FCFC70000FC7FFF1FDCEF0000FC7FFF1FBCF70000FC7FF803B037
      0000FC7FFC07B0370000FC7FFE0FBCF70000FC7FFF1FDCEF0000FC7FFFBFDFEF
      0000FFFFFFBFE79F0000FFFFFFFFF87F00000000000000000000000000000000
      000000000000}
  end
  object ImageList2: TImageList
    Left = 848
    Top = 128
    Bitmap = {
      494C010109008801900110001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000003000000001002000000000000030
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000C6C3
      C60000000000C6C3C60000000000C6C3C60000000000C6C3C600C6C3C6000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000C6C3C600000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000C6C3C60000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000073717300000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000073717300000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000073717300636163007371
      7300737173000000000000000000000000000000000073717300737173007371
      7300636163000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000636163006361630063616300000000007371730073717300737173000000
      0000000000000000000063616300C6C3C6000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000C6C3C600000000006361630000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000063616300C6C3C60000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000063616300C6C3C60000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000C6C3C600636163000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000C6C3C6000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000029282900393C
      3900C6C3C6000000000000000000000000000000000000000000000000000000
      000000000000393C3900393C3900C6C3C6000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000C6C3C600393C3900393C
      3900393C3900000000000000000000000000000000000000000000000000C6C3
      C600393C3900393C390029282900000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000002928
      2900393C3900393C3900C6C3C6000000000000000000C6C3C60000000000393C
      3900393C3900393C390000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000393C3900393C390000000000000000000000000000000000393C3900393C
      3900292829000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000C6C3
      C6000000000029282900393C3900C6C3C60000000000393C3900393C3900393C
      390000000000C6C3C60000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000B5B2B500393C3900393C390000000000393C390029282900B5B2
      B500000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000C6C3
      C60000000000C6C3C60000000000393C3900393C3900393C390000000000C6C3
      C60000000000C6C3C60000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000393C3900393C3900393C3900000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000C6C3
      C60000000000C6C3C600393C3900393C390000000000393C3900393C3900C6C3
      C60000000000C6C3C60000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000393C390029282900B5B2B5000000000029282900393C3900393C
      3900000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000393C3900393C390000000000C6C3C60000000000C6C3C60029282900393C
      3900393C3900C6C3C60000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000C6C3C600393C
      3900393C3900292829000000000000000000000000000000000000000000393C
      3900393C3900393C390000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000029282900393C
      3900393C39000000000000000000000000000000000000000000000000000000
      000029282900393C3900393C3900C6C3C6000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000393C39002928
      2900000000000000000000000000000000000000000000000000000000000000
      000000000000393C390029282900000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000C6C3C6000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000FFFF0000FFFF000000FF000000FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFF0000FFFF
      0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF
      0000FFFF0000FFFF0000FFFF0000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000C6C3C6000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000FFFF0000FFFF0000FFFF000000FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFF0000FFFF
      0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF
      0000FFFF0000FFFF0000FFFF0000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000C6C3
      C600000000000000000000000000000000000000000000000000000000000000
      000000FFFF0000FFFF0000FFFF000000FF000000FF0000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFF0000FFFF
      0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF
      0000FFFF0000FFFF0000FFFF0000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000C6C3
      C600000000000000000000000000B5B2B50000000000B5B2B500000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000FF0000FFFF0000FFFF0000FFFF000000FF0000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFF0000FFFF
      0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF
      0000FFFF0000FFFF0000FFFF0000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000B5B2B50000000000B5B2B50000000000B5B2B50000000000B5B2
      B500000000000000000000000000000000000000000000000000000000000000
      0000000000000000FF000000FF000000FF000000FF000000FF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000C6C3
      C60000000000C6C3C60000000000C6C3C60000000000C6C3C60000000000C6C3
      C60000000000C6C3C60000000000000000000000000000000000000000000000
      000000000000000000000000FF000000FF0000FFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000C6C3
      C60000000000B5B2B50000000000B5B2B50000000000B5B2B50000000000B5B2
      B50000000000C6C3C60000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000C6C3C60000000000B5B2B50000000000B5B2B50000000000C6C3
      C600000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000C6C3C60000000000C6C3C600000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000300000000100010000000000800100000000000000000000
      000000000000000000000000FFFFFF00FFFF000000000000FFFF000000000000
      FFFF000000000000EA9F00000000000080030000000000008001000000000000
      80010000000000008001000000000000F01C000000000000FC7F000000000000
      FE7F000000000000FE7F000000000000FCFF000000000000FFFF000000000000
      FFFF000000000000FFFF000000000000FFFFFFFFFFFFF3FFFFFFFF07DFFFF3FF
      0001FE23C638F9FF3FF9F8318761F9FF0001F038E123F8FFFFFFC318F0079CFF
      FFF9819CE003867FE731999CC001C27FCE31999CC001F36F8C21819CC001FE07
      0001C318C001FF0F8C21F038C001FF07CE71F831F003FE07E739FE23C003FFE3
      FFFFFF07C7F0FFF9FFFFFFFFCFF9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFF
      FFFFFFBFFE3FFFFFFFFFFC0FFE3FFFFFFFFFFBB5FF7FF0FFC001D7BBFE7FF0FF
      C001E7B5FC0FF07FC001DA0EE007F07FC001DC1EC001F83FF00FDE0EC001FC2F
      E007DC16C001FF8FFC33EBB9C001FF8FFF7FF7BAC001FFE7FFFFEBB7F007FFFF
      FFFFFC0FFC1FFFFFFFFFFFBFFFFFFFFF00000000000000000000000000000000
      000000000000}
  end
  object pmPopupMenu1: TPopupMenu
    Images = imglistPM
    Left = 456
    Top = 120
    object mniStandardPoint1: TMenuItem
      Caption = 'New Standard (&Point)'
      object mniAir1: TMenuItem
        Caption = '&Air'
        OnClick = mniAir1Click
      end
      object mniSurface1: TMenuItem
        Caption = '&Surface'
        OnClick = mniSurface1Click
      end
      object mniSubsurface1: TMenuItem
        Caption = 'S&ubsurface'
        OnClick = mniSubsurface1Click
      end
      object mniLand1: TMenuItem
        Caption = '&Land'
        OnClick = mniLand1Click
      end
    end
    object mniNewStandardBearing1: TMenuItem
      Caption = 'New Standard (&Bearing)'
      object mniAir2: TMenuItem
        Caption = '&Air'
        OnClick = mniAir2Click
      end
      object mniSurface2: TMenuItem
        Caption = '&Surface'
        OnClick = mniSurface2Click
      end
      object mniSubsurface2: TMenuItem
        Caption = 'S&ubsurface'
        OnClick = mniSubsurface2Click
      end
      object mniLand2: TMenuItem
        Caption = '&Land'
        OnClick = mniLand2Click
      end
      object mniGeneral1: TMenuItem
        Caption = '&General'
        OnClick = mniGeneral1Click
      end
    end
    object mniNewStandard1: TMenuItem
      Caption = 'New Standard (A&OP)'
      object mniAir3: TMenuItem
        Caption = '&Air'
        OnClick = mniAir3Click
      end
      object mniSurface3: TMenuItem
        Caption = '&Surface'
        OnClick = mniSurface3Click
      end
      object mniSubsurface3: TMenuItem
        Caption = 'S&ubsurface'
        OnClick = mniSubsurface3Click
      end
      object mniLand3: TMenuItem
        Caption = '&Land'
        OnClick = mniLand3Click
      end
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object mniNewSpecialPointGeneral1: TMenuItem
      Caption = 'New Special Point (&General)'
      object mniGeneral2: TMenuItem
        Caption = '&General'
        OnClick = mniGeneral2Click
      end
      object mniBouy1: TMenuItem
        Caption = '&Bouy'
        OnClick = mniBouy1Click
      end
      object mniDatalinkReferencePoint1: TMenuItem
        Caption = '&Datalink Reference Point'
        OnClick = mniDatalinkReferencePoint1Click
      end
      object mniDesse1: TMenuItem
        Caption = 'Distressed &Vessel'
        OnClick = mniDesse1Click
      end
      object mniESM1: TMenuItem
        Caption = '&ESM/ECM Fix'
        OnClick = mniESM1Click
      end
      object mniGroundZero1: TMenuItem
        Caption = 'Ground &Zero'
        OnClick = mniGroundZero1Click
      end
      object mniManInWater1: TMenuItem
        Caption = 'Man In &Water'
        OnClick = mniManInWater1Click
      end
      object mniMaritimeHeadquarter1: TMenuItem
        Caption = 'Maritime Head&quarters'
        OnClick = mniMaritimeHeadquarter1Click
      end
      object mniMineHazard1: TMenuItem
        Caption = '&Mine Hazard'
        OnClick = mniMineHazard1Click
      end
      object mniNavigationalHazard1: TMenuItem
        Caption = '&Navigational Hazard'
        OnClick = mniNavigationalHazard1Click
      end
      object mniOilRig1: TMenuItem
        Caption = '&Oil Rig'
        OnClick = mniOilRig1Click
      end
      object mniStation1: TMenuItem
        Caption = '&Station'
        OnClick = mniStation1Click
      end
      object mniTracticalGridOriginal1: TMenuItem
        Caption = '&Tactical Grid Origin'
        OnClick = mniTracticalGridOriginal1Click
      end
    end
    object mniNewSpecialPointAir1: TMenuItem
      Caption = 'New Special Point (&Air)'
      object mniAirGeneral1: TMenuItem
        Caption = '&Air General'
        OnClick = mniAirGeneral1Click
      end
      object mniAirborneEarlyWarning1: TMenuItem
        Caption = 'Airborne Early &Warning'
        OnClick = mniAirborneEarlyWarning1Click
      end
      object mniBullseye1: TMenuItem
        Caption = '&Bullseye'
        OnClick = mniBullseye1Click
      end
      object mniCombatAirPatrol1: TMenuItem
        Caption = '&Combat Air Patrol'
        OnClick = mniCombatAirPatrol1Click
      end
      object mniDitchedAircraft1: TMenuItem
        Caption = '&Ditched Aircraft'
        OnClick = mniDitchedAircraft1Click
      end
      object mniGate1: TMenuItem
        Caption = '&Gate'
        OnClick = mniGate1Click
      end
    end
    object mniNewSpecialPointASW1: TMenuItem
      Caption = 'New Special Point (A&SW)'
      object mniASWGeneral1: TMenuItem
        Caption = '&ASW General'
        OnClick = mniASWGeneral1Click
      end
      object mniBriefContact1: TMenuItem
        Caption = '&Brief Contact'
        OnClick = mniBriefContact1Click
      end
      object mniDatum1: TMenuItem
        Caption = '&Datum'
        OnClick = mniDatum1Click
      end
      object mniKingpin1: TMenuItem
        Caption = '&Kingpin'
        OnClick = mniKingpin1Click
      end
      object mniRiser1: TMenuItem
        Caption = '&Riser'
        OnClick = mniRiser1Click
      end
      object mniSearchCentre1: TMenuItem
        Caption = 'Search &Centre'
        OnClick = mniSearchCentre1Click
      end
      object mniSinker1: TMenuItem
        Caption = '&Sinker'
        OnClick = mniSinker1Click
      end
      object mniWeaponEntryPoint1: TMenuItem
        Caption = 'Weapon Entry &Point'
        OnClick = mniWeaponEntryPoint1Click
      end
      object mniWreck1: TMenuItem
        Caption = '&Wreck'
        OnClick = mniWreck1Click
      end
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object mniTorpedo: TMenuItem
      Caption = 'Torpedo'
      object mniTorpedoCourse: TMenuItem
        Caption = 'Course...'
        OnClick = mniTorpedoCourseClick
      end
      object mniTorpedoDepth: TMenuItem
        Caption = 'Depth...'
        OnClick = mniTorpedoDepthClick
      end
    end
    object mniSonobuoys: TMenuItem
      Caption = 'Sonobuoys'
      object mniOperatingMode: TMenuItem
        Caption = 'Operating Mode'
        object Active1: TMenuItem
          Caption = 'Active'
        end
        object Passive1: TMenuItem
          Caption = 'Passive'
        end
        object Off1: TMenuItem
          Caption = 'Off'
        end
      end
      object mniDepth: TMenuItem
        Caption = 'Depth...'
        OnClick = mniDepthClick
      end
      object mniEndurance: TMenuItem
        Caption = 'Endurance...'
        OnClick = mniEnduranceClick
      end
      object N12: TMenuItem
        Caption = '-'
      end
      object mniMonitor: TMenuItem
        Caption = 'Monitor'
        object Add2: TMenuItem
          Caption = 'Add'
        end
        object Remove3: TMenuItem
          Caption = 'Remove'
        end
        object ransfer1: TMenuItem
          Caption = 'Transfer...'
          OnClick = ransfer1Click
        end
      end
      object N13: TMenuItem
        Caption = '-'
      end
      object mniDestroy: TMenuItem
        Caption = 'Destroy'
      end
    end
    object Platform1: TMenuItem
      Caption = 'Platform'
    end
    object N11: TMenuItem
      Caption = '-'
    end
    object mniConvertRTtoNRT1: TMenuItem
      Caption = 'Con&vert RT to NRT'
    end
    object N3: TMenuItem
      Caption = '-'
    end
    object mniCharacteristic1: TMenuItem
      Caption = '&Characteristics'
      object mniDomain1: TMenuItem
        Caption = '&Domain'
        object mniAir4: TMenuItem
          AutoCheck = True
          Caption = '&Air'
          RadioItem = True
          OnClick = mniAir4Click
        end
        object mniSurface4: TMenuItem
          AutoCheck = True
          Caption = '&Surface'
          RadioItem = True
          OnClick = mniSurface4Click
        end
        object mniSubsurface4: TMenuItem
          AutoCheck = True
          Caption = 'S&ubsurface'
          RadioItem = True
          OnClick = mniSubsurface4Click
        end
        object mniLand4: TMenuItem
          AutoCheck = True
          Caption = '&Land'
          RadioItem = True
          OnClick = mniLand4Click
        end
        object mniGeneral4: TMenuItem
          AutoCheck = True
          Caption = '&General'
          RadioItem = True
          OnClick = mniGeneral4Click
        end
      end
      object mniIdentity1: TMenuItem
        Caption = '&Identity'
        object mniPending1: TMenuItem
          AutoCheck = True
          Caption = '&Pending'
          RadioItem = True
          OnClick = mniPending1Click
        end
        object mniUnknowns1: TMenuItem
          AutoCheck = True
          Caption = '&Unknown'
          RadioItem = True
          OnClick = mniUnknowns1Click
        end
        object mniAssumedFriend1: TMenuItem
          AutoCheck = True
          Caption = '&Assumed Friend'
          RadioItem = True
          OnClick = mniAssumedFriend1Click
        end
        object mniFriend1: TMenuItem
          AutoCheck = True
          Caption = '&Friend'
          RadioItem = True
          OnClick = mniFriend1Click
        end
        object mniNeutral1: TMenuItem
          AutoCheck = True
          Caption = '&Neutral'
          RadioItem = True
          OnClick = mniNeutral1Click
        end
        object mniSuspect1: TMenuItem
          AutoCheck = True
          Caption = '&Suspect'
          RadioItem = True
          OnClick = mniSuspect1Click
        end
        object mniHostile1: TMenuItem
          AutoCheck = True
          Caption = '&Hostile'
          RadioItem = True
          OnClick = mniHostile1Click
        end
      end
      object mniPlatformType1: TMenuItem
        Caption = '&Platform Type'
        object mniAircraftCarrier1: TMenuItem
          AutoCheck = True
          Caption = 'Aircraft Carrier (CV/CVN)'
          RadioItem = True
          OnClick = mniAircraftCarrier1Click
        end
        object mniAmphibius1: TMenuItem
          AutoCheck = True
          Caption = 'Amphibious Warfare'
          RadioItem = True
          OnClick = mniAmphibius1Click
        end
        object mniAuxiliary1: TMenuItem
          AutoCheck = True
          Caption = 'Auxiliary'
          RadioItem = True
          OnClick = mniAuxiliary1Click
        end
        object mniChaff1: TMenuItem
          AutoCheck = True
          Caption = 'Chaff'
          RadioItem = True
          OnClick = mniChaff1Click
        end
        object mniCruiserGuidedMissileCGCGN1: TMenuItem
          AutoCheck = True
          Caption = 'Cruiser, Guided Missile (CG/CGN)'
          RadioItem = True
          OnClick = mniCruiserGuidedMissileCGCGN1Click
        end
        object mniDestroyer1: TMenuItem
          AutoCheck = True
          Caption = 'Destroyer'
          RadioItem = True
          OnClick = mniDestroyer1Click
        end
        object mniDestroyerGuidedMissle1: TMenuItem
          AutoCheck = True
          Caption = 'Destroyer, Guided Missile (DOG)'
          RadioItem = True
          OnClick = mniDestroyerGuidedMissle1Click
        end
        object mniFrigateFF1: TMenuItem
          AutoCheck = True
          Caption = 'Frigate (FF)'
          RadioItem = True
          OnClick = mniFrigateFF1Click
        end
        object mniFrigateGuidedMissleFFG1: TMenuItem
          AutoCheck = True
          Caption = 'Frigate, Guided Missile (FFG)'
          RadioItem = True
          OnClick = mniFrigateGuidedMissleFFG1Click
        end
        object mniInfra1: TMenuItem
          AutoCheck = True
          Caption = 'Infrared Decoy'
          RadioItem = True
          OnClick = mniInfra1Click
        end
        object mniJammerDecoy1: TMenuItem
          AutoCheck = True
          Caption = 'Jammer Decoy'
          RadioItem = True
          OnClick = mniJammerDecoy1Click
        end
        object mniMerchant1: TMenuItem
          AutoCheck = True
          Caption = 'Merchant'
          RadioItem = True
          OnClick = mniMerchant1Click
        end
        object mniMainwarfare1: TMenuItem
          AutoCheck = True
          Caption = 'Main Warfare'
          OnClick = mniMainwarfare1Click
        end
        object mniPatrolCraftPTPTG1: TMenuItem
          AutoCheck = True
          Caption = 'Patrol Craft (PT/PTG)'
          OnClick = mniPatrolCraftPTPTG1Click
        end
        object mniUtilityVessel1: TMenuItem
          AutoCheck = True
          Caption = 'Utility Vessel'
          OnClick = mniUtilityVessel1Click
        end
        object mniOther1: TMenuItem
          AutoCheck = True
          Caption = 'Other'
          OnClick = mniOther1Click
        end
      end
      object mniPropulsionType1: TMenuItem
        Caption = 'P&ropulsion Type'
        Visible = False
      end
      object N5: TMenuItem
        Caption = '-'
      end
      object mniEdit1: TMenuItem
        Caption = '&Edit'
        SubMenuImages = imglistPM
        ImageIndex = 1
        ShortCut = 16453
        OnClick = mniEdit1Click
      end
    end
    object N4: TMenuItem
      Caption = '-'
    end
    object mniMerge1: TMenuItem
      Caption = '&Merge...'
      ShortCut = 16461
      OnClick = mniMerge1Click
    end
    object mniSplit1: TMenuItem
      Caption = '&Split'
      ShortCut = 16467
      OnClick = mniSplit1Click
    end
    object N6: TMenuItem
      Caption = '-'
    end
    object mniDatalink1: TMenuItem
      AutoHotkeys = maAutomatic
      Caption = '&Datalink'
      object mniTo1: TMenuItem
        Caption = 'To'
        ShortCut = 16468
      end
      object mniFrom1: TMenuItem
        Caption = 'From'
        ShortCut = 16454
      end
    end
    object N7: TMenuItem
      Caption = '-'
    end
    object mniTrackNumber1: TMenuItem
      Caption = 'Track &Number'
      object mniAutomatic1: TMenuItem
        Caption = '&Automatic'
        OnClick = mniAutomatic1Click
      end
      object mniManual1: TMenuItem
        Caption = '&Manual'
        OnClick = mniManual1Click
      end
    end
    object mniTrackHistory1: TMenuItem
      Caption = 'Track &History'
      SubMenuImages = imglistPM
      ImageIndex = 2
    end
    object N8: TMenuItem
      Caption = '-'
    end
    object mniIntiate1: TMenuItem
      Caption = 'Intiate TMA'
    end
    object mniBreakAllFireControl1: TMenuItem
      Caption = 'Break All Fire Control Asset Assignments'
    end
    object mniClearRing1: TMenuItem
      Caption = 'Clear Range Circles and Blind Zones '
    end
    object N9: TMenuItem
      Caption = '-'
    end
    object mniRemove1: TMenuItem
      Caption = 'Remove'
      ImageIndex = 0
      OnClick = mniRemove1Click
    end
    object N10: TMenuItem
      Caption = '-'
    end
    object mniCentre1: TMenuItem
      Caption = 'Centre'
      OnClick = mniCentre1Click
    end
    object mniRangeRing1: TMenuItem
      Caption = 'Range Rings'
      OnClick = mniRangeRing1Click
    end
    object mniTrackTable1: TMenuItem
      Caption = 'Track Table'
      object mniAdd1: TMenuItem
        Caption = 'Add'
        ShortCut = 16449
        OnClick = mniAdd1Click
      end
      object mniRemove2: TMenuItem
        Caption = 'Remove'
        ShortCut = 16466
        OnClick = mniRemove2Click
      end
    end
    object mniAnchor1: TMenuItem
      Caption = 'Anchor Cursor'
      SubMenuImages = imglistPM
      ImageIndex = 3
      OnClick = mniAnchor1Click
    end
  end
  object imglistPM: TImageList
    Left = 544
    Top = 120
    Bitmap = {
      494C010104007C00840010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000002000000001002000000000000020
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000008400000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000292C29000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000084000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000424142007375730063616300393C3900000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      8400000084000000840000000000000000000000000000000000000000000000
      0000000000000000840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00004A494A007B7D7B00A5A2A500B5B6B500BDBABD009C9E9C005A5D5A004A49
      4A00000000000000000000000000000000000000000000000000000000000000
      8400000084000000840000000000000000000000000000000000000000000000
      0000000084000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000525552006361
      6300D6D7D600949694004A4D4A00DEDBDE00BDBABD007B7D7B00D6D3D600BDBA
      BD004A4D4A000000000000000000000000000000000000000000000000000000
      0000000084000000840000008400000000000000000000000000000000000000
      8400000084000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000052555200636163005255
      5200525152000000000000000000E7E3E7005255520000000000000000005255
      52009C9E9C005255520000000000000000000000000000000000000000000000
      0000000000000000840000008400000084000000000000000000000084000000
      8400000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000005A595A00848284005A59
      5A00000000000000000000000000DEDFDE005A595A0000000000000000000000
      00005A595A00B5B6B50000000000000000000000000000000000000000000000
      0000000000000000000000008400000084000000840000008400000084000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000063616300636163000000
      0000000000000000000000000000DEDFDE006361630000000000000000000000
      00006B696B00ADAAAD0000000000000000000000000000000000000000000000
      0000000000000000000000000000000084000000840000008400000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000006B696B00000000000000
      0000000000000000000000000000CECBCE006B696B0000000000000000000000
      0000000000006B696B0000000000000000000000000000000000000000000000
      0000000000000000000000008400000084000000840000008400000084000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000073717300E7E7E700E7E3E700BDBEBD00ADAAAD00CECFCE00949694000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000840000008400000084000000000000000000000084000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000CECFCE007375730000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      8400000084000000840000008400000000000000000000000000000000000000
      8400000084000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000007B7D7B00D6D7D600848684007B7D7B00000000000000
      0000000000000000000000000000000000000000000000000000000084000000
      8400000084000000840000000000000000000000000000000000000000000000
      0000000084000000840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000848284008C8E8C00848284008482840094929400000000000000
      0000000000000000000000000000000000000000000000000000000084000000
      8400000000000000000000000000000000000000000000000000000000000000
      0000000000000000840000008400000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000084868400848684008482840084868400A5A2A500000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000009C9E9C00D6D7D600BDBABD008C8E8C00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000200000000100010000000000000100000000000000000000
      000000000000000000000000FFFFFF0000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FFFFFFFFFFFF0000EFFD001FF81F0000
      C7F7FFDFE70F0000C3EB905FF8070000E3C7FFDF84C30000F187905F07C30000
      F80FFFDF3F830000FC1FFEDF9F030000F83F9D5DBE030000F01FAA093E030000
      C0CFC0719687000081E702F9DF63000083F3F1F9DCA30000CFF9F931E0130000
      FFFFFC0BF81F0000FFFFFFFFFFFF000000000000000000000000000000000000
      000000000000}
  end
  object imgListButton: TImageList
    Left = 552
    Top = 192
    Bitmap = {
      494C0101020054005C0010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000001000000001002000000000000010
      00000000000000000000000000000000000000FF000000FF000000FF000000FF
      000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF
      000000FF000000FF000000FF000000FF00000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000FF000000FF000000FF000000FF
      000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF
      000000FF000000FF000000FF000000FF00000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000FF000000FF000000FF000000FF
      000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF
      000000FF000000FF000000FF000000FF00000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000FF000000FF000000FF000000FF
      000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF
      000000FF000000FF000000FF000000FF00000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000FF000000FF000000FF000000FF
      000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF
      000000FF000000FF000000FF000000FF00000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000FF000000FF000000FF000000FF
      000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF
      000000FF000000FF000000FF000000FF00000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000FF000000FF000000FF000000FF
      000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF
      000000FF000000FF000000FF000000FF00000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000FF000000FF000000FF000000FF
      000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF
      000000FF000000FF000000FF000000FF00000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000FF000000FF000000FF000000FF
      000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF
      000000FF000000FF000000FF000000FF00000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000FF000000FF000000FF000000FF
      000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF
      000000FF000000FF000000FF000000FF00000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000FF000000FF000000FF000000FF
      000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF
      000000FF000000FF000000FF000000FF00000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000FF000000FF000000FF000000FF
      000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF
      000000FF000000FF000000FF000000FF00000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000FF000000FF000000FF000000FF
      000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF
      000000FF000000FF000000FF000000FF00000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000FF000000FF000000FF000000FF
      000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF
      000000FF000000FF000000FF000000FF00000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000FF000000FF000000FF000000FF
      000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF
      000000FF000000FF000000FF000000FF00000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000FF000000FF000000FF000000FF
      000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF
      000000FF000000FF000000FF000000FF00000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000100000000100010000000000800000000000000000000000
      000000000000000000000000FFFFFF0000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000}
  end
  object tmMove: TTimer
    Enabled = False
    Interval = 100
    OnTimer = tmMoveTimer
    Left = 384
    Top = 352
  end
end
