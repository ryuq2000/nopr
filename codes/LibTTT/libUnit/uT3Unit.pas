unit uT3Unit;

interface

uses
  uSimObjects;

type
  TT3Unit = class abstract (TSimObject)
  private
    FInstanceIndex : integer;
    FInstanceName  : String;

    procedure SetInstanceIndex(const Value: integer);
    procedure SetInstanceName(const Value: String);

  public
    constructor Create; virtual;
    destructor Destroy; override;
    procedure Initialize;virtual;

    property InstanceIndex : integer read FInstanceIndex write SetInstanceIndex;
    property InstanceName  : String read FInstanceName write SetInstanceName;
  end;

implementation

{ TT3Unit }
constructor TT3Unit.Create;
begin
  inherited;

end;

destructor TT3Unit.Destroy;
begin

  inherited;
end;

procedure TT3Unit.Initialize;
begin

end;

procedure TT3Unit.SetInstanceIndex(const Value: integer);
begin
  FInstanceIndex := Value;
end;

procedure TT3Unit.SetInstanceName(const Value: String);
begin
  FInstanceName := Value;
end;

end.



