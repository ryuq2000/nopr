unit uT3MountedEO;

interface

uses uT3MountedSensor, uSimObjects, uT3BlindZone, uDBClassDefinition,
  uBaseCoordSystem, uT3PlatformInstance, tttData, uT3Vehicle;

type
  TT3MountedEO = class (TT3MountedSensor)
  private
    FDetectionRange: single;
    FDetailDetectionRange: Single;
    function getEOOnBoardDefinition: TDBEO_On_Board;
    procedure SetDetectionRange(const Value: single);
    procedure calcEOSensor(const aDeltaMs: double);
    procedure EOInactive;
    function getEODefinition: TDBEO_Detection_Definition;

  protected
    function  GetMaxLOSRange(aObject : TSimObject) : Double;
    function  InsideBlindZone(aObject : TSimObject) : boolean; override;
    function  InsideDetailRange(aObject : TSimObject) : Boolean;
    function  InsideRange(aObject : TSimObject) : boolean; override;
    function  InsideOtherMeasurement(aObject : TSimObject) : boolean;override;
    function  InsideVerticalCoverage(aObject : TSimObject) : boolean; override;
  published

  public
    constructor Create; override;

    procedure   Initialize; override;
    procedure   Move(const aDeltaMs: double); override;

    // read only prop
    property EOOnBoardDefinition  : TDBEO_On_Board read getEOOnBoardDefinition;
    property EODefinition  : TDBEO_Detection_Definition read getEODefinition;

    // rw prop
    property DetectionRange : single read FDetectionRange write SetDetectionRange;
    property DetailDetectionRange : Single read FDetailDetectionRange  write FDetailDetectionRange;

  end;

implementation

uses uGlobalVar;

{ TT3MountedEO }

procedure TT3MountedEO.calcEOSensor;
var
  I : integer;
  pfObject : TT3PlatformInstance;
  objBearing : double;
  allState : Boolean;
begin
    if Assigned(simManager.SimPlatforms) then
    begin
      for I := 0 to simManager.SimPlatforms.ItemCount - 1 do
      begin
        pfObject  :=  TT3PlatformInstance( simManager.SimPlatforms.getObject(I) );

        objBearing := CalcBearing(FPosition.X,FPosition.Y,
                pfObject.PosX,pfObject.PosY);

        if pfObject.Equals(PlatformParent) then
          Continue;

        // periksa semua factor deteksi
        allState := InsideRange(pfObject)
              and not InsideBlindZone(pfObject)
              and InsideVerticalCoverage(pfObject)
              and (EmconOperationalStatus <> EmconOn)
              and InsideOtherMeasurement(pfObject);

        case pfObject.DetectabilityType of
          dtNormalDetection:
          begin
            if Assigned(OnSensorDetect) then
              OnSensorDetect(Self, pfObject, allState);
          end;
          dtUndetectable:
          begin
            if Assigned(OnSensorDetect) then
              OnSensorDetect(Self, pfObject, False);
          end;
        end;
      end;
    end;
end;

constructor TT3MountedEO.Create;
begin
  inherited;
  FSensorType := stEO;
end;

procedure TT3MountedEO.EOInactive;
var
  I : integer;
  pfObject : TT3PlatformInstance;
begin
  if Assigned(simManager.SimPlatforms) then
  begin
    for I := 0 to simManager.SimPlatforms.ItemCount - 1 do
    begin
      pfObject  :=  TT3PlatformInstance( simManager.SimPlatforms.getObject(I) );

      if pfObject.Equals(PlatformParent) then
        Continue;

      if Assigned(OnSensorDetect) then
        OnSensorDetect(Self, pfObject, False);

    end;
  end;
end;

function TT3MountedEO.GetMaxLOSRange(aObject: TSimObject): Double;
var
  basicLOS : Double;
  TargetHeight, AntenaHeight, AntenaEOHeight   : Double;
  v : TT3Vehicle;
begin
  v := PlatformParent as TT3Vehicle;

  Result := 0;
  TargetHeight  := 0;

  if not (Assigned(v)) then
    Exit;

  if not (Assigned(aObject)) then
    Exit;

  if not (v is TT3Vehicle) then
    Exit;

  if not( aObject is TT3PlatformInstance) then
    Exit;

  if (TT3PlatformInstance(v).PlatformDomain = vhdSubsurface)
      and (TT3PlatformInstance(v).Altitude <> 0) then
    Exit;

  if (TT3PlatformInstance(aObject).PlatformDomain = vhdSubsurface)
      and (TT3PlatformInstance(aObject).Altitude <> 0) then
    Exit;

  if Assigned(EOOnBoardDefinition) then AntenaEOHeight := EOOnBoardDefinition.Antenna_Height
  else AntenaEOHeight := 0;

  if aObject is TT3Vehicle then
  begin
    if (TT3Vehicle(aObject).PlatformDomain = vhdAir) then
    begin
      TargetHeight := Sqrt(Abs(13 * ((TT3Vehicle(aObject).VehicleDefinition.Height * C_Feet_To_Meter) +
                      TT3Vehicle(aObject).Altitude)));
    end
    else
    if (TT3Vehicle(aObject).PlatformDomain = vhdSurface)
        or (TT3Vehicle(aObject).PlatformDomain = vhdLand)
        or (TT3Vehicle(aObject).PlatformDomain = vhdAmphibious) then
    begin
      TargetHeight := Sqrt(Abs(13 * (TT3Vehicle(aObject).VehicleDefinition.Height * C_Feet_To_Meter)));
    end
    else
    if (TT3Vehicle(aObject).PlatformDomain = vhdSubsurface) then
    begin
      TargetHeight := Sqrt(Abs(13 * ((TT3Vehicle(aObject).VehicleDefinition.Height * C_Feet_To_Meter) -
                      TT3Vehicle(aObject).Altitude)));
    end
    else
    begin
      TargetHeight := Sqrt(Abs(13 * ((TT3Vehicle(aObject).VehicleDefinition.Height * C_Feet_To_Meter) +
                      TT3Vehicle(aObject).Altitude)));
    end;
  end
  else
  begin
    if aObject is TT3PlatformInstance then TargetHeight := Sqrt(Abs(13 * TT3PlatformInstance(aObject).Altitude));
  end;

  if (Assigned(v)) then
  begin
    if (v.PlatformDomain = vhdAir) then
    begin
      AntenaHeight := Sqrt(Abs(13 * ((AntenaEOHeight
                      * C_Feet_To_Meter) + v.Altitude)));
    end
    else
    if (v.PlatformDomain = vhdSurface)
        or (v.PlatformDomain = vhdLand)
        or (v.PlatformDomain = vhdAmphibious) then
    begin
      AntenaHeight := Sqrt(Abs(13 * (AntenaEOHeight
                      * C_Feet_To_Meter)));
    end
    else
    if (v.PlatformDomain = vhdSubsurface) then
    begin
      AntenaHeight := Sqrt(Abs(13 * ((AntenaEOHeight
                      * C_Feet_To_Meter) - v.Altitude)));
    end
    else
    begin
      AntenaHeight := Sqrt(Abs(13 * ((AntenaEOHeight
                      * C_Feet_To_Meter) + v.Altitude)));
    end;
  end
  else AntenaHeight := Sqrt(Abs(13 * (AntenaEOHeight * C_Feet_To_Meter)));

  basicLOS := TargetHeight + AntenaHeight;

  Result := basicLOS;
end;

function TT3MountedEO.getEODefinition: TDBEO_Detection_Definition;
begin
  result := nil;

  if Assigned(FMountedDeviceDefinition) then
    Result := TDBEO_Detection_Definition(FMountedDeviceDefinition);
end;

function TT3MountedEO.getEOOnBoardDefinition: TDBEO_On_Board;
begin
  result := nil;

  if Assigned(FMountedDeviceOnBoardDef) then
    Result := TDBEO_On_Board(FMountedDeviceOnBoardDef);

end;

procedure TT3MountedEO.Initialize;
var
  i      : integer;
  bz     : TT3BlindZone;
  bzList : TDBBlindZoneDefinitionList;
  dbBz   : TDBBlind_Zone_Definition;
begin
  inherited;

  { set definition once }
  FDeviceParent            := simManager.FindT3PlatformByID(PlatformParentInstanceIndex);
  FMountedDeviceDefinition := simManager.Scenario.GetDBEODefinition(MountedDeviceDefinitionIndex);
  FMountedDeviceOnBoardDef := simManager.Scenario.
        GetDBEOOnBoardDef(TT3PlatformInstance(FDeviceParent).PlatformDefinitionIndex,
              MountedDeviceOnBoardDefIndex);

  if Assigned(EOOnBoardDefinition) then
  with EOOnBoardDefinition do begin
    InstanceName         := Instance_Identifier;
    InstanceIndex        := EO_Instance_Index;

    { set blind zone }
    bzList := simManager.Scenario.GetDBBlindZones('EO',EO_Instance_Index);
    if Assigned(bzList) then
      for dbBz in bzList do
      begin
        bz :=  TT3BlindZone.Create;
        bz.Blind_Zone_Type := dbBz.Blind_Zone_Type;
        bz.BlindZone_Number:= dbBz.BlindZone_Number;
        bz.Start_Angle     := dbBz.Start_Angle;
        bz.End_Angle       := dbBz.End_Angle;

        BlindZones.Add(bz);
      end;
  end;

  if Assigned(EODefinition) then
  with EODefinition do begin
    DetectionRange       := Max_Range;
    DetailDetectionRange := Detection_Range;
  end;

end;

function TT3MountedEO.InsideBlindZone(aObject: TSimObject): boolean;
var
  i : integer;
  blindZ : TT3BlindZone;
  bearing : double;
  sAngle, eAngle : double;
begin

  bearing := CalcBearing(FPosition.X,FPosition.Y,
           TT3PlatformInstance(aObject).getPositionX,
           TT3PlatformInstance(aObject).getPositionY);

  Result := false;

  // periksa apakah didalam area blind zone
  if BlindZones.Count > 0 then
    for blindZ in BlindZones do begin
      sAngle := blindZ.Start_Angle  + TT3PlatformInstance(PlatformParent).Course;
      eAngle := blindZ.End_Angle    + TT3PlatformInstance(PlatformParent).Course;
      Result := DegComp_IsBeetwen(bearing, sAngle, eAngle);
      if Result then
        Exit;
    end;
end;

function TT3MountedEO.InsideDetailRange(aObject: TSimObject): Boolean;
var
  range : Double;
begin
  inherited;

  range   := CalcRange(FPosition.X,FPosition.Y,
           aObject.getPositionX,aObject.getPositionY);

  Result := range <= DetailDetectionRange;
end;

function TT3MountedEO.InsideOtherMeasurement(aObject: TSimObject): boolean;
var
  maxLOS, range : Double;
begin
  Result := false;

  maxLOS := GetMaxLOSRange(aObject); {* C_Meter_To_NauticalMile}
  range  := CalcRange(PlatformParent.PosX, PlatformParent.PosY, aObject.PosX, aObject.PosY);

  if range <= maxLOS then
    Result := True;
end;

function TT3MountedEO.InsideRange(aObject: TSimObject): boolean;
var
  range : double;
begin
  inherited;
  range   := CalcRange(FPosition.X,FPosition.Y,
           aObject.getPositionX,aObject.getPositionY);

  result := range <= DetectionRange;
end;

function TT3MountedEO.InsideVerticalCoverage(aObject: TSimObject): boolean;
var
  altitude : Double;
begin
  altitude := TT3PlatformInstance(PlatformParent).Altitude * C_Meter_To_Feet;

  Result := altitude <= 100;
end;

procedure TT3MountedEO.Move(const aDeltaMs: double);
var
  bz : TT3BlindZone;
begin
  inherited;

  for bz in BlindZones do
    bz.Range := DetectionRange;

  // this only run in server
  if machineRole = crServer then
  begin
    if not (OperationalStatus in [sopOff,sopDamage, sopEMCON]) then
      calcEOSensor(aDeltaMs);

    if FDoOnceCalc then
    begin
      EOInactive;
      FDoOnceCalc := False;
    end;
  end;
end;

procedure TT3MountedEO.SetDetectionRange(const Value: single);
begin
  FDetectionRange := Value;
end;

end.
