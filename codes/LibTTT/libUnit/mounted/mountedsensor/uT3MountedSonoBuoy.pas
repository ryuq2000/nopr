unit uT3MountedSonoBuoy;

interface

uses uT3MountedSensor, uDBClassDefinition,
  tttData, uSimObjects, uT3PlatformInstance,
  Generics.Collections;

type
  TT3MountedSonobuoy = class (TT3MountedSensor)
  private
    FControlledSonobuoy : TList<TT3PlatformInstance>; // must be TT3Sonobuoy
    FQuantity: integer;
    FControlDepth: single;
    FControlMode: integer;
    function getSonobuoyDefinition: TDBSonobuoy_Definition;
    function getSonobuoyOnBoardDefinition: TDBSonobuoy_On_Board;
    function getmonitoringSonobuoy : integer;
    procedure SetQuantity(const Value: integer);
    procedure SetControlDepth(const Value: single);
    procedure SetControlMode(const Value: integer);
  published
  public
    constructor Create;override;
    destructor Destroy;override;

    procedure Initialize; override;
    procedure Move(const aDeltaMs: double); override;

    procedure removeControlledSonobuoy(sonobuoy : TT3PlatformInstance);
    { if frommine = true then sonobuoy from this device, decrement quantity }
    procedure addControlledSonobuoy(sonobuoy : TT3PlatformInstance; fromMine : Boolean = True);

    // read only prop
    property SonobuoyOnBoardDefinition  : TDBSonobuoy_On_Board read getSonobuoyOnBoardDefinition;
    property SonobuoyDefinition : TDBSonobuoy_Definition read getSonobuoyDefinition;

    // read write prop
    property Quantity : integer read FQuantity write SetQuantity;
    property MonitoringQuantity : integer read getmonitoringSonobuoy;
    property ControlDepth : single  read FControlDepth write SetControlDepth;
    property ControlMode : integer read FControlMode write SetControlMode;
  end;

implementation

uses
  uGlobalVar;

{ TT3MountedSonobuoy }

procedure TT3MountedSonobuoy.addControlledSonobuoy(
  sonobuoy: TT3PlatformInstance; fromMine: Boolean);
begin
  FControlledSonobuoy.Add(sonobuoy);

  if fromMine then
  begin
    Dec( FQuantity );
  end;
end;

constructor TT3MountedSonobuoy.Create;
begin
  inherited;
  FControlledSonobuoy := TList<TT3PlatformInstance>.create;
  FSensorType := stSonobuoy;

end;

destructor TT3MountedSonobuoy.Destroy;
begin
  FControlledSonobuoy.Clear;
  FControlledSonobuoy.Free;
  inherited;
end;

function TT3MountedSonobuoy.getSonobuoyDefinition: TDBSonobuoy_Definition;
begin
  result := nil;

  if Assigned(FMountedDeviceDefinition) then
    Result := TDBSonobuoy_Definition(FMountedDeviceDefinition);
end;

function TT3MountedSonobuoy.getSonobuoyOnBoardDefinition: TDBSonobuoy_On_Board;
begin
  result := nil;

  if Assigned(FMountedDeviceOnBoardDef) then
    Result := TDBSonobuoy_On_Board(FMountedDeviceOnBoardDef);
end;

procedure TT3MountedSonobuoy.Initialize;
var
  snrDef : TDBSonar_Definition;
  sonoDef : TDBSonobuoy_Definition;
begin
  inherited;

  if Assigned(SonobuoyOnBoardDefinition) then
  with SonobuoyOnBoardDefinition do begin
    InstanceName  := Instance_Identifier;
    InstanceIndex := Sonobuoy_Instance_Index;
    Quantity      := Quantity;

    simManager.Scenario.DBSonobuoy_Definition.TryGetValue(Sonobuoy_Index, sonoDef);
    if Assigned(sonoDef) then
    begin
      snrDef := simManager.Scenario.GetDBSonarDefinition(sonoDef.Sonar_Index);

      if Assigned(snrDef) then
      begin
        {control mode : active = 1, passive = 2}
        if snrDef.Sonar_Classification in [0,2] then
          ControlMode := 1
        else if snrDef.Sonar_Classification in [1,3] then
          ControlMode := 2
      end;
    end;
  end;
end;

function TT3MountedSonobuoy.getmonitoringSonobuoy: integer;
begin
  result := FControlledSonobuoy.Count;
end;

procedure TT3MountedSonobuoy.Move(const aDeltaMs: double);
begin
  inherited;

end;

procedure TT3MountedSonobuoy.removeControlledSonobuoy(
  sonobuoy: TT3PlatformInstance);
begin
  FControlledSonobuoy.Remove(sonobuoy);
end;

procedure TT3MountedSonobuoy.SetControlDepth(const Value: single);
begin
  FControlDepth := Value;
end;

procedure TT3MountedSonobuoy.SetControlMode(const Value: integer);
begin
  FControlMode := Value;
end;

procedure TT3MountedSonobuoy.SetQuantity(const Value: integer);
begin
  FQuantity := Value;
end;

end.
