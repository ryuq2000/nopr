unit uT3MountedRadar;

interface

uses uT3MountedSensor, tttData, uT3BlindZone,
  uBaseCoordSystem, uT3PlatformInstance, uSimObjects,
  uT3Vehicle, uDBClassDefinition, uT3RadarVerticalCoverage;

type
  TT3MountedRadar = class (TT3MountedSensor)
  private
    FDetailDetectionRange    : double;
    FSpotNumber              : integer;
    FRNJammOperationalStatus : TRNJammOperasionalStatus;

    FSwapDegree              : double;
    FLastSwapDegree          : double;
    FisStartPartial          : Boolean;

    FStartScan               : double;
    FEndScan                 : double;
    FScanSector              : TRadarScanSector;
    FShowScanSector          : Boolean;

    FControlMode             : TRadarControlMode;
    FOnControlMode           : TOnRadarControlMode;

    FVerticalCoverage        : TT3RadarVerticalCoverage;

    function getRadarOnBoardDefinition: TDBRadar_On_Board;
    function getRadarDefinition: TDBRadar_Definition;
    function getDetectionRange: double;
    function getECCMType: integer;
    function getDetailDetectionRange: double;
    procedure SetSpotNumber(const Value: integer);
    procedure SetDetailDetectionRange(const Value: double);

    procedure Swap(const aDeltaMs: double);
    procedure RadarInactived;
    procedure calcRadarSensor;
    function  GetMaxLOSRange(aObject : TSimObject) : Double;
    function ObjectDetectability(aObject : TT3PlatformInstance) : Boolean;

    procedure SetControlMode(const Value: TRadarControlMode);
    procedure SetEndScan(const Value: double);
    procedure SetShowScanSector(const Value: boolean);
    procedure SetStartScan(const Value: double);
    procedure SetScanSector(const Value: TRadarScanSector);
    procedure SetRNJammOperationalStatus(const Value: TRNJammOperasionalStatus);
    procedure SetRNJammStatus(const Value: TSensorOperationalStatus; const Jamm: TRNJammOperasionalStatus);
    procedure SetOperationalStatus(const Value: TSensorOperationalStatus); override;
    procedure SetOnControlMode(const Value: TOnRadarControlMode);
    function getSwapRelatifDeg: double;
  protected
    function  InsideBlindZone(aObject : TSimObject) : boolean; override;
    function  InsideVerticalCoverage(aObject: TSimObject): boolean; override;
    function  InsideOtherMeasurement(aObject : TSimObject) : boolean; override;
    function  InsideRange(aObject : TSimObject) : boolean; override;
    function  InsideJammedEffect : Boolean; override;
    function  TargetTypeEligiblity(aTarget: TObject): boolean;
  public
    constructor Create;override;
    destructor Destroy;override;
    procedure Initialize; override;
    procedure Move(const aDeltaMs: double); override;

    // read only prop
    property RadarOnBoardDefinition  : TDBRadar_On_Board read getRadarOnBoardDefinition;
    property RadarDefinition  : TDBRadar_Definition read getRadarDefinition;

    property DetectionRange   : double read getDetectionRange;
    property DetailDetectionRange : double read getDetailDetectionRange;
    property ECCMType         : integer read getECCMType;
    property SwapDegreeRelatifToVehicle : double read getSwapRelatifDeg;

    // rw prop
    property SpotNumber     : integer read FSpotNumber write SetSpotNumber;
    property ControlMode    : TRadarControlMode read FControlMode write SetControlMode;
    property ShowScanSector : boolean read FShowScanSector write SetShowScanSector;
    property StartScan      : double read FStartScan write SetStartScan;
    property EndScan        : double read FEndScan write SetEndScan;
    property ScanSector     : TRadarScanSector read FScanSector write SetScanSector;

    property RNJammOperationalStatus : TRNJammOperasionalStatus
      read FRNJammOperationalStatus write SetRNJammOperationalStatus;

    { event trigger }
    property OnControlMode   : TOnRadarControlMode read FOnControlMode write SetOnControlMode;
  end;

implementation

uses
  uGlobalVar;

{ TT3MountedRadar }

procedure TT3MountedRadar.calcRadarSensor;
var
  doProcess : Boolean;
  dt : double;
  objBearing : double;
  i : Integer;
  pfObject : TT3PlatformInstance;

  isInRange : Boolean;
  isInsideBlindZone : Boolean;
  isInsideVerticalCov : Boolean;
  isInsideOtherMeasurement : Boolean;
  isTargetTypeEligiblity : Boolean;
  isJammed : Boolean;
  allState : Boolean;
begin
  doProcess := FSwapDegree - FLastSwapDegree > 0;

  if doProcess then
  begin

    { swap delta degree }
    dt := FSwapDegree - FLastSwapDegree ;

    { search object between degree}
    if Assigned(simManager.SimPlatforms) then
    begin
      for I := 0 to simManager.SimPlatforms.ItemCount - 1 do
      begin
        pfObject  :=  TT3PlatformInstance( simManager.SimPlatforms.getObject(I) );

        objBearing := CalcBearing(FPosition.X,FPosition.Y,
                pfObject.PosX,pfObject.PosY);

        if (objBearing < FSwapDegree) and (FSwapDegree - objBearing <= dt) then
        begin
          { object between swap }

          if pfObject.Equals(PlatformParent) then
            Continue;

          // periksa apakah dalam jangkauan radar
          isInRange                := InsideRange(pfObject);
          // periksa apa di dalam blind zone
          isInsideBlindZone        := InsideBlindZone(pfObject);
          // periksa apa di dalam vertical cov
          isInsideVerticalCov      := InsideVerticalCoverage(pfObject);
          // periksa pengukuran lainyya
          isInsideOtherMeasurement := InsideOtherMeasurement(pfObject);
          //cek apakah target bisa dideteksi
          isTargetTypeEligiblity   := TargetTypeEligiblity(pfObject);

          // cek jaming proses
          isJammed                 := InsideJammedEffect;

          // periksa semua factor deteksi
          allState := isInRange
                and not isInsideBlindZone
                and isInsideVerticalCov
                and (EmconOperationalStatus <> EmconOn)
                and isInsideOtherMeasurement
                and isTargetTypeEligiblity

                and not(isJammed)

                and ObjectDetectability(pfObject);

          if Assigned(OnSensorDetect) then
            OnSensorDetect(Self, pfObject, allState);
        end;

      end;
    end;

  end;

  FLastSwapDegree := FSwapDegree;
end;

constructor TT3MountedRadar.Create;
begin
  inherited;
  FSwapDegree := 0;
  FLastSwapDegree := 0;

  FControlMode        := rcmOff;
  FScanSector         := rscFull;
  FStartScan          := 0;
  FEndScan            := 360;
  FisStartPartial     := False;

  FShowScanSector     := false;

  FRNJammOperationalStatus := JammedOff;
  FSensorType := stRadar;

  FVerticalCoverage := TT3RadarVerticalCoverage.Create;
end;

destructor TT3MountedRadar.Destroy;
begin
  FVerticalCoverage.Free;
  inherited;
end;

function TT3MountedRadar.getDetailDetectionRange: double;
begin
  result := RadarDefinition.Detection_Range;
end;

function TT3MountedRadar.getDetectionRange: double;
begin
  result := RadarDefinition.Max_Unambig_Detect_Range;
end;

function TT3MountedRadar.getECCMType: integer;
begin
  result := RadarDefinition.ECCM_Type;
end;

function TT3MountedRadar.GetMaxLOSRange(aObject: TSimObject): Double;
var
  basicLOS : Double;
  TargetHeight, AntenaHeight,
  RadarAntena, SubRadarAntena   : Double;
  v : TT3Vehicle;
begin

  v := TT3Vehicle(PlatformParent);

  Result := 0;
  TargetHeight  := 0;

  if not (Assigned(v)) then
    Exit;

  if not (Assigned(aObject)) then
    Exit;

  if not (v is TT3Vehicle) then
    Exit;

  if not( aObject is TT3PlatformInstance) then
    Exit;

  if (TT3PlatformInstance(v).PlatformDomain = vhdSubsurface)
      and (TT3PlatformInstance(v).Altitude <> 0) then
    Exit;

  if (TT3PlatformInstance(aObject).PlatformDomain = vhdSubsurface)
      and (TT3PlatformInstance(aObject).Altitude <> 0) then
    Exit;

  if Assigned(RadarOnBoardDefinition) then
  begin
    RadarAntena := RadarOnBoardDefinition.Rel_Antenna_Height;
    SubRadarAntena := RadarOnBoardDefinition.Submerged_Antenna_Height;
  end
  else
  begin
    RadarAntena := 0;
    SubRadarAntena := 0;
  end;

  if aObject is TT3Vehicle then
  begin
    if (TT3Vehicle(aObject).PlatformDomain = vhdAir) then
    begin
      TargetHeight := Sqrt(Abs(17 * ((TT3Vehicle(aObject).VehicleDefinition.Height * C_Feet_To_Meter) +
                      TT3Vehicle(aObject).Altitude)));
    end
    else
    if (TT3Vehicle(aObject).PlatformDomain = vhdSurface)
        or (TT3Vehicle(aObject).PlatformDomain = vhdLand)
        or (TT3Vehicle(aObject).PlatformDomain = vhdAmphibious) then
    begin
      TargetHeight := Sqrt(Abs(17 * (TT3Vehicle(aObject).VehicleDefinition.Height * C_Feet_To_Meter)));
    end
    else
    if (TT3Vehicle(aObject).PlatformDomain = vhdSubsurface) then
    begin
      TargetHeight := Sqrt(Abs(17 * ((TT3Vehicle(aObject).VehicleDefinition.Height * C_Feet_To_Meter) -
                      TT3Vehicle(aObject).Altitude)));
    end
    else
    begin
      TargetHeight := Sqrt(Abs(17 * ((TT3Vehicle(aObject).VehicleDefinition.Height * C_Feet_To_Meter) +
                      TT3Vehicle(aObject).Altitude)));
    end;
  end
  else
  begin
    if aObject is TT3PlatformInstance then TargetHeight := Sqrt(Abs(17 * TT3PlatformInstance(aObject).Altitude));
  end;

  if (Assigned(v)) then
  begin
    if (v.PlatformDomain = vhdAir) then
    begin
      AntenaHeight := Sqrt(Abs(17 * (((RadarAntena + SubRadarAntena) //+ v.VehicleDefinition.FData.Height)
                      * C_Feet_To_Meter) + v.Altitude)));
    end
    else
    if (v.PlatformDomain = vhdSurface)
        or (v.PlatformDomain = vhdLand)
        or (v.PlatformDomain = vhdAmphibious) then
    begin
      AntenaHeight := Sqrt(Abs(17 * ((RadarAntena + SubRadarAntena) //+ v.VehicleDefinition.FData.Height)
                      * C_Feet_To_Meter)));
    end
    else
    if (v.PlatformDomain = vhdSubsurface) then
    begin
      AntenaHeight := Sqrt(Abs(17 * (((RadarAntena + SubRadarAntena) //+ v.VehicleDefinition.FData.Height)
                      * C_Feet_To_Meter) - v.Altitude)));
    end
    else
    begin
      AntenaHeight := Sqrt(Abs(17 * (((RadarAntena + SubRadarAntena) //+ v.VehicleDefinition.FData.Height)
                      * C_Feet_To_Meter) + v.Altitude)));
    end;
  end
  else AntenaHeight := Sqrt(Abs(17 * ((RadarAntena + SubRadarAntena) * C_Feet_To_Meter)));

  basicLOS := TargetHeight + AntenaHeight;

  Result := basicLOS;
end;

function TT3MountedRadar.getRadarDefinition: TDBRadar_Definition;
begin
  result := nil;

  if Assigned(FMountedDeviceDefinition) then
    Result := TDBRadar_Definition(FMountedDeviceDefinition);

end;

function TT3MountedRadar.getRadarOnBoardDefinition: TDBRadar_On_Board;
begin
  result := nil;

  if Assigned(FMountedDeviceOnBoardDef) then
    Result := TDBRadar_On_Board(FMountedDeviceOnBoardDef);

end;

function TT3MountedRadar.getSwapRelatifDeg: double;
var
  relSwap : double;
begin
  result := FSwapDegree;
  if Assigned(PlatformParent) then
  begin

    relSwap := FSwapDegree - PlatformParent.Course;
    if relSwap < 0 then
      relSwap := 360 - Abs(relSwap);

    result := relSwap;

  end;
end;

procedure TT3MountedRadar.Initialize;
var
  bz            : TT3BlindZone;
  bzList        : TDBBlindZoneDefinitionList;
  dbBz          : TDBBlind_Zone_Definition;
  vertCoverList : TDBRadar_Vertical_CovList;
  vertCover     : TDBRadar_Vertical_Coverage;
begin
  inherited;

  { set definition once }
  FDeviceParent            := simManager.FindT3PlatformByID(PlatformParentInstanceIndex);
  FMountedDeviceDefinition := simManager.Scenario.GetDBRadarDefinition(MountedDeviceDefinitionIndex);
  FMountedDeviceOnBoardDef := simManager.Scenario.
        GetDBRadarOnBoardDef(TT3PlatformInstance(FDeviceParent).PlatformDefinitionIndex,
              MountedDeviceOnBoardDefIndex);

  if Assigned(RadarOnBoardDefinition) then
  with RadarOnBoardDefinition do
  begin
    InstanceName          := Instance_Identifier;
    InstanceIndex         := Radar_Instance_Index;

    { set blind zone }
    bzList := simManager.Scenario.GetDBBlindZones('Radar',Radar_Instance_Index);
    if Assigned(bzList) then
      for dbBz in bzList do
      begin
        bz :=  TT3BlindZone.Create;
        bz.Blind_Zone_Type := dbBz.Blind_Zone_Type;
        bz.BlindZone_Number:= dbBz.BlindZone_Number;
        bz.Start_Angle     := dbBz.Start_Angle;
        bz.End_Angle       := dbBz.End_Angle;

        BlindZones.Add(bz);
      end;
  end;

  if Assigned(RadarDefinition) then begin
    FSpotNumber           := RadarDefinition.Radar_Spot_Number;

    { set vertical coverage }
    vertCoverList := simManager.Scenario.GetDBRadarVerticalCoverages(RadarDefinition.Radar_Index);
    for vertCover in vertCoverList do
      FVerticalCoverage.Add(vertCover);
  end;
end;

function TT3MountedRadar.InsideBlindZone(aObject: TSimObject): boolean;
var
  i : integer;
  blindZ : TT3BlindZone;
  bearing : double;
  sAngle, eAngle : double;
begin

  bearing := CalcBearing(FPosition.X,FPosition.Y,
         TT3PlatformInstance(aObject).getPositionX,
         TT3PlatformInstance(aObject).getPositionY);

  Result := false;

  // periksa apakah didalam area blind zone
  if BlindZones.Count > 0 then
    for blindZ in BlindZones do begin
      sAngle := blindZ.Start_Angle  + TT3PlatformInstance(PlatformParent).Course;
      eAngle := blindZ.End_Angle    + TT3PlatformInstance(PlatformParent).Course;
      Result := DegComp_IsBeetwen(bearing, sAngle, eAngle);
      if Result then
        Exit;
    end

end;

function TT3MountedRadar.InsideJammedEffect: Boolean;
begin
  if FRNJammOperationalStatus = JammedOn then
    Result := True
  else
    Result := False;
end;

function TT3MountedRadar.InsideOtherMeasurement(aObject: TSimObject): boolean;
var
  maxLOS   : Double;
  range, AtmosRefract, HorizonFactor : Double;
  InsideDomain : boolean;
begin
 //add andik
  InsideDomain := true;

  if aObject is TT3PlatformInstance then
  begin
    case TT3PlatformInstance(aObject).PlatformDomain of     //mk 19042012
      2:
        begin
          if TT3PlatformInstance(aObject).Altitude <> 0 then
            InsideDomain := false
          else
            InsideDomain := True;
        end;
    end;
  end;

  if InsideDomain then
  begin
    InsideDomain := false;

    if Assigned(simManager.GameEnvironment) then
    begin
      AtmosRefract := simManager.GameEnvironment.Data.Atmospheric_Refract_Modifier / 100;
    end
    else
    begin
      AtmosRefract := 1;
    end;

    if Assigned(RadarDefinition) then
    begin
      HorizonFactor := RadarDefinition.Radar_Horizon_Factor;
    end
    else
    begin
      HorizonFactor := 1;
    end;

    maxLOS := (GetMaxLOSRange(aObject)* 1000 * C_Meter_To_NauticalMile)
              * AtmosRefract * HorizonFactor;

    range  := CalcRange(FPosition.X, FPosition.Y, aObject.PosX, aObject.PosY);

    if range < maxLOS then
      InsideDomain := True;
  end;

  result := InsideDomain;
end;

function TT3MountedRadar.InsideRange(aObject: TSimObject): boolean;
var
  range : double;
begin
  inherited;

  range   := CalcRange(FPosition.X,FPosition.Y,
           aObject.getPositionX,aObject.getPositionY);

  result := range <= DetectionRange;
end;

function TT3MountedRadar.InsideVerticalCoverage(aObject: TSimObject): boolean;
var
  v : TT3Vehicle;
  min1,max1,min2,max2, range : double;
  AntenaHeight, RadarAntena, SubRadarAntena   : Double;
begin
  Result := False;

  if not Assigned(RadarDefinition) then
    Exit;

  // periksa apakah dalam jangkauan vertikal radar
  if FVerticalCoverage.isDataCoverageExist then
  begin
    if not(Assigned(aObject)) and not(aObject is TT3PlatformInstance) then
      Exit;

    range := CalcRange(FPosition.X,FPosition.Y, TT3PlatformInstance(aObject).getPositionX,
             TT3PlatformInstance(aObject).getPositionY);

    FVerticalCoverage.GetCoverage(range,min1,max1,min2,max2);

    RadarAntena     := RadarOnBoardDefinition.Rel_Antenna_Height;
    SubRadarAntena  := RadarOnBoardDefinition.Submerged_Antenna_Height;

    //Radar Antena Height
    v := PlatformParent as TT3Vehicle;
    if (Assigned(v)) then
    begin
      if RadarAntena = 0 then
      begin
        RadarAntena :=  v.VehicleDefinition.Height;
      end;

      if v.PlatformDomain = vhdAir then
      begin
        AntenaHeight := Sqrt(Abs(17 * (((RadarAntena + SubRadarAntena - 1000)
                        * C_Feet_To_Meter) + v.Altitude)));
      end
      else
      if (v.PlatformDomain = vhdSurface) or (v.PlatformDomain = vhdLand)
          or (v.PlatformDomain = vhdAmphibious) then
      begin
        AntenaHeight := Sqrt(Abs(17 * ((RadarAntena + SubRadarAntena - 1000)* C_Feet_To_Meter)));
      end
      else
      if (v.PlatformDomain = vhdSubsurface) then
      begin
        AntenaHeight := Sqrt(Abs(17 * (((RadarAntena + SubRadarAntena - 1000)
                        * C_Feet_To_Meter) - v.Altitude)));
      end
      else
      begin
        AntenaHeight := Sqrt(Abs(17 * (((RadarAntena + SubRadarAntena - 1000)
                        * C_Feet_To_Meter) + v.Altitude)));
      end;
    end
    else
      AntenaHeight := Sqrt(Abs(17 * ((RadarAntena + SubRadarAntena  - 1000) * C_Feet_To_Meter)));

    // PositionZ belum tau satuannya -> harus diubah dulu ke feet sebelum
    // di bandingkan dengan min dan max
    if (TT3PlatformInstance(aObject).Altitude > ((min1 * C_Feet_To_Meter)+ AntenaHeight)) and
       (TT3PlatformInstance(aObject).Altitude > ((min2 * C_Feet_To_Meter)+ AntenaHeight)) and
       (TT3PlatformInstance(aObject).Altitude < ((max1 * C_Feet_To_Meter)+ AntenaHeight)) and
       (TT3PlatformInstance(aObject).Altitude < ((max2 * C_Feet_To_Meter)+ AntenaHeight)) then
    begin
      result := true;
    end;
  end
  else
  begin
    // dianggap semua ketinggian/kedalaman tercover
    // karena tidak ada data vertical coverage
    result := true;
  end;
end;

procedure TT3MountedRadar.Move(const aDeltaMs: double);
var
  bz : TT3BlindZone;
begin
  inherited;

  for bz in BlindZones do
    bz.Range := DetectionRange;

  // this only run in server
  if machineRole = crServer then
  begin
    if not (OperationalStatus in [sopOff,sopDamage,sopEMCON]) then
      Swap(aDeltaMs);

    if FDoOnceCalc then
    begin
      RadarInactived;
      FDoOnceCalc := False;
    end;

  end;

end;

function TT3MountedRadar.ObjectDetectability(
  aObject: TT3PlatformInstance): Boolean;
begin
  Result := True;

  case aObject.DetectabilityType of
    dtNormalDetection : Result := True;
    dtUndetectable    : Result := False;
    dtPassiveDetection:
      if OperationalStatus = sopPassive then
        Result := True
      else
        Result := False;
    dtAlwaysVisible   : Result := True;
  end;

end;

procedure TT3MountedRadar.RadarInactived;
var
  i : integer;
  pfObject : TT3PlatformInstance;
begin
  { REMOVE DETECTED BY THIS RADAR }
  if Assigned(simManager.SimPlatforms) then
  begin
    for I := 0 to simManager.SimPlatforms.ItemCount - 1 do
    begin
      pfObject  :=  TT3PlatformInstance(simManager.SimPlatforms.getObject(I) );

      if Assigned(OnSensorDetect) then
        OnSensorDetect(Self, pfObject, False);
    end;
  end;
end;

procedure TT3MountedRadar.SetControlMode(const Value: TRadarControlMode);
begin
  FControlMode := Value;

  case Value of
    rcmOff        : SetOperationalStatus(sopOff);
    rcmTrack      : SetOperationalStatus(sopOn);
    rcmSearchTrack: SetOperationalStatus(sopOn);
  end;

  if Assigned(FOnControlMode) then
    FOnControlMode(self,value);
end;

procedure TT3MountedRadar.SetDetailDetectionRange(const Value: double);
begin
  FDetailDetectionRange := Value;
end;

procedure TT3MountedRadar.SetEndScan(const Value: double);
begin
  FEndScan := Value;
end;

procedure TT3MountedRadar.SetOnControlMode(const Value: TOnRadarControlMode);
begin
  FOnControlMode := Value;
end;

procedure TT3MountedRadar.SetOperationalStatus(
  const Value: TSensorOperationalStatus);
begin
  inherited;

  if (Value = sopOff) or (Value = sopDamage) then
  begin
    FDoOnceCalc := True;
//    RemoveAssignedtrack;
  end;

end;

procedure TT3MountedRadar.SetRNJammOperationalStatus(
  const Value: TRNJammOperasionalStatus);
begin
  FRNJammOperationalStatus := Value;
  SetRNJammStatus(OperationalStatus, value);
end;

procedure TT3MountedRadar.SetRNJammStatus(const Value: TSensorOperationalStatus;
  const Jamm: TRNJammOperasionalStatus);
begin
  OperationalStatus := Value;

//  if Assigned(OnSensorOperationalStatus) then
//    OnSensorOperationalStatus(Self, Value);

  if Value = sopOff then
    if Assigned(OnSensorRemoved) then
      OnSensorRemoved(self);

//  if Jamm = JammedOn then
//  begin
//    TT3Radar(Self).RemoveAssignedtrack;
//  end;

end;

procedure TT3MountedRadar.SetScanSector(const Value: TRadarScanSector);
begin
  FScanSector := Value;
end;

procedure TT3MountedRadar.SetShowScanSector(const Value: boolean);
begin
  FShowScanSector := Value;
end;

procedure TT3MountedRadar.SetSpotNumber(const Value: integer);
begin
  FSpotNumber := Value;
end;

procedure TT3MountedRadar.SetStartScan(const Value: double);
begin
  FStartScan := Value;
end;

procedure TT3MountedRadar.Swap(const aDeltaMs: double);
begin
  if not Assigned(RadarDefinition) then
    Exit;

  // do radar swap base on scan rate --> deg/sec
  // every second has x degree increased
  FSwapDegree := FSwapDegree + ( RadarDefinition.Scan_Rate * aDeltaMs );

  // calculate sensor
  case FScanSector of
    rscFull :
    begin
      calcRadarSensor;
    end;
    rscPartial :
    begin
      { assume start always < end }
      if ( FSwapDegree >= FStartScan) and (FSwapDegree <= FEndScan) then
      begin
        FisStartPartial := True;
        FLastSwapDegree := FStartScan;
      end;

      if FisStartPartial then
        calcRadarSensor;

      // one partial swap finished
      if FSwapDegree >= FEndScan then
      begin
        FisStartPartial := False;
      end;

    end;
  end;

  // one full swap finished
  if FSwapDegree >= 360 then
  begin
    FLastSwapDegree := 0;
    FSwapDegree :=  0;
  end;

end;

function TT3MountedRadar.TargetTypeEligiblity(aTarget: TObject): boolean;
begin
  Result := True;

  if aTarget is TT3Vehicle then
  begin
    if TT3Vehicle(aTarget).VehicleDefinition.Detectability_Type = 1 then
      Result := False;
  end;

end;

end.
