unit uT3MountedMAD;

interface

uses uT3MountedSensor, uDBClassDefinition,
  uGlobalVar, tttData, uSimObjects, uBaseCoordSystem, uT3PlatformInstance,
  uT3Vehicle;

  type
  TT3MountedMAD = class (TT3MountedSensor)
  private
    FDetectionRange: single;
    function getMADOnBoardDefinition: TDBMAD_Sensor_On_Board;
    function getMADDefinition: TDBMAD_Definition;
    procedure SetDetectionRange(const Value: single);
    procedure calcMADSensor(const aDeltaMs: double);
  protected
    function  InsideRange(aObject : TSimObject) : boolean;
  public
    constructor Create;override;
    procedure Initialize; override;
    procedure Move(const aDeltaMs: double); override;

    // read only prop
    property MADOnBoardDefinition  : TDBMAD_Sensor_On_Board read getMADOnBoardDefinition;
    property MADDefinition  : TDBMAD_Definition read getMADDefinition;

    // rw prop
    property DetectionRange : single read FDetectionRange write SetDetectionRange;

  end;

implementation

{ TT3MountedMAD }

procedure TT3MountedMAD.calcMADSensor(const aDeltaMs: double);
var
  i,j : integer;
  sensor : TT3MountedSensor;
  pf : TT3PlatformInstance;
  v : TT3Vehicle;
  status : Boolean;
begin

  if Assigned(simManager.SimPlatforms) then
  begin
    for I := 0 to simManager.SimPlatforms.itemCount - 1 do
	  begin
      pf := simManager.SimPlatforms.getObject(I) as TT3PlatformInstance;

      if pf.InstanceIndex = PlatformParentInstanceIndex then
        continue;

      status := InsideRange(pf) and (EmconOperationalStatus <> EmconOn);

      case pf.DetectabilityType of
        dtNormalDetection:
        begin
          case sensor.OperationalStatus of
            sopOn   : OnSensorDetect(Self,pf,status);
          else
            OnSensorDetect(Self,pf,false);
          end;
        end;
        dtUndetectable:
        begin
          if Assigned(OnSensorDetect) then
            OnSensorDetect(Self, pf, False);
        end;
      end;


   end;
  end;
end;

constructor TT3MountedMAD.Create;
begin
  inherited;
  FSensorType := stMAD;

end;

function TT3MountedMAD.getMADDefinition: TDBMAD_Definition;
begin
  result := nil;

  if Assigned(FMountedDeviceDefinition) then
    Result := TDBMAD_Definition(FMountedDeviceDefinition);
end;

function TT3MountedMAD.getMADOnBoardDefinition: TDBMAD_Sensor_On_Board;
begin
  result := nil;

  if Assigned(FMountedDeviceOnBoardDef) then
    Result := TDBMAD_Sensor_On_Board(FMountedDeviceOnBoardDef);

end;

procedure TT3MountedMAD.Initialize;
begin
  inherited;

  { set definition once }
  FDeviceParent            := simManager.FindT3PlatformByID(PlatformParentInstanceIndex);
  FMountedDeviceDefinition := simManager.Scenario.GetDBMADDefinition(MountedDeviceDefinitionIndex);
  FMountedDeviceOnBoardDef := simManager.Scenario.
        GetDBMADOnBoardDef(TT3PlatformInstance(FDeviceParent).PlatformDefinitionIndex,
              MountedDeviceOnBoardDefIndex);

  if Assigned(MADOnBoardDefinition) then
  with MADOnBoardDefinition do begin
    InstanceName    := Instance_Identifier;
    InstanceIndex   := MAD_Instance_Index;
    DetectionRange  := MADDefinition.Baseline_Detect_Range;
  end;

end;

function TT3MountedMAD.InsideRange(aObject: TSimObject): boolean;
var
  Max_Detection_Range, Range : Double;
begin

  Range := CalcRange3D(PlatformParent.PosX, PlatformParent.PosY, aObject.PosX,
           aObject.PosY, PlatformParent.PosZ, aObject.PosZ);

  if aObject is TT3Vehicle then
  begin
    Max_Detection_Range := (TT3Vehicle(aObject).VehicleDefinition.Magnetic_Cross /
                           (MADDefinition.Known_Cross_Section
                            * MADDefinition.Baseline_Detect_Range));
  end
  else
  begin
    Max_Detection_Range := 0;
  end;

  Result := Range <= Max_Detection_Range;
end;

procedure TT3MountedMAD.Move(const aDeltaMs: double);
begin
  inherited;

  // this only run in server
  if machineRole = crServer then
  begin
    if not (OperationalStatus in [sopOff,sopDamage,sopEMCON]) then
      calcMADSensor(aDeltaMs);

    if FDoOnceCalc then
    begin
      calcMADSensor(aDeltaMs);
      FDoOnceCalc := False;
    end;
  end;

end;

procedure TT3MountedMAD.SetDetectionRange(const Value: single);
begin
  FDetectionRange := Value;
end;

end.
