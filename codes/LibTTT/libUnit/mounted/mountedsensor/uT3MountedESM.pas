unit uT3MountedESM;

interface

uses uT3MountedSensor, uDBClassDefinition, uT3BlindZone,
  uGlobalVar, tttData, uSimObjects, uBaseCoordSystem, uT3PlatformInstance,
  uT3Vehicle, uT3MountedDevice;

type
  TT3MountedESM = class (TT3MountedSensor)
  private
    FDetectionRange: single;
    FDetailDetectionRange: Single;
    procedure SetDetectionRange(const Value: single);
    function getESMOnBoardDefinition: TDBESM_On_Board;
    function getESMDefinition: TDBESM_Definition;

    procedure calcESMSensor(const aDeltaMs: double);
  protected
    function GetMaxLOSRange(aObject : TSimObject) : Double; //override;
    function InsideRange(aObject : TSimObject) : boolean; override;
    function InsideBlindZone(aObject : TSimObject) : boolean; override;
    function InsideOtherMeasurement(aObject : TSimObject) : boolean; override;
    function TargetTypeEligiblity(aTarget: TObject): boolean;
  public
    constructor Create;override;
    procedure Initialize; override;
    procedure Move(const aDeltaMs: double); override;
    function getVariance : double;
    // read only prop
    property ESMOnBoardDefinition  : TDBESM_On_Board read getESMOnBoardDefinition;
    property ESMDefinition         : TDBESM_Definition read getESMDefinition;
    // rw prop
    property DetectionRange : single read FDetectionRange write SetDetectionRange;
    property DetailDetectionRange : Single read FDetailDetectionRange  write FDetailDetectionRange;

  end;

implementation

uses uT3MountedRadar;

{ TT3MountedESM }

procedure TT3MountedESM.calcESMSensor(const aDeltaMs: double);
var
  radar : TT3MountedRadar;
  status : Boolean;
  I, cnt : Integer;
begin
  // cek assined property OnSensorDetect
  if not Assigned(OnSensorDetect)     then Exit;

  { check radar only }
  cnt := simManager.SimRadarsOnBoards.itemCount;
  for I := 0 to cnt - 1 do
  begin
    radar := simManager.SimRadarsOnBoards.getObject(I) as TT3MountedRadar;
    if radar.PlatformParentInstanceIndex = PlatformParentInstanceIndex then
      continue;

    if not InsideBlindZone(radar) then
      if radar.OperationalStatus = sopOn then
        if EmconOperationalStatus <> EmconOn then
          if InsideRange(radar) then
            if InsideOtherMeasurement(radar) then
              if TargetTypeEligiblity(radar) then
                status := True;

    case radar.OperationalStatus of
      sopOn   : OnSensorDetect(Self,radar,status);
      else OnSensorDetect(Self,radar,false);
    end
  end;

end;

constructor TT3MountedESM.Create;
begin
  inherited;
  FSensorType := stESM;

end;

function TT3MountedESM.getESMDefinition: TDBESM_Definition;
begin
  result := nil;

  if Assigned(FMountedDeviceDefinition) then
    Result := TDBESM_Definition(FMountedDeviceDefinition);

end;

function TT3MountedESM.getESMOnBoardDefinition: TDBESM_On_Board;
begin
  result := nil;

  if Assigned(FMountedDeviceOnBoardDef) then
    Result := TDBESM_On_Board(FMountedDeviceOnBoardDef);
end;

function TT3MountedESM.GetMaxLOSRange(aObject: TSimObject): Double;
var
  basicLOS : Double;
  maxLOS   : Double;
  v, tgt : TT3Vehicle;
  dev : TT3MountedDevice;
  TargetHeight, AntenaHeight,
  ESMAntena, SubESMAntena,
  TgtAntena, TgtSubAntena,
  AtmosRefract, HorizonFactor : Double;
begin
  Result := 0;
  tgt := nil;
  v := nil;

  TargetHeight  := 0;
  TgtAntena    := 0;
  TgtSubAntena := 0;
  HorizonFactor := 1;
  AtmosRefract := 1;

  if PlatformParent is TT3Vehicle then
    v := PlatformParent as TT3Vehicle;

  if not (Assigned(v)) then
    Exit;

  if not (aObject is TT3MountedDevice) then
    Exit;

  dev := aObject as TT3MountedDevice;
  if dev.PlatformParent is TT3Vehicle then
    tgt := dev.PlatformParent as TT3Vehicle;

  if not Assigned(tgt) then
    Exit;

  if  (TT3PlatformInstance(v).PlatformDomain = vhdSubsurface)
      and (TT3PlatformInstance(v).Altitude <> 0) then
    Exit;

  if (TT3PlatformInstance(tgt).PlatformDomain = vhdSubsurface)
      and (TT3PlatformInstance(tgt).Altitude <> 0) then
    Exit;

  if Assigned(ESMDefinition) then
  begin
    ESMAntena := ESMOnBoardDefinition.Rel_Antenna_Height;
    SubESMAntena := ESMOnBoardDefinition.Submerged_Antenna_Height;
  end
  else
  begin
    ESMAntena := 0;
    SubESMAntena := 0;
  end;

  if dev is TT3MountedRadar then
  begin
    if Assigned(TT3MountedRadar(dev).RadarDefinition) then
    begin
      TgtAntena   := TT3MountedRadar(dev).RadarOnBoardDefinition.Rel_Antenna_Height;
      TgtSubAntena:= TT3MountedRadar(dev).RadarOnBoardDefinition.Submerged_Antenna_Height;
      HorizonFactor := TT3MountedRadar(dev).RadarDefinition.Radar_Horizon_Factor;
    end;
  end;

  if (Assigned(tgt)) then
  begin
    if TgtAntena <= 0 then
      TgtAntena := tgt.VehicleDefinition.Height;

    if (tgt.PlatformDomain = vhdAir) then
    begin
      TargetHeight := Sqrt(Abs(17 * (((TgtAntena + TgtSubAntena) * C_Feet_To_Meter) +
                      tgt.Altitude)));
    end
    else
    if (tgt.PlatformDomain = vhdSurface)
        or (tgt.PlatformDomain = vhdLand)
        or (tgt.PlatformDomain = vhdAmphibious) then
    begin
      TargetHeight := Sqrt(Abs(17 * ((TgtAntena + TgtSubAntena) * C_Feet_To_Meter)));
    end
    else
    if (tgt.PlatformDomain = vhdSubsurface) then
    begin
      TargetHeight := Sqrt(Abs(17 * (((TgtAntena + TgtSubAntena) * C_Feet_To_Meter) -
                      tgt.Altitude)));
    end
    else
    begin
      TargetHeight := Sqrt(Abs(17 * (((TgtAntena + TgtSubAntena) * C_Feet_To_Meter) +
                      tgt.Altitude)));
    end;
  end;

  if (Assigned(v)) then
  begin
    if ESMAntena <= 0 then ESMAntena := v.VehicleDefinition.Height;

    if (v.PlatformDomain = vhdAir) then
    begin
      AntenaHeight := Sqrt(Abs(17 * (((ESMAntena + SubESMAntena + v.VehicleDefinition.Height)
                      * C_Feet_To_Meter) + v.Altitude)));
    end
    else
    if (v.PlatformDomain = vhdSurface)
        or (v.PlatformDomain = vhdLand)
        or (v.PlatformDomain = vhdAmphibious) then
    begin
      AntenaHeight := Sqrt(Abs(17 * ((ESMAntena + SubESMAntena + v.VehicleDefinition.Height)
                      * C_Feet_To_Meter)));
    end
    else
    if (v.PlatformDomain = vhdSubsurface) then
    begin
      AntenaHeight := Sqrt(Abs(17 * (((ESMAntena + SubESMAntena + v.VehicleDefinition.Height)
                      * C_Feet_To_Meter) - v.Altitude)));
    end
    else
    begin
      AntenaHeight := Sqrt(Abs(17 * (((ESMAntena + SubESMAntena + v.VehicleDefinition.Height)
                      * C_Feet_To_Meter) + v.Altitude)));
    end;
  end
  else AntenaHeight := Sqrt(Abs(17 * ((ESMAntena + SubESMAntena + v.VehicleDefinition.Height)
                       * C_Feet_To_Meter)));

  if Assigned(simManager.Scenario.DBGameEnvironment) then
  begin
    AtmosRefract := simManager.Scenario.DBGameEnvironment.Atmospheric_Refract_Modifier / 100;
  end;

  basicLOS := AntenaHeight + TargetHeight;
  maxLOS :=  basicLOS * HorizonFactor * AtmosRefract * 1.8;

  Result := maxLOS;
end;

function TT3MountedESM.getVariance: double;
var
  nRandom, randValue : Integer;
  bMin, bMax, percentage : Double;
begin
  // untuk toleransi kesalahan
  // variance diambil dari random antara min_variance dan init_variance

  nRandom := Random(2);
  randValue := Random(100);

  if Assigned(ESMDefinition) then
  begin
    with ESMDefinition do
    begin
      bMin := Minimum_Bearing_Error_Variance;
      bMax := Initial_Bearing_Error_Variance;
    end;
  end
  else
  begin
    bMin := 2;  //def min
    bMax := 45; //def max
  end;

  percentage := bMin + ((randValue / 100) * (bMax - bMin));

  if nRandom = 1 then
    result := -percentage
  else
    result := percentage;

end;

procedure TT3MountedESM.Initialize;
var
  bz            : TT3BlindZone;
  bzList        : TDBBlindZoneDefinitionList;
  dbBz          : TDBBlind_Zone_Definition;
begin
  inherited;

  { set definition once }
  FDeviceParent            := simManager.FindT3PlatformByID(PlatformParentInstanceIndex);
  FMountedDeviceDefinition := simManager.Scenario.GetDBESMDefinition(MountedDeviceDefinitionIndex);
  FMountedDeviceOnBoardDef := simManager.Scenario.
        GetDBESMOnBoardDef(TT3PlatformInstance(FDeviceParent).PlatformDefinitionIndex,
              MountedDeviceOnBoardDefIndex);

  DetectionRange := 500;
  DetailDetectionRange := 500;

  if Assigned(ESMOnBoardDefinition) then
  with ESMOnBoardDefinition do begin
    InstanceName    := Instance_Identifier;
    InstanceIndex   := ESM_Instance_Index;

    { set blind zone }
    bzList := simManager.Scenario.GetDBBlindZones('ESM',ESM_Instance_Index);
    if Assigned(bzList) then
      for dbBz in bzList do
      begin
        bz :=  TT3BlindZone.Create;
        bz.Blind_Zone_Type := dbBz.Blind_Zone_Type;
        bz.BlindZone_Number:= dbBz.BlindZone_Number;
        bz.Start_Angle     := dbBz.Start_Angle;
        bz.End_Angle       := dbBz.End_Angle;

        BlindZones.Add(bz);
      end;
  end;
end;

function TT3MountedESM.InsideBlindZone(aObject: TSimObject): boolean;
var
  i : integer;
  blindZ : TT3BlindZone;
  bearing : double;
  sAngle, eAngle : double;
begin

  bearing := CalcBearing(FPosition.X,FPosition.Y,
           TT3PlatformInstance(aObject).getPositionX,
           TT3PlatformInstance(aObject).getPositionY);

  Result := false;

  // periksa apakah didalam area blind zone
  if BlindZones.Count > 0 then
    for blindZ in BlindZones do begin
      sAngle := blindZ.Start_Angle  + TT3PlatformInstance(PlatformParent).Course;
      eAngle := blindZ.End_Angle    + TT3PlatformInstance(PlatformParent).Course;
      Result := DegComp_IsBeetwen(bearing, sAngle, eAngle);
      if Result then
        Exit;
    end;
end;

function TT3MountedESM.InsideOtherMeasurement(aObject: TSimObject): boolean;
var
  maxLOS   : Double;
  range : Double;
  isSubSurvase : Boolean;
begin
  maxLOS := GetMaxLOSRange(aObject) {* 1000 * C_Meter_To_NauticalMile};
  range := CalcRange(PlatformParent.PosX, PlatformParent.PosY, aObject.PosX, aObject.PosY);

  Result := false;
  isSubSurvase := True;

  if TT3MountedSensor(aObject).PlatformParent is TT3PlatformInstance then
  begin
    if ((TT3MountedSensor(aObject).PlatformParent.PlatformDomain = 2) and
       (TT3MountedSensor(aObject).PlatformParent.Altitude <> 0)) then
      isSubSurvase := False;
  end;

  if range < maxLOS then
  begin
    Result := true and isSubSurvase;
  end;
end;

function TT3MountedESM.InsideRange(aObject: TSimObject): boolean;
var
  maxRange, range : Double;
begin
  inherited;

  maxRange := 0;
  range     := CalcRange(FPosition.X, FPosition.Y, aObject.getPositionX, aObject.getPositionY);
  //diambil dari radar Object yg terdeteksi oleh ESMSensor * 1.8

  if (TT3MountedRadar(aObject).OperationalStatus = sopOn) then
    maxRange := TT3MountedRadar(aObject).DetectionRange * 1.8;

  result := range <= maxRange;
end;

procedure TT3MountedESM.Move(const aDeltaMs: double);
var
  bz : TT3BlindZone;
begin
  inherited;

  for bz in BlindZones do
    bz.Range := DetectionRange;

  // this only run in server
  if machineRole = crServer then
  begin
    if not (OperationalStatus in [sopOff,sopDamage, sopEMCON]) then
      calcESMSensor(aDeltaMs);

    if FDoOnceCalc then
    begin
      calcESMSensor(aDeltaMs);
      FDoOnceCalc := False;
    end;
  end;
end;

procedure TT3MountedESM.SetDetectionRange(const Value: single);
begin
  FDetectionRange := Value;
end;

function TT3MountedESM.TargetTypeEligiblity(aTarget: TObject): boolean;
var
  ParentTarget : TT3Vehicle;
  Radar : TT3MountedRadar;
begin

  Result := True;

  if aTarget is TT3MountedRadar then
  begin
    Radar := TT3MountedRadar(aTarget);
    if Radar.PlatformParent is TT3Vehicle then
    begin
      ParentTarget := TT3Vehicle(Radar.PlatformParent);

      if ParentTarget.VehicleDefinition.Detectability_Type = 1 then  //17042012 mk
      begin
        Result := False;
        Exit;
      end;
    end;

    //cek frekuensi target apakah masuk dalam batas frekuensi deteksi ESM
    if Assigned(Radar.RadarDefinition) then
    begin
      if Assigned(Radar.RadarDefinition) then
      begin
        if not(((ESMDefinition.Low_Detect_Frequency1 < Radar.RadarDefinition.Frequency)
           and (Radar.RadarDefinition.Frequency < ESMDefinition.High_Detect_Frequency1))
           or ((ESMDefinition.Low_Detect_Frequency2 < Radar.RadarDefinition.Frequency)
           and (Radar.RadarDefinition.Frequency < ESMDefinition.High_Detect_Frequency2)))
        then
        begin
          Result := False;
          Exit;
        end;
      end;
    end;
  end;

  result := True;

end;

end.
