unit uT3MountedIFF;

interface

uses uT3MountedSensor, tttData, uDBClassDefinition, uSimObjects;

type
  TIFFType = class
    IFFType : integer; // 1 :transponder, 2 : interrogator
    Data    : TT3MountedSensor;
  end;

  TT3MountedIFF = class (TT3MountedSensor)
  private
    FTransponderMode2: integer;
    FTransponderMode3: integer;
    FTransponderMode2Enabled: boolean;
    FTransponderMode1: integer;
    FTransponderMode3Enabled: boolean;
    FInterrogatorMode2: integer;
    FTransponderMode1Enabled: boolean;
    FInterrogatorMode3: integer;
    FInterrogatorMode2Enabled: boolean;
    FInterrogatorMode1: integer;
    FInterrogatorMode3Enabled: boolean;
    FTransponderMode4Enabled: boolean;
    FTransponderMode3C: integer;
    FInterrogatorMode1Enabled: boolean;
    FTransponderMode3CEnabled: boolean;
    FInterrogatorMode4Enabled: boolean;
    FTransponderOperateStatus: TSensorOperationalStatus;
    FInterrogatorMode3C: integer;
    FInterrogatorMode3CEnabled: boolean;
    FInterrogatorOperateStatus: TSensorOperationalStatus;
    FDetectionRange: single;
    FModeSearchIFF: byte;
    FIFFDetail: Boolean;
    FTargetObject: integer;
    FPrevTargetObj: integer;
    FTransponderMode2Detail: String;
    FTransponderMode3Detail: String;
    FTransponderMode1Detail: String;
    FTransponderMode3CDetail: String;
    FOnIFFDetect: TOnIFFDetect;
    FIFFTransponder: TIFFType;
    FIFFInterrogator: TIFFType;
    procedure SetInterrogatorMode1Enabled(const Value: boolean);
    procedure SetInterrogatorMode2(const Value: integer);
    procedure SetInterrogatorMode2Enabled(const Value: boolean);
    procedure SetInterrogatorMode3(const Value: integer);
    procedure SetInterrogatorMode3C(const Value: integer);
    procedure SetInterrogatorMode3CEnabled(const Value: boolean);
    procedure SetInterrogatorMode3Enabled(const Value: boolean);
    procedure SetInterrogatorMode4Enabled(const Value: boolean);
    procedure SetInterrogatorOperateStatus(
      const Value: TSensorOperationalStatus);
    procedure SetTransponderMode1(const Value: integer);
    procedure SetTransponderMode1Enabled(const Value: boolean);
    procedure SetTransponderMode2(const Value: integer);
    procedure SetTransponderMode2Enabled(const Value: boolean);
    procedure SetTransponderMode3(const Value: integer);
    procedure SetTransponderMode3C(const Value: integer);
    procedure SetTransponderMode3CEnabled(const Value: boolean);
    procedure SetTransponderMode3Enabled(const Value: boolean);
    procedure SetTransponderMode4Enabled(const Value: boolean);
    procedure SetTransponderOperateStatus(
      const Value: TSensorOperationalStatus);
    procedure SetInterrogatorMode1(const Value: integer);
    function getIFFOnBoardDefinition: TDBIFF_Sensor_On_Board;
    procedure SetDetectionRange(const Value: single);
    procedure SetModeSearchIFF(const Value: byte);
    procedure SetIFFStatus(const value: TSensorOperationalStatus);
    procedure CekIFF (var obj : TSimObject; var value : Boolean; var value2 : Boolean);
    procedure SetIFFDetail(const Value: Boolean);
    procedure SetTargetObject(const Value: integer);
    procedure SetTransponderMode1Detail(const Value: String);
    procedure SetTransponderMode2Detail(const Value: String);
    procedure SetTransponderMode3CDetail(const Value: String);
    procedure SetTransponderMode3Detail(const Value: String);
    procedure IFFMode (var Transmode1, Transmode2, Transmode3, Transmode3C : string);
    procedure SetIFFInterrogator(const Value: TIFFType);
    procedure SetIFFTransponder(const Value: TIFFType);
  public
    constructor Create;override;
    destructor Destroy;override;
    procedure Initialize; override;
    procedure Move(const aDeltaMs: double); override;

    // read only prop
    property IFFOnBoardDefinition: TDBIFF_Sensor_On_Board read getIFFOnBoardDefinition;

    property DetectionRange : single read FDetectionRange write SetDetectionRange;
    property TransponderOperateStatus   : TSensorOperationalStatus
        read FTransponderOperateStatus write SetTransponderOperateStatus;
    property TransponderMode1Enabled : boolean read FTransponderMode1Enabled
       write SetTransponderMode1Enabled;
    property TransponderMode2Enabled : boolean read FTransponderMode2Enabled
       write SetTransponderMode2Enabled;
    property TransponderMode3Enabled : boolean read FTransponderMode3Enabled
       write SetTransponderMode3Enabled;
    property TransponderMode3CEnabled : boolean read FTransponderMode3CEnabled
       write SetTransponderMode3CEnabled;
    property TransponderMode4Enabled : boolean read FTransponderMode4Enabled
       write SetTransponderMode4Enabled;
    property TransponderMode1 : integer read FTransponderMode1 write SetTransponderMode1;
    property TransponderMode2 : integer read FTransponderMode2 write SetTransponderMode2;
    property TransponderMode3 : integer read FTransponderMode3 write SetTransponderMode3;
    property TransponderMode3C : integer read FTransponderMode3C write SetTransponderMode3C;

    property InterrogatorOperateStatus  : TSensorOperationalStatus
        read FInterrogatorOperateStatus write SetInterrogatorOperateStatus;
    property InterrogatorMode1Enabled : boolean read FInterrogatorMode1Enabled
       write SetInterrogatorMode1Enabled;
    property InterrogatorMode2Enabled : boolean read FInterrogatorMode2Enabled
       write SetInterrogatorMode2Enabled;
    property InterrogatorMode3Enabled : boolean read FInterrogatorMode3Enabled
       write SetInterrogatorMode3Enabled;
    property InterrogatorMode3CEnabled : boolean read FInterrogatorMode3CEnabled
       write SetInterrogatorMode3CEnabled;
    property InterrogatorMode4Enabled : boolean read FInterrogatorMode4Enabled
       write SetInterrogatorMode4Enabled;
    property InterrogatorMode1 : integer read FInterrogatorMode1 write SetInterrogatorMode1;
    property InterrogatorMode2 : integer read FInterrogatorMode2 write SetInterrogatorMode2;
    property InterrogatorMode3 : integer read FInterrogatorMode3 write SetInterrogatorMode3;
    property InterrogatorMode3C : integer read FInterrogatorMode3C write SetInterrogatorMode3C;

    property ModeSearchIFF    : byte read FModeSearchIFF write SetModeSearchIFF;
    property IFFDetail        : Boolean read FIFFDetail write SetIFFDetail;
    property TargetObjectIdx  : integer read FTargetObject write SetTargetObject;
    property PrevTargetObjIdx : integer read FPrevTargetObj write FPrevTargetObj;

    property TransponderMode1Detail : String read FTransponderMode1Detail write SetTransponderMode1Detail;
    property TransponderMode2Detail : String read FTransponderMode2Detail write SetTransponderMode2Detail;
    property TransponderMode3Detail : String read FTransponderMode3Detail write SetTransponderMode3Detail;
    property TransponderMode3CDetail : String read FTransponderMode3CDetail write SetTransponderMode3CDetail;

    property IFFTransponder  : TIFFType read FIFFTransponder write SetIFFTransponder;
    property IFFInterrogator : TIFFType read FIFFInterrogator write SetIFFInterrogator;

    property OnIFFDetect : TOnIFFDetect read FOnIFFDetect write FOnIFFDetect;
  end;


function OctToDec(OctStr: string): string;
Function DecToOct(Inp : String): String;

implementation

uses SysUtils, math, uGlobalVar, uT3PlatformInstance, uT3Vehicle, uBaseCoordSystem;

function OctToDec(OctStr: string): string;
var
  OctChar : set of AnsiChar;
  DecNum: Real;
  i: Integer;
  Error: Boolean;
begin
  OctChar:= ['0','1','2','3','4','5','6','7'];
  DecNum := 0;
  Error := False;

  for i := Length(OctStr) downto 1 do
  begin
    if not(CharInSet(OctStr[i], OctChar)) then //(OctStr[i] in ['0','1','2','3','4','5','6','7']) then
    begin
      Error := True;
     // ShowMessage('This is not octal number');

      Break;
    end;

    DecNum := DecNum + StrToInt(OctStr[i]) * Power(8, Length(OctStr)-i);
  end;

  if not Error then
    Result := FloatToStr(DecNum)
  else
    Result := '';
end;

Function DecToOct(Inp : String): String;

Var
  HasilBagi,SisaBagi : Integer;
  Oct,Oktal : String;
  i : integer;
  Des : integer;
Begin
  Oct := '';
  Oktal := '';
  Des:= StrToInt(inp);
  Repeat
  SisaBagi := des Mod 8;
  Oct:= Oct + intToStr(SisaBagi);
  HasilBagi := Des Div 8;
  des:= HasilBagi;
  Until HasilBagi = 0;

  For I := length (Oct) Downto 1 Do
  Begin
  Oktal := Oktal+ Oct[i];
  End;
  Result:= Oktal;
End;

{ TT3MountedIFF }

procedure TT3MountedIFF.CekIFF(var obj: TSimObject; var value, value2: Boolean);
var
 i : integer;
 TargetVehicle : TT3Vehicle;
 SensorDevice  : TT3MountedSensor;
 SensorIFF     : TT3MountedIFF;
 isMode1, isMode2, isMode3 : Boolean;
 TargetObject,PrevTargetObj  : TT3PlatformInstance;
begin
  if obj = nil then
  begin
    value := False;
    value2 := false;
    exit;
  end;

  value := False;
  value2 := false;
  //obj := nil;
  IFFDetail :=False;
  isMode1 := False;
  isMode2 := False;
  isMode3 := False;
  //inisialisasi aja biar gak acess volation
  TransponderMode1Detail := '---';
  TransponderMode2Detail := '---';
  TransponderMode3Detail := '---';
  TransponderMode3CDetail := '---';

//  if InterrogatorOperateStatus = sopOn then
//    begin
//      if ModeSearchIFF = 1 then   //designated
//      begin
//        if obj is TT3Vehicle then
//        begin
//          TargetVehicle := TT3Vehicle(obj);
//          for SensorDevice in TargetVehicle.MountedSensors do
//          begin
//
//            if SensorDevice is TT3MountedIFF then
//            begin
//              SensorIFF := TT3MountedIFF(SensorDevice);
//
//              if TargetObjectIdx = 0 then
//                exit;
//
//              TargetObject := queryPlatformInfo(TargetObjectIdx);
//              if (not Assigned(TargetObject)) or (not (TargetObject is TT3Vehicle)) then
//                Exit;
//
//              if (TT3Vehicle(TargetObject).InstanceIndex) <> (TargetVehicle.InstanceIndex) then
//              begin
//                if (SensorIFF.TransponderOperateStatus = sopOn) and (TargetVehicle.FlagIFF)  then
//                begin
//                  value := true;
//                end
//                else
//                begin
//                  value := false;
//                  TargetVehicle.FlagIFF := false;
//                end;
//
//              end
//              else
//              begin
//                if PrevTargetObjIdx <> 0 then
//                begin
//                  PrevTargetObj := queryPlatformInfo(PrevTargetObjIdx);
//                  if (not Assigned(PrevTargetObj)) or
//                     (TT3Vehicle(PrevTargetObj).InstanceIndex = TT3Vehicle(TargetObject).InstanceIndex) then
//                    exit;
//                end;
//
//                if SensorIFF.TransponderOperateStatus = sopon then
//                begin
//                   if InterrogatorMode1Enabled  and
//                      SensorIFF.TransponderMode1Enabled and
//                      ( InterrogatorMode1 = SensorIFF.TransponderMode1) then
//                   begin
//                     TransponderMode1Detail := DecToOct(inttostr(SensorIFF.TransponderMode1));
//                     isMode1 := True;
//                     IFFDetail :=True;
//                   end;
//
//                   if InterrogatorMode2Enabled  and
//                      SensorIFF.TransponderMode2Enabled and
//                      ( InterrogatorMode2 = SensorIFF.TransponderMode2)then
//                   begin
//                     TransponderMode2Detail :=DecToOct(inttostr(SensorIFF.TransponderMode2));
//                     isMode2 := True;
//                     IFFDetail :=True;
//                   end;
//
//                   if InterrogatorMode3Enabled  and
//                      SensorIFF.TransponderMode3Enabled and
//                      ( InterrogatorMode3 = SensorIFF.TransponderMode3)then
//                   begin
//                     TransponderMode3Detail :=DecToOct(inttostr(SensorIFF.TransponderMode3));
//                     isMode3 := True;
//                     IFFDetail :=True;
//                   end;
//
//                   if InterrogatorMode4Enabled and
//                      SensorIFF.TransponderMode4Enabled then
//                   begin
//                     if TT3Vehicle(PlatformParent).ForceDesignation = TargetVehicle.ForceDesignation then
//                       value2 := true;
//                   end;
//
//                   if (isMode1) or (isMode2) or (isMode3) then
//                     value := True;
//
//                   if value then
//                     TargetVehicle.FlagIFF := true
//                   else
//                    TargetVehicle.FlagIFF := false;
//                end
//                else
//                begin
//                  value := false;
//                  TargetVehicle.FlagIFF := false;
//                  PrevTargetObj := TargetObject;
//                end;
//              end
//
//            end;
//          end;
//
//          if InterrogatorMode3CEnabled then
//          begin
//            if TT3PlatformInstance(obj).PlatformDomain = vhdAir then
//              TransponderMode3CDetail := IntToStr(round((TT3PlatformInstance(obj).Altitude * C_Meter_To_Feet)/100));
//          end
//          else
//             TransponderMode3CDetail := '---';
//        end;
//      end
//      else
//      begin
//        if obj is TT3Vehicle then
//        begin
//          TargetVehicle := TT3Vehicle(obj);
//          for SensorDevice in TargetVehicle.MountedSensors do
//          begin
//
//            if SensorDevice is TT3MountedIFF then
//            begin
//              SensorIFF := TT3MountedIFF(SensorDevice);
//
//              if SensorIFF.TransponderOperateStatus = sopon then
//              begin
//
//                if InterrogatorMode1Enabled  and
//                   SensorIFF.TransponderMode1Enabled and
//                  ( InterrogatorMode1 = SensorIFF.TransponderMode1) then
//                begin
//                  TransponderMode1Detail :=DecToOct(inttostr(SensorIFF.TransponderMode1));
//                  isMode1 := True;
//                  IFFDetail :=True;
//                end;
//
//                if InterrogatorMode2Enabled  and
//                   SensorIFF.TransponderMode2Enabled and
//                  ( InterrogatorMode2 = SensorIFF.TransponderMode2)then
//                begin
//                  TransponderMode2Detail :=DecToOct(inttostr(SensorIFF.TransponderMode2));
//                  isMode2 := True;
//                  IFFDetail :=True;
//                end;
//
//                if InterrogatorMode3Enabled  and
//                   SensorIFF.TransponderMode3Enabled and
//                  ( InterrogatorMode3 = SensorIFF.TransponderMode3)then
//                begin
//                  TransponderMode3Detail :=DecToOct(inttostr(SensorIFF.TransponderMode3));
//                  isMode3 := True;
//                  IFFDetail :=True;
//                end;
//
//                if InterrogatorMode4Enabled and
//                   SensorIFF.TransponderMode4Enabled then
//                begin
//                  if TT3Vehicle(PlatformParent).ForceDesignation = TargetVehicle.ForceDesignation then
//                  value2 := true;
//                end;
//
//                if (isMode1) or (isMode2) or (isMode3) then
//                value := True;
//
//                TargetVehicle.FlagIFF := false;// semua set false untuk otomatis
//              end;
//
//            end;
//          end;
//
//          if InterrogatorMode3CEnabled then
//          begin
//            if TT3PlatformInstance(obj).PlatformDomain = vhdAir then
//              TransponderMode3CDetail := IntToStr(round((TT3PlatformInstance(obj).Altitude * C_Meter_To_Feet)/100));
//          end
//          else
//             TransponderMode3CDetail := '---';
//        end;
//      end;
//    end
//  else
//  begin
//    value:=False;
//    IFFDetail :=False;
//  end;

  if OperationalStatus = sopOffIFF then
  begin
    value:=False;
    IFFDetail :=False;
  end;
end;

constructor TT3MountedIFF.Create;
begin
  inherited;
  FSensorType := stIFF;

  IFFTransponder := TIFFType.Create;
  IFFTransponder.IFFType := 1;
  IFFTransponder.Data := Self;

  IFFInterrogator := TIFFType.Create;
  IFFInterrogator.IFFType := 2;
  IFFInterrogator.Data := Self;

end;

destructor TT3MountedIFF.Destroy;
begin

  IFFTransponder.Free;
  IFFInterrogator.Free;

  inherited;
end;

function TT3MountedIFF.getIFFOnBoardDefinition: TDBIFF_Sensor_On_Board;
begin
  Result := nil;

  if Assigned(FMountedDeviceOnBoardDef) then
    Result := TDBIFF_Sensor_On_Board(FMountedDeviceOnBoardDef);

end;

procedure TT3MountedIFF.IFFMode(var Transmode1, Transmode2, Transmode3,
  Transmode3C: string);
begin
  Transmode1:= TransponderMode1Detail;
  Transmode2:= TransponderMode2Detail;
  Transmode3:= TransponderMode3Detail;
  Transmode3C:= TransponderMode3CDetail;
end;

procedure TT3MountedIFF.Initialize;
var I, fd : Integer;
    iffDefault : TDBGame_Default_IFF_Mode_Code;
begin
  inherited;

  TargetObjectIdx := 0;
  PrevTargetObjIdx:= 0;

  { set definition once }
  FDeviceParent            := simManager.FindT3PlatformByID(PlatformParentInstanceIndex);
  FMountedDeviceDefinition := nil;
  FMountedDeviceOnBoardDef := simManager.Scenario.
        GetDBIFFOnBoardDef(TT3PlatformInstance(FDeviceParent).PlatformDefinitionIndex,
              MountedDeviceOnBoardDefIndex);

  if Assigned(IFFOnBoardDefinition) then begin
    InstanceName  := IFFOnBoardDefinition.Instance_Identifier;
    InstanceIndex := IFFOnBoardDefinition.IFF_Instance_Index;
    DetectionRange := 0;

    if IFFOnBoardDefinition.IFF_Capability = 0 then    //bisa automatic bisa manual
      ModeSearchIFF := 0          // set default automatic
    else
      ModeSearchIFF := 1;
  end;

  if simManager.Scenario.DBGameDefault_IFF <> nil then
    for iffDefault in simManager.Scenario.DBGameDefault_IFF do
    begin
      fd := PlatformParent.ForceDesignation;
      if iffDefault.Force_Designation = fd then
      begin
        if iffDefault.IFF_Device_Type = 0 then      //interogator
        begin
          if iffDefault.IFF_Mode = 0 then
          begin
            InterrogatorMode1 := StrToInt(OctToDec(IntToStr(iffDefault.IFF_Code)));
            if iffDefault.Mode_State = 1 then
              InterrogatorMode1Enabled := True
            else
              InterrogatorMode1Enabled := false;
          end
          else if iffDefault.IFF_Mode = 1 then
          begin
            InterrogatorMode2 := StrToInt(OctToDec(IntToStr(iffDefault.IFF_Code)));
            if iffDefault.Mode_State = 1 then
              InterrogatorMode2Enabled := True
            else
              InterrogatorMode2Enabled := false;
          end
          else if iffDefault.IFF_Mode = 2 then
          begin
            InterrogatorMode3 := StrToInt(OctToDec(IntToStr(iffDefault.IFF_Code)));
            if iffDefault.Mode_State = 1 then
              InterrogatorMode3Enabled := True
            else
              InterrogatorMode3Enabled := false;
          end;
        end
        else if iffDefault.IFF_Device_Type = 1 then      //transponder
        begin
          if iffDefault.IFF_Mode = 0 then
          begin
            TransponderMode1 := StrToInt(OctToDec(IntToStr(iffDefault.IFF_Code)));
            if iffDefault.Mode_State = 1 then
              TransponderMode1Enabled := True
            else
              TransponderMode1Enabled := False;
          end
          else if iffDefault.IFF_Mode = 1 then
          begin
            TransponderMode2 := StrToInt(OctToDec(IntToStr(iffDefault.IFF_Code)));
            if iffDefault.Mode_State = 1 then
              TransponderMode2Enabled := True
            else
              TransponderMode2Enabled := False;
          end
          else if iffDefault.IFF_Mode = 2 then
          begin
            TransponderMode3 := StrToInt(OctToDec(IntToStr(iffDefault.IFF_Code)));
            if iffDefault.Mode_State = 1 then
              TransponderMode3Enabled := True
            else
              TransponderMode3Enabled := False;
          end;
        end
      end;
    end;

end;

procedure TT3MountedIFF.Move(const aDeltaMs: double);
var
  Transponder1Target, Transponder2Target, Transponder3Target,
  Transponder3CTarget : string;
  IFFtarget : TSimObject;
  //IsOn : Boolean;
  RangeIFF : double;
  iscekIFF, isMode4 : Boolean;
  pfObject : TT3PlatformInstance;
  ix : integer;
  j : integer;
begin
  inherited;

  { if role as server }
  if machineRole = crServer then
  begin
    j := simManager.SimPlatforms.itemCount ;

    for ix := 0 to j - 1 do
    begin
      pfObject := TT3PlatformInstance(simManager.SimPlatforms.getObject(ix));

      if pfObject.InstanceIndex = PlatformParentInstanceIndex then
      begin
        continue;
      end;

      IFFtarget := pfObject;
      CekIFF(IFFtarget, iscekIFF, isMode4);
      IFFMode(Transponder1Target, Transponder2Target, Transponder3Target,
              Transponder3CTarget);

      if IFFtarget = nil then begin
        IFFtarget := pfObject;
      end;

      if ( IFFtarget <> nil ) and ( PlatformParentInstanceIndex <> 0 )  then
      begin
        //do something
        if (TT3PlatformInstance(IFFtarget).PlatformDomain = vhdSubsurface) and
          (TT3PlatformInstance(IFFtarget).Altitude > 0) then
        begin
          iscekIFF := False;
        end;

        RangeIFF := CalcRange(PlatformParent.getPositionX, PlatformParent.getPositionY,
                               IFFtarget.getPositionX, IFFtarget.getPositionY);

        if RangeIFF > simManager.Scenario.DBGameDefaults.Max_IFF_Range then begin
          iscekIFF := false;
        end;

        if EmconOperationalStatus = EmconOn then begin
          iscekIFF := false;
        end;

        if Assigned(OnIFFDetect) then begin
          OnIFFDetect(Self, IFFtarget, iscekIFF, isMode4, Transponder1Target,
                      Transponder2Target, Transponder3Target, Transponder3CTarget);
        end;
      end;
    end;
  end;
end;

procedure TT3MountedIFF.SetDetectionRange(const Value: single);
begin
  FDetectionRange := Value;
end;

procedure TT3MountedIFF.SetIFFDetail(const Value: Boolean);
begin
  FIFFDetail := Value;
end;

procedure TT3MountedIFF.SetIFFInterrogator(const Value: TIFFType);
begin
  FIFFInterrogator := Value;
end;

procedure TT3MountedIFF.SetIFFStatus(const value: TSensorOperationalStatus);
begin
  FOperationalStatus := value;
end;

procedure TT3MountedIFF.SetIFFTransponder(const Value: TIFFType);
begin
  FIFFTransponder := Value;
end;

procedure TT3MountedIFF.SetInterrogatorMode1(const Value: integer);
begin
  FInterrogatorMode1 := Value;
end;

procedure TT3MountedIFF.SetInterrogatorMode1Enabled(const Value: boolean);
begin
  FInterrogatorMode1Enabled := Value;
end;

procedure TT3MountedIFF.SetInterrogatorMode2(const Value: integer);
begin
  FInterrogatorMode2 := Value;
end;

procedure TT3MountedIFF.SetInterrogatorMode2Enabled(const Value: boolean);
begin
  FInterrogatorMode2Enabled := Value;
end;

procedure TT3MountedIFF.SetInterrogatorMode3(const Value: integer);
begin
  FInterrogatorMode3 := Value;
end;

procedure TT3MountedIFF.SetInterrogatorMode3C(const Value: integer);
begin
  FInterrogatorMode3C := Value;
end;

procedure TT3MountedIFF.SetInterrogatorMode3CEnabled(const Value: boolean);
begin
  FInterrogatorMode3CEnabled := Value;
end;

procedure TT3MountedIFF.SetInterrogatorMode3Enabled(const Value: boolean);
begin
  FInterrogatorMode3Enabled := Value;
end;

procedure TT3MountedIFF.SetInterrogatorMode4Enabled(const Value: boolean);
begin
  FInterrogatorMode4Enabled := Value;
end;

procedure TT3MountedIFF.SetInterrogatorOperateStatus(
  const Value: TSensorOperationalStatus);
begin
  FInterrogatorOperateStatus := Value;

  if (FInterrogatorOperateStatus = sopOn) or  (FTransponderOperateStatus = sopOn) then
    SetIFFStatus(sopOn)
  else if  (FInterrogatorOperateStatus = sopOffIFF) and  (FTransponderOperateStatus = sopOffIFF) then
    SetIFFStatus(sopOffIFF)
  else
    SetIFFStatus(Value);

//  if Assigned(FOnInterrogatorStatus) then
//    FOnInterrogatorStatus(Self,Value);
end;

procedure TT3MountedIFF.SetModeSearchIFF(const Value: byte);
begin
  FModeSearchIFF := Value;
end;

procedure TT3MountedIFF.SetTargetObject(const Value: integer);
begin
  FTargetObject := Value;
end;

procedure TT3MountedIFF.SetTransponderMode1(const Value: integer);
begin
  FTransponderMode1 := Value;
end;

procedure TT3MountedIFF.SetTransponderMode1Detail(const Value: String);
begin
  FTransponderMode1Detail := Value;
end;

procedure TT3MountedIFF.SetTransponderMode1Enabled(const Value: boolean);
begin
  FTransponderMode1Enabled := Value;
end;

procedure TT3MountedIFF.SetTransponderMode2(const Value: integer);
begin
  FTransponderMode2 := Value;
end;

procedure TT3MountedIFF.SetTransponderMode2Detail(const Value: String);
begin
  FTransponderMode2Detail := Value;
end;

procedure TT3MountedIFF.SetTransponderMode2Enabled(const Value: boolean);
begin
  FTransponderMode2Enabled := Value;
end;

procedure TT3MountedIFF.SetTransponderMode3(const Value: integer);
begin
  FTransponderMode3 := Value;
end;

procedure TT3MountedIFF.SetTransponderMode3C(const Value: integer);
begin
  FTransponderMode3C := Value;
end;

procedure TT3MountedIFF.SetTransponderMode3CDetail(const Value: String);
begin
  FTransponderMode3CDetail := Value;
end;

procedure TT3MountedIFF.SetTransponderMode3CEnabled(const Value: boolean);
begin
  FTransponderMode3CEnabled := Value;
end;

procedure TT3MountedIFF.SetTransponderMode3Detail(const Value: String);
begin
  FTransponderMode3Detail := Value;
end;

procedure TT3MountedIFF.SetTransponderMode3Enabled(const Value: boolean);
begin
  FTransponderMode3Enabled := Value;
end;

procedure TT3MountedIFF.SetTransponderMode4Enabled(const Value: boolean);
begin
  FTransponderMode4Enabled := Value;
end;

procedure TT3MountedIFF.SetTransponderOperateStatus(
  const Value: TSensorOperationalStatus);
begin
  FTransponderOperateStatus := Value;

  if (FInterrogatorOperateStatus = sopOn) or  (FTransponderOperateStatus = sopOn) then
    SetIFFStatus(sopOn)
  else if  (FInterrogatorOperateStatus = sopOffIFF) and  (FTransponderOperateStatus = sopOffIFF) then
    SetIFFStatus(sopOffIFF)
  else if (FInterrogatorOperateStatus = sopDamage) or  (FTransponderOperateStatus = sopDamage) then
    SetIFFStatus(sopDamage)
  else
    SetIFFStatus(Value);

//  if Assigned(FOnTransponderStatus) then
//    FOnTransponderStatus(Self, Value);
end;

end.
