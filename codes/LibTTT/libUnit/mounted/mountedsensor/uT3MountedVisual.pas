unit uT3MountedVisual;

interface

uses uT3MountedSensor, uDBClassDefinition, uT3BlindZone,
  uSimObjects, uBaseCoordSystem, uT3Vehicle, uT3PlatformInstance, tttData,
  SysUtils, Classes;

type
  TT3MountedVisual = class(TT3MountedSensor)
  private
    FOnRequestGameTime: TNotifyEvent;
    FGameTime: TDateTime;
    function getVisualOnBoardDefinition: TDBVisual_Sensor_On_Board;
    function getDetectionRange: double;
    procedure getGameTime;
    procedure calcVisualSensor;
    function GetMaxLOSRange(aObject: TSimObject): double;
    function SecondToTime(const s: Integer): TTime;
    procedure SetOnRequestGameTime(const Value: TNotifyEvent);
  protected
    function InsideRange(aObject: TSimObject): boolean; override;
    function TargetTypeEligiblity(aTarget: TObject): boolean;
    function InsideBlindZone(aObject: TSimObject): boolean; override;
    function InsideOtherMeasurement(aObject: TSimObject): boolean; override;
    function ParentSensor(aObject: TSimObject): boolean;
  public

    constructor Create; override;
    procedure Initialize; override;
    procedure Move(const aDeltaMs: double); override;
    procedure SetGameTime(gt: TDateTime);

    // read only prop
    property VisualOnBoardDefinition: TDBVisual_Sensor_On_Board read getVisualOnBoardDefinition;
    property DetectionRange: double read getDetectionRange;

    property OnRequestGameTime: TNotifyEvent read FOnRequestGameTime write
      SetOnRequestGameTime;
  end;

implementation

uses
  uGlobalVar;

{ TT3MountedVisual }

procedure TT3MountedVisual.calcVisualSensor;
var
  i: Integer;
  pfObject: TT3PlatformInstance;
  allState: boolean;
begin
  { search object }
  if Assigned(simManager.SimPlatforms) then
  begin
    for i := 0 to simManager.SimPlatforms.ItemCount - 1 do
    begin
      pfObject := TT3PlatformInstance(simManager.SimPlatforms.getObject(i));

      if pfObject.InstanceIndex =  PlatformParentInstanceIndex then
        Continue;

      // periksa semua factor deteksi
      allState := InsideRange(pfObject)
        and not InsideBlindZone(pfObject)
        and InsideOtherMeasurement(pfObject)
        and TargetTypeEligiblity(pfObject)
        and ParentSensor(pfObject);

      case pfObject.DetectabilityType of
        dtNormalDetection:
          begin
            if Assigned(OnSensorDetect) then
              OnSensorDetect(Self, pfObject, allState);
          end;
        dtUndetectable:
          begin
            if Assigned(OnSensorDetect) then
              OnSensorDetect(Self, pfObject, False);
          end;
        dtPassiveDetection:
          begin
            if OperationalStatus = sopPassive then
            begin
              if Assigned(OnSensorDetect) then
                OnSensorDetect(Self, pfObject, True);
            end
            else
            begin
              if Assigned(OnSensorDetect) then
                OnSensorDetect(Self, pfObject, False);
            end;
          end;
        dtAlwaysVisible:
          begin
            if Assigned(OnSensorDetect) then
              OnSensorDetect(Self, pfObject, True);
          end;
      end;
    end;

  end;

end;

constructor TT3MountedVisual.Create;
begin
  inherited;
  FSensorType := stVisual;

end;

function TT3MountedVisual.getDetectionRange: double;
var
  visualSensor, daytime, nighttime, sunrise, sunset: double;
  riseHour, riseMin, riseSec, riseMilli: Word;
  setHour, setMin, setSec, setMilli: Word;
  gtHour, gtMin, gtSec, gtMili: Word;
  sun_rise, sun_set: TTime;
begin
  if Assigned(simManager.Scenario.DBGameDefaults) then
  begin
    result := simManager.Scenario.DBGameDefaults.Max_Visual_Range;

    if result = 0 then
    begin
      result := 12;
    end;
  end
  else
  begin
    result := 12;
  end;
//
//  { update det range to environment effect }
//  if Assigned(Environment) then
//  begin
//    visualSensor := 1;
//    Environment.getVisual(daytime, nighttime, sunrise, sunset);
//
//    sun_rise := SecondToTime(Round(sunrise));
//    sun_set := SecondToTime(Round(sunset));
//
//    DecodeTime(sun_rise, riseHour, riseMin, riseSec, riseMilli);
//    DecodeTime(sun_set, setHour, setMin, setSec, setMilli);
//
//    getGameTime;
//    DecodeTime(FGameTime, gtHour, gtMin, gtSec, gtMili);
//
//    if (gtHour >= riseHour) and (gtHour < setHour) then
//      visualSensor := daytime / 100;
//    if (gtHour >= setHour) and (gtHour < riseHour) then
//      visualSensor := nighttime / 100;
//
//    result := result * visualSensor;
//  end;

end;

procedure TT3MountedVisual.getGameTime;
begin
  if Assigned(FOnRequestGameTime) then
    FOnRequestGameTime(Self);
end;

function TT3MountedVisual.GetMaxLOSRange(aObject: TSimObject): double;
var
  basicLOS: double;
  TargetHeight, AntenaHeight, VisuaObsHeight: double;
  v: TT3Vehicle;
begin
  v := TT3Vehicle(PlatformParent);

  result := 0;
  TargetHeight := 0;

  if not(Assigned(v)) then
    Exit;

  if not(Assigned(aObject)) then
    Exit;

  if not(v is TT3Vehicle) then
    Exit;

  if not(aObject is TT3PlatformInstance) then
    Exit;

  if (TT3PlatformInstance(v).PlatformDomain = vhdSubsurface) and
    (TT3PlatformInstance(v).Altitude <> 0) then
    Exit;

  if (TT3PlatformInstance(aObject).PlatformDomain = vhdSubsurface) and
    (TT3PlatformInstance(aObject).Altitude <> 0) then
    Exit;

  if Assigned(VisualOnBoardDefinition) then
    VisuaObsHeight := VisualOnBoardDefinition.Observer_Height
  else
    VisuaObsHeight := 0;

  if aObject is TT3Vehicle then
  begin
    if (TT3Vehicle(aObject).PlatformDomain = vhdAir) then
    begin
      TargetHeight := Sqrt
        (Abs(13 * ((TT3Vehicle(aObject).VehicleDefinition.Height *
                C_Feet_To_Meter) + TT3Vehicle(aObject).Altitude)));
    end
    else if (TT3Vehicle(aObject).PlatformDomain = vhdSurface) or
      (TT3Vehicle(aObject).PlatformDomain = vhdLand) or
      (TT3Vehicle(aObject).PlatformDomain = vhdAmphibious) then
    begin
      TargetHeight := Sqrt
        (Abs(13 * (TT3Vehicle(aObject).VehicleDefinition.Height *
              C_Feet_To_Meter)));
    end
    else if (TT3Vehicle(aObject).PlatformDomain = vhdSubsurface) then
    begin
      TargetHeight := Sqrt
        (Abs(13 * ((TT3Vehicle(aObject).VehicleDefinition.Height *
                C_Feet_To_Meter) - TT3Vehicle(aObject).Altitude)));
    end
    else
    begin
      TargetHeight := Sqrt
        (Abs(13 * ((TT3Vehicle(aObject).VehicleDefinition.Height *
                C_Feet_To_Meter) + TT3Vehicle(aObject).Altitude)));
    end;
  end
  else
  begin
    if aObject is TT3PlatformInstance then
      TargetHeight := Sqrt(Abs(13 * TT3PlatformInstance(aObject).Altitude));
  end;

  if (Assigned(v)) then
  begin
    if (v.PlatformDomain = vhdAir) then
    begin
      AntenaHeight := Sqrt
        (Abs(13 * (((VisuaObsHeight + v.VehicleDefinition.Height)
                * C_Feet_To_Meter) + v.Altitude)));
    end
    else if (v.PlatformDomain = vhdSurface) or (v.PlatformDomain = vhdLand) or
      (v.PlatformDomain = vhdAmphibious) then
    begin
      AntenaHeight := Sqrt
        (Abs(13 * ((VisuaObsHeight + v.VehicleDefinition.Height)
              * C_Feet_To_Meter)));
    end
    else if (v.PlatformDomain = vhdSubsurface) then
    begin
      AntenaHeight := Sqrt
        (Abs(13 * (((VisuaObsHeight + v.VehicleDefinition.Height)
                * C_Feet_To_Meter) - v.Altitude)));
    end
    else
    begin
      AntenaHeight := Sqrt
        (Abs(13 * (((VisuaObsHeight + v.VehicleDefinition.Height)
                * C_Feet_To_Meter) + v.Altitude)));
    end;
  end
  else
    AntenaHeight := Sqrt(Abs(13 * (VisuaObsHeight * C_Feet_To_Meter)));

  basicLOS := TargetHeight + AntenaHeight;

  result := basicLOS;

end;

function TT3MountedVisual.getVisualOnBoardDefinition: TDBVisual_Sensor_On_Board;
begin
  result := nil;

  if Assigned(FMountedDeviceOnBoardDef) and
    (FMountedDeviceOnBoardDef is TDBVisual_Sensor_On_Board) then
    result := TDBVisual_Sensor_On_Board(FMountedDeviceOnBoardDef);
end;

procedure TT3MountedVisual.Initialize;
var
  i: Integer;
  bz : TT3BlindZone;
  bzList : TDBBlindZoneDefinitionList;
  dbBz : TDBBlind_Zone_Definition;
begin
  inherited;

  { set definition once }
  FDeviceParent            := simManager.FindT3PlatformByID(PlatformParentInstanceIndex);
  FMountedDeviceDefinition := nil;
  FMountedDeviceOnBoardDef := simManager.Scenario.
        GetDBVisualOnBoardDef(TT3PlatformInstance(FDeviceParent).PlatformDefinitionIndex,
              MountedDeviceOnBoardDefIndex);

  OperationalStatus := sopOn;

  if Assigned(VisualOnBoardDefinition) then
  with VisualOnBoardDefinition do
  begin
    InstanceName  := Instance_Identifier;
    InstanceIndex := Visual_Instance_Index;

    { set blind zone }
    bzList := simManager.Scenario.GetDBBlindZones('Visual',Visual_Instance_Index);
    if Assigned(bzList) then
      for dbBz in bzList do
      begin
        bz :=  TT3BlindZone.Create;
        bz.Blind_Zone_Type := dbBz.Blind_Zone_Type;
        bz.BlindZone_Number:= dbBz.BlindZone_Number;
        bz.Start_Angle     := dbBz.Start_Angle;
        bz.End_Angle       := dbBz.End_Angle;

        BlindZones.Add(bz);
      end;
  end;
end;

function TT3MountedVisual.InsideBlindZone(aObject: TSimObject): boolean;
var
  i: Integer;
  blindZ: TT3BlindZone;
  bearing: double;
  sAngle, eAngle: double;
begin

  bearing := CalcBearing(FPosition.X, FPosition.Y, TT3PlatformInstance(aObject)
      .getPositionX, TT3PlatformInstance(aObject).getPositionY);

  result := False;

  // periksa apakah didalam area blind zone
  if BlindZones.Count > 0 then
    for blindZ in BlindZones do
    begin
      sAngle := blindZ.Start_Angle + TT3PlatformInstance(PlatformParent).Course;
      eAngle := blindZ.End_Angle + TT3PlatformInstance(PlatformParent).Course;
      result := DegComp_IsBeetwen(bearing, sAngle, eAngle);
      if result then
        Exit;
    end;
end;

function TT3MountedVisual.InsideOtherMeasurement(aObject: TSimObject): boolean;
var
  InsideDomain: boolean;
  maxLOS, range: double;
begin
  result := True;
  InsideDomain := True;

  if not(Assigned(aObject)) then // mk 19042012
    Exit;

  if InsideDomain then
  begin
    InsideDomain := False;

    maxLOS := GetMaxLOSRange(aObject); { * C_Meter_To_NauticalMile }
    range := CalcRange(PlatformParent.PosX, PlatformParent.PosY, aObject.PosX,
      aObject.PosY);

    if range < maxLOS then
      InsideDomain := True;
  end;

  result := { InsideHorizon(aObject) and } InsideDomain;
end;

function TT3MountedVisual.InsideRange(aObject: TSimObject): boolean;
var
  range: double;
begin
  inherited;
  range := CalcRange(FPosition.X, FPosition.Y, aObject.getPositionX,
    aObject.getPositionY);

  result := range <= DetectionRange;
end;

procedure TT3MountedVisual.Move(const aDeltaMs: double);
var
  bz : TT3BlindZone;
begin
  inherited;

  for bz in BlindZones do
    bz.Range := DetectionRange;

  if machineRole = crServer then
    calcVisualSensor;
end;

function TT3MountedVisual.ParentSensor(aObject: TSimObject): boolean;
var
  InsideDomain: boolean;
begin
  if (TT3PlatformInstance(aObject).PlatformDomain = 2) then
  begin
    if TT3PlatformInstance(aObject).Altitude <> 0 then
      InsideDomain := False
    else
      InsideDomain := True;
  end
  else
    InsideDomain := True;

  result := InsideDomain;
end;

function TT3MountedVisual.SecondToTime(const s: Integer): TTime;
var
  h: double;
begin
  h := s / 3600;
  result := h / 24;
end;

procedure TT3MountedVisual.SetGameTime(gt: TDateTime);
begin
  FGameTime := gt;
end;

procedure TT3MountedVisual.SetOnRequestGameTime(const Value: TNotifyEvent);
begin
  FOnRequestGameTime := Value;
end;

function TT3MountedVisual.TargetTypeEligiblity(aTarget: TObject): boolean;
begin
  result := True;

  if aTarget is TT3Vehicle then
  begin
    if TT3Vehicle(aTarget).VehicleDefinition.Detectability_Type = 1 then  //17042012 mk
      Result := False;
  end;

end;

end.
