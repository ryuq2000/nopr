unit uT3MountedSensor;

interface

uses
  uT3MountedDevice, uT3BlindZone, tttData, Generics.Collections,
  uSimObjects, uT3SimContainer, Classes,SysUtils;

type
  TT3MountedSensor = class abstract (TT3MountedDevice)
  private

    FBlindZones : TList<TT3BlindZone>;

    FShowRange                 : boolean;
    FOperatingMode             : TSonarOperatingMode;
    FEmconOperationalStatus    : TEMCON_OperasionalStatus;
    FShowBlindZone             : boolean;
    FOnSensorDetect            : TOnSensorDetect;
    FOnSensorOperationalStatus : TOnSensorOperationalStatus;
    FOnSensorRemoved           : TNotifyEvent;
    FShowBlindSelected         : Boolean;
    FShowRangeSelected         : Boolean;
    FDate_Time                 : TDateTime;

    procedure SetShowRange(const Value: boolean);
    procedure SetOperatingMode(const Value: TSonarOperatingMode);
    procedure SetEmconOperationalStatus(const Value: TEMCON_OperasionalStatus);
    procedure SetShowBlindZone(const Value: boolean);
    procedure SetOnSensorDetect(const Value: TOnSensorDetect);
    procedure SetOnSensorOperationalStatus(
      const Value: TOnSensorOperationalStatus);
    procedure SetOnSensorRemoved(const Value: TNotifyEvent);
    procedure SetShowBlindSelected(const Value: Boolean);
    procedure SetShowRangeSelected(const Value: Boolean);
    procedure SetDateTime(const Value: TDateTime);

  protected
    FOperationalStatus: TSensorOperationalStatus;
    FDoOnceCalc       : Boolean;
    FSensorType       : TESensorType;

    function  InsideBlindZone(aObject : TSimObject) : boolean; virtual;
    function  InsideVerticalCoverage(aObject: TSimObject): boolean; virtual;
    function  InsideOtherMeasurement(aObject : TSimObject) : boolean; virtual;
    function  InsideRange(aObject : TSimObject) : boolean; virtual;
    function  InsideJammedEffect : Boolean; virtual;
    procedure SetOperationalStatus(const Value: TSensorOperationalStatus);virtual;
    procedure UpdateVisualData;override;

  public
    constructor Create; override;
    destructor Destroy;override;

    procedure Move(const aDeltaMs: double); override;

    property BlindZones : TList<TT3BlindZone> read FBlindZones;

    property ShowBlindZone : boolean read FShowBlindZone write SetShowBlindZone;
    property ShowRange : boolean read FShowRange write SetShowRange;
    property ShowRangeSelected : Boolean read FShowRangeSelected write SetShowRangeSelected;
    property ShowBlindSelected : Boolean read FShowBlindSelected write SetShowBlindSelected;
    property SensorTime : TDateTime read FDate_Time write SetDateTime;
    property SensorType : TESensorType read FSensorType;

    property OperationalStatus  : TSensorOperationalStatus  read FOperationalStatus write SetOperationalStatus;
    property OperatingMode      : TSonarOperatingMode read FOperatingMode write SetOperatingMode;
    property EmconOperationalStatus : TEMCON_OperasionalStatus read FEmconOperationalStatus write SetEmconOperationalStatus;

    { object event }
    property OnSensorOperationalStatus : TOnSensorOperationalStatus
      read FOnSensorOperationalStatus write SetOnSensorOperationalStatus;
    property OnSensorDetect   : TOnSensorDetect read FOnSensorDetect
      write SetOnSensorDetect;
    property OnSensorRemoved  : TNotifyEvent read FOnSensorRemoved
      write SetOnSensorRemoved;
  end;

implementation

{ TT3MountedSensor }

constructor TT3MountedSensor.Create;
begin
  inherited;
  FShowBlindZone  := false;
  FShowRange      := false;
  FSensorType     := stSonar;

  FOperationalStatus  := sopOff;
  FEmconOperationalStatus := EmconOff;

  FBlindZones := TList<TT3BlindZone>.Create;

end;

destructor TT3MountedSensor.Destroy;
var
  bz  : TT3BlindZone;
begin

  for bz in FBlindZones do
    bz.Free;

  FBlindZones.CLear;
  FBlindZones.Free;

  inherited;
end;

function TT3MountedSensor.InsideBlindZone(aObject: TSimObject): boolean;
begin
  Result := False;
end;

function TT3MountedSensor.InsideJammedEffect: Boolean;
begin
  Result := False;
end;

function TT3MountedSensor.InsideOtherMeasurement(aObject: TSimObject): boolean;
begin
  Result := False;
end;

function TT3MountedSensor.InsideRange(aObject: TSimObject): boolean;
begin
  Result := False;
end;

function TT3MountedSensor.InsideVerticalCoverage(aObject: TSimObject): boolean;
begin
  Result := False;
end;

procedure TT3MountedSensor.Move(const aDeltaMs: double);
var
  bz : TT3BlindZone;
begin
  inherited;

  for bz in BlindZones do
  begin
    bz.PosX := FPosition.X;
    bz.PosY := FPosition.Y;
    bz.PosZ := FPosition.Z;

    if Assigned( PlatformParent ) then
      bz.Heading := PlatformParent.Course;
  end;

end;

procedure TT3MountedSensor.SetDateTime(const Value: TDateTime);
begin
  FDate_Time := Value;
end;

procedure TT3MountedSensor.SetEmconOperationalStatus(
  const Value: TEMCON_OperasionalStatus);
begin
  FEmconOperationalStatus := Value;
end;


procedure TT3MountedSensor.SetOnSensorDetect(const Value: TOnSensorDetect);
begin
  FOnSensorDetect := Value;
end;

procedure TT3MountedSensor.SetOnSensorOperationalStatus(
  const Value: TOnSensorOperationalStatus);
begin
  FOnSensorOperationalStatus := Value;
end;

procedure TT3MountedSensor.SetOnSensorRemoved(const Value: TNotifyEvent);
begin
  FOnSensorRemoved := Value;
end;

procedure TT3MountedSensor.SetOperatingMode(const Value: TSonarOperatingMode);
begin
  FOperatingMode := Value;
end;

procedure TT3MountedSensor.SetOperationalStatus(
  const Value: TSensorOperationalStatus);
begin

  if Value <> FOperationalStatus  then
    if Assigned(FOnSensorOperationalStatus) then
      FOnSensorOperationalStatus(Self,Value);

  FOperationalStatus := Value;

  { do once sensor calc before off, damage, emcon}
  if FOperationalStatus in [sopOff,sopDamage,sopEMCON] then
    FDoOnceCalc := True;
end;

procedure TT3MountedSensor.SetShowBlindSelected(const Value: Boolean);
begin
  FShowBlindSelected := Value;
end;

procedure TT3MountedSensor.SetShowBlindZone(const Value: boolean);
var
  bz : TT3BlindZone;
begin
  FShowBlindZone := Value;

  for bz in FBlindZones do
    bz.Visible := Value;

end;

procedure TT3MountedSensor.SetShowRange(const Value: boolean);
begin
  FShowRange := Value;
end;

procedure TT3MountedSensor.SetShowRangeSelected(const Value: Boolean);
begin
  FShowRangeSelected := Value;
end;

procedure TT3MountedSensor.UpdateVisualData;
  function SecondToTime(const s: Integer): TTime;
  var
    h: Double;
  begin
    h := s / 3600;
    Result := h / 24;
  end;
var
  i : integer;
  visualSensor, daytime, nighttime, sunrise, sunset : Double;
  riseHour, riseMin, riseSec, riseMilli : Word;
  setHour, setMin, setSec, setMilli : Word;
  sun_rise, sun_set : TTime;
begin
  inherited;
  visualSensor := 1;

  //get visual
//  Environment.getVisual(daytime, nighttime, sunrise, sunset);

  sun_rise := SecondToTime(Round(sunrise));
  sun_set  := SecondToTime(Round(sunset));

  DecodeTime(sun_rise, riseHour, riseMin, riseSec, riseMilli);
  DecodeTime(sun_set, setHour, setMin, setSec, setMilli);


//  if (FmyHour >= riseHour) and (FmyHour < setHour) then
//     visualSensor := daytime/100;
//  if (FmyHour >= setHour) and (FmyHour < riseHour) then
//     visualSensor := nighttime/100;
//
//    FRangeView.Range  := FDetectionRange*visualSensor;
//    FRangeView.mX     := FParent.getPositionX;
//    FRangeView.mY     := FParent.getPositionY;
//    FRangeView.ConvertCoord(Converter);
//
//    FRangeWithVisualEffect := FDetectionRange * visualSensor;
//
//    for i := 0 to FBlindZoneViews.Count - 1 do begin
//      blindZ := FBlindZoneViews.Items[i];
//
//      with blindZ do begin
//        //z := TT3PlatformInstance(FParent).InstanceName;
//        Heading:= TT3PlatformInstance(FParent).Course;
//        Ranges := FDetectionRange;
//        mX     := FParent.getPositionX;
//        mY     := FParent.getPositionY;
//
//        StartAngle := BZStartAngle[i];
//        EndAngle   := BZEndAngle[i];
//
//        ConvertCoord(Self.Converter);
//      end;
//    end;
//  end;

end;

end.
