unit uT3MountedSonar;

interface

uses uT3MountedSensor, tttData, uT3BlindZone,
  uT3SonarCD, uSimObjects, uT3PlatformInstance,
  uT3Vehicle, uBaseCoordSystem, Math, uT3Common, uDBClassDefinition,
  uT3MountedMissile;

type
  TSonarInfo = record
    CheckDeploymentAndOperatingStatusOK: Boolean;
    ScanProcessingOK: Boolean;
    InsideSwipeAzimuthSector: Boolean;
    OutsideBlindZone: Boolean;
    DetermineTargetsAndMaximumRangeOK: Boolean;
    TargetEligible: Boolean;
    OperatingModeValid: Boolean;
    TargetInsideRange: Boolean;
    PhysicalProcessingOK: Boolean;
    OperatingDepthValid: Boolean;
    DeploySpeedValid: Boolean;
    TASOceanDepthValid: Boolean;
    TASDeploySpeedValid: Boolean;
    TASUnkinked: Boolean;
    SubAreaBoundaryProcessingOK: Boolean;
    BoundaryCrossingCount: Integer;
    SonarSNR: Double;
    SonarPOD: Double;
    RandomValue: Double;
    TargetDetected: Boolean;
  end;

  TOnDeploymentStatusChange = procedure(aSender: TObject) of object;

  TT3MountedSonar = class(TT3MountedSensor)
  private
    FSNRData                  : TT3Sonar_Curve_Def;
    FSonarCategory            : TSonarCategory;

    FActualCable              : Double;
    FOrderCable               : Double;
    FDepthCable               : Double;

    FTIOWRange                : TSonarTIOWRange;
    FControlMode              : TSonarControlMode;
    FDeploymentStatus         : TSonarDeploymentStatus;
    FOperatingMode            : TSonarOperatingMode;

    FOnDeploymentStatusChange : TOnDeploymentStatusChange;
    FOnControlMode            : TOnSonarControlMode;

    FSettleKinkTime           : Double;
    FSonarInfo                : TSonarInfo;
    FDetectionRange           : single;

    // setter
    procedure SetSonarCategory(const Value: TSonarCategory);
    procedure SetTIOWRange(const Value: TSonarTIOWRange);
    procedure SetControlMode(const Value: TSonarControlMode);
    procedure SetDeploymentStatus(const Value: TSonarDeploymentStatus);
    procedure SetOperatingMode(const Value: TSonarOperatingMode);
    procedure SetOnControlMode(const Value: TOnSonarControlMode);
    procedure SetOnDeploymentStatusChange
      (const Value: TOnDeploymentStatusChange);

    // getter
    function getSonarDefinition: TDBSonar_Definition;
    function getSonarOnBoardDefinition: TDBSonar_On_Board;
    function getDetectionRange: Double;

    function MaximumRange(aSonarMode: TSonarControlMode): Double;

    procedure calcSonarSensor(aDeltaT: Double);
    procedure SonarInactive;

    function SonarProcess(aDeltaT: Double; aShip: TSimObject): Boolean;
    function MyCheckDeploymentAndOperatingMode(pfObject: TSimObject): Double;
    function MyComputeDetectionFactor(pfObject: TSimObject; aFreq: Double)
      : Double;
    function MyScanProcessing(aDeltaT: Double; pfObject: TSimObject): Boolean;
    function MyDetermineEligibleTargetsAndMaximumRange(pfObject: TSimObject)
      : Boolean;
    function MyPhysicalProcessing(aDeltaT: Double): Boolean;
    function MySubAreaBoundaryProcessing(pfObject: TSimObject): Boolean;
    function MyComputeTargetStrengthAndSourceLevel(pfObject: TSimObject)
      : Double;
    function MyCheckDuctingCondition(pfObject: TSimObject)
      : TSonarTransmissionLossType;
    function MyComputeDirectPathTransmissionLoss(pfObject: TSimObject;
      aFreq: Double; aTransLossType: TSonarTransmissionLossType): Double;
    function MyShadowZonesProcessing(pfObject: TSimObject): Double;
    function MyBottomBounceProcessing(pfObject: TSimObject; aFreq: Double)
      : Double;
    function MyComputeTotalTransmissionLoss(aDirectPathTransmissionLoss,
      aShadowZoneTransmissionLoss, aBottomBounceTransmissionLoss: Double)
      : Double;
    function MyConvergenceZoneProcessing(pfObject: TSimObject): Double;
    function MyComputeAmbientNoise(pfObject: TSimObject; aFreq: Double): Double;
    function MyComputeProbabilityOfDetection(aDetectionFactor, aTargetStrength,
      aTransmissionLoss, aCZReduction, aTotalAmbientNoise: Double): Boolean;

    // Choco - Sonar Support Procedure/Function
    procedure MyResetDetectionState;
    function MyCheckSonarDeployment: Boolean;
    function MyShippingLevelNoise(aFreq: Double): Double;
    function MySeaStateNoise(aFreq: Double): Double;
    function MyRainRateNoise(aFreq: Double): Double;
    function MySonarDepth: Double;
    function MyMaximumRange: Double;
    function MySubArea(aPosX, aPosY: Double): TDBSubArea_Enviro_Definition;
    function MyBoundaryCrossing(pfObject: TSimObject): Integer;
    function CheckDetectability(aSonarDepth, aTargetDepth: Double;
      aSonarSVP, aTargetSVP: Byte; aSonarDOTL, aTargetDOTL: Double): Boolean;
    procedure SetDetectionRange(const Value: single);
    function TargetTypeEligiblity(aTarget: TObject): boolean;


  public
    constructor Create; override;
    destructor Destroy; override;
    procedure Initialize; override;
    procedure Move(const aDeltaMs: Double); override;

    // read only prop
    property SonarOnBoardDefinition: TDBSonar_On_Board read getSonarOnBoardDefinition;
    property SonarDefinition: TDBSonar_Definition read getSonarDefinition;

    property SNRData: TT3Sonar_Curve_Def read FSNRData;
    property SonarInfo: TSonarInfo read FSonarInfo;
    property DetectionRange : single read FDetectionRange write SetDetectionRange;

    // rw prop
    property SonarCategory: TSonarCategory read FSonarCategory write
      SetSonarCategory;
    property OperatingMode: TSonarOperatingMode read FOperatingMode write
      SetOperatingMode;
    property DeploymentStatus
      : TSonarDeploymentStatus read FDeploymentStatus write SetDeploymentStatus;

    property DepthCable: Double read FDepthCable write FDepthCable;
    property ActualCable: Double read FActualCable write FActualCable;
    property OrderCable: Double read FOrderCable write FOrderCable;

    property TIOWRange: TSonarTIOWRange read FTIOWRange write SetTIOWRange;
    property ControlMode: TSonarControlMode read FControlMode write
      SetControlMode;

    property OnControlMode: TOnSonarControlMode read FOnControlMode write
      SetOnControlMode;
    property OnDeploymentStatusChange
      : TOnDeploymentStatusChange read FOnDeploymentStatusChange write
      SetOnDeploymentStatusChange;
  end;

  TDoublePoint = record
    X, Y: Double;
  end;

function FN_Intersect(const p1, p2, p3, p4: TDoublePoint): Boolean;
function FN_CCW(const p0, p1, p2: TDoublePoint): Integer;

implementation

uses
  uT3Torpedo, uT3Mine, uGlobalVar;

{ TT3MountedSonar }

procedure TT3MountedSonar.calcSonarSensor(aDeltaT: Double);
var
  i : integer;
  pi : TT3PlatformInstance;
  isDetected : Boolean;
begin
  for i := 0 to simManager.SimPlatforms.itemCount - 1 do
  begin
    pi := simManager.SimPlatforms.getObject(i) as TT3PlatformInstance;

    if pi = PlatformParent then
      Continue;

    if (pi.PlatformDomain = vhdAir) then
      Continue;

    isDetected := SonarProcess(aDeltaT, pi);

    if Assigned(OnSensorDetect) then
      OnSensorDetect(Self, pi, isDetected);
  end;

end;

function TT3MountedSonar.CheckDetectability(aSonarDepth, aTargetDepth: Double;
  aSonarSVP, aTargetSVP: Byte; aSonarDOTL, aTargetDOTL: Double): Boolean;
var
  depth: Double;
begin
  case TSVPType(aSonarSVP) of
    svpPositive:
      begin
        case TSVPType(aTargetSVP) of
          svpPositive:
            Result := True;
          svpNegative:
            Result := False;
          svpPositiveOverNegative:
            begin
              Result := ((Abs(aSonarDepth) < aSonarDOTL) and
                  (Abs(aTargetDepth) < aTargetDOTL));
            end;
          svpNegativeOverPositive:
            Result := False;
        end;
      end;
    svpNegative:
      begin
        case TSVPType(aTargetSVP) of
          svpPositive:
            Result := False;
          svpNegative:
            Result := True;
          svpPositiveOverNegative:
            Result := False;
          svpNegativeOverPositive:
            begin
              Result := ((Abs(aSonarDepth) < aSonarDOTL) and
                  (Abs(aTargetDepth) < aTargetDOTL));
            end;
        end;
      end;
    svpPositiveOverNegative:
      begin
        case TSVPType(aTargetSVP) of
          svpPositive:
            Result := False;
          svpNegative:
            Result := False;
          svpPositiveOverNegative:
            Result := Abs(aSonarDOTL - aTargetDOTL) < 200;
          svpNegativeOverPositive:
            Result := False;
        end;
      end;
    svpNegativeOverPositive:
      begin
        case TSVPType(aTargetSVP) of
          svpPositive:
            Result := False;
          svpNegative:
            Result := False;
          svpPositiveOverNegative:
            Result := False;
          svpNegativeOverPositive:
            Result := Abs(aSonarDOTL - aTargetDOTL) < 200;
        end;
      end;
  end;
end;

constructor TT3MountedSonar.Create;
begin
  inherited;

  FSNRData       := TT3Sonar_Curve_Def.Create;
  ActualCable    := 0;
  OrderCable     := 0;
  TIOWRange      := strShort;
  DetectionRange := getDetectionRange;
end;

destructor TT3MountedSonar.Destroy;
begin
  FSNRData.Free;
  inherited;
end;

function TT3MountedSonar.getDetectionRange: Double;
begin
  Result := 0;
  if Assigned(SonarDefinition) then
  begin
    case FTIOWRange of
      strShort:
        Result := (1000 / C_NauticalMile_To_Yards)
          * SonarDefinition.TIOW_Short_Range;
      strMedium:
        Result := (1000 / C_NauticalMile_To_Yards)
          * SonarDefinition.TIOW_Medium_Range;
      strLong:
        Result := (1000 / C_NauticalMile_To_Yards)
          * SonarDefinition.TIOW_Long_Range;
    end;
  end
end;

function TT3MountedSonar.getSonarDefinition: TDBSonar_Definition;
begin
  Result := nil;

  if Assigned(FMountedDeviceDefinition) then
    Result := TDBSonar_Definition(FMountedDeviceDefinition);

end;

function TT3MountedSonar.getSonarOnBoardDefinition: TDBSonar_On_Board;
begin
  Result := nil;

  if Assigned(FMountedDeviceOnBoardDef) then
    Result := TDBSonar_On_Board(FMountedDeviceOnBoardDef);
end;

procedure TT3MountedSonar.Initialize;
var
  bz: TT3BlindZone;
  podSNRList : TDBSonar_POD_CurveList;
  podSonar : TDBPOD_vs_SNR_Point;
  bzList : TDBBlindZoneDefinitionList;
  dbBz : TDBBlind_Zone_Definition;
begin
  inherited;

  { set definition once }
  FDeviceParent            := simManager.FindT3PlatformByID(PlatformParentInstanceIndex);
  FMountedDeviceDefinition := simManager.Scenario.GetDBSonarDefinition(MountedDeviceDefinitionIndex);
  FMountedDeviceOnBoardDef := simManager.Scenario.
        GetDBSonarOnBoardDef(TT3PlatformInstance(FDeviceParent).PlatformDefinitionIndex,
              MountedDeviceOnBoardDefIndex);

  if Assigned(SonarOnBoardDefinition) then
  with SonarOnBoardDefinition do
  begin
    InstanceName  := Instance_Identifier;
    InstanceIndex := Sonar_Instance_Index;


    FTIOWRange        := strShort;
    FControlMode      := scmOff;
    FDeploymentStatus := sdsDeployed;

    FOrderCable       := 0;
    FActualCable      := 0;
    FDepthCable       := 0;

    FSettleKinkTime   := 0;

    { set blind zone }
    bzList := simManager.Scenario.GetDBBlindZones('Sonar',Sonar_Instance_Index);
    if Assigned(bzList) then
      for dbBz in bzList do
      begin
        bz :=  TT3BlindZone.Create;
        bz.Blind_Zone_Type := dbBz.Blind_Zone_Type;
        bz.BlindZone_Number:= dbBz.BlindZone_Number;
        bz.Start_Angle     := dbBz.Start_Angle;
        bz.End_Angle       := dbBz.End_Angle;

        BlindZones.Add(bz);
      end;
    end;

  if Assigned(SonarDefinition) then
  with SonarDefinition do
  begin
    case Sonar_Classification of
      0: OperatingMode := somActive;
      1: OperatingMode := somPassive;
      2: OperatingMode := somActivePassive;
      3: OperatingMode := somPassiveIntercept;
    end;

    case Sonar_Category_Index of
      0: FSonarCategory := scHMS;
      1: FSonarCategory := scVDS;
      2: FSonarCategory := scTAS;
      3: FSonarCategory := scSonobuoy;
      4: FSonarCategory := scDipping;
    end;

    // initiate POD_SNR_Curve
    podSNRList := simManager.Scenario.GetDBSonarPODCurvePoints(Curve_Detection_Index);
    if Assigned(podSNRList) then
      for podSonar in podSNRList do
      begin
        FSNRData.addData(podSonar.SNR_Ratio, podSonar.Prob_of_Detection);
      end;
  end;

end;

// function TT3MountedSonar.InsideBlindZone(aObject: TSimObject): boolean;
// var
// i : integer;
// blindZ : TT3BlindZone;
// bearing : double;
// sAngle, eAngle : double;
// begin
//
// bearing := CalcBearing(FPosition.X,FPosition.Y,
// TT3PlatformInstance(aObject).getPositionX,
// TT3PlatformInstance(aObject).getPositionY);
//
// Result := false;
//
// // periksa apakah didalam area blind zone
// if BlindZones.Count > 0 then
// for blindZ in BlindZones do begin
// sAngle := blindZ.Start_Angle  + TT3PlatformInstance(PlatformParent).Course;
// eAngle := blindZ.End_Angle    + TT3PlatformInstance(PlatformParent).Course;
// Result := DegComp_IsBeetwen(bearing, sAngle, eAngle);
// if Result then
// Exit;
// end;
// end;

// function TT3MountedSonar.InsideRange(aObject: TSimObject): boolean;
// var
// range : double;
// begin
// inherited;
// if TT3PlatformInstance(aObject).PlatformDomain = vhdAir then
// begin
// Result := False;
// Exit;
// end;
//
// range   := CalcRange(FPosition.X,FPosition.Y,
// aObject.getPositionX,aObject.getPositionY);
//
// result := range <= MaximumRange(ControlMode);
// end;

(*
  Sonar maximum operating range, depend on operating mode.
  Passive : MAXIMUM DETECTION RANGE
  Active  : Selected TIOW --> SHORT, MEDIUM, LONG : selected by operator  (kyards)

  Parameter :
  aSonarMode : Operating mode
  Output  : range (nm)
*)
function TT3MountedSonar.MaximumRange(aSonarMode: TSonarControlMode): Double;
begin
  Result := SonarDefinition.Max_Detect_Range;

  case aSonarMode of
    scmActive:
      begin
        case FTIOWRange of
          strShort:
            Result := (1000 / C_NauticalMile_To_Yards)
              * SonarDefinition.TIOW_Short_Range;
          strMedium:
            Result := (1000 / C_NauticalMile_To_Yards)
              * SonarDefinition.TIOW_Medium_Range;
          strLong:
            Result := (1000 / C_NauticalMile_To_Yards)
              * SonarDefinition.TIOW_Long_Range;
        end;
      end;
    scmPassive:
      Result := SonarDefinition.Max_Detect_Range;
  else
    Result := SonarDefinition.Max_Detect_Range; // mk 14042012
  end;

end;

procedure TT3MountedSonar.Move(const aDeltaMs: Double);
var
  bz: TT3BlindZone;
begin
  inherited;

  for bz in BlindZones do
    bz.Range := DetectionRange;

  { if role as server }
  if machineRole = crServer then
  begin
    if not (OperationalStatus in [sopOff,sopDamage,sopEMCON]) then
      calcSonarSensor(aDeltaMS);

    if FDoOnceCalc then
    begin
      SonarInactive;
      FDoOnceCalc := False;
    end;

  end;

  // case FControlMode of
  // scmDeploying:
  // begin
  //
  // ActualCable := ActualCable  +
  // ( aDeltaMs * SonarDefinition.FDef. Sonar_Depth_Rate_of_Change ) / C_Meter_To_Feet;
  //
  // if (ActualCable >= OrderCable) or (ActualCable >= SonarDefinition.FDef.Cable_Length) then
  // begin
  //
  // if (ActualCable >= SonarDefinition.FDef.Cable_Length) then
  // ActualCable := SonarDefinition.FDef.Cable_Length
  // else
  // ActualCable := OrderCable;
  //
  // // deployed
  // ControlMode := scmDeployed;
  // end;
  //
  // end;
  //
  // scmStowing:
  // begin
  // ActualCable := ActualCable  -
  // ( aDeltaMs * SonarDefinition.FDef.Sonar_Depth_Rate_of_Change ) / C_Meter_To_Feet;
  //
  // end;
  //
  // end;
  //

end;

function TT3MountedSonar.MyBottomBounceProcessing(pfObject: TSimObject;
  aFreq: Double): Double;
const
  waveHeight: array [0 .. 9] of Double = (0.1, 0.25, 1.0, 2.7, 4.6, 8.0, 11.5,
    20.0, 45.0, 45.0);
  wavewavewave: array [1 .. 9] of Double =
    (5.45, 7.44, 9.51, 12.69, 14.56, 18.81, 21.88, 26.00, 30.39);
var
  Range, numOfBounce, surfaceAttenuationPerBounce,
    bottomAttenuationPerBounce: Double;
begin
  Range := CalcRange(PosX, PosY, pfObject.PosX, pfObject.PosY);

  numOfBounce := Range / 2 * (simManager.Scenario.DBGameEnvironment.Ave_Ocean_Depth /
      C_NauticalMile_To_Feet);

  surfaceAttenuationPerBounce := 10 * Log10
    (1 - 0.0234 * Power(aFreq * waveHeight[simManager.Scenario.DBGameEnvironment.Sea_State], 1.5));

  bottomAttenuationPerBounce := wavewavewave
    [simManager.Scenario.DBGameEnvironment.Bottomloss_Coefficient];

  Result := numOfBounce * (surfaceAttenuationPerBounce +
      bottomAttenuationPerBounce);
end;

function TT3MountedSonar.MyBoundaryCrossing(pfObject: TSimObject): Integer;
var
  i, boundaryCrossing: Integer;
  subArea: TDBSubArea_Enviro_Definition;
  p1, p2, p3, p4: TDoublePoint;
  isIntersect: Boolean;
begin
  Result := 0;

  p1.X := PosX;
  p1.Y := PosY;
  p2.X := pfObject.PosX;
  p2.Y := pfObject.PosY;

  for subArea in simManager.Scenario.DBSubAreaDefinitions do
  begin
    isIntersect := False;

    if not isIntersect then
    begin
      p3.X := subArea.X_Position_1;
      p3.Y := subArea.Y_Position_1;
      p4.X := subArea.X_Position_2;
      p4.Y := subArea.Y_Position_1;

      isIntersect := FN_Intersect(p1, p2, p3, p4);
    end;

    if not isIntersect then
    begin
      p3.X := subArea.X_Position_2;
      p3.Y := subArea.Y_Position_1;
      p4.X := subArea.X_Position_2;
      p4.Y := subArea.Y_Position_2;

      isIntersect := FN_Intersect(p1, p2, p3, p4);
    end;

    if not isIntersect then
    begin
      p3.X := subArea.X_Position_2;
      p3.Y := subArea.Y_Position_2;
      p4.X := subArea.X_Position_1;
      p4.Y := subArea.Y_Position_2;

      isIntersect := FN_Intersect(p1, p2, p3, p4);
    end;

    if not isIntersect then
    begin
      p3.X := subArea.X_Position_1;
      p3.Y := subArea.Y_Position_2;
      p4.X := subArea.X_Position_1;
      p4.Y := subArea.Y_Position_1;

      isIntersect := FN_Intersect(p1, p2, p3, p4);
    end;

    if isIntersect then
      Inc(Result);
  end;

  FSonarInfo.BoundaryCrossingCount := Result;
end;

function TT3MountedSonar.MyCheckDeploymentAndOperatingMode(pfObject: TSimObject)
  : Double;
var
  target: TT3Vehicle;
  i: Integer;
  sensor: TT3MountedSensor;
begin
  if not MyCheckSonarDeployment then
    Exit;

  case FOperatingMode of
    somActive:
      Result := SonarDefinition.Active_Freq_of_Op;
    somPassive:
      Result := SonarDefinition.Passive_Freq_of_Op;
    somActivePassive:
      begin
        case FControlMode of
          scmPassive:
            Result := SonarDefinition.Passive_Freq_of_Op;
          scmActive:
            Result := SonarDefinition.Active_Freq_of_Op;
        end;
      end;
    somPassiveIntercept:
      begin
        Result := 0;

        if not(pfObject is TT3Vehicle) then
          Exit;

        target := TT3Vehicle(pfObject);

//        for sensor in target.MountedSensors do
//        begin
//
//          if not(sensor is TT3MountedSonar) then
//            Continue;
//
//          Result := TT3MountedSonar(sensor)
//            .SonarDefinition.Active_Freq_of_Op;
//          Break;
//        end;
      end;
  end;
end;

function TT3MountedSonar.MyCheckDuctingCondition(pfObject: TSimObject)
  : TSonarTransmissionLossType;
//var
//  target: TT3PlatformInstance;
//  sonarSub, targetSub: TSubArea_Enviro_Definition;
//  sonarDepth, targetDepth, sonarSurUpLimit, sonarSurLowLimit, sonarSubUpLimit,
//    sonarSubLowLimit, targetSurUpLimit, targetSurLowLimit, targetSubUpLimit,
//    targetSubLowLimit, sonarDuctDepth, targetDuctDepth: Double;
//  sonarSVP, targetSVP: TSVPType;
//  surfaceDuctEnabled, subsurfaceDuctEnabled: Boolean;
begin
  Result := stlSpherical;

//  target := TT3PlatformInstance(pfObject);
//  sonarSub := MySubArea(PosX, PosY);
//  targetSub := MySubArea(target.PosX, target.PosY);
//  sonarDepth := MySonarDepth;
//  targetDepth := target.Altitude * C_Meter_To_Feet;
//
//  if Assigned(sonarSub) then
//  begin
//    sonarSVP := TSVPType(sonarSub.FData.Sound_Velocity_Type);
//
//    surfaceDuctEnabled := Boolean(sonarSub.FData.Surface_Ducting_Active);
//    sonarSurUpLimit := sonarSub.FData.Upper_Limit_Sur_Duct_Depth;
//    sonarSurLowLimit := sonarSub.FData.Lower_Limit_Sur_Duct_Depth;
//    sonarSubUpLimit := sonarSub.FData.Upper_Limit_Sub_Duct_Depth;
//    sonarSubLowLimit := sonarSub.FData.Lower_Limit_Sub_Duct_Depth;
//
//    sonarDuctDepth := sonarSub.FData.Ave_Ocean_Depth;
//  end
//  else
//  begin
//    sonarSVP := TSVPType(Environment.FData.Sound_Velocity_Type);
//
//    surfaceDuctEnabled := Boolean(Environment.FData.Surface_Ducting_Active);
//    sonarSurUpLimit := Environment.FData.Upper_Limit_Surface_Duct_Depth;
//    sonarSurLowLimit := Environment.FData.Lower_Limit_Surface_Duct_Depth;
//    sonarSubUpLimit := Environment.FData.Upper_Limit_Sub_Duct_Depth;
//    sonarSubLowLimit := Environment.FData.Lower_Limit_Sub_Duct_Depth;
//
//    sonarDuctDepth := Environment.FData.Ave_Ocean_Depth;
//  end;
//
//  if Assigned(targetSub) then
//  begin
//    targetSVP := TSVPType(targetSub.FData.Sound_Velocity_Type);
//
//    surfaceDuctEnabled := Boolean(targetSub.FData.Surface_Ducting_Active);
//    targetSurUpLimit := targetSub.FData.Upper_Limit_Sur_Duct_Depth;
//    targetSurLowLimit := targetSub.FData.Lower_Limit_Sur_Duct_Depth;
//    targetSubUpLimit := targetSub.FData.Upper_Limit_Sub_Duct_Depth;
//    targetSubLowLimit := targetSub.FData.Lower_Limit_Sub_Duct_Depth;
//
//    targetDuctDepth := targetSub.FData.Ave_Ocean_Depth;
//  end
//  else
//  begin
//    targetSVP := TSVPType(Environment.FData.Sound_Velocity_Type);
//
//    surfaceDuctEnabled := Boolean(Environment.FData.Surface_Ducting_Active);
//    targetSurUpLimit := Environment.FData.Upper_Limit_Surface_Duct_Depth;
//    targetSurLowLimit := Environment.FData.Lower_Limit_Surface_Duct_Depth;
//    targetSubUpLimit := Environment.FData.Upper_Limit_Sub_Duct_Depth;
//    targetSubLowLimit := Environment.FData.Lower_Limit_Sub_Duct_Depth;
//
//    targetDuctDepth := Environment.FData.Ave_Ocean_Depth;
//  end;
//
//  if (sonarSVP = svpPositiveOverNegative) and
//    (targetSVP = svpPositiveOverNegative) then
//  begin
//    if Boolean(Environment.FData.Surface_Ducting_Active) and
//      (Abs(sonarDepth) > sonarSurUpLimit) and
//      (Abs(sonarDepth) < sonarSurLowLimit) and
//      (Abs(targetDepth) > targetSurUpLimit) and
//      (Abs(targetDepth) < targetSurLowLimit) then
//      Result := stlCylindrical;
//  end
//  else if (sonarSVP = svpNegativeOverPositive) and
//    (targetSVP = svpNegativeOverPositive) then
//  begin
//    if Boolean(Environment.FData.Sub_Ducting_Active) and
//      (Abs(sonarDepth) > sonarSubUpLimit) and
//      (Abs(sonarDepth) < sonarSubLowLimit) and
//      (Abs(targetDepth) > targetSurUpLimit) and
//      (Abs(targetDepth) < targetSurLowLimit) then
//      Result := stlCylindrical;
//  end;

//  if (sonarDuctDepth < 1000) or (targetDuctDepth < 1000) then
//    Result := stlCylindrical;
end;

function TT3MountedSonar.MyCheckSonarDeployment: Boolean;
begin
  case FSonarCategory of
    scHMS:
      FSonarInfo.CheckDeploymentAndOperatingStatusOK := True;
    scVDS, scTAS, scDipping:
      FSonarInfo.CheckDeploymentAndOperatingStatusOK :=
        FDeploymentStatus = sdsDeployed;
  end;

  Result := FSonarInfo.CheckDeploymentAndOperatingStatusOK;

end;

function TT3MountedSonar.MyComputeAmbientNoise(pfObject: TSimObject;
  aFreq: Double): Double;
var
  ownShip: TT3Vehicle;
  shippingLevelNoise, rainRateNoise, seaStateNoise, speedRelatedOwnshipNoise,
    noiseAcousticDecoy, ownShipNoise: Double;
  i: Integer;
  sensor: TT3MountedSensor;
  // dev : TT3DeviceUnit;
begin
  ownShip := TT3Vehicle(PlatformParent);
  shippingLevelNoise := MyShippingLevelNoise(aFreq);
  rainRateNoise := MyRainRateNoise(aFreq);
  seaStateNoise := MySeaStateNoise(aFreq);

  if ownShip.Speed <= ownShip.PlatformMotion.Min_Ground_Speed then
    speedRelatedOwnshipNoise :=
      ownShip.VehicleDefinition.LSpeed_Acoustic_Intens
  else if (ownShip.Speed > ownShip.PlatformMotion.Min_Ground_Speed) and
    (ownShip.Speed < ownShip.PlatformMotion.High_Ground_Speed) then
  begin
    if ownShip.Speed <=
      ownShip.VehicleDefinition.Cavitation_Speed_Switch then
      speedRelatedOwnshipNoise :=
        ownShip.VehicleDefinition.Below_Cav_Acoustic_Intens
    else
      speedRelatedOwnshipNoise :=
        ownShip.VehicleDefinition.Above_Cav_Acoustic_Intens;
  end
  else
    speedRelatedOwnshipNoise :=
      ownShip.VehicleDefinition.HSpeed_Acoustic_Intens;

  noiseAcousticDecoy := 0;

  // ??? harusnya dari mounted ecm
//  for sensor in ownShip.MountedSensors do
//  begin

    // if (sensor is TT3AcousticDecoyOnVehicle) and
    // (TT3AcousticDecoyOnVehicle(dev).Control = ecaOn) then
    // begin
    // with TT3AcousticDecoyOnVehicle(dev).ECMDefinition.FAccousticDecoy_Def do
    // noiseAcousticDecoy := noiseAcousticDecoy + Acoustic_Intensity_Increase;
    // end;

//  end;

  case FSonarCategory of
    scHMS, scVDS:
      begin
        ownShipNoise := speedRelatedOwnshipNoise +
          SonarDefinition.Ownship_Increase_due_to_Active +
          noiseAcousticDecoy - ownShip.VehicleDefinition.
          HMS_Noise_Reduction_Factor;
      end;
    scTAS:
      begin
        ownShipNoise := speedRelatedOwnshipNoise +
          SonarDefinition.Ownship_Increase_due_to_Active +
          noiseAcousticDecoy - ownShip.VehicleDefinition.
          TAS_Noise_Reduction_Factor;
      end;
    scDipping, scSonobuoy:
      ownShipNoise := 0;
  end;

  Result := shippingLevelNoise + rainRateNoise + seaStateNoise + ownShipNoise;
end;

function TT3MountedSonar.MyComputeDetectionFactor(pfObject: TSimObject;
  aFreq: Double): Double;
var
  shipingLevelNoise, seaStateNoise: Double;
begin
  shipingLevelNoise := MyShippingLevelNoise(aFreq);
  seaStateNoise := MySeaStateNoise(aFreq);

  case FOperatingMode of
    somActive:
      begin
        Result := FSNRData.getMaxSNRRatio +
          SonarDefinition.Known_Cross_Section -
          (40 * Log10(SonarDefinition.Active_Detect_Range)) -
          (shipingLevelNoise + seaStateNoise);
      end;
    somPassive, somPassiveIntercept:
      begin
        Result := FSNRData.getMaxSNRRatio +
          SonarDefinition.Known_Signal_Source -
          (20 * Log10(SonarDefinition.Passive_Detect_Range)) -
          (shipingLevelNoise + seaStateNoise);
      end;
    somActivePassive:
      begin
        case FControlMode of
          scmPassive:
            begin
              Result := FSNRData.getMaxSNRRatio +
                SonarDefinition.Known_Signal_Source -
                (20 * Log10(SonarDefinition.Passive_Detect_Range)) -
                (shipingLevelNoise + seaStateNoise);
            end;
          scmActive:
            begin
              Result := FSNRData.getMaxSNRRatio +
                SonarDefinition.Known_Cross_Section -
                (40 * Log10(SonarDefinition.Active_Detect_Range)) -
                (shipingLevelNoise + seaStateNoise);
            end;
        end;
      end;
  end;
end;

function TT3MountedSonar.MyComputeDirectPathTransmissionLoss
  (pfObject: TSimObject; aFreq: Double;
  aTransLossType: TSonarTransmissionLossType): Double;
var
  alpha, slantRange, sphericalThresholdRange: Double;
begin
  alpha := (0.036 * Sqr(aFreq)) / (3600 + Sqr(aFreq) + 0.00000032 * Sqr(aFreq));
  slantRange := CalcRange3D(PosX, PosY, pfObject.PosX, pfObject.PosY, PosZ,
    pfObject.PosZ) * C_NauticalMile_To_Metre;

  case aTransLossType of
    stlCylindrical:
      begin
        sphericalThresholdRange := 1050 * Sqrt
          (simManager.Scenario.DBGameEnvironment.Ave_Ocean_Depth) * C_Feet_To_Meter;

        if slantRange < sphericalThresholdRange then
          Result := 20 * Log10(slantRange) + alpha * slantRange
        else
          Result := 10 * Log10(slantRange) + 10 * Log10
            (sphericalThresholdRange) + alpha * slantRange;
      end;
    stlSpherical:
      Result := 20 * Log10(slantRange) + alpha * slantRange;
  end;

  Result := Result * C_Meter_To_NauticalMile;
end;

function TT3MountedSonar.MyComputeProbabilityOfDetection
  (aDetectionFactor, aTargetStrength, aTransmissionLoss, aCZReduction,
  aTotalAmbientNoise: Double): Boolean;
begin
  case FOperatingMode of
    somActive:
      begin
        FSonarInfo.SonarSNR := aDetectionFactor + aTargetStrength - 2 *
          aTransmissionLoss + aCZReduction - aTotalAmbientNoise;
      end;
    somPassive, somPassiveIntercept:
      begin
        FSonarInfo.SonarSNR := aDetectionFactor + aTargetStrength -
          aTransmissionLoss + aCZReduction - aTotalAmbientNoise;
      end;
    somActivePassive:
      begin
        case FControlMode of
          scmPassive:
            begin
              FSonarInfo.SonarSNR := aDetectionFactor + aTargetStrength -
                aTransmissionLoss + aCZReduction - aTotalAmbientNoise;
            end;
          scmActive:
            begin
              FSonarInfo.SonarSNR := aDetectionFactor + aTargetStrength - 2 *
                aTransmissionLoss + aCZReduction - aTotalAmbientNoise;
            end;
        end;
      end
  end;

  FSonarInfo.SonarPOD := SNRData.MyGetProbDetection(FSonarInfo.SonarSNR) * 100;

  FSonarInfo.RandomValue := Random * 100;

  FSonarInfo.TargetDetected := FSonarInfo.RandomValue < FSonarInfo.SonarPOD;
  Result := FSonarInfo.TargetDetected;

end;

function TT3MountedSonar.MyComputeTargetStrengthAndSourceLevel
  (pfObject: TSimObject): Double;
  function isBetween(val1, val2, valtest: Double): Boolean;
  begin
    Result := (valtest >= val1) and (valtest <= val2);
  end;

var
  bearing: Double;
  v: TT3Vehicle;
  tor: TT3Torpedo;
  i: Integer;
  sensor: TT3MountedSensor;
  // dev : TT3DeviceUnit;
begin
  Result := 0;

  if (FOperatingMode = somActive) or ((FOperatingMode = somActivePassive) and
      (FControlMode = scmActive)) then
  begin
    bearing := CalcBearing(PosX, PosY, pfObject.PosX, pfObject.PosY);

    if pfObject is TT3Vehicle then
    begin
      v := TT3Vehicle(pfObject);

      if isBetween(30, 150, bearing) or isBetween(240, 330, bearing) then
        Result := v.VehicleDefinition.Side_Acoustic_Cross
      else
        Result := v.VehicleDefinition.Front_Acoustic_Cross;
    end
    else if pfObject is TT3Torpedo then
    begin
      tor := TT3Torpedo(pfObject);

      // if isBetween(30, 150, bearing) or isBetween(240, 330, bearing) then
      // Result := tor.TorpedoDefinition.FDef.Side_Acoustic_Cross
      // else
      // Result := tor.TorpedoDefinition.FDef.Front_Acoustic_Cross;
    end;
  end;

  if (FOperatingMode = somPassive) or ((FOperatingMode = somActivePassive) and
      (FControlMode = scmPassive)) then
  begin
    if pfObject is TT3Vehicle then
    begin
      v := TT3Vehicle(pfObject);

      if v.Speed <= v.PlatformMotion.Min_Ground_Speed then
        Result := v.VehicleDefinition.LSpeed_Acoustic_Intens
      else if (v.Speed > v.PlatformMotion.Min_Ground_Speed) and
        (v.Speed < v.PlatformMotion.High_Ground_Speed) then
      begin
        if v.Speed <= v.VehicleDefinition.Cavitation_Speed_Switch then
          Result := v.VehicleDefinition.Below_Cav_Acoustic_Intens
        else
          Result := v.VehicleDefinition.Above_Cav_Acoustic_Intens;
      end
      else
        Result := v.VehicleDefinition.HSpeed_Acoustic_Intens;
    end
    else if pfObject is TT3Torpedo then
    begin
      tor := TT3Torpedo(pfObject);

      // if tor.Speed <= tor.PlatformMotion.FData.Min_Ground_Speed then
      // Result := tor.TorpedoDefinition.FDef.LSpeed_Acoustic_Intens
      // else if (tor.Speed > tor.PlatformMotion.FData.Min_Ground_Speed) and
      // (tor.Speed < tor.UnitMotion.FData.High_Ground_Speed) then
      // begin
      // if tor.Speed <= tor.TorpedoDefinition.FDef.Cavitation_Switch_Point then
      // Result := tor.TorpedoDefinition.FDef.Below_Cav_Acoustic_Intens
      // else
      // Result := tor.TorpedoDefinition.FDef.Above_Cav_Acoustic_Intens;
      // end
      // else
      // Result := tor.TorpedoDefinition.FDef.HSpeed_Acoustic_Intens;
    end;
  end;

  if pfObject is TT3Vehicle then
  begin
    v := TT3Vehicle(pfObject);

    // for i := 0 to v.Devices.Count - 1 do
    // begin
    // dev := v.Devices.Items[i];
    //
    // if (dev is TT3AcousticDecoyOnVehicle) and
    // (TT3AcousticDecoyOnVehicle(dev).Control = ecaOn) then
    // begin
    // with TT3AcousticDecoyOnVehicle(dev).ECMDefinition.FAccousticDecoy_Def do
    // Result := Result + Acoustic_Intensity_Increase;
    // end;
    // end;
  end;

  if FOperatingMode = somPassiveIntercept then
  begin
    if pfObject is TT3Vehicle then
    begin
      v := TT3Vehicle(pfObject);

//      for sensor in v.MountedSensors do
//      begin
//
//        if sensor is TT3MountedSonar then
//        begin
//          Result := 10 * Log10(TT3MountedSonar(sensor)
//              .SonarDefinition.FDef.Active_Operating_Power * 1000);
//
//          Break;
//        end;
//      end;
    end
    // else if pfObject is TT3Torpedo then
    // begin
    // Result := 10 * Log10(TT3Torpedo(pfObject).TorpedoDefinition.FDef.
    // Active_Seeker_Power * 1000);
    // end;
  end;
end;

function TT3MountedSonar.MyComputeTotalTransmissionLoss
  (aDirectPathTransmissionLoss, aShadowZoneTransmissionLoss,
  aBottomBounceTransmissionLoss: Double): Double;
begin
  if aShadowZoneTransmissionLoss < aBottomBounceTransmissionLoss then
    Result := aDirectPathTransmissionLoss + aShadowZoneTransmissionLoss
  else
    Result := aDirectPathTransmissionLoss + aBottomBounceTransmissionLoss;

end;

function TT3MountedSonar.MyConvergenceZoneProcessing(pfObject: TSimObject)
  : Double;
var
  pi: TT3PlatformInstance;
  sonarSub, targetSub: TDBSubArea_Enviro_Definition;
  sonarCZActive, targetCZActive: Boolean;
  sonarDepth, targetDepth, sonarCZMaxDepth, targetCZMaxDepth, Range, closestCZ,
    CZCentreRange, CZHalfWidth, CZStartRange, CZEndRange: Double;
begin
  pi := TT3PlatformInstance(pfObject);
  sonarSub := MySubArea(PosX, PosY);
  targetSub := MySubArea(pi.PosX, pi.PosY);

  if Assigned(sonarSub) then
  begin
    sonarCZActive := Boolean(sonarSub.CZ_Active);
    // FGlobal_Conv pada sub area tidak ada. Perlu ditambahi
  end
  else
  begin
    sonarCZActive := Boolean(simManager.Scenario.DBGameEnvironment.CZ_Active);
    sonarCZMaxDepth := simManager.Scenario.DBGameEnvironment.Max_Sonar_Depth;
  end;

  if Assigned(targetSub) then
  begin
    targetCZActive := Boolean(targetSub.CZ_Active);
    // FGlobal_Conv pada sub area tidak ada. Perlu ditambahi
  end
  else
  begin
    targetCZActive := Boolean(simManager.Scenario.DBGameEnvironment.CZ_Active);
    targetCZMaxDepth := simManager.Scenario.DBGameEnvironment.Max_Sonar_Depth;
  end;

  sonarDepth := MySonarDepth;
  targetDepth := pi.Altitude * C_Meter_To_Feet;

  if sonarCZActive and targetCZActive and (sonarDepth < sonarCZMaxDepth) and
    (targetDepth < targetCZMaxDepth) then
  begin
    Range := CalcRange(PosX, PosY, pi.PosX, pi.PosY);
    closestCZ := Round(Range / simManager.Scenario.DBGameEnvironment.Occurance_Range);

    if (closestCZ = 0) or (closestCZ > 6) then
    begin
      Result := 0;
      Exit;
    end;

    CZCentreRange := closestCZ * simManager.Scenario.DBGameEnvironment.Occurance_Range;
    CZHalfWidth := closestCZ * simManager.Scenario.DBGameEnvironment.Width / 2;
    CZStartRange := CZCentreRange - CZHalfWidth;
    CZEndRange := CZCentreRange + CZHalfWidth;

    if (Range > CZStartRange) and (Range < CZEndRange) then
    begin
      Result := simManager.Scenario.DBGameEnvironment.Increase_per_CZ - (closestCZ - 1)
        * simManager.Scenario.DBGameEnvironment.Signal_Reduction_Term;
    end
    else
      Result := 0;
  end
  else
    Result := 0;
end;

function TT3MountedSonar.MyDetermineEligibleTargetsAndMaximumRange
  (pfObject: TSimObject): Boolean;
var
  target: TT3PlatformInstance;
begin
  target := TT3PlatformInstance(pfObject);

  if (target.DetectabilityType = dtUndetectable) or
    ((target.DetectabilityType = dtPassiveDetection) and
      (FOperatingMode = somActive)) then
    FSonarInfo.TargetEligible := False
  else
    FSonarInfo.TargetEligible := True;

  if (FOperatingMode = somActive) or ((FOperatingMode = somActivePassive) and
      (FControlMode = scmActive)) then
  begin
    if target is TT3Vehicle then
    begin
      case target.PlatformDomain of
        vhdSurface:
          FSonarInfo.OperatingModeValid :=
            SonarDefinition.Surface_Detection_Capable;
        vhdSubsurface:
          FSonarInfo.OperatingModeValid :=
            SonarDefinition.SubSurface_Detection_Capable;
      else
        FSonarInfo.OperatingModeValid := False;
      end;
    end
    else if target is TT3Mine then
      FSonarInfo.OperatingModeValid :=
        SonarDefinition.Mine_Detection_Capable
    else if target is TT3Torpedo then
      FSonarInfo.OperatingModeValid :=
        SonarDefinition.Torpedo_Detection_Capable
    else
      FSonarInfo.OperatingModeValid := False;
  end
  else
    FSonarInfo.OperatingModeValid := True;

  FSonarInfo.TargetInsideRange := InsideRange(pfObject);

  FSonarInfo.DetermineTargetsAndMaximumRangeOK :=
    FSonarInfo.TargetEligible and FSonarInfo.OperatingModeValid and FSonarInfo.
    TargetInsideRange;

  Result := FSonarInfo.DetermineTargetsAndMaximumRangeOK;
end;

function TT3MountedSonar.MyMaximumRange: Double;
begin
  if (FOperatingMode = somActive) or ((FOperatingMode = somActivePassive) and
      (FControlMode = scmActive)) then
  begin
    case FTIOWRange of
      strShort:
        Result := 1000 * SonarDefinition.TIOW_Short_Range *
          C_Yard_To_NauticalMile;
      strMedium:
        Result := 1000 * SonarDefinition.TIOW_Medium_Range *
          C_Yard_To_NauticalMile;
      strLong:
        Result := 1000 * SonarDefinition.TIOW_Long_Range *
          C_Yard_To_NauticalMile;
    end;
  end
  else
    Result := SonarDefinition.Max_Detect_Range;
end;

function TT3MountedSonar.MyPhysicalProcessing(aDeltaT: Double): Boolean;
var
  ownShip: TT3PlatformInstance;
  sonarDepth: Double;
  subArea: TDBSubArea_Enviro_Definition;
begin
  ownShip := TT3PlatformInstance(PlatformParent);

  sonarDepth := MySonarDepth;

  FSonarInfo.OperatingDepthValid := not
    (((FSonarCategory = scDipping) and (sonarDepth > 0)) or
      (Abs(sonarDepth) > SonarDefinition.Max_Operating_Depth));

  FSonarInfo.DeploySpeedValid := ownShip.Speed <=
    SonarDefinition.Maximum_Sonar_Speed;

  if FSonarCategory = scTAS then
  begin
    subArea := MySubArea(PosX, PosY);

    if Assigned(subArea) then
      FSonarInfo.TASOceanDepthValid := subArea.Ave_Ocean_Depth >=
        SonarDefinition.Minimum_Depth
    else
      FSonarInfo.TASOceanDepthValid := simManager.Scenario.DBGameEnvironment.Ave_Ocean_Depth >=
        SonarDefinition.Minimum_Depth;

    FSonarInfo.TASDeploySpeedValid := ownShip.Speed >=
      SonarDefinition.Tow_Speed;

    if (OperationalStatus = sopUnkinked) and
      (ownShip.PlatformMotion.Standard_Turn_Rate >
        SonarDefinition.Turn_Rate_2_Kink) then
      OperationalStatus := sopKinked;

    if (OperationalStatus = sopKinked) and
      (ownShip.PlatformMotion.Standard_Turn_Rate <
        SonarDefinition.Turn_Rate_2_Kink) then
    begin
      if FSettleKinkTime > SonarDefinition.Time_2_Settle_Kinked then
        OperationalStatus := sopUnkinked;

      FSettleKinkTime := FSettleKinkTime + aDeltaT;
    end
    else
      FSettleKinkTime := 0;

    FSonarInfo.TASUnkinked := OperationalStatus = sopUnkinked;
  end
  else
  begin
    FSonarInfo.TASOceanDepthValid := True;
    FSonarInfo.TASDeploySpeedValid := True;
    FSonarInfo.TASUnkinked := True;
  end;

  FSonarInfo.PhysicalProcessingOK :=
    FSonarInfo.OperatingDepthValid and FSonarInfo.DeploySpeedValid and
    FSonarInfo.TASOceanDepthValid and FSonarInfo.TASDeploySpeedValid and
    FSonarInfo.TASUnkinked;

  Result := FSonarInfo.PhysicalProcessingOK;
end;

function TT3MountedSonar.MyRainRateNoise(aFreq: Double): Double;
var
  i: Integer;
  newNoise, oldNoise: TDBGame_Rainfall_On_Sonar;
begin

  for newNoise in simManager.Scenario.DBGameRainfall_Sonar do
  begin
    if aFreq = newNoise.Sonar_Frequency then
    begin
      case simManager.Scenario.DBGameEnvironment.Rain_Rate of
        0:
          Result := newNoise.Rain_0_Effect;
        1:
          Result := newNoise.Rain_1_Effect;
        2:
          Result := newNoise.Rain_2_Effect;
        3:
          Result := newNoise.Rain_3_Effect;
        4:
          Result := newNoise.Rain_4_Effect;
        5:
          Result := newNoise.Rain_5_Effect;
        6:
          Result := newNoise.Rain_6_Effect;
      end;

      Break;
    end
    else if aFreq < newNoise.Sonar_Frequency then
    begin
      case simManager.Scenario.DBGameEnvironment.Rain_Rate of
        0:
          begin
            Result := Interpolate(aFreq, oldNoise.Sonar_Frequency,
              oldNoise.Rain_0_Effect, newNoise.Sonar_Frequency,
              newNoise.Rain_0_Effect);
          end;
        1:
          begin
            Result := Interpolate(aFreq, oldNoise.Sonar_Frequency,
              oldNoise.Rain_1_Effect, newNoise.Sonar_Frequency,
              newNoise.Rain_1_Effect);
          end;
        2:
          begin
            Result := Interpolate(aFreq, oldNoise.Sonar_Frequency,
              oldNoise.Rain_2_Effect, newNoise.Sonar_Frequency,
              newNoise.Rain_2_Effect);
          end;
        3:
          begin
            Result := Interpolate(aFreq, oldNoise.Sonar_Frequency,
              oldNoise.Rain_3_Effect, newNoise.Sonar_Frequency,
              newNoise.Rain_3_Effect);
          end;
        4:
          begin
            Result := Interpolate(aFreq, oldNoise.Sonar_Frequency,
              oldNoise.Rain_4_Effect, newNoise.Sonar_Frequency,
              newNoise.Rain_4_Effect);
          end;
        5:
          begin
            Result := Interpolate(aFreq, oldNoise.Sonar_Frequency,
              oldNoise.Rain_5_Effect, newNoise.Sonar_Frequency,
              newNoise.Rain_5_Effect);
          end;
        6:
          begin
            Result := Interpolate(aFreq, oldNoise.Sonar_Frequency,
              oldNoise.Rain_6_Effect, newNoise.Sonar_Frequency,
              newNoise.Rain_6_Effect);
          end;
      end;

      Break;
    end;

    oldNoise := newNoise;
  end;
end;

procedure TT3MountedSonar.MyResetDetectionState;
begin
  with FSonarInfo do
  begin
    CheckDeploymentAndOperatingStatusOK := False;
    ScanProcessingOK := False;
    InsideSwipeAzimuthSector := False;
    OutsideBlindZone := False;
    DetermineTargetsAndMaximumRangeOK := False;
    TargetEligible := False;
    OperatingModeValid := False;
    TargetInsideRange := False;
    PhysicalProcessingOK := False;
    OperatingDepthValid := False;
    DeploySpeedValid := False;
    TASOceanDepthValid := False;
    TASDeploySpeedValid := False;
    TASUnkinked := False;
    SubAreaBoundaryProcessingOK := False;
    BoundaryCrossingCount := 0;
    SonarSNR := 0;
    SonarPOD := 0;
    RandomValue := 0;
    TargetDetected := False;
  end;
end;

function TT3MountedSonar.MyScanProcessing(aDeltaT: Double; pfObject: TSimObject)
  : Boolean;
var
  currentTIOW, integrationPeriod, sweptAzimuthSector: Double;
begin
  if (FOperatingMode = somActive) or ((FOperatingMode = somActivePassive) and
      (FControlMode = scmActive)) then
  begin
    case FTIOWRange of
      strShort:
        currentTIOW := 1000 * SonarDefinition.TIOW_Short_Range *
          C_Yard_To_NauticalMile;
      strMedium:
        currentTIOW := 1000 * SonarDefinition.TIOW_Medium_Range *
          C_Yard_To_NauticalMile;
      strLong:
        currentTIOW := 1000 * SonarDefinition.TIOW_Long_Range *
          C_Yard_To_NauticalMile;
    end;

    integrationPeriod := SonarDefinition.Active_Int_Period * currentTIOW /
      SonarDefinition.TIOW_Short_Range;
  end
  else
    integrationPeriod := SonarDefinition.Passive_Int_Period;

  FSonarInfo.InsideSwipeAzimuthSector := True;
  FSonarInfo.OutsideBlindZone := not InsideBlindZone(pfObject);
  FSonarInfo.ScanProcessingOK :=
    FSonarInfo.InsideSwipeAzimuthSector and FSonarInfo.OutsideBlindZone;
  Result := FSonarInfo.ScanProcessingOK;
end;

function TT3MountedSonar.MySeaStateNoise(aFreq: Double): Double;
var
  i: Integer;
  newNoise, oldNoise: TDBGame_Sea_On_Sonar;
begin
  for newNoise in simManager.Scenario.DBGameSea_Sonar do
  begin

    if aFreq = newNoise.Sonar_Frequency then
    begin
      case simManager.Scenario.DBGameEnvironment.Sea_State of
        0:
          Result := newNoise.Sea_0_Effect;
        1:
          Result := newNoise.Sea_1_Effect;
        2:
          Result := newNoise.Sea_2_Effect;
        3:
          Result := newNoise.Sea_3_Effect;
        4:
          Result := newNoise.Sea_4_Effect;
        5:
          Result := newNoise.Sea_5_Effect;
        6:
          Result := newNoise.Sea_6_Effect;
        7:
          Result := newNoise.Sea_7_Effect;
        8:
          Result := newNoise.Sea_8_Effect;
        9:
          Result := newNoise.Sea_9_Effect;
      end;

      Break;
    end
    else if aFreq < newNoise.Sonar_Frequency then
    begin
      case simManager.Scenario.DBGameEnvironment.Sea_State of
        0:
          begin
            Result := Interpolate(aFreq, oldNoise.Sonar_Frequency,
              oldNoise.Sea_0_Effect, newNoise.Sonar_Frequency,
              newNoise.Sea_0_Effect);
          end;
        1:
          begin
            Result := Interpolate(aFreq, oldNoise.Sonar_Frequency,
              oldNoise.Sea_1_Effect, newNoise.Sonar_Frequency,
              newNoise.Sea_1_Effect);
          end;
        2:
          begin
            Result := Interpolate(aFreq, oldNoise.Sonar_Frequency,
              oldNoise.Sea_2_Effect, newNoise.Sonar_Frequency,
              newNoise.Sea_2_Effect);
          end;
        3:
          begin
            Result := Interpolate(aFreq, oldNoise.Sonar_Frequency,
              oldNoise.Sea_3_Effect, newNoise.Sonar_Frequency,
              newNoise.Sea_3_Effect);
          end;
        4:
          begin
            Result := Interpolate(aFreq, oldNoise.Sonar_Frequency,
              oldNoise.Sea_4_Effect, newNoise.Sonar_Frequency,
              newNoise.Sea_4_Effect);
          end;
        5:
          begin
            Result := Interpolate(aFreq, oldNoise.Sonar_Frequency,
              oldNoise.Sea_5_Effect, newNoise.Sonar_Frequency,
              newNoise.Sea_5_Effect);
          end;
        6:
          begin
            Result := Interpolate(aFreq, oldNoise.Sonar_Frequency,
              oldNoise.Sea_6_Effect, newNoise.Sonar_Frequency,
              newNoise.Sea_6_Effect);
          end;
        7:
          begin
            Result := Interpolate(aFreq, oldNoise.Sonar_Frequency,
              oldNoise.Sea_7_Effect, newNoise.Sonar_Frequency,
              newNoise.Sea_7_Effect);
          end;
        8:
          begin
            Result := Interpolate(aFreq, oldNoise.Sonar_Frequency,
              oldNoise.Sea_8_Effect, newNoise.Sonar_Frequency,
              newNoise.Sea_8_Effect);
          end;
        9:
          begin
            Result := Interpolate(aFreq, oldNoise.Sonar_Frequency,
              oldNoise.Sea_9_Effect, newNoise.Sonar_Frequency,
              newNoise.Sea_9_Effect);
          end;
      end;

      Break;
    end;

    oldNoise := newNoise;
  end;
end;

function TT3MountedSonar.MyShadowZonesProcessing(pfObject: TSimObject): Double;
var
  sonarDepth, sosSurface, sosLayer, sosBottom, AOD, LTD, gradient,
    thetaM: Double;
begin
  sonarDepth := MySonarDepth;
  sosSurface := simManager.Scenario.DBGameEnvironment.Surface_Sound_Speed;
  sosLayer   := simManager.Scenario.DBGameEnvironment.Layer_Sound_Speed;
  sosBottom  := simManager.Scenario.DBGameEnvironment.Bottom_Sound_Speed;
  AOD        := simManager.Scenario.DBGameEnvironment.Ave_Ocean_Depth;
  LTD        := simManager.Scenario.DBGameEnvironment.Thermal_Layer_Depth;

  case TSVPType(simManager.Scenario.DBGameEnvironment.Sound_Velocity_Type) of
    svpPositive:
      begin
        gradient := (sosBottom - sosSurface) / AOD;
        thetaM := -1 * ArcCos(1 - (((AOD - sonarDepth) * gradient) / sosSurface)
          );
        Result := -2 * sosSurface * Sin(thetaM) / gradient;
      end;
    svpNegative:
      begin
        gradient := (sosBottom - sosSurface) / AOD;
        thetaM := ArcCos(1 + ((sonarDepth * gradient) / sosSurface));
        Result := -2 * sosSurface * Sin(thetaM) / gradient;
      end;
    svpPositiveOverNegative:
      begin
        if Abs(sonarDepth) < LTD then // diatas
        begin
          gradient := (sosLayer - sosSurface) / LTD;
          thetaM := -1 * ArcCos
            (1 - (((LTD - sonarDepth) * gradient) / sosSurface));
          Result := -2 * sosSurface * Sin(thetaM) / gradient;
        end
        else
        begin
          gradient := (sosBottom - sosLayer) / (AOD - LTD);
          thetaM := ArcCos(1 + (((sonarDepth - LTD) * gradient) / sosLayer));
          Result := -2 * sosLayer * Sin(thetaM) / gradient;
        end;
      end;
    svpNegativeOverPositive:
      Result := 0;
  end;

  Result := Result / C_NauticalMile_To_Feet;
end;

function TT3MountedSonar.MyShippingLevelNoise(aFreq: Double): Double;
var
  i: Integer;
  newNoise, oldNoise: TDBGame_Ship_On_Sonar;
begin
  Result := 0;

  for newNoise in simManager.Scenario.DBGameShip_Sonar do
  begin

    if aFreq = newNoise.Sonar_Frequency then
    begin
      case TShippingRate(simManager.Scenario.DBGameEnvironment.Shipping_Rate) of
        srDistant:
          Result := newNoise.Distant_Ship_Effect;
        srLow:
          Result := newNoise.Light_Ship_Effect;
        srMedium:
          Result := newNoise.Medium_Ship_Effect;
        srHigh:
          Result := newNoise.Heavy_Ship_Effect;
      end;

      Break;
    end
    else if aFreq < newNoise.Sonar_Frequency then
    begin
      case TShippingRate(simManager.Scenario.DBGameEnvironment.Shipping_Rate) of
        srDistant:
          begin
            Result := Interpolate(aFreq, oldNoise.Sonar_Frequency,
              oldNoise.Distant_Ship_Effect, newNoise.Sonar_Frequency,
              newNoise.Distant_Ship_Effect);
          end;
        srLow:
          begin
            Result := Interpolate(aFreq, oldNoise.Sonar_Frequency,
              oldNoise.Light_Ship_Effect, newNoise.Sonar_Frequency,
              newNoise.Light_Ship_Effect);
          end;
        srMedium:
          begin
            Result := Interpolate(aFreq, oldNoise.Sonar_Frequency,
              oldNoise.Medium_Ship_Effect, newNoise.Sonar_Frequency,
              newNoise.Medium_Ship_Effect);
          end;
        srHigh:
          begin
            Result := Interpolate(aFreq, oldNoise.Sonar_Frequency,
              oldNoise.Heavy_Ship_Effect, newNoise.Sonar_Frequency,
              newNoise.Heavy_Ship_Effect);
          end;
      end;

      Break;
    end;

    oldNoise := newNoise;
  end;
end;

function TT3MountedSonar.MySonarDepth: Double;
var
  pi: TT3PlatformInstance;
begin
  // karena data kedalaman sonar mayoritas adalah feet. maka return value pada
  // procedure ini dijadikan feet juga.
  pi := TT3PlatformInstance(PlatformParent);

  case FSonarCategory of
    scHMS:
      Result := pi.Altitude * C_Meter_To_Feet -
        SonarDefinition.Minimum_Depth;
    scVDS:
      Result := (pi.Altitude - FActualCable) * C_Meter_To_Feet;
    scTAS:
      Result := (pi.Altitude - FActualCable) * C_Meter_To_Feet /
        (0.7 * pi.Speed);
    scDipping:
      Result := (pi.Altitude - FActualCable) * C_Meter_To_Feet;
  end;
end;

function TT3MountedSonar.MySubArea(aPosX, aPosY: Double)
  : TDBSubArea_Enviro_Definition;
var
  i: Integer;
  subArea: TDBSubArea_Enviro_Definition;
begin
  Result := nil;

  for subArea in simManager.Scenario.DBSubAreaDefinitions do
  begin
    with subArea do
    begin
      if (aPosX < X_Position_1) or (aPosX > X_Position_2) or
        (aPosY < Y_Position_1) or (aPosY > Y_Position_2) then
        Continue
      else
      begin
        Result := subArea;
        Break;
      end;
    end;
  end;
end;

function TT3MountedSonar.MySubAreaBoundaryProcessing(pfObject: TSimObject)
  : Boolean;
var
  target: TT3PlatformInstance;
  boundaryCrossing: Integer;
  sonarSub, targetSub: TDBSubArea_Enviro_Definition;
  sonarDepth, targetDepth, sonarDOTL, targetDOTL: Double;
  // DOTL = Depth Of Thermal Layer
  sonarSVP, targetSVP: Byte;
begin
  target := TT3PlatformInstance(pfObject);

  boundaryCrossing := MyBoundaryCrossing(pfObject);

  if boundaryCrossing = 0 then
    FSonarInfo.SubAreaBoundaryProcessingOK := True
  else if boundaryCrossing = 1 then
  begin
    sonarDepth := MySonarDepth;
    targetDepth := target.Altitude * C_Meter_To_Feet;

    sonarSub := MySubArea(PosX, PosY);
    targetSub := MySubArea(target.PosX, target.PosY);

    if Assigned(sonarSub) then
    begin
      sonarSVP := sonarSub.Sound_Velocity_Type;
      sonarDOTL := sonarSub.Thermal_Layer_Depth;
    end
    else
    begin
      sonarSVP := simManager.Scenario.DBGameEnvironment.Sound_Velocity_Type;
      sonarDOTL := simManager.Scenario.DBGameEnvironment.Thermal_Layer_Depth;
    end;

    if Assigned(targetSub) then
    begin
      targetSVP := targetSub.Sound_Velocity_Type;
      targetDOTL := targetSub.Thermal_Layer_Depth;
    end
    else
    begin
      targetSVP := simManager.Scenario.DBGameEnvironment.Sound_Velocity_Type;
      targetDOTL := simManager.Scenario.DBGameEnvironment.Thermal_Layer_Depth;
    end;

    FSonarInfo.SubAreaBoundaryProcessingOK := CheckDetectability
      (sonarDepth, targetDepth, sonarSVP, targetSVP, sonarDOTL, targetDOTL);
  end
  else
    FSonarInfo.SubAreaBoundaryProcessingOK := False;

  Result := FSonarInfo.SubAreaBoundaryProcessingOK;
end;

procedure TT3MountedSonar.SetControlMode(const Value: TSonarControlMode);
begin
  FControlMode := Value;

  case FControlMode of
    scmOff:
      FOperationalStatus := sopOff;
    scmPassive:
      FOperationalStatus := sopPassive;
    scmActive:
      FOperationalStatus := sopActive;
  end;
end;

procedure TT3MountedSonar.SetDeploymentStatus
  (const Value: TSonarDeploymentStatus);
begin
  FDeploymentStatus := Value;
  FOnDeploymentStatusChange(Self);
end;

procedure TT3MountedSonar.SetDetectionRange(const Value: single);
begin
  FDetectionRange := Value;
end;

procedure TT3MountedSonar.SetOnControlMode(const Value: TOnSonarControlMode);
begin
  FOnControlMode := Value;
end;

procedure TT3MountedSonar.SetOnDeploymentStatusChange
  (const Value: TOnDeploymentStatusChange);
begin
  FOnDeploymentStatusChange := Value;
end;

procedure TT3MountedSonar.SetOperatingMode(const Value: TSonarOperatingMode);
begin
  FOperatingMode := Value;
end;

procedure TT3MountedSonar.SetSonarCategory(const Value: TSonarCategory);
begin
  FSonarCategory := Value;
end;

procedure TT3MountedSonar.SetTIOWRange(const Value: TSonarTIOWRange);
begin
  FTIOWRange := Value;
end;

procedure TT3MountedSonar.SonarInactive;
var
  i : integer;
  pi : TT3PlatformInstance;
begin
  for i := 0 to simManager.SimPlatforms.itemCount - 1 do
  begin
    pi := simManager.SimPlatforms.getObject(i) as TT3PlatformInstance;

    if pi = PlatformParent then
      Continue;

    if (pi.PlatformDomain = vhdAir) then
      Continue;

    if Assigned(OnSensorDetect) then
      OnSensorDetect(Self, pi, False);
  end;
end;

function TT3MountedSonar.SonarProcess(aDeltaT: Double; aShip: TSimObject)
  : Boolean;
var
  detectionFactor, operatingFrequency, targetSourceLevel,
    directPathTransmissionLoss, shadowZoneRange, bottomBounceAttenuation,
    transmissionLoss, czReductionTerm, ambientNoise: Double;
  transLossType: TSonarTransmissionLossType;
begin
  Result := False;
  MyResetDetectionState;

  operatingFrequency := MyCheckDeploymentAndOperatingMode(aShip);

  detectionFactor := MyComputeDetectionFactor(aShip, operatingFrequency);

  if not MyScanProcessing(aDeltaT, aShip) then
    Exit;

  if not MyDetermineEligibleTargetsAndMaximumRange(aShip) then
    Exit;

  if not MyPhysicalProcessing(aDeltaT) then
    Exit;

  if not MySubAreaBoundaryProcessing(aShip) then
  // boundaryCrossing belum di cek
    Exit;

  targetSourceLevel := MyComputeTargetStrengthAndSourceLevel(aShip);

  transLossType := MyCheckDuctingCondition(aShip);

  directPathTransmissionLoss := MyComputeDirectPathTransmissionLoss
    (aShip, operatingFrequency, transLossType);

  shadowZoneRange := MyShadowZonesProcessing(aShip);

  bottomBounceAttenuation := MyBottomBounceProcessing
    (aShip, operatingFrequency);

  transmissionLoss := MyComputeTotalTransmissionLoss
    (directPathTransmissionLoss, shadowZoneRange, bottomBounceAttenuation);

  czReductionTerm := MyConvergenceZoneProcessing(aShip);

  ambientNoise := MyComputeAmbientNoise(aShip, operatingFrequency);

  Result := MyComputeProbabilityOfDetection(detectionFactor, targetSourceLevel,
    transmissionLoss, czReductionTerm, ambientNoise);
end;

function TT3MountedSonar.TargetTypeEligiblity(aTarget: TObject): boolean;
var
 surfCapable, subsurfCapable, mineCapable, torpCapable: boolean;
 nVehicle : TT3Vehicle;
begin

  result := false;

  if not (Assigned(aTarget))then      //mk 19042012
    Exit;

  if not (aTarget is TT3PlatformInstance) then     //mk 19042012
    Exit;

  surfCapable     := SonarDefinition.Surface_Detection_Capable    = true;
  subsurfCapable  := SonarDefinition.SubSurface_Detection_Capable = true;
  mineCapable     := SonarDefinition.Mine_Detection_Capable       = true;
  torpCapable     := SonarDefinition.Torpedo_Detection_Capable    = true;

  if aTarget is TT3Vehicle then
  begin
    nVehicle := TT3Vehicle(aTarget);

    if Assigned(nVehicle)then
    begin
      if nVehicle.VehicleDefinition = nil  then   //mk
      begin
        Result := False;
        Exit;
      end;

      if nVehicle.VehicleDefinition.Detectability_Type = 1 then  //17042012 mk
        Exit;

      if (nVehicle.VehicleDefinition.Detectability_Type = 2) and //17042012 mk
        (FControlMode <> scmPassive) then
        Exit

      else
      if (nVehicle.VehicleDefinition.Detectability_Type = 0) or  //17042012 mk
        (nVehicle.VehicleDefinition.Detectability_Type = 3) then
      begin
        if not((aTarget is TT3Vehicle) or (aTarget is TT3Torpedo) or
            (aTarget is TT3Mine)) then
       begin
         result := True;  //mk
         Exit;
       end;

       if (nVehicle.VehicleDefinition.Platform_Domain = 0) and (TT3Vehicle(aTarget).Altitude > 0) then
       begin
          Result := False;
          Exit;
       end
       else
          surfCapable := (nVehicle.VehicleDefinition.Platform_Domain = 1) and surfCapable;

       subsurfCapable := (nVehicle.VehicleDefinition.Platform_Domain = 2) and subsurfCapable;
      end;
    end;
  end
  else if aTarget is TT3Torpedo then
    torpCapable := torpCapable and true
  else if aTarget is TT3Mine then
    mineCapable := mineCapable and true
  else
  if aTarget is TT3MountedMissile then
  begin
    result := False;
    Exit;
  end;

  result := surfCapable or subsurfCapable or mineCapable or torpCapable;

end;

function FN_Intersect(const p1, p2, p3, p4: TDoublePoint): Boolean;
(* ************************************************************************
  * PURPOSE
  * Given two line segments, determine if they intersect.
  *
  * RETURN VALUE
  * TRUE if they intersect, FALSE if not.
  ************************************************************************ *)
begin
  Result := (((FN_CCW(p1, p2, p3) * FN_CCW(p1, p2, p4)) <= 0) and
      ((FN_CCW(p3, p4, p1) * FN_CCW(p3, p4, p2) <= 0)));
end;

function FN_CCW(const p0, p1, p2: TDoublePoint): Integer;
(*
  * PURPOSE
  * Determines, given three points, if when travelling from the first to
  * the second to the third, we travel in a counterclockwise direction.

  *
  * RETURN VALUE
  * (int) 1 if the movement is in a counterclockwise direction, -1 if
  * not.
  ************************************************************************ *)
var
  dx1, dx2, dy1, dy2: Double;
begin
  dx1 := p1.X - p0.X;
  dx2 := p2.X - p0.X;
  dy1 := p1.Y - p0.Y;
  dy2 := p2.Y - p0.Y;

  (* This is basically a slope comparison: we don't do divisions because
    * of divide by zero possibilities with pure horizontal and pure
    * vertical lines.
    *)
  if (dx1 * dy2 > dy1 * dx2) then
    Result := 1
  else
    Result := -1;
end;

end.
