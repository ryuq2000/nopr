unit uT3MountedChaff;

interface

uses uT3MountedECM, uSimObjects, uT3BlindZone, uDBAsset_Countermeasure, uDBBlind_Zone,
  uBaseCoordSystem, uT3PlatformInstance, tttData, uT3Vehicle;

type
  TT3MountedChaff = class abstract (TT3MountedECM)
  private
    FQuantity: integer;
    procedure SetQuantity(const Value: integer);
    function getDefinition: TChaff_On_Board;

  public
    constructor Create; override;
    destructor Destroy; override;

    procedure Initialize; override;

    { chaff db definition }
    property ChaffDefinition  : TChaff_On_Board read getDefinition;
    property Quantity: integer read FQuantity write SetQuantity;
  end;

implementation

{ TT3MountedChaff }

constructor TT3MountedChaff.Create;
begin
  inherited;

end;

destructor TT3MountedChaff.Destroy;
begin

  inherited;
end;

function TT3MountedChaff.getDefinition: TChaff_On_Board;
begin
  result := nil;

  if Assigned(FMountedDeviceDefinition) then
    Result := TChaff_On_Board(FMountedDeviceDefinition);

end;

procedure TT3MountedChaff.Initialize;
begin
  inherited;

  if Assigned(ChaffDefinition) then
  with ChaffDefinition do
  begin

    InstanceName  := FData.Instance_Identifier;
    InstanceIndex := FData.Chaff_Instance_Index;
    Quantity      := FData.Chaff_Qty_On_Board;

    if TT3Vehicle(PlatformParent).PlatformDomain = 0 then
      Category := ecAirborneChaff
    else
      Category := ecSurfaceChaff;
  end;

end;

procedure TT3MountedChaff.SetQuantity(const Value: integer);
begin
  FQuantity := Value;
end;

end.
