unit uT3MountedDefensiveJammer;

interface

uses
  uT3MountedECM, uT3PlatformInstance, uDBClassDefinition,
  uT3Missile, uT3MountedRadar;

type
  TT3MountedDefensiveJammer = class (TT3MountedECM)
  private
    FTargetingType        : TECMTargeting;

    FJammer_TARH_Capable  : boolean;
    FJammer_SARH_Capable  : boolean;
    FType_A_Seducing_Prob : single;
    FType_B_Seducing_Prob : single;
    FType_C_Seducing_Prob : single;
    FBreakLockCounter     : single;
    FECM_Type             : byte;
    FSpotNumber           : Integer;
    FBearing              : Double;
    FTargetInstanceIndex  : Integer;

    function getDefinition: TDBDefensive_Jammer_Definition;
    function getOnBoardDefinition: TDBDefensive_Jammer_On_Board;

    function AcquireMissileTARH : TT3Missile;
    function AcquireFCRadar     : TT3MountedRadar;

    procedure SetTargetingType(const Value: TECMTargeting);

    procedure SetBearing(const Value: Double);
    procedure SetSpotNumber(const Value: Integer);
    procedure SetTargetInstanceIndex(const Value: Integer);public
    procedure Initialize; override;
    procedure Move(const aDeltaMs: double); override;

    { self defense db definition }
    property JammerDefinition        : TDBDefensive_Jammer_Definition read getDefinition;
    property JammerOnBoardDefinition : TDBDefensive_Jammer_On_Board read getOnBoardDefinition;
    property TargetingType           : TECMTargeting read FTargetingType write SetTargetingType;
    property Bearing                 : Double read FBearing write SetBearing;
    property SpotNumber              : Integer read FSpotNumber write SetSpotNumber default 10000;
    property TargetInstanceIndex     : Integer read FTargetInstanceIndex write SetTargetInstanceIndex;
  end;

implementation

uses
  uGlobalVar, tttData;

{ TT3MountedAcousticDeploy }

function TT3MountedDefensiveJammer.AcquireFCRadar: TT3MountedRadar;
var
  I,j, count : integer;
  item : TObject;
  lockedTgt : TObject;
  jammedFCR, candFCR : TT3MountedRadar;
  FCRBearing, dtBearing, minBearing : Double;
begin
  jammedFCR  := nil;
  candFCR    := nil;
  minBearing := 9999;

  count := simManager.SimRadarsOnBoards.itemCount;
//  for I := 0 to count - 1 do
//  begin
//    item := simManager.SimRadarsOnBoards.getObject(i);
//
//    if item is TT3MountedRadar then
//    begin
//      if not (TT3MountedRadar(item).OperationalStatus in [sopOn]) then
//        Continue;
//
//      {cari FCR Radar saja}
//      if TT3MountedRadar(item).RadarDefinition.Num_FCR_Channels > 0 then
//      begin
//        case Status of
//          esAutomatic:
//          begin
//            {hanya FCR yang lock platform ini saja}
//            if (Assigned(TT3MountedRadar(item).AssignedTrack))
//               or (TT3MountedRadar(item).AssignedTrack <> nil) then
//            begin
//              for j := 0 to TT3MountedRadar(item).AssignedTrack.Count - 1 do
//              begin
//                lockedTgt := TT3MountedRadar(item).AssignedTrack.Items[j];
//
//                if (lockedTgt is TT3PlatformInstance) and (lockedTgt = Parent) then
//                begin
//                  jammedFCR := TT3MountedRadar(item);
//                  break;
//                end
//                else
//                if (lockedTgt is TT3DetectedTrack) and
//                   (TT3DetectedTrack(lockedTgt).TrackObject = Parent) then
//                begin
//                  jammedFCR := TT3MountedRadar(item);
//                  break;
//                end;
//              end;
//            end;
//          end;
//
//          esManual :
//          begin
//            case TargetingType of
//              { manual mode dengan track number}
//              ettTrack :
//              begin
//                { Target belum ditrack}
//                if TargetIndex = 0 then
//                  Break;
//
//                if Assigned(TT3MountedRadar(item).Parent) and
//                   (TT3PlatformInstance(TT3MountedRadar(item).Parent).InstanceIndex = TargetIndex) then
//                begin
//                  { apakah FCR lagi lock platform apapun }
//                  if TT3MountedRadar(item).AssignedTrack <> nil then
//                  begin
//                    if TT3MountedRadar(item).AssignedTrack.Count >= 1 then
//                    begin
//                      jammedFCR := TT3MountedRadar(item);
//                      break;
//                    end;
//                  end;
//                end;
//              end;
//
//              { manual mode dengan spot number}
//              ettSpot:
//              begin
//                if TT3MountedRadar(item).SpotNumber = FSpotNumber then
//                begin
//                  if (Assigned(TT3MountedRadar(item).AssignedTrack))
//                     or (TT3MountedRadar(item).AssignedTrack <> nil) then
//                  begin
//                    { apakah FCR lagi lock platform apapun }
//                    if TT3MountedRadar(item).AssignedTrack.Count = 0 then
//                      Continue;
//
//                    { cek candidat FCR dengan bearing set operator }
//                    if Assigned(TT3MountedRadar(item)) then
//                    begin
//                      { bearing jammer to FCR }
//                      FCRBearing := CalcBearing(TT3MountedRadar(item).PosX,
//                          TT3MountedRadar(item).PosY, PosX, PosY);
//
//                      { delta bearing_to_jammer with operator bearing }
//                      dtBearing := Abs(FCRBearing - Bearing);
//
//                      if dtBearing > 180 then
//                        dtBearing := 360 - dtBearing;
//
//                      if minBearing > dtBearing then
//                      begin
//                        minBearing := dtBearing;
//                        candFCR := TT3MountedRadar(item);
//                      end;
//                    end;
//                  end;
//                end;
//              end;
//            end;
//          end;
//        end;
//      end;
//    end;
//
//    if Assigned(candFCR) then
//      jammedFCR := candFCR;
//
//    { jika ketemu ..}
//    if Assigned(jammedFCR) then
//      break;
//  end;

  result := jammedFCR;
end;

function TT3MountedDefensiveJammer.AcquireMissileTARH: TT3Missile;
var
  i : integer;
  item : TObject;
  jammedMissile : TT3Missile;
begin
  result := nil;
  jammedMissile := nil;

//  for I := 0 to simManager.SimPlatforms.itemCount - 1 do
//  begin
//    item := simManager.SimPlatforms.getObject(i);
//
//    {cari yang missile saja}
//    if item is TT3Missile then
//    begin
//      {cari missile yang tipe guidance TARH}
//      if TT3Missile(item). PrimaryGuidance = mgtTARH then
//      begin
//        case Status of
//          { jika mode automatic}
//          esAutomatic:
//          begin
//            if Assigned(TT3Missile(item).TargetObject) and
//               TT3Missile(item).isLockTarget then
//            begin
//              {hanya TARH missile yg narget platform ini yg bisa di jammed}
//              if TT3Missile(item).TargetObject = Parent then
//              begin
//                if TT3Missile(item).EstimateHitTime > -1 then
//                begin
//                  { jika banyak TARH missile yg narget, cari yg estimasi waktu perkenaan
//                    paling pendek }
//
//                  if not Assigned(jammedMissile) then
//                    jammedMissile := TT3Missile(item)
//                  else
//                  if jammedMissile.EstimateHitTime > TT3Missile(item).EstimateHitTime then
//                    jammedMissile := TT3Missile(item);
//                end;
//              end;
//            end;
//          end;
//
//          { jika mode manual}
//          esManual:
//          begin
//            case Targeting of
//              { manual mode dengan track number}
//              ettTrack:
//              begin
//                { Target belum ditrack}
//                if TargetIndex = 0 then
//                  Break;
//
//                if TT3Missile(item).InstanceIndex = TargetIndex then
//                begin
//                  { only missile with locking target eligible for jamming }
//                  if Assigned(TT3Missile(item).TargetObject) and
//                     TT3Missile(item).isLockTarget then
//                  begin
//                    jammedMissile := TT3Missile(item);
//                    break;
//                  end;
//                end;
//              end;
//
//              { manual mode dengan spot number}
//              ettSpot:
//              begin
//                if Assigned(TT3Missile(item).TargetObject) and
//                   TT3Missile(item).isLockTarget then
//                begin
//                  { hanya spot number yang sesuai dengan input operator }
//                  if TT3Missile(item).SpotNumber = FSpotNumber then
//                  begin
//                    jammedMissile := TT3Missile(item);
//                    break;
//                  end;
//                end;
//              end;
//            end;
//          end;
//        end;
//      end;
//    end;
//  end;

  result := jammedMissile;
end;

function TT3MountedDefensiveJammer.getDefinition: TDBDefensive_Jammer_Definition;
begin
  result := nil;

  if Assigned(FMountedDeviceDefinition) then
    Result := TDBDefensive_Jammer_Definition(FMountedDeviceDefinition);

end;

function TT3MountedDefensiveJammer.getOnBoardDefinition: TDBDefensive_Jammer_On_Board;
begin
  result := nil;

  if Assigned(FMountedDeviceOnBoardDef) then
    Result := TDBDefensive_Jammer_On_Board(FMountedDeviceOnBoardDef);
end;

procedure TT3MountedDefensiveJammer.Initialize;
begin
  inherited;

  { set definition once }
  FDeviceParent            := simManager.FindT3PlatformByID(PlatformParentInstanceIndex);
  FMountedDeviceDefinition := simManager.Scenario.GetDBDefJammerDefintion(MountedDeviceDefinitionIndex);
  FMountedDeviceOnBoardDef := simManager.Scenario.
        GetDBDefJammerOnBoardDef(TT3PlatformInstance(FDeviceParent).PlatformDefinitionIndex,
              MountedDeviceOnBoardDefIndex);

  if Assigned(JammerOnBoardDefinition) then
  with JammerOnBoardDefinition do begin
    InstanceName := Instance_Identifier;
    InstanceIndex := Defensive_Jammer_Instance_Index;
    Category      := ecOnBoardDefence;
  end;

  if Assigned(JammerDefinition) then
  begin
    FJammer_TARH_Capable  := JammerDefinition.Jammer_TARH_Capable = 1;
    FJammer_SARH_Capable  := JammerDefinition.Jammer_SARH_Capable = 1;
    FType_A_Seducing_Prob := JammerDefinition.Type_A_Seducing_Prob;
    FType_B_Seducing_Prob := JammerDefinition.Type_B_Seducing_Prob;
    FType_C_Seducing_Prob := JammerDefinition.Type_C_Seducing_Prob;
    FECM_Type             := JammerDefinition.ECM_Type;

  end;

  FBreakLockCounter := 0.00;
end;

procedure TT3MountedDefensiveJammer.Move(const aDeltaMs: double);
begin
  inherited;

end;

procedure TT3MountedDefensiveJammer.SetBearing(const Value: Double);
begin
  FBearing := Value;
end;

procedure TT3MountedDefensiveJammer.SetSpotNumber(const Value: Integer);
begin
  FSpotNumber := Value;
end;

procedure TT3MountedDefensiveJammer.SetTargetingType(const Value: TECMTargeting);
begin
  FTargetingType := Value;
end;

procedure TT3MountedDefensiveJammer.SetTargetInstanceIndex(
  const Value: Integer);
begin
  FTargetInstanceIndex := Value;
end;

end.
