unit uT3MountedInfrared;

interface

uses uT3MountedECM, uSimObjects, uT3BlindZone, uDBClassDefinition,
  uBaseCoordSystem, uT3PlatformInstance, tttData, uT3Vehicle;

type
  TT3MountedInfrared = class (TT3MountedECM)
  private
    FQuantity: integer;
    function getDefinition: TDBInfrared_Decoy_Definition;
    function getOnBoardDefinition: TDBInfrared_Decoy_On_Board;
    procedure SetQuantity(const Value: integer);
  published

  public
    constructor Create; override;
    destructor Destroy; override;

    procedure Initialize; override;
    procedure Move(const dt : Double); override;

    { infrared db definition }
    property InfraredDecoyDefinition  : TDBInfrared_Decoy_Definition read getDefinition;
    property InfraredDecoyOnBoardDefinition  : TDBInfrared_Decoy_On_Board read getOnBoardDefinition;

    property Quantity : integer read FQuantity write SetQuantity;
  end;



implementation

uses
  uGlobalVar;

{ TT3MountedSurfaceChaff }

constructor TT3MountedInfrared.Create;
begin
  inherited;

end;

destructor TT3MountedInfrared.Destroy;
begin

  inherited;
end;

function TT3MountedInfrared.getDefinition: TDBInfrared_Decoy_Definition;
begin
  result := nil;

  if Assigned(FMountedDeviceDefinition) then
    Result := TDBInfrared_Decoy_Definition(FMountedDeviceDefinition);
end;

function TT3MountedInfrared.getOnBoardDefinition: TDBInfrared_Decoy_On_Board;
begin
  result := nil;

  if Assigned(FMountedDeviceOnBoardDef) then
    Result := TDBInfrared_Decoy_On_Board(FMountedDeviceOnBoardDef);
end;

procedure TT3MountedInfrared.Initialize;
begin
  inherited;
  { set definition once }
  FDeviceParent            := simManager.FindT3PlatformByID(PlatformParentInstanceIndex);
  FMountedDeviceDefinition := simManager.Scenario.GetDBInfraredDefintion(MountedDeviceDefinitionIndex);
  FMountedDeviceOnBoardDef := simManager.Scenario.
        GetDBInfraredOnBoardDef(TT3PlatformInstance(FDeviceParent).PlatformDefinitionIndex,
              MountedDeviceOnBoardDefIndex);

  if Assigned(InfraredDecoyOnBoardDefinition) then
  with InfraredDecoyOnBoardDefinition do begin
    InstanceIndex := Infrared_Decoy_Instance_Index;
    InstanceName  := Instance_Identifier;
    Quantity      := Infrared_Decoy_Qty_On_Board;
    Category      := ecInfraredDecoy;
  end;
end;

procedure TT3MountedInfrared.Move(const dt : Double);
begin
  inherited;

end;

procedure TT3MountedInfrared.SetQuantity(const Value: integer);
begin
  FQuantity := Value;
end;

end.
