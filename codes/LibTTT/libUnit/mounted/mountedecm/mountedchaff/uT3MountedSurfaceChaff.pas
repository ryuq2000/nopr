unit uT3MountedSurfaceChaff;

interface

uses uT3MountedECM, uSimObjects, uT3BlindZone, uDBClassDefinition,
  uBaseCoordSystem, uT3PlatformInstance, tttData, uT3Vehicle, uT3MountedChaff,
  uT3MountedChaffLauncher;

type
  TT3MountedSurfaceChaff = class (TT3MountedChaff)

  public
    constructor Create; override;
    destructor Destroy; override;

    procedure Initialize; override;
  end;

implementation

{ TT3MountedSurfaceChaff }

constructor TT3MountedSurfaceChaff.Create;
begin
  inherited;

end;

destructor TT3MountedSurfaceChaff.Destroy;
begin

  inherited;
end;

procedure TT3MountedSurfaceChaff.Initialize;
begin
  inherited;

end;

end.
