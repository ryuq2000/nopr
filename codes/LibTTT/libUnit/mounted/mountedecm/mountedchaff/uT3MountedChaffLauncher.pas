unit uT3MountedChaffLauncher;

interface

uses uT3mountedDevice, SysUtils, uDBClassDefinition, uT3MountedChaff;

type
  TT3MountedChaffLauncher = class (TT3MountedDevice)
  private
    FBearing            : double;
    FNumber             : integer;
    FBloomRange         : double;
    FDelay              : integer;
    FSalvoSize          : integer;
    FBloomAltitude      : double;
    FChaffInstanceIndex : integer;
    FState              : integer;

    FMaxRange      : double;
    FMinRange      : double;
    FMaxElevation  : double;
    FMinElevation  : double;
    FLauncherKind  : integer;
    FLaunchSpeed   : double;

    FCounter      : double;
    FSalvoCounter : integer;
    FFirstLaunch  : boolean;
    FIsFiring     : boolean;

    FChaffInstance : TT3MountedChaff;  // set from SetChaffInstanceIndex

    procedure SetBearing(const Value: double);
    procedure SetBloomAltitude(const Value: double);
    procedure SetBloomRange(const Value: double);
    procedure SetDelay(const Value: integer);
    procedure SetNumber(const Value: integer);
    procedure SetSalvoSize(const Value: integer);
    procedure SetChaffInstanceIndex(const Value: integer);
    function getOnBoardDefinition: TDBChaff_Launcher_On_Board;
    procedure SetState(const Value: integer);
  published

  public
    constructor Create; override;
    destructor Destroy; override;

    procedure Initialize; override;
    procedure Move(const dt : Double);override;

    property LauncherDefinition : TDBChaff_Launcher_On_Board read getOnBoardDefinition;

    property BloomRange    : double read FBloomRange write SetBloomRange;
    property BloomAltitude : double read FBloomAltitude write SetBloomAltitude;
    property SalvoSize     : integer read FSalvoSize write SetSalvoSize;
    property Delay         : integer read FDelay write SetDelay;
    property Bearing       : double read FBearing write SetBearing;
    property Number        : integer read FNumber write SetNumber;
    property State         : integer read FState write SetState;

    property ChaffInstanceIndex: integer read FChaffInstanceIndex write SetChaffInstanceIndex;
  end;

implementation

uses
  uGlobalVar, uT3Vehicle;

{ TT3MountedChaffLauncher }

constructor TT3MountedChaffLauncher.Create;
begin
  inherited;

  FFirstLaunch        := true;
  FState              := 0; // --> 0 : steady, 1 : on salvo
  FCounter            := 0;
  FSalvoCounter       := 0;
  FChaffInstanceIndex := 0;
  FIsFiring           := false;
end;

destructor TT3MountedChaffLauncher.Destroy;
begin

  inherited;
end;

function TT3MountedChaffLauncher.getOnBoardDefinition: TDBChaff_Launcher_On_Board;
begin
  result := nil;

  if Assigned(FMountedDeviceOnBoardDef) then
    Result := TDBChaff_Launcher_On_Board(FMountedDeviceOnBoardDef);
end;

procedure TT3MountedChaffLauncher.Initialize;
var
  launchers : TDBChaffLauncherOnBoardList;
  launcher : TDBChaff_Launcher_On_Board;
begin
  inherited;

  FDeviceParent            := simManager.FindT3PlatformByID(PlatformParentInstanceIndex);

  simmanager.Scenario.DBChaffLauncherOnBoardDict.TryGetValue(
      PlatformParentInstanceIndex,launchers);

  if Assigned(launchers) then
  begin
    for launcher in launchers do
      if launcher.Launcher_Number = MountedDeviceOnBoardDefIndex then
      begin
        FMountedDeviceOnBoardDef := launcher;
        Break;
      end;
  end;

  if Assigned(LauncherDefinition) then
  with LauncherDefinition do
  begin
    FBloomRange         := Def_Bloom_Range; // nm
    FBearing            := 0;
    FNumber             := Launcher_Number;
    FBloomAltitude      := Def_Bloom_Altitude; // feet
    FLauncherKind       := Launcher_Kind;
    FDelay              := 1; // second
    FSalvoSize          := 1;
    FChaffInstanceIndex := 0; // empty
    FMaxRange           := Max_Range;
    FMinRange           := Min_Range;
    FMaxElevation       := Max_Elevation;
    FMinElevation       := Min_Elevation;
    FLaunchSpeed        := Average_Launch_Spd;

    InstanceName        := 'Launcher ' + IntToStr(FNumber);
  end;

  if Assigned(FDeviceParent) and (FDeviceParent is TT3Vehicle) then
  begin
    FDelay     := TT3Vehicle(FDeviceParent).VehicleDefinition.Min_Delay_Between_Chaff_Rounds;
    FSalvoSize := TT3Vehicle(FDeviceParent).VehicleDefinition.Max_Chaff_Salvo_Size;
  end;
end;

procedure TT3MountedChaffLauncher.Move(const dt: Double);
begin
  inherited;

  if FState = 0 then
    exit; // Launcher idle;

  if FFirstLaunch then
  begin
//    FOnFire(Self);

    if not FIsFiring then
      FIsFiring := true;

//    if Assigned(OnLogEventStr) then
//      OnLogEventStr(InstanceName + ' launch chaff ');

    inc(FSalvoCounter);
    FFirstLaunch := false;
  end
  else
  begin
    FCounter := FCounter + dt;
    if FCounter >= FDelay then
    begin
//      if Assigned(FOnFire) then
//        FOnFire(Self);

//      if Assigned(OnLogEventStr) then
//        OnLogEventStr(InstanceName + ' launch chaff ');

      inc(FSalvoCounter);
      FCounter := 0;
    end;
  end;

  if FSalvoCounter = FSalvoSize then
  begin
//    if Assigned(FOnStopFire) then
//      FOnStopFire(Self);

    if FIsFiring then
      FIsFiring := false;

//    if Assigned(OnLogEventStr) then
//      OnLogEventStr(InstanceName + ' stop launch');

    FState := 0;
    FFirstLaunch := true;
  end;

  if Assigned(FChaffInstance) then
  begin
    if FChaffInstance.Quantity = 0 then begin
//      if Assigned(FOnStopFire) then
//        FOnStopFire(Self);

      if FIsFiring then
        FIsFiring := false;

//      if Assigned(OnLogEventStr) then
//        OnLogEventStr(InstanceName + ' stop launch');

      FState := 0;
      FFirstLaunch := true;
    end;

    if FChaffInstance.Quantity > 0 then begin
      FChaffInstance.Quantity := FChaffInstance.Quantity - 1;
    end;
  end
  else
  begin
//      if Assigned(FOnStopFire) then
//        FOnStopFire(Self);

    if FIsFiring then
      FIsFiring := false;

//      if Assigned(OnLogEventStr) then
//        OnLogEventStr(InstanceName + ' stop launch');

    FState := 0;
    FFirstLaunch := true;
  end;

end;

procedure TT3MountedChaffLauncher.SetBearing(const Value: double);
begin
  FBearing := Value;
end;

procedure TT3MountedChaffLauncher.SetBloomAltitude(const Value: double);
begin
  FBloomAltitude := Value;
end;

procedure TT3MountedChaffLauncher.SetBloomRange(const Value: double);
begin
  FBloomRange := Value;
end;

procedure TT3MountedChaffLauncher.SetChaffInstanceIndex(const Value: integer);
begin
  FChaffInstanceIndex := Value;

  simManager.FindT3ECMOnBoardID(PlatformParentInstanceIndex,FChaffInstanceIndex,3);
end;

procedure TT3MountedChaffLauncher.SetDelay(const Value: integer);
begin
  FDelay := Value;
end;

procedure TT3MountedChaffLauncher.SetNumber(const Value: integer);
begin
  FNumber := Value;
end;

procedure TT3MountedChaffLauncher.SetSalvoSize(const Value: integer);
begin
  FSalvoSize := Value;
end;

procedure TT3MountedChaffLauncher.SetState(const Value: integer);
begin
  FState := Value;
end;

end.
