unit uT3MountedChaff;

interface

uses uT3MountedECM, uSimObjects, uT3BlindZone, uDBClassDefinition,
  uBaseCoordSystem, uT3PlatformInstance, tttData, uT3Vehicle;

type
  TT3MountedChaff = class abstract (TT3MountedECM)
  private
    FQuantity: integer;
    procedure SetQuantity(const Value: integer);
    function getDefinition: TDBChaff_Definition;
    function getOnBoardDefinition: TDBChaff_On_Board;

  public
    constructor Create; override;
    destructor Destroy; override;

    procedure Initialize; override;

    { chaff db definition }
    property ChaffDefinition  : TDBChaff_Definition read getDefinition;
    property ChaffOnBoardDefinition  : TDBChaff_On_Board read getOnBoardDefinition;
    property Quantity: integer read FQuantity write SetQuantity;
  end;

implementation

uses
  uGlobalVar;

{ TT3MountedChaff }

constructor TT3MountedChaff.Create;
begin
  inherited;

end;

destructor TT3MountedChaff.Destroy;
begin

  inherited;
end;

function TT3MountedChaff.getDefinition: TDBChaff_Definition;
begin
  result := nil;

  if Assigned(FMountedDeviceDefinition) then
    Result := TDBChaff_Definition(FMountedDeviceDefinition);

end;

function TT3MountedChaff.getOnBoardDefinition: TDBChaff_On_Board;
begin
  result := nil;

  if Assigned(FMountedDeviceOnBoardDef) then
    Result := TDBChaff_On_Board(FMountedDeviceOnBoardDef);
end;

procedure TT3MountedChaff.Initialize;
begin
  inherited;

  { set definition once }
  FDeviceParent            := simManager.FindT3PlatformByID(PlatformParentInstanceIndex);
  FMountedDeviceDefinition := simManager.Scenario.GetDBChaffDefintion(MountedDeviceDefinitionIndex);
  FMountedDeviceOnBoardDef := simManager.Scenario.
        GetDBChaffOnBoardDef(TT3PlatformInstance(FDeviceParent).PlatformDefinitionIndex,
              MountedDeviceOnBoardDefIndex);

  if Assigned(ChaffOnBoardDefinition) then
  with ChaffOnBoardDefinition do
  begin

    InstanceName  := Instance_Identifier;
    InstanceIndex := Chaff_Instance_Index;
    Quantity      := Chaff_Qty_On_Board;

    if TT3Vehicle(PlatformParent).PlatformDomain = 0 then
      Category := ecAirborneChaff
    else
      Category := ecSurfaceChaff;
  end;

end;

procedure TT3MountedChaff.SetQuantity(const Value: integer);
begin
  FQuantity := Value;
end;

end.
