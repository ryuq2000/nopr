unit uT3MountedAirborneChaff;

interface

uses uT3MountedECM, uSimObjects, uT3BlindZone, uDBClassDefinition,
  uBaseCoordSystem, uT3PlatformInstance, tttData, uT3Vehicle, uT3MountedChaff;

type
  TEnumAirborneType = (atBarrier, atHelicopter, atSeduction);
  TT3MountedAirborneChaff = class (TT3MountedChaff)
  private
    FDeploymentType: TEnumAirborneType;
    procedure SetDeploymentType(const Value: TEnumAirborneType);
  published
  public
    constructor Create; override;
    destructor Destroy; override;

    procedure Initialize; override;
    property DeploymentType : TEnumAirborneType read FDeploymentType write SetDeploymentType;

  end;

implementation

{ TT3MountedAirborneChaff }

constructor TT3MountedAirborneChaff.Create;
begin
  inherited;

end;

destructor TT3MountedAirborneChaff.Destroy;
begin

  inherited;
end;

procedure TT3MountedAirborneChaff.Initialize;
begin
  inherited;

  FDeploymentType := atBarrier;

end;

procedure TT3MountedAirborneChaff.SetDeploymentType(
  const Value: TEnumAirborneType);
begin
  FDeploymentType := Value;
end;

end.
