unit uT3MountedAcousticDeploy;

interface

uses uT3MountedECM, uSimObjects, uT3BlindZone, uDBClassDefinition,
  uBaseCoordSystem, uT3PlatformInstance, tttData, uT3Vehicle;

type

  TT3MountedAcousticDeploy = class (TT3MountedECM)
  private
    FFilterSetting    : integer;
    FMode             : TECMAcousticDecoyMode;
    FControl          : TECMControlActivation;
    FDeploymentAction : TECMDeploymentAction;
    FCycleTimer       : TECMCycleTimer;

    function getDefinition: TDBAcoustic_Decoy_Definition;
    function getOnBoardDefinition: TDBAcoustic_Decoy_On_Board;
    procedure SetControl(const Value: TECMControlActivation);
    procedure SetCycleTimer(const Value: TECMCycleTimer);
    procedure SetDeploymentAction(const Value: TECMDeploymentAction);
    procedure SetFilterSetting(const Value: integer);
    procedure SetMode(const Value: TECMAcousticDecoyMode);
  published

  public
    constructor Create; override;

    procedure   Initialize; override;
    procedure   Move(const aDeltaMs: double); override;

    { acoustic db definition }
    property AcousticDefinition  : TDBAcoustic_Decoy_Definition read getDefinition;
    property AcousticOnBoardDefinition  : TDBAcoustic_Decoy_On_Board read getOnBoardDefinition;

    { deployment action : deploy, stow}
    property DeploymentAction : TECMDeploymentAction read FDeploymentAction
       write SetDeploymentAction;
    { control activation : on, off}
    property Control: TECMControlActivation read FControl write SetControl;
    { control cycle timer : on, off}
    property CycleTimer: TECMCycleTimer read FCycleTimer write SetCycleTimer;
    { control mode }
    property Mode: TECMAcousticDecoyMode read FMode write SetMode;
    { control filter setting }
    property FilterSetting: integer read FFilterSetting write SetFilterSetting;

  end;

implementation

uses uGlobalVar;

{ TT3MountedAcousticDeploy }

constructor TT3MountedAcousticDeploy.Create;
begin
  inherited;

end;

function TT3MountedAcousticDeploy.getDefinition: TDBAcoustic_Decoy_Definition;
begin
  result := nil;

  if Assigned(FMountedDeviceDefinition) then
    Result := TDBAcoustic_Decoy_Definition(FMountedDeviceDefinition);
end;

function TT3MountedAcousticDeploy.getOnBoardDefinition: TDBAcoustic_Decoy_On_Board;
begin
  result := nil;

  if Assigned(FMountedDeviceOnBoardDef) then
    Result := TDBAcoustic_Decoy_On_Board(FMountedDeviceDefinition);
end;

procedure TT3MountedAcousticDeploy.Initialize;
begin
  inherited;

  { set definition once }
  FDeviceParent            := simManager.FindT3PlatformByID(PlatformParentInstanceIndex);
  FMountedDeviceDefinition := simManager.Scenario.GetDBAcousticDefintion(MountedDeviceDefinitionIndex);
  FMountedDeviceOnBoardDef := simManager.Scenario.
        GetDBAcousticOnBoardDef(TT3PlatformInstance(FDeviceParent).PlatformDefinitionIndex,
              MountedDeviceOnBoardDefIndex);

  if Assigned(AcousticOnBoardDefinition) then
  with AcousticOnBoardDefinition do begin
    InstanceIndex := Acoustic_Instance_Index;
    InstanceName  := Instance_Identifier;
    Category      := ecAcousticDecoy;
  end;

end;

procedure TT3MountedAcousticDeploy.Move(const aDeltaMs: double);
begin
  inherited;

end;

procedure TT3MountedAcousticDeploy.SetControl(
  const Value: TECMControlActivation);
begin
  FControl := Value;
end;

procedure TT3MountedAcousticDeploy.SetCycleTimer(const Value: TECMCycleTimer);
begin
  FCycleTimer := Value;
end;

procedure TT3MountedAcousticDeploy.SetDeploymentAction(
  const Value: TECMDeploymentAction);
begin
  FDeploymentAction := Value;
end;

procedure TT3MountedAcousticDeploy.SetFilterSetting(const Value: integer);
begin
  FFilterSetting := Value;
end;

procedure TT3MountedAcousticDeploy.SetMode(const Value: TECMAcousticDecoyMode);
begin
  FMode := Value;
end;

end.
