unit uT3MountedAirBubble;

interface

uses uT3MountedECM, uSimObjects, uT3BlindZone, uDBClassDefinition,
  uBaseCoordSystem, uT3PlatformInstance, tttData, uT3Vehicle;

type
  TT3MountedAirbubble = class (TT3MountedECM)
  private
    FQuantity: integer;
    function getDefinition: TDBAir_Bubble_Definition;
    function getOnBoardDefinition: TDBAir_Bubble_Mount;
    procedure SetQuantity(const Value: integer);
  published

  public
    constructor Create; override;
    destructor Destroy; override;

    procedure Initialize; override;
    procedure Move(const dt : Double); override;

    { air bubble db definition }
    property AirBubbleDefinition  : TDBAir_Bubble_Definition read getDefinition;
    property AirBubbleOnBoardDefinition  : TDBAir_Bubble_Mount read getOnBoardDefinition;

    property Quantity : integer read FQuantity write SetQuantity;
  end;

implementation

uses uGlobalVar;

{ TT3MountedAirbubble }

constructor TT3MountedAirbubble.Create;
begin
  inherited;

end;

destructor TT3MountedAirbubble.Destroy;
begin

  inherited;
end;

function TT3MountedAirbubble.getDefinition: TDBAir_Bubble_Definition;
begin
  result := nil;

  if Assigned(FMountedDeviceDefinition) then
    Result := TDBAir_Bubble_Definition(FMountedDeviceDefinition);
end;

function TT3MountedAirbubble.getOnBoardDefinition: TDBAir_Bubble_Mount;
begin
  result := nil;

  if Assigned(FMountedDeviceOnBoardDef) then
    Result := TDBAir_Bubble_Mount(FMountedDeviceOnBoardDef);
end;

procedure TT3MountedAirbubble.Initialize;
begin
  inherited;

  { set definition once }
  FDeviceParent            := simManager.FindT3PlatformByID(PlatformParentInstanceIndex);
  FMountedDeviceDefinition := simManager.Scenario.GetDBAirBubbleDefintion(MountedDeviceDefinitionIndex);
  FMountedDeviceOnBoardDef := simManager.Scenario.
        GetDBAirBubbleOnBoardDef(TT3PlatformInstance(FDeviceParent).PlatformDefinitionIndex,
              MountedDeviceOnBoardDefIndex);

  if Assigned(AirBubbleOnBoardDefinition) then
  with AirBubbleOnBoardDefinition do begin
    InstanceIndex := Air_Bubble_Instance_Index;
    InstanceName  := Instance_Identifier;
    Quantity      := Quantity;
    Category      := ecBubble;
  end;
end;

procedure TT3MountedAirbubble.Move(const dt : Double);
begin
  inherited;

end;

procedure TT3MountedAirbubble.SetQuantity(const Value: integer);
begin
  FQuantity := Value;
end;

end.
