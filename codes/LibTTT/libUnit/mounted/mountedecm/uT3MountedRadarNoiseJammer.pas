unit uT3MountedRadarNoiseJammer;

interface

uses
  uT3MountedECM, uT3PlatformInstance, uDBClassDefinition,
  uT3MountedRadar, uSimObjects;

type
  TNoiseJammerTargeting = (ettNJBarrage, ettNJTrack, ettNJSpotNum, ettNJFreq);
  TT3MountedRadarNoiseJammer = class(TT3MountedECM)
  private
    FJamTargeting        : TNoiseJammerTargeting;
    FJamCenterFreq       : Double;

    FJamBearing          : Double;
    FJamBandWidth        : Double;
    FJamReqTargetRadarId : integer;
    FJamSpotNumber       : Integer;

    FRangeEffective      : Double;

    function getDefinition: TDBJammer_Definition;
    function getOnBoardDefinition: TDBJammer_On_Board;

    function InsideSuitTarget(aObject : TT3MountedRadar) : boolean;
    function InsideSpotNumber(aObject : TT3MountedRadar) : boolean;
    function InsideJamRange(aObject : TT3MountedRadar) : boolean;
    function InsideBearing(aObject : TT3MountedRadar) : boolean;
    function InsideFreqRange(aObject : TT3MountedRadar) : boolean;
    function InsideSectorWidth(aObject : TT3MountedRadar) : boolean;
    function InsideVerticalLimits(aObject : TSimObject) : boolean;
    function CeckJammResistant(aObject : TT3MountedRadar) : boolean;
    function CeckAntiJammCapable(aObject : TT3MountedRadar) : boolean;
    function EfffectiveRange: Double;

    procedure SetJamTargeting(const Value: TNoiseJammerTargeting);
    procedure SetJamBandWidth(const Value: Double);
    procedure SetJamBearing(const Value: Double);
    procedure SetJamCenterFreq(const Value: Double);

    procedure SetJamReqTargetRadarId(const Value: integer);
    procedure SetJamSpotNumber(const Value: Integer);


  public
    constructor Create; override;
    destructor  Destroy; override;

    procedure Initialize; override;
    procedure Move(const aDeltaMs: double); override;

    { noise jammer db definition }
    property JammerDefinition  : TDBJammer_Definition read getDefinition;
    property JammerOnBoardDefinition  : TDBJammer_On_Board read getOnBoardDefinition;

    { from operator form control }
    property JamTargeting  : TNoiseJammerTargeting read FJamTargeting write SetJamTargeting
     default ettNJBarrage;
    property JamCenterFreq : Double read FJamCenterFreq write SetJamCenterFreq;
    property JamBandWidth  : Double read FJamBandWidth write SetJamBandWidth;

    { barrage mode operator input }
    property JamBearing    : Double read FJamBearing write SetJamBearing;

    { track mode operator input }
    property JamTrackInstanceIndex : integer read FJamReqTargetRadarId write SetJamReqTargetRadarId default 0;

    { spot mode operator input }
    property JamSpotNumber : Integer read FJamSpotNumber write SetJamSpotNumber default 10000;
  end;

implementation

uses
  uGlobalVar, tttData, uBaseCoordSystem, uT3Vehicle,
  Math;

{ TT3MountedRadarNoiseJammer }

function TT3MountedRadarNoiseJammer.CeckAntiJammCapable(
  aObject: TT3MountedRadar): boolean;
begin
  { cek apakah tipe ANTI JAMMER RESISTANT radar target melawan JAMMER TYPE dari jammer  }
  if aObject.RadarDefinition.Anti_Jamming_Capable then
  begin
    if JammerDefinition.Jammer_Type = 0 then  //anti jammer tyoe A
    begin
      if aObject.RadarDefinition.Anti_Jamming_A_Resistant then
        Result := True
      else
        Result := False;
    end
    else if JammerDefinition.Jammer_Type = 1 then   //anti jammer Type B
    begin
      if aObject.RadarDefinition.Anti_Jamming_B_Resistant then
        Result := True
      else
        Result := False;
    end
    else if JammerDefinition.Jammer_Type = 2 then   //anti jammer Type C then
    begin
      if aObject.RadarDefinition.Anti_Jamming_C_Resistant then
        Result := True
      else
        Result := False;
    end
    else
    begin
      Result := False;
    end;
  end
  else
    Result := False;
end;

function TT3MountedRadarNoiseJammer.CeckJammResistant(
  aObject: TT3MountedRadar): boolean;
begin
  { cek apakah tipe JAMMER RESISTANT radar target = JAMMER TYPE dari jammer }
  if JammerDefinition.Jammer_Type = 0 then  //jammer tyoe A
  begin
    if aObject.RadarDefinition.Jamming_A_Resistant then
      Result := True
    else
      Result := False;
  end
  else if JammerDefinition.Jammer_Type = 1 then   //jammer Type B
  begin
    if aObject.RadarDefinition.Jamming_B_Resistant then
      Result := True
    else
      Result := False;
  end
  else if JammerDefinition.Jammer_Type = 2 then   //jammer Type C then
  begin
    if aObject.RadarDefinition.Jamming_C_Resistant then
      Result := True
    else
      Result := False;
  end
  else
    Result := False;

end;

constructor TT3MountedRadarNoiseJammer.Create;
begin
  inherited;

  Status   := esOff;
  FRangeEffective := 0 ;//inisialisasi aja
//  FTargetJamming := TList.Create;

  JamBearing    := 0;
  JamCenterFreq := 0;
  JamBandWidth  := 0;
end;

destructor TT3MountedRadarNoiseJammer.Destroy;
begin
//  FTargetJamming.Free;

  inherited;
end;

function TT3MountedRadarNoiseJammer.EfffectiveRange : Double;
var
  RangePowerFactor : Double;
  Bandwidth : Double;
  EffectivePower : Double;
begin
  Result := 0;

  RangePowerFactor := JammerDefinition.Jammer_Power_Density
                 / (JammerDefinition.Max_Effective_Range * JammerDefinition.Max_Effective_Range);

  Bandwidth := (JammerDefinition.Upper_Freq_Limit - JammerDefinition.Lower_Freq_Limit) / 10000;  {sementara di bagi 10000 utk}

  EffectivePower := JammerDefinition.Jammer_Power_Density / Bandwidth;

  Result := Sqrt(Abs(EffectivePower / RangePowerFactor));
end;

function TT3MountedRadarNoiseJammer.getDefinition: TDBJammer_Definition;
begin
  result := nil;

  if Assigned(FMountedDeviceDefinition) then
    Result := TDBJammer_Definition(FMountedDeviceDefinition);
end;

function TT3MountedRadarNoiseJammer.getOnBoardDefinition: TDBJammer_On_Board;
begin
  result := nil;

  if Assigned(FMountedDeviceOnBoardDef) then
    Result := TDBJammer_On_Board(FMountedDeviceOnBoardDef);
end;

procedure TT3MountedRadarNoiseJammer.Initialize;
begin
  inherited;

  { set definition once }
  FDeviceParent            := simManager.FindT3PlatformByID(PlatformParentInstanceIndex);
  FMountedDeviceDefinition := simManager.Scenario.GetDBSelfJammerDefintion(MountedDeviceDefinitionIndex);
  FMountedDeviceOnBoardDef := simManager.Scenario.
        GetDBJammerOnBoardDef(TT3PlatformInstance(FDeviceParent).PlatformDefinitionIndex,
              MountedDeviceOnBoardDefIndex);

  if Assigned(JammerOnBoardDefinition) then
  with JammerOnBoardDefinition do begin
    InstanceIndex := Jammer_Instance_Index;
    InstanceName  := Instance_Identifier;
    Category      := ecRadarNoiseJammer;
  end;

  if Assigned(JammerDefinition) then
    FRangeEffective := EfffectiveRange;
end;

function TT3MountedRadarNoiseJammer.InsideBearing(aObject: TT3MountedRadar): boolean;
var
  bearing : Double;
begin
  { Cek bearing target = bearing jammer request }
  bearing := CalcBearing(FPosition.X, FPosition.Y,
             aObject.getPositionX, aObject.getPositionY);

  if JamTargeting <> ettNJTrack then
  begin
    if Round(bearing) = Round(JamBearing) then
      result := True
    else begin
      result := False;
    end;
  end
  else
    result := True;
end;

function TT3MountedRadarNoiseJammer.InsideFreqRange(
  aObject: TT3MountedRadar): boolean;
var
  FreqRadar, UpperFreqReq, LowerFreqReq : Double;
begin
  { cek frequensinya apakah masuk dalam batas UPPER dan LOWER frequensi limit jammmer }
  FreqRadar := TT3MountedRadar(aObject).RadarDefinition.Frequency;

  { untuk tipe jammer frequency dan jammer barrage }
  if (JamTargeting = ettNJFreq) or (JamTargeting = ettNJBarrage) then
  begin
    UpperFreqReq := JamCenterFreq + (JamBandWidth/2);
    LowerFreqReq := JamCenterFreq - (JamBandWidth/2);
    if (FreqRadar <= UpperFreqReq) and (FreqRadar >= LowerFreqReq)then
      Result := True
    else
      Result := False;
  end
  else
  { untuk tipe jammer track dan jammer spot number }
  begin
    if (FreqRadar <= JammerDefinition.Upper_Freq_Limit) and (FreqRadar >= JammerDefinition.Lower_Freq_Limit)then
      Result := True
    else
      Result := False;
  end;
end;

function TT3MountedRadarNoiseJammer.InsideJamRange(
  aObject: TT3MountedRadar): boolean;
var
  range, EfffectiveJammerRange : double;
begin
  range := CalcRange(FPosition.X,FPosition.Y,
                     aObject.getPositionX,aObject.getPositionY);

  result := range <= FRangeEffective;
end;

function TT3MountedRadarNoiseJammer.InsideSectorWidth(
  aObject: TT3MountedRadar): boolean;
var
  bearing, HeadingJam, StartAngleJam, EndAngleJam : Double;
begin
  { Cek apakah target berada di dalam batas MAKSIMUM SEKTOR WIDTH }
  bearing := CalcBearing(FPosition.X, FPosition.Y,
             aObject.getPositionX, aObject.getPositionY);

  if Assigned(PlatformParent) then
    HeadingJam    := TT3Vehicle(PlatformParent).Heading
  else
   HeadingJam    := 0;

  EndAngleJam   := HeadingJam + (JammerDefinition.Max_Sector_Width/2);
  StartAngleJam := HeadingJam - (JammerDefinition.Max_Sector_Width/2);

  if EndAngleJam > 360 then
    EndAngleJam := EndAngleJam - 360;

  if StartAngleJam < 0 then
    StartAngleJam := StartAngleJam + 360;

  if DegComp_IsBeetwen(bearing, StartAngleJam, EndAngleJam) then
    result := True
  else
    result := False;
end;

function TT3MountedRadarNoiseJammer.InsideSpotNumber(
  aObject: TT3MountedRadar): boolean;
begin
  if JamTargeting = ettNJSpotNum then
  begin
    if aObject.SpotNumber = Self.JamSpotNumber then
      Result := True
    else begin
      Result := False;
    end;
  end
  else
    Result := True;

end;

function TT3MountedRadarNoiseJammer.InsideSuitTarget(
  aObject : TT3MountedRadar): boolean;
begin
  if JamTargeting = ettNJTrack then
  begin
    if aObject.PlatformParentInstanceIndex = Self.JamTrackInstanceIndex then
      Result := True
    else begin
      Result := False;
    end;
  end else
    Result := True;

end;

function TT3MountedRadarNoiseJammer.InsideVerticalLimits(
  aObject: TSimObject): boolean;
var
  RangeJammer, vAltitude, minAltitude, maxAltitude, sumLower,
  sumUpper, degLower, degUpper : Double;
begin
  { cek apakah target berada di dalam VERTICAL COVERAGE LIMITS }
  degLower := JammerDefinition.Lower_Vert_Coverage_Angle;
  degUpper := JammerDefinition.Upper_Vert_Coverage_Angle;

  sumLower := Abs(FRangeEffective*(Tan(degLower)));
  sumUpper := Abs(FRangeEffective*(TAn(degUpper)));

  vAltitude := JammerOnBoardDefinition.Antenna_Height + TT3Vehicle(aObject).Altitude;
  minAltitude := vAltitude - sumLower;
  maxAltitude := vAltitude + sumUpper;

  if (vAltitude >= minAltitude) and (vAltitude <= maxAltitude) then
    Result := True
  else
    Result := False;
end;

procedure TT3MountedRadarNoiseJammer.Move(const aDeltaMs: double);
var
  I, count : integer;
  item : TObject;
  isInSuitTarget, isInSpotNumber, isInBearing, isInRange,
  isInFreqRange, isInSectorWidth, isInVerticalLimits,
  isJammResistant, isAntiJammCapable : boolean;
  allState : boolean;
  //pfItemParent : TT3PlatformInstance;
begin
  inherited;

  if Status = esOff then
    exit;

  count := simManager.SimRadarsOnBoards.itemCount;
  for I := 0 to count - 1 do
  begin
    item := simManager.SimRadarsOnBoards.getObject(i);

    if item is TT3MountedRadar then
    begin
      if not (TT3MountedRadar(item).OperationalStatus = sopOn) then
        Continue;

      if TT3MountedRadar(item).PlatformParentInstanceIndex = Self.PlatformParentInstanceIndex then
        Continue;

      //pfItemParent := simManager.FindT3PlatformByID(TT3MountedRadar(item).PlatformParentInstanceIndex);
      if not Assigned(TT3MountedRadar(item).PlatformParent) then
        Continue;

      { Ceck parent jammer selected target}
      isInSuitTarget      := InsideSuitTarget(TT3MountedRadar(item));
      { Ceck spot number target }
      isInSpotNumber      := InsideSpotNumber(TT3MountedRadar(item));
      { Ceck bearing target with bearing jammer }
      isInBearing         := InsideBearing(TT3MountedRadar(item));
      { Ceck in the range of jammer }
      isInRange           := InsideJamRange(TT3MountedRadar(item));
      { Ceck in the range of jammer frequency}
      isInFreqRange       := InsideFreqRange(TT3MountedRadar(item));
      { Ceck in the sector width of jammer }
      isInSectorWidth     := InsideSectorWidth(TT3MountedRadar(item));
      { Ceck in the vertcal coverage limits of jammer }
      isInVerticalLimits  := InsideVerticalLimits(TT3MountedRadar(item).PlatformParent);
      { Ceck radar jamming resistant }
      isJammResistant     := CeckJammResistant(TT3MountedRadar(item));
      { Ceck radar anti jamming capable }
      isAntiJammCapable   := CeckAntiJammCapable(TT3MountedRadar(item));

      { periksa semua factor deteksi }
      allState := isInSuitTarget and isInSpotNumber and isInBearing
                  and isInRange and isInFreqRange and isInSectorWidth
                  and isInVerticalLimits and isJammResistant
                  and not(isAntiJammCapable);

      { if allState then begin }
      //ApplyJammedEffect(item, allState);
    end;
  end;

end;

procedure TT3MountedRadarNoiseJammer.SetJamBandWidth(const Value: Double);
begin
  FJamBandWidth := Value;
end;

procedure TT3MountedRadarNoiseJammer.SetJamBearing(const Value: Double);
begin
  FJamBearing := Value;
end;

procedure TT3MountedRadarNoiseJammer.SetJamCenterFreq(const Value: Double);
begin
  FJamCenterFreq := Value;
end;

procedure TT3MountedRadarNoiseJammer.SetJamReqTargetRadarId(
  const Value: integer);
begin
  FJamReqTargetRadarId := Value;
end;

procedure TT3MountedRadarNoiseJammer.SetJamSpotNumber(const Value: Integer);
begin
  FJamSpotNumber := Value;
end;

procedure TT3MountedRadarNoiseJammer.SetJamTargeting(
  const Value: TNoiseJammerTargeting);
begin
  FJamTargeting := Value;
end;

end.
