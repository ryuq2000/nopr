unit uT3MountedFloatingDecoy;

interface

uses uT3MountedECM, uSimObjects, uT3BlindZone, uDBClassDefinition,
  uBaseCoordSystem, uT3PlatformInstance, tttData, uT3Vehicle;

type
  TT3MountedFloatingDecoy = class (TT3MountedECM)
  private
    FQuantity: integer;
    function getDefinition: TDBFloating_Decoy_Definition;
    function getOnBoardDefinition: TDBFloating_Decoy_On_Board;
    procedure SetQuantity(const Value: integer);
  published

  public
    constructor Create; override;
    destructor Destroy; override;

    procedure Initialize; override;
    procedure Move(const dt : Double); override;

    { floating decoy db definition }
    property FloatingDecoyDefinition  : TDBFloating_Decoy_Definition read getDefinition;
    property FloatingDecoyOnBoardDefinition  : TDBFloating_Decoy_On_Board read getOnBoardDefinition;

    property Quantity : integer read FQuantity write SetQuantity;
  end;

implementation

uses uGlobalVar;

{ TT3MountedFloatingDecoy }

constructor TT3MountedFloatingDecoy.Create;
begin
  inherited;

end;

destructor TT3MountedFloatingDecoy.Destroy;
begin

  inherited;
end;

function TT3MountedFloatingDecoy.getDefinition: TDBFloating_Decoy_Definition;
begin
  result := nil;

  if Assigned(FMountedDeviceDefinition) then
    Result := TDBFloating_Decoy_Definition(FMountedDeviceDefinition);

end;

function TT3MountedFloatingDecoy.getOnBoardDefinition: TDBFloating_Decoy_On_Board;
begin
  result := nil;

  if Assigned(FMountedDeviceOnBoardDef) then
    Result := TDBFloating_Decoy_On_Board(FMountedDeviceOnBoardDef);
end;

procedure TT3MountedFloatingDecoy.Initialize;
begin
  inherited;

  { set definition once }
  FDeviceParent            := simManager.FindT3PlatformByID(PlatformParentInstanceIndex);
  FMountedDeviceDefinition := simManager.Scenario.GetDBFloatingDecoyDefintion(MountedDeviceDefinitionIndex);
  FMountedDeviceOnBoardDef := simManager.Scenario.
        GetDBFloatingDecoyOnBoardDef(TT3PlatformInstance(FDeviceParent).PlatformDefinitionIndex,
              MountedDeviceOnBoardDefIndex);

  if Assigned(FloatingDecoyOnBoardDefinition) then
  with FloatingDecoyOnBoardDefinition do begin
    InstanceIndex := Floating_Decoy_Instance_Index;
    InstanceName  := Instance_Identifier;
    Quantity      := Quantity;
    Category      := ecFloatingDecoy;
  end;

end;

procedure TT3MountedFloatingDecoy.Move(const dt: Double);
begin
  inherited;

end;

procedure TT3MountedFloatingDecoy.SetQuantity(const Value: integer);
begin
  FQuantity := Value;
end;

end.
