unit uT3MountedTowedJammer;

interface

uses uT3MountedECM, uSimObjects, uT3BlindZone, uDBClassDefinition,
  uBaseCoordSystem, uT3PlatformInstance, tttData, uT3Vehicle;

type
  TT3MountedTowedJammer = class (TT3MountedECM)
  private
    FQuantity           : integer;
    FDeploymentAction   : TECMDeploymentAction;
    FOrderedTowedLength : integer;
    FTargeting          : TECMTargeting;
    FBearing            : integer;
    FActualTOwedLength  : integer;
    FSpotNumber         : Integer;

    function getDefinition: TDBTowed_Jammer_Decoy_Definition;
    function getOnBoardDefinition: TDBTowed_Jammer_Decoy_On_Board;
    procedure SetQuantity(const Value: integer);
    procedure SetDeploymentAction(const Value: TECMDeploymentAction);
    procedure SetActualTOwedLength(const Value: integer);
    procedure SetBearing(const Value: integer);
    procedure SetOrderedTowedLength(const Value: integer);
    procedure SetTargeting(const Value: TECMTargeting);
    procedure SetSpotNumber(const Value: Integer);
  published

  public
    constructor Create; override;
    destructor Destroy; override;

    procedure Initialize; override;
    procedure Move(const dt : Double); override;

    { towed jammer db definition }
    property TowedJammerDefinition  : TDBTowed_Jammer_Decoy_Definition read getDefinition;
    property TowedJammerOnBoardDefinition  : TDBTowed_Jammer_Decoy_On_Board read getOnBoardDefinition;

    property Quantity           : integer read FQuantity write SetQuantity;
    property DeploymentAction   : TECMDeploymentAction read FDeploymentAction write SetDeploymentAction;
    property Bearing            : integer read FBearing write SetBearing;
    property OrderedTowedLength : integer read FOrderedTowedLength write SetOrderedTowedLength;
    property ActualTOwedLength  : integer read FActualTOwedLength write SetActualTOwedLength;
    property Targeting          : TECMTargeting read FTargeting write SetTargeting;
    property SpotNumber         : Integer read FSpotNumber write SetSpotNumber;
  end;

implementation

uses
  uGlobalVar;

{ TT3MountedTowedJammer }

constructor TT3MountedTowedJammer.Create;
begin
  inherited;

end;

destructor TT3MountedTowedJammer.Destroy;
begin

  inherited;
end;

function TT3MountedTowedJammer.getDefinition: TDBTowed_Jammer_Decoy_Definition;
begin
  result := nil;

  if Assigned(FMountedDeviceDefinition) then
    Result := TDBTowed_Jammer_Decoy_Definition(FMountedDeviceDefinition);
end;

function TT3MountedTowedJammer.getOnBoardDefinition: TDBTowed_Jammer_Decoy_On_Board;
begin
  result := nil;

  if Assigned(FMountedDeviceOnBoardDef) then
    Result := TDBTowed_Jammer_Decoy_On_Board(FMountedDeviceOnBoardDef);
end;

procedure TT3MountedTowedJammer.Initialize;
begin
  inherited;

  { set definition once }
  FDeviceParent            := simManager.FindT3PlatformByID(PlatformParentInstanceIndex);
  FMountedDeviceDefinition := simManager.Scenario.GetDBTowedJammerDefintion(MountedDeviceDefinitionIndex);
  FMountedDeviceOnBoardDef := simManager.Scenario.
        GetDBTowedJammerOnBoardDef(TT3PlatformInstance(FDeviceParent).PlatformDefinitionIndex,
              MountedDeviceOnBoardDefIndex);

  if Assigned(TowedJammerOnBoardDefinition) then
  with TowedJammerOnBoardDefinition do begin
    InstanceIndex := Towed_Decoy_Instance_Index;
    InstanceName  := Instance_Identifier;
    Quantity      := Quantity;
    Category      := ecTowedJammer;
  end;

end;

procedure TT3MountedTowedJammer.Move(const dt: Double);
begin
  inherited;

end;

procedure TT3MountedTowedJammer.SetActualTOwedLength(const Value: integer);
begin
  FActualTOwedLength := Value;
end;

procedure TT3MountedTowedJammer.SetBearing(const Value: integer);
begin
  FBearing := Value;
end;

procedure TT3MountedTowedJammer.SetDeploymentAction(
  const Value: TECMDeploymentAction);
begin
  FDeploymentAction := Value;
end;

procedure TT3MountedTowedJammer.SetOrderedTowedLength(const Value: integer);
begin
  FOrderedTowedLength := Value;
end;

procedure TT3MountedTowedJammer.SetQuantity(const Value: integer);
begin
  FQuantity := Value;
end;

procedure TT3MountedTowedJammer.SetSpotNumber(const Value: Integer);
begin
  FSpotNumber := Value;
end;

procedure TT3MountedTowedJammer.SetTargeting(const Value: TECMTargeting);
begin
  FTargeting := Value;
end;

end.
