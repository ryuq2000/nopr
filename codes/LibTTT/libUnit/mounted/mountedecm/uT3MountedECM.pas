unit uT3MountedECM;

interface

uses
  uT3MountedDevice, uT3BlindZone, tttData, Generics.Collections,
  uSimObjects, uT3SimContainer, Classes,SysUtils;

type
  TECMCategory          = (ecAcousticDecoy, ecAirborneChaff, ecFloatingDecoy,
    ecTowedJammer, ecSurfaceChaff, ecOnBoardDefence, ecBubble, ecInfraredDecoy,
    ecRadarNoiseJammer);    //jammer noise;
  TECMStatus            = (esAvailable, esLaunchingChaff, esUnavailable, esDamaged, esOn,
    esOff, esEMCON, esAutomatic, esManual, esDeployed, esStowed,
    esBlooming, esSustain, esDissipating);
  TECMTargeting         = (ettTrack, ettSpot);
  TECMDeploymentAction  = (edaDeploy, edaStow);
  TECMControlActivation = (ecaOn, ecaOff);
  TECMCycleTimer        = (ectOn, ectOff);
  TECMAcousticDecoyMode = (eamSweptFreq, eamNoise, eamPulsedNoise,eamAlternating);

  TT3MountedECM = class abstract (TT3MountedDevice)
  private
    FCategory: TECMCategory;
    FStatus: TECMStatus;
    procedure SetCategory(const Value: TECMCategory);
    procedure SetStatus(const Value: TECMStatus);

  public
    procedure   Initialize; override;
    procedure   Move(const aDeltaMs: double); override;

    property Category : TECMCategory read FCategory write SetCategory;
    property Status   : TECMStatus read FStatus write SetStatus;
  end;


implementation

{ TT3MountedECM }

procedure TT3MountedECM.Initialize;
begin
  inherited;

end;

procedure TT3MountedECM.Move(const aDeltaMs: double);
begin
  inherited;

end;

procedure TT3MountedECM.SetCategory(const Value: TECMCategory);
begin
  FCategory := Value;
end;

procedure TT3MountedECM.SetStatus(const Value: TECMStatus);
begin
  FStatus := Value;
end;

end.
