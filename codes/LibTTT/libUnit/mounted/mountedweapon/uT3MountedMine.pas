unit uT3MountedMine;

interface

uses uT3MountedWeapon, tttData, uDBClassDefinition, uT3PlatformInstance;

type
  TMineCategory = (mcAcoustic, mcImpact, mcMagnetic, mcPressure);
  TMineMooring  = (mmFixed, mmFloating);
  TMineDetectability = (mdNormalDetection, mdundetectable, mdPassiveDetection, mdAlwaysVisible);

  TT3MountedMine = class (TT3MountedWeapon)
  private
    FMineCategory      : TMineCategory;
    FMineMooring       : TMineMooring;
    FMineDetectability : TMineDetectability;
    FDepth             : integer;


    function getDefinition: TDBMine_Definition;
    procedure SetDepth(const Value: integer);
    function getMountedDefinition: TDBFitted_Weapon_On_Board;
  public
    constructor Create; override;
    destructor Destroy;override;

    procedure Initialize;override;

    property MineDefinition : TDBMine_Definition read getDefinition;
    property MountedMineDefinition : TDBFitted_Weapon_On_Board read getMountedDefinition;

    property Depth : integer read FDepth write SetDepth;

  end;

implementation

uses
  uGlobalVar;

{ TT3MountedMine }

constructor TT3MountedMine.Create;
begin
  inherited;

end;

destructor TT3MountedMine.Destroy;
begin

  inherited;
end;

function TT3MountedMine.getDefinition: TDBMine_Definition;
begin
  result := nil;

  if Assigned(MountedDeviceDefinition) then
    result := TDBMine_Definition(MountedDeviceDefinition);

end;

function TT3MountedMine.getMountedDefinition: TDBFitted_Weapon_On_Board;
begin
  result := nil;

  if Assigned(MountedDeviceOnBoardDef) then
    result := TDBFitted_Weapon_On_Board(MountedDeviceOnBoardDef);

end;

procedure TT3MountedMine.Initialize;
begin
  inherited;

  { set definition once }
  FDeviceParent            := simManager.FindT3PlatformByID(PlatformParentInstanceIndex);
  FMountedDeviceDefinition := simManager.Scenario.GetDBMineDefintion(MountedDeviceDefinitionIndex);
  FMountedDeviceOnBoardDef := simManager.Scenario.
        GetDBFittedWeaponOnBoardDef(TT3PlatformInstance(FDeviceParent).PlatformDefinitionIndex,
              MountedDeviceOnBoardDefIndex);

  if Assigned(MountedMineDefinition) then
  with MountedMineDefinition do begin

    InstanceName  := Instance_Identifier;
    InstanceIndex := Fitted_Weap_Index;
    Quantity      := MountedMineDefinition.Quantity;
  end;

  if Assigned(MineDefinition) then
  with MineDefinition do begin
    {Masuk Category Mine apa}
    case Platform_Category of
      2 : FMineCategory := mcAcoustic;
      3 : FMineCategory := mcImpact;
      4 : FMineCategory := mcMagnetic;
      5 : FMineCategory := mcPressure;
    end;

    {Masuk tipe Mooring Mine apa}
    case Mooring_Type of
      0 : FMineMooring := mmFixed;
      1 : FMineMooring := mmFloating;
    end;

    {Masuk tipe Detectability Mine apa}
    case Detectability_Type of
      0 : FMineDetectability := mdNormalDetection;
      1 : FMineDetectability := mdundetectable;
      2 : FMineDetectability := mdPassiveDetection;
      3 : FMineDetectability := mdAlwaysVisible;
    end;

    WeaponCategory    := wcMine;
    WeaponRange       := Engagement_Range;
  end;

end;

procedure TT3MountedMine.SetDepth(const Value: integer);
begin
  FDepth := Value;
end;

end.
