unit uT3MountedGunAutoManual;

interface

uses uT3MountedGun, tttData,uDBClassDefinition, uT3BlindZone;

type
  TT3MountedGunAutoManual = class (TT3MountedGun)
  private
    FThresholdSpeed: double;
    FInterceptRange: double;
    procedure SetInterceptRange(const Value: double);
    procedure SetThresholdSpeed(const Value: double);
  published
  public
    constructor Create; override;
    destructor Destroy;override;

    procedure Move(const aDeltaMs: double); override;

    property InterceptRange  : double read FInterceptRange write SetInterceptRange;
    property ThresholdSpeed  : double read FThresholdSpeed write SetThresholdSpeed;
  end;

implementation

{ TT3MountedGunAutoManual }

constructor TT3MountedGunAutoManual.Create;
begin
  inherited;

end;

destructor TT3MountedGunAutoManual.Destroy;
begin

  inherited;
end;

procedure TT3MountedGunAutoManual.Move(const aDeltaMs: double);
begin
  inherited;

end;

procedure TT3MountedGunAutoManual.SetInterceptRange(const Value: double);
begin
  FInterceptRange := Value;
end;

procedure TT3MountedGunAutoManual.SetThresholdSpeed(const Value: double);
begin
  FThresholdSpeed := Value;
end;

end.
