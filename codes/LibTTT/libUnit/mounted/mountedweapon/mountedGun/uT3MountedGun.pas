unit uT3MountedGun;

interface

uses uT3MountedWeapon, tttData,uDBClassDefinition, uT3BlindZone,
  uT3PlatformInstance;

type
  TT3MountedGun = class (TT3MountedWeapon)
  private
    FSalvoSize: integer;
    FSalvoMode: integer;
    FDelay    : Double;

    function getOnBoardDefinition: TDBPoint_Effect_On_Board;
    function getGunDefinition : TDBGun_Definition;

    procedure SetSalvoSize(const Value: integer);
    procedure SetSalvoMode(const Value: integer);
  public
    constructor Create; override;
    destructor Destroy;override;

    procedure Initialize;override;

    property GunDefinition : TDBGun_Definition read getGunDefinition;
    property GunOnBoardDefinition : TDBPoint_Effect_On_Board read getOnBoardDefinition;

    property SalvoSize : integer read FSalvoSize write SetSalvoSize;
    property SalvoMode : integer read FSalvoMode write SetSalvoMode;

  end;

implementation

uses
  uGlobalVar;

{ TT3MountedGun }

constructor TT3MountedGun.Create;
begin
  inherited;

end;

destructor TT3MountedGun.Destroy;
begin

  inherited;
end;

function TT3MountedGun.getGunDefinition: TDBGun_Definition;
begin
  result := nil;

  if Assigned(MountedDeviceDefinition) then
    result := TDBGun_Definition(MountedDeviceDefinition);
end;

function TT3MountedGun.getOnBoardDefinition: TDBPoint_Effect_On_Board;
begin
  result := nil;

  if Assigned(MountedDeviceOnBoardDef) then
    result := TDBPoint_Effect_On_Board(MountedDeviceOnBoardDef);

end;

procedure TT3MountedGun.Initialize;
var
  bz: TT3BlindZone;
  bzList : TDBBlindZoneDefinitionList;
  dbBz : TDBBlind_Zone_Definition;
begin
  inherited;

  { set definition once }
  FDeviceParent            := simManager.FindT3PlatformByID(PlatformParentInstanceIndex);
  FMountedDeviceDefinition := simManager.Scenario.GetDBGunDefinition(MountedDeviceDefinitionIndex);
  FMountedDeviceOnBoardDef := simManager.Scenario.
        GetDBPointEffectOnBoardDef(TT3PlatformInstance(FDeviceParent).PlatformDefinitionIndex,
              MountedDeviceOnBoardDefIndex);

  if Assigned(GunOnBoardDefinition) then
  with GunOnBoardDefinition do begin

    InstanceName  := Instance_Identifier;
    InstanceIndex := Point_Effect_Index;

    if Assigned(GunDefinition) then
    begin
      if  GunDefinition.Rate_of_Fire <> 0 then
        FDelay := ( 60 / (GunDefinition.Rate_of_Fire))  //mk
      else
        FDelay := 1;

      case GunDefinition.Gun_Category of
        1 : FWeaponCategory := wcGunCIWS;
        2 : FWeaponCategory := wcGunGun;
        4 : FWeaponCategory := wcGunRocket;
      end;
    end;

    Quantity          := Quantity;

    { set blind zone }
    simManager.Scenario.DBBlindZonePointDict.TryGetValue(Point_Effect_Index,bzList);
    if Assigned(bzList) then
      for dbBz in bzList do
      begin
        bz :=  TT3BlindZone.Create;
        bz.Blind_Zone_Type := dbBz.Blind_Zone_Type;
        bz.BlindZone_Number:= dbBz.BlindZone_Number;
        bz.Start_Angle     := dbBz.Start_Angle;
        bz.End_Angle       := dbBz.End_Angle;

        BlindZones.Add(bz);
      end;
  end;
end;

procedure TT3MountedGun.SetSalvoMode(const Value: integer);
begin
  FSalvoMode := Value;
end;

procedure TT3MountedGun.SetSalvoSize(const Value: integer);
begin
  FSalvoSize := Value;
end;

end.
