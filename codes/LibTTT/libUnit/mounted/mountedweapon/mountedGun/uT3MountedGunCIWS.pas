unit uT3MountedGunCIWS;

interface

uses uT3MountedGun;

type
  TT3MountedGunCIWS = class (TT3MountedGun)
  public
    constructor Create; override;
    destructor Destroy;override;

    procedure Move(const aDeltaMs: double); override;
  end;

implementation

{ TT3MountedGunCIWS }

constructor TT3MountedGunCIWS.Create;
begin
  inherited;

end;

destructor TT3MountedGunCIWS.Destroy;
begin

  inherited;
end;

procedure TT3MountedGunCIWS.Move(const aDeltaMs: double);
begin
  inherited;

end;

end.
