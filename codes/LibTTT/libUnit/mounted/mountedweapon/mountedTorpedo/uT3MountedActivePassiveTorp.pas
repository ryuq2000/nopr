unit uT3MountedActivePassiveTorp;

interface
uses uT3MountedTorpedo, tttData,uDBClassDefinition;

type
  TT3MountedActivePassiveTorp = class (TT3MountedTorpedo)
  private
    FSearchDepth: Double;
    FSearchRadius: Double;
    FSeekerRange: Double;
    FSafetyCeiling: Double;
    FSalvoSize: integer;
    FButtonLaunch: Boolean;
    procedure SetSafetyCeiling(const Value: Double);
    procedure SetSalvoSize(const Value: integer);
    procedure SetSearchDepth(const Value: Double);
    procedure SetSearchRadius(const Value: Double);
    procedure SetSeekerRange(const Value: Double);
    procedure SetButtonLaunch(const Value: Boolean);

  public
    constructor Create; override;
    destructor Destroy;override;

    procedure Move(const aDeltaMs: double); override;

    property ButtonLaunch : Boolean read FButtonLaunch write SetButtonLaunch;
    {active passive property}
    property SalvoSize : integer read FSalvoSize write SetSalvoSize;
    property SearchRadius : Double read FSearchRadius write SetSearchRadius;    {satuan yards}
    property SearchDepth : Double read FSearchDepth write SetSearchDepth;       {satuan meter}
    property SafetyCeiling : Double read FSafetyCeiling write SetSafetyCeiling; {satuan meter}
    property SeekerRange : Double read FSeekerRange write SetSeekerRange;       {satuan yards}

  end;

implementation

{ TT3MountedActivePassiveTorp }

constructor TT3MountedActivePassiveTorp.Create;
begin
  inherited;

end;

destructor TT3MountedActivePassiveTorp.Destroy;
begin

  inherited;
end;

procedure TT3MountedActivePassiveTorp.Move(const aDeltaMs: double);
begin
  inherited;

end;

procedure TT3MountedActivePassiveTorp.SetButtonLaunch(const Value: Boolean);
begin
  FButtonLaunch := Value;
end;

procedure TT3MountedActivePassiveTorp.SetSafetyCeiling(const Value: Double);
begin
  FSafetyCeiling := Value;
end;

procedure TT3MountedActivePassiveTorp.SetSalvoSize(const Value: integer);
begin
  FSalvoSize := Value;
end;

procedure TT3MountedActivePassiveTorp.SetSearchDepth(const Value: Double);
begin
  FSearchDepth := Value;
end;

procedure TT3MountedActivePassiveTorp.SetSearchRadius(const Value: Double);
begin
  FSearchRadius := Value;
end;

procedure TT3MountedActivePassiveTorp.SetSeekerRange(const Value: Double);
begin
  FSeekerRange := Value;
end;

end.
