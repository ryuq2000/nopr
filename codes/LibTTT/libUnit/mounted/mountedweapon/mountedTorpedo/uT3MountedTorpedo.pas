unit uT3MountedTorpedo;

interface

uses uT3MountedWeapon, tttData,uDBClassDefinition, uT3BlindZone, uT3PlatformInstance;

type
  TT3MountedTorpedo = class abstract (TT3MountedWeapon)
  private
    FLaunchMethod: TTorpedoLaunchMethod;
    FQuantity: integer;
    function getOnBoardDefinition: TDBFitted_Weapon_On_Board;
    procedure SetLaunchMethod(const Value: TTorpedoLaunchMethod);
    function getTorpedoDefintion: TDBTorpedo_Definition;
  public
    constructor Create; override;
    destructor Destroy;override;

    procedure Initialize;override;

    property TorpedoOnBoardDefinition  : TDBFitted_Weapon_On_Board read getOnBoardDefinition;
    property TorpedoDefintion          : TDBTorpedo_Definition read getTorpedoDefintion;

    property LaunchMethod              : TTorpedoLaunchMethod read FLaunchMethod write SetLaunchMethod;

  end;

implementation

uses
  uGlobalVar;

{ TT3MountedTorpedo }

constructor TT3MountedTorpedo.Create;
begin
  inherited;

end;

destructor TT3MountedTorpedo.Destroy;
begin

  inherited;
end;

function TT3MountedTorpedo.getOnBoardDefinition: TDBFitted_Weapon_On_Board;
begin
  result := nil;

  if Assigned(MountedDeviceOnBoardDef) then
    result := TDBFitted_Weapon_On_Board(MountedDeviceOnBoardDef);

end;

function TT3MountedTorpedo.getTorpedoDefintion: TDBTorpedo_Definition;
begin
  result := nil;

  if Assigned(MountedDeviceDefinition) then
    result := TDBTorpedo_Definition(MountedDeviceDefinition);
end;

procedure TT3MountedTorpedo.Initialize;
var
  i : integer;
  bz: TT3BlindZone;
  bzList : TDBBlindZoneDefinitionList;
  dbBz : TDBBlind_Zone_Definition;
begin
  inherited;

  { set definition once }
  FDeviceParent            := simManager.FindT3PlatformByID(PlatformParentInstanceIndex);
  FMountedDeviceDefinition := simManager.Scenario.GetDBTorpedoDefintion(MountedDeviceDefinitionIndex);
  FMountedDeviceOnBoardDef := simManager.Scenario.
        GetDBFittedWeaponOnBoardDef(TT3PlatformInstance(FDeviceParent).PlatformDefinitionIndex,
              MountedDeviceOnBoardDefIndex);

  if Assigned(TorpedoOnBoardDefinition) then
  with TorpedoOnBoardDefinition do begin

    InstanceName  := Instance_Identifier;
    InstanceIndex := Fitted_Weap_Index;

    if Assigned(TorpedoDefintion) then
    begin
      {Set katagori torpedo}
      case TorpedoDefintion.Guidance_Type of
        9  : FWeaponCategory := wcTorpedoStraigth;
        10 :
          if (PlatformParent.PlatformDomain = 0) then
            FWeaponCategory := wcTorpedoAirDropped
          else
            FWeaponCategory := wcTorpedoActiveAcoustic;
        11 : FWeaponCategory := wcTorpedoPassiveAcoustic;
        18 : FWeaponCategory := wcTorpedoActivePassive;
        12 : FWeaponCategory := wcTorpedoWireGuided;
        13 : FWeaponCategory := wcTorpedoWakeHoming;
      end;

      {Set Method launch torpedo}
      case TorpedoDefintion.Launch_Method of
        1 : FLaunchMethod := tlmAimpoint;
        2 : FLaunchMethod := tlmBearing;
        3 : FLaunchMethod := tlmAimBearing;
        4 : FLaunchMethod := tlmDirect;
      end;

      WeaponRange := TorpedoDefintion.Max_Range;
    end;

    { default prop}
    Quantity    := TorpedoOnBoardDefinition.Quantity;

    { set blind zone }
    simManager.Scenario.DBBlindZoneFittedDict.TryGetValue(Fitted_Weap_Index,bzList);
    if Assigned(bzList) then
      for dbBz in bzList do
      begin
        bz :=  TT3BlindZone.Create;
        bz.Blind_Zone_Type := dbBz.Blind_Zone_Type;
        bz.BlindZone_Number:= dbBz.BlindZone_Number;
        bz.Start_Angle     := dbBz.Start_Angle;
        bz.End_Angle       := dbBz.End_Angle;

        BlindZones.Add(bz);
      end;
  end;
end;


procedure TT3MountedTorpedo.SetLaunchMethod(const Value: TTorpedoLaunchMethod);
begin
  FLaunchMethod := Value;
end;

end.
