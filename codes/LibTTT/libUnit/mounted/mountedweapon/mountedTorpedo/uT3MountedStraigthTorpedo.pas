unit uT3MountedStraigthTorpedo;

interface

uses uT3MountedTorpedo, tttData,uDBClassDefinition;

type
  TT3MountedStraigthTorpedo = class (TT3MountedTorpedo)
  private

  public
    constructor Create; override;
    destructor Destroy;override;

    procedure Move(const aDeltaMs: double); override;

  end;


implementation

{ TT3MountedStraigthTorpedo }

constructor TT3MountedStraigthTorpedo.Create;
begin
  inherited;

end;

destructor TT3MountedStraigthTorpedo.Destroy;
begin

  inherited;
end;

procedure TT3MountedStraigthTorpedo.Move(const aDeltaMs: double);
begin
  inherited;

end;

end.
