unit uT3MountedActiveAcousticTorp;

interface

uses uT3MountedTorpedo, tttData,uDBClassDefinition;

type
  TT3MountedActiveAcousticTorp = class (TT3MountedTorpedo)
  private
    FGyroAngle: Integer;
    FButtonPlan: Boolean;
    FTubeOn: Byte;
    FFiringMode: Byte;
    FRunOutMode: Byte;
    FSearchDepth: Double;
    FSearchRadius: Double;
    FSeekerRange: Double;
    FSafetyCeiling: Double;
    FButtonLaunch: Boolean;
    procedure SetButtonPlan(const Value: Boolean);
    procedure SetFiringMode(const Value: Byte);
    procedure SetGyroAngle(const Value: Integer);
    procedure SetRunOutMode(const Value: Byte);
    procedure SetTubeOn(const Value: Byte);
    procedure SetSafetyCeiling(const Value: Double);
    procedure SetSearchDepth(const Value: Double);
    procedure SetSearchRadius(const Value: Double);
    procedure SetSeekerRange(const Value: Double);
    procedure SetButtonLaunch(const Value: Boolean);

  public
    constructor Create; override;
    destructor Destroy;override;

    procedure Move(const aDeltaMs: double); override;

    property ButtonLaunch : Boolean read FButtonLaunch write SetButtonLaunch;
    property ButtonPlan : Boolean read FButtonPlan write SetButtonPlan;

    {Acoustic properties}
    property SearchRadius  : Double read FSearchRadius write SetSearchRadius;    {satuan yards}
    property SearchDepth   : Double read FSearchDepth write SetSearchDepth;       {satuan meter}
    property SafetyCeiling : Double read FSafetyCeiling write SetSafetyCeiling; {satuan meter}
    property SeekerRange   : Double read FSeekerRange write SetSeekerRange;       {satuan yards}
    property FiringMode    : Byte read FFiringMode write SetFiringMode;
    property RunOutMode    : Byte read FRunOutMode write SetRunOutMode;
    property GyroAngle     : Integer read FGyroAngle write SetGyroAngle;
    property TubeOn        : Byte read FTubeOn write SetTubeOn;

  end;

implementation

{ TT3MountedActiveAcousticTorp }

constructor TT3MountedActiveAcousticTorp.Create;
begin
  inherited;

end;

destructor TT3MountedActiveAcousticTorp.Destroy;
begin

  inherited;
end;

procedure TT3MountedActiveAcousticTorp.Move(const aDeltaMs: double);
begin
  inherited;

end;

procedure TT3MountedActiveAcousticTorp.SetButtonLaunch(const Value: Boolean);
begin
  FButtonLaunch := Value;
end;

procedure TT3MountedActiveAcousticTorp.SetButtonPlan(const Value: Boolean);
begin
  FButtonPlan := Value;
end;

procedure TT3MountedActiveAcousticTorp.SetFiringMode(const Value: Byte);
begin
  FFiringMode := Value;
end;

procedure TT3MountedActiveAcousticTorp.SetGyroAngle(const Value: Integer);
begin
  FGyroAngle := Value;
end;

procedure TT3MountedActiveAcousticTorp.SetRunOutMode(const Value: Byte);
begin
  FRunOutMode := Value;
end;

procedure TT3MountedActiveAcousticTorp.SetSafetyCeiling(const Value: Double);
begin
  FSafetyCeiling := Value;
end;

procedure TT3MountedActiveAcousticTorp.SetSearchDepth(const Value: Double);
begin
  FSearchDepth := Value;
end;

procedure TT3MountedActiveAcousticTorp.SetSearchRadius(const Value: Double);
begin
  FSearchRadius := Value;
end;

procedure TT3MountedActiveAcousticTorp.SetSeekerRange(const Value: Double);
begin
  FSeekerRange := Value;
end;

procedure TT3MountedActiveAcousticTorp.SetTubeOn(const Value: Byte);
begin
  FTubeOn := Value;
end;

end.
