unit uT3MountedWireGuided;

interface

uses uT3MountedTorpedo, tttData,uDBClassDefinition;

type
  TT3MountedWireGuided = class (TT3MountedTorpedo)
  private

  public
    constructor Create; override;
    destructor Destroy;override;

    procedure Move(const aDeltaMs: double); override;

  end;

implementation

{ TT3MountedWireGuided }

constructor TT3MountedWireGuided.Create;
begin
  inherited;

end;

destructor TT3MountedWireGuided.Destroy;
begin

  inherited;
end;

procedure TT3MountedWireGuided.Move(const aDeltaMs: double);
begin
  inherited;

end;

end.
