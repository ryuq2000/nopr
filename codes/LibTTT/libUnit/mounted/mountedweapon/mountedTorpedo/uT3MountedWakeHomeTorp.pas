unit uT3MountedWakeHomeTorp;

interface

uses uT3MountedTorpedo, tttData,uDBClassDefinition;

type
  TT3MountedWakeHomeTorp = class (TT3MountedTorpedo)
  private

  public
    constructor Create; override;
    destructor Destroy;override;

    procedure Move(const aDeltaMs: double); override;

  end;

implementation

{ TT3MountedWakeHomeTorp }

constructor TT3MountedWakeHomeTorp.Create;
begin
  inherited;

end;

destructor TT3MountedWakeHomeTorp.Destroy;
begin

  inherited;
end;

procedure TT3MountedWakeHomeTorp.Move(const aDeltaMs: double);
begin
  inherited;

end;

end.
