unit uT3MountedAirDroppedTorp;

interface

uses uT3MountedTorpedo, tttData,uDBClassDefinition;

type
  TT3MountedAirDroppedTorp = class (TT3MountedTorpedo)
  private

  public
    constructor Create; override;
    destructor Destroy;override;

    procedure Move(const aDeltaMs: double); override;

  end;


implementation

{ TT3MountedAirDroppedTorp }

constructor TT3MountedAirDroppedTorp.Create;
begin
  inherited;

end;

destructor TT3MountedAirDroppedTorp.Destroy;
begin

  inherited;
end;

procedure TT3MountedAirDroppedTorp.Move(const aDeltaMs: double);
begin
  inherited;

end;

end.
