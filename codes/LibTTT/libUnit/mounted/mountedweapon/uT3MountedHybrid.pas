unit uT3MountedHybrid;

interface

uses uT3MountedWeapon, tttData,uDBClassDefinition;

type
  TT3MountedHybrid = class (TT3MountedWeapon)
  private
    FTargetType: integer;
    FSalvoSize: Integer;
    FCruiseAltitude: Double;
    FSeekerRange: Double;
    function getOnBoardDefinition: TDBFitted_Weapon_On_Board;
    procedure SetTargetType(const Value: integer);
    procedure SetSalvoSize(const Value: Integer);
    procedure SetCruiseAltitude(const Value: Double);
    procedure SetSeekerRange(const Value: Double);
  public
    constructor Create; override;
    destructor Destroy;override;

    procedure Initialize;override;
    property HybridOnBoardDefinition : TDBFitted_Weapon_On_Board read getOnBoardDefinition;


    // 1 :  track, 2 : aimpoint, 3 : bearing
    property TargetType     : integer read FTargetType write SetTargetType default 1;
    property SalvoSize      : Integer read FSalvoSize write SetSalvoSize;
    property CruiseAltitude : Double read FCruiseAltitude write SetCruiseAltitude;
    property SeekerRange    : Double read FSeekerRange write SetSeekerRange;

  end;

implementation

{ TT3MountedHybrid }

constructor TT3MountedHybrid.Create;
begin
  inherited;

end;

destructor TT3MountedHybrid.Destroy;
begin

  inherited;
end;

function TT3MountedHybrid.getOnBoardDefinition: TDBFitted_Weapon_On_Board;
begin
  result := nil;

  if Assigned(MountedDeviceOnBoardDef) then
    result := TDBFitted_Weapon_On_Board(MountedDeviceOnBoardDef);

end;

procedure TT3MountedHybrid.Initialize;
begin
  inherited;

  if Assigned(HybridOnBoardDefinition) then
  with HybridOnBoardDefinition do begin

    InstanceName  := Instance_Identifier;
    InstanceIndex := Fitted_Weap_Index;


  end;

end;

procedure TT3MountedHybrid.SetCruiseAltitude(const Value: Double);
begin
  FCruiseAltitude := Value;
end;

procedure TT3MountedHybrid.SetSalvoSize(const Value: Integer);
begin
  FSalvoSize := Value;
end;

procedure TT3MountedHybrid.SetSeekerRange(const Value: Double);
begin
  FSeekerRange := Value;
end;

procedure TT3MountedHybrid.SetTargetType(const Value: integer);
begin
  FTargetType := Value;
end;

end.
