unit uT3MountedMissile;

interface

uses uT3MountedWeapon, tttData, uDBAsset_Weapon,
  uT3BlindZone, uDBBlind_Zone;

type
  TT3MountedMissile = class abstract (TT3MountedWeapon)
  private
    FSalvoSize: integer;
    function getDefinition: TMissile_On_Board;
    procedure SetSalvoSize(const Value: integer);
  public
    constructor Create; override;
    destructor Destroy;override;

    procedure Move(const aDeltaMs: double); override;
    procedure Initialize;override;

    // read only prop
    property MissileDefinition : TMissile_On_Board read getDefinition;

    // rw prop
    property SalvoSize  : integer read FSalvoSize write SetSalvoSize;
  end;

implementation

{ TT3MountedMissile }

constructor TT3MountedMissile.Create;
begin
  inherited;

end;

destructor TT3MountedMissile.Destroy;
begin

  inherited;
end;

function TT3MountedMissile.getDefinition: TMissile_On_Board;
begin
  result := nil;

  if Assigned(MountedDeviceDefinition) then
    result := TMissile_On_Board(MountedDeviceDefinition);

end;

procedure TT3MountedMissile.Initialize;
var
  i : integer;
  bz: TT3BlindZone;
begin
  inherited;

  if Assigned(MissileDefinition) then
  with MissileDefinition do begin

    InstanceName  := FData.Instance_Identifier;
    InstanceIndex := FData.Fitted_Weap_Index;

    { missile category }
    case MissileDefinition.FDef.Primary_Target_Domain of
      0 : FWeaponCategory := wcMissileAirToSurfaceSubsurface;
      1 : FWeaponCategory := wcMissileSurfaceSubsurfaceToSurfaceSubsurface;
      2 : FWeaponCategory := wcMissileSurfaceSubsurfaceToAir;
      3 : FWeaponCategory := wcMissileAirToAir;
      4 : FWeaponCategory := wcMissileLandAttack;
    end;

    { default prop }
    Quantity    := FData.Quantity;
    WeaponRange := FDef.Max_Range;
    FSalvoSize  := 1;

    { set blind zone }
    if FBlind.Count > 0 then begin
      for i := 0 to FBlind.Count - 1 do begin

        bz :=  TT3BlindZone.Create;
        bz.Blind_Zone_Type := TBlind_Zone(FBlind.Items[i]).FData.Blind_Zone_Type;
        bz.BlindZone_Number:= TBlind_Zone(FBlind.Items[i]).FData.BlindZone_Number;
        bz.Start_Angle     := TBlind_Zone(FBlind.Items[i]).FData.Start_Angle;
        bz.End_Angle       := TBlind_Zone(FBlind.Items[i]).FData.End_Angle;

        BlindZones.Add(bz);
      end;
      
    end;
  end;
end;

procedure TT3MountedMissile.Move(const aDeltaMs: double);
begin
  inherited;

end;

procedure TT3MountedMissile.SetSalvoSize(const Value: integer);
begin
  FSalvoSize := Value;
end;

end.
