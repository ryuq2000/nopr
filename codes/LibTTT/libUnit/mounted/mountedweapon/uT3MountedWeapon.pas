unit uT3MountedWeapon;

interface

uses
  uT3MountedDevice, uT3BlindZone, tttData, Generics.Collections,
  uSimObjects, uT3SimContainer, Classes,SysUtils;

type
  TT3MountedWeapon = class abstract (TT3MountedDevice)
  private
    FBlindZones        : TList<TT3BlindZone>;
    FShowBlindZone     : boolean;
    FShowRange         : boolean;
    FWeaponRange       : double;
    FQuantity          : integer;
    FWeaponStatus      : TWeaponStatus;
    FTargetInstanceIdx : integer;
    FTargetTrackNumber : integer;
    procedure SetWeaponCategory(const Value: TWeaponCategory);
    procedure SetShowBlindZone(const Value: boolean);
    procedure SetShowRange(const Value: boolean);
    procedure SetQuantity(const Value: integer);
    procedure SetWeaponRange(const Value: double);
    procedure SetWeaponStatus(const Value: TWeaponStatus);
    procedure SetTargetInstanceIdx(const Value: integer);
    procedure SetTargetTrackNumber(const Value: integer);
  published
  protected
    FWeaponCategory: TWeaponCategory;
  public
    constructor Create; override;
    destructor Destroy;override;

    procedure Move(const aDeltaMs: double); override;
    function  InsideBlindZone(aObject : TSimObject) : boolean;

    // read only prop
    property BlindZones        : TList<TT3BlindZone> read FBlindZones;

    // rw prop
    property ShowBlindZone     : boolean             read FShowBlindZone     write SetShowBlindZone;
    property ShowRange         : boolean             read FShowRange         write SetShowRange;

    property WeaponRange       : double              read FWeaponRange       write SetWeaponRange;
    property WeaponCategory    : TWeaponCategory     read FWeaponCategory    write SetWeaponCategory;
    property Quantity          : integer             read FQuantity          write SetQuantity;
    property WeaponStatus      : TWeaponStatus       read FWeaponStatus      write SetWeaponStatus;

    {
      target track now save with its track number
      to get its instance index, or maybe its only a non real track
        if machinerole = crserver, get from TrackManager
        if machinerole = crCOntroller/crCubicle, get from CubicleTracks
    }
    property TargetTrackNumber : integer             read FTargetTrackNumber write SetTargetTrackNumber;
    {
      if track number exist, this must be has same ref to instance index,
      if instance index 0, its mean track is nonreal track
    }
    property TargetInstanceIdx : integer             read FTargetInstanceIdx write SetTargetInstanceIdx;
  end;

implementation

uses
  uBaseCoordSystem, uT3PlatformInstance;

{ TT3MountedWeapon }

constructor TT3MountedWeapon.Create;
begin
  inherited;
  FBlindZones := TList<TT3BlindZone>.Create;

  FWeaponStatus := wsAvailable;

//  FShowBlindZone  := false;
//  FShowRange      := false;
//
//  FOperationalStatus  := sopOff;
//  FEmconOperationalStatus := EmconOff;


end;

destructor TT3MountedWeapon.Destroy;
var
  bz  : TT3BlindZone;
begin

  for bz in FBlindZones do
    bz.Free;

  FBlindZones.CLear;
  FBlindZones.Free;
  inherited;
end;

function TT3MountedWeapon.InsideBlindZone(aObject: TSimObject): boolean;
var
  i : integer;
  blindZ : TT3BlindZone;
  bearing : double;
begin
  inherited;

  result := false;

  bearing := CalcBearing(FPosition.X,FPosition.Y,
           TT3PlatformInstance(aObject).getPositionX,
           TT3PlatformInstance(aObject).getPositionY);

  // periksa apakah didalam area blind zone
  if FBlindZones.Count > 0 then begin
    for I := 0 to FBlindZones.Count - 1 do begin

      blindZ := TT3BlindZone(FBlindZones.Items[i]);
      Result := DegComp_IsBeetwen(bearing, blindZ.Start_Angle, blindZ.End_Angle);
    end;
  end;
end;

procedure TT3MountedWeapon.Move(const aDeltaMs: double);
begin
  inherited;

end;

procedure TT3MountedWeapon.SetQuantity(const Value: integer);
begin
  FQuantity := Value;
end;

procedure TT3MountedWeapon.SetShowBlindZone(const Value: boolean);
begin
  FShowBlindZone := Value;
end;

procedure TT3MountedWeapon.SetShowRange(const Value: boolean);
begin
  FShowRange := Value;
end;

procedure TT3MountedWeapon.SetTargetInstanceIdx(const Value: integer);
begin
  FTargetInstanceIdx := Value;
end;

procedure TT3MountedWeapon.SetTargetTrackNumber(const Value: integer);
begin
  FTargetTrackNumber := Value;
end;

procedure TT3MountedWeapon.SetWeaponCategory(const Value: TWeaponCategory);
begin
  FWeaponCategory := Value;
end;

procedure TT3MountedWeapon.SetWeaponRange(const Value: double);
begin
  FWeaponRange := Value;
end;

procedure TT3MountedWeapon.SetWeaponStatus(const Value: TWeaponStatus);
begin
  FWeaponStatus := Value;
end;

end.
