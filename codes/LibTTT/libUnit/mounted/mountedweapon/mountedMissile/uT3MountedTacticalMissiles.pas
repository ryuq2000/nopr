unit uT3MountedTacticalMissiles;

interface

uses uT3MountedMissile, tttData, uDBClassDefinition,
  uT3BlindZone;

type
  TT3MountedTacticalMissiles = class  (TT3MountedMissile)
  private
    FAimPointX: double;
    FAimPointY: double;
    FBearing: double;
    FCruiseAltitude: double;
    FSeekerRange: double;
    FTargetType: integer;
    procedure SetAimPointX(const Value: double);
    procedure SetAimPointY(const Value: double);
    procedure SetBearing(const Value: double);
    procedure SetCruiseAltitude(const Value: double);
    procedure SetSeekerRange(const Value: double);
    procedure SetTargetType(const Value: integer);
  published
  public
    constructor Create; override;
    destructor Destroy;override;

    procedure Move(const aDeltaMs: double); override;

    property AimPointX      : double read FAimPointX write SetAimPointX;
    property AimPointY      : double read FAimPointY write SetAimPointY;
    property Bearing        : double read FBearing write SetBearing;
    property CruiseAltitude : double read FCruiseAltitude write SetCruiseAltitude;
    property SeekerRange    : double read FSeekerRange write SetSeekerRange;

    // 1 :  track, 2 : aimpoint, 3 : bearing
    property TargetType     : integer read FTargetType write SetTargetType default 1;

  end;

implementation

{ TT3MountedTacticalMissiles }

constructor TT3MountedTacticalMissiles.Create;
begin
  inherited;

end;

destructor TT3MountedTacticalMissiles.Destroy;
begin

  inherited;
end;

procedure TT3MountedTacticalMissiles.Move(const aDeltaMs: double);
begin
  inherited;

end;

procedure TT3MountedTacticalMissiles.SetAimPointX(const Value: double);
begin
  FAimPointX := Value;
end;

procedure TT3MountedTacticalMissiles.SetAimPointY(const Value: double);
begin
  FAimPointY := Value;
end;

procedure TT3MountedTacticalMissiles.SetBearing(const Value: double);
begin
  FBearing := Value;
end;

procedure TT3MountedTacticalMissiles.SetCruiseAltitude(const Value: double);
begin
  FCruiseAltitude := Value;
end;

procedure TT3MountedTacticalMissiles.SetSeekerRange(const Value: double);
begin
  FSeekerRange := Value;
end;

procedure TT3MountedTacticalMissiles.SetTargetType(const Value: integer);
begin
  FTargetType := Value;
end;

end.
