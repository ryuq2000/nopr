unit uT3MountedSurfaceToSurfaceMissile;

interface

uses uT3MountedMissile, tttData, uDBClassDefinition,
  uT3BlindZone;

type
  TT3MountedSurfaceToSurfaceMissile = class  (TT3MountedMissile)
  private
    FEngagement      : TMissileEngagement;
    FFiringMode      : TMissileFiringMode;
    FDestruckRange   : Double;
    FCrossOverRange  : Double;
    FBOLW_Name       : string;
    FRBLW_PointX     : Double;
    FRBLW_PointY     : Double;
    FRangeBOLWCircle : Double;
    FBOLW_PointX     : Double;
    FBOLW_PointY     : Double;
    FRBLW_Name       : string;
    FSeekerRangeTurn : single;
    FLauncherActive  : integer;
    FRippleCountArray: Array[0..7] of boolean;
    FRippleDegreeLauncher : array[0..7] of Integer;

    procedure SetEngagement(const Value: TMissileEngagement);
    procedure SetFiringMode(const Value: TMissileFiringMode);
    procedure SetCrossOverRange(const Value: Double);
    procedure SetLauncherActive(const Value: integer);
  published
  public
    constructor Create; override;
    destructor Destroy;override;

    procedure Move(const aDeltaMs: double); override;

    property FiringMode      : TMissileFiringMode read FFiringMode write SetFiringMode;
    property Engagement      : TMissileEngagement read FEngagement write SetEngagement;
    property DestruckRange   : Double read FDestruckRange write FDestruckRange;
    property CrossOverRange  : Double read FCrossOverRange write SetCrossOverRange;
    property SeekerRangeTurn  : single read FSeekerRangeTurn write FSeekerRangeTurn;

    property RBLW_PointX     : Double read FRBLW_PointX write FRBLW_PointX;
    property RBLW_PointY     : Double read FRBLW_PointY write FRBLW_PointY;
    property RBLW_Name       : string read FRBLW_Name write FRBLW_Name;
    property RangeBOLWCircle : Double  read FRangeBOLWCircle write FRangeBOLWCircle;
    property BOLW_PointX     : Double read FBOLW_PointX write FBOLW_PointX;
    property BOLW_PointY     : Double read FBOLW_PointY write FBOLW_PointY;
    property BOLW_Name       : string read FBOLW_Name write FBOLW_Name;
    property LauncherActive  : integer read FLauncherActive write SetLauncherActive;

  end;

implementation


{ TT3MountedSurfaceToSurfaceMissile }

constructor TT3MountedSurfaceToSurfaceMissile.Create;
var
  I: Integer;
begin

  inherited;

  for I := 0 to Length(FRippleCountArray) do
    FRippleCountArray[I] := False;

  for I := 0 to Length(FRippleDegreeLauncher) do
    FRippleDegreeLauncher[I] := 0;

end;

destructor TT3MountedSurfaceToSurfaceMissile.Destroy;
begin

  inherited;
end;

procedure TT3MountedSurfaceToSurfaceMissile.Move(const aDeltaMs: double);
begin
  inherited;

end;

procedure TT3MountedSurfaceToSurfaceMissile.SetCrossOverRange(
  const Value: Double);
begin
  FCrossOverRange := Value;
end;

procedure TT3MountedSurfaceToSurfaceMissile.SetEngagement(
  const Value: TMissileEngagement);
begin
  FEngagement := Value;
end;

procedure TT3MountedSurfaceToSurfaceMissile.SetFiringMode(
  const Value: TMissileFiringMode);
begin
  FFiringMode := Value;
end;

procedure TT3MountedSurfaceToSurfaceMissile.SetLauncherActive(
  const Value: integer);
var
  i : integer;
//  launcher : TFitted_Weap_Launcher_On_Board;
  LauncherAngle : Integer;
begin
  FLauncherActive := Value;

//  if FLauncherActive = 0 then
//    Exit;
//
//  case FiringMode of
//    mfmRBl,mfmBOL:
//    begin
//      for i := 0 to 7 do
//        FRippleCountArray[i] := False;
//    end;
//  end;
//
//  for I := 0 to MissileDefinition.FLaunchs.Count - 1 do
//  begin
//    launcher := MissileDefinition.FLaunchs.Items[i];
//    if launcher.FData.Launcher_Type > 8 then
//    begin
//      if FLauncherActive = (launcher.FData.Launcher_Type - 8) then
//      begin
//        LauncherAngle := launcher.FData.Launcher_Angle;
//        Break;
//      end;
//    end
//    else
//    begin
//      if FLauncherActive = launcher.FData.Launcher_Type then
//      begin
//        LauncherAngle := launcher.FData.Launcher_Angle;
//        Break;
//      end;
//    end;
//  end;

end;

end.
