unit uT3MountedSurfaceToAirMissile;

interface

uses uT3MountedMissile, tttData, uDBClassDefinition,
  uT3BlindZone;

type
  TT3MountedSurfaceToAirMissile = class  (TT3MountedMissile)
  private
  published
  public
    constructor Create; override;
    destructor Destroy;override;

    procedure Move(const aDeltaMs: double); override;
  end;

implementation

{ TT3MountedSurfaceToAirMissile }

constructor TT3MountedSurfaceToAirMissile.Create;
begin
  inherited;

end;

destructor TT3MountedSurfaceToAirMissile.Destroy;
begin

  inherited;
end;

procedure TT3MountedSurfaceToAirMissile.Move(const aDeltaMs: double);
begin
  inherited;

end;

end.
