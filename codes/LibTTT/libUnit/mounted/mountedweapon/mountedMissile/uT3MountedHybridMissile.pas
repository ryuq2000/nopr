unit uT3MountedHybridMissile;

interface


uses uT3MountedMissile, tttData, uDBClassDefinition;


type
  TT3MountedHybridMissile = class  (TT3MountedMissile)
  private
    function getHybridDefinition : TDBHybrid_Definition;
  public
    constructor Create; override;
    destructor Destroy;override;

    procedure Move(const aDeltaMs: double); override;

    property HybridDefinition : TDBHybrid_Definition read getHybridDefinition;
  end;

implementation

{ TT3MountedHybridMissile }

constructor TT3MountedHybridMissile.Create;
begin
  inherited;

end;

destructor TT3MountedHybridMissile.Destroy;
begin

  inherited;
end;

function TT3MountedHybridMissile.getHybridDefinition: TDBHybrid_Definition;
begin
  result := nil;

  if Assigned(MountedDeviceDefinition) then
    result := TDBHybrid_Definition(MountedDeviceDefinition);
end;

procedure TT3MountedHybridMissile.Move(const aDeltaMs: double);
begin
  inherited;

end;

end.
