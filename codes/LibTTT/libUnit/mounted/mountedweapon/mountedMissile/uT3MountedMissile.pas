unit uT3MountedMissile;

interface

uses uT3MountedWeapon, tttData, uDBClassDefinition,
  uT3BlindZone, uT3PlatformInstance;

type
  TT3MountedMissile = class abstract (TT3MountedWeapon)
  private
    FSalvoSize: integer;
    FPlanned: boolean;
    function getOnBoardDefinition: TDBFitted_Weapon_On_Board;
    procedure SetSalvoSize(const Value: integer);
    procedure SetPlanned(const Value: boolean);
    function getMissileDefinition: TDBMissile_Definition;
  public
    constructor Create; override;
    destructor Destroy;override;

    procedure Move(const aDeltaMs: double); override;
    procedure Initialize;override;

    // read only prop
    property MissileOnBoardDefinition : TDBFitted_Weapon_On_Board read getOnBoardDefinition;
    property MissileDefinition  : TDBMissile_Definition read getMissileDefinition;

    // rw prop
    property SalvoSize  : integer read FSalvoSize write SetSalvoSize;
    { only on STS and STA mounted missile, true if missile already planned to be launched }
    property Planned   : boolean read FPlanned write SetPlanned;
  end;

implementation

uses uGlobalVar;

{ TT3MountedMissile }

constructor TT3MountedMissile.Create;
begin
  inherited;

end;

destructor TT3MountedMissile.Destroy;
begin

  inherited;
end;

function TT3MountedMissile.getMissileDefinition: TDBMissile_Definition;
begin
  result := nil;

  if Assigned(MountedDeviceDefinition) then
    result := TDBMissile_Definition(MountedDeviceDefinition);
end;

function TT3MountedMissile.getOnBoardDefinition: TDBFitted_Weapon_On_Board;
begin
  result := nil;

  if Assigned(MountedDeviceOnBoardDef) then
    result := TDBFitted_Weapon_On_Board(MountedDeviceOnBoardDef);

end;

procedure TT3MountedMissile.Initialize;
var
  bz: TT3BlindZone;
  bzList : TDBBlindZoneDefinitionList;
  dbBz : TDBBlind_Zone_Definition;
begin
  inherited;

  { set definition once }
  FDeviceParent            := simManager.FindT3PlatformByID(PlatformParentInstanceIndex);
  FMountedDeviceDefinition := simManager.Scenario.GetDBMissileDefintion(MountedDeviceDefinitionIndex);
  FMountedDeviceOnBoardDef := simManager.Scenario.
        GetDBFittedWeaponOnBoardDef(TT3PlatformInstance(FDeviceParent).PlatformDefinitionIndex,
              MountedDeviceOnBoardDefIndex);

  if Assigned(MissileOnBoardDefinition) then
  with MissileOnBoardDefinition do begin

    InstanceName  := Instance_Identifier;
    InstanceIndex := Fitted_Weap_Index;

    if Assigned(MissileDefinition) then
    begin
      { missile category }
      case MissileDefinition.Primary_Target_Domain of
        0 : FWeaponCategory := wcMissileAirToSurfaceSubsurface;
        1 : FWeaponCategory := wcMissileSurfaceSubsurfaceToSurfaceSubsurface;
        2 : FWeaponCategory := wcMissileSurfaceSubsurfaceToAir;
        3 : FWeaponCategory := wcMissileAirToAir;
        4 : FWeaponCategory := wcMissileLandAttack;
      end;

      { default prop }
      WeaponRange := MissileDefinition.Max_Range;
    end;

    Quantity    := Quantity;
    FSalvoSize  := 1;

    { set blind zone }
    simManager.Scenario.DBBlindZoneFittedDict.TryGetValue(Fitted_Weap_Index,bzList);
    if Assigned(bzList) then
      for dbBz in bzList do
      begin
        bz :=  TT3BlindZone.Create;
        bz.Blind_Zone_Type := dbBz.Blind_Zone_Type;
        bz.BlindZone_Number:= dbBz.BlindZone_Number;
        bz.Start_Angle     := dbBz.Start_Angle;
        bz.End_Angle       := dbBz.End_Angle;

        BlindZones.Add(bz);
      end;
  end;
end;

procedure TT3MountedMissile.Move(const aDeltaMs: double);
begin
  inherited;

end;

procedure TT3MountedMissile.SetPlanned(const Value: boolean);
begin
  FPlanned := Value;
end;

procedure TT3MountedMissile.SetSalvoSize(const Value: integer);
begin
  FSalvoSize := Value;
end;

end.
