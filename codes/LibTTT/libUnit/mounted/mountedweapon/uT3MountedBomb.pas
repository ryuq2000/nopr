unit uT3MountedBomb;

interface

uses uT3MountedWeapon, tttData, uDBClassDefinition, uT3PlatformInstance;

type
  TBombType = (btBomb, btDepthCharge);
  TT3MountedBomb = class (TT3MountedWeapon)
  private
    FBombType : TBombType;
    FSalvoSize: Integer;
    FDropWithoutTarget: Boolean;
    function getDefinition: TDBBomb_Definition;
    function getMountedDefinition: TDBPoint_Effect_On_Board;
    procedure SetSalvoSize(const Value: Integer);
    procedure SetDropWithoutTarget(const Value: Boolean);
  public
    constructor Create; override;
    destructor Destroy;override;

    procedure Initialize;override;

    property BombDefinition : TDBBomb_Definition read getDefinition;
    property MountedBombDefinition : TDBPoint_Effect_On_Board read getMountedDefinition;

    property BombType : TBombType read FBombType;
    property SalvoSize : Integer read FSalvoSize write SetSalvoSize;
    property DropWithoutTarget : Boolean read FDropWithoutTarget write SetDropWithoutTarget;
  end;

implementation

uses uGlobalVar;

{ TT3MountedBomb }

constructor TT3MountedBomb.Create;
begin
  inherited;

  SalvoSize := 1;
  DropWithoutTarget := False;
end;

destructor TT3MountedBomb.Destroy;
begin

  inherited;
end;

function TT3MountedBomb.getDefinition: TDBBomb_Definition;
begin
  result := nil;

  if Assigned(MountedDeviceDefinition) then
    result := TDBBomb_Definition(MountedDeviceDefinition);

end;

function TT3MountedBomb.getMountedDefinition: TDBPoint_Effect_On_Board;
begin
  result := nil;

  if Assigned(MountedDeviceOnBoardDef) then
    result := TDBPoint_Effect_On_Board(MountedDeviceOnBoardDef);

end;

procedure TT3MountedBomb.Initialize;
begin
  inherited;

  { set definition once }
  FDeviceParent            := simManager.FindT3PlatformByID(PlatformParentInstanceIndex);
  FMountedDeviceDefinition := simManager.Scenario.GetDBBombDefintion(MountedDeviceDefinitionIndex);
  FMountedDeviceOnBoardDef := simManager.Scenario.
        GetDBPointEffectOnBoardDef(TT3PlatformInstance(FDeviceParent).PlatformDefinitionIndex,
              MountedDeviceOnBoardDefIndex);

  if Assigned(MountedBombDefinition) then
  with MountedBombDefinition do begin

    InstanceName  := Instance_Identifier;
    InstanceIndex := Point_Effect_Index;

    WeaponCategory    := wcBomb;
    Quantity          := Quantity;

  end;

  if Assigned(BombDefinition) then
  with BombDefinition do begin
    WeaponRange       := Max_Range;

    { tipe bomb / depthcharge }
    case Bomb_Type of
      1 : FBombType := btBomb;
      2 : FBombType := btDepthCharge;
    end;
  end;
end;

procedure TT3MountedBomb.SetDropWithoutTarget(const Value: Boolean);
begin
  FDropWithoutTarget := Value;
end;

procedure TT3MountedBomb.SetSalvoSize(const Value: Integer);
begin
  FSalvoSize := Value;
end;

end.
