unit uT3MountedDevice;

interface

uses uT3Unit, uT3PlatformInstance;

type
  TT3MountedDevice = class abstract(TT3Unit)
  private

    FDeviceType: String;
    FPlatformParentIndex: integer;

    FMountedDeviceDefinitionIndex: integer;
    FMountedDeviceOnBoardDefIndex: integer;

    procedure SetDeviceType(const Value: String);
    procedure SetMountedDeviceDefinition(const Value: integer);
    procedure SetPlatformParentIndex(const Value: integer);
    procedure SetMountedDeviceOnBoardDef(const Value: integer);
  protected
    FMountedDeviceDefinition: TObject;
    FMountedDeviceOnBoardDef: TObject;
    FDeviceParent           : TT3PlatformInstance;

    procedure UpdateVisualData; virtual;

  public

    procedure Move(const aDeltaMs: double); override;

    procedure Initialize;override;

    { device type : mounted sensor, mounted weapon, countermeasure }
    property DeviceType: String read FDeviceType write SetDeviceType;

    property PlatformParentInstanceIndex: integer read FPlatformParentIndex write
      SetPlatformParentIndex;
    property PlatformParent : TT3PlatformInstance read FDeviceParent;

    { device db definition }
    property MountedDeviceDefinitionIndex
      : integer read FMountedDeviceDefinitionIndex write SetMountedDeviceDefinition;
    property MountedDeviceDefinition : TObject read FMountedDeviceDefinition;

    { onboard db defintion }
    property MountedDeviceOnBoardDefIndex
      : integer read FMountedDeviceOnBoardDefIndex write SetMountedDeviceOnBoardDef;
    property MountedDeviceOnBoardDef : TObject read FMountedDeviceOnBoardDef;

  end;

implementation

uses uGlobalVar;

{ TT3MountedDevice }

procedure TT3MountedDevice.Initialize;
begin
  inherited;

  FDeviceParent := simManager.FindT3PlatformByID(FPlatformParentIndex);

end;

procedure TT3MountedDevice.Move(const aDeltaMs: double);
begin
  inherited;

  { always update position to parent position }
  if Assigned(FDeviceParent) then
  begin
    FPosition.X := FDeviceParent.PosX;
    FPosition.Y := FDeviceParent.PosY;
    FPosition.Z := FDeviceParent.PosZ;
   end;

  // UpdateVisualData;

end;

procedure TT3MountedDevice.SetDeviceType(const Value: String);
begin
  FDeviceType := Value;
end;

procedure TT3MountedDevice.SetMountedDeviceDefinition(const Value: integer);
begin
  FMountedDeviceDefinitionIndex := Value;
end;

procedure TT3MountedDevice.SetMountedDeviceOnBoardDef(const Value: integer);
begin
  FMountedDeviceOnBoardDefIndex := Value;
end;

procedure TT3MountedDevice.SetPlatformParentIndex(const Value: integer);
begin
  FPlatformParentIndex := Value;
end;

procedure TT3MountedDevice.UpdateVisualData;
begin

end;

end.
