unit uT3EngagementGuide;

interface

uses uT3VehicleGuidance;

type
  TT3EngagementGuidance = class(TT3VehicleGuidance)
  private
    FPlatformInstanceIndex: integer;
    FStandOffDistance: double;
    procedure SetPlatformInstanceIndex(const Value: integer);
    procedure SetStandOffDistance(const Value: double);
  protected
    procedure doGuide(const aDeltaMs: Double); override;

  public
    constructor Create; override;

    property PlatformInstanceIndex : integer read FPlatformInstanceIndex write SetPlatformInstanceIndex;
    property StandOffDistance : double read FStandOffDistance write SetStandOffDistance;

  end;

implementation
uses tttdata;


{ TT3EngagementGuidance }

procedure TT3EngagementGuidance.doGuide(const aDeltaMs: Double);
begin
  inherited;
  GuidanceType := vgtEngagement;

end;

constructor TT3EngagementGuidance.Create;
begin
  inherited;

end;

procedure TT3EngagementGuidance.SetPlatformInstanceIndex(const Value: integer);
begin

end;

procedure TT3EngagementGuidance.SetStandOffDistance(const Value: double);
begin
  FStandOffDistance := Value;
end;

end.
