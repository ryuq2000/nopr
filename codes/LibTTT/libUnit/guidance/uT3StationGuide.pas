unit uT3StationGuide;

interface

uses uT3VehicleGuidance, tttData;

type
  TT3StationGuidance = class (TT3VehicleGuidance)
  private
    FStationRange: Double;
    FStationPostX: Double;
    FStationPostY: Double;
    FStationBearing_state: Byte;
    FStationMode: Byte;
    FStationBearing: Double;
    FStationDrift: Boolean;
    FStationTrackIndex: Integer;
    procedure SetStationBearing(const Value: Double);
    procedure SetStationBearing_state(const Value: Byte);
    procedure SetStationDrift(const Value: Boolean);
    procedure SetStationMode(const Value: Byte);
    procedure SetStationPostX(const Value: Double);
    procedure SetStationPostY(const Value: Double);
    procedure SetStationRange(const Value: Double);
    procedure SetStationTrackIndex(const Value: Integer);
  protected
    procedure doGuide(const aDeltaMs: Double); override;
  public
    constructor Create;override;

    property StationMode          : Byte read FStationMode write SetStationMode;
    property StationTrackIndex    : Integer read FStationTrackIndex write SetStationTrackIndex;
    property StationBearing       : Double read FStationBearing write SetStationBearing;
    property StationBearing_state : Byte read FStationBearing_state write SetStationBearing_state;
    property StationRange         : Double read FStationRange write SetStationRange;
    property StationPostX         : Double read FStationPostX write SetStationPostX;
    property StationPostY         : Double read FStationPostY write SetStationPostY;
    property StationDrift         : Boolean read FStationDrift write SetStationDrift;
    //IsStationSpeedSet : Boolean;
  end;

implementation

{ TT3StationGuidance }

procedure TT3StationGuidance.doGuide(const aDeltaMs: Double);
begin
  inherited;

end;

constructor TT3StationGuidance.Create;
begin
  inherited;
  GuidanceType := vgtStation;

end;

procedure TT3StationGuidance.SetStationBearing(const Value: Double);
begin
  FStationBearing := Value;
end;

procedure TT3StationGuidance.SetStationBearing_state(const Value: Byte);
begin
  FStationBearing_state := Value;
end;

procedure TT3StationGuidance.SetStationDrift(const Value: Boolean);
begin
  FStationDrift := Value;
end;

procedure TT3StationGuidance.SetStationMode(const Value: Byte);
begin
  FStationMode := Value;
end;

procedure TT3StationGuidance.SetStationPostX(const Value: Double);
begin
  FStationPostX := Value;
end;

procedure TT3StationGuidance.SetStationPostY(const Value: Double);
begin
  FStationPostY := Value;
end;

procedure TT3StationGuidance.SetStationRange(const Value: Double);
begin
  FStationRange := Value;
end;

procedure TT3StationGuidance.SetStationTrackIndex(const Value: Integer);
begin
  FStationTrackIndex := Value;
end;

end.
