unit uT3HelmGuide;

interface

uses uT3VehicleGuidance, uBaseCoordSystem;

type
  TT3HelmGuidance = class (TT3VehicleGuidance)
  private
    FFirstHeading: double;
    FHelmDegree: double;
    FHelmTurnRate : double;
    procedure SetFirstHeading(const Value: double);
    procedure SetHelmDegree(const Value: double);
  protected
    procedure doGuide(const aDeltaMs: Double); override;
    procedure SetOrderedHeading(const Value: double); override;
  public
    constructor Create;override;
    procedure GuidanceInit;override;

    property FirstHeading : double read FFirstHeading write SetFirstHeading;
    property HelmDegree : double read FHelmDegree write SetHelmDegree;
  end;

implementation

uses tttdata;
{ TT3Helmuidance }

procedure TT3HelmGuidance.doGuide(const aDeltaMs: Double);
var
  orderedHeadingAchieved : Boolean;
  dHead : double;
  order : Double;
begin
  inherited;

  if HelmDegree = 0 then
    SetOrderedHeading(Round(ResultHeading))
  else
  begin

    dHead := getDeltaH(FOrderedHeading, ConvCartesian_To_Compass(Mover.Direction));
    orderedHeadingAchieved := Abs(dHead) < 2.0;

    if orderedHeadingAchieved then
    begin
      order := Round(ResultHeading) + HelmDegree;
      if order > 360 then
        OrderedHeading := order - 360
      else
      if order < 0 then
        OrderedHeading := 360 + order
      else
        OrderedHeading := order;
    end;
  end;

end;

procedure TT3HelmGuidance.GuidanceInit;
begin
  inherited;
  HelmDegree := 0;

end;

constructor TT3HelmGuidance.Create;
begin
  inherited;
  GuidanceType := vgtHelm;
end;

procedure TT3HelmGuidance.SetFirstHeading(const Value: double);
begin
  FFirstHeading := Value;
end;

procedure TT3HelmGuidance.SetHelmDegree(const Value: double);
var
  order : Double;
begin
  FHelmDegree := Value;
  FHelmTurnRate     := (abs(FHelmDegree)* Tight_Turn_Rate) / Max_Helm_Angle;

  order := Round(ResultHeading) + FHelmDegree;
  if order > 360 then
    OrderedHeading := order - 360
  else
  if order < 0 then
    OrderedHeading := 360 + order
  else
    OrderedHeading := order;
end;

procedure TT3HelmGuidance.SetOrderedHeading(const Value: double);
var
  orderedHeadingAchieved : Boolean;
  dHead : double;
begin
  //inherited;
  FOrderedHeading := Value;

  dHead := getDeltaH(Value, ConvCartesian_To_Compass(Mover.Direction));
  orderedHeadingAchieved := Abs(dHead) < 2.0;

  { this default ordered for straight line }
  if orderedHeadingAchieved then
  begin
    SetHeading(Value);
    FHeadingState := mcsStable;
  end
  else
  if dHead < 0 then
  begin
    Mover.TurnRate := FHelmTurnRate;
    FHeadingState := mcsIncrease
  end
  else
  begin
    Mover.TurnRate := -FHelmTurnRate;
    FHeadingState := mcsDecrease
  end;
end;

end.
