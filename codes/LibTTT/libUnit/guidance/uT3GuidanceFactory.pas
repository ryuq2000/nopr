unit uT3GuidanceFactory;

interface

uses uT3VehicleGuidance, tttData;

type
  TT3GuidanceFactory = class
  public
    constructor Create;
    procedure createGuidance(guidanceType : TVehicleGuidanceType ) ;
  end;

implementation

uses
  uT3StraightLineGuide, uT3HelmGuide;

{ TT3GuidanceFactory }

constructor TT3GuidanceFactory.Create;
begin

end;

procedure TT3GuidanceFactory.createGuidance(guidanceType: TVehicleGuidanceType);
var
  guidance : TT3VehicleGuidance;
begin
  guidance := nil;

  case guidanceType of
    vgtNone         : ;
    vgtStraightLine : guidance := TT3StraightLineGuidance.Create;
    vgtHelm         : guidance := TT3HelmGuidance.Create;
    vgtCircle       : ;
    vgtStation      : ;
    vgtZigzag       : ;
    vgtSinuation    : ;
    vgtFormation    : ;
    vgtEvasion      : ;
    vgtWaypoint     : ;
    vgtOutrun       : ;
    vgtEngagement   : ;
    vgtShadow       : ;
    vgtReturnToBase : ;
  end;

end;

end.
