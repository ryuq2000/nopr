unit uT3OutrunGuide;

interface

uses uT3VehicleGuidance;

type
  TT3OutrunGuidance = class(TT3VehicleGuidance)
  private
    FPlatformInstanceIndex: integer;
    procedure SetPlatformInstanceIndex(const Value: integer);
  protected
    procedure doGuide(const aDeltaMs: Double); override;
  public
    constructor Create; override;

    property PlatformInstanceIndex : integer read FPlatformInstanceIndex write SetPlatformInstanceIndex;
  end;

implementation
uses tttdata;

{ TT3OutrunGuidance }

procedure TT3OutrunGuidance.doGuide(const aDeltaMs: Double);
begin
  inherited;

end;

constructor TT3OutrunGuidance.Create;
begin
  inherited;
  GuidanceType := vgtOutrun;

end;

procedure TT3OutrunGuidance.SetPlatformInstanceIndex(const Value: integer);
begin
  FPlatformInstanceIndex := Value;
end;

end.
