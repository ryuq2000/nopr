unit uT3CircleGuide;

interface

uses uT3VehicleGuidance;

type
  TCGState = (cstateStraight = 1,cstateCircle);
  TCGMode  = (cModePosition = 1,cModeTrack);
  TCGBearing = (cBearingTrue = 1,cBearingRelative );
  TCGForm  = (formNonAbsolute,formAbsolute);

  TT3CircleGuidance = class (TT3VehicleGuidance)
  private
    FCircleMode: TCGMode;
    FCircleRadius: double;
    FCircleBearing: double;
    FTargetIndex: integer;
    FCircleState: TCGState;
    FCircleBearingState: TCGBearing;
    FShadowDistance: double;
    FCircleDirection: double;
    FCenterCirclePointX: double;
    FCenterCirclePointY: double;
    FCircleRange: double;
    procedure SetCircleBearing(const Value: double);
    procedure SetCircleBearingState(const Value: TCGBearing);
    procedure SetCircleDirection(const Value: double);
    procedure SetCircleMode(const Value: TCGMode);
    procedure SetCircleRadius(const Value: double);
    procedure SetCircleState(const Value: TCGState);
    procedure SetShadowDistance(const Value: double);
    procedure SetTargetIndex(const Value: integer);
    procedure SetCenterCirclePointX(const Value: double);
    procedure SetCenterCirclePointY(const Value: double);
    procedure SetCircleRange(const Value: double);
  protected
    procedure doGuide(const aDeltaMs: Double);override;

  public
    constructor Create;override;

    property CircleMode : TCGMode read FCircleMode write SetCircleMode;
    property CircleState : TCGState read FCircleState write SetCircleState;
    property CircleRadius : double read FCircleRadius write SetCircleRadius;
    property CircleDirection : double read FCircleDirection write SetCircleDirection;
    property CircleBearing : double read FCircleBearing write SetCircleBearing;
    property CircleRange : double read FCircleRange write SetCircleRange;
    property CircleBearingState : TCGBearing read FCircleBearingState write SetCircleBearingState;
    property ShadowDistance : double read FShadowDistance write SetShadowDistance;

    property CenterCirclePointX : double read FCenterCirclePointX write SetCenterCirclePointX;
    property CenterCirclePointY : double read FCenterCirclePointY write SetCenterCirclePointY;

    property TargetInstanceIndex : integer read FTargetIndex write SetTargetIndex;
  end;

implementation

uses tttData;

{ TT3CircleGuidance }

procedure TT3CircleGuidance.doGuide(const aDeltaMs: Double);
begin
  inherited;

end;

constructor TT3CircleGuidance.Create;
begin
  inherited;
  GuidanceType := vgtCircle;

end;

procedure TT3CircleGuidance.SetCenterCirclePointX(const Value: double);
begin
  FCenterCirclePointX := Value;
end;

procedure TT3CircleGuidance.SetCenterCirclePointY(const Value: double);
begin
  FCenterCirclePointY := Value;
end;

procedure TT3CircleGuidance.SetCircleBearing(const Value: double);
begin
  FCircleBearing := Value;
end;

procedure TT3CircleGuidance.SetCircleBearingState(const Value: TCGBearing);
begin
  FCircleBearingState := Value;
end;

procedure TT3CircleGuidance.SetCircleDirection(const Value: double);
begin
  FCircleDirection := Value;
end;

procedure TT3CircleGuidance.SetCircleMode(const Value: TCGMode);
begin
  FCircleMode := Value;
end;

procedure TT3CircleGuidance.SetCircleRadius(const Value: double);
begin
  FCircleRadius := Value;
end;

procedure TT3CircleGuidance.SetCircleRange(const Value: double);
begin
  FCircleRange := Value;
end;

procedure TT3CircleGuidance.SetCircleState(const Value: TCGState);
begin
  FCircleState := Value;
end;

procedure TT3CircleGuidance.SetShadowDistance(const Value: double);
begin
  FShadowDistance := Value;
end;

procedure TT3CircleGuidance.SetTargetIndex(const Value: integer);
begin
  FTargetIndex := Value;
end;

end.
