unit uT3WaypointGuide;

interface

uses uT3VehicleGuidance;

type

  TT3WaypointGuidance = class(TT3VehicleGuidance)
  protected
    procedure doGuide(const aDeltaMs: Double); override;
  public
    constructor Create;override;
  end;

implementation
uses tttdata;

{ TT3WaypointGuidance }

procedure TT3WaypointGuidance.doGuide(const aDeltaMs: Double);
begin
  inherited;

end;

constructor TT3WaypointGuidance.Create;
begin
  inherited;
  GuidanceType := vgtWaypoint;

end;

end.
