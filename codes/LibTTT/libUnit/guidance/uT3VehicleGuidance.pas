unit uT3VehicleGuidance;

interface

uses tttData, u2DMover,
  uBaseCoordSystem;

type
  TMoveChangeState  = (mcsStable, mcsIncrease, mcsDecrease);

  TT3VehicleGuidance = class abstract
  private

    FGuidanceType: TVehicleGuidanceType;
    FOwnAltitude: double;
    FOwnHeading: double;
    FOwnSpeed: double;
    FAcceleration: double;
    FNormal_Climb_Rate: double;
    FStandard_Turn_Rate: double;
    FDeceleration: double;
    FMax_Helm_Angle: double;
    FMaxGroundSpeed: double;
    FTight_Turn_Rate: double;
    FVertical_Accel: double;
    FMinGroundSpeed: double;
    FNormal_Descent_Rate: double;
    FMover: T2DMover;
    procedure SetGuidanceType(const Value: TVehicleGuidanceType);
    procedure SetAcceleration(const Value: double);
    procedure SetDeceleration(const Value: double);
    procedure SetMax_Helm_Angle(const Value: double);
    procedure SetMaxGroundSpeed(const Value: double);
    procedure SetMinGroundSpeed(const Value: double);
    procedure SetNormal_Climb_Rate(const Value: double);
    procedure SetStandard_Turn_Rate(const Value: double);
    procedure SetTight_Turn_Rate(const Value: double);
    procedure SetVertical_Accel(const Value: double);
    procedure SetNormal_Descent_Rate(const Value: double);
    procedure SetMover(const Value: T2DMover);

    { set mover heading }
    function getHeading: double;
    function getAltitude: double;
    function getSpeed: double;
    procedure setAltitude(const Value: double);
    procedure setSpeed(const Value: double);

  protected
    FOrderedSpeed: double;
    FOrderedHeading: double;
    FOrderedAltitude: double;

    { movement state }
    FSpeedState: TMoveChangeState;
    FHeadingState: TMoveChangeState;
    FAltitudeState: TMoveChangeState;

    //FDeltaHead: Double;

    procedure CalcSpeed ; virtual;
    procedure CalcAltitude; virtual;
    procedure CalcHeading; virtual;

    procedure doGuide(const aDeltaMs: Double);virtual;abstract;

    procedure SetHeading(Value : double);
    procedure SetOrderedHeading(const Value: double); virtual;
    procedure SetOrderedGroundSpeed(const Value: double);virtual;
    procedure SetOrderedAltitude(const Value: double); virtual;

  published
  public
    constructor Create; virtual;
    { must be implemented in inherited }
    procedure CalcGuidance(const aDeltaMs: Double);
    procedure GuidanceInit;virtual;

    property GuidanceType : TVehicleGuidanceType read FGuidanceType write SetGuidanceType;

    property OrderedHeading : double read FOrderedHeading write SetOrderedHeading;
    property OrderedSpeed : double read FOrderedSpeed write SetOrderedGroundSpeed;
    property OrderedAltitude : double read FOrderedAltitude write SetOrderedAltitude;

    property ResultHeading  : double read getHeading write setHeading;
    property ResultSpeed    : double read getSpeed write setSpeed;
    property ResultAltitude : double read getAltitude write setAltitude;

    property MaxGroundSpeed : double read FMaxGroundSpeed write SetMaxGroundSpeed;
    property MinGroundSpeed : double read FMinGroundSpeed write SetMinGroundSpeed;
    property Acceleration : double read FAcceleration write SetAcceleration;
    property Deceleration : double read FDeceleration write SetDeceleration;

    property Tight_Turn_Rate : double read FTight_Turn_Rate write SetTight_Turn_Rate;
    property Max_Helm_Angle : double read FMax_Helm_Angle write SetMax_Helm_Angle;
    property Standard_Turn_Rate : double read FStandard_Turn_Rate write SetStandard_Turn_Rate;

    property Normal_Climb_Rate : double read FNormal_Climb_Rate write SetNormal_Climb_Rate;
    property Vertical_Accel : double read FVertical_Accel write SetVertical_Accel;
    property Normal_Descent_Rate : double read FNormal_Descent_Rate write SetNormal_Descent_Rate;

    property Mover : T2DMover read FMover write SetMover;

  end;

implementation


{ TT3VehicleGuidance }

procedure TT3VehicleGuidance.CalcAltitude;
begin
  { altitude change }
  if FAltitudeState = mcsIncrease then
  begin
    if FMover.Z >= FOrderedAltitude then
    begin
      FMover.Z := FOrderedAltitude;
      FMover.Vertical_Accel := 0.0;
      FMover.VerticalSpeed := 0.0;
      FMover.ClimbRate := 0;
      FMover.DescentRate := 0;
      FAltitudeState := mcsStable;
    end
  end
  else if FAltitudeState = mcsDecrease then
  begin
    if FMover.Z <= FOrderedAltitude then
    begin
      FMover.Z := FOrderedAltitude;
      FMover.Vertical_Accel := 0.0;
      FMover.VerticalSpeed := 0.0;
      FMover.ClimbRate := 0;
      FMover.DescentRate := 0;
      FAltitudeState := mcsStable;
    end;
  end
  else if FAltitudeState = mcsStable then
  begin
    FMover.Vertical_Accel := 0.0;
    FMover.ClimbRate := 0;
    FMover.DescentRate := 0;
    FMover.VerticalSpeed := 0.0;
  end;
end;

procedure TT3VehicleGuidance.CalcGuidance(const aDeltaMs: Double);
begin
  if not Assigned(FMover) then
    Exit;

  { do guide here }
  doGuide(aDeltaMs);

  CalcSpeed;
  CalcAltitude;
  CalcHeading;

  FMover.Move(aDeltaMs);
end;

procedure TT3VehicleGuidance.CalcHeading;
var
  dHead : double;
  orderedHeadingAchieved : Boolean;
begin
  { do custom heading calc on inherited class }

  { heading change }
  dHead := getDeltaH(OrderedHeading, ResultHeading);
  orderedHeadingAchieved := Abs(dHead) < 2.0;

  if orderedHeadingAchieved then
  begin
    FMover.TurnRate := 0.0;
    FHeadingState := mcsStable;
    SetHeading(FOrderedHeading);
  end

end;

procedure TT3VehicleGuidance.CalcSpeed;
begin
  { speed calc }
  if FSpeedState = mcsIncrease then
  begin
    if (FMover.Speed >= FOrderedSpeed) then
    begin
      FMover.Speed := FOrderedSpeed;
      FMover.Acceleration := 0.0;
      FSpeedState := mcsStable;
    end;
    if FMover.Speed >= MaxGroundSpeed then
    begin
      FMover.Acceleration := 0;
      FMover.Speed := MaxGroundSpeed;
      FSpeedState := mcsStable;
    end;
  end
  else if FSpeedState = mcsDecrease then
  begin
    if FMover.Speed < FOrderedSpeed then
    begin
      FMover.Speed := FOrderedSpeed;
      FMover.Acceleration := 0.0;
      FSpeedState := mcsStable;
    end;
  end;
end;

constructor TT3VehicleGuidance.Create;
begin
  FGuidanceType := vgtNone;

  FAltitudeState := mcsStable;
  FSpeedState := mcsStable;
  FHeadingState := mcsStable;
end;

function TT3VehicleGuidance.getAltitude: double;
begin
  Result := FMover.Z;
end;

function TT3VehicleGuidance.getHeading: double;
begin
  Result := ConvCartesian_To_Compass(FMover.Direction);
end;

function TT3VehicleGuidance.getSpeed: double;
begin
  Result := FMover.Speed;
end;

procedure TT3VehicleGuidance.GuidanceInit;
begin

  FMover.Vertical_Accel := 0.0;
  FMover.VerticalSpeed := 0.0;
  FMover.ClimbRate := 0;
  FMover.DescentRate := 0;
  FAltitudeState := mcsStable;

  FMover.TurnRate := 0.0;
  FHeadingState := mcsStable;

  FMover.Acceleration := 0.0;
  FSpeedState := mcsStable;

end;

procedure TT3VehicleGuidance.SetAcceleration(const Value: double);
begin
  FAcceleration := Value;
end;

procedure TT3VehicleGuidance.setAltitude(const Value: double);
begin
  FMover.Z := Value;
end;

procedure TT3VehicleGuidance.SetDeceleration(const Value: double);
begin
  FDeceleration := Value;
end;

procedure TT3VehicleGuidance.SetGuidanceType(const Value: TVehicleGuidanceType);
begin
  FGuidanceType := Value;
end;

procedure TT3VehicleGuidance.SetHeading(Value: double);
begin
  if Assigned(FMover) then
    FMover.Direction := ConvCompass_To_Cartesian(Value);
end;

procedure TT3VehicleGuidance.SetMaxGroundSpeed(const Value: double);
begin
  FMaxGroundSpeed := Value;
end;

procedure TT3VehicleGuidance.SetMax_Helm_Angle(const Value: double);
begin
  FMax_Helm_Angle := Value;
end;

procedure TT3VehicleGuidance.SetMinGroundSpeed(const Value: double);
begin
  FMinGroundSpeed := Value;
end;

procedure TT3VehicleGuidance.SetMover(const Value: T2DMover);
begin
  FMover := Value;
end;

procedure TT3VehicleGuidance.SetNormal_Climb_Rate(const Value: double);
begin
  FNormal_Climb_Rate := Value;
end;

procedure TT3VehicleGuidance.SetNormal_Descent_Rate(const Value: double);
begin
  FNormal_Descent_Rate := Value;
end;

procedure TT3VehicleGuidance.SetOrderedAltitude(const Value: double);
var
  dH: Double;
begin
  FOrderedAltitude := Value / (C_NauticalMile_To_Metre *
      C_Degree_To_NauticalMile);

  dH := (OrderedAltitude - Mover.Z);
  if Abs(dH) < 0.000000000001 then
  begin
    FAltitudeState := mcsStable;
    Mover.Vertical_Accel := 0;
  end
  else
  begin
    if OrderedAltitude > Mover.Z then
    begin
      Mover.ClimbRate := Normal_Climb_Rate;
      Mover.Vertical_Accel := Abs(Vertical_Accel);

      FAltitudeState := mcsIncrease;
    end
    else
    begin
      Mover.DescentRate := Normal_Descent_Rate;
      Mover.Vertical_Accel := -Abs(Vertical_Accel);

      FAltitudeState := mcsDecrease;
    end;
  end;
end;

procedure TT3VehicleGuidance.SetOrderedGroundSpeed(const Value: double);
begin
  FOrderedSpeed := Value;

  if not Assigned(Mover) then
    Exit;

  if OrderedSpeed > Mover.Speed then
  begin
    Mover.Acceleration := Acceleration; // knot per second
    FSpeedState := mcsIncrease;
  end
  else if OrderedSpeed < Mover.Speed then
  begin
    Mover.Acceleration := -Abs(Deceleration);
    // knot per second
    FSpeedState := mcsDecrease;
  end
  else
  begin
    Mover.Acceleration := 0.0;
    FSpeedState := mcsStable;

  end;
end;

procedure TT3VehicleGuidance.SetOrderedHeading(const Value: double);
var
  orderedHeadingAchieved : Boolean;
  dHead : double;
begin

  FOrderedHeading := Value;

  dHead := getDeltaH(Value, ConvCartesian_To_Compass(Mover.Direction));
  orderedHeadingAchieved := Abs(dHead) < 2.0;

  { this default ordered for straight line }
  if orderedHeadingAchieved then
  begin
    SetHeading(Value);
    FHeadingState := mcsStable;
  end
  else
  if dHead < 0 then
  begin
    Mover.TurnRate := Standard_Turn_Rate;
    FHeadingState := mcsIncrease
  end
  else
  begin
    Mover.TurnRate := -Standard_Turn_Rate;
    FHeadingState := mcsDecrease
  end;

end;

procedure TT3VehicleGuidance.setSpeed(const Value: double);
begin
  FMover.Speed := Value;
end;

procedure TT3VehicleGuidance.SetStandard_Turn_Rate(const Value: double);
begin
  FStandard_Turn_Rate := Value;
end;

procedure TT3VehicleGuidance.SetTight_Turn_Rate(const Value: double);
begin
  FTight_Turn_Rate := Value;
end;

procedure TT3VehicleGuidance.SetVertical_Accel(const Value: double);
begin
  FVertical_Accel := Value;
end;

end.
