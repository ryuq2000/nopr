unit uT3SinuationGuide;

interface

uses uT3VehicleGuidance, tttData;

type
  TT3SinuationGuidance = class (TT3VehicleGuidance)
  private
    FAmplitudoSinuation: double;
    FOrderedSinuation: Integer;
    FPeriodSinuation: double;
    procedure SetAmplitudoSinuation(const Value: double);
    procedure SetOrderedSinuation(const Value: Integer);
    procedure SetPeriodSinuation(const Value: double);
  protected
    procedure doGuide(const aDeltaMs: Double); override;
  public
    constructor Create;override;

    property OrderedSinuation : Integer read FOrderedSinuation write SetOrderedSinuation;
    property PeriodSinuation  : double read FPeriodSinuation write SetPeriodSinuation;
    property AmplitudoSinuation : double read FAmplitudoSinuation write SetAmplitudoSinuation;

  end;

implementation

{ TT3SinuationGuidance }

procedure TT3SinuationGuidance.doGuide(const aDeltaMs: Double);
begin
  inherited;

end;

constructor TT3SinuationGuidance.Create;
begin
  inherited;
  GuidanceType := vgtSinuation;

end;

procedure TT3SinuationGuidance.SetAmplitudoSinuation(const Value: double);
begin
  FAmplitudoSinuation := Value;
end;

procedure TT3SinuationGuidance.SetOrderedSinuation(const Value: Integer);
begin
  FOrderedSinuation := Value;
end;

procedure TT3SinuationGuidance.SetPeriodSinuation(const Value: double);
begin
  FPeriodSinuation := Value;
end;

end.
