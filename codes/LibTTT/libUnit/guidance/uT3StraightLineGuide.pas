unit uT3StraightLineGuide;

interface

uses uT3VehicleGuidance, tttData, uBaseCoordSystem;

type
  TT3StraightLineGuidance = class (TT3VehicleGuidance)
  private
  protected
    procedure doGuide(const aDeltaMs: Double); override;
    procedure SetOrderedHeading(const Value: double); override;
    procedure SetOrderedGroundSpeed(const Value: double);override;
    procedure SetOrderedAltitude(const Value: double); override;
  public
    constructor Create;override;

  end;

implementation

{ TT3StraightLineGuidance }

procedure TT3StraightLineGuidance.doGuide(const aDeltaMs: Double);
begin
  inherited;

end;

constructor TT3StraightLineGuidance.Create;
begin
  inherited;
  GuidanceType := vgtStraightLine;
end;

procedure TT3StraightLineGuidance.SetOrderedAltitude(const Value: double);
begin
  inherited;
end;

procedure TT3StraightLineGuidance.SetOrderedGroundSpeed(const Value: double);
begin
  inherited;

end;

procedure TT3StraightLineGuidance.SetOrderedHeading(const Value: double);
begin
  inherited;


end;

end.
