unit uT3ZigzagGuide;

interface

uses uT3VehicleGuidance;

type
  TZigZagLegType = (zzShort = 1, zzLong, zzVeryLong);

  TT3ZigZagGuidance = class(TT3VehicleGuidance)
  private
    FPeriodZigzag: double;
    FAmplitudoZigzag: double;
    FZigZagLegType: TZigZagLegType;
    FOrderedZigzag: double;
    procedure SetAmplitudoZigzag(const Value: double);
    procedure SetPeriodZigzag(const Value: double);
    procedure SetZigZagLegType(const Value: TZigZagLegType);
    procedure SetOrderedZigzag(const Value: double);
  protected
    procedure doGuide(const aDeltaMs: Double); override;
  public
    constructor Create; override;

    property AmplitudoZigzag
      : double read FAmplitudoZigzag write SetAmplitudoZigzag;
    property PeriodZigzag: double read FPeriodZigzag write SetPeriodZigzag;
    property ZigZagLegType: TZigZagLegType read FZigZagLegType write
      SetZigZagLegType;
    property OrderedZigzag : double read FOrderedZigzag write SetOrderedZigzag;

  end;

implementation

uses tttData;

{ TT3ZigZagGuidance }

procedure TT3ZigZagGuidance.doGuide(const aDeltaMs: Double);
begin
  inherited;

end;

constructor TT3ZigZagGuidance.Create;
begin
  inherited;
  GuidanceType := vgtZigzag;
end;

procedure TT3ZigZagGuidance.SetAmplitudoZigzag(const Value: double);
begin
  FAmplitudoZigzag := Value;
end;

procedure TT3ZigZagGuidance.SetOrderedZigzag(const Value: double);
begin
  FOrderedZigzag := Value;
end;

procedure TT3ZigZagGuidance.SetPeriodZigzag(const Value: double);
begin
  FPeriodZigzag := Value;
end;

procedure TT3ZigZagGuidance.SetZigZagLegType(const Value: TZigZagLegType);
begin
//  FZigZagLegType := Value;
//
//  case FZigZagLegType of
//    zzShort:
//      if queryGameDefault <> nil then
//      begin
//        FAmplitudoZigzag := queryGameDefault.FData.Short_Period_Amplitude;
//        FPeriodZigzag := queryGameDefault.FData.Short_Period_Distance;
//      end
//      else
//      begin
//        FAmplitudoZigzag := 0.5;
//        FPeriodZigzag := 2;
//      end
//      ;
//    zzLong:
//      if queryGameDefault <> nil then
//      begin
//        FAmplitudoZigzag := queryGameDefault.FData.Long_Period_Amplitude;
//        FPeriodZigzag := queryGameDefault.FData.Long_Period_Distance;
//      end
//      else
//      begin
//        FAmplitudoZigzag := 0.75;
//        FPeriodZigzag := 4;
//      end;
//    zzVeryLong:
//      if queryGameDefault <> nil then
//      begin
//        FAmplitudoZigzag := queryGameDefault.FData.Very_Period_Amplitude;
//        FPeriodZigzag := queryGameDefault.FData.Very_Period_Distance;
//      end
//      else
//      begin
//        FAmplitudoZigzag := 1;
//        FPeriodZigzag := 6;
//      end;
//
//  end;
end;

end.
