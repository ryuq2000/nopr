unit uT3BearingTrack;

interface

uses Generics.Collections, uSimObjects, Classes, uT3MountedSensor, uT3Track,
  uSteppers;

type

  TT3BearingTrack = class (TT3Track)
  private
    FBearing         : double;
    FValidRTTrack    : Boolean;
    FAmbigousBearing : Boolean;
    FObjectSensorId  : integer;
    FDelayer         : TDelayer;
    FRange         : double;
    FRangeInterval : double;

    procedure SetBearing(const Value: double);
    procedure SetAmbigousBearing(const Value: Boolean);
    procedure SetObjectSensorId(const Value: integer);
    procedure OnDelayedAction(Sender : Tobject);
    procedure SetRange(const Value: double);
    procedure SetRangeInterval(const Value: double);
  public
    constructor Create;override;
    destructor Destroy;override;
    procedure Move(const aDeltaMs: double); override;

    { id for detected sensor by esm , set 0 for non real}
    property ObjectSensorId  : integer read FObjectSensorId write SetObjectSensorId;
    property Bearing         : double read FBearing write SetBearing;
    property Range           : double read FRange write SetRange;
    property RangeInterval   : double read FRangeInterval write SetRangeInterval;
    { for esm if ambigous bearing enabled }
    property AmbigousBearing : Boolean read FAmbigousBearing write SetAmbigousBearing;
    property ValidRTTrack    : Boolean read FValidRTTrack;
  end;

implementation

uses uT3PlatformInstance, uGlobalVar,
  uT3MountedRadar, tttData,
  uSensorinfo, uBaseCoordSystem,
  uT3Vehicle, uT3MountedESM;

{ TT3BearingTrack }

constructor TT3BearingTrack.Create;
begin
  inherited;
  FValidRTTrack    := True;
  FAmbigousBearing := False;

  FDelayer := TDelayer.Create;
  FDelayer.DInterval := 1;
  FDelayer.OnTime := OnDelayedAction;

end;

destructor TT3BearingTrack.Destroy;
begin
  FDelayer.Free;
  inherited;
end;

procedure TT3BearingTrack.Move(const aDeltaMs: double);
var
  pi,pi2 : TT3PlatformInstance;
  sensor : TPair<TT3MountedSensor,TSensorTrackInfo>;
  abearing, orientation : double;
  radar  : TT3MountedRadar;
  update : boolean;
begin
  inherited;

  FDelayer.Run(aDeltaMs);

  { always update track position to platform owner }
  if TrackCategory = tcRealTrack then
  begin
    { jika RT Track dan tidak dideteksi dengan sensor apapun = invalid }
    if DetectedBySensors.Count <= 0 then
      FValidRTTrack := False
    else
    begin
      FValidRTTrack := True;
      {ambil satu sensor saja utk update asal pos bearing }
      for sensor in DetectedBySensors do
      begin

        if not (sensor.Key is TT3MountedESM) then
          continue;

        pi := simManager.FindT3PlatformByID(sensor.Key.PlatformParent.InstanceIndex);

        if Assigned(pi) then
        begin
          setPositionX(Pi.getPositionX);
          setPositionY(Pi.getPositionY);
          setPositionZ(Pi.getPositionZ);
        end
        else
          exit;

        break;
      end;
    end;

  end;

end;

procedure TT3BearingTrack.OnDelayedAction(Sender: Tobject);
begin

end;

procedure TT3BearingTrack.SetAmbigousBearing(const Value: Boolean);
begin
  FAmbigousBearing := Value;
end;

procedure TT3BearingTrack.SetBearing(const Value: double);
begin
  FBearing := Value;
end;

procedure TT3BearingTrack.SetObjectSensorId(const Value: integer);
begin
  FObjectSensorId := Value;
end;

procedure TT3BearingTrack.SetRange(const Value: double);
begin
  FRange := Value;
end;

procedure TT3BearingTrack.SetRangeInterval(const Value: double);
begin
  FRangeInterval := Value;
end;

end.
