unit uT3PlatformInstance;

interface

uses uT3Unit,
  tttData, u2DMover, SysUtils, uGameData_TTT, uDBClassDefinition;

type
  TT3PlatformInstance = class abstract (TT3Unit)
  private
    FForceDesignation   : integer;
    FTrackID            : String;

    {*------------------------------------------------------------------------------
      lookup to FDBMotionCharacteristics : FPlatformMotion = Motion_Index
    -------------------------------------------------------------------------------}
    FPlatformMotion     : integer;

    FPlatformCategory   : integer;
    FPlatformType       : integer;
    FPlatformDomain     : integer;
    FPlatformGroup      : integer;

    FDetectabilityType  : Integer;

    procedure SetForceDesignation(const Value: integer);
    procedure SetTrackID(const Value: String);
    procedure SetPlatformMotion(const Value: integer);
    procedure SetPlatformCategory(const Value: integer);
    procedure SetPlatformDomain(const Value: integer);
    procedure SetPlatformType(const Value: integer);
    procedure setHealthStatus(const Index: Integer; Value: single);
    procedure updateHealthStatus(const i: integer; val: single); virtual;

    function getHealthStatus(const Index: Integer): single;
    procedure SetPlatformGroup(const Value: integer);
    function getDamagePercent(const Index: Integer): integer;
    function getFuelPercentage: double;

  protected
    {*------------------------------------------------------------------------------
      base on platform type (see DBScenario on LoadPlatformDefinition function),
      FPlatformDefinition lookup to platform dict can be -> FDBVehicleDefinition,
      FDBTorpedo_Definition, FDBSatellite_Definition, FDBMissileDefinition,
      FDBMine_Definition, FDBSonobuoy_Definition

      FPlatformDefinition = platform index
    -------------------------------------------------------------------------------}
    FDBPlatformDefinitionIndex : integer;
    FDBPlatformDefinition      : TObject;

    FDBPlatformMotionIndex     : integer;
    FDBPlatformMotion          : TDBMotion_Characteristics;

    FMover            : T2DMover;

    { damage state }
    FDamageStatus   : TRecDamageStatus;

    // Endurance properties - Range
    FRangeEndurance   : double;           // nm
    FRangeRemaining   : double;

    // Endurance properties - Time
    FTimeEndurance    : double;          // second
    FTimeRemaining    : double;

    // Endurance properties - Fuel
    FFuelEndurance    : TRecFuelEndurance;   // consumption / hour
    FFuelCapacity     : double;
    FFuelRemaining    : double;

    function getAltitude: single; virtual;
    function getGroundSpeed: Double; virtual;
    function getHeading: Double; virtual;
    function getPos(const Index: Integer): double; virtual;
    procedure SetAltitude(const Value: single); virtual;
    procedure setGroundSpeed(const Value: Double); virtual;
    procedure setHeading(const Value: Double); virtual;
    procedure setPos(const Index: Integer; const Value: double); virtual;

    { get plpatform class name, must override on subclass, to get platform class }
    function getPlatformClass: string; virtual;
  published

  public
    constructor Create; override;
    destructor Destroy; override;
    procedure Initialize;override;

    procedure Move(const aDeltaMs: double); override;
    procedure setPositionX(const v: double); override;
    procedure setPositionY(const v: double); override;
    procedure setPositionZ(const v: double); override;

    function GetMovementData: TRecUDP_PlatformMovement;
    procedure SetMovementData(const aRec: TRecUDP_PlatformMovement);

    property Altitude : single read getAltitude write SetAltitude;
    property Course   : double index 1 read getPos write setPos;
    property Speed    : double index 2 read getPos write setPos;
    property Heading  : Double  read getHeading write setHeading;
    property GroundSpeed: Double read getGroundSpeed write setGroundSpeed;

    property PlatformType   : integer  read FPlatformType write SetPlatformType;
    property PlatformDomain : integer  read FPlatformDomain write SetPlatformDomain;
    property PlatformCategory : integer  read FPlatformCategory write SetPlatformCategory;

    { DB class definition, read only -> prevent change in database object property }
    property PlatformDefinitionIndex : integer read FDBPlatformDefinitionIndex write FDBPlatformDefinitionIndex;
    property PlatformDefinition : TObject read FDBPlatformDefinition;

    { DB motion platform definition}
    property PlatformMotionIndex : integer read FDBPlatformMotionIndex write FDBPlatformMotionIndex;
    property PlatformMotion : TDBMotion_Characteristics read FDBPlatformMotion;

    { platform detectability }
    property DetectabilityType : Integer read FDetectabilityType write FDetectabilityType;
    { platform group }
    property PlatformGroup : integer read FPlatformGroup write SetPlatformGroup;
    { get platform class name / readonly }
    property PlatformClass : string read getPlatformClass;

    property ForceDesignation : integer read FForceDesignation write SetForceDesignation;
    property TrackID : String read FTrackID write SetTrackID;
    property Mover : T2DMover read FMover;

    { damage state }
    property  DamageSensor       : boolean read FDamageStatus.SensorDamage write FDamageStatus.SensorDamage;
    property  DamageWeapon       : boolean read FDamageStatus.WeaponDamage write FDamageStatus.WeaponDamage;
    property  DamageComm         : boolean read FDamageStatus.CommDamage write FDamageStatus.CommDamage;
    property  DamageHelm         : boolean read FDamageStatus.HelmDamage write FDamageStatus.HelmDamage;
    property  DamagePropulsion   : boolean read FDamageStatus.PropultionDamage write FDamageStatus.PropultionDamage;
    property  FuelLeakage        : boolean read FDamageStatus.FuelLeakage write FDamageStatus.FuelLeakage;
    property  Health             : single index 1  read getHealthStatus write setHealthStatus;
    property  HealthPercent      : single index 2 read getHealthStatus write setHealthStatus;
    property  DamagePercentSpeed : integer index 1 read getDamagePercent;
    property  DamageOverall      : integer index 2 read getDamagePercent;
    property  FuelPercentage     : double read getFuelPercentage;

  end;

implementation

{ TT3PlatformInstance }

constructor TT3PlatformInstance.Create;
begin
  inherited;
  FMover    := T2DMover.Create;

  with FDamageStatus do begin
    CurrentHealth   := 100.0;
    DamageCapacity  := 100.0;
    PercentHealth   := 100.0;
    PercentMaxSpeed := 100.0;
    SensorDamage    := false;
    WeaponDamage    := false;
    CommDamage      := false;
    HelmDamage      := false;
    PropultionDamage:= false;
  end;

end;

destructor TT3PlatformInstance.Destroy;
begin

  FreeAndNil(FMover);
  inherited;
end;

function TT3PlatformInstance.getAltitude: single;
begin
  result := 0;
end;

function TT3PlatformInstance.getDamagePercent(const Index: Integer): integer;
begin
  result := 0;

  case Index of
    1: result := Round(FDamageStatus.PercentMaxSpeed);
    2: result := Round(100 - FDamageStatus.PercentHealth);
  end;
end;

function TT3PlatformInstance.getFuelPercentage: double;
begin
  result := 0.00;
  if FFuelCapacity > 0 then
    result := (FFuelRemaining / FFuelCapacity) * 100;
end;

function TT3PlatformInstance.getGroundSpeed: Double;
begin
  result := 0;
end;

function TT3PlatformInstance.getHeading: Double;
begin
  result := 0;
end;

function TT3PlatformInstance.getHealthStatus(const Index: Integer): single;
begin
  result := 0;

  case Index of
   1 : result := FDamageStatus.CurrentHealth;
   2 : result := Round(FDamageStatus.PercentHealth);
  end;

end;

function TT3PlatformInstance.GetMovementData: TRecUDP_PlatformMovement;
begin
  result.PlatformID := InstanceIndex;

  result.X := getPositionX;
  result.Y := getPositionY;
  result.Z := getPositionZ;

  result.Course     := Heading;       //course;
  result.Speed      := GroundSpeed;   //Speed;
  result.Accel      := 0;
  result.TurnRate   := 0;

  result.FuelRemaining := FFuelRemaining;
  result.TimeRemaining := FTimeRemaining;
  result.RangeRemaining:= FRangeRemaining;

end;

function TT3PlatformInstance.getPlatformClass: string;
begin

end;

function TT3PlatformInstance.getPos(const Index: Integer): double;
begin
  result := 0;
end;

procedure TT3PlatformInstance.Initialize;
begin
  inherited;

  DetectabilityType := dtNormalDetection;
end;

procedure TT3PlatformInstance.Move(const aDeltaMs: double);
begin
  inherited;

end;

procedure TT3PlatformInstance.SetAltitude(const Value: single);
begin

end;

procedure TT3PlatformInstance.SetForceDesignation(const Value: integer);
begin
  FForceDesignation := Value;
end;

procedure TT3PlatformInstance.setGroundSpeed(const Value: Double);
begin

end;

procedure TT3PlatformInstance.setHeading(const Value: Double);
begin

end;

procedure TT3PlatformInstance.setHealthStatus(const Index: Integer;
  Value: single);
begin
  case Index of
    1 : begin
      if Value < 0 then Value := 0;
      if Value > FDamageStatus.DamageCapacity  then
        Value := FDamageStatus.DamageCapacity;

      FDamageStatus.CurrentHealth := Value;
    end;
    2 : begin
      if Value < 0    then Value := 0;
      if Value > 100  then Value := 100;

      FDamageStatus.PercentHealth := Value;
      FDamageStatus.CurrentHealth := (0.01 * Value) * FDamageStatus.DamageCapacity;
    end;
  end;

  updateHealthStatus(Index, Value);

end;

procedure TT3PlatformInstance.SetMovementData(
  const aRec: TRecUDP_PlatformMovement);
begin
//  if FIsRepositioning then exit;

  setPositionX(aRec.X);
  setPositionY(aRec.Y);
  setPositionZ(aRec.Z);

  {Speed}  GroundSpeed := aRec.Speed;
  {Course} Heading     := aRec.Course;

//  FuelRemaining   := aRec.FuelRemaining;
//  MLRemaining := aRec.MLRemaining;
//  ATRemaining := aRec.ATRemaining;
//  FoodRemaining := aRec.FoodRemaining;
//  WaterRemaining := aRec.WaterRemaining;
  FTimeRemaining  := aRec.TimeRemaining;
  FRangeRemaining := aRec.RangeRemaining;

end;

procedure TT3PlatformInstance.SetPlatformCategory(const Value: integer);
begin
  FPlatformCategory := Value;
end;

procedure TT3PlatformInstance.SetPlatformDomain(const Value: integer);
begin
  FPlatformDomain := Value;
end;

procedure TT3PlatformInstance.SetPlatformGroup(const Value: integer);
begin
  FPlatformGroup := Value;
end;

procedure TT3PlatformInstance.SetPlatformMotion(
  const Value: integer);
begin
  FPlatformMotion := Value;
end;

procedure TT3PlatformInstance.SetPlatformType(const Value: integer);
begin
  FPlatformType := Value;
end;

procedure TT3PlatformInstance.setPos(const Index: Integer; const Value: double);
begin

end;

procedure TT3PlatformInstance.setPositionX(const v: double);
begin
  inherited;
  FMover.X := v;
end;

procedure TT3PlatformInstance.setPositionY(const v: double);
begin
  inherited;
  FMover.Y := v;
end;

procedure TT3PlatformInstance.setPositionZ(const v: double);
begin
  inherited;
  FMover.Z := v;
end;

procedure TT3PlatformInstance.SetTrackID(const Value: String);
begin
  FTrackID := Value;
end;

procedure TT3PlatformInstance.updateHealthStatus(const i: integer; val: single);
begin
  with FDamageStatus do begin
    if DamageCapacity > 0 then
      PercentHealth :=  (CurrentHealth * 100) / DamageCapacity
    else
      PercentHealth := 100;

    //if Assigned(FOnDamageChanged) then
    //  FOnDamageChanged(self, diOverall);
  end;
end;

end.
