unit uT3Vehicle;

interface

uses uT3PlatformInstance, uT3vehicleGuidance, tttData,
  Generics.Collections, uDBClassDefinition {uT3MountedSensor, uDBAsset_Vehicle,
  uT3MountedWeapon, uT3MountedECM, uWaypoint, uFormationInfo};

type

  TMotionDataType = (mdMaxGSpeed,mdMinGSpeed,mdTightTurnRate,mdAccel,mdDecel,
    mdMaxHelmAngle, mdStandardTurnRate,mdNormalClimbRate,mdVerticalAccel,
    mdNormalDescentRate);

  TT3Vehicle = class (TT3PlatformInstance)
  private

    { guidance control }
    FVehicleGuidance: TT3VehicleGuidance;
    FGuidances      : TObjectDictionary<TVehicleGuidanceType,TT3VehicleGuidance>;

    { formation status info }
//    FFormationStatus : TFormationInfo;

    FIsOnLand       : boolean;
    FIsGrounded     : boolean;
    FRecentlyUsedWeapon: integer;
    FRecentlyUsedSensor: integer;
    FDBVehicleDefinition: TDBVehicle_Definition;

//    FWaypoints      : TWaypoint;

    function createGuidance(guidanceType : TVehicleGuidanceType ) : TT3VehicleGuidance ;
    function getMotionData(motionType : TMotionDataType) : double;
    procedure guidanceInit;
    procedure calcEnvironmentEffect(var dx, dy : double; const aDt : Double);

    procedure OnWaypointReached(behaviour , events : TObject);

    { procedure from property }
    function getOrderedAltitude: Double;
    function getOrderedHeading: Double;
    function getOrderedSpeed: Double;

    procedure SetVehicleGuidance(const Value: TT3VehicleGuidance);
    procedure SetOrderedAltitude(const Value: Double);
    procedure SetOrderedHeading(const Value: Double);
    procedure SetOrderedSpeed(const Value: Double);
    procedure setGrounded(const Value: boolean);
    procedure setOnLand(const Value: boolean);
    procedure SetRecentlyUsedSensor(const Value: integer);
    procedure SetRecentlyUsedWeapon(const Value: integer);
    procedure SetEMCON_HFDataLinkVehicle(const Value: TEMCON_HFDataLinkVehicle);
    procedure SetEMCON_UHFVHFDataLinkVehicle(
      const Value: TEMCON_UHFVHFDataLinkVehicle);
    procedure SetLastEMCON_HFDLink(const Value: TEMCON_HFDataLinkVehicle);
    procedure SetLastEMCON_UHFVHFDLink(
      const Value: TEMCON_UHFVHFDataLinkVehicle);

  protected
    { course and velocity after environment effect }
    FCourse: Double;
    FVelocity: Double;

    function getHeading: Double; override;
    procedure setHeading(const Value: double); override;
    function getPos(const Index: Integer): Double; override;
    procedure setPos(const Index: Integer; const Value: Double); override;
    function getAltitude: single; override;
    procedure SetAltitude(const Value: single); override;
    function getGroundSpeed: Double; override;
    procedure setGroundSpeed(const Value: Double); override;
    function getPlatformClass: string; override;

  public

    constructor Create;override;
    destructor Destroy;override;

    procedure Initialize;override;
    procedure Move(const aDeltaMs: Double); override;
    procedure changeGuidance(guidanceType : TVehicleGuidanceType );
    procedure DamageRepair(const dt : TDamageItemType);
    function getMaxRangeRadar : double;

    { devices count by mounted device class : ecm, sensor, weapon }
    function getDeviceCount(devBase : String; devClass : TClass) : integer;

    // read only prop
    property VehicleGuidance     : TT3VehicleGuidance read FVehicleGuidance;
    property VehicleDefinition   : TDBVehicle_Definition read FDBVehicleDefinition;

//    property Waypoints      : TWaypoint read FWaypoints;
//    property FormationStatus: TFormationInfo read FFormationStatus;

    property OrderedSpeed: Double read getOrderedSpeed write SetOrderedSpeed;
    property OrderedHeading : Double read getOrderedHeading write SetOrderedHeading;
    property OrderedAltitude: Double read getOrderedAltitude write SetOrderedAltitude;

    { use in weapon form control }
    property RecentlyUsedWeapon : integer read FRecentlyUsedWeapon write SetRecentlyUsedWeapon;
    property RecentlyUsedSensor : integer read FRecentlyUsedSensor write SetRecentlyUsedSensor;

    property OnGrounded: boolean read FIsGrounded write setGrounded;
    property onLand: boolean read FIsOnLand write setOnLand;

    {flag state}
//    property FlagIFF : boolean read FFlagIFF write SetFlagIFF;
  end;

implementation

uses uBaseCoordSystem, {uDBAsset_Scripted,}
  uT3StraightLineGuide, uT3HelmGuide, uT3CircleGuide,
  uT3EngagementGuide, uT3OutrunGuide, uT3ShadowGuide,
  uT3SinuationGuide, uT3StationGuide, uT3WaypointGuide,
  uT3FormationGuide, uT3EvasionGuide, uT3ReturnBaseGuide,
  uT3ZigZagGuide,
  uGlobalVar
  {uT3MountedRadar} ;

const
  C_Dmgs_Speed : array [0..4, 0..2] of single = (
   (04, 18, 26), (19, 34, 42), (35, 50 , 58),
   (54, 66, 74), (75, 82, 90)
   );
  C_Limit_Sensor : array [0..2] of single =
   ( 68, 52, 52 );
  C_Limit_weapon : array [0..2] of single =
   ( 52, 36, 36 );
  C_Limit_comm : array [0..2] of single =
   ( 36, 20, 20 );
  C_Limit_helm : array [0..2] of single =
   ( 20, 20, 20 );

function getDamageIndex(const pDmg: single): integer;
begin
   if pDmg <= 0 then
     result := 0
   else if pDmg <= 20 then
     result := 1
   else if pDmg <=36  then
     result := 2
   else if pDmg <=52  then
     result := 3
   else if pDmg <=68  then
     result := 4
   else if pDmg <=84  then
     result := 5
   else
     result := 6;
end;

function getGTIndex(const dCap: single): byte;
begin
  result := 0;

  if dCap <= 25.0 then // GT < 500
    result := 0
  else if dCap < 75.0 then
    result := 1
  else if dCAp >= 75.0 then
    result := 2;
end;

{ TT3Vehicle }

procedure TT3Vehicle.calcEnvironmentEffect(var dx, dy : double;const aDt : Double);
var
  dmn: Integer;
  temp: boolean;
begin
  dmn := VehicleDefinition.Platform_Domain;
  temp := false;

  if dmn = vhdAir then
    temp := true;

  if Assigned(simManager.GameEnvironment) then
  begin
    if dmn = vhdAir then
    begin
      if Altitude <= 0 then
      begin
        dx := 0;
        dy := 0;
        FCourse := Heading;
        FVelocity := GroundSpeed;
      end
      else
      begin
        simManager.GameEnvironment.getCalc_EnviEffect(dmn, Heading, GroundSpeed, temp,
          FCourse, FVelocity);

        dx := simManager.GameEnvironment.get_DX(dmn, aDt);
        dy := simManager.GameEnvironment.get_DY(dmn, aDt);
      end;
    end
    else if dmn = vhdSurface then
    begin
      if FIsGrounded or FIsOnLand then
      begin
        dx := 0;
        dy := 0;
        FCourse := Heading;
        FVelocity := GroundSpeed;
      end
      else
      begin
        simManager.GameEnvironment.getCalc_EnviEffect(dmn, Heading, GroundSpeed, temp,
          FCourse, FVelocity);

        dx := simManager.GameEnvironment.get_DX(dmn, aDt);
        dy := simManager.GameEnvironment.get_DY(dmn, aDt);
      end;
    end
    else if dmn = vhdSubsurface then
    begin
      if FIsGrounded or FIsOnLand then
      begin
        dx := 0;
        dy := 0;
        FCourse := Heading;
        FVelocity := GroundSpeed;
      end
      else
      begin
        simManager.GameEnvironment.getCalc_EnviEffect(dmn, Heading, GroundSpeed, temp,
          FCourse, FVelocity);

        dx := simManager.GameEnvironment.get_DX(dmn, aDt);
        dy := simManager.GameEnvironment.get_DY(dmn, aDt);
      end;
    end
    else if dmn = vhdAmphibious then
    begin
      if FIsGrounded or FIsOnLand then
      begin
        dx := 0;
        dy := 0;
        FCourse := Heading;
        FVelocity := GroundSpeed;
      end
      else
      begin
        simManager.GameEnvironment.getCalc_EnviEffect(dmn, Heading, GroundSpeed, temp,
          FCourse, FVelocity);

        dx := simManager.GameEnvironment.get_DX(dmn, aDt);
        dy := simManager.GameEnvironment.get_DY(dmn, aDt);
      end;
    end
    else
    begin
      simManager.GameEnvironment.getCalc_EnviEffect(dmn, Heading, GroundSpeed, temp, FCourse,
        FVelocity);

      dx := simManager.GameEnvironment.get_DX(dmn, aDt);
      dy := simManager.GameEnvironment.get_DY(dmn, aDt);
    end;
  end
  else
  begin
    dx := 0;
    dy := 0;
    FVelocity := GroundSpeed;
  end;
end;

procedure TT3Vehicle.changeGuidance(guidanceType: TVehicleGuidanceType);
begin
  SetVehicleGuidance( createGuidance( guidanceType ) );
end;


constructor TT3Vehicle.Create;
begin
  inherited;

  { list of guidances }
  FGuidances  := TObjectDictionary<TVehicleGuidanceType,TT3VehicleGuidance>.Create;
  { default guidance }
  FVehicleGuidance := createGuidance(vgtStraightLine);

  { formation status }
//  FFormationStatus := TFormationInfo.Create;

  { waypoint }
//  FWaypoints:= TWaypoint.Create;
//  FWaypoints.OnPointReached := OnWaypointReached;
//
//  FMountedSensors := TList<TT3MountedSensor>.Create;
//  FMountedWeapons := TList<TT3MountedWeapon>.Create;
//  FMountedECM     := TList<TT3MountedECM>.Create;

  { default recently used }
  FRecentlyUsedSensor := 0;
  FRecentlyUsedWeapon := 0;

  FVelocity  := 0.00;
  FCourse     := 0.00;

  { default damage status }
  with FDamageStatus do begin
    CurrentHealth   := 100.0;
    DamageCapacity  := 100.0;
    PercentHealth   := 100.0;
    PercentMaxSpeed := 100.0;
    SensorDamage    := false;
    WeaponDamage    := false;
    CommDamage      := false;
    HelmDamage      := false;
    PropultionDamage:= false;
  end;

end;

function TT3Vehicle.createGuidance(guidanceType: TVehicleGuidanceType) : TT3VehicleGuidance;
var
  guidance : TT3VehicleGuidance;
begin
  guidance := nil;

  { if laready exist }
  if FGuidances.TryGetValue(guidanceType,guidance) then
    result := guidance
  else
  begin
    case guidanceType of
      vgtNone         : ;
      vgtStraightLine : guidance := TT3StraightLineGuidance.Create;
      vgtHelm         : guidance := TT3HelmGuidance.Create;
      vgtCircle       : guidance := TT3CircleGuidance.Create;
      vgtStation      : guidance := TT3StationGuidance.Create;
      vgtZigzag       : guidance := TT3ZigZagGuidance.Create;
      vgtSinuation    : guidance := TT3SinuationGuidance.Create;
      vgtFormation    : guidance := TT3FormationGuidance.Create;
      vgtWaypoint     : guidance := TT3WaypointGuidance.Create;
      vgtOutrun       : guidance := TT3OutrunGuidance.Create;
      vgtEngagement   : guidance := TT3EngagementGuidance.Create;
      vgtShadow       : guidance := TT3ShadowGuidance.Create;
      vgtReturnToBase : guidance := TT3ReturnBaseGuidance.Create;
    end;

    if Assigned(guidance) then
      FGuidances.Add(guidanceType,guidance);
  end;

  result := guidance;
end;

procedure TT3Vehicle.DamageRepair(const dt: TDamageItemType);
var
  gt: byte;
begin
  with FDamageStatus do
  begin
    gt :=  getGTIndex(DamageCapacity);
  end;

  case dt of
     diOverall    : HealthPercent := 100;
     diMaxSpeed   : HealthPercent := 100;

     diSensor     : HealthPercent := 75;
     diWeapon     : HealthPercent := C_Limit_Sensor[gt] - 1;
     diCommm      : HealthPercent := C_Limit_weapon[gt] - 1;
     diHelm       : HealthPercent := C_Limit_comm[gt] - 1;
     diPropulsion : HealthPercent := 100;

  end;
end;

destructor TT3Vehicle.Destroy;
var
  guide : TPair<TVehicleGuidanceType,TT3VehicleGuidance>;
begin
//  FFormationStatus.Free;
//
//  FMountedSensors.Clear;
//  FMountedSensors.Free;
//
//  FMountedWeapons.Clear;
//  FMountedWeapons.Free;
//
//  FMountedECM.Clear;
//  FMountedECM.Free;

  for guide in FGuidances do
    guide.Value.Free;

  FGuidances.Clear;
  FGuidances.Free;

  inherited;
end;

function TT3Vehicle.getAltitude: single;
begin
  Result := ConvCartesian_To_Compass(FMover.Direction);
  //result := FMover.Z * C_Degree_To_NauticalMile * C_NauticalMile_To_Metre;
end;

function TT3Vehicle.getDeviceCount(devBase: String; devClass: TClass): integer;
var
  i : integer;
  dev : TObject;
begin

//  result := 0;
//  if devBase = 'ecm' then
//  begin
//    for I := 0 to FMountedECM.Count - 1 do begin
//      dev := FMountedECM.Items[i];
//      if dev.ClassType = DevClass then
//        result := result + 1;
//    end;
//  end
//  else
//  if devBase = 'sensor' then
//  begin
//    for I := 0 to FMountedSensors.Count - 1 do begin
//      dev := FMountedSensors.Items[i];
//      if dev.ClassType = DevClass then
//        result := result + 1;
//    end;
//  end
//  else
//  if devBase = 'weapon' then
//  begin
//    for I := 0 to FMountedWeapons.Count - 1 do begin
//      dev := FMountedWeapons.Items[i];
//      if dev.ClassType = DevClass then
//        result := result + 1;
//    end;
//  end;

end;

//function TT3Vehicle.getMountedECM(const id: integer; const className: TClass): TObject;
//var
//  ecm : TT3MountedECM;
//begin
//  result := nil;
//  for ecm in FMountedECM do
//    if (ecm.InstanceIndex = id) and (ecm is className) then
//    begin
//      result := ecm;
//      break;
//    end;
//end;
//
//function TT3Vehicle.getMountedECM(const name: String;
//  const className: TClass): TObject;
//var
//  ecm : TT3MountedECM;
//begin
//  result := nil;
//  for ecm in FMountedECM do
//    if (ecm.InstanceName = name) and (ecm is className) then
//    begin
//      result := ecm;
//      break;
//    end;
//end;
//
//function TT3Vehicle.getMountedSensor(const id: Integer;
//  const sensorType: TESensorType): TObject;
//var
//  sensor : TT3MountedSensor;
//begin
//  result := nil;
//  for sensor in FMountedSensors do
//    if (sensor.InstanceIndex = id) and (sensor.SensorType = sensorType) then
//    begin
//      result := sensor;
//      break;
//    end;
//end;
//
//function TT3Vehicle.getMountedWeapon(const id: integer;
//  const name: String): TObject;
//var
//  weapon : TT3MountedWeapon;
//begin
//  result := nil;
//  for weapon in FMountedWeapons do
//    if (weapon.InstanceIndex = id) and (weapon.InstanceName = name) then
//    begin
//      result := weapon;
//      break;
//    end;
//end;

function TT3Vehicle.getGroundSpeed: Double;
begin
  result := FMover.Speed;
end;

function TT3Vehicle.getHeading: Double;
begin
  if Assigned(FVehicleGuidance) then

  result := FVehicleGuidance.ResultHeading;
end;

function TT3Vehicle.getMaxRangeRadar: double;
var
  I: Integer;
//  dev : TT3MountedSensor;
  range : double;
begin
//  if FMountedSensors.Count = 0 then
//  begin
//    Result := 0;
//    Exit;
//  end;
//
//  range := 0;
//  for dev in FMountedSensors do
//  begin
//    if dev is TT3MountedRadar then
//    begin
//      if (range < TT3MountedRadar(dev).DetectionRange)
//          and (TT3MountedRadar(dev).OperationalStatus = sopOn) then
//        range := TT3MountedRadar(dev).DetectionRange;
//    end;
//  end;
//
//  Result := range;

end;

function TT3Vehicle.getMotionData(motionType: TMotionDataType): double;
var
  vdef : TDBVehicle_Definition;
begin
  result := 0;

  if Assigned(FDBPlatformMotion) then
  begin
    case motionType of
      mdMaxGSpeed         : result := 0.01 * FDamageStatus.PercentMaxSpeed *
                                FDBPlatformMotion.Max_Ground_Speed;
      mdMinGSpeed         : result := FDBPlatformMotion.Min_Ground_Speed;
      mdTightTurnRate     : result := FDBPlatformMotion.Tight_Turn_Rate;
      mdAccel             : result := FDBPlatformMotion.Acceleration;
      mdDecel             : result := FDBPlatformMotion.Deceleration;
      mdMaxHelmAngle      : result := FDBPlatformMotion.Max_Helm_Angle;
      mdStandardTurnRate  : result := FDBPlatformMotion.Standard_Turn_Rate;
      mdNormalClimbRate   : result := FDBPlatformMotion.Normal_Climb_Rate;
      mdVerticalAccel     : result := FDBPlatformMotion.Vertical_Accel;
      mdNormalDescentRate : result := FDBPlatformMotion.Normal_Descent_Rate;
    end;
  end;
end;

function TT3Vehicle.getOrderedAltitude: Double;
begin
  result := 0;
  if Assigned(FVehicleGuidance) then
    result := FVehicleGuidance.OrderedAltitude;

end;

function TT3Vehicle.getOrderedHeading: Double;
begin
  result := 0;
  if Assigned(FVehicleGuidance) then
    result := FVehicleGuidance.OrderedHeading;
end;

function TT3Vehicle.getOrderedSpeed: Double;
begin
  result := 0;
  if Assigned(FVehicleGuidance) then
    result := FVehicleGuidance.OrderedSpeed;
end;

function TT3Vehicle.getPlatformClass: string;
begin
//  result := VehicleDefinition.FData.Vehicle_Identifier
end;

function TT3Vehicle.getPos(const Index: Integer): Double;
begin
  result := 0;

  case Index of
    1:
      result := FCourse; // ConvCartesian_To_Compass(FMover.Direction);
    2:
      result := FVelocity;
  end;
end;

//function TT3Vehicle.getMountedSensor(const id: Integer;
//  const className: TClass): TObject;
//var
//  sensor : TT3MountedSensor;
//begin
//  result := nil;
//  for sensor in FMountedSensors do
//    if (sensor.InstanceIndex = id) and (sensor is className) then
//    begin
//      result := sensor;
//      break;
//    end;
//end;
//
//function TT3Vehicle.getMountedWeapon(const id: integer;
//  const className: TClass): TObject;
//var
//  weapon : TT3MountedWeapon;
//begin
//  result := nil;
//  for weapon in FMountedWeapons do
//    if (weapon.InstanceIndex = id) and (weapon is className) then
//    begin
//      result := weapon;
//      break;
//    end;
//end;
//
//function TT3Vehicle.getMountedWeapon(const name: String;
//  const className: TClass): TObject;
//var
//  weapon : TT3MountedWeapon;
//begin
//  result := nil;
//  for weapon in FMountedWeapons do
//    if (weapon.InstanceName = name) and (weapon is className) then
//    begin
//      result := weapon;
//      break;
//    end;
//end;
//
//function TT3Vehicle.getVehicleDefinition: TVehicle_Definition;
//begin
//  result := nil;
//  if Assigned(PlatformDefinition) then
//    result := TVehicle_Definition(PlatformDefinition);
//end;

procedure TT3Vehicle.guidanceInit;
begin
  if Assigned(FVehicleGuidance) then
  begin
    with FVehicleGuidance do begin
      MaxGroundSpeed  := getMotionData(mdMaxGSpeed);
      MinGroundSpeed  := getMotionData(mdMinGSpeed);
      Acceleration    := getMotionData(mdAccel);
      Deceleration    := getMotionData(mdDecel);

      Tight_Turn_Rate := getMotionData(mdTightTurnRate);
      Max_Helm_Angle  := getMotionData(mdMaxHelmAngle);
      Standard_Turn_Rate := getMotionData(mdStandardTurnRate);

      Normal_Climb_Rate := getMotionData(mdNormalClimbRate);
      Vertical_Accel    := getMotionData(mdVerticalAccel);
      Normal_Descent_Rate := getMotionData(mdNormalDescentRate);
    end;

  end;
end;

procedure TT3Vehicle.Initialize;
var
  guidance : TT3VehicleGuidance;
  act : TDBPlatform_Activation;
begin
  inherited;

  act := getScenario.GetDBPlatformActivation(InstanceIndex);
  if Assigned(act) then
  begin
    Altitude            := act.Init_Altitude;
    FMover.X            := act.Init_Position_Longitude;
    FMover.Y            := act.Init_Position_Latitude;
    FMover.Direction    := ConvCompass_To_Cartesian(act.Init_Course);
    FMover.Speed        := act.Init_Ground_Speed;
  end;

  FDBVehicleDefinition := getScenario.GetDBVehicleDefinition(FDBPlatformDefinitionIndex);
  if Assigned(FDBVehicleDefinition) then
  begin
    PlatformType        := 1; // always vehicle
    PlatformDomain      := FDBVehicleDefinition.Platform_Domain;
    PlatformCategory    := FDBVehicleDefinition.Platform_Category;

    FDBPlatformMotion := getScenario.GetDBPlatformMotion(1,FDBVehicleDefinition.Vehicle_Index);
    if Assigned(FDBPlatformMotion) then
    begin
      FMover.MinSpeed     := FDBPlatformMotion.Min_Ground_Speed;
      FMover.CruiseSpeed  := FDBPlatformMotion.Cruise_Ground_Speed;
      FMover.MaxSpeed     := FDBPlatformMotion.Max_Ground_Speed;
      FMover.MaxAltitude  := FDBPlatformMotion.Max_Altitude;
      FMover.MaxDepth     := FDBPlatformMotion.Max_Depth;
    end;
  end;

  FMover.Enabled := true;

  FPosition.X := FMover.X;
  FPosition.Y := FMover.Y;
  FPosition.Z := Altitude;

  { default guidance if activation not exist }
  if not Assigned(act) then
  begin
    guidance := createGuidance(vgtStraightLine);
    if Assigned(guidance) then
    begin
      SetVehicleGuidance(guidance);

      guidance.OrderedHeading := 0;
      guidance.OrderedSpeed   := 0;
    end;
  end;

  { guidance activation }
  if Assigned(act) then
  case act.Init_Guidance_Type of
    0:  // straight/heading
      begin
        guidance := createGuidance(vgtStraightLine);
        if Assigned(guidance) then
        begin
          SetVehicleGuidance(guidance);

          guidance.OrderedHeading := act.Init_Course;
          guidance.OrderedSpeed   := act.Init_Ground_Speed;

        end;
      end;
    5:  // helm guidance
      begin
        guidance := createGuidance(vgtHelm);
        if Assigned(guidance) then
        begin
          SetVehicleGuidance(guidance);

          guidance.OrderedHeading := act.Init_Helm_Angle;
          guidance.OrderedSpeed   := act.Init_Ground_Speed;

        end;
      end;
    9:  // circle on track
      begin

        guidance := createGuidance(vgtCircle);
        if Assigned(guidance) then
        begin
          SetVehicleGuidance(guidance);

          with TT3CircleGuidance(guidance) do
          begin

            OrderedSpeed    := act.Init_Ground_Speed;
            CircleMode      := cModeTrack;
            CircleState     := cstateStraight;
            TargetInstanceIndex := act.Guidance_Target;
            CircleRadius    := act.Radius_of_Travel;
            ShadowDistance  := act.Dynamic_Circle_Range_Offset;
            CircleBearing   := act.Dynamic_Circle_Angle_Offset;

            case act.Direction_of_Travel of
              0:  CircleDirection := 90;
              1:  CircleDirection := -90;
            end;

            case act.Dynamic_Circle_Offset_Mode of
              0: CircleBearingState := cBearingTrue;
              1: CircleBearingState := cBearingRelative;
            end;

          end;
        end;

      end;
    10:  // circle on point
      begin
        guidance := createGuidance(vgtCircle);
        if Assigned(guidance) then
        begin

          SetVehicleGuidance(guidance);

          with TT3CircleGuidance(guidance) do
          begin

            OrderedSpeed        := act.Init_Ground_Speed;
            CircleMode          := cModePosition;
            CircleState         := cstateStraight;
            CenterCirclePointX  := act.Circle_Longitude;
            CenterCirclePointY  := act.Circle_Latitude;
            CircleRadius        := act.Radius_of_Travel;

            case act.Direction_of_Travel of
              0:  CircleDirection := 90;
              1:  CircleDirection := -90;
            end;

          end;
        end;

      end;
    11: // zig-zag
      begin

        guidance := createGuidance(vgtZigzag);
        if Assigned(guidance) then
        begin

          SetVehicleGuidance(guidance);

          with TT3ZigZagGuidance(guidance) do
          begin

            OrderedZigzag := act.Init_Course;
            OrderedSpeed  := act.Init_Ground_Speed;

            case act.Zig_Zag_Leg_Type of
              1: ZigZagLegType := zzShort;
              2: ZigZagLegType := zzLong;
              3: ZigZagLegType := zzVeryLong;
            end;

            if act.Period_Distance <> 0 then
              PeriodZigzag := act.Period_Distance;
            if act.Amplitude_Distance <> 0 then
              AmplitudoZigzag := act.Amplitude_Distance;

          end;
        end;
      end;
  end;
end;

procedure TT3Vehicle.Move(const aDeltaMs: Double);
var
  nX, nY, nZ : double;
  dx, dy : double;
begin
  inherited;

  { now guidance handle orderedspeed, ordered altitude, ordered heading }

  if not Assigned(FVehicleGuidance) then
  begin

  end else
  begin

    FVehicleGuidance.CalcGuidance(aDeltaMs);
    calcEnvironmentEffect(dx,dy,aDeltaMs);

    nx := FMover.X + dx;
    ny := FMover.Y + dy;

    FMover.X := nx;
    FMover.Y := ny;

    FPosition.x := nx;
    FPosition.y := ny;
    FPosition.Z := FMover.Z;

  end;

end;

procedure TT3Vehicle.OnWaypointReached(behaviour, events: TObject);
begin
  if Assigned(behaviour) then
  begin
//    SetOrderedSpeed(TScripted_Behav_Definition(behaviour).FData.Speed);
//    SetOrderedAltitude(TScripted_Behav_Definition(behaviour).FData.Altitude);
  end;
end;

procedure TT3Vehicle.SetAltitude(const Value: single);
begin
  inherited;
  FMover.Z := Value / (C_Degree_To_NauticalMile * C_NauticalMile_To_Metre);
end;

procedure TT3Vehicle.SetEMCON_HFDataLinkVehicle(
  const Value: TEMCON_HFDataLinkVehicle);
begin
  //FEMCON_HFDataLinkVehicle := Value;
end;

procedure TT3Vehicle.SetEMCON_UHFVHFDataLinkVehicle(
  const Value: TEMCON_UHFVHFDataLinkVehicle);
begin
  //FEMCON_UHFVHFDataLinkVehicle := Value;
end;


procedure TT3Vehicle.setGrounded(const Value: boolean);
begin
  FIsGrounded := Value;
end;

procedure TT3Vehicle.setGroundSpeed(const Value: Double);
begin
  inherited;
  FMover.Speed := Value;
end;

procedure TT3Vehicle.setHeading(const Value: double);
begin
  inherited;
  FMover.Direction := ConvCompass_To_Cartesian(value);
end;

procedure TT3Vehicle.SetLastEMCON_HFDLink(
  const Value: TEMCON_HFDataLinkVehicle);
begin
  //FLastEMCON_HFDLink := Value;
end;

procedure TT3Vehicle.SetLastEMCON_UHFVHFDLink(
  const Value: TEMCON_UHFVHFDataLinkVehicle);
begin
  //FLastEMCON_UHFVHFDLink := Value;
end;

procedure TT3Vehicle.setOnLand(const Value: boolean);
begin
  FIsOnLand := Value;
end;

procedure TT3Vehicle.SetOrderedAltitude(const Value: Double);
begin
  if Assigned(FVehicleGuidance) then
    FVehicleGuidance.OrderedAltitude := Value;
end;

procedure TT3Vehicle.SetOrderedHeading(const Value: Double);
begin
  if Assigned(FVehicleGuidance) then
    FVehicleGuidance.OrderedHeading := Value;
end;

procedure TT3Vehicle.SetOrderedSpeed(const Value: Double);
begin
  if Assigned(FVehicleGuidance) then
    FVehicleGuidance.OrderedSpeed := Value;
end;

procedure TT3Vehicle.setPos(const Index: Integer; const Value: Double);
begin
  inherited;

  case Index of
    1:
      FMover.Direction := ConvCompass_To_Cartesian(Value);
    2:
      FMover.Speed := Value;
  end;

end;

procedure TT3Vehicle.SetRecentlyUsedSensor(const Value: integer);
begin
  FRecentlyUsedSensor := Value;
end;

procedure TT3Vehicle.SetRecentlyUsedWeapon(const Value: integer);
begin
  FRecentlyUsedWeapon := Value;
end;

procedure TT3Vehicle.SetVehicleGuidance(const Value: TT3VehicleGuidance);
var
  oldGuide : TT3VehicleGuidance;
begin
  oldGuide := FVehicleGuidance;
  FVehicleGuidance := Value;

  if Assigned(oldGuide) then
  begin
    if Assigned(FVehicleGuidance) then
    begin
      { update guidance motion data }
      guidanceInit;

      FVehicleGuidance.Mover := FMover;
      FVehicleGuidance.GuidanceInit;
      //FVehicleGuidance.OrderedHeading := oldGuide.OrderedHeading;
      FVehicleGuidance.OrderedSpeed     := oldGuide.OrderedSpeed;
      FVehicleGuidance.OrderedAltitude  := oldGuide.OrderedAltitude;
    end;

  end;

end;

//function TT3Vehicle.getMountedWeapon(const name: String): TObject;
//var
//  weapon : TT3MountedWeapon;
//begin
//  result := nil;
//  for weapon in FMountedWeapons do
//    if (weapon.InstanceName = name) then
//    begin
//      result := weapon;
//      break;
//    end;
//end;

end.
