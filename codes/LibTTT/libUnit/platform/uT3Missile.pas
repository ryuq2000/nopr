unit uT3Missile;

interface

uses uT3PlatformInstance, uDBClassDefinition;

type
  TT3Missile = class (TT3PlatformInstance)
  public
    constructor Create;override;
    procedure Initialize; override;
  end;

implementation

uses uGlobalVar, uBaseCoordSystem;

{ TT3Missile }

constructor TT3Missile.Create;
begin
  inherited;

end;

procedure TT3Missile.Initialize;
var
  act : TDBPlatform_Activation;
begin
  inherited;

  act := getScenario.GetDBPlatformActivation(InstanceIndex);
  if Assigned(act) then
  begin
    Altitude            := act.Init_Altitude;
    FMover.X            := act.Init_Position_Longitude;
    FMover.Y            := act.Init_Position_Latitude;
    FMover.Direction    := ConvCompass_To_Cartesian(act.Init_Course);
    FMover.Speed        := act.Init_Ground_Speed;
  end;

end;

end.
