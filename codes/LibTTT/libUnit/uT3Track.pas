unit uT3Track;

interface

uses Generics.Collections, uSimObjects, Classes, uT3MountedSensor,
  uSensorInfo, SysUtils, tttData, uSteppers;

{ Track class handle track model }
type
  TTrackType = (trBearing, trAOP, trPoint, trSpecialPoint);
  TTrackCategory = (tcRealTrack, tcNonRealTrack);
  TTrackIdentity = (tiPending, tiUnknown, tiAssumedFriend, tiFriend,
                    tiNeutral,tiSuspect,tiHostile);

  TT3Track = class abstract (TSimObject)
  private

    { keep index sensor only }
    FSensors             : TObjectDictionary<TT3MountedSensor,TSensorTrackInfo>;
    FFirstTimeDetected   : Boolean;

    FLastDetected        : TDateTime;
    FFirstDetected       : TDateTime;
    FTrackNumber         : integer;
    FObjectInstanceIndex : integer;
    FTrackType           : TTrackType;
    FTrackCategory       : TTrackCategory;
    FTrackDesignation    : integer;
    FTrackIdent          : integer;
    FTrackDomain         : integer;
    FTrackTypeDomain     : integer;
    FDetectedSpeed       : double;
    FDetectedAltitude    : double;
    FDetectedCourse      : double;
    FisSelected          : Boolean;
    FOnSelected          : TnotifyEvent;
    FTrackLabel          : string;
    FTrackID             : Integer;
    FDetailedDetection   : TRadarDetailedCapabilities;
    FTextSign            : string;

    procedure SetFirstDetected(const Value: TDateTime);
    procedure SetLastDetected(const Value: TDateTime);
    procedure SetTrackNumber(const Value: integer);
    procedure SetObjectInstanceIndex(const Value: integer);
    procedure SetTrackType(const Value: TTrackType);
    procedure SetTrackCategory(const Value: TTrackCategory);
    procedure SetTrackDesignation(const Value: integer);
    procedure SetTrackDomain(const Value: integer);
    procedure SetTrackIdent(const Value: integer);
    procedure SetTrackTypeDomain(const Value: integer);
    procedure SetDetectedAltitude(const Value: double);
    procedure SetDetectedCourse(const Value: double);
    procedure SetDetectedSpeed(const Value: double);
    procedure SetisSelected(const Value: Boolean);
    procedure SetOnSelected(const Value: TnotifyEvent);
    procedure SetTrackLabel(const Value: string);
    function getlabelInfo: string;
    procedure setIsLostContact(const Value: Boolean);
    function getDetectedSpeed: double;

    procedure updateDetailedDetectionShowed;
    procedure SetTextSign(const Value: string);
    function getTextSign: string;

  protected
    { Lost contact flag }
    FIsLostContact       : Boolean;
    FAlreadyCountDown    : Boolean;
    FCOunter             : double;

  public
    constructor Create;virtual;
    destructor Destroy;override;

    { add remove sensor }
    function addDetectBy(device: TT3MountedSensor; dateDetected : TDateTime;
        out sensorInfo : TSensorTrackInfo): Boolean;virtual;
    function removeDetectBy(device: TT3MountedSensor): boolean;

    procedure Move(const aDeltaMs: double); override;

    { track detected time }
    property FirstDetected : TDateTime read FFirstDetected write SetFirstDetected;
    property LastDetected  : TDateTime read FLastDetected write SetLastDetected;

    { track label -> base on vehicle labeled track }
    property TrackLabel     : string  read FTrackLabel write SetTrackLabel;
    { track number -> base on track block }
    property TrackNumber    : integer read FTrackNumber write SetTrackNumber;
    { track : point, aop, bearing }
    property TrackType      : TTrackType read FTrackType write SetTrackType;
    { track : real track, non real track }
    property TrackCategory  : TTrackCategory read FTrackCategory write SetTrackCategory;

    { track :  vhtRotaryRec, vhtFixedRec, etc }
    property TrackTypeDomain : integer read FTrackTypeDomain write SetTrackTypeDomain;
    { track : force designation }
    property TrackDesignation : integer read FTrackDesignation write SetTrackDesignation;
    { track :
      0 piPending          : clWhite ;
      1 piUnknown          : clWhite ;
      2 piAssumedFriend    : clBlue  ;
      3 piFriend           : clBlue  ;
      4 piNeutral          : clGreen ;
      5 piSuspect          : clRed   ;
      6 piHostile          : clRed   ;
    }
    property TrackIdentifier: integer read FTrackIdent write SetTrackIdent;
    { track : air, surface, subsurface, dll  }
    property TrackDomain    : integer read FTrackDomain write SetTrackDomain;

    { index object to track, empty if nonreal track }
    property ObjectInstanceIndex : integer read FObjectInstanceIndex write SetObjectInstanceIndex;
    { track label info : controler -> tracklabel, cubicle -> tracknumber}
    property TrackLabelInfo : string read getlabelInfo;

    { property can be set from sensor, or directly from platform object if isAlwaysAvailable=true }
    property DetectedSpeed  : double read getDetectedSpeed write SetDetectedSpeed;
    property DetectedCourse : double read FDetectedCourse write SetDetectedCourse;
    property DetectedAltitude : double read FDetectedAltitude write SetDetectedAltitude;

    { property selecting track}
    property isSelected : Boolean read FisSelected write SetisSelected;
    { read only for lost contact flag }
    property isLostContact : Boolean read FIsLostContact write setIsLostContact;

    property DetectedBySensors : TObjectDictionary<TT3MountedSensor,TSensorTrackInfo> read FSensors;

    { event triger }
    property OnSelected   : TnotifyEvent read FOnSelected write SetOnSelected;

    { property showing detail of track }
    property DetailedDetectionShowed: TRadarDetailedCapabilities read FDetailedDetection write FDetailedDetection;

    {
      visual text on display

      +LA 12345
      ||| 37  |
      |||  |  |--- Track number : 5 number
      |||  |------ PU Number    : indicates the datalink participating units which hold (owns) the detected track.
      |||--------- Threat ranking : indicates the ranking of non-friendly air tracks (position tracks only) from A
      ||                            (highest threat) to H (lowest threat).
      ||---------- Link Status: used to indicate how the track is being detected
      |                           L (Local): detected by local sensors on command platforms
      |                           R (Remote): detected by sensors of other link PUs (not by local)
      |                           Blank: detected by either both local and remote sensors or detected by local sensors and
      |                                  the user has selected the track for link transmission.
      |----------- Track Mode: indicates if the track is Real Time (+), that is, detected by sensors, or is Non-Real
                                Time (-), which is held only by the tracking system.
    }
    property TrackTextSign : string read getTextSign;
  end;

implementation

uses
  uGlobalVar, uBaseCoordSystem, {uT3MountedVisual,} uT3MountedRadar;

{ TT3Track }

function TT3Track.addDetectBy(device: TT3MountedSensor; dateDetected : TDateTime;
  out sensorInfo : TSensorTrackInfo): Boolean;
var
  iposX,iposY,iposZ, course, speed, alt : double;
  builtInfo : Boolean;
  updateinfo : Boolean;
begin
  if FFirstTimeDetected then
  begin
    FirstDetected := dateDetected;
    FFirstTimeDetected := false;
  end;
  sensorInfo := nil;
  updateinfo := False;

  result := FSensors.ContainsKey(device);
  if dateDetected > FLastDetected then
    updateinfo := True;

  FLastDetected := dateDetected;
  builtInfo := True;

  queryPlatformInfo(ObjectInstanceIndex,iposX,iposY,iposZ,course,speed,alt);

  {update sensor info }
  if result then
  begin
    if FSensors.TryGetValue(device,sensorInfo) then
    begin
      sensorInfo.updateData(dateDetected,iposX,iposY,iposZ,course,speed,alt);
      builtInfo := False;
    end
  end;

  if builtInfo then
  begin
    sensorInfo := SensorTrackInfoFactory(device.SensorType);
    sensorInfo.setFirstDetected(dateDetected,iposX,iposY,iposZ,course,speed,alt);
    FSensors.Add(device,sensorInfo);

    { update data detail }
    updateDetailedDetectionShowed;
  end;

  if FSensors.Count > 0 then
    FIsLostContact := False;

  { update track info }
  if updateinfo then
  begin
    PosX := iposX;
    PosY := iposY;
    PosZ := iposZ;

    if sensorInfo is TRadarTrackInfo then
    begin
      DetectedSpeed    := TRadarTrackInfo(sensorInfo).DetectedSpeed;
      DetectedCourse   := TRadarTrackInfo(sensorInfo).DetectedCourse;
      DetectedAltitude := TRadarTrackInfo(sensorInfo).DetectedAltitude;
    end
    else
    begin
      DetectedSpeed    := speed;
      DetectedCourse   := course;
      DetectedAltitude := alt;
    end;
  end;
end;

constructor TT3Track.Create;
begin
  FSensors           := TObjectDictionary<TT3MountedSensor,TSensorTrackinfo>.create([doOwnsValues]);
  FFirstTimeDetected := True;
  FTrackCategory     := tcRealTrack;

  FisSelected        := False;
  FIsLostContact     := False;

  FDetectedSpeed     := 0.0;
  FDetectedAltitude  := 0.0;
  FDetectedCourse    := 0.0;

  FTextSign          := '';

  FAlreadyCountDown  := False;
  FCounter           := 0;

end;

destructor TT3Track.Destroy;
begin
  FSensors.Free;

  inherited;
end;

function TT3Track.getDetectedSpeed: double;
begin
  result := FDetectedSpeed;
end;

function TT3Track.getlabelInfo: string;
begin
  if machineRole = crController then
    result := TrackLabel
  else
    result := FormatTrackNumber(TrackNumber);

end;

function TT3Track.getTextSign: string;
var
  sign : string;
begin

  case TrackCategory of
    tcRealTrack   : sign := '+';
    tcNonRealTrack: sign := '-';
  end;

  { link status : L,R,(blank)}
  if DetectedBySensors.Count > 0 then
    sign := sign + 'L'
  else
    sign := sign + ' ';

  {threat ranking }
  sign := sign + ' ';

  if machineRole in [crController,crServer] then
    sign := sign + ' ' + FormatTrackNumber(TrackNumber)
  else
    sign := sign + ' ' + FormatTrackNumber(TrackNumber);

  result := sign;

end;

procedure TT3Track.Move(const aDeltaMs: double);
begin
  inherited;

end;

function TT3Track.removeDetectBy(device: TT3MountedSensor): boolean;
var
  info : TSensorTrackInfo;
begin
  if FSensors.TryGetValue(device,info) then
  begin
    updateDetailedDetectionShowed;
  end;
  FSensors.Remove(device);

  if FTrackCategory = tcRealTrack then
  begin
//    if FSensors.Count <= 0 then
//      FIsLostContact := True;

    { startin to countdown to lost contact }
  end;
end;


procedure TT3Track.SetDetectedAltitude(const Value: double);
begin
  FDetectedAltitude := Value;
end;

procedure TT3Track.SetDetectedCourse(const Value: double);
begin
  FDetectedCourse := Value;
end;

procedure TT3Track.SetDetectedSpeed(const Value: double);
begin
  FDetectedSpeed := Value;
end;

procedure TT3Track.SetFirstDetected(const Value: TDateTime);
begin
  FFirstDetected := Value;
end;

procedure TT3Track.setIsLostContact(const Value: Boolean);
begin
  FIsLostContact := Value;

  // clear all sensor if lost contact
  if FIsLostContact then
  begin
    FSensors.Clear;
  end;
end;

procedure TT3Track.SetisSelected(const Value: Boolean);
begin
  FisSelected := Value;

  if Assigned(FOnSelected) then
    FOnSelected(Self);

end;

procedure TT3Track.SetLastDetected(const Value: TDateTime);
begin
  FLastDetected := Value;
end;

procedure TT3Track.SetObjectInstanceIndex(const Value: integer);
begin
  FObjectInstanceIndex := Value;
end;

procedure TT3Track.SetOnSelected(const Value: TnotifyEvent);
begin
  FOnSelected := Value;
end;

procedure TT3Track.SetTextSign(const Value: string);
begin
  FTextSign := Value;
end;

procedure TT3Track.SetTrackCategory(const Value: TTrackCategory);
begin
  FTrackCategory := Value;
end;

procedure TT3Track.SetTrackDesignation(const Value: integer);
begin
  FTrackDesignation := Value;
end;

procedure TT3Track.SetTrackDomain(const Value: integer);
begin
  FTrackDomain := Value;
end;

procedure TT3Track.SetTrackIdent(const Value: integer);
begin
  FTrackIdent := Value;
end;

procedure TT3Track.SetTrackLabel(const Value: string);
begin
  FTrackLabel := Value;
end;

procedure TT3Track.SetTrackNumber(const Value: integer);
begin
  FTrackNumber := Value;
end;

procedure TT3Track.SetTrackType(const Value: TTrackType);
begin
  FTrackType := Value;
end;

procedure TT3Track.SetTrackTypeDomain(const Value: integer);
begin
  FTrackTypeDomain := Value;
end;

procedure TT3Track.updateDetailedDetectionShowed;
var
  sensorP : TPair<TT3MountedSensor,TSensorTrackInfo>;
  foundVisual : Boolean;
begin
  foundVisual := False;

  { reset }
  with FDetailedDetection do
  begin
    IFF_Capability               := false;
    Altitude_Data_Capability     := false;
    Ground_Speed_Data_Capability := false;
    Heading_Data_Capability      := false;
    Plat_Type_Recog_Capability   := false;
    Plat_Class_Recog_Capability  := false;
    Plat_Name_Recog_Capability   := false;
    Track_ID                     := false;
  end;

  { find specific sensor }
//  for sensorP in FSensors do
//    if sensorP.Key is TT3MountedVisual then
//    begin
//      foundVisual := True;
//      break;
//    end
//    else if sensorP.Key is TT3MountedRadar then
//    begin
//      with FDetailedDetection do
//        with TT3MountedRadar(sensorP.Key).RadarDefinition do
//        begin
//          IFF_Capability               := FDef.IFF_Capability or IFF_Capability;
//          Altitude_Data_Capability     := FDef.Altitude_Data_Capability
//                                          or Altitude_Data_Capability;
//          Ground_Speed_Data_Capability := FDef.Ground_Speed_Data_Capability
//                                          or Ground_Speed_Data_Capability;
//          Heading_Data_Capability      := FDef.Heading_Data_Capability
//                                          or Heading_Data_Capability;
//          Plat_Type_Recog_Capability   := FDef.Plat_Type_Recog_Capability
//                                          or Plat_Type_Recog_Capability;
//          Plat_Class_Recog_Capability  := FDef.Plat_Class_Recog_Capability
//                                          or Plat_Class_Recog_Capability;
//          Plat_Name_Recog_Capability   := True;
//          Track_ID                     := True;
//        end;
//    end;

  { if visual, set all cap true }
  if foundVisual then
    with FDetailedDetection do
    begin
      IFF_Capability               := true;
      Altitude_Data_Capability     := true;
      Ground_Speed_Data_Capability := true;
      Heading_Data_Capability      := true;
      Plat_Type_Recog_Capability   := true;
      Plat_Class_Recog_Capability  := true;
      Plat_Name_Recog_Capability   := true;
      Track_ID                     := True;
    end;

end;

end.
