unit uT3PointTrack;

interface

uses Generics.Collections, uSimObjects, Classes, uT3MountedSensor, uT3Track,
  u2DMover;

{ Track class handle track model }
type

  TT3PointTrack = class (TT3Track)
  private
    FCanControl        : Boolean;
    FisControlled      : Boolean;
    FOnControlled      : TNotifyEvent;
    FisAlwaysAvailable : boolean;

    FMover             : T2DMover;
    FShowTrails: Boolean;

    procedure SetCanControl(const Value: Boolean);
    procedure SetisControlled(const Value: Boolean);
    procedure SetOnControlled(const Value: TNotifyEvent);
    procedure SetisAlwaysAvailable(const Value: boolean);
    procedure SetShowTrails(const Value: Boolean);
  public
    constructor Create;override;
    destructor Destroy; override;
    procedure Move(const aDeltaMs: double); override;

    { property controlling track, only if track is showing own platform }
    property isControlled : Boolean read FisControlled write SetisControlled;
    {
      set read only controlled track, ( able control or no )
      it means that track is group platform
    }
    property CanControl : Boolean read FCanControl write SetCanControl;

    { event triger }
    property OnControlled : TNotifyEvent read FOnControlled write SetOnControlled;

    { set to true if always tracking a platform (usually as controller or cubicle owner),
      set false if detect by device }
    property isAlwaysTracking : boolean read FisAlwaysAvailable write SetisAlwaysAvailable;

    { trails on track }
    property ShowTrails : Boolean read FShowTrails write SetShowTrails;
  end;

implementation

uses
  uGlobalVar, uT3PlatformInstance,
  tttData;

{ TT3PointTrack }

constructor TT3PointTrack.Create;
begin
  inherited;
  FMover    := T2DMover.Create;

  FCanControl := False;
  FisControlled := False;

  FShowTrails := False;

end;

destructor TT3PointTrack.Destroy;
begin
  FMover.Free;
  inherited;
end;

procedure TT3PointTrack.Move(const aDeltaMs: double);
var
  pi : TT3PlatformInstance;
begin
  inherited;

  if (TrackCategory = tcRealTrack) and isAlwaysTracking then
  begin
    { always update track position to platform position, ussually controller role }
    pi := simManager.FindT3PlatformByID(ObjectInstanceIndex);

    if Assigned(pi) then
    begin
      setPositionX(Pi.getPositionX);
      setPositionY(Pi.getPositionY);
      setPositionZ(Pi.getPositionZ);

      DetectedSpeed := Pi.Speed;
      DetectedCourse := Pi.Heading;
      DetectedAltitude := Pi.ALtitude;
    end;
  end;

  { calculate lost contact }
  if (DetectedBySensors.Count <= 0) and (TrackCategory = tcRealTrack)
    and (not isAlwaysTracking) then
  begin
    if not FAlreadyCountDown then
      FAlreadyCountDown := True;

    if FAlreadyCountDown then
    begin
      FCOunter := FCOunter + aDeltaMs;
      case TrackDomain of
        // air
        0 :
          if FCOunter >= queryGameDefault.Air_Lost_Time then
            FIsLostContact := True;
        // surface land
        1,3 :
          if FCOunter >= queryGameDefault.Sur_Lost_Time then
            FIsLostContact := True;
        // subsurface
        2 :
          if FCOunter >= queryGameDefault.Sub_Lost_Time then
            FIsLostContact := True;
      end;
    end;

  end
  else
  begin
    FIsLostContact := False;
    FAlreadyCountDown := False;
    FCounter := 0;
  end;
end;

procedure TT3PointTrack.SetCanControl(const Value: Boolean);
begin
  FCanControl := Value;
end;

procedure TT3PointTrack.SetisAlwaysAvailable(const Value: boolean);
begin
  FisAlwaysAvailable := Value;
end;

procedure TT3PointTrack.SetisControlled(const Value: Boolean);
begin
  FisControlled := Value;

  if not CanControl then
    FisControlled := False
  else
  begin
    if Assigned(FOnControlled) then
      FOnControlled(Self);
  end;
end;

procedure TT3PointTrack.SetOnControlled(const Value: TNotifyEvent);
begin
  FOnControlled := Value;
end;

procedure TT3PointTrack.SetShowTrails(const Value: Boolean);
begin
  FShowTrails := Value;
end;

end.
