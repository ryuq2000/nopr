unit uT3BlindZone;

interface

uses uSimObjects;

type
  TT3BlindZone = class ( TSimObject )
  private
    FBlind_Zone_Type: byte;
    FBlindZone_Number: byte;
    FStart_Angle: single;
    FEnd_Angle: single;
    FHeading: Single;
    FVisible: Boolean;
    FRange: Single;

    procedure SetBlind_Zone_Type(const Value: byte);
    procedure SetBlindZone_Number(const Value: byte);
    procedure SetEnd_Angle(const Value: single);
    procedure SetStart_Angle(const Value: single);
    procedure SetHeading(const Value: Single);
    procedure SetVisible(const Value: Boolean);
    procedure SetRange(const Value: Single);
  published

  public
    property Blind_Zone_Type        : byte read FBlind_Zone_Type write SetBlind_Zone_Type;
    property BlindZone_Number       : byte read FBlindZone_Number write SetBlindZone_Number;
    property Start_Angle            : single read FStart_Angle write SetStart_Angle;
    property End_Angle              : single read FEnd_Angle write SetEnd_Angle;
    property Heading                : Single  read FHeading write SetHeading;
    property Range                  : Single  read FRange write SetRange;
    property Visible                : Boolean read FVisible write SetVisible;
  end;

implementation

{ TT3BlindZone }

procedure TT3BlindZone.SetBlindZone_Number(const Value: byte);
begin
  FBlindZone_Number := Value;
end;

procedure TT3BlindZone.SetBlind_Zone_Type(const Value: byte);
begin
  FBlind_Zone_Type := Value;
end;

procedure TT3BlindZone.SetEnd_Angle(const Value: single);
begin
  FEnd_Angle := Value;
end;

procedure TT3BlindZone.SetHeading(const Value: Single);
begin
  FHeading := Value;
end;

procedure TT3BlindZone.SetRange(const Value: Single);
begin
  FRange := Value;
end;

procedure TT3BlindZone.SetStart_Angle(const Value: single);
begin
  FStart_Angle := Value;
end;

procedure TT3BlindZone.SetVisible(const Value: Boolean);
begin
  FVisible := Value;
end;

end.
