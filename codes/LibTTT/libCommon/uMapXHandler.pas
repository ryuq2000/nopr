unit uMapXHandler;

interface

uses
  MapXLib_TLB, Windows, Classes, Graphics, Controls,
  tttData, Sysutils, uTMapTouch;

type
  TScreenPt = record
    X : Single;
    Y : Single;
  end;

//==============================================================================
  TNotifyDraw     = procedure (aCanvas: TCanvas) of object;
  TMapMouseMove   = procedure (sender: TObject; Shift: TShiftState; X, Y: Integer) of object;
  TMapMouseDown   = procedure (sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer) of object;
  TMapMouseUp     = procedure (sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer) of object;
  TMapMouseDouble = procedure (Sender: TObject) of object;
  TMapMouseSingle = procedure (Sender: TObject) of object;
  TMapMouseExit   = procedure (Sender: TObject) of object;

//==============================================================================

  TSimMap = class
  private

    FLyrDraw    : CMapXLayer;
    FCanvas     : TCanvas;

    FRectZoom   : TRect;
    FMouseIsDown: boolean;

    FOnDrawToCanvas: TNotifyDraw;

    FOnMapChange: TNotifyEvent;
    FOnToolUsed     : TEventToolMapUsed;
    FOnMapMouseMove : TMapMouseMove;
    FOnMapMouseDown : TMapMouseDown;
    FOnMapMouseUp   : TMapMouseUp;
    FOnLogTemporary : TGetStrProc;
    FOnAddWayoint: TEventAddWaypoint;
    FOnEditWayoint: TEventEditWaypoint;
    FOnMapMouseDouble: TMapMouseDouble;
    FOnMapMouseSingle: TMapMouseSingle;
    FOnMapMouseExit: TMapMouseExit;

    procedure CreateMapTool;
    procedure SetMapComponent;
    procedure SetOnMapMouseMove(const Value: TMapMouseMove);
    procedure SetOnMapMouseDown(const Value: TMapMouseDown);
    procedure SetOnMapMouseUp(const Value: TMapMouseUp);
    procedure SetOnLogTemporary(const Value: TGetStrProc);
    procedure SetOnToolUsed(const Value: TEventToolMapUsed);
    procedure SetOnAddWayoint(const Value: TEventAddWaypoint);
    procedure SetOnEditWayoint(const Value: TEventEditWaypoint);
    procedure SetOnMapMouseDouble(const Value: TMapMouseDouble);
    procedure SetOnMapMouseExit(const Value: TMapMouseExit);
    procedure SetOnMapMouseSingle(const Value: TMapMouseSingle);
  public
    FMap        : TMapXTouch;

    constructor Create(aMap: TMapXTouch);
    destructor Destroy; override;

    procedure LoadMap(aGeoset: String);
    procedure applySetting(const MapZoom: integer;
      const mX, mY: double; const Colorbg : TColor);

    procedure DrawMap;

    procedure SetMapCenter(const x, y: double);
    procedure SetMapZoom(const z: double);

    procedure MapDrawUserLayer(ASender: TObject; const Layer: IDispatch;
      hOutputDC, hAttributeDC: Cardinal;
      const RectFull, RectInvalid: IDispatch);

    procedure MapToolUsed(ASender: TObject; ToolNum: Smallint;
      X1, Y1, X2, Y2, Distance: double; Shift, Ctrl: WordBool;
      var EnableDefault: WordBool);

    procedure MapMouseDown(sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);

    procedure MapMouseMove(sender: TObject; Shift: TShiftState; X, Y: Integer);

    procedure MapMouseUp(sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);

    procedure MapMouseDouble(sender: TObject);
    procedure MapMouseSingle(sender: TObject);
    procedure MapMouseExit(sender: TObject);

    procedure MapViewChanged(Sender: TObject);

  public

    property OnDrawToCanvas: TNotifyDraw
      read FOnDrawToCanvas write FOnDrawToCanvas;

    property OnMapChange: TNotifyEvent
      read FOnMapChange write FOnMapChange;

    property OnToolUsed : TEventToolMapUsed read FOnToolUsed write SetOnToolUsed;
    property OnAddWayoint : TEventAddWaypoint read FOnAddWayoint write SetOnAddWayoint;
    property OnEditWayoint : TEventEditWaypoint read FOnEditWayoint write SetOnEditWayoint;
    property OnMapMouseMove : TMapMouseMove read FOnMapMouseMove write SetOnMapMouseMove;
    property OnMapMouseDown : TMapMouseDown  read FOnMapMouseDown write SetOnMapMouseDown;
    property OnMapMouseUp   : TMapMouseUp  read FOnMapMouseUp write SetOnMapMouseUp;
    property OnMapMouseDouble : TMapMouseDouble read FOnMapMouseDouble write SetOnMapMouseDouble;
    property OnMapMouseSingle: TMapMouseSingle read FOnMapMouseSingle write SetOnMapMouseSingle;
    property OnMapMouseExit: TMapMouseExit read FOnMapMouseExit write SetOnMapMouseExit;
    property OnLogTemporary: TGetStrProc read FOnLogTemporary write SetOnLogTemporary;
  end;

implementation

uses
  Dialogs;

procedure InitOleVariant(var TheVar: OleVariant);
begin
  TVarData(TheVar).vType := varError;
  TVarData(TheVar).vError := DISP_E_PARAMNOTFOUND;
end;

{ TSimMapXHandler }

procedure TSimMap.applySetting(const MapZoom: integer; const mX, mY: double;
  const Colorbg: TColor);
begin
  FMap.ZoomTo(MapZoom, mX, mY);
  FMap.BackColor := Colorbg;

end;

constructor TSimMap.Create(aMap: TMapXTouch);
begin

  FCanvas   := TCanvas.Create;
  FMap      := aMap;
  FMap.DoubleBuffered := True;

  CreateMapTool;

  SetMapComponent;
end;

destructor TSimMap.Destroy;
begin
  inherited;

  FMap.Layers.RemoveAll;
  FMap := nil;
  FCanvas.Free;
end;

procedure TSimMap.CreateMapTool;
begin
  FMap.CreateCustomTool(mtAddWaypoint, 0, miCrossCursor, miCrossCursor);
  FMap.CreateCustomTool(mtEditWaypoint, 0, miCrossCursor, miCrossCursor);
  FMap.CreateCustomTool(mtSelectObject, 0, miSelectRectPlusCursor);
  FMap.CreateCustomTool(mtDeployPosition, 0, miCrossCursor);
  FMap.CreateCustomTool(mtCenterOnHook, 0, miCrossCursor);

  {Mubdi}
  FMap.CreateCustomTool(mtRuler, 0, miCrossCursor);


  FMap.CreateCustomTool(mtAnchorTool, 0, miCrossCursor);

  {nando}
  FMap.CreateCustomTool(mtAimpoint, 0, miCrossCursor, miCrossCursor);

  {farid}
  FMap.CreateCustomTool(mtDeployChaff, 0, miCrossCursor);

  {mk}
  FMap.CreateCustomTool(mtDeployNGS, 0, miCrossCursor);

  {Hambali}
  FMap.CreateCustomTool(mtEditOverlayDynamic, 0, miCrossCursor);
  FMap.CreateCustomTool(mtEditOverlayStatic, 0, miCrossCursor);
  FMap.CreateCustomTool(mtAddOverlay, 0, miCrossCursor);
  FMap.CreateCustomTool(mtMultiMode, 0, miCrossCursor);
  FMap.CreateCustomTool(mtAddLogistic, 0, miCrossCursor);
  FMap.CreateCustomTool(mtEditLogistic, 0, miCrossCursor);
  FMap.CreateCustomTool(mtAddBase, 0, miCrossCursor);

  {choco}
  FMap.CreateCustomTool(mtLayerInfo, 0, miInfoCursor);
  FMap.CreateCustomTool(mtCameraPosition, 0, miCrossCursor);
  FMap.CreateCustomTool(mtDeployMine, 0, miCrossCursor);
end;

procedure TSimMap.DrawMap;
var
  i : Integer;
begin

  FMap.Repaint;
end;

procedure TSimMap.LoadMap(aGeoset: String);
var
  i: Integer;
  z: OleVariant;
  mInfo: CMapXLayerInfo;
begin
  if FMap = nil then
    Exit;
  InitOleVariant(z);
  FMap.Layers.RemoveAll;

  FMap.Geoset := aGeoset;

  if aGeoset <> '' then
  begin
    for i := 1 to FMap.Layers.Count do
    begin
      FMap.Layers.Item(i).Selectable := False;
      FMap.Layers.Item(i).Editable := False;
      FMap.Layers.Item(i).AutoLabel := False;
    end;

    mInfo := CoLayerInfo.Create;
    mInfo.type_ := miLayerInfoTypeUserDraw;
    mInfo.AddParameter('Name', 'LYR_DRAW');
    FLyrDraw := FMap.Layers.Add(mInfo, 1);

    FMap.Layers.AnimationLayer := FLyrDraw;
    FMap.MapUnit := miUnitNauticalMile;
    FMap.Title.Visible := False;
  end;
  FMap.BackColor := RGB(192, 224, 255);
end;

procedure TSimMap.MapDrawUserLayer(ASender: TObject; const Layer: IDispatch;
  hOutputDC, hAttributeDC: Cardinal; const RectFull, RectInvalid: IDispatch);
begin
  if not Assigned(FCanvas) then
    Exit;

  FCanvas.Handle := hOutputDC;

  if Assigned(FOnDrawToCanvas) then
    FOnDrawToCanvas(FCanvas);

  if FMouseIsDown then begin
    if FMap.CurrentTool = miZoomInTool then begin
       FCanvas.Brush.Color := clWhite;
       FCanvas.FrameRect(FRectZoom);
    end;
  end;

end;

procedure TSimMap.MapMouseDouble(sender: TObject);
begin
  if Assigned(FOnMapMouseDouble) then
    FOnMapMouseDouble(sender);

  if FMouseIsDown then
  begin
    if (FMap.CurrentTool = miZoomInTool) or (FMap.CurrentTool = mtMultiMode) then
    begin
      FRectZoom.Left   := FMap.X;
      FRectZoom.Top    := FMap.Y;
      FRectZoom.Right  := FMap.X;
      FRectZoom.Bottom := FMap.Y;
    end;

    FMap.Repaint;
  end;

end;

procedure TSimMap.MapMouseDown(sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin

  if Assigned(FOnMapMouseDown) then
    FOnMapMouseDown(sender,Button,shift,X,Y);

  FMouseIsDown := Button = mbLeft;
  if FMouseIsDown then begin
    if FMap.CurrentTool = miZoomInTool then begin
     FRectZoom.Left   := x;
     FRectZoom.Top    := y;
     FRectZoom.Right  := x;
     FRectZoom.Bottom := y;

    end;

    FMap.Repaint;
  end;

end;

procedure TSimMap.MapMouseExit(sender: TObject);
begin
  if Assigned(FOnMapMouseExit) then
    FOnMapMouseExit(sender);

  if FMouseIsDown then
  begin
    if (FMap.CurrentTool = miZoomInTool) or (FMap.CurrentTool = mtMultiMode) then
    begin
      FRectZoom.Left   := FMap.X;
      FRectZoom.Top    := FMap.Y;
      FRectZoom.Right  := FMap.X;
      FRectZoom.Bottom := FMap.Y;
    end;

    FMap.Repaint;
  end;
end;

procedure TSimMap.MapMouseMove(sender: TObject; Shift: TShiftState;
  X, Y: Integer);
begin
  if Assigned(FOnMapMouseMove) then
    FOnMapMouseMove(sender,shift,X,Y);

  if FMouseIsDown then begin
    if (FMap.CurrentTool = miZoomInTool) or (FMap.CurrentTool = mtMultiMode) then begin
     FRectZoom.Right  := x;
     FRectZoom.Bottom := y;
    end;

    FMap.Repaint;
  end;

end;

procedure TSimMap.MapMouseSingle(sender: TObject);
begin
  if Assigned(FOnMapMouseSingle) then
    FOnMapMouseSingle(sender);

  if FMouseIsDown then
  begin
    if (FMap.CurrentTool = miZoomInTool) or (FMap.CurrentTool = mtMultiMode) then
    begin
      FRectZoom.Left   := FMap.X;
      FRectZoom.Top    := FMap.Y;
      FRectZoom.Right  := FMap.X;
      FRectZoom.Bottom := FMap.Y;
    end;

    FMap.Repaint;
  end;

end;

procedure TSimMap.MapMouseUp(sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if Assigned(FOnMapMouseUp) then
    FOnMapMouseUp(sender,Button,shift,X,Y);

  if FMouseIsDown then
     FMap.Repaint;

  FMouseIsDown := false;
end;

procedure TSimMap.MapToolUsed(ASender: TObject; ToolNum: Smallint;
  X1, Y1, X2, Y2, Distance: double; Shift, Ctrl: WordBool;
  var EnableDefault: WordBool);
begin
  if Assigned(FOnToolUsed) then
    FOnToolUsed(ASender, ToolNum, X1, Y1, X2, Y2,
            Distance, Shift, Ctrl, EnableDefault);

  case ToolNum of
    mtAddWaypoint : if Assigned(FOnAddWayoint) then FOnAddWayoint(X1,Y1);
    mtEditWaypoint : if Assigned(FOnEditWayoint) then FOnEditWayoint(X1,Y1);
  end;
end;

procedure TSimMap.MapViewChanged(Sender: TObject);
begin
  if Assigned(FOnMapChange) then
    FOnMapChange(FMap)
end;


procedure TSimMap.SetMapComponent;
begin
  FMap.OnDrawUserLayer  := MapDrawUserLayer;
  FMap.OnMouseMove      := MapMouseMove;
  FMap.OnMouseUp        := MapMouseUp;
  FMap.OnMouseDown      := MapMouseDown;
  FMap.OnDblClick       := MapMouseDouble;
  FMap.OnClick          := MapMouseSingle;
  FMap.OnExit           := MapMouseExit;
  FMap.OnToolUsed       := MapToolUsed;
  FMap.OnMapViewChanged := MapViewChanged;
end;

procedure TSimMap.SetOnAddWayoint(const Value: TEventAddWaypoint);
begin
  FOnAddWayoint := Value;
end;

procedure TSimMap.SetOnEditWayoint(const Value: TEventEditWaypoint);
begin
  FOnEditWayoint := Value;
end;

procedure TSimMap.SetOnLogTemporary(const Value: TGetStrProc);
begin
  FOnLogTemporary := Value;
end;

procedure TSimMap.SetOnMapMouseDouble(const Value: TMapMouseDouble);
begin
  FOnMapMouseDouble := Value;
end;

procedure TSimMap.SetOnMapMouseDown(const Value: TMapMouseDown);
begin
  FOnMapMouseDown := Value;
end;

procedure TSimMap.SetOnMapMouseExit(const Value: TMapMouseExit);
begin
  FOnMapMouseExit := Value;
end;

procedure TSimMap.SetOnMapMouseMove(const Value: TMapMouseMove);
begin
  FOnMapMouseMove := Value;
end;

procedure TSimMap.SetOnMapMouseSingle(const Value: TMapMouseSingle);
begin
  FOnMapMouseSingle := Value;
end;

procedure TSimMap.SetOnMapMouseUp(const Value: TMapMouseUp);
begin
  FOnMapMouseUp := Value;
end;

procedure TSimMap.SetOnToolUsed(const Value: TEventToolMapUsed);
begin
  FOnToolUsed := Value;
end;

procedure TSimMap.SetMapCenter(const x, y: double);
begin
  FMap.ZoomTo(FMap.Zoom, X, Y);
end;

procedure TSimMap.SetMapZoom(const z: double);
begin
  FMap.ZoomTo(Z, FMap.CenterX, FMap.CenterY);
end;

end.
