unit uFormationInfo;

interface

type
  TFormationInfo = class
  private
    FFormationAltitude: double;
    FFormationBearing: double;
    FisFormationLeader: Boolean;
    FFormationState: integer;
    FFormationLeader: integer;
    FFormationRange: double;
    FFormationModeActive: Boolean;
    FFormationName: String;
    procedure SetFormationAltitude(const Value: double);
    procedure SetFormationBearing(const Value: double);
    procedure SetFormationLeader(const Value: integer);
    procedure SetFormationModeActive(const Value: Boolean);
    procedure SetFormationName(const Value: String);
    procedure SetFormationRange(const Value: double);
    procedure SetFormationState(const Value: integer);
    procedure SetisFormationLeader(const Value: Boolean);
  published
  public
    property isFormationLeader    : Boolean read FisFormationLeader write SetisFormationLeader;
    property FormationModeActive  : Boolean read FFormationModeActive write SetFormationModeActive;
    property FormationName        : String read FFormationName write SetFormationName;
    property FormationLeader      : integer read FFormationLeader write SetFormationLeader;
    property FormationState       : integer read FFormationState write SetFormationState;
    property FormationRange       : double read FFormationRange write SetFormationRange;
    property FormationAltitude    : double read FFormationAltitude write SetFormationAltitude;
    property FormationBearing     : double read FFormationBearing write SetFormationBearing;

  end;
implementation

{ TFormationInfo }

procedure TFormationInfo.SetFormationAltitude(const Value: double);
begin
  FFormationAltitude := Value;
end;

procedure TFormationInfo.SetFormationBearing(const Value: double);
begin
  FFormationBearing := Value;
end;

procedure TFormationInfo.SetFormationLeader(const Value: integer);
begin
  FFormationLeader := Value;
end;

procedure TFormationInfo.SetFormationModeActive(const Value: Boolean);
begin
  FFormationModeActive := Value;
end;

procedure TFormationInfo.SetFormationName(const Value: String);
begin
  FFormationName := Value;
end;

procedure TFormationInfo.SetFormationRange(const Value: double);
begin
  FFormationRange := Value;
end;

procedure TFormationInfo.SetFormationState(const Value: integer);
begin
  FFormationState := Value;
end;

procedure TFormationInfo.SetisFormationLeader(const Value: Boolean);
begin
  FisFormationLeader := Value;
end;

end.
