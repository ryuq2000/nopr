unit uT3Environment;

interface

uses

 tttData,Classes,math,
 uDBClassDefinition,
 uCalculationEnvi,
 uCoordConvertor;

type

  TT3GameEnvironment = class
  private
    FConverter : TCoordConverter;
    // this var class clone from dbgameenvironment, can be edited from game
    FEnvironment : TDBGame_Environment_Definition;
    max_power_scale_area : integer;
  public
    CalculateEnvi : CalculationEnvironment;

    constructor Create(conv : TCoordConverter);
    destructor Destroy;override;

    procedure LoadEnvironmentFromDB(dbGameEnvi :TDBGame_Environment_Definition );
    procedure calculateMaxPowerScaleArea;
    procedure getVisual(var daytime: Double; var nighttime: Double;
              var sunrise: Double; var sunset: Double);
    procedure getCalc_EnviEffect(TrackDomain: Integer; ship_course: Double;
              ship_speed: Double; temp : Boolean; var Course: Double; var Speed: Double);

    function getMaxPOwerScaleArea:integer;
    function checkSubArea(Longitude : Double; Latitude: Double): TDBSubArea_Enviro_Definition;
    function get_DX(TrackDomain: Integer; deltaT: double): Double;
    function get_DY(TrackDomain: Integer; deltaT: double): Double;

    function convertSpeed(Value : Double): Double;
    function msToKnots(Value : Double): Double;

    property Data : TDBGame_Environment_Definition read FEnvironment;
  end;

implementation

uses Windows, uGlobalVar;

const
   c_msToKnots = 1.942615;

{ TGame_Environment_Definition }

constructor TT3GameEnvironment.Create;
begin
  CalculateEnvi := CalculationEnvironment.Create;

  FEnvironment := TDBGame_Environment_Definition.Create;
  FConverter := conv;
end;

destructor TT3GameEnvironment.Destroy;
begin
  CalculateEnvi.Free;
  FEnvironment.Free;
  inherited;
end;

procedure TT3GameEnvironment.calculateMaxPowerScaleArea;
var area_length:extended;
    power:integer;
begin
  power := 10;
  area_length := simManager.Scenario.DBGameAreaDefinition.Game_X_Dimension; //ambil x dan y sama saja

  if area_length<>0 then
    power := trunc(log2(area_length))+1;

  max_power_scale_area := power;

end;

function TT3GameEnvironment.getMaxPOwerScaleArea;
begin
 result := max_power_scale_area;
end;

function  TT3GameEnvironment.checkSubArea(Longitude : Double; Latitude: Double): TDBSubArea_Enviro_Definition;
var
  I: Integer;
  pt : TPoint;
  point_X, point_Y  : Integer;
  rect : TRect;
  item : TDBSubArea_Enviro_Definition;
begin
   Result := nil;

   FConverter.ConvertToScreen(Longitude, Latitude, point_X, point_Y);
   pt.X := point_X;
   pt.Y := point_Y;
   for item in simManager.Scenario.DBSubAreaDefinitions do
   begin
      with rect do
      begin
        Left   := Round(item.X_Position_1);
        Top    := Round(item.Y_Position_1);
        Right  := Round(item.X_Position_2);
        Bottom := Round(item.Y_Position_2);
      end;

      if PtInRect(rect, pt) then
      begin
       Result := item;
       Break;
      end;
   end;
end;

procedure TT3GameEnvironment.getVisual(var daytime: Double; var nighttime: Double;
var sunrise: Double; var sunset: Double);
begin
   daytime   := simManager.Scenario.DBGameEnvironment.Daytime_Visual_Modifier;
   nighttime := simManager.Scenario.DBGameEnvironment.Nighttime_Visual_Modifier;
   sunrise   := simManager.Scenario.DBGameEnvironment.Sunrise;
   sunset    := simManager.Scenario.DBGameEnvironment.Sunset;
end;

procedure TT3GameEnvironment.getCalc_EnviEffect(TrackDomain: Integer; ship_course: Double; ship_speed: Double;
temp: Boolean; var Course: Double; var Speed: Double);
var
   envSpeed, envCourse : double;
begin
   if TrackDomain = 3 then
   begin
      Course := 0;
      Speed  := 0;
   end
   else
   begin
     if temp then
     begin
        envSpeed  := simManager.Scenario.DBGameEnvironment.Wind_Speed;
        envCourse := simManager.Scenario.DBGameEnvironment.Wind_Direction;
     end
     else
     begin
        envSpeed  := simManager.Scenario.DBGameEnvironment.Ocean_Current_Speed;
        envCourse := simManager.Scenario.DBGameEnvironment.Ocean_Current_Direction;
     end;

       CalculateEnvi.getCurrent_EnviEffect
       (ship_course, envCourse,
        ship_speed,  envSpeed,
        Course,      Speed);
   end;
end;

function TT3GameEnvironment.get_DX(TrackDomain: Integer; deltaT: double): Double;
var
   temp : double;
begin
   Result := 0;
   if TrackDomain = 0 then                             // domain = air
     if simManager.Scenario.DBGameEnvironment.Wind_Speed <> 0 then
     begin
       temp   := convertSpeed(simManager.Scenario.DBGameEnvironment.Wind_Speed);
       Result := temp*cos(DegToRad(90-simManager.Scenario.DBGameEnvironment.Wind_Direction))*deltaT;
     end;

   if (TrackDomain = 1) OR (TrackDomain = 2) then      // domain surface / subsurface
     if simManager.Scenario.DBGameEnvironment.Ocean_Current_Speed <> 0 then
     begin
       temp   := convertSpeed(simManager.Scenario.DBGameEnvironment.Ocean_Current_Speed);
       Result := temp*cos(DegToRad(90-simManager.Scenario.DBGameEnvironment.Ocean_Current_Direction))*deltaT;
     end;

   if TrackDomain = 3 then                             // domain land
      Result := 0;
end;

function TT3GameEnvironment.get_DY(TrackDomain: Integer; deltaT: double): Double;
var
   temp : double;
begin
   Result := 0;
   if TrackDomain = 0 then                            // domain = air
     if simManager.Scenario.DBGameEnvironment.Wind_Speed <> 0 then
     begin
       temp   := convertSpeed(simManager.Scenario.DBGameEnvironment.Wind_Speed);
       Result := temp*sin(DegToRad(90-simManager.Scenario.DBGameEnvironment.Wind_Direction))*deltaT;
     end;

   if (TrackDomain = 1) OR (TrackDomain = 2) then    // domain surface / subsurface
     if simManager.Scenario.DBGameEnvironment.Ocean_Current_Speed <> 0 then
     begin
       temp   := convertSpeed(simManager.Scenario.DBGameEnvironment.Ocean_Current_Speed);
       Result := temp*sin(DegToRad(90-simManager.Scenario.DBGameEnvironment.Ocean_Current_Direction))*deltaT;
     end;

   if TrackDomain = 3 then                          // domain land
      Result := 0;
end;

procedure TT3GameEnvironment.LoadEnvironmentFromDB;
begin
  with FEnvironment do
  begin
    Game_Enviro_Index                := dbGameEnvi.Game_Enviro_Index             ;
    Game_Enviro_Identifier           := dbGameEnvi.Game_Enviro_Identifier        ;
    Game_Area_Index                  := dbGameEnvi.Game_Area_Index               ;
    Wind_Speed                       := dbGameEnvi.Wind_Speed                    ;
    Wind_Direction                   := dbGameEnvi.Wind_Direction                ;
    Daytime_Visual_Modifier          := dbGameEnvi.Daytime_Visual_Modifier       ;
    Nighttime_Visual_Modifier        := dbGameEnvi.Nighttime_Visual_Modifier     ;
    Daytime_Infrared_Modifier        := dbGameEnvi.Daytime_Infrared_Modifier     ;
    Nighttime_Infrared_Modifier      := dbGameEnvi.Nighttime_Infrared_Modifier   ;
    Sunrise                          := dbGameEnvi.Sunrise                       ;
    Sunset                           := dbGameEnvi.Sunset                        ;
    Period_of_Twilight               := dbGameEnvi.Period_of_Twilight            ;
    Rain_Rate                        := dbGameEnvi.Rain_Rate                     ;
    Cloud_Base_Height                := dbGameEnvi.Cloud_Base_Height             ;
    Cloud_Attenuation                := dbGameEnvi.Cloud_Attenuation             ;
    Sea_State                        := dbGameEnvi.Sea_State                     ;
    Ocean_Current_Speed              := dbGameEnvi.Ocean_Current_Speed           ;
    Ocean_Current_Direction          := dbGameEnvi.Ocean_Current_Direction       ;
    Thermal_Layer_Depth              := dbGameEnvi.Thermal_Layer_Depth           ;
    Sound_Velocity_Type              := dbGameEnvi.Sound_Velocity_Type           ;
    Surface_Sound_Speed              := dbGameEnvi.Surface_Sound_Speed           ;
    Layer_Sound_Speed                := dbGameEnvi.Layer_Sound_Speed             ;
    Bottom_Sound_Speed               := dbGameEnvi.Bottom_Sound_Speed            ;
    Bottomloss_Coefficient           := dbGameEnvi.Bottomloss_Coefficient        ;
    Ave_Ocean_Depth                  := dbGameEnvi.Ave_Ocean_Depth               ;
    CZ_Active                        := dbGameEnvi.CZ_Active                     ;
    Surface_Ducting_Active           := dbGameEnvi.Surface_Ducting_Active        ;
    Upper_Limit_Surface_Duct_Depth   := dbGameEnvi.Upper_Limit_Surface_Duct_Depth;
    Lower_Limit_Surface_Duct_Depth   := dbGameEnvi.Lower_Limit_Surface_Duct_Depth;
    Sub_Ducting_Active               := dbGameEnvi.Sub_Ducting_Active            ;
    Upper_Limit_Sub_Duct_Depth       := dbGameEnvi.Upper_Limit_Sub_Duct_Depth    ;
    Lower_Limit_Sub_Duct_Depth       := dbGameEnvi.Lower_Limit_Sub_Duct_Depth    ;
    Shipping_Rate                    := dbGameEnvi.Shipping_Rate                 ;
    Shadow_Zone_Trans_Loss           := dbGameEnvi.Shadow_Zone_Trans_Loss        ;
    Atmospheric_Refract_Modifier     := dbGameEnvi.Atmospheric_Refract_Modifier  ;
    Barometric_Pressure              := dbGameEnvi.Barometric_Pressure           ;
    Air_Temperature                  := dbGameEnvi.Air_Temperature               ;
    Surface_Temperature              := dbGameEnvi.Surface_Temperature           ;
    Start_HF_Range_Gap               := dbGameEnvi.Start_HF_Range_Gap            ;
    End_HF_Range_Gap                 := dbGameEnvi.End_HF_Range_Gap              ;
    Occurance_Range                  := dbGameEnvi.Occurance_Range               ;
    Width                            := dbGameEnvi.Width                         ;
    Signal_Reduction_Term            := dbGameEnvi.Signal_Reduction_Term         ;
    Increase_per_CZ                  := dbGameEnvi.Increase_per_CZ               ;
    Max_Sonar_Depth                  := dbGameEnvi.Max_Sonar_Depth               ;
  end;

end;

//function TGame_Environment_Definition.get_DZ(deltaT: Double): Double;
//var
//   temp, tempInKnots : double;
//begin
//   tempInKnots := msToKnots(FData.Wind_Speed);
//   temp        := convertSpeed(tempInKnots);
//   Result      := temp*sin(DegToRad(90-tempInKnots))*deltaT;
//end;

function  TT3GameEnvironment.convertSpeed(Value : Double): Double;
begin
  Result := (1.0 / 60.0) * Value * (1 / 3600.0);
end;

function TT3GameEnvironment.msToKnots(value : Double): Double;
begin
  Result := c_msToKnots * Value;
end;

end.
