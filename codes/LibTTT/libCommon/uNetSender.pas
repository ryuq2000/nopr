unit uNetSender;

interface

uses
  uGameData_TTT;

type

  TNetCmdSender = class abstract
  private
    procedure SetSessionID(const Value: Integer);
  published

  protected
    FSessionID: Integer;

    // record variable
    recSonarDeploy  : TRecCmd_SonarDeploy;
    recSensor       : TRecCmd_Sensor;
    recIFFSMode     : TRecCmd_IFFSearchMode;
    recIFFTarget    : TRecCmd_TargetIFF;
    recIFFSensor    : TRecCmd_SensorIFF;

  public
    constructor Create(sessionID : Integer);overload;
    constructor Create;overload;

    procedure CmdGameControl(var r: TRecCmd_GameCtrl); virtual;
    procedure CmdPlatformGuidance(var r : TRecCmd_PlatformGuidance ); virtual;

    procedure CmdSensor(var r : TRecCmd_Sensor); virtual;

    {*------------------------------------------------------------------------------
      List of Command procedure for counter measure
    -------------------------------------------------------------------------------}
    procedure CmdAcousticDecoyOnBoard(var r : TRecCmdAcousticDecoyOnBoard); virtual;
    procedure CmdChaffOnBoard(var r : TRecCmdChaffOnBoard); virtual;
    procedure CmdBubbleOnBoard(var r : TRecCmdBubbleOnBoard); virtual;
    procedure CmdFloatingDecoyOnBoard(var r : TRecCmdFloatingDecoyOnBoard); virtual;
    procedure CmdSelfDefenceOnBoard(var r : TRecCmdSelfDefenseOnBoard); virtual;
    procedure CmdRadarNoiseOnBoard(var r : TRecCmdRadarNoiseOnBoard); virtual;
    {*------------------------------------------------------------------------------
    -------------------------------------------------------------------------------}

    procedure CmdRepos(var r : TRecCmd_Platform_MOVE); virtual;
    procedure CmdNoiseJammer(var r : TrecRadarNoiseJammer); virtual;
    procedure CmdEMCON(var r : TRecCmd_ModeEmcon); virtual;
    procedure CmdMissile(var r : TRecCmd_LaunchMissile); virtual;
    procedure CmdLaunchRP(var r : TRecCmd_LaunchRP); virtual;
    procedure CmdGunFire(var r : TRecCmd_GunFire); virtual;
    procedure CmdLaunchChaff(var r : TRecCmd_LaunchChaff); virtual;
    procedure CmdSyncWaypoint(var r : TrecSinc_Waypoint); virtual;
    procedure CmdSynTrack(var r : TRecTrack);virtual;
    procedure CmdTCPRequest(var r: TRecTCP_Request);virtual;

    procedure CmdSonarDeploy(
            const aShipID, aSonarID: Integer;
            const aParam: Byte;
            const aTime : Integer;
            const aActualCable, aOrderCable: Double);virtual;
    procedure CmdIFFSearchMode(const pi_id : integer; const sensID: integer;
            const aMode : byte);virtual;
    procedure CmdTargetIFF(const pi_id: integer; const tgt_id: integer;
        const sensID: integer);virtual;
    procedure CmdSensorIFF(const pi_id: integer; const tgt_id: integer;
        const sensID: integer; const index_mod: Byte; const check_mod: Boolean;
        const value_mod: integer);virtual;

    property SessionID : Integer read FSessionID write SetSessionID;


  end;

implementation

{ TNetCmdSender }

constructor TNetCmdSender.Create(sessionID: Integer);
begin
  FSessionID := sessionID;
end;

procedure TNetCmdSender.CmdAcousticDecoyOnBoard(var r: TRecCmdAcousticDecoyOnBoard);
begin
  r.SessionID := FSessionID;
end;

procedure TNetCmdSender.CmdBubbleOnBoard(var r: TRecCmdBubbleOnBoard);
begin
  r.SessionID := FSessionID;
end;

procedure TNetCmdSender.CmdChaffOnBoard(var r: TRecCmdChaffOnBoard);
begin
  r.SessionID := FSessionID;
end;

procedure TNetCmdSender.CmdEMCON(var r: TRecCmd_ModeEmcon);
begin
  r.SessionID := FSessionID;
end;

procedure TNetCmdSender.CmdFloatingDecoyOnBoard(
  var r: TRecCmdFloatingDecoyOnBoard);
begin
  r.SessionID := FSessionID;
end;

procedure TNetCmdSender.CmdGameControl(var r: TRecCmd_GameCtrl);
begin
  r.SessionID := FSessionID;
end;

procedure TNetCmdSender.CmdGunFire(var r: TRecCmd_GunFire);
begin
  r.SessionID := FSessionID;
end;

procedure TNetCmdSender.CmdIFFSearchMode(const pi_id, sensID: integer;
  const aMode: byte);
begin
  with recIFFSMode do
  begin
    SessionID := FSessionID;
    PlatformID    := pi_id;
    SensorID      := sensID;
    ModeSearch    := aMode;
  end;
end;

procedure TNetCmdSender.CmdLaunchChaff(var r: TRecCmd_LaunchChaff);
begin
  r.SessionID := FSessionID;
end;

procedure TNetCmdSender.CmdMissile(var r: TRecCmd_LaunchMissile);
begin
  r.SessionID := FSessionID;
end;

procedure TNetCmdSender.CmdLaunchRP(var r: TRecCmd_LaunchRP);
begin
  r.SessionID := FSessionID;
end;

procedure TNetCmdSender.CmdNoiseJammer(var r: TrecRadarNoiseJammer);
begin
  r.SessionID := FSessionID;

end;

procedure TNetCmdSender.CmdPlatformGuidance(var r: TRecCmd_PlatformGuidance);
begin
  r.SessionID := FSessionID;
end;

procedure TNetCmdSender.CmdRadarNoiseOnBoard(var r: TRecCmdRadarNoiseOnBoard);
begin
  r.SessionID := FSessionID;
end;

procedure TNetCmdSender.CmdRepos(var r: TRecCmd_Platform_MOVE);
begin
  r.SessionID := FSessionID;
end;

procedure TNetCmdSender.CmdSensor(var r: TRecCmd_Sensor);
begin
  r.SessionID := FSessionID;
end;

procedure TNetCmdSender.CmdSelfDefenceOnBoard(var r: TRecCmdSelfDefenseOnBoard);
begin
  r.SessionID := FSessionID;
end;

procedure TNetCmdSender.CmdSonarDeploy(const aShipID, aSonarID: Integer;
      const aParam: Byte; const aTime : Integer;
      const aActualCable, aOrderCable: Double);
begin
  with recSonarDeploy do
  begin
    SessionID   := FSessionID;
    PlatformID  := aShipID;
    SensorID    := aSonarID;
    Param       := aParam;
    TimeToActive := aTime;
    ActualCable := aActualCable;
    OrderCable  := aOrderCable;
  end;
end;

procedure TNetCmdSender.CmdSyncWaypoint(var r: TrecSinc_Waypoint);
begin
  r.SessionID := FSessionID;
end;

procedure TNetCmdSender.CmdSynTrack(var r: TRecTrack);
begin
  r.SessionID := FSessionID;
end;

procedure TNetCmdSender.CmdTargetIFF(const pi_id, tgt_id, sensID: integer);
begin
  with recIFFTarget do
  begin
    SessionID   := FSessionID;
    PlatformID  := pi_id;
    TargetID    := tgt_id;
    SensorID    := sensID;
  end;

end;

procedure TNetCmdSender.CmdTCPRequest(var r: TRecTCP_Request);
begin
  r.SessionID := FSessionID;
end;

constructor TNetCmdSender.Create;
begin

end;

procedure TNetCmdSender.CmdSensorIFF(const pi_id, tgt_id,
  sensID: integer; const index_mod: Byte; const check_mod: Boolean;
  const value_mod: integer);
begin
  with recIFFSensor do
  begin
    SessionID := FSessionID;
    PlatformID := pi_id;
    TargetID := tgt_id;
    SensorID := sensID;
    index_mode := index_mod;
    check_mode := check_mod;
    value_mode := value_mod;
  end;
end;

procedure TNetCmdSender.SetSessionID(const Value: Integer);
begin
  FSessionID := Value;
end;

end.
