unit uT3RadarVerticalCoverage;

interface

uses  Classes, uDBClassDefinition, Generics.Collections;

type

  TCoverage = class
  public
    range : double;
    min, max : double;
  end;

  TCoverageList = TObjectList<TCoverage>;

  TT3RadarVerticalCoverage   = class
  private
    covDiagram1, covDiagram2 : TCoverageList;

    procedure AddCoverage(diagram : TCoverageList; coverage : TCoverage);
  public
    constructor Create;
    destructor Destroy;override;

    procedure ReSort;
    procedure Add(pData : TDBRadar_Vertical_Coverage);
    procedure GetCoverage(range : double; var min1,max1,min2,max2 : double);
    function isDataCoverageExist : boolean;
  end;

implementation

{ TT3RadarVerticalCoverage }

procedure TT3RadarVerticalCoverage.Add(pData: TDBRadar_Vertical_Coverage);
var
  coverage : TCoverage;
begin
  // ignore 0 range
  if pData.Vert_Coverage_Range <= 0.0009 then exit;

  coverage := TCoverage.Create;
  coverage.range := pData.Vert_Coverage_Range;
  coverage.min   := pData.Vert_Cover_Min_Elevation;
  coverage.max   := pData.Vert_Cover_Max_Elevation;

  case pData.Coverage_Diagram of
    // first vertical coverage diagram
    1 : AddCoverage(covDiagram1, coverage);
    // second vertical coverage diagram
    2 : AddCoverage(covDiagram2, coverage);
  end;
end;

procedure TT3RadarVerticalCoverage.AddCoverage(diagram: TCoverageList;
  coverage: TCoverage);
var
  item : TCoverage;
  i : integer;
begin
  // add and sorted by range
  if diagram.Count = 0 then begin
    diagram.Add(coverage);
    exit;
  end;

  i := 0;
  for item in diagram do begin
    if item.range >= coverage.range then break;

    Inc(i);
  end;

  diagram.Insert(i,coverage);

end;

constructor TT3RadarVerticalCoverage.Create;
begin
  covDiagram1 := TCoverageList.Create;
  covDiagram2 := TCoverageList.Create;
end;

destructor TT3RadarVerticalCoverage.Destroy;
begin
  covDiagram1.Free;
  covDiagram2.Free;

  inherited;
end;

procedure TT3RadarVerticalCoverage.GetCoverage(range: double; var min1, max1,
  min2, max2: double);
var i : integer;
    item, item2 : TCoverage;
    val : double;
begin
  min1 := -100000000;
  min2 := -100000000;
  max1 := 100000000;
  max2 := 100000000;

  // diagram vertical coverage pertama
  if covDiagram1.Count > 1 then begin
    item :=  nil;
    for i := 0 to covDiagram1.Count - 1 do begin
      item := covDiagram1.Items[i];
      if range > item.range then continue else break;
    end;
    if Assigned(item) then begin

      if (i = 0) or (i = covDiagram1.Count - 1) then begin
        min1 := item.min;
        max1 := item.max;
      end else
      begin
        item2 := covDiagram1.Items[i-1];
        // pake interpolasi
        // Y = (((X - X0)*Y1) + ((X1 - X)*Y0)) / (X1 - X0);

        val := (item.range - item2.range);
        if val = 0 then begin
          min1 := 0;
          max1 := 0;
        end else begin
          min1 := (((range - item2.range)*item.min) +
                  ((item.range-range)*item2.min)) /
                  (item.range - item2.range);
          max1 := (((range - item2.range)*item.max) +
                  ((item.range-range)*item2.max)) /
                  (item.range - item2.range);
        end;
      end;
    end;
  end;

  // diagram vertical coverage kedua
  if covDiagram2.Count > 1 then begin
    item := nil;
    for i := 0 to covDiagram2.Count - 1 do begin
      item := covDiagram2.Items[i];
      if range > item.range then continue else break;
    end;

    if Assigned(item) then begin
      if (i = 0) or (i = covDiagram2.Count - 1) then begin
        min2 := item.min;
        max2 := item.max;
      end else
      begin
        item2 := covDiagram2.Items[i-1];
        // pake interpolasi
        // Y = (((X - X0)*Y1) + ((X1 - X)*Y0)) / (X1 - X0);

        val := (item.range - item2.range);
        if val = 0 then begin
          min1 := 0;
          max1 := 0;
        end else begin
          min2 := (((range - item2.range)*item.min) +
                  ((item.range-range)*item2.min)) /
                  (item.range - item2.range);
          max2 := (((range - item2.range)*item.max) +
                  ((item.range-range)*item2.max)) /
                  (item.range - item2.range);
        end;
      end;
    end;
  end;
end;

function TT3RadarVerticalCoverage.isDataCoverageExist: boolean;
begin
  result := (covDiagram1.Count > 0) or (covDiagram2.Count > 0)
end;

procedure TT3RadarVerticalCoverage.ReSort;
begin

end;

end.
