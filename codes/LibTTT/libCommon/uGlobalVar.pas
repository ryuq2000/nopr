unit uGlobalVar;

interface

uses tttData, uT3SimManager, uT3PlatformInstance, uNetHandle_TTT, uDBClassDefinition,
  uDBScenario;
  { note, all variable declared are singleton }

  var
    { global variable to set machine role as cubicle, controller or server
      set from clientmanager or servermanager }
    machineRole : TMachineRole;

    { golbal variable for sim manager, can be servermanager or clientmanager,
      depend on machine role }
    simManager : TT3SimManager;

    { global variable for network handle, can be client or server,
      must initialize ini script client or script server
     }
    nethandle : TNetHandle_TTT;

  { singleton,  only 1 instance game scenario }
  function getScenario : TT3DBScenario;

  { singleton,  only 1 instance game default }
  function queryGameDefault : TDBGame_Defaults;
  { singleton,  only 1 instance environment }
  function queryEnvironment : TDBGame_Environment_Definition;

  {
    be sure to set simmanager first before use these function
    overload if necessary
  }
  procedure queryPlatformInfo(instanceIdx : integer; out posX, posY, posZ, Course, Speed, Altitude : double);overload;

implementation

function getScenario : TT3DBScenario;
begin
  result := simManager.Scenario;
end;

function queryEnvironment : TDBGame_Environment_Definition;
begin
  result := nil;
  if Assigned(simManager) then
    result := simManager.Scenario.DBGameEnvironment;
end;

function queryGameDefault : TDBGame_Defaults;
begin
  result := nil;
  if Assigned(simManager) then
    result := simManager.Scenario.DBGameDefaults;
end;

procedure queryPlatformInfo(instanceIdx : integer; out posX, posY, posZ, Course,
  Speed, Altitude : double);
var
  pf : TT3PlatformInstance;
begin
  posX     := 0;
  posY     := 0;
  posZ     := 0;
  Course   := 0;
  Speed    := 0;
  Altitude := 0;

  if Assigned(simManager) then
  begin
    pf := simManager.FindT3PlatformByID(instanceIdx);
    if Assigned(pf) then
    begin
      posX     := pf.PosX;
      posY     := pf.PosY;
      posZ     := pf.PosZ;
      course   := pf.Course;
      Speed    := pf.Speed;
      Altitude := pf.Altitude;
    end;
  end;

end;

end.
