unit uT3CubicleGroup;

interface

uses
  Generics.Collections, uT3Track, uDBClassDefinition, uT3MountedSensor,
  uT3PlatformInstance;

type
  { copied from T3CubicleGroup, modified }
  TTracksDict = TObjectDictionary<integer, TT3Track>;

  TT3CubicleGroup = class
  private
    FTracks                      : TTracksDict;
    FBlockNumStart, FBlockNumEnd : integer;
    FNextTrackNum                : integer;
    FGroupIndex: integer;

    function assignTrackNumber: integer;
    procedure OnTrackNeedUpdatePos(sender: TObject);
    procedure SetGroupIndex(const Value: integer);
  public
    { create group by cubicle group }
    constructor Create(grp: TDBCubicle_Group);overload;
    { group own by controller }
    constructor Create;overload;
    destructor Destroy;override;

    function TrackNumberExist(tn : integer) : boolean;
    procedure AddTrack(track: TT3Track);
    procedure RemoveTrack(trackNum: integer);overload;
    procedure RemoveTrack(sensor : TT3MountedSensor);overload;

    {*------------------------------------------------------------------------------
      Mencari track yang bener2 dimiliki oleh cubicle ini, bukan track dari
      sensor

      @param pid : platform ID
      @return
    -------------------------------------------------------------------------------}
    function isPlatformIDMemberCubicle(pid: integer): Boolean;

    property Tracks       : TObjectDictionary<integer, TT3Track> read FTracks;
    property GroupIndex   : integer read FGroupIndex write SetGroupIndex;

  end;

implementation

uses
  uGlobalVar, uT3PointTrack;

{ TT3CubicleGroup }

procedure TT3CubicleGroup.AddTrack(track: TT3Track);
begin
  track.TrackNumber := assignTrackNumber;
  FTracks.Add(track.UniqueID,track);
end;

function TT3CubicleGroup.assignTrackNumber: integer;
var
  num, key: integer;
  found : boolean;
begin

  if not TrackNumberExist(FNextTrackNum) then
  begin
    result := FNextTrackNum;
    { calc next available track number }
    Inc(FNextTrackNum);
    { back to start block num again }
    if FNextTrackNum > FBlockNumEnd then
      FNextTrackNum := FBlockNumStart;
  end
  else
  begin
    { search empty slot track number }
    num := FBlockNumStart;

    while (num <= FBlockNumEnd) do
    begin

      if TrackNumberExist(num) then
      begin
        result := num;
        FNextTrackNum := num + 1;
        found := True;
        break;
      end;

      Inc(num);
    end;

    if not found then
      {slot full, number not avail}
      result := -1;
  end;

end;

constructor TT3CubicleGroup.Create;
begin
  FTracks := TObjectDictionary<integer, TT3Track>.Create([doOwnsValues]);

  FBlockNumStart := 1;
  FBlockNumEnd := 10000;

  FNextTrackNum := FBlockNumStart;

end;

constructor TT3CubicleGroup.Create(grp: TDBCubicle_Group);
begin
  FTracks := TObjectDictionary<integer, TT3Track>.Create([doOwnsValues]);

  FBlockNumStart := grp.Track_Block_Start;
  FBlockNumEnd := grp.Track_Block_End;

  FNextTrackNum := grp.Track_Block_Start;

  FGroupIndex := grp.Group_Index;
end;

destructor TT3CubicleGroup.Destroy;
begin
  FTracks.Free;
  inherited;
end;

function TT3CubicleGroup.isPlatformIDMemberCubicle(pid: integer): Boolean;
var
  pair : TPair<Integer, TT3Track>;
begin
  result := False;
  for pair in FTracks do
  begin
    if pair.Value.ObjectInstanceIndex = pid then
    begin
      { }
      if (pair.Value is TT3PointTrack) and (TT3PointTrack(pair.Value).CanControl) then
      begin
        Result := True;
        Break;
      end;
    end;
  end;
end;

procedure TT3CubicleGroup.OnTrackNeedUpdatePos(sender: TObject);
var
  track: TT3Track;
  Pi: TT3PlatformInstance;
begin
  if (sender is TT3Track) then
  begin
    track := TT3Track(sender);
    Pi := nil;

    if Assigned(simManager) then
      Pi := simManager.FindT3PlatformByID(track.ObjectInstanceIndex);

    if Assigned(Pi) then
    begin
      track.setPositionX(Pi.getPositionX);
      track.setPositionY(Pi.getPositionY);
      track.setPositionZ(Pi.getPositionZ);

      track.DetectedSpeed := Pi.Speed;
      track.DetectedCourse := Pi.Heading;
      track.DetectedAltitude := Pi.ALtitude;
    end;
  end;
end;

procedure TT3CubicleGroup.RemoveTrack(trackNum: integer);
begin
  FTracks.Remove(trackNum);
end;

procedure TT3CubicleGroup.RemoveTrack(sensor: TT3MountedSensor);
var
  pair : TPair<Integer, TT3Track>;
begin
  for pair in FTracks do
    pair.Value.removeDetectBy(sensor)
end;

procedure TT3CubicleGroup.SetGroupIndex(const Value: integer);
begin
  FGroupIndex := Value;
end;

function TT3CubicleGroup.TrackNumberExist(tn: integer): boolean;
begin
  result := FTracks.ContainsKey(tn);
end;

end.
