unit uSensorInfo;

interface

uses uBaseCoordSystem, DateUtils, tttData;

type

  TSensorTrackInfo =  class abstract
  private
    FFirstDetected : TDateTime;
    FLastDetected  : TDateTime;
    FLastPosX, FlastPosY, FLastPosZ : double;
    FFlagUpdate: boolean;
    procedure SetFlagUpdate(const Value: boolean);
  protected
    procedure doUpdate(time : TDateTime; currPosX, currPosY, currPosZ, course,speed, altitude : double);virtual;
  public
    constructor Create;virtual;
    procedure updateData(time : TDateTime; currPosX, currPosY, currPosZ, course,speed, altitude : double);
    procedure setFirstDetected(time : TDateTime; currPosX, currPosY,currPosZ,course,speed, altitude : double);

    property LastDetected   : TDateTime read FLastDetected write FLastDetected;
    property FirstDetected  : TDateTime read FFirstDetected write FFirstDetected;
    property LastPostX      : double read FLastPosX;
    property LastPostY      : double read FLastPosY;
    property LastPostZ      : double read FLastPosZ;

    property FlagUpdate     : boolean read FFlagUpdate write SetFlagUpdate; // usefull in track sync
  end;

  TRadarTrackInfo = class (TSensorTrackInfo)
  private
    FDetectedSpeed : double;
    FDetectedCourse : double;
    FDetectedAltitude : double;
    procedure setDetectedAltitude(const Value: double);
    procedure setDetectedCourse(const Value: double);
    procedure setDetectedSpeed(const Value: double);
  protected
    procedure doUpdate(time : TDateTime; currPosX, currPosY, currPosZ, course, speed, altitude : double);override;
  public
    constructor Create;override;

    property DetectedSpeed : double read FDetectedSpeed write setDetectedSpeed;
    property DetectedCourse : double read FDetectedCourse write setDetectedCourse;
    property DetectedAltitude : double read FDetectedAltitude write setDetectedAltitude;
  end;

  TIFFTrackInfo = class (TSensorTrackInfo)
  private
    FTransMode3CDetected: String;
    FisShowIFF: boolean;
    FisMode4IFF: boolean;
    FTransMode2Detected: String;
    FTransMode3Detected: String;
    FTransMode1Detected: String;
    procedure SetisMode4IFF(const Value: boolean);
    procedure SetisShowIFF(const Value: boolean);
    procedure SetTransMode1Detected(const Value: String);
    procedure SetTransMode2Detected(const Value: String);
    procedure SetTransMode3CDetected(const Value: String);
    procedure SetTransMode3Detected(const Value: String);
  published
  public
    property isShowIFF  : boolean read FisShowIFF write SetisShowIFF;
    property isMode4IFF : boolean read FisMode4IFF write SetisMode4IFF;
    property TransMode1Detected : String read FTransMode1Detected write SetTransMode1Detected;
    property TransMode2Detected : String read FTransMode2Detected write SetTransMode2Detected;
    property TransMode3Detected : String read FTransMode3Detected write SetTransMode3Detected;
    property TransMode3CDetected : String read FTransMode3CDetected write SetTransMode3CDetected;
  end;

  TESMTrackInfo = class (TSensorTrackInfo)
  private
    FAmbigousBearing: boolean;
    FBearing: single;
    FRange: single;
    procedure SetAmbigousBearing(const Value: boolean);
    procedure SetBearing(const Value: single);
    procedure SetRange(const Value: single);
  published
    property Bearing         : single read FBearing write SetBearing;
    property Range           : single read FRange write SetRange;
    property AmbigousBearing : boolean read FAmbigousBearing write SetAmbigousBearing;
  end;

function SensorTrackInfoFactory(sensorType : TESensorType) : TSensorTrackInfo;

implementation

function SensorTrackInfoFactory(sensorType : TESensorType) : TSensorTrackInfo;
begin
  case sensorType of
    stRadar : result := TRadarTrackInfo.Create;
    stIFF   : result := TIFFTrackInfo.Create;
    stSonar,
    stESM,
    stEO,
    stSonobuoy,
    stVisual,
    stMAD : result := TSensorTrackInfo.Create;
    else
    result := TSensorTrackInfo.Create;
  end;
end;

{ TSensorTrackInfo }

constructor TSensorTrackInfo.Create;
begin
  FFirstDetected := 0;
  FLastDetected  := 0;
  FLastPosX := 0;
  FlastPosY := 0;
  FlastPosZ := 0;
end;

procedure TSensorTrackInfo.doUpdate(time: TDateTime; currPosX, currPosY,
  currPosZ, course,speed, altitude: double);
begin

end;

procedure TSensorTrackInfo.setFirstDetected(time: TDateTime; currPosX,
  currPosY,currPosZ,course,speed,altitude: double);
begin
  FFirstDetected := time;
  FLastDetected  := time;
  FLastPosX := currPosX;
  FlastPosY := currPosY;
  FlastPosZ := currPosZ;
end;

procedure TSensorTrackInfo.SetFlagUpdate(const Value: boolean);
begin
  FFlagUpdate := Value;
end;

procedure TSensorTrackInfo.updateData(time: TDateTime; currPosX,
  currPosY, currPosZ, course,speed, altitude : double);
begin

  {do update here}
  doUpdate(time, currPosX, currPosY, currPosZ, course,speed, altitude);

  FLastDetected := time;
  FLastPosX := currPosX;
  FlastPosY := currPosY;
  FLastPosZ := currPosZ;

end;

{ TRadarTrackInfo }

constructor TRadarTrackInfo.Create;
begin
  inherited;
  FDetectedSpeed    := 0;
  FDetectedCourse   := 0;
  FDetectedAltitude := 0;
end;

procedure TRadarTrackInfo.doUpdate(time: TDateTime; currPosX,
  currPosY, currPosZ, course,speed, altitude: double);
var
  m : double;
  s : double;
begin

  if time <> FLastDetected then
  begin
    s := SecondsBetween(time, FLastDetected);
    m := CalcRange(currPosX,currPosY,FLastPosX,FlastPosY) * C_NauticalMile_To_Metre;

    if (m <= 0.001) or (s <= 0.0001) then
      FDetectedSpeed := 0
    else
      FDetectedSpeed := m / s; // meter / sec

//    if speed < 0.00001 then
//      FDetectedSpeed    := 0.0
//    else
//      FDetectedSpeed    := speed;

    if course < 0.00001 then
      FDetectedCourse   := 0.0
    else
      FDetectedCourse   := course;

    if currPosZ < 0.00001 then
      FDetectedAltitude := 0.0
    else
      FDetectedAltitude := C_NauticalMile_To_Metre * currPosZ; // meter
  end;

end;

procedure TRadarTrackInfo.setDetectedAltitude(const Value: double);
begin
  FDetectedAltitude := Value;
end;

procedure TRadarTrackInfo.setDetectedCourse(const Value: double);
begin
  FDetectedCourse := Value;
end;

procedure TRadarTrackInfo.setDetectedSpeed(const Value: double);
begin
  FDetectedSpeed := Value;
end;

{ TIFFTrackInfo }

procedure TIFFTrackInfo.SetisMode4IFF(const Value: boolean);
begin
  FisMode4IFF := Value;
end;

procedure TIFFTrackInfo.SetisShowIFF(const Value: boolean);
begin
  FisShowIFF := Value;
end;

procedure TIFFTrackInfo.SetTransMode1Detected(const Value: String);
begin
  FTransMode1Detected := Value;
end;

procedure TIFFTrackInfo.SetTransMode2Detected(const Value: String);
begin
  FTransMode2Detected := Value;
end;

procedure TIFFTrackInfo.SetTransMode3CDetected(const Value: String);
begin
  FTransMode3CDetected := Value;
end;

procedure TIFFTrackInfo.SetTransMode3Detected(const Value: String);
begin
  FTransMode3Detected := Value;
end;

{ TESMTrackInfo }

procedure TESMTrackInfo.SetAmbigousBearing(const Value: boolean);
begin
  FAmbigousBearing := Value;
end;

procedure TESMTrackInfo.SetBearing(const Value: single);
begin
  FBearing := Value;
end;

procedure TESMTrackInfo.SetRange(const Value: single);
begin
  FRange := Value;
end;

end.
