unit uBitmapSymbol;

interface

uses Generics.Collections, Graphics, SysUtils;


type
  {*------------------------------------------------------------------------------
    TBitmapObject digunakan untuk definisi bitmap
  -------------------------------------------------------------------------------}
  TBitmapObject = class
  private
    FBmp        : TBitmap;
    FBitmapName : String;

  public
  {*------------------------------------------------------------------------------
    Fungsi load bitmap
    @param fName nama bitmap
  -------------------------------------------------------------------------------}
    procedure LoadBitmap(fName: string); overload;

  end;

  {*------------------------------------------------------------------------------
    TBitmapsPool digunakan untuk menyimpan uniq bitmap sybol yang
    dipake di simulasi, jika symbol tidak ditemukan maka create dan simpan,
    jika ditemukan maka refer ke object tersebut.
  -------------------------------------------------------------------------------}
  TBitmapsPool = class
  private
    FObjColl : TObjectDictionary<String,TBitmapObject>
  end;


implementation

{ TBitmapObject }

procedure TBitmapObject.LoadBitmap(fName: string);
begin
  if not FileExists(fname) then exit;

  if (fName <> FBitmapName)  then begin

    FBitmapName := fName;
    FBmp.LoadFromFile(fName);

    FBmpClr.Width   := FBmpOrg.Width;
    FBmpClr.Height  := FBmpOrg.Height;

    FBmp.Width  := FBmpOrg.Width;
    FBmp.Height := FBmpOrg.Height;
  end;
end;

end.
