unit ufrmPlatformController;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  TController = class(TForm)
    btnAllRadarOn: TButton;
    Label2: TLabel;
    btnAllSonarOn: TButton;
    Label3: TLabel;
    Label1: TLabel;
    btnAllMADOn: TButton;
    Label4: TLabel;
    btnAllEOOn: TButton;
    btnAllESMOn: TButton;
    Label5: TLabel;
    procedure btnSonarAllClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnRadarAllClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btnMADAllClick(Sender: TObject);
    procedure btnAllEOOnClick(Sender: TObject);
    procedure btnAllESMOnClick(Sender: TObject);
  private
    { Private declarations }
    FAllRadarOn : Boolean;
    FAllSonarOn : Boolean;
    FAllMADOn   : Boolean;
    FAllEOOn    : Boolean;
  public
    { Public declarations }
  end;

var
  Controller: TController;

implementation

{uses
  uT3ServerManager, uT3MountedRadar, tttData, uT3MountedSonar,
  uT3MountedMAD, uT3MountedEO;}

{$R *.dfm}

procedure TController.btnAllEOOnClick(Sender: TObject);
var
  i, cnt : integer;
  sensor : TT3MountedEO;
  obj : TObject;
begin
//
//  TButton(Sender).Enabled := False;
//  if not FAllEOOn then // turn on
//  begin
//    i := serverManager.SimMountedSensor.ItemCount;
//
//    for I := 0 to cnt - 1 do
//    begin
//      obj := serverManager.SimMountedSensor.getObject(i);
//      if Assigned(obj) and (obj is TT3MountedEO) then
//        with TT3MountedEO(obj) do
//        begin
//          OperationalStatus := sopOn;
//          Sleep(10);
//        end;
//    end;
//    btnAllEOOn.Caption := 'OFF';
//  end
//  else
//  begin // turn off
//    i := serverManager.SimMountedSensor.ItemCount;
//
//    for I := 0 to cnt - 1 do
//    begin
//      obj := serverManager.SimMountedSensor.getObject(i);
//      if Assigned(obj) and (obj is TT3MountedEO) then
//        with TT3MountedEO(obj) do
//        begin
//          OperationalStatus := sopOff;
//          Sleep(10);
//        end;
//    end;
//    btnAllEOOn.Caption := 'ON';
//  end;
//
//  TButton(Sender).Enabled := True;
//
//  FAllEOOn := not FAllEOOn;
end;

procedure TController.btnAllESMOnClick(Sender: TObject);
begin
//
end;

procedure TController.btnMADAllClick(Sender: TObject);
var
  i, cnt : integer;
  sensor : TT3MountedRadar;
  obj : TObject;
begin

  TButton(Sender).Enabled := False;
  if not FAllMADOn then // turn on
  begin
    i := serverManager.SimMountedSensor.ItemCount;

    for I := 0 to cnt - 1 do
    begin
      obj := serverManager.SimMountedSensor.getObject(i);
      if Assigned(obj) and (obj is TT3MountedMAD) then
        with TT3MountedMAD(obj) do
        begin
          OperationalStatus := sopOn;
          Sleep(10);
        end;
    end;
    btnAllMADOn.Caption := 'OFF';
  end
  else
  begin // turn off
    i := serverManager.SimMountedSensor.ItemCount;

    for I := 0 to cnt - 1 do
    begin
      obj := serverManager.SimMountedSensor.getObject(i);
      if Assigned(obj) and (obj is TT3MountedMAD) then
        with TT3MountedMAD(obj) do
        begin
          OperationalStatus := sopOff;
          Sleep(10);
        end;
    end;
    btnAllMADOn.Caption := 'ON';
  end;

  TButton(Sender).Enabled := True;

  FAllMADOn := not FAllMADOn;
end;

procedure TController.btnRadarAllClick(Sender: TObject);
var
  i, cnt : integer;
  sensor : TT3MountedRadar;
  obj : TObject;
begin

  TButton(Sender).Enabled := False;
  if not FAllRadarOn then // turn on
  begin
    i := serverManager.SimMountedSensor.ItemCount;

    for I := 0 to cnt - 1 do
    begin
      obj := serverManager.SimMountedSensor.getObject(i);
      if Assigned(obj) and (obj is TT3MountedRadar) then
        with TT3MountedRadar(obj) do
        begin

          case RadarDefinition.FType.Radar_Type_Index of
            0   : begin
                    if OperationalStatus in [sopOff] then
                      ControlMode := rcmSearchTrack;
                  end;
            2,3 : begin
                    if OperationalStatus in [sopOff] then
                      ControlMode := rcmTrack;
                  end;
          end;

          Sleep(10);
        end;
    end;
    btnAllRadarOn.Caption := 'OFF';
  end
  else
  begin // turn off
    i := serverManager.SimMountedSensor.ItemCount;

    for I := 0 to cnt - 1 do
    begin
      obj := serverManager.SimMountedSensor.getObject(i);
      if Assigned(obj) and (obj is TT3MountedRadar) then
        with TT3MountedRadar(obj) do
        begin
          ControlMode := rcmOff;
          Sleep(10);
        end;
    end;
    btnAllRadarOn.Caption := 'ON';
  end;

  TButton(Sender).Enabled := True;

  FAllRadarOn := not FAllRadarOn;
end;

procedure TController.btnSonarAllClick(Sender: TObject);
var
  i, cnt : integer;
  sensor : TT3MountedRadar;
  obj : TObject;
begin

  TButton(Sender).Enabled := False;
  if not FAllSonarOn then // turn on
  begin
    i := serverManager.SimMountedSensor.ItemCount;

    for I := 0 to cnt - 1 do
    begin
      obj := serverManager.SimMountedSensor.getObject(i);
      if Assigned(obj) and (obj is TT3MountedSonar) then
        with TT3MountedSonar(obj) do
        begin

          case SonarDefinition.FDef.Sonar_Classification of
            // active / passive
            0, 2:
              begin
                ControlMode := scmActive;
              end;
            // passive or passive intercept
            1, 3:
              begin
                ControlMode := scmPassive;
              end;
          end;
          Sleep(10);

        end;
    end;
    btnAllSonarOn.Caption := 'OFF';
  end
  else
  begin // turn off
    i := serverManager.SimMountedSensor.ItemCount;

    for I := 0 to cnt - 1 do
    begin
      obj := serverManager.SimMountedSensor.getObject(i);
      if Assigned(obj) and (obj is TT3MountedSonar) then
        with TT3MountedSonar(obj) do
        begin
          ControlMode := scmOff;
          Sleep(10);
        end;
    end;
    btnAllSonarOn.Caption := 'ON';
  end;

  TButton(Sender).Enabled := True;

  FAllSonarOn := not FAllSonarOn;
end;

procedure TController.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;
  Controller := nil;
end;

procedure TController.FormCreate(Sender: TObject);
begin
  FAllRadarOn := False;
  FAllSonarOn := False;
  FAllMADOn   := False;
  FAllEOOn    := False;
end;

end.
