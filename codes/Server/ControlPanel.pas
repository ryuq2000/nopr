unit ControlPanel;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, uT3ServerManager, ExtCtrls, Buttons, ComCtrls, ToolWin,
  ImgList,  uT3TrackManager, uT3Track, Generics.Collections, uT3MountedSensor,
  uSensorInfo, uT3PointTrack, uT3CubicleGroup, uT3PlatformInstance;

type
  TfrmControlPanel = class(TForm)
    Panel1: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    lblGameState: TLabel;
    lblGameSpeed: TLabel;
    lblGameTime: TLabel;
    lblScenario: TLabel;
    ImageList1: TImageList;
    tlb1: TToolBar;
    btnBtnGameFreeze: TToolButton;
    btnStartGame: TToolButton;
    btnBtnDoubleSpeed: TToolButton;
    tmrGameTime: TTimer;
    pgc1: TPageControl;
    ts1: TTabSheet;
    mmo2: TMemo;
    ts2: TTabSheet;
    mmoEvents: TMemo;
    btn1: TButton;
    ts3: TTabSheet;
    ts4: TTabSheet;
    mmoEvent: TMemo;
    ts5: TTabSheet;
    pnl1: TPanel;
    cbb1: TComboBox;
    btn4: TButton;
    btn5: TButton;
    mmo3: TMemo;
    tsDatalink: TTabSheet;
    lblNCS: TLabel;
    lblDL1: TLabel;
    lblDL2: TLabel;
    lblDL3: TLabel;
    mmoDL3: TMemo;
    mmoDL2: TMemo;
    mmoDL1: TMemo;
    tsDataBuffer: TTabSheet;
    mmo1: TMemo;
    btn2: TButton;
    tsGroupTrack: TTabSheet;
    Panel2: TPanel;
    Button1: TButton;
    Button3: TButton;
    ListBox1: TListBox;
    Button4: TButton;
    cbGroup: TComboBox;
    Label5: TLabel;
    lvGroupTrack: TListView;
    procedure FormCreate(Sender: TObject);
    procedure btnStartGameClick(Sender: TObject);
    procedure btnBtnDoubleSpeedClick(Sender: TObject);
    procedure btnBtnGameFreezeClick(Sender: TObject);
    procedure tmrGameTimeTimer(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure cbGroupChange(Sender: TObject);
    procedure Button4Click(Sender: TObject);
  private
    { Private declarations }
    FLastS : string;
    FCounter : Integer;
    FFlagUpdate, FFlagClear  : Boolean;
    procedure AppMessage(var Msg: TMsg; var Handled: Boolean);
    procedure updateGroup;overload;
    procedure updateGroup(trackmanager : TT3TrackManager);overload;
  public
    { Public declarations }
    procedure updateGameInfo(const st: byte; const gSpeed: double);
    procedure updateGroupTrack(trackmanager : TT3TrackManager);
    procedure updateGroupID(trackmanager : TT3TrackManager);
    procedure LogStr(const s: string);
    procedure LogCommontStr(const s: string);
  end;

var
  frmControlPanel: TfrmControlPanel;
  MyMsg : Cardinal;

implementation

uses
  uGameData_TTT {,ufrmPlatformController};

{$R *.dfm}

procedure TfrmControlPanel.AppMessage(var Msg: TMsg; var Handled: Boolean);
begin
  if Msg.Message = MyMsg then begin
     Application.Restore;
     SetForeGroundWindow(Application.MainForm.Handle);
     Handled := true;
  end;
end;

procedure TfrmControlPanel.btnBtnDoubleSpeedClick(Sender: TObject);
var
  gs: double;
  r: TRecCmd_GameCtrl;
begin ;

  if serverManager.GameSpeed >= 1.0 then
  begin
    gs := Round(serverManager.GameSpeed);
    if abs(gs) < 0.0001 then // nol
      gs := 1.0
    else
    begin
      gs := 2.0 * gs;
      if gs > 16.0 then
        exit;
    end;
    serverManager.GameSPEED := gs;

    r.GameCtrl := CORD_ID_game_speed;
    r.GameSpeed := gs;

    serverManager.NetCmdSender.CmdGameControl(r);
  end;
end;

procedure TfrmControlPanel.btnBtnGameFreezeClick(Sender: TObject);
var
  r: TRecCmd_GameCtrl;
begin
  serverManager.GamePause;
  serverManager.GameSPEED := 0.0;

  r.GameCtrl := CORD_ID_pause;
  r.GameSpeed := 0.0;

  serverManager.NetCmdSender.CmdGameControl(r);
end;

procedure TfrmControlPanel.btnStartGameClick(Sender: TObject);
var
  r: TRecCmd_GameCtrl;
begin ;
  r.GameCtrl := CORD_ID_start;
  r.GameSpeed := 1.0;

  serverManager.GameStart;
  serverManager.GameSPEED := 1.0;

  serverManager.NetCmdSender.CmdGameControl(r);
end;

procedure TfrmControlPanel.Button1Click(Sender: TObject);
begin
  FFlagUpdate := True;
end;

procedure TfrmControlPanel.Button2Click(Sender: TObject);
begin
//  if not Assigned(Controller) then
//    Controller := TController.Create(self);
//
//  Controller.Show;
end;

procedure TfrmControlPanel.Button3Click(Sender: TObject);
begin
  FFlagClear := True;
end;

procedure TfrmControlPanel.Button4Click(Sender: TObject);
var
  item : TT3PlatformInstance;
  I: Integer;
begin
  ListBox1.Clear;
  for I := 0 to serverManager.SimPlatforms.ItemCount - 1 do
  begin
    item := serverManager.SimPlatforms.getObject(I) as TT3PlatformInstance;
    ListBox1.Items.Add( IntToStr(item.InstanceIndex) + ':' + item.InstanceName );
  end;

end;

procedure TfrmControlPanel.cbGroupChange(Sender: TObject);
begin

  FFlagUpdate := True;
end;

procedure TfrmControlPanel.FormCreate(Sender: TObject);
begin
  Application.OnMessage := AppMessage;
  FCounter := 0;
end;

procedure TfrmControlPanel.LogCommontStr(const s: string);
begin
  if mmoEvent.Lines.Count > 1000 then
    mmoEvent.Lines.CLear;

  mmoEvent.Lines.Insert(0, s)

end;

procedure TfrmControlPanel.LogStr(const s: string);
begin
  if s <> FLastS then
    mmo2.Lines.Insert(0, s)
  else
    mmo2.Lines[0] := mmo2.Lines[0] + '*';

  FLastS := s;

end;

procedure TfrmControlPanel.tmrGameTimeTimer(Sender: TObject);
var dt: TDateTime;
begin
  dt := serverManager.GameTIME;
  lblGameTime.Caption := FormatDateTime('hh : nn : ss', dt);
end;

procedure TfrmControlPanel.updateGameInfo(const st: byte;
      const gSpeed: double);
begin
  case st of
    0: lblGameState.Caption := 'Stopped';
    1: lblGameState.Caption := 'Running';
  end;
  if gSpeed >= 1.0 then
    lblGameSpeed.Caption := IntToStr(Round(gSpeed))
  else
    lblGameSpeed.Caption := FormatFloat('0.00', gSpeed);

end;

procedure TfrmControlPanel.updateGroup(trackmanager : TT3TrackManager);
var
  status     : String;
  grp        : integer;
  track      : TPair<integer,TT3Track>;
  cubTrack   : TT3CubicleGroup;
  li         : TListItem;
  sensordata : TObjectDictionary<TT3MountedSensor,TSensorTrackInfo> ;
  sensor     : TPair<TT3MountedSensor,TSensorTrackInfo>;
begin

  if (cbGroup.Items.Count < 0) or
    (cbGroup.ItemIndex < 0) then
    Exit;

  lvGroupTrack.Items.Clear;

  TryStrToInt(cbGroup.Items[cbGroup.ItemIndex],grp);
  trackmanager.CubicleTracks.TryGetValue(grp,cubTrack);

  if Assigned(cubTrack) then
    for track in cubTrack.Tracks do
    begin
      li := lvGroupTrack.Items.Add;
      li.Data := track.Value;

      with track.Value do begin
        li.Caption := IntToStr(TrackNumber);
        li.SubItems.Add(IntToStr(ObjectInstanceIndex));

        case TrackType of
          trBearing : li.SubItems.Add('Bearing');
          trPoint : li.SubItems.Add('Point');
          trAOP   : li.SubItems.Add('AOP');
        end;

        case TrackCategory of
          tcRealTrack : li.SubItems.Add('RealTime');
          tcNonRealTrack : li.SubItems.Add('NRT');
        end;

        if (track.Value is TT3PointTrack) and (TT3PointTrack(track.Value).CanControl) then
          li.SubItems.Add('YES')
        else
          li.SubItems.Add('NO');

        li.SubItems.Add(TimeToStr(FirstDetected));
        li.SubItems.Add(TimeToStr(LastDetected));

        sensordata := DetectedBySensors;
        status := '';

        if sensordata.Count <= 0 then
          status := '[None]'
        else
        begin
          status := '';
          for sensor in sensordata do
          begin
            status := status + sensor.Key.InstanceName;
            status := status + ',';
          end;
        end;

        li.SubItems.Add(status);
      end;
    end;
end;

procedure TfrmControlPanel.updateGroupTrack(trackmanager: TT3TrackManager);
begin

  if FFlagUpdate then
  begin
    updateGroup(serverManager.TrackManager);
    FFlagUpdate := False;
  end
  else
    updateGroup;

  if FFlagClear then
  begin
   lvGroupTrack.Items.Clear;
   FFlagClear := False;
  end;

end;

procedure TfrmControlPanel.updateGroup;
var
  status     : String;
  li         : TListItem;
  I          : integer;
  sensordata : TObjectDictionary<TT3MountedSensor,TSensorTrackInfo> ;
  sensor     : TPair<TT3MountedSensor,TSensorTrackInfo>;
begin

  for I := 0 to lvGroupTrack.Items.Count - 1 do
  begin
    li := lvGroupTrack.Items[I];

    if Assigned(li.Data) then
    begin
      with TT3Track(li.Data) do begin
        li.Caption := IntToStr(TrackNumber);
        li.SubItems[0] := IntToStr(ObjectInstanceIndex);

        case TrackType of
          trBearing : li.SubItems[1] := 'Bearing';
          trPoint   : li.SubItems[1] := 'Point';
          trAOP     : li.SubItems[1] := 'AOP';
        end;

        case TrackCategory of
          tcRealTrack : li.SubItems[2] := 'RealTime';
          tcNonRealTrack : li.SubItems[2] := 'NRT';
        end;

        if (TT3Track(li.Data) is TT3PointTrack) and (TT3PointTrack(li.Data).CanControl) then
          li.SubItems[3] := 'YES'
        else
          li.SubItems[3] := 'NO';

        li.SubItems[4] := TimeToStr(FirstDetected);
        li.SubItems[5] := TimeToStr(LastDetected);

        sensordata := DetectedBySensors;
        status := '';

        if sensordata.Count <= 0 then
          status := '[None]'
        else
        begin
          status := '';
          for sensor in sensordata do
          begin
            status := status + sensor.Key.InstanceName;
            status := status + ',';
          end;
        end;

        li.SubItems[6] := status;
      end;
    end;

  end;
end;

procedure TfrmControlPanel.updateGroupID(trackmanager: TT3TrackManager);
var
  pair : TCubicleTrackPair;
begin
  cbGroup.Items.Clear;

  for pair in trackmanager.CubicleTracks do
    cbGroup.Items.Add(IntToStr(pair.Key));

  if cbGroup.Items.Count > 0 then
    cbGroup.ItemIndex := 0;
end;

end.
