unit uNetServerSender;

interface

uses
  uNetSender, uGameData_TTT;

type
  TNetServerCmdSender = class(TNetCmdSender)

  public
    procedure CmdGameControl(var r: TRecCmd_GameCtrl); override;
    procedure CmdPlatformGuidance(var r : TRecCmd_PlatformGuidance ); override;
    procedure CmdSensor(var r : TRecCmd_Sensor);override;
    procedure CmdSyncWaypoint(var r : TrecSinc_Waypoint); override;
    procedure CmdSynTrack(var r : TRecTrack);override;
    procedure CmdRepos(var r : TRecCmd_Platform_MOVE); override;
    procedure CmdLaunchRP(var r : TRecCmd_LaunchRP); override;

    {*------------------------------------------------------------------------------
      Client command from countermeasure control panel
    -------------------------------------------------------------------------------}
    procedure CmdAcousticDecoyOnBoard(var r : TRecCmdAcousticDecoyOnBoard); override;
    procedure CmdChaffOnBoard(var r : TRecCmdChaffOnBoard); override;
    procedure CmdBubbleOnBoard(var r : TRecCmdBubbleOnBoard); override;
    procedure CmdFloatingDecoyOnBoard(var r : TRecCmdFloatingDecoyOnBoard); override;
    procedure CmdSelfDefenceOnBoard(var r : TRecCmdSelfDefenseOnBoard); override;
    procedure CmdRadarNoiseOnBoard(var r : TRecCmdRadarNoiseOnBoard); override;
    {*------------------------------------------------------------------------------
    -------------------------------------------------------------------------------}

    procedure CmdGameControlInfoTo(rec : TRecUDP_GameCtrl_info; ipDest : string);
    procedure CmdGameTime(var r: TRecUDP_GameTime);
    procedure CmdPlatformPropertiesUpdate(var r: TRecUDP_PlatformMovement);

  end;

implementation

uses
  uNetHandle_Server, uGlobalVar;

{ TNetServerCmdSender }

procedure TNetServerCmdSender.CmdAcousticDecoyOnBoard(
  var r: TRecCmdAcousticDecoyOnBoard);
begin
  inherited;
  TNetHandle_Server(nethandle).SendBroadcast_UDP_Data(CPID_CMD_ACOUSTIC_DECOY_ONBOARD , @r);

end;

procedure TNetServerCmdSender.CmdBubbleOnBoard(var r: TRecCmdBubbleOnBoard);
begin
  inherited;
  TNetHandle_Server(nethandle).SendBroadcast_UDP_Data(CPID_CMD_BUBBLE_ONBOARD , @r);
end;

procedure TNetServerCmdSender.CmdChaffOnBoard(var r: TRecCmdChaffOnBoard);
begin
  inherited;
  TNetHandle_Server(nethandle).SendBroadcast_UDP_Data(CPID_CMD_CHAFF_ONBOARD , @r);
end;

procedure TNetServerCmdSender.CmdFloatingDecoyOnBoard(
  var r: TRecCmdFloatingDecoyOnBoard);
begin
  inherited;
  TNetHandle_Server(nethandle).SendBroadcast_UDP_Data(CPID_CMD_FLOATING_ONBOARD , @r);
end;

procedure TNetServerCmdSender.CmdGameControl(var r: TRecCmd_GameCtrl);
begin
  inherited;
  TNetHandle_Server(nethandle).SendBroadcast_UDP_Data(CPID_CMD_GAME_CTRL , @r);
end;

procedure TNetServerCmdSender.CmdGameControlInfoTo(rec: TRecUDP_GameCtrl_info;
  ipDest: string);
begin
  TNetHandle_Server(nethandle).SendUDP_To(CPID_UDP_GAMECTRL_INFO, @rec, ipDest);
end;

procedure TNetServerCmdSender.CmdGameTime(var r: TRecUDP_GameTime);
begin
  TNetHandle_Server(nethandle).SendBroadcast_UDP_Data(CPID_GAMETIME , @r);
end;

procedure TNetServerCmdSender.CmdLaunchRP(var r: TRecCmd_LaunchRP);
begin
  inherited;
  TNetHandle_Server(nethandle).SendBroadcast_UDP_Data(CPID_CMD_LAUNCH_RUNTIME_PLATFORM , @r);
end;

procedure TNetServerCmdSender.CmdPlatformPropertiesUpdate(var r: TRecUDP_PlatformMovement);
begin
  TNetHandle_Server(nethandle).SendBroadcast_UDP_Data(CPID_PLATFORM_MOVE , @r);
end;

procedure TNetServerCmdSender.CmdPlatformGuidance(var r: TRecCmd_PlatformGuidance);
begin
  inherited;
  TNetHandle_Server(nethandle).SendBroadcast_UDP_Data(CPID_CMD_PLATFORM , @r);
end;

procedure TNetServerCmdSender.CmdRadarNoiseOnBoard(
  var r: TRecCmdRadarNoiseOnBoard);
begin
  inherited;
  TNetHandle_Server(nethandle).SendBroadcast_UDP_Data(CPID_CMD_RADARNOISE_ONBOARD , @r);
end;

procedure TNetServerCmdSender.CmdRepos(var r: TRecCmd_Platform_MOVE);
begin
  inherited;
  TNetHandle_Server(nethandle).SendBroadcast_UDP_Data(CPID_CMD_PLATFORM_REPOS , @r);
end;

procedure TNetServerCmdSender.CmdSelfDefenceOnBoard(
  var r: TRecCmdSelfDefenseOnBoard);
begin
  inherited;
  TNetHandle_Server(nethandle).SendBroadcast_UDP_Data(CPID_CMD_SELFDEFENSE_ONBOARD , @r);
end;

procedure TNetServerCmdSender.CmdSensor(var r: TRecCmd_Sensor);
begin
  inherited;
  TNetHandle_Server(nethandle).SendBroadcast_UDP_Data(CPID_CMD_SENSOR , @r);
end;

procedure TNetServerCmdSender.CmdSyncWaypoint(var r: TrecSinc_Waypoint);
begin
  inherited;
  TNetHandle_Server(nethandle).SendBroadcast_UDP_Data(CPID_SincWaypoint , @r);
end;

procedure TNetServerCmdSender.CmdSynTrack(var r: TRecTrack);
begin
  inherited;
  r.SessionID := FSessionID;
  TNetHandle_Server(nethandle).SendBroadcast_UDP_Data(CPID_UDP_TRACK_SYNC , @r);
end;

end.
