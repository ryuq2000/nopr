unit uScriptSimServer;


interface

  procedure BeginGame_Server;
  procedure EndGame_Server;

implementation

uses
  SysUtils,
  uScriptCommon,
  uMapXData,
  uDBScenario,
  uLibSettingTTT,
  uT3ServerManager,
  ControlPanel,
  uNetHandle_Server,
  //uT3ServerEventManager,
  uCoordConvertor,
  uGamePlayType,
  uSRRFunction,
  uGlobalVar;

procedure BeginGame_Server;
begin
  BeginGameCommon;

  {procedure dipindah untuk load peta sama dengan db}
  VMapData := TSimMapData.Create;
//  if FileExists(vMapSetting.MapDataGeoset)  then
//    VMapData.LoadDataMap(vMapSetting.MapDataGeoset);

  serverManager := TT3ServerManager.Create(VMapData.DMap);
  simManager := serverManager;

  serverManager.OnLogStr         := frmControlPanel.LogStr;
  serverManager.OnCommonLogStr   := frmControlPanel.LogCommontStr;
  //serverManager.OnLogEventStr    := frmControlPanel.LogInitStr;
  //serverManager.EventManager.OnLogEventStr := frmControlPanel.LogInitStr;
  serverManager.OnUpdateGameInfo := frmControlPanel.updateGameInfo;
  serverManager.onUpdateFormTrack:= frmControlPanel.updateGroupTrack;
  serverManager.SessionID        := vGameDataSetting.GameSessionID;

//  if TGamePlayType(vGameDataSetting.GamePlayMode.GameType)=gpmReplay then
//     simMgrServer.OnReplayStr      := fMainGServer.LogReplayGetFrame;

  if UseSnapshot(vGameDataSetting) then
  begin
//    simMgrServer.LoadFromSnapshot := True;
//    simMgrServer.SnapshotLoadFile(vGameDataSetting.SnapshotName);
  end
  else
  begin
//    simMgrServer.LoadFromSnapshot := False;
    serverManager.LoadScenarioId(vGameDataSetting);
//    serverManager.TrackManager.initCubicleGroup;
  end;

  LoadFF_NetClientSetting(vSettingFile, vNetClientSetting);
  LoadFF_NetServerSetting(vSettingFile, vNetServerSetting);

  nethandle                := TNetHandle_Server.Create;
  with TNetHandle_Server(nethandle) do
  begin
    GamePort         := vNetSetting.GamePort;
    CommandPort      := vNetSetting.CommandPort;
    BroadCastAddress := vNetSetting.GameAddress;
    SessionPort      := vNetSetting.SessionPort;
    SessionServerIP  := vNetClientSetting.ServerIP;
    StubPort         := vNetSetting.MapStubPort;
    StubAddr         := vNetSetting.MapStubAddr;

    OnClientConnect    :=  serverManager.FNetServerOnClientConnect;
    OnClientDisConnect :=  serverManager.FNetServerOnClientDisConnect;
//    OnConnectedToSession := serverManager.netSessionOnConnectedRequestCubicle;

    StartNetworking;  // Create server port
    OnlogRecv := frmControlPanel.LogStr;
  end;

  serverManager.InitNetwork;    // register procedure
  //VNetServer.startListen di pindah, dijalankan setelah
  //sequence request session selesai.
  serverManager.BroadcastData := true;
  frmControlPanel.updateGroupID(serverManager.TrackManager);

//  fMainGServer.UpdateViewSetting;
//
//  simMgrServer.LoadPredefinedPattern;

end;

procedure EndGame_Server;
begin

  serverManager.GamePause;
  serverManager.Free;

  TNetHandle_Server(nethandle).StopNetworking;
  TNetHandle_Server(nethandle).Free;

  VMapData.Free;
end;



end.


