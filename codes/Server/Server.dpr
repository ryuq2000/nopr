program Server;

uses
  FastMM4,
  Windows,
  Messages,
  Forms,
  ControlPanel in 'ControlPanel.pas' {frmControlPanel},
  uBaseSimObjects in '..\SimFramework\LibSims\SimSystem\uBaseSimObjects.pas',
  uSimObjects in '..\SimFramework\LibSims\SimSystem\uSimObjects.pas',
  uDataTypes in '..\SimFramework\LibBaseUtils\CoordSystem\uDataTypes.pas',
  uBaseCoordSystem in '..\SimFramework\LibBaseUtils\CoordSystem\uBaseCoordSystem.pas',
  uStringFunc in '..\SimFramework\LibBaseUtils\StringUtils\uStringFunc.pas',
  uSimVisuals in '..\LibTTT\libVisual\uSimVisuals.pas',
  uCoordConvertor in '..\SimFramework\LibBaseUtils\CoordSystem\uCoordConvertor.pas',
  uSimContainers in '..\SimFramework\LibSims\SimSystem\uSimContainers.pas',
  uThreadTimer in '..\SimFramework\LibBaseUtils\Counter\uThreadTimer.pas',
  uVirtualTime in '..\SimFramework\LibBaseUtils\Counter\uVirtualTime.pas',
  uT3SimContainer in '..\LibTTT\libContainer\uT3SimContainer.pas',
  uT3DrawContainer in '..\LibTTT\libContainer\uT3DrawContainer.pas',
  uScriptCommon in '..\LibTTT\libCommon\uScriptCommon.pas',
  uLibSettingTTT in '..\LibTTT\libCommon\uLibSettingTTT.pas',
  uScriptSimServer in 'libServer\uScriptSimServer.pas',
  uIniFilesProcs in '..\SimFramework\LibBaseUtils\uIniFilesProcs.pas',
  uCodecBase64 in '..\SimFramework\LibBaseUtils\StringUtils\uCodecBase64.pas',
  uMapXData in '..\LibTTT\libCommon\uMapXData.pas',
  uNetHandle_TTT in '..\LibTTT\netTTT\uNetHandle_TTT.pas',
  uBaseNetHandler in '..\SimFramework\LibNets\uBaseNetHandler.pas',
  uNetHandler in '..\SimFramework\LibNets\uNetHandler.pas',
  uNetHandle_Server in 'libServer\uNetHandle_Server.pas',
  uNetBaseSocket in '..\SimFramework\LibNets\NetComponent\uNetBaseSocket.pas',
  uNetUDPnode in '..\SimFramework\LibNets\NetComponent\uNetUDPnode.pas',
  uPacketBuffer in '..\SimFramework\LibNets\NetComponent\uPacketBuffer.pas',
  uPacketRegister in '..\SimFramework\LibNets\NetComponent\uPacketRegister.pas',
  uTCPClient in '..\SimFramework\LibNets\NetComponent\uTCPClient.pas',
  uTCPServer in '..\SimFramework\LibNets\NetComponent\uTCPServer.pas',
  uDataBuffer in '..\SimFramework\LibNets\NetComponent\uDataBuffer.pas',
  uGameData_TTT in '..\LibTTT\netTTT\uGameData_TTT.pas',
  uTCPDatatype in '..\SimFramework\LibNets\NetComponent\uTCPDatatype.pas',
  uSteppers in '..\SimFramework\LibBaseUtils\Counter\uSteppers.pas',
  uMapXHandler in '..\LibTTT\libCommon\uMapXHandler.pas',
  uDBScenario in '..\LibTTT\libDBScenario\uDBScenario.pas',
  ufProgress in '..\LibTTT\forms\ufProgress.pas' {frmProgress},
  uNetSessionData in '..\LibTTT\libNetSession\uNetSessionData.pas',
  ufDBSetting in '..\LibTTT\forms\ufDBSetting.pas' {frmDBSetting},
  u2DMover in '..\SimFramework\LibSims\SimsObjects\u2DMover.pas',
  uT3Common in '..\LibTTT\libCommon\uT3Common.pas',
  uSRRFunction in '..\LibTTT\tttReplay\uSRRFunction.pas',
  uGamePlayType in '..\LibTTT\tttReplay\uGamePlayType.pas',
  uSnapshotUtils in '..\LibTTT\tttReplay\uSnapshotUtils.pas',
  uDBClassDefinition in '..\LibTTT\libDBScenario\uDBClassDefinition.pas',
  uDMLite in '..\LibTTT\forms\uDMLite.pas' {DMLite: TDataModule},
  uT3Listener in '..\LibTTT\libUnit\uT3Listener.pas',
  uNetSender in '..\LibTTT\libCommon\uNetSender.pas',
  tttData in '..\LibTTT\libDBScenario\tttData.pas',
  uSimManager in '..\LibTTT\libManager\uSimManager.pas',
  uT3SimManager in '..\LibTTT\libManager\uT3SimManager.pas',
  uT3ServerManager in '..\LibTTT\libManager\uT3ServerManager.pas',
  uT3ObjectFactory in '..\LibTTT\libFactory\uT3ObjectFactory.pas',
  uT3PlatformInstance in '..\LibTTT\libUnit\platform\uT3PlatformInstance.pas',
  uT3Unit in '..\LibTTT\libUnit\uT3Unit.pas',
  uT3MountedDevice in '..\LibTTT\libUnit\mounted\uT3MountedDevice.pas',
  uT3Vehicle in '..\LibTTT\libUnit\platform\uT3Vehicle.pas',
  uT3Satellite in '..\LibTTT\libUnit\platform\uT3Satellite.pas',
  uT3Sonobuoy in '..\LibTTT\libUnit\platform\uT3Sonobuoy.pas',
  uT3Mine in '..\LibTTT\libUnit\platform\uT3Mine.pas',
  uT3Torpedo in '..\LibTTT\libUnit\platform\uT3Torpedo.pas',
  uT3Missile in '..\LibTTT\libUnit\platform\uT3Missile.pas',
  uT3CircleGuide in '..\LibTTT\libUnit\guidance\uT3CircleGuide.pas',
  uT3EngagementGuide in '..\LibTTT\libUnit\guidance\uT3EngagementGuide.pas',
  uT3EvasionGuide in '..\LibTTT\libUnit\guidance\uT3EvasionGuide.pas',
  uT3FormationGuide in '..\LibTTT\libUnit\guidance\uT3FormationGuide.pas',
  uT3GuidanceFactory in '..\LibTTT\libUnit\guidance\uT3GuidanceFactory.pas',
  uT3HelmGuide in '..\LibTTT\libUnit\guidance\uT3HelmGuide.pas',
  uT3OutrunGuide in '..\LibTTT\libUnit\guidance\uT3OutrunGuide.pas',
  uT3ReturnBaseGuide in '..\LibTTT\libUnit\guidance\uT3ReturnBaseGuide.pas',
  uT3ShadowGuide in '..\LibTTT\libUnit\guidance\uT3ShadowGuide.pas',
  uT3SinuationGuide in '..\LibTTT\libUnit\guidance\uT3SinuationGuide.pas',
  uT3StationGuide in '..\LibTTT\libUnit\guidance\uT3StationGuide.pas',
  uT3StraightLineGuide in '..\LibTTT\libUnit\guidance\uT3StraightLineGuide.pas',
  uT3VehicleGuidance in '..\LibTTT\libUnit\guidance\uT3VehicleGuidance.pas',
  uT3WaypointGuide in '..\LibTTT\libUnit\guidance\uT3WaypointGuide.pas',
  uT3ZigzagGuide in '..\LibTTT\libUnit\guidance\uT3ZigzagGuide.pas',
  uT3Hybrid in '..\LibTTT\libUnit\platform\uT3Hybrid.pas',
  uGlobalVar in '..\LibTTT\libCommon\uGlobalVar.pas',
  uNetServerSender in 'libServer\uNetServerSender.pas',
  uT3BlindZone in '..\LibTTT\libUnit\uT3BlindZone.pas',
  uT3SonarCD in '..\LibTTT\libCommon\uT3SonarCD.pas',
  uT3BearingTrack in '..\LibTTT\libUnit\uT3BearingTrack.pas',
  uT3PointTrack in '..\LibTTT\libUnit\uT3PointTrack.pas',
  uT3Track in '..\LibTTT\libUnit\uT3Track.pas',
  uT3MountedEO in '..\LibTTT\libUnit\mounted\mountedsensor\uT3MountedEO.pas',
  uT3MountedESM in '..\LibTTT\libUnit\mounted\mountedsensor\uT3MountedESM.pas',
  uT3MountedIFF in '..\LibTTT\libUnit\mounted\mountedsensor\uT3MountedIFF.pas',
  uT3MountedMAD in '..\LibTTT\libUnit\mounted\mountedsensor\uT3MountedMAD.pas',
  uT3MountedRadar in '..\LibTTT\libUnit\mounted\mountedsensor\uT3MountedRadar.pas',
  uT3MountedSensor in '..\LibTTT\libUnit\mounted\mountedsensor\uT3MountedSensor.pas',
  uT3MountedSonar in '..\LibTTT\libUnit\mounted\mountedsensor\uT3MountedSonar.pas',
  uT3MountedSonoBuoy in '..\LibTTT\libUnit\mounted\mountedsensor\uT3MountedSonoBuoy.pas',
  uT3MountedVisual in '..\LibTTT\libUnit\mounted\mountedsensor\uT3MountedVisual.pas',
  uT3MountedAcousticDeploy in '..\LibTTT\libUnit\mounted\mountedECM\uT3MountedAcousticDeploy.pas',
  uT3MountedDefensiveJammer in '..\LibTTT\libUnit\mounted\mountedECM\uT3MountedDefensiveJammer.pas',
  uT3MountedECM in '..\LibTTT\libUnit\mounted\mountedECM\uT3MountedECM.pas',
  uT3MountedRadarNoiseJammer in '..\LibTTT\libUnit\mounted\mountedECM\uT3MountedRadarNoiseJammer.pas',
  uT3MountedAirborneChaff in '..\LibTTT\libUnit\mounted\mountedECM\mountedchaff\uT3MountedAirborneChaff.pas',
  uT3MountedChaff in '..\LibTTT\libUnit\mounted\mountedECM\mountedchaff\uT3MountedChaff.pas',
  uT3MountedChaffLauncher in '..\LibTTT\libUnit\mounted\mountedECM\mountedchaff\uT3MountedChaffLauncher.pas',
  uT3MountedSurfaceChaff in '..\LibTTT\libUnit\mounted\mountedECM\mountedchaff\uT3MountedSurfaceChaff.pas',
  uSensorInfo in '..\LibTTT\libCommon\uSensorInfo.pas',
  uT3RadarVerticalCoverage in '..\LibTTT\libCommon\uT3RadarVerticalCoverage.pas',
  uT3MountedActiveAcousticTorp in '..\LibTTT\libUnit\mounted\mountedweapon\mountedTorpedo\uT3MountedActiveAcousticTorp.pas',
  uT3MountedActivePassiveTorp in '..\LibTTT\libUnit\mounted\mountedweapon\mountedTorpedo\uT3MountedActivePassiveTorp.pas',
  uT3MountedAirDroppedTorp in '..\LibTTT\libUnit\mounted\mountedweapon\mountedTorpedo\uT3MountedAirDroppedTorp.pas',
  uT3MountedStraigthTorpedo in '..\LibTTT\libUnit\mounted\mountedweapon\mountedTorpedo\uT3MountedStraigthTorpedo.pas',
  uT3MountedTorpedo in '..\LibTTT\libUnit\mounted\mountedweapon\mountedTorpedo\uT3MountedTorpedo.pas',
  uT3MountedWakeHomeTorp in '..\LibTTT\libUnit\mounted\mountedweapon\mountedTorpedo\uT3MountedWakeHomeTorp.pas',
  uT3MountedWireGuided in '..\LibTTT\libUnit\mounted\mountedweapon\mountedTorpedo\uT3MountedWireGuided.pas',
  uT3MountedHybridMissile in '..\LibTTT\libUnit\mounted\mountedweapon\mountedMissile\uT3MountedHybridMissile.pas',
  uT3MountedMissile in '..\LibTTT\libUnit\mounted\mountedweapon\mountedMissile\uT3MountedMissile.pas',
  uT3MountedSurfaceToAirMissile in '..\LibTTT\libUnit\mounted\mountedweapon\mountedMissile\uT3MountedSurfaceToAirMissile.pas',
  uT3MountedSurfaceToSurfaceMissile in '..\LibTTT\libUnit\mounted\mountedweapon\mountedMissile\uT3MountedSurfaceToSurfaceMissile.pas',
  uT3MountedTacticalMissiles in '..\LibTTT\libUnit\mounted\mountedweapon\mountedMissile\uT3MountedTacticalMissiles.pas',
  uT3MountedGun in '..\LibTTT\libUnit\mounted\mountedweapon\mountedGun\uT3MountedGun.pas',
  uT3MountedGunAutoManual in '..\LibTTT\libUnit\mounted\mountedweapon\mountedGun\uT3MountedGunAutoManual.pas',
  uT3MountedGunCIWS in '..\LibTTT\libUnit\mounted\mountedweapon\mountedGun\uT3MountedGunCIWS.pas',
  uT3MountedBomb in '..\LibTTT\libUnit\mounted\mountedweapon\uT3MountedBomb.pas',
  uT3MountedHybrid in '..\LibTTT\libUnit\mounted\mountedweapon\uT3MountedHybrid.pas',
  uT3MountedMine in '..\LibTTT\libUnit\mounted\mountedweapon\uT3MountedMine.pas',
  uT3MountedWeapon in '..\LibTTT\libUnit\mounted\mountedweapon\uT3MountedWeapon.pas',
  uT3Environment in '..\LibTTT\libCommon\uT3Environment.pas',
  uCalculationEnvi in '..\LibTTT\libCommon\uCalculationEnvi.pas',
  uT3TrackManager in '..\LibTTT\libManager\uT3TrackManager.pas',
  uT3MountedAirBubble in '..\LibTTT\libUnit\mounted\mountedECM\uT3MountedAirBubble.pas',
  uT3MountedFloatingDecoy in '..\LibTTT\libUnit\mounted\mountedECM\uT3MountedFloatingDecoy.pas',
  uT3MountedInfrared in '..\LibTTT\libUnit\mounted\mountedECM\uT3MountedInfrared.pas',
  uT3MountedTowedJammer in '..\LibTTT\libUnit\mounted\mountedECM\uT3MountedTowedJammer.pas',
  uT3EventManager in '..\LibTTT\libManager\uT3EventManager.pas',
  uT3CubicleGroup in '..\LibTTT\libCommon\uT3CubicleGroup.pas',
  uT3ServerEventManager in 'libServer\uT3ServerEventManager.pas',
  uTMapTouch in '..\LibTTT\libCommon\uTMapTouch.pas';

{$R *.res}

var
  Mutex : THandle;

begin
  MyMsg := RegisterWindowMessage('TTTGameServer_Message');
  Mutex := CreateMutex(nil, True, 'TTTGameServer_Mutex');
  if (Mutex = 0) OR (GetLastError = ERROR_ALREADY_EXISTS) then
  begin
    SendMessage(HWND_BROADCAST, MyMsg, 0, 0);

    Application.MessageBox('Another Game Server has already running.',
      'TTT Game Server', MB_OK + MB_ICONINFORMATION);
  end
  else
  begin
    Application.Initialize;
    Application.MainFormOnTaskbar := True;
    Application.CreateForm(TDMLite, DMLite);
    Application.CreateForm(TfrmControlPanel, frmControlPanel);
    uScriptSimServer.BeginGame_Server;

    Application.Run;

    uScriptSimServer.EndGame_Server;
    if Mutex <> 0 then
      CloseHandle(Mutex);
  end;
end.
