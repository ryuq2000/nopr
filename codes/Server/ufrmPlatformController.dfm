object Controller: TController
  Left = 0
  Top = 0
  BorderStyle = bsToolWindow
  Caption = 'Controller'
  ClientHeight = 233
  ClientWidth = 228
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label2: TLabel
    Left = 16
    Top = 8
    Width = 86
    Height = 13
    Caption = 'All Platform Radar'
  end
  object Label3: TLabel
    Left = 128
    Top = 8
    Width = 85
    Height = 13
    Caption = 'All Platform Sonar'
  end
  object Label1: TLabel
    Left = 16
    Top = 56
    Width = 79
    Height = 13
    Caption = 'All Platform MAD'
  end
  object Label4: TLabel
    Left = 128
    Top = 56
    Width = 71
    Height = 13
    Caption = 'All Platform EO'
  end
  object Label5: TLabel
    Left = 16
    Top = 112
    Width = 77
    Height = 13
    Caption = 'All Platform ESM'
  end
  object btnAllRadarOn: TButton
    Tag = 1
    Left = 16
    Top = 27
    Width = 81
    Height = 25
    Caption = 'ON'
    TabOrder = 0
    OnClick = btnRadarAllClick
  end
  object btnAllSonarOn: TButton
    Tag = 3
    Left = 128
    Top = 27
    Width = 85
    Height = 25
    Caption = 'ON'
    TabOrder = 1
    OnClick = btnSonarAllClick
  end
  object btnAllMADOn: TButton
    Tag = 1
    Left = 16
    Top = 75
    Width = 81
    Height = 25
    Caption = 'ON'
    TabOrder = 2
    OnClick = btnMADAllClick
  end
  object btnAllEOOn: TButton
    Tag = 1
    Left = 128
    Top = 75
    Width = 81
    Height = 25
    Caption = 'ON'
    TabOrder = 3
    OnClick = btnAllEOOnClick
  end
  object btnAllESMOn: TButton
    Tag = 1
    Left = 16
    Top = 131
    Width = 81
    Height = 25
    Caption = 'ON'
    TabOrder = 4
    OnClick = btnAllESMOnClick
  end
end
